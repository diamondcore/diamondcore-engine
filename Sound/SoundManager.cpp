#include "SoundManager.h"

#include <assert.h>
#include <algorithm>
#include <math.h>

int SoundManager::MAX_SOUNDS = 50;
int SoundManager::MAX_CHANNELS = 5;

void SoundManager::Initialize()
{
	static bool isInitialized = false;

	if (isInitialized)
		return;

	mIDCounter = 0;

	mMasterVolume = 1.0f;

	mElapsedFadeTime = 0;

	isInitialized = true;
	mFadeBGM = false;

	FMOD_RESULT result;
	FMOD::System_Create(&mSoundSystem);
	mSoundSystem->init(MAX_SOUNDS, FMOD_INIT_NORMAL, NULL);

	// Initialize our channelgroups
	for (long double x = 0; x < MAX_CHANNELS; x++)
	{
		FMOD::ChannelGroup* newGroup;
		mSoundSystem->createChannelGroup(to_string(x).c_str(), &newGroup);
		mChannelGroups.push_back(newGroup);
	}

	mBackgroundMusic[0] = NULL;
	mBackgroundMusic[1] = NULL;
}

SoundState SoundManager::GetState(int id)
{
	auto iter = mSoundPool.find(id);
	if (iter == mSoundPool.end())
	{
		// couldnt find sound.
		assert(false);
		return STOPPED;
	}

	return iter->second->GetState();
}

FMOD::Sound* SoundManager::loadSound(string soundToLoad, bool is3D, bool isLooping, bool isStreaming)
{
	// load the sound if it hasn't been yet.
	if (mLoadedSounds.find(soundToLoad) != mLoadedSounds.end())
		return mLoadedSounds[soundToLoad];

	FMOD::Sound* loadedSound;
	FMOD_MODE soundMode;

	if (is3D)
		soundMode = FMOD_3D;
	else
		soundMode = FMOD_SOFTWARE;

	if (isLooping)
		soundMode |= FMOD_LOOP_NORMAL;

	if (isStreaming)
		mSoundSystem->createStream(soundToLoad.c_str(), soundMode, 0, &loadedSound);
	else
		mSoundSystem->createSound(soundToLoad.c_str(), soundMode, 0, &loadedSound);

	mLoadedSounds.insert(make_pair(soundToLoad, loadedSound));

	return loadedSound;
}

void SoundManager::PlayBGM(string fileName, float fadeDuration)
{
	if (mFadeBGM)
		return;

	// Slot 1 is the currently playing track, slot2 is the track to be played.

	// if not crossfading or there's no current track playing, play instantly.

	auto bgm = mSoundPool[CreateInstance2D(fileName, false, 1, true)];
	if (fadeDuration == 0.0f)
	{
		if (mBackgroundMusic[0] != NULL)
			mBackgroundMusic[0]->Stop();

		mBackgroundMusic[0] = bgm;
		return;
	}
	else
	{
		mBackgroundMusic[1] = bgm;
		mBackgroundMusic[1]->SetVolume(0.0f);
	}

	mElapsedFadeTime = 0.0f;
	mBGMFadeDuration = fadeDuration;
	mFadeBGM = true;
}

void SoundManager::Apply3D(Vector3 pos, Vector3 forward, Vector3 up, Vector3 velocity)
{
	static FMOD_VECTOR p, fwd, u;
	p.x = pos.x;
	p.y = pos.y;
	p.z = pos.z;

	fwd.x = -forward.x;
	fwd.y = -forward.y;
	fwd.z = -forward.z;

	u.x = up.x;
	u.y = up.y;
	u.z = up.z;
	
	mSoundSystem->set3DListenerAttributes(0, &p, NULL, &fwd, &u);
}

void SoundManager::Update(float dt)
{
	if (dt == 0)
		return;

	ProcessMessages();

	// Clean up dead sounds TODO: DEALLOCATE DEAD ONES PROPERLY!
	for(auto iter = mPlayingSounds.begin(); iter != mPlayingSounds.end();)
	{
		if((*iter)->GetState() == SoundState::STOPPED)
		{
			delete (*iter);
			iter = mPlayingSounds.erase(iter);
		}
		else
			++iter;
	}

	mSoundSystem->update();
	
	for(auto iter = mSoundPool.begin(); iter != mSoundPool.end(); iter++)
	{
		iter->second->Update();
	}

	if (!mFadeBGM)
		return;

	// Crossfade in/out any BGM music
	mElapsedFadeTime = min(mElapsedFadeTime + dt, mBGMFadeDuration);
	
	auto fade = MathUtil::Lerp(mElapsedFadeTime / mBGMFadeDuration);

	static float chanVol;
	GetChannelVolume(1, &chanVol);

    // Do the fade.

	if (mBackgroundMusic[0] != NULL)
		mBackgroundMusic[0]->SetVolume(1.0f - (chanVol * fade));
    
	mBackgroundMusic[1]->SetVolume(chanVol * fade);

	if (fade >= 1.0f)
	{
		if (mBackgroundMusic[0] != NULL)
			mBackgroundMusic[0]->Stop();

		mBackgroundMusic[0] = mBackgroundMusic[1];
		mBackgroundMusic[1] = NULL;
		mFadeBGM = false;
	}
}

unsigned int SoundManager::CreateInstance3D(string soundToPlay, Vector3 position, bool paused, int channelGroup, bool loop)
{
	FMOD::Channel *newChannel;
	auto sound = loadSound(soundToPlay, true, loop);
	mSoundSystem->playSound(	FMOD_CHANNEL_FREE,  
										sound,
										true,
										&newChannel);

	newChannel->setChannelGroup(mChannelGroups[channelGroup]);

	auto newInst = new SoundInstance();
	newInst->recycle(newChannel, sound, false, false);
	newInst->Apply3D(position, Vector3(1.0f, 1.0f, 1.0f));
	newChannel->setChannelGroup(mChannelGroups[channelGroup]);

	auto newID = getNextID();
	mSoundPool.insert(make_pair(newID, newInst));

	newInst->Play();

	return newID;
}

unsigned int SoundManager::CreateInstance2D(string soundToCreate, bool paused, int channelGroup, bool isLooping)
{
	FMOD::Channel *newChannel;
	auto sound = loadSound(soundToCreate, false, isLooping);
	 mSoundSystem->playSound(	FMOD_CHANNEL_FREE,  
										sound,
										paused,
										&newChannel);

	auto inst = new SoundInstance();
	inst->recycle(newChannel, sound, true, false);
	newChannel->setChannelGroup(mChannelGroups[channelGroup]);

	auto newID = getNextID();
	mSoundPool.insert(make_pair(newID, inst));

	return newID;
}
	
void SoundManager::PlayOnce3D(string soundToPlay, float posx, float posy, float posz, int channelGroup, bool loop)
{
	auto newInst = new SoundInstance();
	auto sound = loadSound(soundToPlay, true, loop, false);

	FMOD::Channel *newChannel;
	mSoundSystem->playSound(FMOD_CHANNEL_FREE,  
									sound, 
									true,
									&newChannel);	

	newInst->recycle(newChannel, sound, true);
	newChannel->setChannelGroup(mChannelGroups[channelGroup]);
	
	static FMOD_VECTOR pos;
	pos.x = posx;
	pos.y = posy;
	pos.z = posz;
	newChannel->set3DAttributes(&pos, NULL);
	newChannel->set3DMinMaxDistance(10, 50);

	mPlayingSounds.push_back(newInst);
	newInst->Play();
}

void SoundManager::PlayOnce2D(string soundToPlay, int channelGroup, bool loop)
{
	auto newInst = new SoundInstance();
	auto sound = loadSound(soundToPlay, false, loop, false);

	FMOD::Channel *newChannel;
	mSoundSystem->playSound(FMOD_CHANNEL_FREE,  
									sound, 
									true,
									&newChannel);

	newInst->recycle(newChannel, sound, true);
	newChannel->setChannelGroup(mChannelGroups[channelGroup]);
	newInst->Play();

	mPlayingSounds.push_back(newInst);
}

void SoundManager::PlaySFX(unsigned int id, bool loop, int channelIdx)
{
	auto iter = mSoundPool.find(id);
	if (iter == mSoundPool.end())
	{
		// couldnt find sound.
		assert(false);
		return;
	}

	auto instance = iter->second;
	if (instance->GetState() == SoundState::PLAYING)
		return;

	instance->Stop();

	FMOD::Channel *newChannel;
	mSoundSystem->playSound( FMOD_CHANNEL_FREE,  
							 instance->GetSound(), 
							 true,
							 &newChannel);
	instance->SetChannel(newChannel);
	newChannel->setChannelGroup(mChannelGroups[channelIdx]);
	instance->Play();
}

void SoundManager::PauseSFX(unsigned int id)
{
	auto iter = mSoundPool.find(id);
	if (iter == mSoundPool.end())
	{
		// couldnt find sound.
		assert(false);
		return;
	}

	auto instance = iter->second;
	if (instance->GetState() == SoundState::PAUSED)
		return;

	instance->Pause();
}

void SoundManager::StopSFX(unsigned int id)
{
	auto iter = mSoundPool.find(id);
	if (iter == mSoundPool.end())
	{
		// couldnt find sound.
		assert(false);
		return;
	}

	auto instance = iter->second;
	if (instance->GetState() == SoundState::STOPPED)
		return;

	instance->Stop();
}

void SoundManager::PauseAll(int channelGroup)
{
	if (channelGroup != -1)
	{
		mChannelGroups[channelGroup]->setPaused(true);
		return;
	}

	for (auto it = mChannelGroups.begin(); it != mChannelGroups.end(); it++)
		(*it)->setPaused(true);
}

void SoundManager::ResumeAll(int channelGroup)
{
	if (channelGroup != -1)
	{
		mChannelGroups[channelGroup]->setPaused(false);
		return;
	}

	for (auto it = mChannelGroups.begin(); it != mChannelGroups.end(); it++)
		(*it)->setPaused(false);
}

void SoundManager::StopAll(int channelGroup)
{
	if (channelGroup != -1)
	{
		mChannelGroups[channelGroup]->stop();
		return;
	}

	for (auto it = mChannelGroups.begin(); it != mChannelGroups.end(); it++)
		(*it)->stop();
}

void SoundManager::DestroySound(unsigned int id)
{
	auto iter = mSoundPool.find(id);
	if (iter == mSoundPool.end())
	{
		// couldnt find sound.
		assert(false);
		return;
	}

	iter->second->Stop();

	delete iter->second;

	mSoundPool.erase(iter);
}


void SoundManager::ProcessMessages()
{
	Mail* message = NULL;
	// process all messages in queue
	while (m_sMyMessages.size() > 0)
	{
		// get message
		m_sMyMessages.GetNextMessage( &message );

		switch(message->m_iMsg)
		{
		case msg_GetMasterVolume:
			{
				// Cast the message paramaters
				MessageData msgData = MessageData();
				msgData.m_ifloatData[0] = GetMasterVolume();
				m_pMessageMgr->DispatchMsg(CT_SOUND, message->m_iSender, msg_GetMasterVolume, &msgData);
				break;
			}
			case msg_SetMasterVolume:
			{
				// 0 holds the incoming volume
				SetMasterVolume(message->m_mMessageData->m_ifloatData[0]);
				break;
			}
			case msg_StopAll:
			{
				// 0 holds the channel group. -1 for all channels.
				StopAll(message->m_mMessageData->m_iIntData[0]);
				break;
			}
			case msg_ResumeAll:
			{
				// 0 holds the channel group. -1 for all channels.
				ResumeAll(message->m_mMessageData->m_iIntData[0]);
				break;
			}
			case msg_PauseAll:
			{
				// 0 holds the channel group. -1 for all channels.
				PauseAll(message->m_mMessageData->m_iIntData[0]);
				break;
			}
			case msg_PlayOnce2D:
			{
				MessageData::SoundPlayOnceData* mData = (MessageData::SoundPlayOnceData*)message->m_mMessageData;
				PlayOnce2D(mData->m_cFileName, mData->m_iChannelGroup, mData->m_bLoop);
				break;
			}
			case msg_PlayOnce3D:
			{
				MessageData::SoundPlayOnceData* mData = (MessageData::SoundPlayOnceData*)message->m_mMessageData;
				PlayOnce3D(mData->m_cFileName, mData->m_cPos[0], mData->m_cPos[1], mData->m_cPos[2], mData->m_iChannelGroup, mData->m_bLoop);
				break;
			}
			case msg_PlayBGM:
			{
				MessageData::SoundPlayBGMData* mData = (MessageData::SoundPlayBGMData*)message->m_mMessageData;
				PlayBGM(mData->m_cFileName, mData->m_fduration);
				break;
			}
			case msg_SetChannelVolume:
			{
				auto data = message->m_mMessageData;
				SetChannelVolume(0, 1.0f);
				break;
			}
		default:
			throw; // Message not recognized
		};

		delete message;
	}
}

HRESULT CreateSoundObject(HINSTANCE hDLL, ISoundCore **pInterface)
{
	std::cout << "          Entering MathDLL::CreateMathObject" << std::endl;
	if(!*pInterface)
	{
		std::cout << "               Creating a new MathDLL Object" << std::endl;
		*pInterface = new SoundManager();
		std::cout << "          Leaving MathDLL::CreateMathObject" << std::endl;
		return 1;
	}

	std::cout << "               Failed to Create new MathDLL Object" << std::endl;
	std::cout << "          Leaving MathDLL::CreateMathObject" << std::endl;
	return 0;
}

HRESULT ReleaseSoundObject(ISoundCore **pInterface)
{
	std::cout << "          Entering MathDLL::ReleaseMathObject" << std::endl;
	if(!*pInterface)
	{
		std::cout << "               Failed to Release, Object doesnt exist" << std::endl;
		std::cout << "          Leaving MathDLL::ReleaseMathObject" << std::endl;
		return 0;
	}

	std::cout << "               Deleting Resource in MathDLL" << std::endl;
	delete *pInterface;
	*pInterface = NULL;
	std::cout << "          Leaving MathDLL::ReleaseMathObject" << std::endl;
	return 1;
}