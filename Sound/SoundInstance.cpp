#include "SoundInstance.h"

void SoundInstance::recycle(FMOD::Channel* channel, FMOD::Sound* sound, bool isFireOnce, bool is2dSound)
{
	mInstance = channel;
	mIs2D = is2dSound;
	mIsFireOnce = isFireOnce;
	mSound = sound;
	mSoundState = PAUSED;
}

void SoundInstance::Play()
{
	if (mSoundState == PLAYING)
		return;

	mSoundState = PLAYING;

	mInstance->setPaused(false);
}

void SoundInstance::Pause()
{
	mSoundState = PAUSED;

	mInstance->setPaused(true);
}

void SoundInstance::Update()
{
	if ( mSoundState != PLAYING )
		return;

	bool isPlaying;
	FMOD_RESULT res = mInstance->isPlaying(&isPlaying);
	if (res == FMOD_OK && isPlaying)
		mSoundState = PLAYING;
	else // If the channel has been stopped, we get an error here. Set ourselves as stopped
		mSoundState = STOPPED;
}

void SoundInstance::Apply3D(Vector3 position, Vector3 velocity)
{
	if (mIs2D)
		return;

	FMOD_VECTOR pos, vel;

	pos.x = position.x;
	pos.y = position.y;
	pos.z = position.z;

	// TODO: fix this eeeeew!
	if (velocity != NULL)
	{
		vel.x = velocity.x;
		vel.y = velocity.y;
		vel.z = velocity.z;
	}
	else
	{
		mInstance->set3DAttributes(&pos, NULL);
		return;
	}

	mInstance->set3DAttributes(&pos, &vel);
}
