#pragma once

#include "SoundInstance.h"
#include <SoundDLLInterface.h>

#include <string>
#include <map>
#include <vector>
using namespace std;

class SoundManager : public ISoundCore
{
private:

	float mMasterVolume;
	float mElapsedFadeTime;
	float mBGMFadeDuration;
	bool mFadeBGM;
	unsigned int mIDCounter;

	inline unsigned int getNextID() { return ++mIDCounter; }

	SoundInstance* mBackgroundMusic[2];

	static int MAX_SOUNDS;
	static int MAX_CHANNELS;

	FMOD::System* mSoundSystem;

	map<string, FMOD::Sound*> mLoadedSounds;

	// For sounds like UI clicks that are fired once and forgotton.
	vector<SoundInstance*> mPlayingSounds;

	map<unsigned int, SoundInstance*> mSoundPool;

	vector<FMOD::ChannelGroup*> mChannelGroups;

	FMOD::Sound* loadSound(string fileName, bool is3D = false, bool isLooping = false, bool isStreaming = false);

public:
	SoundManager() :
		mBGMFadeDuration(4.0) { }

	void SetMasterVolume(float newVolume)
	{
		mMasterVolume = min(1.0f, max(0.0f, newVolume));
	}
	float GetMasterVolume() { return mMasterVolume; }

	void SetChannelVolume(int channelIndex, float newVolume)
	{
		mChannelGroups[channelIndex]->setVolume(newVolume * mMasterVolume);
	}
	void GetChannelVolume(int channelIndex, float* val)
	{
		// TODO: Technically this will return the volume that was scaled by
		// MasterVolume... let's undo the math here later when I've had some sleep.
		mChannelGroups[channelIndex]->getVolume(val);
	}

	void PreLoadSound(string fileName, bool is3D = false, bool isLooping = false, bool isStreaming = false) { loadSound(fileName, is3D, isLooping, isStreaming);}

	SoundState GetState(int id);

	void Update(float dt);
	void Apply3D(Vector3 pos, Vector3 forward, Vector3 up, Vector3 velocity);

	int GetCorePriority() { return 5; }

	void PauseAll(int channelGroup = -1);
	void ResumeAll(int channelGroup = -1);
	void StopAll(int channelGroup = -1);

	void PlaySFX(unsigned int instance, bool loop = false, int channelIdx = 0);
	void PauseSFX(unsigned int instance);
	void StopSFX(unsigned int instance);

	void PlayBGM(string fileName, float fadeDuration = 0.0f);
	
	void PlayOnce2D(string soundToPlay, int channelGroup = 0, bool loop = false);
	void PlayOnce3D(string soundToPlay, float posx, float posy, float posZ, int channelGroup = 0, bool loop = false);

	unsigned int CreateInstance3D(string soundToPlay, Vector3 position, bool paused = true, int channelGroup = 0, bool loop = false);
	unsigned int CreateInstance2D(string soundToCreate, bool paused = true, int channelGroup = 0, bool loop = false);

	void DestroySound(unsigned int id);

	void Initialize();
	void Shutdown() { }
	void Pause() { }

	void ProcessMessages();
};

extern "C"
{

	HRESULT CreateSoundObject(HINSTANCE hDLL, ISoundCore **pInterface);
	HRESULT ReleaseSoundObject(ISoundCore **pInterface);	
}

typedef HRESULT (*CREATESOUNDOBJECT)(HINSTANCE hDLL, ISoundCore **pInterface);
typedef HRESULT(*RELEASESOUNDOBJECT)(ISoundCore **pInterface);
