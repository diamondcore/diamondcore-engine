#pragma once

#include <string>
#include <fmod.hpp>
#include <DCMathLib.h>
#include <SoundDLLInterface.h>


class SoundInstance
{
private:
	SoundState mSoundState;
	
	bool mIs2D;
	bool mIsFireOnce;

public:
	FMOD::Channel* mInstance;
	FMOD::Sound* mSound;
	SoundInstance() {}

	void Play();
	void Pause();
	void Apply3D(Vector3 position, Vector3 velocity);
	inline void Stop() { mInstance->stop(); }
	inline SoundState GetState() { return mSoundState; }
	inline FMOD::Sound* GetSound() { return mSound; }
	inline void SetChannel(FMOD::Channel* channel) { mInstance = channel; }

	inline void SetVolume(float newVolume) { mInstance->setVolume(newVolume); }
	void ClearChannel() { mInstance = NULL; };
	void Update();

	// TODO: Friend class this.
	void recycle(FMOD::Channel* channel, FMOD::Sound* sound, bool is2dSound, bool isFireOnce = true);

};