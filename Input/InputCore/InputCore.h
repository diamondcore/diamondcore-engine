#pragma once

#include <d3d9.h>
#include <string>
#include <iostream>
#include <map>

#include "DirectInput.h"
#include "ControllerInput.h"
#include <InputDLLInterface.h>

struct Keys
{
	float delay;
	char m_Key;
	std::string m_Label;
	void (*CallBack)();
	float timeStamp;
	enum KeyState {up, down, release, press} m_state;
	Keys()
	{
		delay = 1.0f;// time between dbounce may be changed
	}
	~Keys(){};
	void Update(float dt);
};

class ControllerInput;
class InputCore : public IInputCore
{
public:

	InputCore();
	~InputCore();

	HWND* m_pMainWnd;	// Pointer to the main window

	bool mouseButtonL, mouseButtonR, m_bMouseButtons[2];

	bool	wasKeyPressed(char key) { return false; }	// Determine if key wass already pressed no bounce
	bool	isKeyUp(char key) { return false; }			// Key is up
	bool	wasMousePressed(int key) { return false; }	


	// current keyboard buffer
	bool m_bKeyState[256];
	
	bool bPause; //Pause or Play song
	DirectInput *DI;

	void InputInit(HWND window);

	ControllerInput* mController;

	void Initialize() {}
	void Initialize(HWND hwnd, HINSTANCE hinst);
	virtual void Update(float dt);
	virtual void Shutdown();
	virtual void Pause(){};
	virtual void ProcessMessages();

	HWND FindMyTopMostWindow();

	// Keyboard
	bool	IsKeyDown(char key) { return gDInput->IsKeyDown(key); }			// Key is down
	bool	IsNewKeyRelease(char key) { return gDInput->IsNewKeyRelease(key); }	// Key has been released
	std::vector<int>* GetReleasedKeys(){ return gDInput->GetReleasedKeys(); }
	char* GetKeyboardState() { return gDInput->GetKeyboardState(); }

	// Mouse
	bool	IsMouseButtonDown(int button) { return gDInput->IsMouseButtonDown(button); }
	bool	IsNewMouseClick(int button) { return gDInput->IsNewMouseClick(button); }

	bool	IsMouseInBounds(RECT bounds);
	Vector2	GetMousePos() { return gDInput->GetMousePos(); }
	float	GetMouseWheel() { return gDInput->GetMouseWheel(); }

	// change in x,y,z axis for mouse
	float GetMouseDX() { return gDInput->mouseDX();}
	float GetMouseDY() { return gDInput->mouseDY();}
	float GetMouseDZ() { return gDInput->mouseDZ();}

	// Controller
	bool IsNewButtonRelease(unsigned int btn) { return mController->IsNewButtonRelease(btn); }
	bool IsButtonDown(unsigned int btn) { return mController->IsButtonDown(btn); }
	Vector2 GetTriggers() { return mController->GetTriggers(); }
	Vector2 GetTriggerDelta() { return mController->GetTriggerDelta(); }
	Vector2 GetLeftStick() { return mController->GetLeftStick(); }
	Vector2 GetRightStick() { return mController->GetRightStick(); }
	void VibrateController(float duration, Vector2 strength = Vector2()) { if( mController->GetControllerIsConnected() ) mController->Vibrate(duration, strength); }
	bool GetControllerIsConnected(){return mController->GetControllerIsConnected();}

private:

	/////////////////////
	// SCRATCH PAD VARS
	/////////////////////
	int i,j,k;
	unsigned char key_status[256];
	
};

extern "C"
{

	HRESULT CreateInputObject(HINSTANCE hDLL, IInputCore **pInterface);
	HRESULT ReleaseInputObject(IInputCore **pInterface);	
}

typedef HRESULT (*CREATEINPUTOBJECT)(HINSTANCE hDLL, IInputCore **pInterface);
typedef HRESULT(*RELEASEINPUTOBJECT)(IInputCore **pInterface);


