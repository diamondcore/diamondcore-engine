#pragma once

// No MFC
#define WIN32_LEAN_AND_MEAN

// We need the Windows Header and the XInput Header
#include <windows.h>
#include <XInput.h>    // XInput API

struct Vector2;
// Now, the XInput Library
// NOTE: COMMENT THIS OUT IF YOU ARE NOT USING
// A COMPILER THAT SUPPORTS THIS METHOD OF LINKING LIBRARIES
#pragma comment(lib, "XInput.lib")

class ControllerInput
{
private:
	XINPUT_STATE	mCurrentGamepadState;
	XINPUT_STATE	mLastGamepadState;

public:

    ControllerInput();
	~ControllerInput();

    bool mIsConnected;
	float mVibrationTimer;
	float mVibrationDuration;
    void Vibrate(float duration, Vector2 strength);	
	void Update(float dt);
	bool IsNewButtonRelease(unsigned int btn);
	bool IsButtonDown(unsigned int btn);

	Vector2 GetTriggers();
	Vector2 GetTriggerDelta();

	Vector2 GetLeftStick();
	Vector2 GetRightStick();

	bool GetControllerIsConnected(){return mIsConnected;}
};
