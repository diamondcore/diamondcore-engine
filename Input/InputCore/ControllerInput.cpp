#include "ControllerInput.h"
#include "InputCore.h"

ControllerInput::ControllerInput()
{
	ZeroMemory( &mCurrentGamepadState, sizeof(XINPUT_STATE) );
	ZeroMemory( &mLastGamepadState, sizeof(XINPUT_STATE) );
}

ControllerInput::~ControllerInput()
{
	// We don't want the controller to be vibrating accidentally when we exit the app
	if (!mIsConnected || !mVibrationDuration)
		return;

	Vibrate(0, Vector2());
}

void ControllerInput::Vibrate(float duration, Vector2 strength)
{
	// Vibration should be a value between 0-1 to represent
	// min-max strength.
	assert(strength.x <= 1 && strength.x >= 0);
	assert(strength.y <= 1 && strength.y >= 0);
	assert(duration >= 0);

    // Create a Vibraton State
    XINPUT_VIBRATION vState;

	// Scale the strength to where we really need to be.
	strength *= 65535;

    // Set the Vibration Values
	vState.wLeftMotorSpeed = strength.x;
	vState.wRightMotorSpeed = strength.y;

    XInputSetState(0, &vState);

	mVibrationTimer = 0.0f;
	mVibrationDuration = duration;
}

void ControllerInput::Update(float dt)
{
	memcpy(&mLastGamepadState, &mCurrentGamepadState, sizeof(XINPUT_STATE));

	ZeroMemory( &mCurrentGamepadState, sizeof(XINPUT_STATE) );
	mIsConnected = XInputGetState( 0, &mCurrentGamepadState ) == ERROR_SUCCESS;

	if (!mIsConnected || !mVibrationDuration)
		return;

	mVibrationTimer += dt;

	if (mVibrationTimer < mVibrationDuration)
		return;

	Vibrate(0, Vector2());
}

bool ControllerInput::IsNewButtonRelease(unsigned int btn)
{
	if (!(mLastGamepadState.Gamepad.wButtons & btn))
		return false;

	return !(mCurrentGamepadState.Gamepad.wButtons & btn);
}

bool ControllerInput::IsButtonDown(unsigned int btn)
{
	return mCurrentGamepadState.Gamepad.wButtons & btn;
}

Vector2 ControllerInput::GetTriggers()
{
	return Vector2( mCurrentGamepadState.Gamepad.bLeftTrigger / 255.0f,
					mCurrentGamepadState.Gamepad.bRightTrigger / 255.0f );
}

Vector2 ControllerInput::GetTriggerDelta()
{
	auto lDelta = mCurrentGamepadState.Gamepad.bLeftTrigger - mLastGamepadState.Gamepad.bLeftTrigger;
	auto rDelta = mCurrentGamepadState.Gamepad.bRightTrigger - mLastGamepadState.Gamepad.bRightTrigger;

	return Vector2( lDelta / 255.0f,
					rDelta / 255.0f );
}

Vector2 ControllerInput::GetLeftStick()
{
	float xVal = mCurrentGamepadState.Gamepad.sThumbLX;
	float yVal = mCurrentGamepadState.Gamepad.sThumbLY;
	
	if (abs(xVal) < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
		xVal = 0;

	if (abs(yVal) < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
		yVal = 0;

	float xNormalize = 32767;//xVal >= 0 ? 32767 : -32768;
	float yNormalize = 32767;//yVal >= 0 ? 32767 : -32768;
	
	return Vector2(xVal / xNormalize, yVal / yNormalize);
}

Vector2 ControllerInput::GetRightStick()
{
	float xVal = mCurrentGamepadState.Gamepad.sThumbRX;
	float yVal = mCurrentGamepadState.Gamepad.sThumbRY;

	if (abs(xVal) < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
		xVal = 0;

	if (abs(yVal) < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
		yVal = 0;

	float xNormalize = 32767;//xVal >= 0 ? 32767 : -32768;
	float yNormalize = 32767;//yVal >= 0 ? 32767 : -32768;
	
	return Vector2(xVal / xNormalize, yVal / yNormalize);
}