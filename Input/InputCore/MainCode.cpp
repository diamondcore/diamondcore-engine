#include "d3dApp.h"
#include <crtdbg.h>
#include <tchar.h>
//#include "DirectInput.h"
//#include "XBOX.h"
#include "MacroTools.h"
#include <time.h>
#include "InputCore.h"
#include <vector>
#include <iostream>
#include <XInput.h>
#include <dinput.h>

//XBOX *player[4];
ID3DXFont* mFont;

//std::vector<XBOX*> vPlayers;
XBOX *aPlayers[4];
XBOX *m_XBOXController;

InputCore *m_ICore;

class MainCode : public D3DApp
{
public:
	MainCode(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP);
	~MainCode();
	InputCore *m_ICore;
	bool checkDeviceCaps();
	void onLostDevice();
	void onResetDevice();
	void updateScene(float dt);
	void drawScene();
};

// Console Window
int main()
{
 WinMain(GetModuleHandle(NULL), NULL, "", 0);
}
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{
	// Enable Memory Leak Detection (in Debug mode)
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
		
		//Uncomment to break on the Xth memory allocation.
		//_CrtSetBreakAlloc(298543);
	#endif

	// Use this msg structure to catch window messages
	MSG msg; 
	ZeroMemory(&msg, sizeof(msg));
	bool result;
//	InputCore app(hInstance, "Input Core", D3DDEVTYPE_HAL, D3DCREATE_HARDWARE_VERTEXPROCESSING);
	
	MainCode app(hInstance, "Input Core", D3DDEVTYPE_HAL, D3DCREATE_HARDWARE_VERTEXPROCESSING);
	gd3dApp = &app;

	
	//***********************************************************************
	// Initialize Direct Input
	//HWND window = gd3dApp->getMainWnd();
	//DirectInput di(window, hInstance, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND, 
	//			DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	//gDInput = &di;
	//CHECK(gDInput)
	//	
	//************************Initialize XInput******************************
	//for (int i=0; i<4 ; i++)
	//{
	//	gXInput[i] = new XBOX(i);
	//}
	//***********************************************************************
	// Initialize gDInputrect Input
	HWND window = gd3dApp->getMainWnd();
	m_ICore->InputInit(window, hInstance );

	return gd3dApp->run();
}

// THIS IS MY BASIC MAIN FOR TESTING THE INPUT CORE
MainCode::MainCode(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP)
: D3DApp(hInstance, winCaption, devType, requestedVP)
{
	if(!checkDeviceCaps())
	{
		MessageBox(0, "checkDeviceCaps() Failed", 0, 0);
		PostQuitMessage(0);
	}	
//	m_ICore->InputInit(hInstance);

	// Create text to the screen
	D3DXFONT_DESC fontDesc;
	fontDesc.Height          = 18;
    fontDesc.Width           = 0;
    fontDesc.Weight          = 0;
    fontDesc.MipLevels       = 1;
    fontDesc.Italic          = false;
    fontDesc.CharSet         = DEFAULT_CHARSET;
    fontDesc.OutputPrecision = OUT_DEFAULT_PRECIS;
    fontDesc.Quality         = DEFAULT_QUALITY;
    fontDesc.PitchAndFamily  = DEFAULT_PITCH | FF_DONTCARE;
    _tcscpy_s(fontDesc.FaceName, _T("Times New Roman"));
	
	HR(D3DXCreateFontIndirect(gd3dDevice, &fontDesc, &mFont));	
	
	onResetDevice();
}

MainCode::~MainCode()
{
	m_ICore->Shutdown();
}

bool MainCode::checkDeviceCaps()
{
	// Nothing to check.
	return true;
}

void MainCode::onLostDevice()
{

}

void MainCode::onResetDevice()
{
	// Call the onResetDevice of other objects.

}

void MainCode::updateScene(float dt)
{
	// Controller Variables

	// Triggers
	float trigL = 0.0f;
	float trigR = 0.0f;
	// Thumb sticks
	float stickLX = 0.0f;
	float stickLY = 0.0f;
	float stickRX = 0.0f;
	float stickRY = 0.0f;

	//Buttons
	bool button_A;
	bool button_B;
	bool button_X;
	bool button_Y;
	bool button_DpadUp;
	bool button_DpadDown;
	bool button_DpadLeft;
	bool button_DpadRight;
	bool button_Lstick;
	bool button_Rstick;
	bool button_Lshoulder;
	bool button_Rshoulder;
	bool button_Start;
	bool button_Back;

//	m_ICore->GetInput();

	/**
	bLeftTrigger: (0 to 255)
	bRightTrigger: (0 to 255)
	sThumbLX: (-32768 to 32767) //value 0 is the center
	sThumbLY: (-32768 to 32767) //positive value is UP or RIGHT
	sThumbRX: (-32768 to 32767) //negative value is DOWN or LEFT
	sThumbRY: (-32768 to 32767)
	Filter a thumbstick input with:  
		XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE or 
		XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE
**/

		for(int i=0; i<4 ; i++)
	{

		m_ICore->GetController(i)->GetState();
		m_ICore->GetController(i)->Update(i);

		//TRIGGERS  (returns value 0 - 255)
		trigL = m_ICore->GetLTrigger(i);
		if (trigL > 0)
			m_ICore->GetController(i)->Vibrate(trigL*400, 0);
		
		trigR = m_ICore->GetRTrigger(i);
		if (trigR > 0)
			m_ICore->GetController(i)->Vibrate(0, trigR*400);
		
		// Thumb Sticks (returns value -32768 to 32767)
		stickLX = m_ICore->GetLThumbX(i);
		stickLY = m_ICore->GetLThumbY(i);
		stickRX = m_ICore->GetRThumbX(i);
		stickRY = m_ICore->GetRThumbY(i);
		
		if (stickLX > 0)
			m_ICore->GetController(i)->Vibrate(stickLX*100, 0);
		if (stickLY > 0)
			m_ICore->GetController(i)->Vibrate(stickLY*100, 0);
		if (stickRX > 0)
			m_ICore->GetController(i)->Vibrate(0, stickRX*100);
		if (stickRY > 0)
			m_ICore->GetController(i)->Vibrate(0, stickRY*100);
		
		// BUTTONS
		button_A = m_ICore->GetA(i);
		button_B = m_ICore->GetB(i);
		button_X = m_ICore->GetX(i);
		button_Y = m_ICore->GetY(i);
		if (button_A)
			m_ICore->GetController(i)->Vibrate(65535, 65535);
		if (button_B)
			m_ICore->GetController(i)->Vibrate(65535, 65535);
		if (button_X)
			m_ICore->GetController(i)->Vibrate(0, 0);
		if (button_Y)
			m_ICore->GetController(i)->Vibrate(65535, 65535);

		// remander buttons to be implemented the same
		button_DpadUp = m_ICore->GetDpadUp(i);
		button_DpadDown = m_ICore->GetDpadDown(i);
		button_DpadLeft = m_ICore->GetDpadLeft(i);
		button_DpadRight = m_ICore->GetDpadRight(i);
		button_Lstick = m_ICore->GetLStick(i);
		button_Rstick = m_ICore->GetRStick(i);
		button_Lshoulder = m_ICore->GetLShoulder(i);
		button_Rshoulder = m_ICore->GetRShoulder(i);
		button_Start = m_ICore->GetStart(i);
		button_Back = m_ICore->GetSelect(i);


	}

	float mPosX = m_ICore->getMousePosX();
	float mPosY = m_ICore->getMousePosY();
	
	if(m_ICore->wasMousePressed(0)!=0)
		std::cout<< "lft mouse mutton pressed @ (" << mPosX << ", " << mPosY <<")   ";
	if(m_ICore->wasMousePressed(1)!=0)
		std::cout<< "rt mouse mutton pressed @ (" << mPosX << ", " << mPosY <<")   ";
	if(m_ICore->wasKeyPressed(DIK_Z) )
		std::cout << "\nZ pressed   ";
	if(m_ICore->wasKeyPressed(DIK_ESCAPE))
		PostQuitMessage(0);
	
//Then you can loop through the array and check which keys were pressed by checking their high-order bit, like this:
	unsigned char key_status[256];
    GetKeyboardState(key_status);
	static int curKey = 0;
	bool isPressed = false;
    for (int i=0; i<256; ++i)
    {
        if (key_status[i] & 0x80)
        {
			if (curKey !=i && isPressed == false)
			{
				curKey = i;
				isPressed = true;
			}
			else
			{
				isPressed = false;
			}
			if (isPressed == true)
	            std::cout << "key " << i << " is pressed\n";
        }
    }

}

void MainCode::drawScene()
{	
	int m_X = 10;
	int m_Y = 30;
	// Clear the backbuffer and depth buffer.
	HR(gd3dDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xffffffff, 1.0f, 0));

	HR(gd3dDevice->BeginScene());

	// Draw text
	// Make static so memory is not allocated every frame.
	static char buffer[256];
//	GetInput();
	sprintf_s(buffer,"INPUT CORE");

	RECT R = {m_X+5.0f, m_Y+5.0f, m_X, m_Y};
	HR(mFont->DrawText(0, buffer, -1, &R, DT_NOCLIP, D3DCOLOR_XRGB(255,0,0)));
	
	HR(gd3dDevice->EndScene());
	HR(gd3dDevice->Present(0, 0, 0, 0));
}

