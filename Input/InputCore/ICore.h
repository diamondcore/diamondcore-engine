
#pragma once
#include <InputDLLInterface.h>
#include <Windows.h>
#include <iostream>
#include "BaseCore.h"

class ICore// : public IInputCore
{

 public:
  virtual void Seek() = 0;
  virtual void Flee() = 0;
  virtual void Arrive() = 0;
  virtual void Pathing() = 0;

  /*virtual void Update(float dt) = 0;
  virtual void Initialize() = 0;
  virtual void Shutdown() = 0;
  virtual void Pause() = 0;
  virtual void ProcessMessages() = 0;*/



};



#pragma once
#include <InputDLLInterface.h>
#include <Windows.h>
#include <iostream>

class InputCore : public IInputCore
{
	public:
		
		InputCore(void);
		~InputCore(void);
		
		void Update(float dt);
		void Shutdown();

		XBOX* GetController(int i);		
		void GetXBOXInput();	
		virtual bool GetA();
		virtual bool GetB();
		virtual bool GetX();
		virtual bool GetY();
		
		virtual bool GetDpadUp();
		virtual bool GetDpadDown();
		virtual bool GetDpadLeft();
		virtual bool GetDpadRight();
		
		virtual bool GetLShoulder();
		virtual bool GetRShoulder();
		virtual bool GetLStick();
		virtual bool GetRStick();
		
		virtual bool GetStart();
		virtual bool GetSelect();
		
		virtual float GetLTrigger();
		virtual float GetRTrigger();
		virtual float GetLThumbX();
		virtual float GetLThumbY();
		virtual float GetRThumbX();
		virtual float GetRThumbY();		

		// Keyboard
		bool Register(char key,std::string label, void(*CallBack)());
		bool Register(char key,std::string label, void(*CallBack)(),float delay);
		std::string GetRegister(char);	
		void RemoveRegister(char key);
		bool	wasKeyPressed(char key);	// Determine if key wass already pressed no bounce
		bool	isKeyUp(char key);			// Key is up
		bool	isKeyDown(char key);		// Key is down
		bool	isKeyReleased(char key);	// Key has been released
	
		bool keyIsPressed(char key);

		// Mouse
		bool	wasMousePressed(int button);
		bool	isMouseButtonDown(int button);
		bool	isMouseButtonReleased(int button);

		float	getMousePosX();				// Screen coordinates (Works better)
		float	getMousePosY();
		float	getMouseStateX();			// Change in direction
		float	getMouseStateY();
		float	getMouseStateWheel();

};

