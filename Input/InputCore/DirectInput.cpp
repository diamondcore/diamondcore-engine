#include "DirectInput.h"
#include "MacroTools.h"
DirectInput* gDInput = 0;

DirectInput::DirectInput(HWND mainWnd, HINSTANCE mainInstance, 
						DWORD keyboardFlag, DWORD mouseFlag)
{
	ZeroMemory(m_KeyRegistry, 256);
	ZeroMemory(m_KeyboardPrev, 256);
	ZeroMemory(m_KeyboardState, 256);	// Manually adding 256 as the size fixed an Acquire bug
	ZeroMemory(&m_MousePrev, sizeof(m_MousePrev));
	ZeroMemory(&m_MouseState, sizeof(m_MouseState));

	HRESULT hr = DirectInput8Create(mainInstance, DIRECTINPUT_VERSION,
						IID_IDirectInput8, (void**)&m_InputObject, 0);
	m_pMainWnd = mainWnd;	// Stored for polling reference

	// Initialize Keyboard
	hr = m_InputObject->CreateDevice(GUID_SysKeyboard, &m_Keyboard, 0);
	hr = m_Keyboard->SetDataFormat(&c_dfDIKeyboard);
	hr = m_Keyboard->SetCooperativeLevel(mainWnd, // DISCL_FOREGROUND | DISCL_NONEXCLUSIVE (Default)
									keyboardFlag);
	m_Keyboard->Acquire();

	// Initialize  Mouse
	hr = m_InputObject->CreateDevice(GUID_SysMouse, &m_Mouse, 0);
	hr = m_Mouse->SetDataFormat(&c_dfDIMouse2);
	hr = m_Mouse->SetCooperativeLevel(mainWnd, // DISCL_FOREGROUND | DISCL_NONEXCLUSIVE (Default)
								mouseFlag);
	m_Mouse->Acquire();
}

DirectInput::DirectInput(const DirectInput& rhs)
{
	*this = rhs;
}

DirectInput& DirectInput::operator=(const DirectInput& rhs)
{
	DirectInput((const DirectInput&)(rhs));
	return *this;
}

DirectInput::~DirectInput()
{
	SAFE_RELEASE(m_InputObject);
	m_Keyboard->Unacquire();
	m_Mouse->Unacquire();
	SAFE_RELEASE(m_Keyboard);
	SAFE_RELEASE(m_Mouse);
}

void DirectInput::poll()
{
	// Save previous key States and mouse state
	memcpy(&m_KeyboardPrev, &m_KeyboardState, 256);
	memcpy(&m_MousePrev, &m_MouseState, sizeof(m_MouseState));

	// Poll the Keyboard
	HRESULT hr = m_Keyboard->GetDeviceState(256, (void**)m_KeyboardState);

	m_ReleasedKeys.clear();
	for (auto x = 0; x < 256; x++)
	{
		if ((m_KeyboardState[x] == m_KeyboardPrev[x]))
			continue;

		// Adding 1 to X will retrieve its scan code.
		if ((m_KeyboardState[x] & 0x80) == 0)
			m_ReleasedKeys.push_back(x);
	}
	
	if(FAILED(hr))
	{
		// Keyboard disconnected, zero out keyboard
		ZeroMemory(m_KeyboardState, 256);

		// Prepare for next poll
		hr = m_Keyboard->Acquire();
	}
	// Poll the Mouse
	hr = m_Mouse->GetDeviceState(sizeof(DIMOUSESTATE2), (void**)&m_MouseState);
	if(FAILED(hr))
	{
		// Mouse disconnected, zero out mouse data
		ZeroMemory(&m_MouseState, sizeof(m_MouseState));
		
		// Prepare for next poll
		hr = m_Mouse->Acquire();
	}
	
	// Get position of mouse on the screen
	POINT mouse;
	GetCursorPos(&mouse);
	ScreenToClient(m_pMainWnd, &mouse);
	
	m_MousePos.x = mouse.x;
	m_MousePos.y = mouse.y;
}

bool DirectInput::IsNewKeyRelease(char key)
{
	if (m_KeyboardPrev[key] == m_KeyboardState[key])
		return false;

	return (m_KeyboardState[key] & 0x80) == 0;
}

bool DirectInput::IsKeyDown(char keys)
{
	return (m_KeyboardState[keys] & 0x80) != 0;	
}

bool DirectInput::IsNewMouseClick(int button)
{
	if((m_MousePrev.rgbButtons[button] & 0x80) != 0 &&	// Check if mouse button was down
		(m_MouseState.rgbButtons[button] & 0x80) == 0)	// Check if mouse button is not down
		return true;
	else
		return false;
}
bool DirectInput::IsMouseButtonDown(int button)
{
	return (m_MouseState.rgbButtons[button] & 0x80) != 0;
}

Vector2 DirectInput::GetMousePos()
{
	return Vector2(m_MousePos.x, m_MousePos.y);
}

float DirectInput::GetMouseWheel()
{
	return (float)m_MouseState.lZ;
}
int DirectInput::GetMouseButton( char button )
{
    return (m_MouseState.rgbButtons[button] & 0x80);
}

float DirectInput::mouseDX()
{
	return (float)m_MouseState.lX;
}

float DirectInput::mouseDY()
{
	return (float)m_MouseState.lY;
}

float DirectInput::mouseDZ()
{
	return (float)m_MouseState.lZ;
}