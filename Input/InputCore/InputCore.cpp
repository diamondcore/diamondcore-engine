#include "InputCore.h"
#include <conio.h>
#include <ctype.h>
#include "MyTools.h"
#include "ControllerInput.h"

//#define DBOUNCE 0.5f

InputCore::InputCore()
{
}

HWND InputCore::FindMyTopMostWindow()
{
    DWORD dwProcID = GetCurrentProcessId();
    HWND hWnd = GetTopWindow(GetDesktopWindow());
    while(hWnd)
    {
        DWORD dwWndProcID = 0;
        GetWindowThreadProcessId(hWnd, &dwWndProcID);
        if(dwWndProcID == dwProcID)
            return hWnd;            
        hWnd = GetNextWindow(hWnd, GW_HWNDNEXT);
    }
    return NULL;
 }

void InputCore::Initialize(HWND hwnd, HINSTANCE hinst)
{
	if(!checkDeviceCaps())
	{
		MessageBox(0, "checkDeviceCaps() Failed", 0, 0);
		PostQuitMessage(0);
	}	

	AttachThreadInput(GetCurrentThreadId(),GetWindowThreadProcessId(hwnd, NULL),TRUE);

	// getting instance handle
	HINSTANCE hInstance = GetModuleHandle(NULL);

	//HWND window = FindMyTopMostWindow();


	//************************ Initialize DirectInput *********************
	static DirectInput DI(hwnd, hinst, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND, 
				DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	gDInput = &DI;
		
	//************************ Initialize XInput ****************************
	mController = new ControllerInput();
	//***********************************************************************

	//CheckConnections();

}
InputCore::~InputCore()
{
	delete DI;
	delete mController;
}

void InputCore::Shutdown()
{
}

void InputCore::Update(float dt)
{
	if (!m_pSharedMemory->GetMemMgr()->GetIsActiveWindow())
		return;
	// Update and buffer mouse/keyboard
	gDInput->poll();

	mController->Update(dt);

	static int lastKey = 0;

	bool isPressed = false;

	for (i=0; i<256; ++i)
    {
        if (gDInput->GetKeyState(i) & 0x80)
        {
			/*if ( lastKey !=i || count > DBOUNCE )
			{
				
				lastKey = i;
				isPressed = true;*/
				m_bKeyState[i] = true;
				//count = 0.0f;
			//}
			//else
			//{
			//	isPressed = false;
			//	m_bKeyState[i] = false;
			//}
			//if (isPressed == true)
			//{
				//static char buffer[256];
				////char msg;// = "key %i is pressed\n";

				//sprintf_s(buffer, "key %d is pressed\n", i);			
	   //         OutputDebugString(buffer);
			//}
        }
		else
			m_bKeyState[i] = false;
    }
}
//void InputCore::onLostDevice()
//{
//	//if(mFont)
//	//HR(mFont->OnLostDevice());
//}
//
//void InputCore::onResetDevice()
//{
//	//if(mFont)
//	//HR(mFont->OnResetDevice());
//}
//bool InputCore::Register(char key,std::string label, void(*CallBack)())
//{
//	if(m_KeyboardMap.find(key) != m_KeyboardMap.end())
//		return false;	// Key already registered
//	Keys *temp = new Keys();
//	temp->m_Key=key;
//	temp->m_Label = label;
//	temp->CallBack=CallBack;
//	m_KeyboardMap.insert(std::pair<char,Keys*>(key,temp));
//	return true;
//}
//
//bool InputCore::Register(char key,std::string label, void(*CallBack)(),float delay)
//{
//	if(m_KeyboardMap.find(key) != m_KeyboardMap.end())
//		return false;	// Key already registered
//	Keys temp;
//	temp.m_Key=key;
//	temp.m_Label = label;
//	temp.CallBack=CallBack;
//	temp.delay=delay;
//	m_KeyboardMap.insert(std::pair<char,Keys>(key,temp));
//	return true;
//}
//
//std::string InputCore::GetRegister(char key)
//{
//	return m_KeyboardMap[key].m_Label;
//}
////RemoveRegister
//void InputCore::RemoveRegister(char key)
//{
//	auto iter = m_KeyboardMap.find(key);
//	if( iter == m_KeyboardMap.end())
//		return;	// Key not found in register
//	m_KeyboardMap.erase( iter );	// Remove key register
//}
//
//void InputCore::UpdateKeys(float dt)
//{
//	for (i = m_KeyboardMap.size()-1; i>=0; --i)
//	{
//		isKeyUp(m_KeyboardMap[i])
//	}
//}
//
//void Keys::Update(float dt)
//{ 
//	switch(m_state)
//	{
//	case up:
//		if (isKeyUp(m_Key))
//		{
//			CallBack();
//			break;
//		}
//	case down:
//		if (ICORE->isKeyDown(m_Key))
//		{
//			CallBack();
//			break;
//		}
//	case release:
//		{
//			if (timeStamp <= 0.0f && ICORE->isKeyReleased(m_Key))
//			{
//				CallBack();
//				timeStamp=delay;
//			}
//			else if( timeStamp > 0.0 )
//			{
//				timeStamp -= dt;
//			}
//			break;
//		}
//	case press:
//		{
//			if (timeStamp <= 0.0f && ICORE->wasKeyPressed(m_Key))
//			{
//				CallBack();
//				timeStamp=delay;
//			}
//			else if( timeStamp > 0.0 )
//			{
//				timeStamp -= dt;
//			}
//			break;
//		}
//	};
//}		
//
//
//	bool InputCore::GetA(int i) {return gXInput[i]->previous.A;}
//	bool InputCore::GetB(int i) {return gXInput[i]->previous.B;}
//	bool InputCore::GetX(int i) {return gXInput[i]->previous.X;}
//	bool InputCore::GetY(int i) {return gXInput[i]->previous.Y;}
//	
//	bool InputCore::GetDpadUp(int i) {return gXInput[i]->previous.DPAD_UP;}
//	bool InputCore::GetDpadDown(int i) {return gXInput[i]->previous.DPAD_DOWN;}
//	bool InputCore::GetDpadLeft(int i) {return gXInput[i]->previous.DPAD_LEFT;}
//	bool InputCore::GetDpadRight(int i) {return gXInput[i]->previous.DPAD_RIGHT;}
//	
//	bool InputCore::GetLShoulder(int i) {return gXInput[i]->previous.LShoulder;}
//	bool InputCore::GetRShoulder(int i) {return gXInput[i]->previous.RShoulder;}
//	bool InputCore::GetLStick(int i) {return gXInput[i]->previous.LStick;}
//	bool InputCore::GetRStick(int i) {return gXInput[i]->previous.RStick;}
//	
//	bool InputCore::GetStart(int i) {return gXInput[i]->previous.Start;}
//	bool InputCore::GetSelect(int i) {return gXInput[i]->previous.Select;}
//	
//	float InputCore::GetLTrigger(int i) {return gXInput[i]->current.LTrig;}
//	float InputCore::GetRTrigger(int i) {return gXInput[i]->current.RTrig;}
//	float InputCore::GetLThumbX(int i) {return gXInput[i]->current.LAnol.x;}
//	float InputCore::GetLThumbY(int i) {return gXInput[i]->current.LAnol.y;}
//	float InputCore::GetRThumbX(int i) {return gXInput[i]->current.RAnol.x;}
//	float InputCore::GetRThumbY(int i) {return gXInput[i]->current.RAnol.y;}		


bool InputCore::IsMouseInBounds(RECT bounds)
{
	auto pos = GetMousePos();

	if (pos.x > bounds.right ||
		pos.x < bounds.left ||
		pos.y < bounds.top ||
		pos.y > bounds.bottom)
		return false;

	return true;
}




void InputCore::ProcessMessages()
{
	
}

HRESULT CreateInputObject(HINSTANCE hDLL, IInputCore **pInterface)
{
	std::cout << "          Entering InputCore::CreateInputObject" << std::endl;
	if(!*pInterface)
	{
		std::cout << "               Creating a new InputCore Object" << std::endl;
		*pInterface = new InputCore();
		std::cout << "          Leaving InputCore::CreateInputObject" << std::endl;
		return 1;
	}

	std::cout << "               FInputled to Create new InputCore Object" << std::endl;
	std::cout << "          Leaving InputCore::CreateInputObject" << std::endl;
	return 0;
}

HRESULT ReleaseInputObject(IInputCore **pInterface)
{
	std::cout << "          Entering InputCore::ReleaseInputObject" << std::endl;
	if(!*pInterface)
	{
		std::cout << "               FInputled to Release, Object doesnt exist" << std::endl;
		std::cout << "          Leaving InputCore::ReleaseInputObject" << std::endl;
		return 0;
	}

	std::cout << "               Deleting Resource in InputCore" << std::endl;
	delete *pInterface;
	*pInterface = NULL;
	std::cout << "          Leaving InputCore::ReleaseInputObject" << std::endl;
	return 1;
}