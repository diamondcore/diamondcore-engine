#pragma once

#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include <DxErr.h>	// Debug tools
#include <DCMathLib.h>

#pragma comment(lib, "dxguid.lib")	// Linking a helper library
#pragma comment(lib, "dinput8.lib") // Linking a helper library
#pragma comment(lib, "DxErr.lib")	// Linking to debug	library

class DirectInput
{
private:
	HWND					m_pMainWnd;	// Pointer to the main window
	DirectInput(const DirectInput& rhs);
	DirectInput& operator=(const DirectInput& rhs);

	IDirectInput8*			m_InputObject;

	IDirectInputDevice8*	m_Keyboard;
	bool					m_KeyRegistry[256];		//  Key Registry array
	char					m_KeyboardState[256];	// Current Keyboard State
	char					m_KeyboardPrev[256];	// Previous Keyboard State

	IDirectInputDevice8*	m_Mouse;
	DIMOUSESTATE2			m_MouseState;
	DIMOUSESTATE2			m_MousePrev;

	POINT					m_MousePos;	// Mouse position on screen

	std::vector<int>		m_ReleasedKeys;

public:
	int GetMouseButton( char button );
    char	GetKeyState(int key) { return m_KeyboardState[key]; }
	DirectInput(HWND mainWnd, HINSTANCE mainInstance, 
				DWORD keyboardFlag, DWORD mouseFlag);
	DirectInput();
	~DirectInput();

	// Rescan and buffer
	void	poll();		

	// Keyboard
	bool	IsKeyDown(char key);		// Key is down
	bool	IsNewKeyRelease(char key);	// Key has been released
	char* GetKeyboardState() { return &m_KeyboardState[0]; }
	std::vector<int>*	GetReleasedKeys()		  { return &m_ReleasedKeys; }

	// Mouse
	bool	IsMouseButtonDown(int button);
	bool	IsNewMouseClick(int button); // If there's a new mouse RELEASE.

	Vector2 GetMousePos();
	float	GetMouseWheel();
	bool	wasMousePressed(int button);
	bool	isMouseButtonDown(int button);
	bool	isMouseButtonReleased(int button);

	float	getMousePosX();	// Screen coordinates (Works better)
	float	getMousePosY();
	float	getMouseStateX();		// Change in direction
	float	getMouseStateY();
	float	getMouseStateWheel();

	// change in x,y,z axis for mouse
	float mouseDX();
	float mouseDY();
	float mouseDZ();
};
extern DirectInput* gDInput;
