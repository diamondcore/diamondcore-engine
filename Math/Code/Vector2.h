#pragma once

#include <cmath>
#include <D3DX10Math.h>

typedef float real;

struct Vector2 : D3DXVECTOR2
{
	Vector2() : D3DXVECTOR2(0, 0) { };
	Vector2(real xVal, real yVal) : D3DXVECTOR2(xVal, yVal) {};
	Vector2(D3DXVECTOR2 dxVec2): D3DXVECTOR2(dxVec2) { };

	Vector2& operator=(const Vector2 &rhs);
	Vector2& operator=(const D3DXVECTOR2 &rhs);
	bool operator==(const Vector2 &rhs);
	bool operator< (const Vector2 &rhs);
	Vector2 operator-= (const Vector2 &rhs);
	Vector2 operator+= (const Vector2 &rhs); 
    Vector2 operator- (const Vector2 &rhs);  
	Vector2 operator+ (const Vector2 &rhs);
	Vector2 operator/= (const Vector2 &rhs);
	Vector2 operator/ (const Vector2 &rhs);
	Vector2 operator*= (real value);
	Vector2 operator* (real value);
	Vector2 operator/= (real value);
	Vector2 operator/ (real value);

	void Clear();
	void Negate();
	real Length();
	real LengthSquared();
	Vector2 Normalize();
	Vector2 ScaleRet(Vector2 rhs);

	// Static methods
	static Vector2 Zero() { return Vector2(0, 0); };
	static Vector2 Max(Vector2 v1, Vector2 v2) { return Vector2(max(v1.x, v2.x), max(v1.y, v2.y)); };
};