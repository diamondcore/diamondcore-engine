#include "Vector4.h"
#include "MathUtil.h"

#pragma region Operator Overloads

Vector4& Vector4::operator=(const Vector4 &rhs) 
{
    if (this != &rhs) 
	{
       this->x = rhs.x;
	   this->y = rhs.y;
	   this->z = rhs.z;
	   this->w = rhs.w;
    }

    return *this;
}

Vector4& Vector4::operator=(const D3DXVECTOR4 &rhs) 
{
    this->x = rhs.x;
	this->y = rhs.y;
	this->z = rhs.z;
	this->w = rhs.w;

    return *this;
}

bool Vector4::operator==(const Vector4 &rhs)
{
	return  this->x == rhs.x &&
			this->y == rhs.y &&
			this->z == rhs.z &&
			this->w == rhs.w;

}

Vector4 Vector4::operator+= (const Vector4 &rhs)
{
	D3DXVec4Add(this, this, &rhs);

	return *this; 
}

Vector4 Vector4::operator+ (const Vector4 &rhs)
{
	return Vector4(*this) += rhs;
}

Vector4 Vector4::operator-= ( const Vector4 &rhs )
{
	return (Vector4)*D3DXVec4Subtract(this, this, &rhs);
}

Vector4 Vector4::operator- ( const Vector4 &rhs )
{
	return Vector4(*this) -= rhs;
}

real Vector4::operator*= ( const Vector4 &rhs )
{
	return D3DXVec4Dot(this, &rhs);
}

real Vector4::operator* ( const Vector4 &rhs )
{
	return Vector4(*this) *= rhs;
}

// Scalar Operations

Vector4 Vector4::operator*= ( real value )
{
	D3DXVec4Scale(this, this, value);

	return *this; 
}

Vector4 Vector4::operator* ( real value )
{
	return Vector4(*this) *= value;
}

// TODO: Replace this with D3DXVec3Multiply(1 / value) ?
Vector4 Vector4::operator/= ( real value )
{
	this->x /= value;
	this->y /= value;
	this->z /= value;
	this->w /= value;

	return *this; 
}

Vector4 Vector4::operator/ ( real value )
{
	return Vector4(*this) /= value;
}

#pragma endregion

real Vector4::Length()
{
	return sqrt( x * x + y * y + z * z + w * w );
}

real Vector4::LengthSquared()
{
	return ( x * x ) + ( y * y ) + ( z * z ) + ( w * w );
}

Vector4 Vector4::Normalize()
{
	auto length = this->Length();
	this->x /= length;
	this->y /= length;
	this->z /= length;
	this->w /= length;
	return *this;
}

void Vector4::Clear()
{
	this->x = this->y = this->z = this->w = 0;
}

/// Multiplies each component by -1
void Vector4::Negate()
{
	this->x *= -1;
	this->y *= -1;
	this->z *= -1;
	this->w *= -1;
}


Vector4 Vector4::Lerp(float weight, Vector4 a, Vector4 b )
{
	Vector4 outputVec;

	outputVec.x = MathUtil::Lerp(weight, a.x, b.x);
	outputVec.y = MathUtil::Lerp(weight, a.y, b.y);
	outputVec.z = MathUtil::Lerp(weight, a.z, b.z);
	outputVec.w = MathUtil::Lerp(weight, a.w, b.w);

	return outputVec;
}

Vector4 Vector4::Min(Vector4 v1, Vector4 v2)
{
	return Vector4(min(v1.x, v2.x), min(v1.y, v2.y), min(v1.z, v2.z), min(v1.w, v2.w));
}

Vector4 Vector4::Max(Vector4 v1, Vector4 v2)
{
	return Vector4(max(v1.x, v2.x), max(v1.y, v2.y), max(v1.z, v2.z), max(v1.w, v2.w));
}