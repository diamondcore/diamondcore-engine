#include "Vector2.h"

#pragma region Operator Overloads

Vector2& Vector2::operator=(const Vector2 &rhs) 
{
    if (this != &rhs) 
	{
       this->x = rhs.x;
	   this->y = rhs.y;
    }

    return *this;
}

Vector2& Vector2::operator=(const D3DXVECTOR2 &rhs) 
{
    this->x = rhs.x;
	this->y = rhs.y;

    return *this;
}

bool Vector2::operator==(const Vector2 &rhs)
{
	return  this->x == rhs.x &&
			this->y == rhs.y;

}

bool Vector2::operator< (const Vector2 &rhs)
{
	Vector2 temp = rhs;
	return ( this->LengthSquared() < temp.LengthSquared() );
}

Vector2 Vector2::operator+= (const Vector2 &rhs)
{
	D3DXVec2Add(this, this, &rhs);

	return *this; 
}

Vector2 Vector2::operator+ (const Vector2 &rhs)
{
	return Vector2(*this) += rhs;
}

Vector2 Vector2::operator/= (const Vector2 &rhs)
{
	this->x = this->x / rhs.x;
	this->y = this->y / rhs.y;

	return *this; 
}

Vector2 Vector2::operator/ (const Vector2 &rhs)
{
	return Vector2(*this) /= rhs;
}

Vector2 Vector2::operator-= ( const Vector2 &rhs )
{
	return (Vector2)*D3DXVec2Subtract(this, this, &rhs);
}

Vector2 Vector2::operator- ( const Vector2 &rhs )
{
	return Vector2(*this) -= rhs;
}

// Scalar Operations

Vector2 Vector2::operator*= ( real value )
{
	D3DXVec2Scale(this, this, value);

	return *this; 
}

Vector2 Vector2::operator* ( real value )
{
	return Vector2(*this) *= value;
}

// TODO: Replace this with D3DXVec2Multiply(1 / value) ?
Vector2 Vector2::operator/= ( real value )
{
	this->x /= value;
	this->y /= value;

	return *this; 
}

Vector2 Vector2::operator/ ( real value )
{
	return Vector2(*this) /= value;
}

#pragma endregion

real Vector2::Length()
{
	return sqrt( x * x + y * y );
}

real Vector2::LengthSquared()
{
	return ( x * x ) + ( y * y );
}

Vector2 Vector2::Normalize()
{
	auto length = this->Length();

	if (length != 0)
	{
		this->x /= length;
		this->y /= length;
	} 

	return *this;
}

void Vector2::Clear()
{
	this->x = this->y = 0;
}

/// Multiplies each component by -1
void Vector2::Negate()
{
	this->x *= -1;
	this->y *= -1;
}

Vector2 Vector2::ScaleRet(Vector2 scale)
{
	Vector2 temp;
	temp.x = this->x * scale.x;
	temp.y = this->y * scale.y;
	return temp;
}