#include "Matrix3.h"
#include "vector3.h"
#include "Quaternion.h"

Matrix3::Matrix3()
{
	/*for(int i = 0; i<3; i++)
		for(int j = 0; j<3; j++)
			m_data[i][j] = 0;*/

	data[0] = data[1] = data[2] = data[3] = data[4] = data[5] =
                data[6] = data[7] = data[8] = 0;
	

	
}
Matrix3::Matrix3(const Vector3 &compOne, const Vector3 &compTwo,
            const Vector3 &compThree)
{
    SetComponents(compOne, compTwo, compThree);
}
//Matrix3::Matrix3(float m_data[3][3])
//{
//	for(int i = 0; i<3; i++)
//		for(int j = 0; j<3; j++)
//			m_data[i][j] = m_data[i][j];
//}

Matrix3::Matrix3(float c0, float c1, float c2, float c3, float c4, float c5,
            float c6, float c7, float c8)
{
	/*m_data[0][0] = c0;
	m_data[0][1] = c1;
	m_data[0][2] = c2;

	m_data[1][0] = c3;
	m_data[1][1] = c4;
	m_data[1][2] = c5;

	m_data[2][0] = c6;
	m_data[2][1] = c7;
	m_data[2][2] = c8;*/

	data[0] = c0; data[1] = c1; data[2] = c2;
    data[3] = c3; data[4] = c4; data[5] = c5;
    data[6] = c6; data[7] = c7; data[8] = c8;
}



void Matrix3::operator+=(const Matrix3 &o)
{
    data[0] += o.data[0]; data[1] += o.data[1]; data[2] += o.data[2];
    data[3] += o.data[3]; data[4] += o.data[4]; data[5] += o.data[5];
    data[6] += o.data[6]; data[7] += o.data[7]; data[8] += o.data[8];
}

//Matrix3 Matrix3::operator + (const Matrix3 &rhs)
//{
//	Matrix3 temp;
//	
//	for(int i = 0; i<3; i++)
//		for(int j = 0; j<3; j++)
//			temp.m_data[i][j] = m_data[i][j]+ rhs.m_data[i][j];
//
//	return temp;
//}
Matrix3 Matrix3::operator * (const Matrix3 &o)
{
	/*return Matrix3(
		m_data[0][0]*o.m_data[0][0] + m_data[0][1]*o.m_data[1][0] + m_data[0][2]*o.m_data[2][0],
		m_data[0][0]*o.m_data[0][1] + m_data[0][1]*o.m_data[1][1] + m_data[0][2]*o.m_data[2][1],
		m_data[0][0]*o.m_data[0][2] + m_data[0][1]*o.m_data[1][2] + m_data[0][2]*o.m_data[2][2],

		m_data[1][0]*o.m_data[0][0] + m_data[1][1]*o.m_data[1][0] + m_data[1][2]*o.m_data[2][0],
		m_data[1][0]*o.m_data[0][1] + m_data[1][1]*o.m_data[1][1] + m_data[1][2]*o.m_data[2][1],
		m_data[1][0]*o.m_data[0][2] + m_data[1][1]*o.m_data[1][2] + m_data[1][2]*o.m_data[2][2],

		m_data[2][0]*o.m_data[0][0] + m_data[2][1]*o.m_data[1][0] + m_data[2][2]*o.m_data[2][0],
		m_data[2][0]*o.m_data[0][1] + m_data[2][1]*o.m_data[1][1] + m_data[2][2]*o.m_data[2][1],
		m_data[2][0]*o.m_data[0][2] + m_data[2][1]*o.m_data[1][2] + m_data[2][2]*o.m_data[2][2]
	);*/
	return Matrix3(
        data[0]*o.data[0] + data[1]*o.data[3] + data[2]*o.data[6],
        data[0]*o.data[1] + data[1]*o.data[4] + data[2]*o.data[7],
        data[0]*o.data[2] + data[1]*o.data[5] + data[2]*o.data[8],

        data[3]*o.data[0] + data[4]*o.data[3] + data[5]*o.data[6],
        data[3]*o.data[1] + data[4]*o.data[4] + data[5]*o.data[7],
        data[3]*o.data[2] + data[4]*o.data[5] + data[5]*o.data[8],

        data[6]*o.data[0] + data[7]*o.data[3] + data[8]*o.data[6],
        data[6]*o.data[1] + data[7]*o.data[4] + data[8]*o.data[7],
        data[6]*o.data[2] + data[7]*o.data[5] + data[8]*o.data[8]
        );


}
void Matrix3::operator *= (const Matrix3 &o)
{
	float t1;
    float t2;
    float t3;

    t1 = data[0]*o.data[0] + data[1]*o.data[3] + data[2]*o.data[6];
            t2 = data[0]*o.data[1] + data[1]*o.data[4] + data[2]*o.data[7];
            t3 = data[0]*o.data[2] + data[1]*o.data[5] + data[2]*o.data[8];
            data[0] = t1;
            data[1] = t2;
            data[2] = t3;

            t1 = data[3]*o.data[0] + data[4]*o.data[3] + data[5]*o.data[6];
            t2 = data[3]*o.data[1] + data[4]*o.data[4] + data[5]*o.data[7];
            t3 = data[3]*o.data[2] + data[4]*o.data[5] + data[5]*o.data[8];
            data[3] = t1;
            data[4] = t2;
            data[5] = t3;

            t1 = data[6]*o.data[0] + data[7]*o.data[3] + data[8]*o.data[6];
            t2 = data[6]*o.data[1] + data[7]*o.data[4] + data[8]*o.data[7];
            t3 = data[6]*o.data[2] + data[7]*o.data[5] + data[8]*o.data[8];
            data[6] = t1;
            data[7] = t2;
            data[8] = t3;

	

}

void Matrix3::operator *= (const float &scalar)
{
	/*for(int i = 0; i<3; i++)
		for(int j = 0; j<3; j++)
			m_data[i][j] += scalar;

	return *this;*/

	data[0] *= scalar; data[1] *= scalar; data[2] *= scalar;
    data[3] *= scalar; data[4] *= scalar; data[5] *= scalar;
    data[6] *= scalar; data[7] *= scalar; data[8] *= scalar;

}

/**
    * Transform the given vector by this matrix.
    *
    * @param vector The vector to transform.
    */
Vector3 Matrix3::operator*(const Vector3 &vector)
{
   /* return Vector3(
        vector.x * m_data[0][0] + vector.y * m_data[0][1] + vector.z * m_data[0][2],
        vector.x * m_data[1][0] + vector.y * m_data[1][1] + vector.z * m_data[1][2],
        vector.x * m_data[2][0] + vector.y * m_data[2][1] + vector.z * m_data[2][2]
    );*/
	 return Vector3(
        vector.x * data[0] + vector.y * data[1] + vector.z * data[2],
        vector.x * data[3] + vector.y * data[4] + vector.z * data[5],
        vector.x * data[6] + vector.y * data[7] + vector.z * data[8]
    );

}

//Vector3 Matrix3::operator * (const Vector3 &rhs)
//{
//	Vector3 result;
//
//	result.x = m_data[0][0]*rhs.x + m_data[0][1]*rhs.y + m_data[0][2]*1;    
//	result.y = m_data[1][0]*rhs.x + m_data[1][1]*rhs.y + m_data[1][2]*1;
//
//	return result;
//	
//
//}
//Matrix3 Matrix3::operator = (const Matrix3 &rhs)
//{
//	for(int i = 0; i<3; i++)
//		for(int j = 0; j<3; j++)
//			m_data[i][j] = rhs.m_data[i][j];
//	return *this;
//}

//void Matrix3::scalar(float scale)
//{
//	for(int i = 0; i<3; i++)
//		for(int j = 0; j<3; j++)
//			m_data[i][j] = m_data[i][j] * scale;
//
//}
//void Matrix3::print()
//{
//	for(int i = 0; i<3; i++)
//	{
//		for(int j = 0; j<3; j++)
//			cout<< m_data[i][j] << " ";
//		
//		cout<< endl;
//	}
//}

void Matrix3::SetTranspose(Matrix3 &m)	// returns new matrix containing transpose of this matrix
{
	////0
	//m_data[0][0] = m.m_data[0][0];
	////1
	//m_data[0][1] = m.m_data[1][0];
	////2
	//m_data[0][2] = m.m_data[2][0];
	////3
	//m_data[1][0] = m.m_data[0][1];
	////4
	//m_data[1][1] = m.m_data[1][1];
	////5
	//m_data[1][2] = m.m_data[2][1];
	////6
	//m_data[2][0] = m.m_data[0][2];
	////7
	//m_data[2][1] = m.m_data[1][2];
	////8
	//m_data[2][2] = m.m_data[2][2];

	data[0] = m.data[0];
    data[1] = m.data[3];
    data[2] = m.data[6];
    data[3] = m.data[1];
    data[4] = m.data[4];
    data[5] = m.data[7];
    data[6] = m.data[2];
    data[7] = m.data[5];
    data[8] = m.data[8];

	
}

/**
    * Sets this matrix to be the rotation matrix corresponding to
    * the given quaternion.
    */
void Matrix3::SetOrientation(const Quaternion &q)
{
    data[0] = 1 - (2*q.y*q.y + 2*q.z*q.z);
    data[1] = 2*q.x*q.y + 2*q.z*q.w;
    data[2] = 2*q.x*q.z - 2*q.y*q.w;
    data[3] = 2*q.x*q.y - 2*q.z*q.w;
    data[4] = 1 - (2*q.x*q.x  + 2*q.z*q.z);
    data[5] = 2*q.y*q.z + 2*q.x*q.w;
    data[6] = 2*q.x*q.z + 2*q.y*q.w;
    data[7] = 2*q.y*q.z - 2*q.x*q.w;
    data[8] = 1 - (2*q.x*q.x  + 2*q.y*q.y);
}

Matrix3 Matrix3::Transpose()	// returns new matrix containing transpose of this matrix
{
	Matrix3 result;
	result.SetTranspose(*this);
	return result;
}


//Matrix3 Matrix3::translateMatrix(Vector3 deltaXY)
//{
//	Matrix3 temp;
//	
//	temp.m_data[0][0] += 1;
//	temp.m_data[0][2] += deltaXY.x;
//	temp.m_data[1][2] += deltaXY.y;
//	temp.m_data[1][1] += 1;
//	temp.m_data[2][2] += 1;
//
//
//	return temp;
//}
//
//Matrix3 Matrix3::scaleMatrix(Vector3 scaleXY)
//{
//	Matrix3 temp;
//
//	temp.m_data[0][0] += scaleXY.x;
//	temp.m_data[1][1] += scaleXY.y;
//	temp.m_data[2][2] += 1;
//
//	return temp;
//}
//
//Matrix3 Matrix3::rotationMatrix(float theta)
//{
//	Matrix3 temp;
//	float thetaRad = theta * 3.14f / 180;
//
//	temp.m_data[0][0] += (cos(thetaRad));
//	temp.m_data[0][1] += (-sin(thetaRad));
//	temp.m_data[1][0] += (sin(thetaRad));
//	temp.m_data[1][1] += (cos(thetaRad));
//	temp.m_data[2][2] += 1;
//
//	return temp;
//
//}
/*
	pass in values and use combo matrix for all calcs.

*/
//Matrix3 Matrix3:: comboMatrix(float translateX, float translateY, float translateZ, float rotationVal, float scaleX, 
//	float scaleY, float scaleZ)
//{
//	Matrix3 combo;
//	Matrix3 translate,rotate,scale;
//
//	translate.initializeIdentMatrix();
//	rotate.initializeIdentMatrix();
//	scale.initializeIdentMatrix();
//
//	if (translateX != 0 || translateY !=0)
//	{
//		Vector3 translationVec(translateX, translateY, translateZ);
//		translate = translate.translateMatrix(translationVec);
//	}
//
//
//	if ( rotationVal != 0)
//		rotate = rotate.rotationMatrix(rotationVal);
//
//	if (scaleX != 0 || scaleY != 0)
//	{
//		if(scaleX == 0)
//			scaleX = 1;
//
//		if(scaleY == 0)
//			scaleY = 1;
//
//		Vector3 scaleVec(scaleX,scaleY,scaleZ);
//		scale = scale.scaleMatrix(scaleVec);
//	}
//		
//		combo = translate * scale * rotate;
//	
//	
//
//	return combo;
//
//}

////default change of basis - goes around object's own origin
//
//Matrix3 Matrix3::worldToLocal(Vector3 point)
//{
//	Matrix3 temp;
//
//	float worldLocalX = point.x * (-1);
//	float worldLocalY = point.y * (-1);
//	
//	temp.m_data[0][0] += 1;
//	temp.m_data[0][2] = worldLocalX;
//	temp.m_data[1][2] = worldLocalY;
//	temp.m_data[1][1] += 1;
//	temp.m_data[2][2] += 1;
//
//	return temp;
//}
//
//// overloaded: takes two points - the point to be changed, and an origin
//// NOTE: to rotate around its own origin, just Set the origin to the object's position
//
//Matrix3 Matrix3::worldToLocal(Vector3 point, Vector3 origin)
//{
//	Matrix3 temp;
//
//	float worldLocalX = point.x - origin.x * -1;
//	float worldLocalY = point.y - origin.y * -1;
//	
//	temp.m_data[0][0] += 1;
//	temp.m_data[0][2] = worldLocalX;
//	temp.m_data[1][2] = worldLocalY;
//	temp.m_data[1][1] += 1;
//	temp.m_data[2][2] += 1;
//
//	return temp;
//}
//
////default change of basis - goes around world origin
//
//Matrix3 Matrix3::localToWorld(Vector3 point)
//{
//	Matrix3 temp;
//	
//	temp.m_data[0][0] += 1;
//	temp.m_data[0][2] += point.x;
//	temp.m_data[1][2] += point.y;
//	temp.m_data[1][1] += 1;
//	temp.m_data[2][2] += 1;
//
//	return temp;
//}
//
////overloaded: takes a 2nd point as the origin
//// NOTE: to rotate around its own origin, just Set the origin to the object's position
//
//Matrix3 Matrix3::localToWorld(Vector3 point, Vector3 origin)
//{
//	Matrix3 temp;
//
//	float localWorldX = -(point.x - origin.x);
//	float localWorldY = -(point.y - origin.y);
//	
//	temp.m_data[0][0] += 1;
//	temp.m_data[0][2] += localWorldX;
//	temp.m_data[1][2] += localWorldY;
//	temp.m_data[1][1] += 1;
//	temp.m_data[2][2] += 1;
//
//	return temp;
//}

//void Matrix3::initializeIdentMatrix()
//{
//	for(int i = 0; i<3; i++)
//		for(int j = 0; j<3; j++)
//			m_data[i][j] = 0;
//	
//	m_data[0][0] += 1;
//	m_data[1][1] += 1;
//	m_data[2][2] += 1;
//}

void Matrix3::SetInverse(const Matrix3 &m)
 {


	 //double t4 = m.m_data[0][0]*m.m_data[1][1];
  //  double t6 = m.m_data[0][0]*m.m_data[1][2];
  //  double t8 = m.m_data[0][1]*m.m_data[1][0];
  //  double t10 = m.m_data[0][2]*m.m_data[1][0];
  //  double t12 = m.m_data[0][1]*m.m_data[2][0];
  //  double t14 = m.m_data[0][2]*m.m_data[2][0];

  //          // Calculate the determinant
  //  double t16 = (t4*m.m_data[2][2] - t6*m.m_data[2][1] - t8*m.m_data[2][2]+
  //                      t10*m.m_data[2][1] + t12*m.m_data[1][2] - t14*m.m_data[1][1]);

  //          // Make sure the determinant is non-zero.
  //    if (t16 == (double)0.0f) return;
  //    double t17 = 1/t16;

  //    m_data[0][0] = (m.m_data[1][1]*m.m_data[2][2]-m.m_data[1][2]*m.m_data[2][1])*t17;
  //    m_data[0][1] = -(m.m_data[0][1]*m.m_data[2][2]-m.m_data[0][2]*m.m_data[2][1])*t17;
  //    m_data[0][2] = (m.m_data[0][1]*m.m_data[1][2]-m.m_data[0][2]*m.m_data[1][1])*t17;
  //    m_data[1][0] = -(m.m_data[1][0]*m.m_data[2][2]-m.m_data[1][2]*m.m_data[2][0])*t17;
  //    m_data[1][1] = (m.m_data[0][0]*m.m_data[2][2]-t14)*t17;
  //    m_data[1][2] = -(t6-t10)*t17;
  //    m_data[2][0] = (m.m_data[1][0]*m.m_data[2][1]-m.m_data[1][1]*m.m_data[2][0])*t17;
  //    m_data[2][1] = -(m.m_data[0][0]*m.m_data[2][1]-t12)*t17;
  //    m_data[2][2] = (t4-t8)*t17;

	float t4 = m.data[0]*m.data[4];
    float t6 = m.data[0]*m.data[5];
    float t8 = m.data[1]*m.data[3];
    float t10 = m.data[2]*m.data[3];
    float t12 = m.data[1]*m.data[6];
    float t14 = m.data[2]*m.data[6];

    // Calculate the determinant
    float t16 = (t4*m.data[8] - t6*m.data[7] - t8*m.data[8]+
                t10*m.data[7] + t12*m.data[5] - t14*m.data[4]);

    // Make sure the determinant is non-zero.
    if (t16 == (float)0.0f) return;
    float t17 = 1/t16;

    data[0] = (m.data[4]*m.data[8]-m.data[5]*m.data[7])*t17;
    data[1] = -(m.data[1]*m.data[8]-m.data[2]*m.data[7])*t17;
    data[2] = (m.data[1]*m.data[5]-m.data[2]*m.data[4])*t17;
    data[3] = -(m.data[3]*m.data[8]-m.data[5]*m.data[6])*t17;
    data[4] = (m.data[0]*m.data[8]-t14)*t17;
    data[5] = -(t6-t10)*t17;
    data[6] = (m.data[3]*m.data[7]-m.data[4]*m.data[6])*t17;
    data[7] = -(m.data[0]*m.data[7]-t12)*t17;
    data[8] = (t4-t8)*t17;
}

 /**
         * Sets the matrix values from the given three vector components.
         * These are arranged as the three columns of the vector.
         */
void Matrix3::SetComponents(const Vector3 &compOne, const Vector3 &compTwo, const Vector3 &compThree)
{
	/*m_data[0][0] = compOne.x;
	m_data[0][1] = compTwo.x;
	m_data[0][2] = compThree.x;
	m_data[1][0] = compOne.y;
	m_data[1][1] = compTwo.y;
	m_data[1][2] = compThree.y;
	m_data[2][0] = compOne.z;
	m_data[2][1] = compTwo.z;
	m_data[2][2] = compThree.z;*/

	data[0] = compOne.x;
    data[1] = compTwo.x;
    data[2] = compThree.x;
    data[3] = compOne.y;
    data[4] = compTwo.y;
    data[5] = compThree.y;
    data[6] = compOne.z;
    data[7] = compTwo.z;
    data[8] = compThree.z;

}



/**
    * Transform the given vector by this matrix.
    *
    * @param vector The vector to transform.
    */
Vector3 Matrix3::Transform(const Vector3 &vector) 
{
    return (*this) * vector;
}

/**
* Transform the given vector by the transpose of this matrix.
*
* @param vector The vector to transform.
*/
Vector3 Matrix3::transformTranspose(const Vector3 &vector)
{
    /*return Vector3(
        vector.x * m_data[0][0] + vector.y * m_data[1][0] + vector.z * m_data[2][0],
        vector.x * m_data[0][1] + vector.y * m_data[1][1] + vector.z * m_data[2][1],
        vector.x * m_data[0][2] + vector.y * m_data[1][2] + vector.z * m_data[2][2]
    );*/

	return Vector3(
        vector.x * data[0] + vector.y * data[3] + vector.z * data[6],
        vector.x * data[1] + vector.y * data[4] + vector.z * data[7],
        vector.x * data[2] + vector.y * data[5] + vector.z * data[8]
    );
}

/**
    * Sets the matrix to be a diagonal matrix with the given
    * values along the leading diagonal.
    */
void Matrix3::SetDiagonal(float a, float b, float c)
{
    SetInertiaTensorCoeffs(a, b, c);
}

/**
    * Sets the value of the matrix from inertia tensor values.
    */
void Matrix3::SetInertiaTensorCoeffs(float ix, float iy, float iz,
    float ixy, float ixz, float iyz)
{
   /* m_data[0][0] = ix;
    m_data[0][1] = m_data[1][0] = -ixy;
    m_data[0][2] = m_data[2][0] = -ixz;
    m_data[1][1] = iy;
    m_data[1][2] = m_data[2][1] = -iyz;
    m_data[2][2] = iz;*/

	data[0] = ix;
    data[1] = data[3] = -ixy;
    data[2] = data[6] = -ixz;
    data[4] = iy;
    data[5] = data[7] = -iyz;
    data[8] = iz;

}

/**
    * Sets the value of the matrix as an inertia tensor of
    * a rectangular block aligned with the body's coordinate
    * system with the given axis half-sizes and mass.
    */
void Matrix3::SetBlockInertiaTensor(const Vector3 &halfSizes, float mass)
{
    Vector3 squares = halfSizes.ComponentProduct(halfSizes);
    SetInertiaTensorCoeffs(0.6f*mass*(squares.y + squares.z),
        0.6f*mass*(squares.x + squares.z),
        0.6f*mass*(squares.x + squares.y));
}

/**
    * Sets the matrix to be a skew symmetric matrix based on
    * the given vector. The skew symmetric matrix is the equivalent
    * of the vector product. So if a,b are vectors. a x b = A_s b
    * where A_s is the skew symmetric form of a.
    */
void Matrix3::SetSkewSymmetric(const Vector3 vector)
{
  /*  m_data[0][0] = m_data[1][1] = m_data[2][2] = 0;
    m_data[0][1] = -vector.z;
    m_data[0][2] = vector.y;
    m_data[1][0] = vector.z;w
    m_data[1][2] = -vector.x;
    m_data[2][0] = -vector.y;
    m_data[2][1] = vector.x;*/

	data[0] = data[4] = data[8] = 0;
    data[1] = -vector.z;
    data[2] = vector.y;
    data[3] = vector.z;
    data[5] = -vector.x;
    data[6] = -vector.y;
    data[7] = vector.x;
}



/** Returns a new matrix containing the inverse of this matrix. */
Matrix3 Matrix3::Inverse() const
{
    Matrix3 result;
    result.SetInverse(*this);
    return result;
}

/**
    * Inverts the matrix.
    */
void Matrix3::Invert()
{
    SetInverse(*this);
}

/**
    * Gets a vector representing one row in the matrix.
    *
    * @param i The row to return.
    */
Vector3 Matrix3::GetRowVector(int i) const
{
    return Vector3(data[i*3], data[i*3+1], data[i*3+2]);
}

/**
    * Gets a vector representing one axis (i.e. one column) in the matrix.
    *
    * @param i The row to return.
    *
    * @return The vector.
    */
Vector3 Matrix3::GetAxisVector(int i) const
{
    return Vector3(data[i], data[i+3], data[i+6]);
}

Matrix3 Matrix3::linearInterpolate(const Matrix3& a, const Matrix3& b, float prop)
{
    Matrix3 result;
    for (unsigned i = 0; i < 9; i++) {
        result.data[i] = a.data[i] * (1-prop) + b.data[i] * prop;
    }
    return result;
}