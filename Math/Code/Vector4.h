#pragma once

// For the typedef of real. and DXMath Depenedencies
#include "Vector2.h" 

struct Matrix;
struct Vector4 : D3DXVECTOR4
{
	Vector4() : D3DXVECTOR4(0, 0, 0, 0) { };
	Vector4(real xVal, real yVal, real zVal, real wVal) : D3DXVECTOR4(xVal, yVal, zVal, wVal) {};
	Vector4(real xVal, real yVal, real zVal) : D3DXVECTOR4(xVal, yVal, zVal, 1) {};
	Vector4(D3DXVECTOR4 dxVec3): D3DXVECTOR4(dxVec3) { };
	Vector4(real val) : D3DXVECTOR4(val, val, val, val) {};

	Vector4& operator=(const Vector4 &rhs);
	Vector4& operator=(const D3DXVECTOR4 &rhs);
	bool operator==(const Vector4 &rhs);
	Vector4 operator-= (const Vector4 &rhs);
	Vector4 operator+= (const Vector4 &rhs); 
    Vector4 operator- (const Vector4 &rhs);  
	Vector4 operator+ (const Vector4 &rhs);
	Vector4 operator*= (real value);
	Vector4 operator* (real value);
	Vector4 operator/= (real value);
	Vector4 operator/ (real value);
	real operator*= ( const Vector4 &rhs );
	real operator* (const Vector4 &rhs);	//Dot Product

	void Clear();
	void Negate();
	real Length();
	real LengthSquared();
	Vector4 Normalize();

	// Static methods
	static Vector4 One() { return Vector4(1, 1, 1, 1); };
	static Vector4 Zero() { return Vector4(0, 0, 0, 0); };
	static Vector4 Lerp(float weight, Vector4 a, Vector4 b);
	static Vector4 Min(Vector4 v1, Vector4 v2);
	static Vector4 Max(Vector4 v1, Vector4 v2);
};