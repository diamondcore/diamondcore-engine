#pragma once

#include <math.h>
#include "Matrix3.h"
#include "vector3.h"
#include <d3dx9.h>



class Matrix4;

class Quaternion
	: public D3DXQUATERNION
{

public:
	
		
	//float data[4];
	Quaternion(Vector3 axis,float theta);
	Quaternion(float w =1.0f,float x=0.0f,float=0.0f,float=0.0f);
	Quaternion(float iw, Vector3 qVec);
	Quaternion(const D3DXQUATERNION &quat);
	Quaternion(const Quaternion &quat);
	Quaternion(const Matrix4 &other);
	
	void Normalize();
	Quaternion operator *(Quaternion &multiplier);
	void rotateByVector(const Vector3& vector);
	void AddScaledVector(const Vector3& vector,float scale);
	void operator*=(Quaternion q);
	Quaternion operator *(float);
	void operator +=(Quaternion q);
	Quaternion operator +(Quaternion q2);
	void CreateBasis(Vector3* pA, Vector3* pB, Vector3* pC);
	void QuatFromVectors(Vector3 from, Vector3 to);
	Quaternion GetConjugate();

	Vector3 GetForward();
	Vector3 GetUp();
	Vector3 GetRight();

	// Returns a Vector3 with x = pitch, y = yaw, z = roll
	Vector3 GetYawPitchRoll();
	void SetFromYawPitchRoll(float yaw, float pitch, float roll);
	/*Matrix3 Rotate(Quaternion pQuat);*/

	/*Matrix3 GetRotationMatrix();*/




};


