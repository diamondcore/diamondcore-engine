#pragma once

#include "Vector3.h"
#include <vector>

using namespace std;

class Spline
{
public:
	// This is only public for now for quick debug rendering
	vector<Vector3> mControlPoints;
	inline void AddControlPoint(Vector3 newPoint) { mControlPoints.push_back(newPoint); }
	Vector3 GetPosition(float dt);
	Vector3 GetLookAtPoint(float dt, bool reverseDirection);
	int GetNumOfSegments() { return mControlPoints.size() - 3; }

	static Spline* CreateMovementSpline(Vector3 startPoint, Vector3 endPoint, Vector3 normalizedModulationDir, int numPoints);
};