#include "Spline.h"
#include "Quaternion.h"

Vector3 Spline::GetPosition(float dt)
{
	auto numPoints = mControlPoints.size();
	auto segment = (int)floor(dt);
	auto segmentWeight = dt - segment;

	Vector3 position;

	D3DXVec3CatmullRom(&position,
						&mControlPoints[segment - 1],
						&mControlPoints[segment],
						&mControlPoints[segment + 1],
						&mControlPoints[segment + 2],
						segmentWeight);

	return position;
}

Vector3 Spline::GetLookAtPoint(float dt, bool reverseDirection)
{
	auto numPoints = mControlPoints.size();
	auto segment = (int)floor(dt);
	auto segmentWeight = dt - segment;

	if (reverseDirection)
		return mControlPoints[segment - 1];
	else
		return mControlPoints[segment + 1];
}

Spline* Spline::CreateMovementSpline(Vector3 startPoint, Vector3 endPoint, Vector3 normalizedModulationDir, int numPoints)
{
	auto outputSpline = new Spline();
	auto totalNumOfPoints = numPoints;
	
	auto directionToEnd = (endPoint - startPoint);
	auto segmentLength = directionToEnd.Magnitude() / numPoints;
	
	directionToEnd.Normalize();

	auto pointPosition = startPoint - (directionToEnd * segmentLength / 2);
	
	outputSpline->AddControlPoint(pointPosition);
	outputSpline->AddControlPoint(startPoint);
	pointPosition = startPoint;

	Vector3 headingDirection;

	int modulate = 1;
	for (int x = 0; x < numPoints; x++)
	{
		// Get the vector to the end.
		directionToEnd = (endPoint - pointPosition);
		directionToEnd.Normalize();

		//Get the direction we'll be displacing along.
		auto directionFlip = modulate <= 1 == 0 ? 1 : -1;
		auto displacementDir = normalizedModulationDir * directionFlip;

		//Find the direction we'll be heading.
		headingDirection = (displacementDir + directionToEnd) / 2;
		headingDirection.Normalize();

		//Move in that direction
		pointPosition += headingDirection * (segmentLength * 2);

		//Add this point.
		outputSpline->AddControlPoint(pointPosition);

		// Hack to simulate the motion of a sine wave.
		modulate++;
		if (modulate == 4)
			modulate = 0;
	}

	return outputSpline;
}