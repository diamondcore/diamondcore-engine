#pragma once

//For Pi.
#define _USE_MATH_DEFINES
#include <math.h>

static class MathUtil
{
public:

	template <typename T>
	static inline T Lerp(T weight, T a = 0.0f, T b = 1.0f) { return a + weight * (b - a); }
	
	static float EaseInOut(float s, int power)
	{
		s *= 2;

		if (s < 1)
			return pow(s, power) / 2;
		auto sign = power % 2 == 0 ? -1 : 1;
		return (float)(sign / 2.0 * (pow(s - 2, power) + sign * 2));
	}

	template <typename T>
	static inline T Clamp(T value, T min, T max) { return max(min(value, max), min); }
	static inline float Pi() { return (float)M_PI; }
	static inline float TwoPi() { return M_PI * 2; }
};

