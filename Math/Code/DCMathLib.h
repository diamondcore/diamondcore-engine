// ESMathLib.h

#pragma once

#pragma warning(disable : 4995)

#include <math.h>
#include <sstream>
#include <string>
#include <vector>
#include <limits>
#include <cassert>
#include <iomanip>
#include <iostream>
#include <cmath>
 

#define float_MAX FLT_MAX    
#define float_sqrt sqrt
#define float_abs fabs
#define float_sin sin
#define float_cos cos
#define float_exp exp
#define float_pow pow
#define float_fmod fmod
#define R_PI 3.14159265358979

	
#define DAMPING .99f
#define DEG2RAD R_PI / 180.0f
#define ELASTICITY .90f

// Safety include
#include <Windows.h>
#include <d3d10_1.h>
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"
#include "Matrix3.h"
#include "Matrix4.h"
#include "Quaternion.h"
#include "Spline.h"
#include "MathUtil.h"

#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
