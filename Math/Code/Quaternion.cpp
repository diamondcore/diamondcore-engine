#include "Quaternion.h"
#include "Matrix4.h"


Quaternion::Quaternion(float iw,float ix,float iy,float iz)
	:D3DXQUATERNION(ix,iy,iz,iw)
{
}

Quaternion::Quaternion(Vector3 axis,float theta)
{
	//just in case, normalizing the axis

	axis.Normalize();

	w= cos(theta/2);
	x= axis.x*sin(theta/2);
	y= axis.y*sin(theta/2);
	z= axis.z*sin(theta/2);

	

}

Quaternion::Quaternion(float iw,Vector3 qVec)
{

	if (iw + qVec.x + qVec.y + qVec.z <= 0)
		iw = 1.00;

	w=iw;
	x=qVec.x;
	y=qVec.y;
	z=qVec.z;


}

Quaternion::Quaternion(const D3DXQUATERNION &quat)
	:D3DXQUATERNION(quat)
{

}

Quaternion::Quaternion(const Quaternion &quat)
	:D3DXQUATERNION(quat.x,quat.y,quat.z,quat.w)
{

}

Quaternion::Quaternion(const Matrix4 &other)
{
	// step 1 find trace
	float T = other[0] + other[5] + other[10] +1;

	if (T > 0.0f)
	{
		//"instant calculation"
		float S = .5f / sqrtf(T);
		w = .25f / S;
		x = (other[9] - other[6]) * S;
		y = (other[2] - other[8]) * S;
		z = (other[4] - other[1]) * S;
	}
	else
	{
		//find highest value of 3 diagonals
		float highest = other[0];
		auto column = 0;
		unsigned i = 0;
		for (i; i < 3; ++i)
		{
			if (other[i*5] > highest)
			{
				highest = other[i*5];
				column = i;
			}

		}
		float S;

		switch(column)
		{
		case 0:
			S  = sqrt( 1.0 + other[0] - other[5] - other[10] ) * 2;
			
			x = .5 / S;
			y = (other[1] + other[4] ) / S;
			z = (other[2] + other[8] ) / S;
			w = (other[6] + other[9] ) / S;
			break;
		case 1:
			S  = sqrt( 1.0 + other[5] - other[0] - other[10] ) * 2;

			x = (other[1] + other[4] ) / S;
			y = 0.5 / S;
			z = (other[6] + other[9] ) / S;
			w = (other[2] + other[8] ) / S;
			break;
		case 2:
			S  = sqrt( 1.0 + other[10] - other[0] - other[5] ) * 2;

			x = (other[2] + other[8] ) / S;
			y = (other[6] + other[9] ) / S;
			z = 0.5 / S;
			w = (other[1] + other[4] ) / S;
			break;
		}
		
	}
}


void Quaternion::Normalize()
{
	D3DXQuaternionNormalize(this,this);

}
Quaternion Quaternion::operator +(Quaternion multiplier)
{
	return (D3DXQUATERNION) *this + multiplier;
}
void Quaternion::operator+=(Quaternion q){

	*this = (D3DXQUATERNION) *this + q;
}



Quaternion Quaternion::operator *(Quaternion &m)
{
	return (D3DXQUATERNION) *this * m;
}


void Quaternion::operator *= (Quaternion q)
{
	
	*this = *this * q;

}


void Quaternion::rotateByVector(const Vector3& vector) {

	Quaternion q(0.0,vector.x,vector.y,vector.z);
	(*this) *=q;
		
}

void Quaternion::AddScaledVector(const Vector3& vector,float scale)
{

	Quaternion q(0.0,vector.x*scale,vector.y*scale,vector.z*scale);
	q *= *this;


	w+= (q.w * .5f);
	x+= (q.x * .5f);
	y+= (q.y * .5f);
	z+= (q.z * .5f);


}

Quaternion Quaternion::operator *(float duration)
{
	
	
	return (D3DXQUATERNION) *this * duration;
}


//void Quaternion::CreateBasis(Vector3* pA, Vector3* pB, Vector3* pC)
//{
//	//create rotate matrix
//	Matrix3 Rot = Rotate(*this);
//
//	*pA = Rot * Vector3(1, 0, 0);
//	*pB = Rot * Vector3(0, 1, 0);
//	*pC = Rot * Vector3(0, 0, 1);
//}

void Quaternion::QuatFromVectors(Vector3 from, Vector3 to)
{
	// Get the axis of rotation
	Vector3 axis = from % to;

	// Get the scaled cos of angle between vectors and set initial quaternion
	*this = Quaternion(from*to, axis.x, axis.y, axis.z);

	// Normalize to get cos theta, sin theta r
	Normalize();

	// Set up for half angle calculation
	w += 1.0f;

	// If vectors are opposing
	if( w <= FLT_EPSILON)
	{
		// Find orthogonal vector
		if(from.z*from.z > from.x*from.x)
			*this = Quaternion(-from.y, 0.0f, from.z, 0.0f);
		else
			*this = Quaternion(0.0f, 0.0f, from.y, -from.x);
	}

	// Normalize again to get rotation quaternion
	Normalize();

	/* // Good, but somewhat inaccurate
	from.Normalize();
	to.Normalize();

	Vector3 a = from.CrossProductReturn(to);

	//float dot = from * to;
	float fW;
	//if(dot > 0.999999 && dot < -0.999999)
	//	fW = 1 + dot;
	//else
		fW = sqrtf(from.MagnitudeSquared() * to.MagnitudeSquared()) + from*to;
	
	x = a.x;
	y = a.y;
	z = a.z;
	w = fW;
	Normalize();
	*/
}

//Matrix3 Quaternion::Rotate(Quaternion pQuat)
//{
//	Matrix3 temp;
//
//	 temp.m_data[0][0] = 1-(2*(pQuat.y*pQuat.y)+2*(pQuat.z*pQuat.z));
//	 temp.m_data[0][1] = 2*pQuat.x*pQuat.y+2*pQuat.z*pQuat.w;
//	 temp.m_data[0][2] = 2*pQuat.x*pQuat.z-2*pQuat.y*pQuat.w;
//	 temp.m_data[1][0] = 2*pQuat.x*pQuat.y-2*pQuat.z*pQuat.w;
//	 temp.m_data[1][1] = 1-(2*(pQuat.x*pQuat.x)+2*(pQuat.z*pQuat.z));
//	 temp.m_data[1][2] = 2*pQuat.y*pQuat.z+2*pQuat.x*pQuat.w;
//	 temp.m_data[2][0] = 2*pQuat.x*pQuat.z+2*pQuat.y*pQuat.w;
//	 temp.m_data[2][1] = 2*pQuat.y*pQuat.z-2*pQuat.x*pQuat.w;
//	 temp.m_data[2][2] = 1-(2*(pQuat.x*pQuat.x)+2*(pQuat.x*pQuat.y));
//	 
//	 
//	 return temp;
//}
//
//Matrix3 Quaternion::GetRotationMatrix()
//{
//	Matrix3 temp;
//
//	 temp.m_data[0][0] = 1-(2*(this->y*this->y)+2*(this->z*this->z));
//	 temp.m_data[0][1] = 2*this->x*this->y+2*this->z*this->w;
//	 temp.m_data[0][2] = 2*this->x*this->z-2*this->y*this->w;
//	 temp.m_data[1][0] = 2*this->x*this->y-2*this->z*this->w;
//	 temp.m_data[1][1] = 1-(2*(this->x*this->x)+2*(this->z*this->z));
//	 temp.m_data[1][2] = 2*this->y*this->z+2*this->x*this->w;
//	 temp.m_data[2][0] = 2*this->x*this->z+2*this->y*this->w;
//	 temp.m_data[2][1] = 2*this->y*this->z-2*this->x*this->w;
//	 temp.m_data[2][2] = 1-(2*(this->x*this->x)+2*(this->x*this->y));
//	 
//	 
//	 return temp;
//}

Quaternion Quaternion::GetConjugate()
{
	Quaternion q;

	q.x = this->x * -1;
	q.y = this->y * -1;
	q.z = this->z * -1;
	return (q);
}

Vector3 Quaternion::GetForward()
{
    return Vector3( 2 * (x * z + w * y), 
                    2 * (y * x - w * x),
                    1 - 2 * (x * x + y * y));
}
 
Vector3 Quaternion::GetUp()
{
    return Vector3( 2 * (x * y - w * z), 
                    1 - 2 * (x * x + z * z),
                    2 * (y * z + w * x));
}
 
Vector3 Quaternion::GetRight()
{
    return Vector3( 1 - 2 * (y * y + z * z),
                    2 * (x * y + w * z),
                    2 * (x * z - w * y));
}
// Returns a Vector3 with x = pitch, y = yaw, z = roll
Vector3 Quaternion::GetYawPitchRoll()
{
	Vector3 result;
	float test = x*y + z*w;
	if (test > 0.499) 
	{		
		// singularity at north pole
		result.x = 2 * atan2(x, w);
		result.y = 3.14159265358979/2;
		result.z = 0;
		return result;
	}
	if (test < -0.499) 
	{ 
		// singularity at south pole
		result.x = -2 * atan2(x, w);
		result.y = - 3.14159265358979/2;
		result.z = 0;
		return result;
	}
    float sqx = x*x;
    float sqy = y*y;
    float sqz = z*z;
    result.x = atan2(2*y*w-2*x*z , 1 - 2*sqy - 2*sqz);
	result.y = asin(2*test);
	result.z = atan2(2*x*w-2*y*z , 1 - 2*sqx - 2*sqz);
	return result;
}

void Quaternion::SetFromYawPitchRoll(float yaw, float pitch, float roll)
{
    float rollOver2 = roll * 0.5f;
    float sinRollOver2 = (float)sinf((double)rollOver2);
    float cosRollOver2 = (float)cosf((double)rollOver2);
    float pitchOver2 = pitch * 0.5f;
    float sinPitchOver2 = (float)sinf((double)pitchOver2);
    float cosPitchOver2 = (float)cosf((double)pitchOver2);
    float yawOver2 = yaw * 0.5f;
    float sinYawOver2 = (float)sinf((double)yawOver2);
    float cosYawOver2 = (float)cosf((double)yawOver2);

    // X = PI is giving incorrect result (pitch)

    // Heading = Yaw
    // Attitude = Pitch
    // Bank = Roll
    w = cosYawOver2 * cosPitchOver2 * cosRollOver2 - sinYawOver2 * sinPitchOver2 * sinRollOver2;
    x = sinYawOver2 * sinPitchOver2 * cosRollOver2 + cosYawOver2 * cosPitchOver2 * sinRollOver2;
    y = sinYawOver2 * cosPitchOver2 * cosRollOver2 + cosYawOver2 * sinPitchOver2 * sinRollOver2;
	z = cosYawOver2 * sinPitchOver2 * cosRollOver2 - sinYawOver2 * cosPitchOver2 * sinRollOver2;
}
