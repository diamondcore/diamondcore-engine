#pragma once

#define	WIN32_LEAN_AND_MEAN	// Excludes rarely used services
#include <Windows.h>
#pragma comment(lib, "winmm.lib")

// Enable Direct3D Debug
#if defined(DEBUG) | defined(_DEBUG)
#ifndef D3D_DEBUG_INFO
#define D3D_DEBUG_INFO
#endif
#endif

#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>
	
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "DxErr.lib")
#pragma comment(lib, "strmiids.lib")

class DirectXClass
{
protected:
	//////////////////////////////////////////////////////////////////////////
	// Direct3D Variables
	//////////////////////////////////////////////////////////////////////////
	IDirect3D9*				m_pObject;	// Direct3D 9 Object
	ID3DXSprite*			m_pSprite;	// Direct3D 9 Sprite
	IDirect3DDevice9*		m_pDevice;	// Direct3D 9 Device
	D3DCAPS9				m_D3DCaps;	// Device Capabilities
	

	//////////////////////////////////////////////////////////////////////////
	// Application Variables
	//////////////////////////////////////////////////////////////////////////
	D3DPRESENT_PARAMETERS	m_Dpp;		// Present Parameters
	HWND					m_hWnd;		// Handle to the Window
	HINSTANCE				m_hInst;	// Handle to the Instance
	bool					m_bVsync;	// Boolean for vertical syncing
	D3DXCOLOR				m_BkgColor;	// Window's Background Color
	D3DFORMAT				m_LumFormat;	// Luminance suported format

	void	MultiSampling(bool isOn);
	void	PreRestartDevice();
	void	PostRestartDevice();

public:
	DirectXClass();
	~DirectXClass();
	void	RestartDevice();

	bool	Init(HINSTANCE hInst, HWND hWnd, bool windowed, bool Vsync, bool multiSample);
	void	Shutdown();
	void	BeginScene();
	void	EndScene();

	ID3DXSprite*		GetSprite();
	IDirect3DDevice9*	GetDevice();

	HWND				GetWindowHandle();
	HINSTANCE			GetWindowInstance();

	bool		IsVsyncOn();		
	void		SetFullScreen(bool isFullScreen);
	bool		IsFullScreen();
	void		ChangeScreenSize();
	float		GetScreenWidth();
	float		GetScreenHeight();
	void		SetBackgroundColor(D3DXCOLOR color);
	D3DFORMAT	GetLuminanceFormat();
	bool		CheckFormatSupport(D3DFORMAT device, D3DFORMAT depth);
};