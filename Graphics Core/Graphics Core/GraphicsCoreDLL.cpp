#include "GraphicsCoreDLL.h"
#include <Messaging System/MessageDispatcher.h>
#include <DataTypes.h>
#include "GraphicsCore.h"
#include "MacroTools.h"

GraphicsCoreDLL::GraphicsCoreDLL(void)
	:IGraphicsCore(),
	 m_pApp(0),
	 m_bDeviceLost(0)
{
	//std::cout << "	Graphics Core Constructor Called." << std::endl;
}

GraphicsCoreDLL::~GraphicsCoreDLL(void)
{
	// To prevent any memory leaks or some other stupidity
	if(m_pApp)
	{
		// This odviously hasn't been shutdown yet.
		m_pApp->Shutdown();
		delete m_pApp;
		m_pApp = 0;
	}
}

void GraphicsCoreDLL::BeginRender()
{
	// Render 3D Graphics Objects
	if(m_pApp && !m_bDeviceLost)
		m_pApp->m_pGraphicsMgr->BeginRender();
}
// Rendering 2D Calls
void GraphicsCoreDLL::Render2D(char* textureName, D3DXMATRIX transform)	// Render 2D Texture
{
	if(!m_pApp || m_bDeviceLost)
		return;
	if(!m_pApp->m_pGraphicsMgr->HasStartedRender())
	{
		MessageBox(0, "You must call BeginRender() before Render2D().", "Error", MB_OK);
		return;
	}
	GCI->GetTextureManager()->RenderTexture(textureName, transform);
}
void GraphicsCoreDLL::Render2D(char* textureName, D3DXMATRIX transform, D3DXCOLOR color) // Render 2D Texture with colors
{
	if(!m_pApp || m_bDeviceLost)
		return;
	if(!m_pApp->m_pGraphicsMgr->HasStartedRender())
	{
		MessageBox(0, "You must call BeginRender() before Render2D().", "Error", MB_OK);
		return;
	}
	GCI->GetTextureManager()->RenderTexture(textureName, transform, color);
}
// Batch Rendering
void GraphicsCoreDLL::Render2D(char* textureName, D3DXMATRIX transform[], int count)
{
	if(!m_pApp || m_bDeviceLost)
		return;
	if(!m_pApp->m_pGraphicsMgr->HasStartedRender())
	{
		MessageBox(0, "You must call BeginRender() before Render2D().", "Error", MB_OK);
		return;
	}
	GCI->GetTextureManager()->RenderTexture(textureName, transform, count);
}
void GraphicsCoreDLL::Render2D(char* textureName, D3DXMATRIX transform[], D3DXCOLOR color[], int count)
{
	if(!m_pApp || m_bDeviceLost)
		return;
	if(!m_pApp->m_pGraphicsMgr->HasStartedRender())
	{
		MessageBox(0, "You must call BeginRender() before Render2D().", "Error", MB_OK);
		return;
	}
	GCI->GetTextureManager()->RenderTexture(textureName, transform, color, count);
}

void GraphicsCoreDLL::LoadFont(const char* fontName)
{
	GCI->GetFontManager()->Load(fontName);
}

void GraphicsCoreDLL::UnloadFont(const char* fontName)
{
	GCI->GetFontManager()->Unload(fontName);
}

Vector2 GraphicsCoreDLL::MeasureString(const char* fontName, char* text)
{
	return GCI->GetFontManager()->MeasureString(fontName, text);
}

void GraphicsCoreDLL::RenderString(const char* fontName, const char* text, Vector2 pos, D3DXCOLOR color)
{
	GCI->GetFontManager()->Render(text, pos.x, pos.y, fontName, color);
}

void GraphicsCoreDLL::RenderString(const char* fontName, const char* text, Vector2 pos, float depth, D3DXCOLOR color)
{
	GCI->GetFontManager()->Render(text, pos.x, pos.y, depth, fontName, color);
}

void GraphicsCoreDLL::EndRender()
{
	// Present buffer to the screen
	if(m_pApp && !m_bDeviceLost)
		m_pApp->m_pGraphicsMgr->EndRender();
}

///Use as Test Variables///
int id;
//////////////////////////

// VIRTUAL OVERRIDING
void GraphicsCoreDLL::Update(float dt)
{
	if(!m_pApp)
		return;
	
	//////////////////////////Test Code////////////////////////
	//// Rotate Box
	//Quaternion rot;
	//static float angle = 0.0f;
	//angle += dt * 0.1f;
	//if(angle >= 360.0f)
	//	angle -= 360.0f;
	//rot.SetFromYawPitchRoll(angle, 0.0f, angle);
	//m_pSharedMemory->GetSharedResource(id)->SetOrientation(rot);
	////////////////////////////////////////////////////////////

	// Manually Throttle this thread
	// Make every frame last 1/60th of a second.
/*	float delay = (1.0f - m_pApp->GetAccumTime());	// Get the time left in this 1 second interval	
	delay /= (60.0f - m_pApp->GetNumFrames());		// Divide the time left by the number of frames left
	if(delay > 0.0f)
		Sleep( delay*1000.0f );	// Delay until the rest of our 1/60th of a second is up
*/	

	//GCI->Lock();
	//OutputDebugString("Enter GraphicsCore::Lock() - Update()");

	// Make sure the device is not lost before anything
	if(m_bDeviceLost = GCI->CheckDevice())
	{
		//GCI->Unlock();
		//OutputDebugString("Enter GraphicsCore::Unlock() - Update()");
		return;
	}

	// Process Messages
	ProcessMessages();

	// Update Graphics Objects
	m_pApp->Update(dt, m_pSharedMemory);

	// Check for picked quads and store in a message
	//auto mData = MessageData();
	//m_pApp->m_pGraphicsMgr->CheckQuadPicking(mData.m_mQuadPickData.m_iPickedIDs, mData.m_mQuadPickData.m_iCount);
	// Send back message with picked quad IDs and count
	// TODO: Send the message to the correct target.
	
	//if(mData.m_mQuadPickData.m_iCount > 0)
		//m_pMessageMgr->DispatchMsg(CT_GRAPHICS, CT_SCRIPTING, 4, &mData);
				
	//GCI->Unlock();
	//OutputDebugString("Enter GraphicsCore::Unlock() - Update()");
}

void GraphicsCoreDLL::Initialize()
{
	//std::cout << "Initialization for Graphics Core" << std::endl;
	Initialize(GetModuleHandle(NULL), "Diamond Core Engine");
}
void GraphicsCoreDLL::Initialize(HINSTANCE hInst, const char* windowTitle)
{
	if(m_pApp)
	{
		//std::cout << "Can't Initialize Graphics Core already exist." << std::endl;
		return;
	}
	//std::cout << "Initializing Graphics Core" << std::endl;
	
	// Create The Window and Initialize DirectX
	m_pApp = new BaseApp();
	m_pApp->Init(hInst, windowTitle);
	m_pApp->SetMemMgr(m_pSharedMemory);	// Only want to set this once (used only for pause)

	// Set window handle to share outside the core
	m_hWnd = GCI->GetDirectX()->GetWindowHandle();

	

	///////////Test Code///////////////
	// Set Camera to shared id
	//int camera = m_pSharedMemory->CreateSharedResource();
	//SetCameraID(camera);
	//m_pSharedMemory->GetSharedResource(camera)->SetPosition(Vector3(3, 9, -50));

	//// Create Box with normal map texture
	//id = m_pSharedMemory->CreateSharedResource();
	//CreateMesh(id, "Box.x", "tile010.jpg", 1);
	//m_pSharedMemory->GetSharedResource(id)->SetPosition(Vector3(0, 10, -27));
	//m_pSharedMemory->GetSharedResource(id)->SetOrientation(Quaternion(0.0f, 0.7071f, 0.0f, 0.7071f));
	////SetMeshColor(id, "Box.x", D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));	// Change color to white
	//SetMeshColor(id, "Box.x", D3DXCOLOR(0.8f, 0.8f, 0.8f, 1.0f));	// Change color to gray

	//// Setup Lighting
	//SetTimeOfDay(21, 0, 0);


	///////////////////////////////////
}


HWND GraphicsCoreDLL::GetWindow()
{
	return m_hWnd;
}
void GraphicsCoreDLL::ReloadShader()
{
	if(!m_pApp)
		return;
	GCI->GetDeferredR()->ReloadShader();
}
void GraphicsCoreDLL::PrintBuffer(const char* buffer)
{
	if(!m_pApp)
		return;
	GCI->GetDeferredR()->PrintBuffer(buffer);
}
void GraphicsCoreDLL::Shutdown()
{
	// Shutdown the Graphics Core 
	if(m_pApp)
	{
		m_pApp->Shutdown();
		delete m_pApp;
		m_pApp = 0;
	}
}
void GraphicsCoreDLL::Pause()
{

}
void GraphicsCoreDLL::ProcessMessages()
{
	if(!m_pApp)	// Make sure the app has been created
		return;
	
	// Create temp message
	Mail* message = NULL;

	// process all messages in queue
	while (m_sMyMessages.size() > 0)
	{
		// get message
		m_sMyMessages.GetNextMessage(&message);

		switch(message->m_iMsg)
		{
		case msg_CreateMesh:
			{
				// Cast the message paramaters
				MessageData::MeshParam* params = (MessageData::MeshParam*)message->m_mMessageData;
				m_pApp->m_pGraphicsMgr->CreateMesh(message->m_iSender, params->m_cMeshname, params->m_bUseShadowMap); 
				break;
			}
		case msg_CreateTexMesh:
			{
				// Cast the message paramaters
				MessageData::MeshTexParam* params = (MessageData::MeshTexParam*)message->m_mMessageData;
				m_pApp->m_pGraphicsMgr->CreateMesh(message->m_iSender, params->m_cMeshname, params->m_cTexname, params->m_bUseShadowMap);
				break;
			}
		case msg_CreateTexture:
			{
				// Textures just need to cast the name as a char*
				m_pApp->m_pGraphicsMgr->CreateTexture(message->m_iSender, (char*)message->m_mMessageData->m_cCharData);
				break;
			}
		case msg_CreateAnimMesh:
			{
				// Cast the message paramaters
				MessageData::MeshParam* params = (MessageData::MeshParam*)message->m_mMessageData;
				m_pApp->m_pGraphicsMgr->CreateAnimMesh(message->m_iSender, params->m_cMeshname, params->m_bUseShadowMap); 
				break;
			}
		case msg_CreateAnimMeshTex:
			{
				// Cast teh message paramaters
				MessageData::MeshTexParam* params = (MessageData::MeshTexParam*)message->m_mMessageData;
				m_pApp->m_pGraphicsMgr->CreateAnimMesh(message->m_iSender, params->m_cMeshname, params->m_cTexname, params->m_bUseShadowMap);
				break;
			}
		case msg_CreateFontText:
			{
				// Cast the message paramaters
				MessageData::FontParam* params = (MessageData::FontParam*)message->m_mMessageData;
				//if(params->pData)
				//	m_pApp->m_pGraphicsMgr->CreateFont(message->m_iSender, params->fontType, params->text, params->pData);
				//else
				m_pApp->m_pGraphicsMgr->CreateFont(message->m_iSender, params->m_cFontType, params->m_cText);
				break;
			}
		case msg_RemoveMesh:
			{
				// Cast the message paramaters
				MessageData::MeshParam* params = (MessageData::MeshParam*)message->m_mMessageData;
				m_pApp->m_pGraphicsMgr->RemoveMesh(params->m_cMeshname, message->m_iSender);
				break;
			}
		case msg_RemoveTexture:
			{
				// Testures just need to cast the name as a char*
				m_pApp->m_pGraphicsMgr->RemoveTexture((char*)message->m_mMessageData->m_cCharData, message->m_iSender);
				break;
			}
		case msg_RemoveAnimMesh:
			{
				// Cast the message paramaters
				MessageData::MeshParam* params = (MessageData::MeshParam*)message->m_mMessageData;
				m_pApp->m_pGraphicsMgr->RemoveAnimMesh(params->m_cMeshname, message->m_iSender);
				break;
			}
		case msg_RemoveFontText:
			{
				// Cast the message paramaters
				MessageData::FontParam* params = (MessageData::FontParam*)message->m_mMessageData;
				m_pApp->m_pGraphicsMgr->RemoveFont(params->m_cFontType, message->m_iSender);
				break;
			}
		case msg_UseDOF:
			{
				GCI->GetDeferredR()->UseDOF((bool)message->m_mMessageData->m_iIntData[0]);
				break;
			}
		case msg_UseSSAO:
			{
				GCI->GetDeferredR()->UseSSAO((bool)message->m_mMessageData->m_iIntData[0]);
				break;
			}
		case msg_UseHDR:
			{
				GCI->GetDeferredR()->UseHDR((bool)message->m_mMessageData->m_iIntData[0]);
				break;
			}
		case msg_UseShadowMap:
			{
				GCI->GetDeferredR()->UseShadowMap((bool)message->m_mMessageData->m_iIntData[0]);
				break;
			}
		case msg_SetTimeOfDay:
			{
				int* time = (int*)(message->m_mMessageData);
				GCI->GetDeferredR()->m_Light.SetTime(time[0], time[1], time[2]);
				break;
			}
		case msg_DisplayFPS:
			{
				m_pApp->DisplayFPS( (bool)message->m_mMessageData->m_iIntData[0] );
				break;
			}
		case msg_DisplayNumObjs:
			{
				m_pApp->DisplayNumObjs( (bool)message->m_mMessageData->m_iIntData[0] );
				break;
			}
		case msg_Anim_Play:
			{
				GCI->GetAnimationManager()->PlayChannel(message->m_iSender, message->m_mMessageData->m_iIntData[0]);
				break;
			}
		case msg_Anim_Pause:
			{
				GCI->GetAnimationManager()->PauseChannel(message->m_iSender, message->m_mMessageData->m_iIntData[0]);
				break;
			}
		case msg_Anim_Set:
			{
				auto aData = message->m_mMessageData->m_mAnimTrackData;
				if(aData.m_cAnimTrack != "")
					GCI->GetAnimationManager()->SetAnimationToChannel(message->m_iSender, aData.channel, aData.m_cAnimTrack, aData.weight, aData.speed, aData.priority);
				else
					GCI->GetAnimationManager()->SetAnimationToChannel(message->m_iSender, aData.channel, aData.track, aData.weight, aData.speed, aData.priority);
				break;
			}
		case msg_SetCameraID:
			{
				m_pApp->m_iCameraID = message->m_iSender;
				break;
			}
		case msg_CreateBillboard:	// Instancing
			{
				auto bData = message->m_mMessageData->m_mMeshData;
				m_pApp->m_pGraphicsMgr->CreateBillboard(message->m_iSender, (char*)bData.m_cMeshname, bData.m_bUseShadowMap);
				break;
			}
		case msg_RemoveBillboard:
			{
				m_pApp->m_pGraphicsMgr->RemoveBillboard(message->m_mMessageData->m_cCharData, message->m_iSender);
				break;
			}
		case msg_CreateParticle:	// Particles
			{
				GCI->GetParticleManager()->CreateParticle(message->m_iSender, message->m_mMessageData->m_cCharData, 0);
				break;
			}
		case msg_RemoveParticle:
			{
				GCI->GetParticleManager()->RemoveParticle(message->m_iSender);
				break;
			}
		case msg_SetMeshColor:		// Set Color
			{
				auto color = message->m_mMessageData->m_mColorData.m_fColor;
				m_pApp->m_pGraphicsMgr->SetMeshColor(message->m_iSender, message->m_mMessageData->m_mColorData.m_cResourceName, D3DXCOLOR(color[0], color[1], color[2], color[3])); 
				break;
			}
		case msg_SetTextColor:
			{
				auto color = message->m_mMessageData->m_mColorData.m_fColor;
				m_pApp->m_pGraphicsMgr->SetTextureColor(message->m_iSender, message->m_mMessageData->m_mColorData.m_cResourceName, D3DXCOLOR(color[0], color[1], color[2], color[3])); 
				break;
			}
		case msg_SetAnimMeshColor:
			{
				auto color = message->m_mMessageData->m_mColorData.m_fColor;
				m_pApp->m_pGraphicsMgr->SetAnimMeshColor(message->m_iSender, message->m_mMessageData->m_mColorData.m_cResourceName, D3DXCOLOR(color[0], color[1], color[2], color[3])); 
				break;
			}
		case msg_SetFontColor:
			{
				auto color = message->m_mMessageData->m_mColorData.m_fColor;
				m_pApp->m_pGraphicsMgr->SetFontColor(message->m_iSender, message->m_mMessageData->m_mColorData.m_cResourceName, D3DXCOLOR(color[0], color[1], color[2], color[3])); 
				break;
			}
		case msg_SetBillboardColor:
			{
				auto color = message->m_mMessageData->m_mColorData.m_fColor;
				m_pApp->m_pGraphicsMgr->SetBillboardColor(message->m_iSender, message->m_mMessageData->m_mColorData.m_cResourceName, D3DXCOLOR(color[0], color[1], color[2], color[3])); 
				break;
			}
		case msg_ClearScene:
			{
				m_pApp->m_pGraphicsMgr->ClearScene();
				break;
			}
		case msg_CheckQuadPick:
			{
				// This has been moved to update
				break;
			}
		case msg_ScaleMesh:
			{
				auto sData = message->m_mMessageData->m_mScaleData;
				m_pApp->m_pGraphicsMgr->SetMeshScale(message->m_iSender, sData.m_cResourceName, D3DXVECTOR3(sData.m_fScale[0], sData.m_fScale[1], sData.m_fScale[2]));
				break;
			}
		case msg_ScaleTexture:
			{
				auto sData = message->m_mMessageData->m_mScaleData;
				m_pApp->m_pGraphicsMgr->SetTextureScale(message->m_iSender, sData.m_cResourceName, D3DXVECTOR3(sData.m_fScale[0], sData.m_fScale[1], sData.m_fScale[2]));
				break;
			}
		case msg_ScaleAnimMesh:
			{
				auto sData = message->m_mMessageData->m_mScaleData;
				m_pApp->m_pGraphicsMgr->SetAnimMeshScale(message->m_iSender, sData.m_cResourceName, D3DXVECTOR3(sData.m_fScale[0], sData.m_fScale[1], sData.m_fScale[2]));
				break;
			}
		case msg_ScaleBillboard:
			{
				auto sData = message->m_mMessageData->m_mScaleData;
				m_pApp->m_pGraphicsMgr->SetBillboardScale(message->m_iSender, sData.m_cResourceName, D3DXVECTOR3(sData.m_fScale[0], sData.m_fScale[1], sData.m_fScale[2]));
				break;
			}
		default:
			{
				//std::cout << "Error Message not recognized." << std::endl;
				break;
			}
		};

		delete message;
	}	
}

HRESULT CreateGraphicsObject(HINSTANCE hDLL, IGraphicsCore **pInterface)
{
	//std::cout << "          Entering GraphicsCoreDLL::CreateGraphicsObject" << std::endl;
	if(!*pInterface)
	{
		//std::cout << "               Creating a new GraphicsCoreDLL Object" << std::endl;
		*pInterface = new GraphicsCoreDLL();
		//std::cout << "          Leaving GraphicsCoreDLL::CreateGraphicsObject" << std::endl;
		return 1;
	}

	//std::cout << "               Failed to Create new GraphicsCoreDLL Object" << std::endl;
	//std::cout << "          Leaving GraphicsCoreDLL::CreateGraphicsObject" << std::endl;
	return 0;
}

HRESULT ReleaseGraphicsObject(IGraphicsCore **pInterface)
{
	//std::cout << "          Entering GraphicsCoreDLL::ReleaseGraphicsObject" << std::endl;
	if(!*pInterface)
	{
	//	std::cout << "               Failed to Release, Object doesnt exist" << std::endl;
	//	std::cout << "          Leaving GraphicsCoreDLL::ReleaseGraphicsObject" << std::endl;
		return 0;
	}

	//std::cout << "               Deleting Resource in GraphicsCoreDLL" << std::endl;
	delete *pInterface;
	*pInterface = NULL;
	//std::cout << "          Leaving GraphicsCoreDLL::ReleaseAIObject" << std::endl;
	return 1;
}


// Create Methods
void GraphicsCoreDLL::CreateMesh(int id, std::string meshName, bool shadowMap)
{
	m_pApp->m_pGraphicsMgr->CreateMesh(id, meshName, shadowMap);
}
void GraphicsCoreDLL::CreateMesh(int id, std::string meshName, std::string texName, bool shadowMap)
{
	m_pApp->m_pGraphicsMgr->CreateMesh(id, meshName, texName, shadowMap);
}
void GraphicsCoreDLL::CreateTexture(int id, std::string texName)
{
	m_pApp->m_pGraphicsMgr->CreateTexture(id, texName);
}
void GraphicsCoreDLL::CreateTexture(int id, std::string texName, D3DXCOLOR alpha)
{
	m_pApp->m_pGraphicsMgr->CreateTexture(id, texName, alpha);
}
void GraphicsCoreDLL::CreateFont(int id, char* font, char* text)
{
	m_pApp->m_pGraphicsMgr->CreateFont(id, font, text);
}
void GraphicsCoreDLL::CreateFont(int id, char* font, char* text, float* pData)
{
	m_pApp->m_pGraphicsMgr->CreateFont(id, font, text, pData);
}
void GraphicsCoreDLL::CreateAnimMesh(int id, char* animMeshName, bool shadosMap)
{
	m_pApp->m_pGraphicsMgr->CreateAnimMesh(id, animMeshName, shadosMap);
}
void GraphicsCoreDLL::CreateAnimMesh(int id, char* animMeshName, char* texName, bool shadowMap)
{
	m_pApp->m_pGraphicsMgr->CreateAnimMesh(id, animMeshName, texName, shadowMap);
}
void GraphicsCoreDLL::CreateBillboard(int id, char* texName, bool shadowMap)
{
	m_pApp->m_pGraphicsMgr->CreateBillboard(id, texName, shadowMap);
}
void GraphicsCoreDLL::CreateParticle(int id, char* particle, void* param)
{
	GCI->GetParticleManager()->CreateParticle(id, particle, param);
}

// Delete Methods
void GraphicsCoreDLL::RemoveMesh(int id, char* meshName)
{
	m_pApp->m_pGraphicsMgr->RemoveMesh(meshName, id);
}
void GraphicsCoreDLL::RemoveTextrue(int id, char* textureName)
{
	m_pApp->m_pGraphicsMgr->RemoveTexture(textureName, id);
}
void GraphicsCoreDLL::RemoveFont(int id, char* font)
{
	m_pApp->m_pGraphicsMgr->RemoveFont(font, id);
}
void GraphicsCoreDLL::RemoveAnimMesh(int id, char* animMeshName)
{
	m_pApp->m_pGraphicsMgr->RemoveAnimMesh(animMeshName, id);
}
void GraphicsCoreDLL::RemoveBillboard(int id, char* texName)
{
	m_pApp->m_pGraphicsMgr->RemoveBillboard(texName, id);
}
void GraphicsCoreDLL::RemoveParticle(int id)
{
	GCI->GetParticleManager()->RemoveParticle(id);
}


// Set Color
void GraphicsCoreDLL::SetMeshColor(int id, char* meshName, D3DXCOLOR color)
{
	m_pApp->m_pGraphicsMgr->SetMeshColor(id, meshName, color);
}
void GraphicsCoreDLL::SetTextureColor(int id, char* texName, D3DXCOLOR color)
{
	m_pApp->m_pGraphicsMgr->SetTextureColor(id, texName, color);
}
void GraphicsCoreDLL::SetFontColor(int id, char* fontName, D3DXCOLOR color)
{
	m_pApp->m_pGraphicsMgr->SetFontColor(id, fontName, color);
}
void GraphicsCoreDLL::SetAnimMeshColor(int id, char* animMeshName, D3DXCOLOR color)
{
	m_pApp->m_pGraphicsMgr->SetAnimMeshColor(id, animMeshName, color);
}
void GraphicsCoreDLL::SetBillboardColor(int id, char* texture, D3DXCOLOR color)
{
	m_pApp->m_pGraphicsMgr->SetBillboardColor(id, texture, color);
}

// Set Scale
void GraphicsCoreDLL::SetMeshScale(int id, char* meshName, D3DXVECTOR3 scale)
{
	m_pApp->m_pGraphicsMgr->SetMeshScale(id, meshName, scale);
}
void GraphicsCoreDLL::SetTextureScale(int id, char* texName, D3DXVECTOR3 scale)
{
	m_pApp->m_pGraphicsMgr->SetTextureScale(id, texName, scale);
}
void GraphicsCoreDLL::SetFontScale(int id, char* fontName, D3DXVECTOR3 scale)
{
	m_pApp->m_pGraphicsMgr->SetFontScale(id, fontName, scale);
}
void GraphicsCoreDLL::SetAnimMeshScale(int id, char* animMeshName, D3DXVECTOR3 scale)
{
	m_pApp->m_pGraphicsMgr->SetAnimMeshScale(id, animMeshName, scale);
}
void GraphicsCoreDLL::SetBillboardScale(int id, char* texName, D3DXVECTOR3 scale)
{
	m_pApp->m_pGraphicsMgr->SetBillboardScale(id, texName, scale);
}

// Get Scale
void GraphicsCoreDLL::GetMeshScale(int id, char* meshName, D3DXVECTOR3& scale)
{
	if(!m_pApp)
		return;
	m_pApp->m_pGraphicsMgr->GetMeshScale(id, meshName, scale);
}

// Utility Methods
void GraphicsCoreDLL::SetCameraID(int id)
{
	m_pApp->m_iCameraID = id;
}
void GraphicsCoreDLL::SetCameraPos(void* viewPos)
{
	GCI->GetCamera()->SetPos((D3DXVECTOR3*)(viewPos));
}
void GraphicsCoreDLL::SetCameraView(void* viewMat)
{
	GCI->GetCamera()->SetView((D3DXMATRIX*)(viewMat));
}
void GraphicsCoreDLL::SetCameraViewByCopy(void* viewMat)
{
	GCI->GetCamera()->SetView( *((D3DXMATRIX*)(viewMat)) );
}
void GraphicsCoreDLL::SetCameraGlareType(int glareType)
{
	GCI->GetDeferredR()->SetGlareType((EGlareType)glareType);
}
void GraphicsCoreDLL::SetTimeOfDay(int hour, int min, int sec)
{
	GCI->GetDeferredR()->m_Light.SetTime(hour, min, sec);
}
void GraphicsCoreDLL::DisplayFPS(bool enable)
{
	m_pApp->DisplayFPS(enable);
}

float GraphicsCoreDLL::GetFPS()
{
	return m_pApp->GetFPS();
}

void GraphicsCoreDLL::DisplayNumObjs(bool enable)
{
	m_pApp->DisplayNumObjs(enable);
}
void GraphicsCoreDLL::UseDOF(bool enable)
{
	GCI->GetDeferredR()->UseDOF(enable);
}
void GraphicsCoreDLL::UseSSAO(bool enable)
{
	GCI->GetDeferredR()->UseSSAO(enable);
}
void GraphicsCoreDLL::UseHDR(bool enable)
{
	GCI->GetDeferredR()->UseHDR(enable);
}
void GraphicsCoreDLL::UseShadowMap(bool enable)
{
	GCI->GetDeferredR()->UseShadowMap(enable);
}
void GraphicsCoreDLL::UseDistortion(bool enable)
{
	GCI->GetDeferredR()->UseHeatHaze(enable);
}
void GraphicsCoreDLL::UseBlur(bool enable)
{
	GCI->GetDeferredR()->UseBlur(enable);
}
void GraphicsCoreDLL::UseDecals(bool enable)
{
	GCI->GetDeferredR()->UseDecals(enable);
}
void GraphicsCoreDLL::ClearScene()
{
	m_pApp->m_pGraphicsMgr->ClearScene();
}
void GraphicsCoreDLL::CheckQuadPicking(int hitIDs[], int& count)	// Will fill the array with picked IDs and set the count that were picked
{
	m_pApp->m_pGraphicsMgr->CheckQuadPicking(hitIDs, count);
}
void GraphicsCoreDLL::GetImageInfo(const char* texName, D3DXIMAGE_INFO& info)
{
	auto texList = &GCI->GetTextureManager()->TextureList;
	if(texList->find(texName) != texList->end())	// Make sure the texture is loaded first
		info = (*texList)[texName]->ImageInfo;
}
void GraphicsCoreDLL::GetMeshBV(char* meshName, Vector3& halfExtent)
{
	auto meshList = GCI->GetMeshManager();
	halfExtent = meshList->GetBVHalfExtent(meshName);
}

// Must be in the same folder as the mesh. 
//This will also auto load normal and spec maps with the same base name
void GraphicsCoreDLL::SetDefaultMeshTexture(char* meshName, char* texName)
{
	if( GCI->GetMeshManager()->HasMesh(meshName) )
		GCI->GetMeshManager()->GetMesh(meshName)->SetTextue(texName);
}
void GraphicsCoreDLL::SetDefaultMeshLightAttributes(char* meshName, float diffuseCoeff, float phongCoeff, float phongExp)
{
	if( GCI->GetMeshManager()->HasMesh(meshName) )
		GCI->GetMeshManager()->GetMesh(meshName)->SetLightAttributes(diffuseCoeff, phongCoeff, phongExp);
}

// Lighting controlers
void GraphicsCoreDLL::AddLight(int& returnID, D3DXVECTOR4 pos, D3DXVECTOR4 intensity, int power)
{
	returnID = GCI->GetDeferredR()->AddLight(pos, intensity, power);
}
void GraphicsCoreDLL::ChangeLightPos(int id, D3DXVECTOR4 pos)
{
	GCI->GetDeferredR()->ChangeLightPos(id, pos);
}
void GraphicsCoreDLL::ChangeLightPower(int id, int power)
{
	GCI->GetDeferredR()->ChangeLightPower(id, power);
}
void GraphicsCoreDLL::RemoveLight(int id)
{
	GCI->GetDeferredR()->RemoveLight(id);
}

void GraphicsCoreDLL::CreateDistortionMesh(int sharedID, char* meshName, int groupID)
{
	GCI->GetDistortionManager()->CreateMesh(sharedID, meshName, groupID);
}
void GraphicsCoreDLL::RemoveDistortionMesh(int sharedID, char* meshName)
{
	GCI->GetDistortionManager()->RemoveMesh(meshName, sharedID);
}
void GraphicsCoreDLL::SetDistortOffset(int groupID, D3DXVECTOR2 *offset1, D3DXVECTOR2 *offset2)
{
	GCI->GetDistortionManager()->SetDistortOffset(groupID, offset1, offset2);
}
void GraphicsCoreDLL::SetDistortSpeed(int groupID, D3DXVECTOR2 *speed1, D3DXVECTOR2 *speed2)
{
	GCI->GetDistortionManager()->SetDistortSpeed(groupID, speed1, speed2);
}
void GraphicsCoreDLL::SetDistortBump(int groupID, float bump1, float bump2)
{
	GCI->GetDistortionManager()->SetDistortBump(groupID, bump1, bump2);
}
void GraphicsCoreDLL::SetDistortScale(int groupID, float scale1, float scale2)
{
	GCI->GetDistortionManager()->SetDistortScale(groupID, scale1, scale2);
}
void GraphicsCoreDLL::SetDistortIntensity(int groupID, float intensity1, float intensity2)
{
	GCI->GetDistortionManager()->SetDistortIntensity(groupID, intensity1, intensity2);
}
void GraphicsCoreDLL::SetDistortionMeshScale(int id, char* meshName, int groupID, D3DXVECTOR3 scale)
{
	BaseObjectPtr obj = GCI->GetDistortionManager()->GetObject(id, meshName, groupID);
	if(obj)
		obj->m_Tranform.m_Scale = scale;
}

void GraphicsCoreDLL::SetBlurScale(float scale)
{
	GCI->GetDeferredR()->SetBlurScale(scale);
}

void GraphicsCoreDLL::AddDecal(int& returnID, int SharedID, char*  meshName, char* textureName, bool isDynamic, bool lockToObject, float maxAge,
							   D3DXVECTOR3 pos, D3DXVECTOR3 target, D3DXVECTOR3 up)
{
	if(isDynamic)
	{
		returnID = GCI->GetDecalManager()->AddDynamicDecal(GCI->GetTextureManager()->TextureList[textureName]->texture,
												SharedID, meshName, pos, target, lockToObject, maxAge, up);
		return;
	}
	else
		returnID = GCI->GetDecalManager()->AddStaticDecal(GCI->GetTextureManager()->TextureList[textureName]->texture,
												SharedID, meshName, pos, target, lockToObject, up);
}

void GraphicsCoreDLL::AddDecal(int& returnID, int iSharedID, char*  meshName, char* textureName, bool isDynamic, bool lockToObject, float maxAge,
								D3DXVECTOR3 target, D3DXVECTOR3 normal, float size, D3DXVECTOR3 up)
{
	D3DXVECTOR3 pos;
	D3DXVec3Scale(&pos, &normal, size);
	D3DXVec3Add(&pos, &pos, &target);
	if(isDynamic)
	{
		returnID = GCI->GetDecalManager()->AddDynamicDecal(GCI->GetTextureManager()->TextureList[textureName]->texture,
												iSharedID, meshName, pos, target, lockToObject, maxAge, up);
		return;
	}
	else
		returnID = GCI->GetDecalManager()->AddStaticDecal(GCI->GetTextureManager()->TextureList[textureName]->texture,
												iSharedID, meshName, pos, target, lockToObject, up);
}

bool GraphicsCoreDLL::SetDecalPosition(UINT id, bool lookInDynamic, D3DXVECTOR3 pos)
{
	if(lookInDynamic)
		return GCI->GetDecalManager()->SetDynamicDecalPosition(id, pos);
	else
		return GCI->GetDecalManager()->SetStaticDecalPosition(id, pos);
}

void GraphicsCoreDLL::RemoveDecalsByMesh(char* MeshName)
{
	GCI->GetDecalManager()->RemoveDecalsByMeshName(MeshName);
}

void GraphicsCoreDLL::AddBolt(int &ID, int groupID, int numSegments, std::vector<int> *additionalGroups)
{
	GCI->GetElectricityManager()->AddBolt(ID, groupID, numSegments, additionalGroups);
}

bool GraphicsCoreDLL::RemoveBolt(int ID)
{
	return GCI->GetElectricityManager()->RemoveBolt(ID);
}

void GraphicsCoreDLL::RemoveBoltGroup(int groupID)
{
	GCI->GetElectricityManager()->RemoveBoltGroup(groupID);
}

void GraphicsCoreDLL::AddBoltObject(int &ID, D3DXVECTOR3 startPos, D3DXVECTOR3 endPos, float boltSize, float ArcAmount, float ArcMidpoint,
		UINT selectionType, int selectionID, D3DXVECTOR4 color)
{
	GCI->GetElectricityManager()->AddBoltObject(ID, startPos, endPos, boltSize, ArcAmount, ArcMidpoint, selectionType, selectionID, color);
}

bool GraphicsCoreDLL::RemoveBoltObject(int ID)
{
	return GCI->GetElectricityManager()->RemoveBoltObject(ID);
}

bool GraphicsCoreDLL::SetBoltObjPositions(int ID, D3DXVECTOR3 startPos, D3DXVECTOR3 endPos)
{
	return GCI->GetElectricityManager()->SetBoltObjPositions(ID, startPos, endPos);
}

bool GraphicsCoreDLL::SetBoltObjArcScale(int ID, float scale)
{
	return GCI->GetElectricityManager()->SetBoltObjArcScale(ID, scale);
}

bool GraphicsCoreDLL::SetBoltObjThickness(int ID, float thickness)
{
	return GCI->GetElectricityManager()->SetBoltObjThickness(ID, thickness);
}

bool GraphicsCoreDLL::SetBoltObjMidpoint(int ID, float midpoint)
{
	return GCI->GetElectricityManager()->SetBoltObjMidpoint(ID, midpoint);
}

bool GraphicsCoreDLL::SetBoltObjRandOffset(int ID, int offset)
{
	return GCI->GetElectricityManager()->SetBoltObjRandOffset(ID, offset);
}

bool GraphicsCoreDLL::SetBoltObjSelectionType(int ID, UINT selectionType)
{
	return GCI->GetElectricityManager()->SetBoltObjSelectionType(ID, selectionType);
}

bool GraphicsCoreDLL::SetBoltObjSelectionID(int ID, int selectionID)
{
	return GCI->GetElectricityManager()->SetBoltObjSelectionID(ID, selectionID);
}

bool GraphicsCoreDLL::SetBoltObjVisibility(int ID, bool visible)
{
	return GCI->GetElectricityManager()->SetBoltObjVisibility(ID, visible);
}

bool GraphicsCoreDLL::SetBoltObjColor(int ID, D3DXVECTOR4 color)
{
	return GCI->GetElectricityManager()->SetBoltObjColor(ID, color);
}

// Terrain Morphing
void GraphicsCoreDLL::InitTerrain(unsigned int rows, unsigned int cols, unsigned int sizeZ, unsigned int sizeX, float texScale)
{
	auto DSX = GCI->GetDeferredR();
	if(!DSX->m_pTerrain)
	{
		DSX->m_pTerrain = new Terrain;
		DSX->m_pTerrain->Init(rows, cols, sizeZ, sizeX, texScale);
	}
}
void GraphicsCoreDLL::LoadTerrain(char* path, char* fileName)
{
	auto DSX = GCI->GetDeferredR();
	if(!DSX->m_pTerrain)				// Make sure the terrain has been created before loading it
		DSX->m_pTerrain = new Terrain;
	DSX->m_pTerrain->Load(path, fileName);
}
void GraphicsCoreDLL::SaveTerrain(char* path, char* fileName)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->Save(path, fileName);
}
void GraphicsCoreDLL::ResetTerrain()
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->Reset();
}
void GraphicsCoreDLL::RemoveTerrain()
{
	auto DSX = GCI->GetDeferredR();
	if(DSX->m_pTerrain)
	{
		delete (DSX->m_pTerrain);
		(DSX->m_pTerrain) = 0;
	}
}
void GraphicsCoreDLL::SetBrush(float* radius)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->SetBrush(radius);
}
void GraphicsCoreDLL::MorphUp(float increment)		// increment [0, 1]
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->MorphUp(increment);
}
void GraphicsCoreDLL::MorphDown(float increment)	// increment [0, 1]
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->MorphDown(increment);
}
void GraphicsCoreDLL::Smooth(float scale)			// scale [0, 1]
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->Smooth(scale);
}
void GraphicsCoreDLL::SmoothCenter(float scale)		// scale [0, 1]
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->SmoothCenter(scale);
}
void GraphicsCoreDLL::UpdateBrush()
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->UpdateBrush();
}
int GraphicsCoreDLL::GetNumVerts()
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		return pTerrain->GetNumVerts();
	else
		return 0;
}
int GraphicsCoreDLL::GetNumIndices()
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		return pTerrain->GetNumIndices();
	else
		return 0;
}
void GraphicsCoreDLL::GetTerrainVerts(Vector3 verts[])
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->GetVerts(verts);
}
void GraphicsCoreDLL::GetTerrainIndices(WORD indices[])
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->GetIndices(indices);
}
void GraphicsCoreDLL::GetTerrainSize(Vector3& size)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->GetSize(size);
}
void GraphicsCoreDLL::GetTerrainNumRowsCols(int& rows, int& cols)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->GetNumRowsCols(rows, cols);
}

// Terrain Painting
void GraphicsCoreDLL::SetTerrainTex1(char* texName)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->SetTexture1(texName);
}
void GraphicsCoreDLL::SetTerrainTex2(char* texName)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->SetTexture2(texName);
}
void GraphicsCoreDLL::SetTerrainTex3(char* texName)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->SetTexture3(texName);
}
void GraphicsCoreDLL::PaintTex1(float increment)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->PaintTex1(0, increment);
}
void GraphicsCoreDLL::PaintTex2(float increment)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->PaintTex2(0, increment);
}
void GraphicsCoreDLL::PaintTex3(float increment)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->PaintTex3(0, increment);
}
void GraphicsCoreDLL::GetTexture(int tex, char* texName)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
	{
		std::string result;
		pTerrain->GetTextureName(tex, result);
		strcpy(texName, &result[0]);
	}
}
// Cusom Terrain Coloring
void GraphicsCoreDLL::PaintCustomTex1(float increment)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->PaintTex1(1, increment);
}
void GraphicsCoreDLL::PaintCustomTex2(float increment)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->PaintTex2(1, increment);
}
void GraphicsCoreDLL::PaintCustomTex3(float increment)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->PaintTex3(1, increment);
}
void GraphicsCoreDLL::GetColorAt(Vector3 worldPos, D3DXCOLOR& color)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		color = pTerrain->GetColorAt(worldPos);
}
void GraphicsCoreDLL::RenderCustomTextue(bool render)
{
	auto pTerrain = GCI->GetDeferredR()->m_pTerrain;
	if(pTerrain)
		pTerrain->SetCustomRenderTex(render);
}

// Selecting Objects
void GraphicsCoreDLL::GetSelectedObj(char* name, int& id)
{
	if(!m_pApp)
		return;
	std::string result;
	m_pApp->m_pGraphicsMgr->GetSelectedMesh(result, id);
	if(id >= 0)
		strcpy(name, &result[0]);
}

// Manual Load/Unload Texture Calls
void GraphicsCoreDLL::LoadTexture(const char* textureName)
{
	if(!m_pApp)
		return;
	GCI->GetTextureManager()->LoadTexture(textureName);
}
void GraphicsCoreDLL::LoadTexture(const char* textureName, D3DXCOLOR alpha)
{
	if(!m_pApp)
		return;
	GCI->GetTextureManager()->LoadTexture(textureName, alpha);
}
void GraphicsCoreDLL::UnloadTexture(const char* textureName)
{
	if(!m_pApp)
		return;
	GCI->GetTextureManager()->UnloadTexture(textureName);
}

Vector2 GraphicsCoreDLL::GetScreenSize()
{
	if(!m_pApp)
		return Vector2::Zero();

	auto dirX = GCI->GetDirectX();
	return Vector2(SCREEN_WIDTH, SCREEN_HEIGHT);	// All sizes are automatically scaled based on starting screen size.
	//return Vector2(dirX->GetScreenWidth(), dirX->GetScreenHeight());
}
void GraphicsCoreDLL::SetResizeCallBack(void (*funcP)())
{
	if(m_pApp)
		m_pApp->SetResizeCallBack( funcP );
}
bool GraphicsCoreDLL::IsDeviceLost()
{
	if(!m_pApp)
		return true;
	return m_bDeviceLost;
}