#pragma once
#include <GraphicsDLLInterface.h>
#include "BaseApp.h"

class SpriteFont;

class GraphicsCoreDLL : public IGraphicsCore
{
	BaseApp*	m_pApp;
	bool		m_bDeviceLost;

public:
	GraphicsCoreDLL(void);
	~GraphicsCoreDLL(void);

	// Exposing Methods ;)
	// Create Methods
	void CreateMesh(int id, std::string meshName, bool shadowMap);
	void CreateMesh(int id, std::string meshName, std::string texName, bool shadowMap);
	void CreateTexture(int id, std::string texName);
	void CreateTexture(int id, std::string texName, D3DXCOLOR alpha);
	void CreateFont(int id, char* font, char* text);
	void CreateFont(int id, char* font, char* text, float* pData);
	void CreateAnimMesh(int id, char* animMeshName, bool shadosMap);
	void CreateAnimMesh(int id, char* animMeshName, char* texName, bool shadowMap);
	void CreateBillboard(int id, char* texName, bool shadowMap);
	void CreateParticle(int id, char* particle, void* param);

	// Delete Methods
	void RemoveMesh(int id, char* meshName);
	void RemoveTextrue(int id, char* textureName);
	void RemoveFont(int id, char* font);
	void RemoveAnimMesh(int id, char* animMeshName);
	void RemoveBillboard(int id, char* texName);
	void RemoveParticle(int id);

	// Set Color
	void SetMeshColor(int id, char* meshName, D3DXCOLOR color);
	void SetTextureColor(int id, char* texName, D3DXCOLOR color);
	void SetFontColor(int id, char* fontName, D3DXCOLOR color);
	void SetAnimMeshColor(int id, char* animMeshName, D3DXCOLOR color);
	void SetBillboardColor(int id, char* texture, D3DXCOLOR color);

	// Set Scale
	void SetMeshScale(int id, char* meshName, D3DXVECTOR3 scale);
	void SetTextureScale(int id, char* texName, D3DXVECTOR3 scale);
	void SetFontScale(int id, char* fontName, D3DXVECTOR3 scale);
	void SetAnimMeshScale(int id, char* animMeshName, D3DXVECTOR3 scale);
	void SetBillboardScale(int id, char* texName, D3DXVECTOR3 scale);

	// Get Scale
	void GetMeshScale(int id, char* meshName, D3DXVECTOR3& scale);

	// Utility Methods
	void SetCameraID(int id);
	void SetCameraPos(void* viewPos);
	void SetCameraView(void* viewMat);
	void SetCameraViewByCopy(void* viewMat);
	void SetCameraGlareType(int glareType);
	void SetTimeOfDay(int hour, int min, int sec);
	void DisplayFPS(bool enable);
	float GetFPS();
	void DisplayNumObjs(bool enable);
	void UseDOF(bool enable);
	void UseSSAO(bool enable);
	void UseHDR(bool enable);
	void UseShadowMap(bool enable);
	void UseDistortion(bool enable);
	void UseBlur(bool enable);
	void UseDecals(bool enable);
	void ClearScene();
	void CheckQuadPicking(int hitIDs[], int& count);	// Will fill the array with picked IDs and set the count that were picked
	void GetImageInfo(const char* texName, D3DXIMAGE_INFO& info);
	void GetMeshBV(char* meshName, Vector3& halfExtent);
	
	// Must be in the same folder as the mesh. 
	//This will also auto load normal and spec maps with the same base name
	void SetDefaultMeshTexture(char* meshName, char* texName);	
	void SetDefaultMeshLightAttributes(char* meshName, float diffuseCoeff, float phongCoeff, float phongExp);

	// Lighting controlers
	void AddLight(int& returnID, D3DXVECTOR4 pos, D3DXVECTOR4 intensity, int power);
	void ChangeLightPos(int id, D3DXVECTOR4 pos);
	void ChangeLightPower(int id, int power);
	void RemoveLight(int id);

	void CreateDistortionMesh(int sharedID, char* meshName, int groupID);
	void RemoveDistortionMesh(int sharedID, char* meshName);
	void SetDistortOffset(int groupID, D3DXVECTOR2 *offset1, D3DXVECTOR2 *offset2);
	void SetDistortSpeed(int groupID, D3DXVECTOR2 *speed1, D3DXVECTOR2 *speed2);
	void SetDistortBump(int groupID, float bump1, float bump2);
	void SetDistortScale(int groupID, float scale1, float scale2);
	void SetDistortIntensity(int groupID, float intensity1, float intensity2);
	void SetDistortionMeshScale(int id, char* meshName, int groupID, D3DXVECTOR3 scale);

	// Blur Controllers
	void SetBlurScale(float scale);

	// DecalControllers
	void AddDecal(int& returnID, int iSharedID, char*  meshName, char* textureName, bool isDynamic, bool lockToObject, float maxAge,
							D3DXVECTOR3 pos, D3DXVECTOR3 target, D3DXVECTOR3 up);
	void AddDecal(int& returnID, int iSharedID, char*  meshName, char* textureName, bool isDynamic, bool lockToObject, float maxAge,
							D3DXVECTOR3 target, D3DXVECTOR3 normal, float size, D3DXVECTOR3 up);
	bool SetDecalPosition(UINT id, bool lookInDynamic, D3DXVECTOR3 pos);
	void RemoveDecalsByMesh(char* MeshName);

	// Electricity Controllers
	void AddBolt(int &ID, int groupID, int numSegments, std::vector<int> *additionalGroups);
	bool RemoveBolt(int ID);
	void RemoveBoltGroup(int groupID);

	void AddBoltObject(int &ID, D3DXVECTOR3 startPos, D3DXVECTOR3 endPos, float boltSize, float ArcAmount, float ArcMidpoint,
		UINT selectionType, int selectionID, D3DXVECTOR4 color);
	bool RemoveBoltObject(int ID);

	bool SetBoltObjPositions(int ID, D3DXVECTOR3 startPos, D3DXVECTOR3 endPos);
	bool SetBoltObjArcScale(int ID, float scale);
	bool SetBoltObjThickness(int ID, float thickness);
	bool SetBoltObjMidpoint(int ID, float midpoint);
	bool SetBoltObjRandOffset(int ID, int offset);
	bool SetBoltObjSelectionType(int ID, UINT selectionType);
	bool SetBoltObjSelectionID(int ID, int selectionID);
	bool SetBoltObjVisibility(int ID, bool visible);
	bool SetBoltObjColor(int ID, D3DXVECTOR4 color);

	// Terrain Morphing
	void InitTerrain(unsigned int rows, unsigned int cols, unsigned int sizeZ, unsigned int sizeX, float texScale);
	void LoadTerrain(char* path, char* fileName);
	void SaveTerrain(char* path, char* fileName);
	void ResetTerrain();
	void RemoveTerrain();
	void SetBrush(float* radius);
	void MorphUp(float increment);		// increment [0, 1]
	void MorphDown(float increment);	// increment [0, 1]
	void Smooth(float scale);			// scale [0, 1]
	void SmoothCenter(float scale);		// scale [0, 1]
	void UpdateBrush();
	int  GetNumVerts();
	int  GetNumIndices();
	void GetTerrainVerts(Vector3 verts[]);
	void GetTerrainIndices(WORD indices[]);
	void GetTerrainSize(Vector3& size);
	void GetTerrainNumRowsCols(int& rows, int& cols);

	// Terrain Painting
	void SetTerrainTex1(char* texName);
	void SetTerrainTex2(char* texName);
	void SetTerrainTex3(char* texName);
	void PaintTex1(float increment);
	void PaintTex2(float increment);
	void PaintTex3(float increment);
	void GetTexture(int tex, char* texName);

	// Cusom Terrain Coloring
	void PaintCustomTex1(float increment);
	void PaintCustomTex2(float increment);
	void PaintCustomTex3(float increment);
	void GetColorAt(Vector3 worldPos, D3DXCOLOR& color);
	void RenderCustomTextue(bool render);

	// Selecting Objects
	void GetSelectedObj(char* name, int& id);

	// Manual Load/Unload Texture Calls
	void LoadTexture(const char* textureName);
	void LoadTexture(const char* textureName, D3DXCOLOR alpha);
	void UnloadTexture(const char* textureName);

	// 3D Rendering
	void BeginRender();
	
	// Rendering 2D Calls
	void Render2D(char* textureName, D3DXMATRIX transform);	// Render 2D Texture
	void Render2D(char* textureName, D3DXMATRIX transform, D3DXCOLOR color); // Render 2D Texture with colors
	
	// Batch Rendering
	void Render2D(char* textureName, D3DXMATRIX transform[], int count);
	void Render2D(char* textureName, D3DXMATRIX transform[], D3DXCOLOR color[], int count);

	void RenderString(const char* fontname, const char* text, Vector2 position, D3DXCOLOR color);
	void RenderString(const char* fontname, const char* text, Vector2 position, float depth, D3DXCOLOR color);
	void LoadFont(const char* fontName);
	void UnloadFont(const char* fontName);

	Vector2 MeasureString(const char* fontName, char* text);
	Vector2 GetScreenSize();
	void SetResizeCallBack(void (*funcP)());
	bool IsDeviceLost();

	void EndRender();

	HWND GetWindow();
	void ReloadShader();
	void PrintBuffer(const char* buffer);

public:

	// VIRTUAL OVERRIDING
	virtual void Update(float dt);
	virtual void Initialize();
	virtual void Initialize(HINSTANCE hInst, const char* windowTitle);
	virtual void Shutdown();
	virtual void Pause();
	virtual void ProcessMessages();
	int GetCorePriority() { return 6; }
};

extern "C"
{

	HRESULT CreateGraphicsObject(HINSTANCE hDLL, IGraphicsCore **pInterface);
	HRESULT ReleaseGraphicsObject(IGraphicsCore **pInterface);	
}

typedef HRESULT (*CREATEGRAPHICSOBJECT)(HINSTANCE hDLL, IGraphicsCore **pInterface);
typedef HRESULT(*RELEASEGRAPHICSOBJECT)(IGraphicsCore **pInterface);

