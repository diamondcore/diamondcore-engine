#include "BaseApp.h"
#include "GraphicsCore.h"
#include "MacroTools.h"
#include <MemoryManagement/Module.h>

BaseApp* pCurrentApp = 0;
BaseApp::BaseApp()
{
	m_hInst				= 0;
	m_hWnd				= 0;
	m_fClock			= 0.0f;
	m_fFPS				= 0.0f;
	m_fMsPF				= 0.0f;
	m_fAccumTime		= 0.0f;
	m_fNumFrames		= 0.0f;
	m_bEnableFPS		= 0;
	m_bEnableNumObjs	= 0;
	m_bEnableTimeOfDay	= 0;
	m_iCameraID			= -1;
	m_pSharedMem		= 0;
	cb_Resize			= 0;
}
BaseApp::~BaseApp()
{
	pCurrentApp		= 0;
	m_pSharedMem	= 0;
}

void BaseApp::Init(HINSTANCE hInst, const char* windowTitle)
{
	// Create the Window
	m_hInst = hInst;
	m_Title = windowTitle;
	InitWindow();

	// Set the Current App
	pCurrentApp = this;

	//GCI->Lock();
	// Initialise the Graphics Core
	GCI->Init(m_hInst, m_hWnd);

	// Set Default shader settings
	GCI->GetDeferredR()->UseShadowMap(false);
	GCI->GetDeferredR()->UseSSAO(false);
	//GCI->GetDeferredR()->UseDOF(true);
	//GCI->GetDeferredR()->UseHDR(false);

	// Initialize the Graphics Entity Manager
	m_pGraphicsMgr = new GraphicsManager;

	// Load test Font
	//int id = 0;
	//DisplayFPS( true );
	//DisplayNumObjs( true );
	//DisplayTimeOfDay( true );
	
	// Register the Graphics Manager with the Deferred Shader
	GCI->GetDeferredR()->RegisterRendering(m_pGraphicsMgr);
	//GCI->Unlock();
}
void BaseApp::Shutdown()
{
	delete m_pGraphicsMgr;

	// Shutdown the Graphics Core
	GCI->Shutdown();

	// Unregister Window
	UnregisterClass(m_Title.c_str(), m_hInst);
	m_hInst = 0;
	m_hWnd	= 0;
}

void BaseApp::Render()
{
	// Render the Graphics Manager
	m_pGraphicsMgr->BeginRender();
	m_pGraphicsMgr->EndRender();
}


void BaseApp::Update(float dt, Module* sharedMem)
{
	FPS(dt);
	GCI->SetTimeElapsed(dt);
	/********************************/
	// Update the camera
	//if(m_iCameraID > -1)
	//{
	//	auto camera = GCI->GetCamera();
	//	camera->m_Position = sharedMem->GetSharedResource(m_iCameraID)->GetPosition();
	//	//D3DXVECTOR3 dir	= sharedMem->GetSharedResource(m_iCameraID)->GetPosition();
	//	//camera->SetPosByDir(dir, dt);
	//	static D3DXVECTOR3 prevEuler(0, 0, 0);
	//	D3DXVECTOR3 euler	= sharedMem->GetSharedResource(m_iCameraID)->GetOrientation().GetYawPitchRoll();
	//	camera->SetView(euler.y-prevEuler.y, euler.x-prevEuler.x);
	//	prevEuler = euler;
	//}

	// Update the Graphics Manager
	m_pGraphicsMgr->Update(dt, sharedMem);
}
void BaseApp::FPS(float dt)
{
	m_fNumFrames += 1.0f;			// Increment the number of frames since the last Second
	m_fAccumTime += dt;				// Accumulate the time since the last Second
	m_fClock += dt;					// Total time counter

	if( m_fAccumTime >= 1.0f)
	{
		m_fFPS = m_fNumFrames;			// Sets the Frames Per Second counter

		m_fMsPF = 1000.0f/m_fFPS;	// Time that it takes to render a frame

		// Reset the counters
		m_fAccumTime = 0.0f;		
		m_fNumFrames = 0.0f;
	}
}

void BaseApp::InitWindow()
{
	WNDCLASSEX wndClass;  
	ZeroMemory(&wndClass, sizeof(wndClass));
	bool windowed = true;

	// set up the window
	wndClass.cbSize			= sizeof(WNDCLASSEX);			// size of window structure
	wndClass.lpfnWndProc	= (WNDPROC)WndProc;				// message callback
	wndClass.lpszClassName	= m_Title.c_str();				// class name
	wndClass.hInstance		= m_hInst;						// handle to the application
	wndClass.hCursor		= LoadCursor(NULL, IDC_ARROW);	// default cursor
	wndClass.hbrBackground	= (HBRUSH)(COLOR_WINDOWFRAME);	// background brush
	wndClass.style			= CS_HREDRAW | CS_VREDRAW;

	// register a new type of window
	if(!RegisterClassEx(&wndClass))
	{
		MessageBox(0, "Failed to RegisterClassEx()", "Error", MB_OK);
		PostQuitMessage(0);
	}

	// Adjust windows size to fit client dimentions
	RECT rect;
	SetRect(&rect, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	AdjustWindowRectEx(&rect, windowed ? WS_OVERLAPPEDWINDOW | WS_VISIBLE:(WS_POPUP | WS_VISIBLE), 
				FALSE, NULL);
	
	// Calculate the window's position on the screen
	int width	= GetSystemMetrics(SM_CXSCREEN);
	int height	= GetSystemMetrics(SM_CYSCREEN);
	width -= rect.right;
	height -= rect.bottom;
	width *= 0.5f;
	height *= 0.5f;

	// Create the Window
	m_hWnd = CreateWindowEx( NULL,
		m_Title.c_str(), m_Title.c_str(), 									// window class name and title
		windowed ? WS_OVERLAPPEDWINDOW | WS_VISIBLE:(WS_POPUP | WS_VISIBLE),// window style
		width, height,										// x and y coordinates
		(rect.right-rect.left), (rect.bottom-rect.top),						// width and height of window	
		NULL, NULL,															// parent window and menu
		m_hInst,															// handle to application
		NULL);
	if(!m_hWnd)	// Check for Error
	{
		MessageBox(0, "Failed to CreateWindow()", "Error", MB_OK);
		PostQuitMessage(0);
	}

	// Display the window
	ShowWindow(m_hWnd, SW_SHOW);
	UpdateWindow(m_hWnd);
}
void BaseApp::SetMemMgr(Module* sharedMem)	// Set Shared memory once
{	m_pSharedMem = sharedMem;	}
void BaseApp::SetPause(bool pause)
{
	// Set if Window is paused or not
	if(m_pSharedMem)
		m_pSharedMem->GetMemMgr()->SetIsActiveWindow(!pause);
}
void BaseApp::SetResizeCallBack(void (*funcP)())
{	cb_Resize = funcP;	}
void BaseApp::UseResizeCallBack()
{
	cb_Resize();	// Use the resize callback
}

//////////////////////////////////////////////////////////////////////////
// Windows Method
//////////////////////////////////////////////////////////////////////////
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// Attempt to handle your messages
	switch(message)
	{
	case (WM_PAINT):
		{
			InvalidateRect(hWnd,NULL,TRUE);
			break;
		}	
	case (WM_CLOSE):
		{
			DestroyWindow(hWnd);
			return 0;
		}
	case(WM_DESTROY):
		{
			PostQuitMessage(0); 
			return 0;	//break;
		}
	case (WM_SIZE):
		{
			//GCI->Lock();
			//OutputDebugString("Enter GraphicsCore::Lock() - WM_SIZE");
			// Set to FullScreen when Maximized button is pressed
			if(wParam == SIZE_MAXIMIZED)
			{
				GCI->GetDirectX()->SetFullScreen(true);		
				pCurrentApp->UseResizeCallBack();
			}
			//GCI->Unlock();
			//OutputDebugString("Enter GraphicsCore::Unlock() - WM_SIZE");
			break;
		}
	case (WM_EXITSIZEMOVE):
		{
			//GCI->Lock();
			//OutputDebugString("Enter GraphicsCore::Lock() - WM_EXITSIZEMOVE");
			// Update Device Backbuffer to new Windows size
			GCI->GetDirectX()->ChangeScreenSize();
			pCurrentApp->UseResizeCallBack();
			//GCI->Unlock();
			//OutputDebugString("Enter GraphicsCore::Unlock() - WM_EXITSIZEMOVE");
			break;
		}
	case(WM_KEYDOWN):
		{
			switch(wParam)
			{
			/*case 'F':	// Switch between FullScreen and Windowed
				{
					GCI->GetDirectX()->SetFullScreen(!GCI->GetDirectX()->IsFullScreen());	
					break;
				}*/
		/*	case VK_ESCAPE:	// If in FullScreen mode, then set to windowed
				{
					GCI->Lock();
					//OutputDebugString("Enter GraphicsCore::Lock() - VK_ESCAPE button pressed");
					if(GCI->GetDirectX()->IsFullScreen())
					{
						GCI->GetDirectX()->SetFullScreen(false);
						pCurrentApp->UseResizeCallBack();
					
					}
					else
						PostQuitMessage(0);	// If Windowed, then Exit
					GCI->Unlock();
					//OutputDebugString("Enter GraphicsCore::Unlock() - VK_ESCAPE button pressed");
					break;
				} */
			}
			break;
		}
	case (WM_ACTIVATE):
		{
			// Pause/Unpause App when window is deselected/selected
			if((wParam) == WA_INACTIVE)
			{
				if(pCurrentApp)
					pCurrentApp->SetPause(true);
			}
			else
			{
				if(pCurrentApp)
					pCurrentApp->SetPause(false);
			}
			break;
		}
	default:
		break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

void BaseApp::DisplayFPS(bool enable)
{
	if(enable == m_bEnableFPS)	// Make sure we aren't already at this setting
		return;
	m_bEnableFPS = enable;

	// Create or delete the font depending on if its enabled/disabled
	if(m_bEnableFPS)
		m_pGraphicsMgr->CreateFont(-2, "TimesNewRoman", "Frames Per Second: %0.0f", &m_fFPS);
	else
		m_pGraphicsMgr->RemoveFont("TimesNewRoman", -2);
}
void BaseApp::DisplayNumObjs(bool enable)
{
	if(enable == m_bEnableNumObjs)	// Make sure we aren't already at this setting
		return;
	m_bEnableNumObjs = enable;

	// Create or delete the font depending on if its enabled/disabled
	if(m_bEnableNumObjs)
	{
		m_pGraphicsMgr->CreateFont(-3, "TimesNewRoman", "Number Of Objects: %0.0f", GCI->GetDeferredR()->GetNumObjDrawnFloat());
		// Move position down under FPS stats
		m_pGraphicsMgr->GetFontText("TimesNewRoman", -3)->m_Tranform.m_Position = D3DXVECTOR3(0.0f, 15.0f, 0.0f);
	}
	else
		m_pGraphicsMgr->RemoveFont("TimesNewRoman", -3);
}
void BaseApp::DisplayTimeOfDay(bool enable)
{
	if(enable == m_bEnableTimeOfDay)	// Make sure we aren't already at this setting
		return;
	m_bEnableTimeOfDay = enable;

	// Create or delete the font depending on if its enabled/disabled
	if(m_bEnableTimeOfDay)
	{
		m_pGraphicsMgr->CreateFont(-4, "TimesNewRoman", "World Time: %0.0f", (float*)GCI->GetDeferredR()->m_Light.GetHours());
		// Move position down under FPS stats
		m_pGraphicsMgr->GetFontText("TimesNewRoman", -4)->m_Tranform.m_Position = D3DXVECTOR3(300.0f, 0.0f, 0.0f);
	}
	else
		m_pGraphicsMgr->RemoveFont("TimesNewRoman", -4);
}
