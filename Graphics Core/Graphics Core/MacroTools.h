#ifndef WESTLEYS_TOOLBOX
#define	WESTLEYS_TOOLBOX	

// Enable Memory Leak Detection (in Debug mode)
#if defined(DEBUG) | defined(_DEBUG)
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif

/**********************************
Outputs the File and Line when a
memory leak occurs.
***********************************/
#if defined(DEBUG) | defined(_DEBUG)
#define DEBUG_NEW new(_NORMAL_BLOCK,__FILE__, __LINE__)
#else
#define DEBUG_NEW new
#endif
//#define new DEBUG_NEW


/************************************
		Westley's Toolbox
************************************/
const float INFINITY = 3.402823466e+38F;	// FLT_MAX
const float EPSILON = 0.001f;

// Windowed Screen Width
#define SCREEN_WIDTH 1280

// Windowed Screen Height
#define SCREEN_HEIGHT 720

// Define whether to record stats or not
#define	RECORD_STATS 1

// Convert from Radians to Degrees
#define	DEG(x)	if(x){x = (180.0f/D3DX_PI) * x;}

// Convert from Degrees to Radians
#define	RAD(x)	if(x){x = (D3DX_PI/180.0f) * x;}

// Macro for a Method Pointer
// #define CALL_MEMBER(Object, pMethod)	((Object).*(pMethod))	// Broken

// Macro to check if successful
#define CHECK(x)	if(!x){return false;}

// Macro to release COM objects fast and safely
#define SAFE_RELEASE(x) if(x){x->Release(); x = 0;}

// Macro to safely delete
#define SAFE_DELETE(x)	if(x){delete(x); x = 0;}

// Macro to safely delete arrays
#define SAFE_DELETE_ARRAY(x)	if(x){delete[] x; x = 0;}

// Macro to safely shutdown
#define SAFE_SHUTDOWN(x)	if(x){x->Shutdown(); delete(x); x = 0;}

// Round function for rounding decimals Up or Down
inline int round(float i)
{				
	float sign = 0.4999999f;
	int roundDown;
	_asm{								
		mov eax, i				
		and eax, 0x80000000		
		or sign, eax			
		fld i
		fsub sign				
		fistp roundDown;				
	}					
	if((i - roundDown) > sign)
		return ++roundDown;	// Round Up
	else
		return roundDown;	// Round Down			
}

inline unsigned int approx_dist(int dx, int dy)
{
	unsigned int min, max, approx;

	if(dx < 0) dx = -dx;
	if(dy < 0) dy = -dy;
	
	if(dx < dy)
	{
		min = dx;
		max = dy;
	}
	else
	{
		min = dy;
		max = dx;
	}

	approx = (max * 1007) + (min * 441);
	if(max < (min << 4))
		approx -= (max * 40);

	// Round properly
	return ((approx + 512) >> 10);
}

inline unsigned int approx_dist(int dx, int dy, int dz)
{
	unsigned int min, max, approx;

	if(dx < 0) dx = -dx;
	if(dy < 0) dy = -dy;
	if(dz < 0) dz = -dz;
	
	if(dx < dy)
	{
		if(dx < dz)
			min = dx;
		else
			min = dz;
		if(dz > dy)
			max = dz;
		else
			max = dy;
	}
	else
	{
		if(dy < dz)
			min = dy;
		else
			min = dz;
		if(dz > dx)
			max  = dz;
		else
			max = dx;
	}

	approx = (max * 1007) + (min * 441);
	if(max < (min << 4))
		approx -= (max * 40);

	// Round properly
	return ((approx + 512) >> 10);
}


// Property Macros
/*
#define PROPERTY(t, x)	__declspec( property ( put = property__set_##x, get = property__get_##x) ) t x;\
			typedef t property__tmp_type_##x
#define READONLY_PROPERTY(t, x)	__declspec( property (get = property__get_##x)) t x;\
			typedef t property__tmp_type_##x
#define WRITEONLY_PROPERTY(t, x)	__declspec( property (put = property__set_##x)) t x;\
			typedef t property__tmp_type_##x
#define	GET(x)	property__tmp_type_##x property__get_##x()
#define	SET(x)	void property__set_##x(const property__tmp_type_##x& value)
*/
#endif	//WESTLEYS_TOOLBOX