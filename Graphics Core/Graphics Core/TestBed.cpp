#include "TestBed.h"

#define DEMO_ANIMATION 1
#define DEMO_INSTANCING 0
#define DEMO_PARTICLES 0
#define DEMO_SHADOWMAP 0


TestBed::TestBed()
{
	m_hInst		= 0;
	m_hWnd		= 0;
	m_fClock	= 0.0f;
	m_fFPS		= 0.0f;
	m_fMsPF		= 0.0f;
	m_bInstanceDemo	= 0;
}
TestBed::~TestBed()
{
}

void TestBed::Init(HINSTANCE hInst)
{
	// Create the Window
	m_hInst = hInst;
	m_Title = "Graphics Core: Test Bed";
	InitWindow();

	// Initialise the Graphics Core
	GCI->Init(m_hInst, m_hWnd);
	GCI->GetDeferredR()->UseShadowMap(DEMO_SHADOWMAP);
	GCI->GetDeferredR()->UseSSAO(false);
	//GCI->GetDeferredR()->UseDOF(true);
	//GCI->GetDeferredR()->UseHDR(false);

	// Load test Font
	if( !GCI->GetFontManager()->Load("TimesNewRoman") )
		MessageBox(m_hWnd, "Could not Load Font", "Error", MB_OK);
	D3DXMatrixIdentity(&m_IdentityMat);

	//Load TEST Textures
	GCI->GetTextureManager()->LoadTexture("Floor.bmp");

	//Set Texture Matrix for Render TEST 
	D3DXMATRIX _rotX, _rotY, _rotZ;
	D3DXMATRIX _rot;
	D3DXMATRIX _scale;
	D3DXMATRIX _pos;
	D3DXMatrixScaling(&_scale, 0.25f, 0.25f, 0.25f) ;
	D3DXMatrixRotationX( &_rotX , 0) ;
	D3DXMatrixRotationY( &_rotY , 0) ;
	D3DXMatrixRotationZ( &_rotZ , 0) ;
	_rot = _rotX * _rotY * _rotZ;
	D3DXMatrixTranslation( &_pos , 0 ,0 , 0) ;
	m_TextureEntity = ( _scale *_rot *_pos);

	//Unload TEST
	//GCI->GetTextureManager()->UnloadTexture("Floor.bmp");

	// Load Meshes
	GCI->GetMeshManager()->Load("SmallBuilding.x");
	// Set Default Mtrl
	GCI->GetMeshManager()->GetMesh("SmallBuilding.x")->SetMtrl(WHITE, WHITE, WHITE, 8.0f);

	// Create Entityies somewhere in the world
	for(int i = 0; i < 5; i++)
	{
		// Generate transform matrix
		WTW::Transform transform;
		transform.m_Position = D3DXVECTOR3(i*10.0f, 0.0f, i+20.0f);
		m_EntityList[i] = transform.GetTransform();
	}

#if DEMO_ANIMATION
	//---------------ANIMATION TESTING----------------------
	if(!GCI->GetAnimationManager()->LoadAnimatedMesh("Zombie.x"))
		MessageBox(0, "Could not load animated mesh.", "Error", MB_OK);
	GCI->GetTextureManager()->LoadTexture("zombie_1.jpg");
	GCI->GetTextureManager()->LoadTexture("zombie_2.jpg");
	int id = 0;
	GCI->GetAnimationManager()->CreateAnimController("Zombie.x",id);
	GCI->GetAnimationManager()->SetTrackAnimationSet(id, 0,10,"shamble",1.0,0.5,1.0);
	//Add a second animation which will be blended into the first based on weight
	GCI->GetAnimationManager()->SetTrackAnimationSet(id, 1,10,"attack1",0.2,1.0,1.0);
	//Play the default animation
	GCI->GetAnimationManager()->PlayTrack(id,0);
	//Play the second animation that we added
	GCI->GetAnimationManager()->PlayTrack(id,1);
	//---Will Play all tracks--
	//a_Api->Play(0);
	//--Will Stop All Tracks--
	//a_Api->Pause(0);
	D3DXMatrixTranslation(&m_AnimationMat[0], 0.0f, -3.0f, -9.0f);
	GCI->GetAnimationManager()->CreateAnimController("Zombie.x",id);
	GCI->GetAnimationManager()->SetTrackAnimationSet(id, 0,10,"shamble",1.0,0.5,1.0);
	//Add a second animation which will be blended into the first based on weight
	GCI->GetAnimationManager()->SetTrackAnimationSet(id, 1,10,"attack1",0.2,1.0,1.0);
	//Play the default animation
	GCI->GetAnimationManager()->PlayTrack(id,0);
	//Play the second animation that we added
	GCI->GetAnimationManager()->PlayTrack(id,1);
	D3DXMatrixTranslation(&m_AnimationMat[1], -3.0f, -3.0f, -9.0f);
	//--------------------------------------------------------
#endif
#if DEMO_INSTANCING
	m_bInstanceDemo = 1;
	// Create TEST instances
	if( m_bInstanceDemo )
	{
		D3DXMATRIX transforms[44];
		D3DXCOLOR colors[44];
		for(int i = 0; i < 44; i++)
		{
			// Generate transform matrix
			WTW::Transform transform;
			transform.m_Position = D3DXVECTOR3(i%11, (i/11.0f), 20.0f);

			// Create instance
			transforms[i] = transform.GetTransform();
			colors[i] = D3DXCOLOR(i/44.0f, i/44.0f, 1.0f, 1.0f);
		}
		// Create instances
		GCI->GetInstanceManager()->AddInstance("testInst", "Floor.bmp", transforms, colors, 44);
		GCI->GetInstanceManager()->AddInstance("testInst2", "Floor.bmp", transforms, colors, 44);
		GCI->GetInstanceManager()->AddInstance("testInst3", "Floor.bmp", transforms, colors, 44);
		GCI->GetInstanceManager()->AddInstance("testInst4", "Floor.bmp", transforms, colors, 44);
		GCI->GetInstanceManager()->AddInstance("testInst5", "Floor.bmp", transforms, colors, 44);
		GCI->GetInstanceManager()->AddInstance("testInst6", "Floor.bmp", transforms, colors, 44);
	
		// Set it's relative world matrix
		D3DXMATRIX W;
		D3DXMatrixTranslation(&W, -16.0f, 8.0f, 0.0f);
		GCI->GetInstanceManager()->SetWorldMat("testInst", W);
		D3DXMatrixTranslation(&W, -16.0f, -12.0f, 0.0f);
		GCI->GetInstanceManager()->SetWorldMat("testInst2", W);
		D3DXMatrixTranslation(&W, -16.0f, 4.0f, 0.0f);
		GCI->GetInstanceManager()->SetWorldMat("testInst3", W);
		D3DXMatrixTranslation(&W, -16.0f, -8.0f, 0.0f);
		GCI->GetInstanceManager()->SetWorldMat("testInst4", W);
		D3DXMatrixTranslation(&W, -16.0f, -4.0f, 0.0f);
		GCI->GetInstanceManager()->SetWorldMat("testInst5", W);
		D3DXMatrixTranslation(&W, -16.0f, 0.0f, 0.0f);
		GCI->GetInstanceManager()->SetWorldMat("testInst6", W);
	}
#endif
#if DEMO_PARTICLES
	// Test fire and smoke GPU particles
	GCI->GetParticleManager()->Init();
#endif
}
void TestBed::Shutdown()
{
	// Shutdown the Graphics Core
	GCI->Shutdown();

	// Unregister Window
	UnregisterClass(m_Title.c_str(), m_hInst);
	m_hInst = 0;
	m_hWnd	= 0;
}

void TestBed::Render()
{
	if( GCI->BeginRender() )
	{
#if DEMO_SHADOWMAP
		// Render Shadow Map
		GCI->GetDeferredR()->BeginShadowMap();
		GCI->GetMeshManager()->Render("SmallBuilding.x", m_EntityList, 5);
		GCI->GetDeferredR()->EndShadowMap();
#endif

		// Begin 3D Rendering
		GCI->Begin3DRender();
		//GCI->GetMeshManager()->Render("SmallBuilding.x", "Floor.bmp", m_EntityList, 5);	//Test overwriting texture

#if DEMO_ANIMATION
		//--------TEST RENDERANIMATION-------
		//Render with specified texture
		GCI->GetAnimationManager()->PreRender();
		//Default Rendering
		//GCI->GetAnimationManager()->RenderAnimatedMesh(0, m_AnimationMat[1]);
		GCI->GetAnimationManager()->RenderAnimatedMesh(0, "zombie_1.jpg", m_AnimationMat[0]);
		GCI->GetAnimationManager()->RenderAnimatedMesh(1, "zombie_2.jpg", m_AnimationMat[1]);
		GCI->GetAnimationManager()->PostRender();
		//------END TEST RENDER ANIMATION------
#endif

		// Begin 2D Rendering
		GCI->Begin2DRender();
		GCI->GetTextureManager()->RenderTexture("Floor.bmp", m_TextureEntity);

		// Begin Font Rendering
		GCI->GetDirectX()->GetSprite()->SetTransform(&m_IdentityMat);	// Reset the Sprite's transform
		
		// Output the frames per second
		char buff[32];
		sprintf(buff, "Frames Per Second: %i", (int)m_fFPS);
		GCI->GetFontManager()->Render(buff, 0, 0);
		ZeroMemory(buff, sizeof(buff));
		sprintf(buff, "Number of Objects: %i", (int)*GCI->GetDeferredR()->GetNumObjDrawn());
		GCI->GetFontManager()->Render(buff, 0, 15);

		// End all Rendering
		GCI->EndRender();
	}
}


void TestBed::Update(float dt)
{
	FPS(dt);
	GCI->SetTimeElapsed(dt);
	/********************************/
#if DEMO_INSTANCING
	if( m_bInstanceDemo )
	{
		// Update Instance demo
		static float angle = 0.0f;
		angle += 1.0f;
		angle = (int)angle % 360;
	
		// Update instances
		D3DXMATRIX	transforms[44];
		D3DXCOLOR	colors[44];
		for(int i = 0; i < 44; i++)
		{
			// Generate transform matrix
			WTW::Transform transform;
			transform.m_Position = D3DXVECTOR3(i%11, (i/11.0f), 20.0f);
			transform.m_Rotation = D3DXVECTOR3(angle, 0.0f, angle);
			transforms[i] = transform.GetTransform();
			colors[i] = D3DXCOLOR(angle/360, i/44.0f, 1.0f, 1.0f);
		}

		// Apply updated transforms and colors
		GCI->GetInstanceManager()->UpdateInstance("testInst", transforms, 44);
		GCI->GetInstanceManager()->UpdateInstance("testInst", colors, 44);
		GCI->GetInstanceManager()->UpdateInstance("testInst2", transforms, 44);
		GCI->GetInstanceManager()->UpdateInstance("testInst2", colors, 44);
		GCI->GetInstanceManager()->UpdateInstance("testInst3", transforms, 44);
		GCI->GetInstanceManager()->UpdateInstance("testInst3", colors, 44);
		GCI->GetInstanceManager()->UpdateInstance("testInst4", transforms, 44);
		GCI->GetInstanceManager()->UpdateInstance("testInst4", colors, 44);
		GCI->GetInstanceManager()->UpdateInstance("testInst5", transforms, 44);
		GCI->GetInstanceManager()->UpdateInstance("testInst5", colors, 44);
		GCI->GetInstanceManager()->UpdateInstance("testInst6", transforms, 44);
		GCI->GetInstanceManager()->UpdateInstance("testInst6", colors, 44);
	}
#endif
}
void TestBed::FPS(float dt)
{
	static float numFrames = 0.0f;
	static float accumTime = 0.0f;

	numFrames += 1.0f;				// Increment the number of frames since the last Second
	accumTime += dt;				// Accumulate the time since the last Second
	m_fClock += dt;					// Total time counter

	if( accumTime >= 1.0f)
	{
		m_fFPS = numFrames;			// Sets the Frames Per Second counter

		m_fMsPF = 1000.0f/m_fFPS;	// Time that it takes to render a frame

		// Reset the counters
		accumTime = 0.0f;		
		numFrames = 0.0f;
	}
}

void TestBed::InitWindow()
{
	WNDCLASSEX wndClass;  
	ZeroMemory(&wndClass, sizeof(wndClass));
	bool windowed = true;

	// set up the window
	wndClass.cbSize			= sizeof(WNDCLASSEX);			// size of window structure
	wndClass.lpfnWndProc	= (WNDPROC)WndProc;				// message callback
	wndClass.lpszClassName	= m_Title.c_str();				// class name
	wndClass.hInstance		= m_hInst;						// handle to the application
	wndClass.hCursor		= LoadCursor(NULL, IDC_ARROW);	// default cursor
	wndClass.hbrBackground	= (HBRUSH)(COLOR_WINDOWFRAME);	// background brush

	// register a new type of window
	if(!RegisterClassEx(&wndClass))
	{
		MessageBox(0, "Failed to RegisterClassEx()", "Error", MB_OK);
		PostQuitMessage(0);
	}

	m_hWnd = CreateWindow(
		m_Title.c_str(), m_Title.c_str(), 							// window class name and title
		windowed ? WS_OVERLAPPEDWINDOW | WS_VISIBLE:(WS_POPUP | WS_VISIBLE),// window style
		CW_USEDEFAULT, CW_USEDEFAULT,							// x and y coordinates
		800, 600,												// width and height of window
		NULL, NULL,												// parent window and menu
		m_hInst,												// handle to application
		NULL);
	if(!m_hWnd)	// Check for Error
	{
		MessageBox(0, "Failed to CreateWindow()", "Error", MB_OK);
		PostQuitMessage(0);
	}

	// Display the window
	ShowWindow(m_hWnd, SW_SHOW);
	UpdateWindow(m_hWnd);
}

//////////////////////////////////////////////////////////////////////////
// Windows Method
//////////////////////////////////////////////////////////////////////////
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// Attempt to handle your messages
	switch(message)
	{
	case (WM_PAINT):
		{
			InvalidateRect(hWnd,NULL,TRUE);
			break;
		}	
	case (WM_CLOSE):
		{
			DestroyWindow(hWnd);
			return 0;
		}
	case(WM_DESTROY):
		{
			PostQuitMessage(0); 
			return 0;	//break;
		}
	case (WM_SIZE):
		{
			// Set to FullScreen when Maximized button is pressed
			if(wParam == SIZE_MAXIMIZED)
				GCI->GetDirectX()->SetFullScreen(true);
			break;
		}
	case (WM_EXITSIZEMOVE):
		{
			// Update Device Backbuffer to new Windows size
			GCI->GetDirectX()->ChangeScreenSize();
			break;
		}
	case(WM_KEYDOWN):
		{
			switch(wParam)
			{
			case 'F':	// Switch between FullScreen and Windowed
				{
					GCI->GetDirectX()->SetFullScreen(!GCI->GetDirectX()->IsFullScreen());	
					break;
				}
			case VK_ESCAPE:	// If in FullScreen mode, then set to windowed
				{
					if(GCI->GetDirectX()->IsFullScreen())
						GCI->GetDirectX()->SetFullScreen(false);
					else
						PostQuitMessage(0);	// If Windowed, then Exit
					break;
				}
			}
			break;
		}
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}
