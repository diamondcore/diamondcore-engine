#include "GraphicsCore.h"
#include "MacroTools.h"

GraphicsCore* GraphicsCore::s_Instance = 0;
GraphicsCore* GraphicsCore::Instance()
{
	if(!s_Instance)
		s_Instance = DEBUG_NEW GraphicsCore;
	return s_Instance;
}
GraphicsCore::GraphicsCore()
{
	m_pDirectX			= 0;
	m_pDeferredR		= 0;
	m_pCamera			= 0;
	m_pMeshManager		= 0;
	m_pTextureManager	= 0;
	m_pAnimationManager	= 0;
	m_pFontManager		= 0;
	m_pParticleManager	= 0;
	m_pPicker			= 0;
	m_fTimeElapsed		= 0.0f;
	m_bDeviceLost		= 1;
}
GraphicsCore::~GraphicsCore()
{}

void GraphicsCore::PreRestartDevice()
{
	// Refresh Sprite Mananger
	m_pParticleManager->PreRestartDevice();

	// 3D Objects
	m_pCamera->PreRestartDevice();
	//m_pMeshManager->PostRestartDevice();
	m_pDeferredR->PreRestartDevice();

	// Restart Sprite Core
	m_pDirectX->GetSprite()->OnLostDevice();

	// Release and prepare to restart Font
	m_pFontManager->PreRestartDevice();

	m_pElectricityManager->PreRestartDevice();
}
void GraphicsCore::PostRestartDevice()
{
	// Rebuild Sprite Core
	m_pDirectX->GetSprite()->OnResetDevice();

	// 3D objects
	m_pDeferredR->PostRestartDevice();
	//m_pMeshManager->PostRestartDevice();
	m_pCamera->PostRestartDevice();

	// Rebuild and Restore Font
	m_pFontManager->PostRestartDevice();
	m_pTextureManager->PostRestartDevice();
	m_pParticleManager->PostRestartDevice();
	m_pElectricityManager->PostRestartDevice();
}
bool GraphicsCore::IsDeviceLost()	// Checks if Graphics device is lost
{
	HRESULT hr = m_pDirectX->GetDevice()->TestCooperativeLevel();	// Get the state of the Graphics card

	// If the device is lost, and can't be reset yet.  Then wait
	if(hr == D3DERR_DEVICELOST)
	{
		Sleep(20);
		return true;
	}
	else if(hr == D3DERR_DRIVERINTERNALERROR)	// Driver Error
	{
		MessageBoxA(0, "Internal Driver Error!", 0, 0);
		PostQuitMessage(0);
		return true;
	}
	else if( hr == D3DERR_DEVICENOTRESET)	// Device is lost
	{
		PreRestartDevice();
		m_pDirectX->RestartDevice();		// Restart the Device
		// Test if device reset successfully
		hr = m_pDirectX->GetDevice()->TestCooperativeLevel();
		if(FAILED(hr))
			return true;
		PostRestartDevice();
		return false;	// Device is back
	}
	else
		return false;	// Device is back
}

bool GraphicsCore::Init(HINSTANCE hInst, HWND hWnd)
{
	bool result;

	//*****************************************
	// Initialize the DirectXClass
	m_pDirectX = new DirectXClass;
	result = m_pDirectX->Init(hInst, hWnd, 1, 0, 0);
	if(!result)
	{
		MessageBoxW(m_pDirectX->GetWindowHandle(), 
			L"Could not initialize Direct3D", L"Error", MB_OK);
		return false;
	}

	//*****************************************
	// Create Camera
	m_pCamera = new WTW::Camera;
	
	//*****************************************
	// Set Up Render in FX
	m_pDeferredR = new DeferredShader;
	m_pDeferredR->Init();
	
	// Create Shape Manager
	m_pMeshManager = new MeshManager;

	//*****************************************
	// Create Font Manager
	m_pFontManager = new FontManager;
	m_pFontManager->Init();

	//*****************************************
	// Create Texture Manager
	m_pTextureManager = new TextureManager;

	// Add For normal map test
	//m_pTextureManager->LoadTexture("tile010_normal.jpg");
	//m_pTextureManager->LoadTexture("tile010_spec.jpg");
	m_pMeshManager->Load("Sphere(33).x");

	// Create Animation Manager
	m_pAnimationManager = new AnimationManager;

	//*****************************************
	// Create Particle Manager
	m_pParticleManager = new ParticleManager;

	//*****************************************
	// Create Decal Manager
	m_pDecalManager = new DecalMgr;
	m_pDecalManager->Init();

	//*****************************************
	// Create Electricity Manager
	m_pElectricityManager = new ElectricityManager;
	m_pElectricityManager->Init();

	//*****************************************
	// Create Distortion Manager
	m_pDistortionManager = new DistMeshManager;
	//m_pDecalManager->Init();

	//*****************************************
	// Create the picker
	m_pPicker = new QuadPicking;

	// Return Successful
	return true;
}

void GraphicsCore::Shutdown()
{	
	SAFE_DELETE(m_pPicker);
	SAFE_SHUTDOWN(m_pParticleManager);
	SAFE_SHUTDOWN(m_pFontManager);
	SAFE_SHUTDOWN(m_pAnimationManager);
	m_pMeshManager->Shutdown();
	m_pTextureManager->Shutdown();
	//SAFE_SHUTDOWN(m_pMeshManager);
	//SAFE_SHUTDOWN(m_pTextureManager);
	SAFE_DELETE(m_pDecalManager);
	SAFE_DELETE(m_pElectricityManager);
	SAFE_DELETE(m_pDistortionManager);
	m_pCamera->PreRestartDevice();
	SAFE_DELETE(m_pCamera);
	SAFE_SHUTDOWN(m_pDeferredR);
	SAFE_SHUTDOWN(m_pDirectX);
	SAFE_DELETE(s_Instance);
}

// Rendering is now seperated out to give more control to the Main Core
bool GraphicsCore::BeginRender()
{
	// Check if Device is lost
	//m_bDeviceLost = IsDeviceLost();	// Move this to the front of Update In GraphicsCoreDLL
	CHECK(!m_bDeviceLost);		// Return if Device was lost

	// Update Camera
	m_pCamera->Update( m_fTimeElapsed );

	// Update the picking ray
	GCI->GetPicker()->UpdateRay();

	//*****************************************
	// Clear the Buffers
	m_pDirectX->BeginScene();
	return true;
}
bool GraphicsCore::Begin3DRender()
{
	CHECK(!m_bDeviceLost);		// Return if Device was lost

	// Call Game State Render 3D
	m_pDeferredR->BeginRender( m_pCamera );
	return true;
}
bool GraphicsCore::Begin2DRender()
{
	CHECK(!m_bDeviceLost);		// Return if Device was lost
	//*****************************************
	// End the Deffered Rendering and start PostProcessing
	m_pDeferredR->EndRender();

	//*****************************************
	// Build 2D Sprites
	m_pDirectX->GetSprite()->Begin(D3DXSPRITE_ALPHABLEND | D3DXSPRITE_SORT_TEXTURE | D3DXSPRITE_SORT_DEPTH_FRONTTOBACK);
	return true;
}
bool GraphicsCore::EndRender()
{
	CHECK(!m_bDeviceLost);		// Return if Device was lost
	//*****************************************
	// End Sprite Scene
	m_pDirectX->GetSprite()->End();

	// Present the scene to the screen
	m_pDirectX->EndScene();

	//*****************************************
	// Return Successful
	return true;
}

void GraphicsCore::SetTimeElapsed(float dt)
{	m_fTimeElapsed = dt;	}
 
//////////////////////////////////////////////////////////////////////////
// Getter Methods
//////////////////////////////////////////////////////////////////////////
DirectXClass* GraphicsCore::GetDirectX()
{	return m_pDirectX;	}
WTW::Camera* GraphicsCore::GetCamera()
{	return m_pCamera;	}
MeshManager* GraphicsCore::GetMeshManager()
{	return m_pMeshManager;	}
TextureManager* GraphicsCore::GetTextureManager()
{	return m_pTextureManager;	}
AnimationManager* GraphicsCore::GetAnimationManager()
{	return m_pAnimationManager;	}
FontManager* GraphicsCore::GetFontManager()
{	return m_pFontManager;	}
ParticleManager* GraphicsCore::GetParticleManager()
{	return m_pParticleManager;	}
DeferredShader* GraphicsCore::GetDeferredR()
{	return m_pDeferredR;	}
DecalMgr* GraphicsCore::GetDecalManager()
{	return m_pDecalManager;	}
ElectricityManager* GraphicsCore::GetElectricityManager()
{	return m_pElectricityManager;	}
DistMeshManager* GraphicsCore::GetDistortionManager()
{	return m_pDistortionManager;	}
QuadPicking* GraphicsCore::GetPicker()
{	return m_pPicker;	}
float GraphicsCore::GetTimeElapsed()
{	return m_fTimeElapsed;	}
bool GraphicsCore::CheckDevice()
{	
	// Check Device and set local variable
	return (m_bDeviceLost = IsDeviceLost());
}





