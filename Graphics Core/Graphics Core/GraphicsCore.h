#pragma once

#include "DirectXClass.h"

#define GCI GraphicsCore::Instance()

#include "3D\MeshManager.h"
#include "3D\Camera.h"
#include "2D\FontManager.h"
#include "2D\TextureManager.h"
#include "Pipeline\DeferredShader.h"
#include "3D\Animation\AnimationManager.h"
#include "Pipeline\Particles\ParticleManager.h"
#include "Pipeline\DecalManager.h"
#include "Pipeline\ElectricityManager.h"

// Intitial Graphics Values
const float	SCREEN_FAR		= 1000.0f;
const float	SCREEN_NEAR		= 0.1f;

class GraphicsCore
{
private:
	DirectXClass*		m_pDirectX;
	WTW::Camera*		m_pCamera;
	MeshManager*		m_pMeshManager;
	DeferredShader*		m_pDeferredR;
	TextureManager*		m_pTextureManager;
	AnimationManager*	m_pAnimationManager;
	FontManager*		m_pFontManager;
	ParticleManager*	m_pParticleManager;
	DecalMgr*			m_pDecalManager;
	ElectricityManager*	m_pElectricityManager;
	DistMeshManager*	m_pDistortionManager;
	QuadPicking*		m_pPicker;
	float				m_fTimeElapsed;

private:
	static GraphicsCore* s_Instance;
	GraphicsCore();

public:
	static GraphicsCore* Instance();
	~GraphicsCore();
	void	PreRestartDevice();
	void	PostRestartDevice();

	bool	Init(HINSTANCE hInst, HWND hWnd);
	void	Shutdown();

	void	SetTimeElapsed(float dt);
	float	GetTimeElapsed();

	// Rendering is now seperated out to give more control to the Main Core
	bool	CheckDevice();
	bool	BeginRender();
	bool	Begin3DRender();
	bool	Begin2DRender();
	bool	EndRender();

	// Used for multi-threading issues
	void	Lock()		{m_csGraphicsCore.Lock();}
	void	Unlock()	{m_csGraphicsCore.Unlock();}

public:
	DirectXClass*		GetDirectX();
	WTW::Camera*		GetCamera();
	MeshManager*		GetMeshManager();
	DeferredShader*		GetDeferredR();
	TextureManager*		GetTextureManager();
	AnimationManager*	GetAnimationManager();
	FontManager*		GetFontManager();
	ParticleManager*	GetParticleManager();
	DecalMgr*			GetDecalManager();
	ElectricityManager* GetElectricityManager();
	DistMeshManager*	GetDistortionManager();
	QuadPicking*		GetPicker();

private:
	CriticalSection	m_csGraphicsCore;
	bool	IsDeviceLost();
	bool	m_bDeviceLost;
};