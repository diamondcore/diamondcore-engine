#include "DirectXClass.h"
#include "MacroTools.h"
#include "GraphicsCore.h"

DirectXClass::DirectXClass()
{
	m_bVsync	= false;
	m_pObject	= 0;
	m_pDevice	= 0;
	m_pSprite	= 0;
}
DirectXClass::~DirectXClass()
{}
void DirectXClass::RestartDevice()
{
	m_pDevice->Reset(&m_Dpp);
}

bool DirectXClass::Init(HINSTANCE hInst, HWND hWnd, bool windowed, bool VSync, bool multiSample)
{
	m_bVsync	= VSync;
	m_hInst		= hInst;
	m_hWnd		= hWnd;

	//////////////////////////////////////////////////////////////////////////
	// Direct3D Foundations - D3D Object, Present Parameters, and D3D Device
	//////////////////////////////////////////////////////////////////////////
	// Create the D3D Object
	m_pObject = Direct3DCreate9(D3D_SDK_VERSION);
	CHECK(m_pObject);

	// Check adapters
	D3DDISPLAYMODE	mode;
	for(UINT i = 0; i < m_pObject->GetAdapterCount(); i++)
	{
		UINT modeCnt = m_pObject->GetAdapterModeCount(i, D3DFMT_X8R8G8B8);
		for(UINT j = 0; j < modeCnt; j++)
			m_pObject->EnumAdapterModes(i, D3DFMT_X8R8G8B8, j, &mode);
	}

	// Find the width and height of window using hWnd and GetWindowRect()
	RECT rect;
	GetClientRect(m_hWnd, &rect);
	//GetWindowRect(m_hWnd, &rect);
	int width = rect.right;	//rect.right - rect.left;
	int height = rect.bottom;	//rect.bottom - rect.top;

	// Set D3D Device presentation parameters before creating the device
	ZeroMemory(&m_Dpp, sizeof(m_Dpp));  // NULL the structure's memory
	m_Dpp.hDeviceWindow					= m_hWnd;									// Handle to the focus window
	m_Dpp.Windowed						= windowed;									// Windowed or Full-screen boolean
	m_Dpp.AutoDepthStencilFormat		= D3DFMT_D24S8;								// Format of depth/stencil buffer, 24 bit depth, 8 bit stencil
	m_Dpp.EnableAutoDepthStencil		= TRUE;										// Enables Z-Buffer (Depth Buffer)
	m_Dpp.BackBufferCount				= 1;										// Change if need of > 1 is required at a later date
	m_Dpp.BackBufferFormat				= D3DFMT_X8R8G8B8;							// Back-buffer format, 8 bits for each pixel
	m_Dpp.BackBufferHeight				= height;									// Make sure resolution is supported, use adapter modes
	m_Dpp.BackBufferWidth				= width;									// (Same as above)
	m_Dpp.SwapEffect					= D3DSWAPEFFECT_DISCARD;					// Discard back-buffer, must stay discard to support multi-sample
	m_Dpp.PresentationInterval			= m_bVsync ? D3DPRESENT_INTERVAL_DEFAULT : D3DPRESENT_INTERVAL_IMMEDIATE; // Present back-buffer immediately, unless V-Sync is on								
	m_Dpp.Flags							= D3DPRESENTFLAG_DISCARD_DEPTHSTENCIL;		// This flag should improve performance, if not set to NULL.
	m_Dpp.FullScreen_RefreshRateInHz	= windowed ? 0 : D3DPRESENT_RATE_DEFAULT;	// Full-screen refresh rate, use adapter modes or default
	
	// Set Multi Sample settings
	MultiSampling(multiSample);	// Turn on/off MultiSampling

	// Check device capabilities
	DWORD deviceBehaviorFlags = 0;
	m_pObject->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &m_D3DCaps);

	// Determine vertex processing mode
	if(m_D3DCaps.DevCaps & D3DCREATE_HARDWARE_VERTEXPROCESSING)
	{
		// Hardware vertex processing supported? (Video Card)
		deviceBehaviorFlags |= D3DCREATE_HARDWARE_VERTEXPROCESSING;	
	}
	else
	{
		// If not, use software (CPU)
		deviceBehaviorFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING; 
	}
	
	// If hardware vertex processing is on, check pure device support
	if(m_D3DCaps.DevCaps & D3DDEVCAPS_PUREDEVICE && deviceBehaviorFlags & D3DCREATE_HARDWARE_VERTEXPROCESSING)
	{
		deviceBehaviorFlags |= D3DCREATE_PUREDEVICE;	
	}

	// Create the D3D Device with the present parameters and device flags above
	m_pObject->CreateDevice(
		D3DADAPTER_DEFAULT,		// which adapter to use, set to primary
		D3DDEVTYPE_HAL,			// device type to use, set to hardware rasterization
		m_hWnd,					// handle to the focus window
		deviceBehaviorFlags,	// behavior flags
		&m_Dpp,					// presentation parameters
		&m_pDevice);			// returned device pointer
	CHECK(m_pDevice);

	// Set Full Screen Setings
	if(!windowed)
		SetFullScreen(true);
	
	m_BkgColor	= D3DXCOLOR(0.0f, 0.2f, 0.4f, 1.0f);	// Set Background color
	//*************************************************************************
	// Check Luminance format
	if(CheckFormatSupport(D3DFMT_R32F, D3DFMT_D24X8))
		m_LumFormat = D3DFMT_R32F;
	else if(CheckFormatSupport(D3DFMT_R16F, D3DFMT_D24X8))
		m_LumFormat = D3DFMT_R16F;
	else
		m_LumFormat = D3DFMT_X8R8G8B8;
	//*************************************************************************
	// Create a sprite object, note you will only need one for all 2D sprites
	D3DXCreateSprite(m_pDevice, &m_pSprite);
	CHECK(m_pSprite);

	// Return Successful
	return true;
}
	
void DirectXClass::SetFullScreen(bool enable)	// Swithch between Windowed and FullScreen
{
	if(enable)
	{
		if(!m_Dpp.Windowed)	// Check if already in Fullscreen
			return;

		// Get total current Screen Resolustion
		int width	= GetSystemMetrics(SM_CXSCREEN);
		int height	= GetSystemMetrics(SM_CYSCREEN);

		m_Dpp.BackBufferFormat	= D3DFMT_X8R8G8B8;
		m_Dpp.BackBufferWidth	= width;
		m_Dpp.BackBufferHeight	= height;
		m_Dpp.Windowed			= false;

		// Change to a Fullscreen Style window
		SetWindowLongPtr(m_hWnd, GWL_STYLE, WS_POPUP);

		// Apply the new window's dimensions
		SetWindowPos(m_hWnd, HWND_TOP, 0, 0, width, height, 
							SWP_NOZORDER | SWP_SHOWWINDOW);
		
		// Lock cursor to adjusted window size
		RECT rect = {0,0, width, height};
		ClipCursor(&rect);
	}
	else	// Switch to Window
	{
		if(m_Dpp.Windowed)	// Check if already in Window
			return;

		// Set to original Window size
		RECT rect;
		SetRect(&rect, 0,0, SCREEN_WIDTH, SCREEN_HEIGHT);
		AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, false);

		// Change to a Windowed Style window
		SetWindowLongPtr(m_hWnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);

		// Apply the new Windows's dimensions
		SetWindowPos(m_hWnd, HWND_TOP, 100, 100,
						(rect.right-rect.left), (rect.bottom-rect.top),
						SWP_NOZORDER | SWP_SHOWWINDOW);

		// Get the client rectangle size
		RECT cRect;
		GetClientRect(m_hWnd, &cRect);
		m_Dpp.BackBufferFormat	= D3DFMT_UNKNOWN;
		m_Dpp.BackBufferWidth	= cRect.right;	//SCREEN_WIDTH;
		m_Dpp.BackBufferHeight	= cRect.bottom;	//SCREEN_HEIGHT;
		m_Dpp.Windowed			= true;
	}

	// Restart the device with the new parameters
	PreRestartDevice();
	m_pDevice->Reset(&m_Dpp);
	PostRestartDevice();
}
bool DirectXClass::IsFullScreen()
{	return (!m_Dpp.Windowed);	}

void DirectXClass::ChangeScreenSize()
{
	// Update Backbuffer to Windows screen size
	RECT clientRect;
	GetClientRect(m_hWnd, &clientRect);
	m_Dpp.BackBufferWidth	= clientRect.right;
	m_Dpp.BackBufferHeight	= clientRect.bottom;

	// Reset updated Device backbuffer
	PreRestartDevice();
	m_pDevice->Reset(&m_Dpp);
	PostRestartDevice();
}

void DirectXClass::MultiSampling(bool isOn)
{
	if(isOn)	// Enable MultiSampling
	{
		// Check for MultiSampling compatibility
		D3DMULTISAMPLE_TYPE msType = D3DMULTISAMPLE_2_SAMPLES;
		DWORD msQuality = 0;

		// Check if compatible with the backbuffer
		if(SUCCEEDED(m_pObject->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, 
																D3DFMT_X8R8G8B8, true, msType, &msQuality)))
		{
			// Check if compatible with the depth format
			if(SUCCEEDED(m_pObject->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, 
																D3DFMT_D24S8, true, msType, &msQuality)))
			{		
				// Discard back-buffer, must stay discard to support multi-sample
				m_Dpp.SwapEffect	= D3DSWAPEFFECT_DISCARD;		

				m_Dpp.MultiSampleType		= msType;				// Apply the MultiSample changes
				m_Dpp.MultiSampleQuality	= msQuality-1;
			}
		}
	}
	else	// Disable MultiSampling
	{
		// Change the MultiSampling settings back to default
		m_Dpp.MultiSampleQuality	= 0;			
		m_Dpp.MultiSampleType		= D3DMULTISAMPLE_NONE;
	}

	// If the device was not created yet, then return
	if(!m_pDevice)
		return;

	// Restart the device with the new parameters
	PreRestartDevice();
	m_pDevice->Reset(&m_Dpp);
	PostRestartDevice();
}

void DirectXClass::BeginScene()
{
	// If the device was not created successfully, return
	if(!m_pDevice)
		return;
	//*************************************************************************
	// Clear the back buffer, call BeginScene()
	m_pDevice->Clear(0,NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 
					m_BkgColor, 1.0f, 0);
	m_pDevice->BeginScene();
}
void DirectXClass::EndScene()
{
	// EndScene, and Present the back buffer to the display buffer
	m_pDevice->EndScene();
	m_pDevice->Present(0, 0, 0, 0);
	//*************************************************************************
}

void DirectXClass::Shutdown()
{
	//*************************************************************************
	// Release objects in the opposite order they were created in
	// Sprite
	SAFE_RELEASE(m_pSprite);

	// D3DDevice	
	SAFE_RELEASE(m_pDevice);

	// 3DObject
	SAFE_RELEASE(m_pObject);
	//*************************************************************************
	// Shutdown Window
	ShowCursor(true);	// Set screen settings back to normal

	// Change to Windowed mode, before destroying
	if(!m_Dpp.Windowed)
		ChangeDisplaySettings(NULL, 0);
	//*************************************************************************
}

bool DirectXClass::CheckFormatSupport(D3DFORMAT device, D3DFORMAT depth)
{
	D3DDISPLAYMODE mode;
	m_pObject->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &mode);

	// Windowed
	if(FAILED(m_pObject->CheckDeviceFormat(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,
		mode.Format, D3DUSAGE_RENDERTARGET, D3DRTYPE_TEXTURE, device)))
		return false;
	if(FAILED(m_pObject->CheckDepthStencilMatch(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,
		mode.Format, device, depth)))
		return false;

	// Full Screen
	if(FAILED(m_pObject->CheckDeviceFormat(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,
		D3DFMT_X8R8G8B8, D3DUSAGE_RENDERTARGET, D3DRTYPE_TEXTURE, device)))
		return false;
	if(FAILED(m_pObject->CheckDepthStencilMatch(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,
		D3DFMT_X8R8G8B8, device, depth)))
		return false;
	return true;
}


//////////////////////////////////////////////////////////////////////////
// Direct3D Variables
//////////////////////////////////////////////////////////////////////////
ID3DXSprite* DirectXClass::GetSprite()
{	return m_pSprite;	}
IDirect3DDevice9* DirectXClass::GetDevice()
{	return m_pDevice;	}
D3DFORMAT DirectXClass::GetLuminanceFormat()
{	return m_LumFormat;	}
	
//////////////////////////////////////////////////////////////////////////
// Application Variables
//////////////////////////////////////////////////////////////////////////
bool DirectXClass::IsVsyncOn()
{	return m_bVsync;	}
void DirectXClass::SetBackgroundColor(D3DXCOLOR color)
{	m_BkgColor = color;	}

HWND DirectXClass::GetWindowHandle()
{	return m_hWnd;	}
HINSTANCE DirectXClass::GetWindowInstance()
{	return m_hInst;	}

float DirectXClass::GetScreenWidth()
{	return (float)m_Dpp.BackBufferWidth;	}
float DirectXClass::GetScreenHeight()
{	return (float)m_Dpp.BackBufferHeight;	}

// Reset from GraphicsCore
void DirectXClass::PreRestartDevice()
{
	// Use the Graphics PreRestartDevice
	GCI->PreRestartDevice();
}
void DirectXClass::PostRestartDevice()
{
	// Use the Graphics PostRestartDevice
	GCI->PostRestartDevice();
}