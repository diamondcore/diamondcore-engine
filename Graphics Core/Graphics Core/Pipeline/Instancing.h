#pragma once

#include "../3D/VertexDecl.h"
#include <string>

/********************************
		Quad Instancing
*********************************/
class Instancing
{
	IDirect3DIndexBuffer9*	InstIndexBuffer;
	IDirect3DVertexBuffer9*	InstVertexBuffer;
	D3DXMATRIX				m_WorldMat;

	// Max Array size in shaders 3.0 is 224 Constant Registers(float4) (tested and proven)
	D3DXVECTOR4				instanceData[220];	// Can store WorldMat and Color
	unsigned int			vec4Count;			// Count of inst world Vector4
	unsigned int			instCount;			// Number of instances
	unsigned int			numVerts;			// Total number of Vertices
	unsigned int			numIndex;			// Total number of Indices
	std::string				m_TexName;
	bool					m_bIsImposter;

	void	BuildInstVertexBuff();
	void	BuildInstIndexBuff();

public:
	Instancing();
	~Instancing();

	// To Bach, add instances before creating
	void	AddBachInstance(D3DXMATRIX worldTransform, D3DXCOLOR color);	// Add Instance to array
	void	Create(std::string texture);
	void	Shutdown();

	// Any instance added after Create() is called, should use the method below
	void	AddDynamicInstance(D3DXMATRIX worldTransform, D3DXCOLOR color);	// Dyanmically Add to the buffer
	void	Update(unsigned int instID, D3DXMATRIX worldTransform);			// Update an existing Instance
	void	Update(unsigned int instID, D3DXCOLOR color);

	void	SetWorldMat(D3DXMATRIX world);		// Set general world matrix for all of the instances
	void	SetAlwaysFaceCamera(bool enable);
	void	Render();

	int		GetNumInstances();
};