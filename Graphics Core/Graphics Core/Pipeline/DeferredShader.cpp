#include "DeferredShader.h"
#include "../MacroTools.h"
#include "../GraphicsCore.h"
#include "../Utility.h"
#include "../Westley's Lib/Transform.h"
#include <fstream>

#define DISTORTSPEED1 D3DXVECTOR2(-0.002f, 0.01f);
#define DISTORTSPEED2 D3DXVECTOR2(0.0093f, -0.0035f);

DeferredShader::DeferredShader()
	:m_bUseHDR(1),
	m_bUseShadowMap(0),
	 m_bUseSSAO(1),
	 m_bUseDOF(0),
	 m_bUseBlur(0),
	 m_bUseDecal(0),
	 m_bUseHeatHaze(0),
	 m_iNumObjDrawn(0),
	 m_pTerrain(0),
	 m_pBlackTex(0),
	 m_pFX(0)
{}
DeferredShader::~DeferredShader()
{}

void DeferredShader::PreRestartDevice()
{
	SAFE_RELEASE(m_pBackBuff);

	if(m_pTerrain)
		m_pTerrain->PreRestartDevice();
	m_Light.PreRestartDevice();
	m_HDR.PreRestartDevice();

	//SAFE_RELEASE(m_pHeatMtrlTex);
	//SAFE_RELEASE(m_pHeatMaskTex);
	//SAFE_RELEASE(m_pHazeBuff);

	m_VelocityBlurSurface.PreRestartDevice();
	//m_HazeMaskSurface.PreRestartDevice();
	m_SSAOsurface.PreRestartDevice();
	//m_LightSurface.PreRestartDevice();
	//m_ShadowSurface.PreRestartDevice();
	m_MtrlSurface.PreRestartDevice();
	m_NormSurface.PreRestartDevice();
	m_PosSurface.PreRestartDevice();
	m_DepthSurface.PreRestartDevice();
	m_pFX->OnLostDevice();
}
void DeferredShader::PostRestartDevice()
{
	m_pFX->OnResetDevice();
	// Set the screen sizes again (in-case it changed)
	m_fWidth	= GCI->GetDirectX()->GetScreenWidth();
	m_fHeight	= GCI->GetDirectX()->GetScreenHeight();
	m_MtrlSurface.m_iWidth		= m_fWidth;
	m_MtrlSurface.m_iHeight		= m_fHeight;
	m_NormSurface.m_iWidth		= m_fWidth;
	m_NormSurface.m_iHeight		= m_fHeight;
	m_PosSurface.m_iWidth		= m_fWidth;
	m_PosSurface.m_iHeight		= m_fHeight;
	m_DepthSurface.m_iWidth		= m_fWidth;
	m_DepthSurface.m_iHeight	= m_fHeight;
	/*m_ShadowSurface.m_iWidth	= m_fWidth* 400/800;
	m_ShadowSurface.m_iHeight	= m_fHeight* 300/600;
	m_pFX->SetValue("SHADOW_MAP_SIZE", D3DXVECTOR2(m_ShadowSurface.m_iWidth, m_ShadowSurface.m_iHeight), sizeof(D3DXVECTOR2));
	m_LightSurface.m_iWidth		= m_fWidth;
	m_LightSurface.m_iHeight	= m_fHeight;*/
	m_SSAOsurface.m_iWidth	= m_fWidth;// /4;
	m_SSAOsurface.m_iHeight	= m_fHeight;// /4;
	m_VelocityBlurSurface.m_iHeight = m_fHeight;
	m_VelocityBlurSurface.m_iWidth = m_fWidth;
	//m_HazeMaskSurface.m_iHeight = m_fHeight;
	//m_HazeMaskSurface.m_iWidth = m_fWidth;



	m_MtrlSurface.PostRestartDevice();
	m_NormSurface.PostRestartDevice();
	m_PosSurface.PostRestartDevice();
	m_DepthSurface.PostRestartDevice();
	//m_ShadowSurface.PostRestartDevice();
	//m_LightSurface.PostRestartDevice();
	m_SSAOsurface.PostRestartDevice();
	m_VelocityBlurSurface.PostRestartDevice();
	//m_HazeMaskSurface.PostRestartDevice();
	
	//m_pFX->SetTexture("NormalMapTexture", m_pBlackTex);
	//D3DXCreateTexture(GCI->GetDirectX()->GetDevice(),
	//					m_fWidth,
	//					m_fHeight,
	//					1,
	//					D3DUSAGE_RENDERTARGET,
	//					D3DFMT_A8R8G8B8,
	//					D3DPOOL_DEFAULT,
	//					&m_pHeatMtrlTex);
	//D3DXCreateTexture(GCI->GetDirectX()->GetDevice(),
	//					m_fWidth,
	//					m_fHeight,
	//					1,
	//					D3DUSAGE_RENDERTARGET,
	//					D3DFMT_A8R8G8B8,
	//					D3DPOOL_DEFAULT,
	//					&m_pHeatMaskTex);
	////m_pHeatMtrlTex->GetSurfaceLevel(0, &m_pBackBuff);
	//m_pHeatMaskTex->GetSurfaceLevel(0, &m_pHazeBuff);
	
	m_Light.PostRestartDevice();
	m_HDR.PostRestartDevice();
	if(m_pTerrain)
		m_pTerrain->PostRestartDevice();
}

void DeferredShader::Init()
{
	if(!CheckDeviceCaps())	// Check if Computer is capable of running shaders
	{
		MessageBoxW(0, L"Hardware does not support: Shaders 3.0", L"Error", MB_OK);
		PostQuitMessage(0);
	}

	// Initialize the Vertex Declerations
	InitAllVertexDeclarations();

	ReloadShader();

	// Set up some constant Values
	D3DXMatrixIdentity(&m_WorldMat);
	D3DXMatrixInverse(&m_WorldInvTransMat, 0, &m_WorldMat);
	//D3DXMatrixTranspose(&m_WorldInvTransMat, &m_WorldInvTransMat);

	D3DXCreateTextureFromFile(GCI->GetDirectX()->GetDevice(), "Content/Textures/blacktex.bmp", &m_pTex);
	D3DXCreateTextureFromFile(GCI->GetDirectX()->GetDevice(), "Content/Textures/whitetex.dds", &m_pDefaultTex);
	D3DXCreateTextureFromFile(GCI->GetDirectX()->GetDevice(), "Content/Textures/RandomNormals.png", &m_pBlackTex);

	// Initialize the Deferred Surfaces
	int width	= GCI->GetDirectX()->GetScreenWidth(); //256;
	int height	= GCI->GetDirectX()->GetScreenHeight(); //256;
	bool useDepthS = false; //true;
	//m_MtrlSurface.setDepthStencil(D3DFMT_D24S8, true);	// For Stencil Light calculations
	m_MtrlSurface.setDepthStencil(D3DFMT_D24X8, useDepthS);
	m_MtrlSurface.setTexture(D3DFMT_X8R8G8B8, false);
	m_MtrlSurface.m_iWidth	= width;
	m_MtrlSurface.m_iHeight	= height;
	m_MtrlSurface.Init();

	m_NormSurface.setDepthStencil(D3DFMT_D24X8, useDepthS);	//true);
	//m_NormSurface.setTexture(D3DFMT_A2R10G10B10, false);
	m_NormSurface.setTexture(D3DFMT_A8R8G8B8, false);
	m_NormSurface.m_iWidth	= width;
	m_NormSurface.m_iHeight	= height;
	m_NormSurface.Init();

	m_PosSurface.setDepthStencil(D3DFMT_D24X8, useDepthS);	//true);
	//m_PosSurface.setTexture(D3DFMT_A2R10G10B10, false);
	m_PosSurface.setTexture(D3DFMT_A8R8G8B8, false);
	m_PosSurface.m_iWidth	= width;
	m_PosSurface.m_iHeight	= height;
	m_PosSurface.Init();

	if(!GCI->GetDirectX()->CheckFormatSupport(D3DFMT_R32F, D3DFMT_D24X8))
		MessageBox(GCI->GetDirectX()->GetWindowHandle(), "Device format not supported.", "Failed", MB_OK);

	m_DepthSurface.setDepthStencil(D3DFMT_D24S8, true);//useDepthS);
	m_DepthSurface.setTexture(D3DFMT_R32F, false);
	//m_DepthSurface.setTexture(D3DFMT_A8R8G8B8, false);
	m_DepthSurface.m_iWidth		= width;
	m_DepthSurface.m_iHeight	= height;
	m_DepthSurface.Init();

	//m_ShadowSurface.setDepthStencil(D3DFMT_D24X8, true);
	//m_ShadowSurface.setTexture(D3DFMT_R32F, false);
	////m_ShadowSurface.setTexture(D3DFMT_A8R8G8B8, false);
	//m_ShadowSurface.m_iWidth	= 512; //width* 400/800;	//512;
	//m_ShadowSurface.m_iHeight	= 512; //height* 300/600;	//512;
	//m_ShadowSurface.Init();
	//m_pFX->SetValue("SHADOW_MAP_SIZE", D3DXVECTOR2(m_ShadowSurface.m_iWidth, m_ShadowSurface.m_iHeight), sizeof(D3DXVECTOR2));

	//m_LightSurface.setDepthStencil(D3DFMT_D24S8, true);
	//m_LightSurface.setTexture(D3DFMT_A8R8G8B8, false);
	//m_LightSurface.m_iWidth		= width;
	//m_LightSurface.m_iHeight	= height;
	//m_LightSurface.Init();

	m_SSAOsurface.setDepthStencil(D3DFMT_D24X8, false);	//true);
	//m_SSAOsurface.setTexture(D3DFMT_A8R8G8B8, false);
	m_SSAOsurface.setTexture(D3DFMT_R32F, false);
	m_SSAOsurface.m_iWidth	= width; // /4;	//512;
	m_SSAOsurface.m_iHeight	= height; // /4;	//512;
	m_SSAOsurface.Init();

	// Set Screen Space Quad
	m_fWidth = width;
	m_fHeight = height;

	m_HDR.Init();
	m_pBackBuff = 0;
	m_pGraphicsMgr = 0;

	//m_HazeMaskSurface.setDepthStencil(D3DFMT_D24S8, true);
	//m_HazeMaskSurface.setTexture(D3DFMT_A8R8G8B8, false);
	//m_HazeMaskSurface.m_iWidth = width;
	//m_HazeMaskSurface.m_iHeight = height;
	//m_HazeMaskSurface.Init();
	//m_pFX->SetFloat(m_hHeatBump, 0.01f);
	//float offset[] = {0, 0};
	//m_pFX->SetFloatArray(m_hHeatOffset1, offset, 2);
	//m_pFX->SetFloatArray(m_hHeatOffset2, offset, 2);
	//m_pFX->SetFloat(m_hHeatIntensity, 5.0f);
	//m_pFX->SetFloat(m_hHeatScale, 1.0f);
	///m_pFX->SetTexture("NormalMapTexture", m_pBlackTex);
	//D3DXCreateTexture(GCI->GetDirectX()->GetDevice(),
	//					width,
	//					height,
	//					1,
	//					D3DUSAGE_RENDERTARGET,
	//					D3DFMT_A8R8G8B8,
	//					D3DPOOL_DEFAULT,
	//					&m_pHeatMtrlTex);
	//D3DXCreateTexture(GCI->GetDirectX()->GetDevice(),
	//					width,
	//					height,
	//					1,
	//					D3DUSAGE_RENDERTARGET,
	//					D3DFMT_A8R8G8B8,
	//					D3DPOOL_DEFAULT,
	//					&m_pHeatMaskTex);
	////m_pHeatMtrlTex->GetSurfaceLevel(0, &m_pBackBuff);
	//m_pHeatMaskTex->GetSurfaceLevel(0, &m_pHazeBuff);

	m_VelocityBlurSurface.setDepthStencil(D3DFMT_D24X8, useDepthS);	//true);
	//m_NormSurface.setTexture(D3DFMT_A2R10G10B10, false);
	m_VelocityBlurSurface.setTexture(D3DFMT_A8R8G8B8, false);
	m_VelocityBlurSurface.m_iWidth	= width;
	m_VelocityBlurSurface.m_iHeight	= height;
	m_VelocityBlurSurface.Init();
}
void DeferredShader::Shutdown()
{
	SAFE_DELETE(m_pTerrain);
	SAFE_RELEASE(m_pBlackTex);
	SAFE_RELEASE(m_pDefaultTex);
	SAFE_RELEASE(m_pTex);
	//SAFE_RELEASE(m_pHeatMtrlTex);
	//SAFE_RELEASE(m_pHeatMaskTex);
	SAFE_RELEASE(m_pBackBuff);
	//SAFE_RELEASE(m_pHazeBuff);
	m_VelocityBlurSurface.PreRestartDevice();
	//m_HazeMaskSurface.PreRestartDevice();
	m_HDR.Shutdown();
	m_SSAOsurface.PreRestartDevice();
	//m_LightSurface.PreRestartDevice();
	//m_ShadowSurface.PreRestartDevice();
	m_MtrlSurface.PreRestartDevice();
	m_NormSurface.PreRestartDevice();
	m_PosSurface.PreRestartDevice();
	m_DepthSurface.PreRestartDevice();
	SAFE_RELEASE(m_pFX);	// Release the m_pFX effect
	DestroyAllVertexDeclarations();
}

void DeferredShader::BeginRender(WTW::Camera* camera)
{
	m_iNumObjDrawn = 0;	// Reset object count
	m_fNumObjDrawn = 0.0f;
	
	// Store the old render target
	if(m_pBackBuff == 0)
		GCI->GetDirectX()->GetDevice()->GetRenderTarget(0, &m_pBackBuff);
	
	// Flag the Shadow Map settings
	m_pFX->SetBool("f_UseLightShadow", m_bUseShadowMap);
	
	// Set the Render Targets
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, m_MtrlSurface.m_pTopSurface);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(1, m_NormSurface.m_pTopSurface);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(2, m_PosSurface.m_pTopSurface);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(3, m_DepthSurface.m_pTopSurface);

	// Set up the Camera
	/*D3DXMATRIX viewInv, projInv;
	D3DXMatrixInverse(&viewInv, 0, &camera->GetView());
	D3DXMatrixInverse(&projInv, 0, &camera->GetProj());
	m_pFX->SetMatrix("g_sProjInv", &projInv);
	m_pFX->SetMatrix("g_sViewInv", &viewInv);*/

	D3DXMATRIX view = GCI->GetCamera()->GetView();

	m_pFX->SetMatrix(m_hWorld, &m_WorldMat);
	m_pFX->SetMatrix(m_hWorldInvTrans, &m_WorldInvTransMat);
	m_pFX->SetMatrix(m_hWVP, &camera->GetViewProj());
	m_pFX->SetMatrix("g_sViewProj", &camera->GetViewProj());
	m_pFX->SetMatrix("g_sView", &camera->GetView());
	m_pFX->SetMatrix("g_sProj", &camera->GetProj());
	//m_pFX->SetValue(m_hCameraPos, &camera->m_Position, sizeof(D3DXVECTOR3));	
	
	D3DXMatrixInverse(&view, 0, &view);
	
	m_pFX->SetValue(m_hCameraPos, &D3DXVECTOR3(view._41, view._42, view._43), sizeof(D3DXVECTOR3));

	GCI->GetDirectX()->GetDevice()->SetVertexDeclaration(VertexBlendEx::Decl);

	//m_pFX->GetMatrix("g_sViewProj", &m_mPrevViewProj);
	m_pFX->SetMatrix("g_sPrevViewProj", &m_mPrevViewProj);
	m_mPrevViewProj = camera->GetViewProj();

	D3DXMATRIX viewProjInv = GCI->GetCamera()->GetViewProj();
	D3DXMatrixInverse(&viewProjInv, 0, &viewProjInv);
	m_pFX->SetMatrix("g_sViewProjInv", &viewProjInv);
	
	// Set up the Light
	m_Light.PreRender();

	m_pFX->SetTexture("g_sShadowMapTex", m_ShadowSurface.m_pTexture);
	m_pFX->SetTexture("g_sTexture", m_pTex);
	m_pFX->CommitChanges();
	
	/////////////////////////////////////////////////////////////////
	UINT numPasses = 0;
	m_pFX->SetTechnique(m_thMain);	// For now just use Main
	m_pFX->Begin(&numPasses, D3DXFX_DONOTSAVESTATE); //0);
	GCI->GetDirectX()->GetDevice()->Clear(0,NULL, D3DCLEAR_TARGET, // | D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL, 
						D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f), 1.0f, 0);
						//D3DXCOLOR(0.0f, 0.2f, 0.4f, 1.0f), 1.0f, 0);
	
	m_pFX->BeginPass( 0 );
	/////////////////////////////////////
}
void DeferredShader::EndRender()	// Run through post-processing effects
{	
	// Render Terrain Demo (Remove Later)
	if(m_pTerrain)
	{
		m_pTerrain->Render();
	}
	/////////////////////////////////////
	m_pFX->EndPass();
	m_pFX->End();
	
	/////////////////////////////////////
	// Debug Tools: Save to an image file
	//m_DepthSurface.DumpSurface(0, 0, m_DepthSurface.m_iWidth, m_DepthSurface.m_iHeight);
	//m_DepthSurface.SaveToFile("Depth");
	//m_PosSurface.DumpSurface(0, 0, m_PosSurface.m_iWidth, m_PosSurface.m_iHeight);
	//m_PosSurface.SaveToFile("Pos");
	//m_NormSurface.DumpSurface(0, 0, m_NormSurface.m_iWidth, m_NormSurface.m_iHeight);
	//m_NormSurface.SaveToFile("Norm");
	//m_MtrlSurface.DumpSurface(0, 0, m_MtrlSurface.m_iWidth, m_MtrlSurface.m_iHeight);
	//m_MtrlSurface.SaveToFile("Mtrl");
	//m_ShadowSurface.DumpSurface(0, 0, m_ShadowSurface.m_iWidth, m_ShadowSurface.m_iHeight);
	//m_ShadowSurface.SaveToFile("ShadowDepth");
	/////////////////////////////////////

	// Set the Default Material buffer
	m_pFX->SetTexture(m_hDefMtrl, m_MtrlSurface.m_pTexture);
	m_pFX->SetBool("f_UseSSAO", m_bUseSSAO);	

	if(m_bUseDecal)
		GCI->GetDecalManager()->RenderAllDecals(GCI->GetTimeElapsed());
	
	GCI->GetElectricityManager()->RenderAll();

	if( m_bUseSSAO )		// Toggle SSAO
		BuildSSAOMap();
	
	if(m_bUseBlur)
		ApplyBlur();

	//RunDOF( m_HDR.LDR.m_pTopSurface); //m_pBackBuff );
	//SAFE_RELEASE(m_pBackBuff);

	// Render alpha quads
	if(m_pGraphicsMgr)
		m_pGraphicsMgr->RenderBillboards();

	// Render Particles
	//GCI->GetParticleManager()->Render();

	if(m_bUseHeatHaze)
		ApplyHeatHaze();


	if( m_bUseHDR )
		m_HDR.Render();

	// Render Particles
	GCI->GetParticleManager()->Render();
}

void DeferredShader::BeginShadowMap()
{
	// Save the back buffer for later use
	GCI->GetDirectX()->GetDevice()->GetRenderTarget(0, &m_pBackBuff);

	GCI->GetDirectX()->GetDevice()->SetVertexDeclaration(VertexBlendEx::Decl);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, m_ShadowSurface.m_pTopSurface);
	GCI->GetDirectX()->GetDevice()->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
						0x00000000, 1.0f, 0);
	m_pFX->SetTechnique("BuildShadowMapTech");

	//m_pFX->SetValue(m_hPixelSize, D3DXVECTOR2(m_fWidth/m_ShadowSurface.m_iWidth, m_fHeight/m_ShadowSurface.m_iHeight),
	m_pFX->SetValue(m_hPixelSize, D3DXVECTOR2(1.0f/m_fWidth, 1.0f/m_fHeight), 
		sizeof(D3DXVECTOR2));	// Set Screen Size in pixels
	
	// Set light properties
	m_Light.PreRender();
	m_pFX->CommitChanges();

	UINT numPasses = 0;
	m_pFX->Begin(&numPasses, 0);
	m_pFX->BeginPass(0);
}	
void DeferredShader::EndShadowMap()
{
	m_pFX->EndPass();
	m_pFX->End();
}

void DeferredShader::BuildSSAOMap()
{
	// Set the output surface
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, m_SSAOsurface.m_pTopSurface);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(1, 0);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(2, 0);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(3, 0);

	// Set shader technique and variables
	m_pFX->SetTechnique(m_thSSAO);
	//m_pFX->SetValue(m_hPixelSize, D3DXVECTOR2(m_fWidth/m_SSAOsurface.m_iWidth, m_fHeight/m_SSAOsurface.m_iHeight),
	m_pFX->SetValue(m_hPixelSize, D3DXVECTOR2(1.0f/m_fWidth, 1.0f/m_fHeight), 
		sizeof(D3DXVECTOR2));	// Set Screen Size in pixels
	
	// Set Deferred texture maps
	m_pFX->SetTexture(m_hDefNormW, m_NormSurface.m_pTexture);
	m_pFX->SetTexture(m_hDefPosW, m_PosSurface.m_pTexture);
	m_pFX->SetTexture(m_hDefDepth, m_DepthSurface.m_pTexture);
	//m_pFX->SetTexture("g_sRandomTex", m_pBlackTex);
	m_pFX->CommitChanges();

	// Run the shader
	UINT numPasses = 0;
	m_pFX->Begin(&numPasses, 0);
	for(UINT p = 0; p < numPasses; p++)
	{
		m_pFX->BeginPass(p);
		/////////////////////////////////////
		DrawFullScreenQuad();
		/////////////////////////////////////
		m_pFX->EndPass();
	}
	m_pFX->End();
	
	/////////////////////////////////////
	// Debug Tools
	//m_SSAOsurface.DumpSurface(0, 0, m_SSAOsurface.m_iWidth, m_SSAOsurface.m_iHeight);
	//m_SSAOsurface.SaveToFile("SSAO");
	/////////////////////////////////////
}

void DeferredShader::RunDOF(IDirect3DSurface9* modSurface)
{
	// Set the surface to modify
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, modSurface);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(1, 0);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(2, 0);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(3, 0);
		
	m_pFX->SetTechnique(m_thDOF);	

	// Only enable DOF while right mouse button is pressed
	m_pFX->SetBool("f_UseDOF", m_bUseDOF);

	// Set Deferred texture maps
	m_pFX->SetValue(m_hPixelSize, D3DXVECTOR2(1.0f/m_fWidth, 1.0f/m_fHeight), sizeof(D3DXVECTOR2));	// Set Screen Size in pixels
	//m_pFX->SetTexture(m_hDefMtrl, m_MtrlSurface.m_pTexture);
	m_pFX->SetTexture(m_hDefNormW, m_NormSurface.m_pTexture);
	m_pFX->SetTexture(m_hDefPosW, m_PosSurface.m_pTexture);
	m_pFX->SetTexture(m_hDefDepth, m_DepthSurface.m_pTexture);
	m_pFX->SetTexture("g_sSSAOMap", m_SSAOsurface.m_pTexture);
	m_pFX->CommitChanges();

	// Depth Of Field pass
	UINT numPasses = 0;
	m_pFX->Begin(&numPasses, 0);
	for(UINT p = 0; p < numPasses; p++)
	{
		m_pFX->BeginPass(p);
		/////////////////////////////////////
		DrawFullScreenQuad();
		/////////////////////////////////////
		m_pFX->EndPass();
	}
	m_pFX->End();
}

void DeferredShader::ApplyHeatHaze()
{
	m_pFX->SetTexture("g_sNormalTex", m_pBlackTex);
	m_pFX->SetTechnique(m_thHaze);

	//D3DXVECTOR2 temp;
	//m_pFX->GetFloatArray(m_hHeatOffset1, temp, 2);
	//temp +=  DISTORTSPEED1;
	//if(temp.x > 1.0)
	//	temp.x--;
	//if(temp.y > 1.0)
	//	temp.y--;
	//m_pFX->SetFloatArray(m_hHeatOffset1, temp, 2);
	//m_pFX->GetFloatArray(m_hHeatOffset2, temp, 2);
	//temp +=  DISTORTSPEED2;
	//if(temp.x > 1.0)
	//	temp.x--;
	//if(temp.y > 1.0)
	//	temp.y--;
	//m_pFX->SetFloatArray(m_hHeatOffset2, temp, 2);
	//m_pFX->CommitChanges();

	m_pFX->Begin(NULL, NULL);
#if 0
	m_pFX->BeginPass(0);	

	DrawFullScreenQuad();
#else
	m_pFX->BeginPass(1);
	
	GCI->GetDistortionManager()->Render();

	//m_pGraphicsMgr->RenderMesh(0, "barrel1.x");
	//if(m_pTerrain)
	//	m_pTerrain->Render();
#endif
	m_pFX->EndPass();
	m_pFX->End();
}

void DeferredShader::BeginDecals()
{
	m_pFX->SetTechnique(m_thDecals);

	UINT numPasses = 0;
	m_pFX->CommitChanges();
	m_pFX->Begin(&numPasses, 0);
	m_pFX->BeginPass(0);
}

void DeferredShader::EndDecals()
{
	m_pFX->EndPass();
	m_pFX->End();
}

void DeferredShader::RenderDecal(int SharedID, std::string MeshName, IDirect3DTexture9 *texture, D3DXMATRIX decalMatrix, D3DXVECTOR3 decalLook, bool lockToObject)
{
	m_pFX->SetTexture(m_hDecalTexture, texture);
	m_pFX->SetMatrix(m_hDecalViewProj, &decalMatrix);
	m_pFX->SetFloatArray(m_hDecalNormal, decalLook, 3);
	m_pFX->SetBool(m_hDecalLock, lockToObject);
	//Matrix4 temp = decalMatrix;
	//D3DXVECTOR3 v(temp.GetAxisVector(0), temp.GetAxisVector(1), temp.GetAxisVector(2));

	m_pFX->CommitChanges();
	m_pGraphicsMgr->RenderMesh(SharedID, MeshName);
}

void DeferredShader::ApplyBlur(IDirect3DSurface9* modSurface)
{
	if(modSurface)
		m_pFX->SetTechnique(m_thBlur);
	else
		m_pFX->SetTechnique("MotionBlurDeffTech");
	//m_pFX->SetInt("g_sNumBlurSamples", 3);
	UINT numPasses = 0;
	m_pFX->CommitChanges();

	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, m_VelocityBlurSurface.m_pTopSurface);
	GCI->GetDirectX()->GetDevice()->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
						0x00000000, 1.0f, 0);
	m_pFX->Begin(&numPasses, 0);
	m_pFX->BeginPass(0);
	if(m_pTerrain)
		m_pTerrain->Render();
	
	m_pGraphicsMgr->RenderAllMeshes();
	m_pFX->EndPass();

	m_pFX->SetTexture("g_VelocityBlurTex", m_VelocityBlurSurface.m_pTexture);
	if(!modSurface)
	{
		// Blur all buffers, so that lighting is caluclated with respect to blur
		m_pFX->SetTexture(m_hDefMtrl, m_MtrlSurface.m_pTexture);
		m_pFX->SetTexture(m_hDefNormW, m_NormSurface.m_pTexture);
		m_pFX->SetTexture(m_hDefPosW, m_PosSurface.m_pTexture);
		m_pFX->SetTexture(m_hDefDepth, m_DepthSurface.m_pTexture);
		GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, m_MtrlSurface.m_pTopSurface);
		GCI->GetDirectX()->GetDevice()->SetRenderTarget(1, m_NormSurface.m_pTopSurface);
		GCI->GetDirectX()->GetDevice()->SetRenderTarget(2, m_PosSurface.m_pTopSurface);
		GCI->GetDirectX()->GetDevice()->SetRenderTarget(3, m_DepthSurface.m_pTopSurface);
	}
	else
	{
		// Blur the very final screen image
		GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, modSurface);
	}
	m_pFX->CommitChanges();
	m_pFX->BeginPass(1);
	DrawFullScreenQuad();
	m_pFX->EndPass();
	m_pFX->End();
	
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(1, NULL);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(2, NULL);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(3, NULL);
	
	// Debug tool
	//static float count = 0.0f;
	//if(count > 1.0f)
	//{
	//	m_MtrlSurface.SaveToFile("Mtrl");
	//	//m_VelocityBlurSurface.SaveToFile("VelocityBuff");
	//	count = 0.0f;
	//}
	//else
	//	count += GCI->GetTimeElapsed();
}
void DeferredShader::SetBlurScale(float scale)
{
	m_pFX->SetFloat(m_hBlurScale, scale);
	m_pFX->CommitChanges();
}

/*
void DeferredShader::BlurTexMap(IDirect3DTexture9* blurTex)
{
	// Set the output surface
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, output);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(1, 0);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(2, 0);
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(3, 0);

	// Set shader technique and variables
	m_pFX->SetTechnique(m_thSSAO);
	m_pFX->SetValue(m_hPixelSize, D3DXVECTOR2(1.0f/m_fWidth, 1.0f/m_fHeight), sizeof(D3DXVECTOR2));	// Set Screen Size in pixels
	
	// Set Deferred texture maps
	m_pFX->SetTexture(m_hDefNormW, m_NormSurface.m_pTexture);
	m_pFX->SetTexture(m_hDefPosW, m_PosSurface.m_pTexture);
	m_pFX->SetTexture(m_hDefDepth, m_DepthSurface.m_pTexture);
	m_pFX->SetTexture("g_sRandomTex", m_pBlackTex);
	m_pFX->CommitChanges();

	// Run the shader
	UINT numPasses = 0;
	m_pFX->Begin(&numPasses, 0);
	for(UINT p = 0; p < numPasses; p++)
	{
		m_pFX->BeginPass(p);
		/////////////////////////////////////
		DrawFullScreenQuad();
		/////////////////////////////////////
		m_pFX->EndPass();
	}
	m_pFX->End();
}
*/

void DeferredShader::BeginElectricity()
{
	m_pFX->SetTechnique(m_thElectricity);

	UINT numPasses = 0;
	m_pFX->Begin(&numPasses, 0);
	m_pFX->BeginPass(0);
}

void DeferredShader::EndElectricity()
{
	m_pFX->EndPass();
	m_pFX->End();
}

void DeferredShader::ApplyElectricityProperties(D3DXMATRIX *world, Vector3 startPos, Vector3 endPos,
		float boltScale, float ArcScale, float midpoint, int randOffset,
		D3DXVECTOR4 color)
{
	m_pFX->SetMatrix(m_hWorld, world);
	m_pFX->SetFloatArray(m_hElectricStartPos, startPos, 3);
	m_pFX->SetFloatArray(m_hElectricEndPos, endPos, 3);
	m_pFX->SetFloat(m_hElectricSize, boltScale);
	m_pFX->SetFloat(m_hElectricArcScale, ArcScale);
	m_pFX->SetFloat(m_hRandOffset, float(randOffset));
	m_pFX->SetFloat(m_hElectricArcMidpoint, midpoint);
	m_pFX->SetFloatArray(m_hElectricColor, color, 4);
	m_pFX->CommitChanges();
}

//void DeferredShader::RenderElectricity()
//{
//}

bool DeferredShader::CheckDeviceCaps()
{
	D3DCAPS9 caps;		// Device Capabilities
	HRESULT hr = GCI->GetDirectX()->GetDevice()->GetDeviceCaps(&caps);

	// Check vertex shader 3.0
	if(caps.VertexShaderVersion < D3DVS_VERSION(3,0))
		return false;

	// Check pixel shader 3.0
	if(caps.PixelShaderVersion < D3DPS_VERSION(3,0))
		return false;

	return true;
}

// Switch shaders on/off
void DeferredShader::UseHDR(bool enable)
{	m_bUseHDR = enable;	}
void DeferredShader::UseSSAO(bool enable)
{	m_bUseSSAO = enable;	}
void DeferredShader::UseDOF(bool enable)
{	m_bUseDOF = enable;	}
void DeferredShader::UseShadowMap(bool enable)
{	m_bUseShadowMap = enable;	}
void DeferredShader::UseHeatHaze(bool enable)
{	m_bUseHeatHaze = enable;	}
void DeferredShader::UseDecals(bool enable)
{	m_bUseDecal = enable;	}
void DeferredShader::UseBlur(bool enable)
{	m_bUseBlur = enable;	}

// Get status of shaders
bool DeferredShader::IsUsingHDR()
{	return m_bUseHDR;	}
bool DeferredShader::IsUsingSSAO()
{	return m_bUseSSAO;	}
bool DeferredShader::IsUsingDOF()
{	return m_bUseDOF;	}
bool DeferredShader::IsUsingShadowMap()
{	return m_bUseShadowMap;	}
bool DeferredShader::NeedToUpdateShadowMap()
{
	// Check if we are even using the shadow map
	if(!m_bUseShadowMap)
		return false;

	// This will decide if the shadow map needs to be updated and then reset the flag
	bool result = m_Light.NeedToUpdateLight();
	if(result)
		m_Light.LightHasBeenUpdated();
	return result;
}
bool DeferredShader::IsUsingHeatHaze()
{	return m_bUseHeatHaze;	}

void DeferredShader::AddNumObjDrawn(unsigned int numObj)
{	
	m_iNumObjDrawn += numObj;
	m_fNumObjDrawn += (float)numObj;
}
int* DeferredShader::GetNumObjDrawn()
{	return &m_iNumObjDrawn;	}
float* DeferredShader::GetNumObjDrawnFloat()
{	return &m_fNumObjDrawn;	}
void DeferredShader::SetGlareType(EGlareType glareType)
{
	m_HDR.SetGlareType(glareType);
}

// Light Controller
int DeferredShader::AddLight(D3DXVECTOR4 pos, D3DXVECTOR4 intensity, int power)
{
	return m_HDR.AddLight(pos, intensity, power);
}
void DeferredShader::ChangeLightPos(int light, D3DXVECTOR4 pos)
{
	m_HDR.ChangeLightPos(light, pos);
}
void DeferredShader::ChangeLightPower(int id, int power)
{
	m_HDR.ChangeLightPower(id, power);
}
void DeferredShader::RemoveLight(int light)
{
	m_HDR.RemoveLight(light);
}
void DeferredShader::RemoveAllLights()
{
	m_HDR.RemoveAllLights();
}

// Debug Save Buffer to image
void DeferredShader::PrintBuffer(std::string buffer)
{	
	/////////////////////////////////////
	// Debug Tools: Save to an image file
	if(buffer == "Depth")
		m_DepthSurface.SaveToFile("Depth");
	else if(buffer == "Pos")
		m_PosSurface.SaveToFile("Pos");
	else if(buffer == "Norm")
		m_NormSurface.SaveToFile("Norm");
	else if(buffer == "Mtrl")
		m_MtrlSurface.SaveToFile("Mtrl");
	else if(buffer == "ShadowDepth")
		m_ShadowSurface.SaveToFile("ShadowDepth");
	else if(buffer == "SSAO")
		m_SSAOsurface.SaveToFile("SSAO");
	/////////////////////////////////////
}

// Alpha Quad Rendering Is tricky in Deferred Shading.
// We register the Graphics Manager so that we can 
// render it's alpha Quads at the right time.
void DeferredShader::RegisterRendering(GraphicsManager* gMgr)
{	m_pGraphicsMgr = gMgr;	}


///////////////////////////////////////////////////////////////////////
struct PlaneVertex {
		float x, y, z, w;
		float u, v;
	};
void DeferredShader::DrawFullScreenQuad()
{
	// create plane once (screen size) on which the scene is rendered
	static PlaneVertex axPlaneVertices[] =
	{
		{ -0.5f,		 -0.5f,			 0.5f, 1.0f, 0.0f, 0.0f }, //0 + 0.5f / m_fWidth,	0 + 0.5f / m_fHeight },
		{ m_fWidth-0.5f, -0.5f,			 0.5f, 1.0f, 1.0f, 0.0f }, //1 + 0.5f / m_fWidth,	0 + 0.5f / m_fHeight },
		{ m_fWidth-0.5f, m_fHeight-0.5f, 0.5f, 1.0f, 1.0f, 1.0f }, //1 + 0.5f / m_fWidth,	1 + 0.5f / m_fHeight },
		{ -0.5f,		 m_fHeight-0.5f, 0.5f, 1.0f, 0.0f, 1.0f }  //0 + 0.5f / m_fWidth,	1 + 0.5f / m_fHeight }
	};

	if(axPlaneVertices[2].x != m_fWidth-0.5f || 
		axPlaneVertices[2].y != m_fHeight-0.5f)	// Adjust Quad when Screen Size changes
	{
		PlaneVertex temp[] = 
		{
			{ -0.5f,		 -0.5f,			 0.5f, 1.0f, 0.0f, 0.0f }, //0 + 0.5f / m_fWidth,	0 + 0.5f / m_fHeight },
			{ m_fWidth-0.5f, -0.5f,			 0.5f, 1.0f, 1.0f, 0.0f }, //1 + 0.5f / m_fWidth,	0 + 0.5f / m_fHeight },
			{ m_fWidth-0.5f, m_fHeight-0.5f, 0.5f, 1.0f, 1.0f, 1.0f }, //1 + 0.5f / m_fWidth,	1 + 0.5f / m_fHeight },
			{ -0.5f,		 m_fHeight-0.5f, 0.5f, 1.0f, 0.0f, 1.0f }  //0 + 0.5f / m_fWidth,	1 + 0.5f / m_fHeight }
		};
		memcpy(&axPlaneVertices, &temp, sizeof(temp));
	}
	

	// Draw the Full ScreenQuad
	GCI->GetDirectX()->GetDevice()->SetFVF(D3DFVF_XYZRHW | D3DFVF_TEX1);
	GCI->GetDirectX()->GetDevice()->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, 2, axPlaneVertices, sizeof(PlaneVertex));

	//QuadCoord temp= {0, 0, m_fWidth, m_fHeight};
	//DrawFullScreenQuad(temp);
}
void DeferredShader::DrawFullScreenQuad(QuadCoord coords)
{
	D3DSURFACE_DESC		desc;
	IDirect3DSurface9*	pRTsurface;

	// Get the width and height of the render target
	GCI->GetDirectX()->GetDevice()->GetRenderTarget(0, &pRTsurface);
	pRTsurface->GetDesc(&desc);
	SAFE_RELEASE(pRTsurface);

	// Directly map texels to pixels by 0.5 offset
	float width = (float)desc.Width - 0.5f;
	float height = (float)desc.Height - 0.5f;

	// Create fullscreen quad to rendered on
	PlaneVertex screenVerts[] = 
	{
		{ -0.5f, -0.5f,  0.5f, 1.0f,	coords.m_LeftU,		coords.m_TopV },
		{ width, -0.5f,	 0.5f, 1.0f,	coords.m_RightU,	coords.m_TopV },
		{ -0.5f, height, 0.5f, 1.0f,	coords.m_LeftU,		coords.m_BottomV },
		{ width, height, 0.5f, 1.0f,	coords.m_RightU,	coords.m_BottomV }
	};

	// Draw the Full ScreenQuad
	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_ZENABLE, FALSE);
	GCI->GetDirectX()->GetDevice()->SetFVF(D3DFVF_XYZRHW | D3DFVF_TEX1);
	GCI->GetDirectX()->GetDevice()->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, screenVerts, sizeof(PlaneVertex));
	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_ZENABLE, TRUE);
}

void DeferredShader::ReloadShader()
{
	// Safely release old shader 
	SAFE_RELEASE( m_pFX );

	// Create the Effect File
	ID3DXBuffer* FX_Errors;	// Stores the Error Codes
#if defined(DEBUG) | defined(_DEBUG)
	D3DXCreateEffectFromFile(GCI->GetDirectX()->GetDevice(), "Content/Shaders/DeferredShader.fx",	//"Graphics/3D/Textures/DeferredShader.fx", 
					0, 0, 0, 0, &m_pFX, &FX_Errors);
#else
	// Load compiled effect
	std::ifstream file( "Content/Shaders/DeferredShader.fxo", std::ios::binary );
	file.seekg(0, std::ios_base::end);
	int size = (int)file.tellg();
	file.seekg(0, std::ios_base::beg);
	std::vector<char> compiledShader(size);

	file.read(&compiledShader[0], size);
	file.close();
	D3DXCreateEffect(GCI->GetDirectX()->GetDevice(), &compiledShader[0], size, 
					0, 0, 0, 0, &m_pFX, &FX_Errors);
#endif
	if(FX_Errors)		// Output if there is any errors
		MessageBoxA(0, (char*)FX_Errors->GetBufferPointer(), 0, 0);

	// Set the Technique
	m_thMain	= m_pFX->GetTechniqueByName("MainTech");
	m_thLight	= m_pFX->GetTechniqueByName("DefLightTech");
	m_thDOF		= m_pFX->GetTechniqueByName("DofTech");
	m_thSSAO	= m_pFX->GetTechniqueByName("SSAOTech");
	m_thHaze	= m_pFX->GetTechniqueByName("Haze");
	m_thDecals	= m_pFX->GetTechniqueByName("DecalTech");
	m_thBlur	= m_pFX->GetTechniqueByName("MotionBlurTech");
	m_thElectricity = m_pFX->GetTechniqueByName("ElectricityTech");

	m_pFX->SetTechnique(m_thMain);

	// Set handles to FX parameters
	m_hWVP				= m_pFX->GetParameterByName(0, "g_sWVP");
	m_hWorldInvTrans	= m_pFX->GetParameterByName(0, "g_sWorldInvTrans");
	m_hWorld			= m_pFX->GetParameterByName(0, "g_sWorld");
	m_hCameraPos		= m_pFX->GetParameterByName(0, "g_sCameraPos");
	m_hPixelSize		= m_pFX->GetParameterByName(0, "g_sPixelSize");
	m_hLightPos			= m_pFX->GetParameterByName(0, "g_sLightPosW");
	m_hDiffuseLight		= m_pFX->GetParameterByName(0, "g_sDiffuseLight");
	m_hAmbientLight		= m_pFX->GetParameterByName(0, "g_sAmbientLight");
	m_hSpecLight		= m_pFX->GetParameterByName(0, "g_sSpecLight");
	m_hSpecPower		= m_pFX->GetParameterByName(0, "g_sSpecPower");
	m_hDiffuseMtrl		= m_pFX->GetParameterByName(0, "g_sDiffuseMtrl");
	m_hAmbientMtrl		= m_pFX->GetParameterByName(0, "g_sAmbientMtrl");
	m_hSpecMtrl			= m_pFX->GetParameterByName(0, "g_sSpecMtrl");
	m_hLightAttenuation	= m_pFX->GetParameterByName(0, "g_sAttenuation");
	m_hHeatBump1		= m_pFX->GetParameterByName(0, "g_fBump1");
	m_hHeatBump2		= m_pFX->GetParameterByName(0, "g_fBump2");
	m_hHeatOffset1		= m_pFX->GetParameterByName(0, "g_fNormTexOffset1");
	m_hHeatOffset2		= m_pFX->GetParameterByName(0, "g_fNormTexOffset2");
	m_hHeatIntensity1	= m_pFX->GetParameterByName(0, "g_fHeatIntensity1");
	m_hHeatIntensity2	= m_pFX->GetParameterByName(0, "g_fHeatIntensity2");
	m_hHeatScale1		= m_pFX->GetParameterByName(0, "g_fHeatScale1");
	m_hHeatScale2		= m_pFX->GetParameterByName(0, "g_fHeatScale2");

	m_hBlurScale		= m_pFX->GetParameterByName(0, "g_fVelScale");

	m_hDecalViewProj	= m_pFX->GetParameterByName(0, "g_mDecalMatrix");
	m_hDecalNormal		= m_pFX->GetParameterByName(0, "g_vDecalDir");
	m_hDecalTexture		= m_pFX->GetParameterByName(0, "m_gDecalTexture");
	m_hDecalLock		= m_pFX->GetParameterByName(0, "m_DecalLock");

	m_hElectricStartPos	= m_pFX->GetParameterByName(0, "g_sElectricStartPos");
	m_hElectricEndPos	= m_pFX->GetParameterByName(0, "g_sElectricEndPos");
	m_hElectricArcScale	= m_pFX->GetParameterByName(0, "g_sElectricArcScale");
	m_hRandOffset		= m_pFX->GetParameterByName(0, "g_sRandOffset");
	m_hElectricSize		= m_pFX->GetParameterByName(0, "g_sElectricSize");
	m_hElectricArcMidpoint	= m_pFX->GetParameterByName(0, "g_sElectricArcMidpoint");
	m_hElectricTexture	= m_pFX->GetParameterByName(0, "ArcColorTex");
	m_hElectricColor	= m_pFX->GetParameterByName(0, "g_sElectricColor");

	// Flags
	m_hPhong			= m_pFX->GetParameterByName(0, "f_Phong");
	m_hBlinnPhong		= m_pFX->GetParameterByName(0, "f_BlinnPhong");
	m_hIsTex			= m_pFX->GetParameterByName(0, "f_IsTex");
	
	// Set handles to the Deferred parameters
	m_hDefMtrl			= m_pFX->GetParameterByName(0, "g_sDeferredMtrl");
	m_hDefNormW			= m_pFX->GetParameterByName(0, "g_sDeferredNormW");
	m_hDefPosW			= m_pFX->GetParameterByName(0, "g_sDeferredPosW");
	m_hDefDepth			= m_pFX->GetParameterByName(0, "g_sDeferredDepth");
	
	if( m_pBlackTex )
		m_pFX->SetTexture("g_sNormalTex", m_pBlackTex);
	m_pFX->SetFloat(m_hBlurScale, 1.0f);

	// Load HDR values
	m_HDR.ReloadShader();
}