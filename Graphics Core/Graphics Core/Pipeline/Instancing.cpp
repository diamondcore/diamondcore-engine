#include "Instancing.h"
#include "../GraphicsCore.h"
#include "../MacroTools.h"

Instancing::Instancing()
{
	vec4Count			= 0;
	instCount			= 0;
	numVerts			= 0;
	numIndex			= 0;
	InstVertexBuffer	= 0;
	InstIndexBuffer		= 0;
	m_bIsImposter		= 0;
	D3DXMatrixIdentity(&m_WorldMat);
}
Instancing::~Instancing()
{}

void Instancing::Create(std::string texture)
{
	m_TexName = texture;
	BuildInstVertexBuff();
	BuildInstIndexBuff();
}
void Instancing::Shutdown()
{
	SAFE_RELEASE(InstVertexBuffer);
	SAFE_RELEASE(InstIndexBuffer);
}

void Instancing::AddBachInstance(D3DXMATRIX worldTransform, D3DXCOLOR color)
{
	if(instCount >= 44)	// Make sure the user doesn't go over the maximum number of instances
	{
		MessageBox(0, "You can not create more the 44 instances, per instance object.", "Warning", MB_OK);
		return;
	}
	instCount++;
	instanceData[vec4Count++] = *(D3DXVECTOR4*) &worldTransform._11;	// First row of the matrix
	instanceData[vec4Count++] = *(D3DXVECTOR4*) &worldTransform._21;	// Second row of the matrix
	instanceData[vec4Count++] = *(D3DXVECTOR4*) &worldTransform._31;	// Third row of the matrix
	instanceData[vec4Count++] = *(D3DXVECTOR4*) &worldTransform._41;	// Fourth row of the matrix
	instanceData[vec4Count++] = *(D3DXVECTOR4*) &color;
}

void Instancing::AddDynamicInstance(D3DXMATRIX worldTransform, D3DXCOLOR color)
{
	if(instCount >= 44)	// Make sure the user doesn't go over the maximum number of instances
	{
		MessageBox(0, "You can not create more the 44 instances, per instance object.", "Warning", MB_OK);
		return;
	}
	instCount++;
	instanceData[vec4Count++] = *(D3DXVECTOR4*) &worldTransform._11;	// First row of the matrix
	instanceData[vec4Count++] = *(D3DXVECTOR4*) &worldTransform._21;	// Second row of the matrix
	instanceData[vec4Count++] = *(D3DXVECTOR4*) &worldTransform._31;	// Third row of the matrix
	instanceData[vec4Count++] = *(D3DXVECTOR4*) &worldTransform._41;	// Fourth row of the matrix
	instanceData[vec4Count++] = *(D3DXVECTOR4*) &color;

	// Rebuild the buffer to include the new instance
	Shutdown();
	Create(m_TexName);
}

void Instancing::Update(unsigned int instID, D3DXMATRIX worldTransform)
{
	instID = instID + (instID*4);	// Get the corresponding index in the array
	instanceData[instID]   = *(D3DXVECTOR4*) &worldTransform._11;
	instanceData[instID++] = *(D3DXVECTOR4*) &worldTransform._21;
	instanceData[instID++] = *(D3DXVECTOR4*) &worldTransform._31;
	instanceData[instID++] = *(D3DXVECTOR4*) &worldTransform._41;
}

void Instancing::Update(unsigned int instID, D3DXCOLOR color)
{
	instanceData[(instID + (instID*4)+4)]   = *(D3DXVECTOR4*) &color;	// Get the corresponding index in the array
}

void Instancing::BuildInstVertexBuff()
{
	// Create Vertex Buffer
	HRESULT hr;
	numVerts = 4*instCount;
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexBuffer(numVerts* sizeof(VertexInstPNT), 
				D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED, &InstVertexBuffer, 0);

	// Create Quad Vertices
	VertexInstPNT	*quadVerts = 0;

	// Edit Buffer
	hr = InstVertexBuffer->Lock(0, 0, (void**)&quadVerts, 0);
	
	// Front
	for(unsigned int i = 0; i < numVerts; i+=4)
	{
		quadVerts[i+0].position = D3DXVECTOR3(-0.5f, -0.5f, 0.0f);
		quadVerts[i+1].position = D3DXVECTOR3(-0.5f, 0.5f, 0.0f);
		quadVerts[i+2].position = D3DXVECTOR3(0.5f, 0.5f, 0.0f);
		quadVerts[i+3].position = D3DXVECTOR3(0.5f, -0.5f, 0.0f);
		D3DXVec3Normalize(&quadVerts[i+0].normal, &D3DXVECTOR3(0.0f, 0.0f, 1.0f));
		D3DXVec3Normalize(&quadVerts[i+1].normal, &D3DXVECTOR3(0.0f, 0.0f, 1.0f));
		D3DXVec3Normalize(&quadVerts[i+2].normal, &D3DXVECTOR3(0.0f, 0.0f, 1.0f));
		D3DXVec3Normalize(&quadVerts[i+3].normal, &D3DXVECTOR3(0.0f, 0.0f, 1.0f));
		quadVerts[i+0].texturePos = D3DXVECTOR2(0.0f, 0.5f);
		quadVerts[i+1].texturePos = D3DXVECTOR2(0.0f, 0.0f);
		quadVerts[i+2].texturePos = D3DXVECTOR2(0.5f, 0.0f);
		quadVerts[i+3].texturePos = D3DXVECTOR2(0.5f, 0.5f);

		// IndexID for this instance
		int id = i + (i/4);
		quadVerts[i+0].instID = id;
		quadVerts[i+1].instID = id;
		quadVerts[i+2].instID = id;
		quadVerts[i+3].instID = id;
	}

	hr = InstVertexBuffer->Unlock();
}

void Instancing::BuildInstIndexBuff()
{
	// Create Index Buffer (Dynamic)
	numIndex = 6*instCount;
	HRESULT hr = GCI->GetDirectX()->GetDevice()->CreateIndexBuffer(numIndex *sizeof(WORD), 
					D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &InstIndexBuffer, 0);
	
	WORD* iQuad = 0;

	// Access Buffer
	hr = InstIndexBuffer->Lock(0,0, (void**)&iQuad, 0);

	// Front face
	unsigned int v = 0;
	for(unsigned int i = 0; i < numIndex; i+=6)
	{
		iQuad[i+0] = v+0; iQuad[i+1] = v+1; iQuad[i+2] = v+2;		// Triangle 0
		iQuad[i+3] = v+0; iQuad[i+4] = v+2; iQuad[i+5] = v+3;		// Triangle 1
		v += 4;		// Vertex Incrementer
	}

	hr = InstIndexBuffer->Unlock();	// Done accessing
}

void Instancing::SetWorldMat(D3DXMATRIX world)
{	m_WorldMat = world;	}
void Instancing::SetAlwaysFaceCamera(bool enable)
{	m_bIsImposter = enable;	}

void Instancing::Render()
{
	DirectXClass* DXI = GCI->GetDirectX();
	DeferredShader* DSX = GCI->GetDeferredR();

	// Set the Stream source
	HRESULT hr = DXI->GetDevice()->SetVertexDeclaration(VertexInstPNT::Decl);
	hr = DXI->GetDevice()->SetStreamSource(0, InstVertexBuffer, 0, sizeof(VertexInstPNT));
	hr = DXI->GetDevice()->SetIndices(InstIndexBuffer);

	// Set the Instancing Data
	hr = DSX->m_pFX->SetBool("g_sDrawImposter", m_bIsImposter);
	hr = DSX->m_pFX->SetVectorArray("g_sInstancingData", instanceData, vec4Count);
	hr = DSX->m_pFX->SetTexture("g_sTexture", GCI->GetTextureManager()->TextureList[m_TexName].texture);
	
	// Set world matrix
	DSX->m_pFX->SetMatrix(DSX->m_hWorld, &m_WorldMat);
	DSX->m_pFX->SetMatrix(DSX->m_hWVP, &(m_WorldMat*GCI->GetCamera()->GetViewProj()));

	// Draw Indexed Primitives
	hr = DSX->m_pFX->CommitChanges();
	hr = DXI->GetDevice()->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, numVerts, 
									0, numIndex/3);	

	// Add to the number of objects drawn
	DSX->AddNumObjDrawn( instCount );
}

int Instancing::GetNumInstances()
{	return instCount;	}
