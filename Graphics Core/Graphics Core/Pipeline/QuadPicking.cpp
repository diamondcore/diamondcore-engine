#include "QuadPicking.h"
#include "../GraphicsCore.h"
#include "../MacroTools.h"

QuadPicking::QuadPicking()
	:m_dirW(0.0f, 0.0f, 0.0f),
	m_originW(0.0f, 0.0f, 0.0f),
	m_iSelectedObjID(-1),
	m_fSelectedDist(FLT_MAX)
{
	m_pMesh	= 0;
}
QuadPicking::~QuadPicking()
{
	// Back-up release incase somebody forgets to shutdown
	Shutdown();
}
void QuadPicking::Init()
{
	// Declare Vertex Element
	D3DVERTEXELEMENT9 vertElement[64];
	UINT numElements = 0;
	HRESULT hr = VertexInstPNT::Decl->GetDeclaration(vertElement, &numElements);

	// Create a mesh
	hr = D3DXCreateMesh(2, 4, D3DXMESH_MANAGED, 
			vertElement, GCI->GetDirectX()->GetDevice(), &m_pMesh);

	// Create Vertex's
	VertexInstPNT*	quadVerts = 0;
	int i = 0;

	hr = m_pMesh->LockVertexBuffer(0, (void**)&quadVerts);
	quadVerts[i+0].position = D3DXVECTOR3(-0.5f, -0.5f, 0.0f);
	quadVerts[i+1].position = D3DXVECTOR3(-0.5f, 0.5f, 0.0f);
	quadVerts[i+2].position = D3DXVECTOR3(0.5f, 0.5f, 0.0f);
	quadVerts[i+3].position = D3DXVECTOR3(0.5f, -0.5f, 0.0f);
	D3DXVec3Normalize(&quadVerts[i+0].normal, &D3DXVECTOR3(0.0f, 0.0f, 1.0f));
	D3DXVec3Normalize(&quadVerts[i+1].normal, &D3DXVECTOR3(0.0f, 0.0f, 1.0f));
	D3DXVec3Normalize(&quadVerts[i+2].normal, &D3DXVECTOR3(0.0f, 0.0f, 1.0f));
	D3DXVec3Normalize(&quadVerts[i+3].normal, &D3DXVECTOR3(0.0f, 0.0f, 1.0f));
	quadVerts[i+0].texturePos = D3DXVECTOR2(0.0f, 0.5f);
	quadVerts[i+1].texturePos = D3DXVECTOR2(0.0f, 0.0f);
	quadVerts[i+2].texturePos = D3DXVECTOR2(0.5f, 0.0f);
	quadVerts[i+3].texturePos = D3DXVECTOR2(0.5f, 0.5f);

	// IndexID for this instance
	int id = i + (i/4);
	quadVerts[i+0].instID = id;
	quadVerts[i+1].instID = id;
	quadVerts[i+2].instID = id;
	quadVerts[i+3].instID = id;
	hr = m_pMesh->UnlockVertexBuffer();

	// Create Indices
	WORD* iQuad = 0;
	unsigned int v = 0;
	hr = m_pMesh->LockIndexBuffer(0, (void**)&iQuad);
	iQuad[i+0] = v+0; iQuad[i+1] = v+1; iQuad[i+2] = v+2;		// Triangle 0
	iQuad[i+3] = v+0; iQuad[i+4] = v+2; iQuad[i+5] = v+3;		// Triangle 1
	hr = m_pMesh->UnlockIndexBuffer();
}
void QuadPicking::Shutdown()
{
	m_iSelectedObjID = -1;
	m_fSelectedDist = FLT_MAX;
	SAFE_RELEASE(m_pMesh);
}

void QuadPicking::CreatePickingRayW(D3DXVECTOR3& originW,D3DXVECTOR3& dirW)
{
	auto DXI = GCI->GetDirectX();
	auto camera = GCI->GetCamera();

	//Get the screen point clicked.
	POINT s;
	GetCursorPos(&s);

	// Make it relative to the client area window.
	ScreenToClient(DXI->GetWindowHandle(), &s);
	
	// By the way we've been constructing things, the entire 
	// backbuffer is the viewport.

	float w = (float)DXI->GetScreenWidth();
	float h = (float)DXI->GetScreenHeight();

	D3DXMATRIX proj = camera->GetProj();

	float x = (2.0f*s.x/w - 1.0f) / proj(0,0);
	float y = (-2.0f*s.y/h + 1.0f) / proj(1,1);

	// Build picking ray in view space.
	D3DXVECTOR3 origin(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 dir(x, y, 1.0f);

	// So if the view matrix transforms coordinates from 
	// world space to view space, then the inverse of the
	// view matrix transforms coordinates from view space
	// to world space.
	D3DXMATRIX invView;
	D3DXMatrixInverse(&invView, 0, &camera->GetView());

	// Transform picking ray to world space.
	D3DXVec3TransformCoord(&originW, &origin, &invView);
	D3DXVec3TransformNormal(&dirW, &dir, &invView);
	D3DXVec3Normalize(&dirW, &dirW);


}
void QuadPicking::UpdateRay()
{
	CreatePickingRayW(m_originW, m_dirW);
	
	// Reset previouse selection
	m_cSelectedObjName = "";
	m_iSelectedObjID = -1;
	m_fSelectedDist = FLT_MAX;
}
bool QuadPicking::PickingChecks(D3DXMATRIX transform)
{
	// Set local copies of variables
	D3DXVECTOR3 originW = m_originW;
	D3DXVECTOR3 dirW = m_dirW;

	BOOL hit = 0;
	float dist = 0.0f;

	// Use inverse of matrix
	D3DXMATRIX matInverse;
	D3DXMatrixInverse(&matInverse,NULL,&transform);

	D3DXVec3TransformCoord(&originW,&originW,&matInverse);
	D3DXVec3TransformNormal(&dirW,&dirW,&matInverse);
	D3DXVec3Normalize(&dirW,&dirW);

	D3DXIntersect(m_pMesh, &originW, &dirW, &hit,NULL, NULL, NULL, &dist, NULL, NULL);
			
	// We hit anything?
	if( hit )
	{
		return true;
				
	}
	return false;
}
bool QuadPicking::PickingPos(ID3DXMesh** mesh, D3DXVECTOR3* pos)
{
	// Set local copies of variables
	D3DXVECTOR3 originW = m_originW;
	D3DXVECTOR3 dirW = m_dirW;

	BOOL hit = 0;
	float dist = 0.0f;

	// Use inverse of matrix
	D3DXMATRIX matInverse, transform;
	D3DXMatrixIdentity(&transform);
	D3DXMatrixInverse(&matInverse,NULL,&transform);

	D3DXVec3TransformCoord(&originW,&originW,&matInverse);
	D3DXVec3TransformNormal(&dirW,&dirW,&matInverse);
	D3DXVec3Normalize(&dirW,&dirW);

	D3DXIntersect(*mesh, &originW, &dirW, &hit,NULL, NULL, NULL, &dist, NULL, NULL);
			
	// We hit anything?
	if( hit )
	{
		// Set the hit position
		*pos = originW;
		*pos += dirW * dist;
		return true;		
	}
	return false;
}

bool QuadPicking::PickingSelect(ID3DXMesh** mesh, D3DXMATRIX transform, std::string objName, int objID)
{
	// Set local copies of variables
	D3DXVECTOR3 originW = m_originW;
	D3DXVECTOR3 dirW = m_dirW;

	BOOL hit = 0;
	float dist = 0.0f;

	// Use inverse of matrix
	D3DXMATRIX matInverse;
	D3DXMatrixInverse(&matInverse,NULL,&transform);

	D3DXVec3TransformCoord(&originW,&originW,&matInverse);
	D3DXVec3TransformNormal(&dirW,&dirW,&matInverse);
	D3DXVec3Normalize(&dirW,&dirW);

	D3DXIntersect((*mesh), &originW, &dirW, &hit,NULL, NULL, NULL, &dist, NULL, NULL);
				
	// We hit anything?
	if( hit )
	{
		// Set the hit position
		if(dist < m_fSelectedDist)
		{
			m_cSelectedObjName = objName;
			m_iSelectedObjID = objID;
			m_fSelectedDist = dist;
			return true;
		}
	}
	return false;
}

// Selecting object tools
void QuadPicking::GetSelectedObj(std::string& objName, int& objID)
{
	objName = m_cSelectedObjName;
	objID = m_iSelectedObjID;
}
