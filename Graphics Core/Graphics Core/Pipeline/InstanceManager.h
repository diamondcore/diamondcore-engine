#pragma once

#include "Instancing.h"
#include <map>
#include <vector>
#include <memory>
#include "../SceneGraph/MeshList.h"

class InstanceManager
{
	std::vector< std::shared_ptr<Instancing> >			m_InstanceList;
	std::map<std::string, std::shared_ptr<Instancing>>	m_InstanceMap;

public:
	InstanceManager();
	~InstanceManager();
	
	void	Init();
	void	Shutdown();

	void	AddInstance(std::string instName, std::string texture, D3DXMATRIX transforms[], D3DXCOLOR colors[], int count);
	void	RemoveInstance(std::string instName);
	
	void	UpdateInstance(std::string instName, std::vector<BaseObjectPtr> objects);
	void	UpdateInstance(std::string instName, D3DXMATRIX transforms[], int count);
	void	UpdateInstance(std::string instName, D3DXCOLOR colors[], int count);
	void	SetWorldMat(std::string instName, D3DXMATRIX world);

	void	Render(std::string instance);	// Renders Specific instances
	void	Render();						// Renders All
};