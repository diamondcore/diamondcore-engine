#include "Billboards.h"
#include "../GraphicsCore.h"
#include "../MacroTools.h"
#include <MemoryManagement/Module.h>

Billboards::Billboards(std::string texture)
	:m_TexName(texture)
{
	m_iVec4Count		= 0;
	m_iInstCount		= 0;
	m_iNumVerts			= 0;
	m_iNumIndex			= 0;
	m_pInstVertexBuffer	= 0;
	m_pInstIndexBuffer	= 0;
	m_bIsImposter		= 0;
	m_pSibling			= 0;
	D3DXMatrixIdentity(&m_WorldMat);
}
Billboards::~Billboards()
{
	m_pSibling = 0;

	if(m_pInstIndexBuffer)
		Shutdown();
	m_Registry.clear();
}

void Billboards::Init()
{
	BuildInstVertexBuff();
	BuildInstIndexBuff();
}
void Billboards::Shutdown()
{
	SAFE_RELEASE(m_pInstVertexBuffer);
	SAFE_RELEASE(m_pInstIndexBuffer);
}

void Billboards::AddInstance(int sharedMemID, bool shadowMap)
{
	if(m_iInstCount >= 44)	// Make sure the user doesn't go over the maximum number of instances
	{
		if(!m_pSibling)	// Need to create the sibling if it hasn't been created yet
			m_pSibling = std::shared_ptr<Billboards>(new Billboards(m_TexName));	

		// Add the instance to the sibling
		m_pSibling->AddInstance(sharedMemID, shadowMap);	// Send this id to a sibling instance
	}
	else
	{
		// Increment the billboard count
		m_iInstCount++;
		m_iVec4Count += 5;
	
		// Add to the registry
		m_Registry.insert( std::pair<int, BaseObjectPtr>
			(sharedMemID, std::shared_ptr<BaseObject>(new BaseObject)) );
		
		// Set the variables
		m_Registry[sharedMemID]->m_bShadowMap = shadowMap;
		m_Registry[sharedMemID]->m_SharedMemID = sharedMemID;

		// Rebuild the buffers with the edited data
		Shutdown();
		Init();
	}
}
void Billboards::RemoveInstance(int sharedMemID)
{
	auto iter = m_Registry.find( sharedMemID );
	if( iter == m_Registry.end() )	// Check if this object is in this instance
	{
		// The object is not in this instance
		if(m_pSibling)	// If sibling, then try to remove from sibling
			m_pSibling->RemoveInstance( sharedMemID );
		return;
	}
	
	// Decrement count
	m_iInstCount--;
	m_iVec4Count -= 5;

	// Remove object
	(*iter).second = 0;
	m_Registry.erase( iter );
}

void Billboards::Update(Module* sharedMem)
{
	for(auto i = m_Registry.begin();
		i != m_Registry.end(); i++)
	{
		// Update the information with the shared memory
		(*i).second->m_Tranform.m_Position = sharedMem->GetSharedResource((*i).second->m_SharedMemID)->GetPosition();
		(*i).second->m_Tranform.m_Quaternion =  sharedMem->GetSharedResource((*i).second->m_SharedMemID)->GetOrientation();
	}
	
	// Update the sibling if there is one
	if(m_pSibling)
		m_pSibling->Update(sharedMem);
}

void Billboards::Render(bool shadowMap)
{
	int vID = 0;
	D3DXMATRIX temp;

	// Set the Instance Array
	for(auto i = m_Registry.begin();
		i != m_Registry.end(); i++)
	{
		// If using shadow map, then only render the shadow map
		if(shadowMap &&	!(*i).second->m_bShadowMap)
			continue;

		// Get the transorm
		temp = (*i).second->m_Tranform.GetTransform();
		
		// Set the transform in the instance data array
		m_InstanceData[vID++]	= temp.m[0];
		m_InstanceData[vID++]	= temp.m[1];
		m_InstanceData[vID++]	= temp.m[2];
		m_InstanceData[vID++]	= temp.m[3];
		m_InstanceData[vID++]	= (D3DXVECTOR4)(*i).second->m_Color;
	}

	DirectXClass* DXI = GCI->GetDirectX();
	DeferredShader* DSX = GCI->GetDeferredR();

	// Set the Stream source
	HRESULT hr = DXI->GetDevice()->SetVertexDeclaration(VertexInstPNT::Decl);
	hr = DXI->GetDevice()->SetStreamSource(0, m_pInstVertexBuffer, 0, sizeof(VertexInstPNT));
	hr = DXI->GetDevice()->SetIndices(m_pInstIndexBuffer);

	// Set the Instancing Data
	hr = DSX->m_pFX->SetBool("g_sDrawImposter", m_bIsImposter);
	hr = DSX->m_pFX->SetVectorArray("g_sInstancingData", m_InstanceData, vID);
	hr = DSX->m_pFX->SetTexture("g_sTexture", GCI->GetTextureManager()->TextureList[m_TexName]->texture);
	
	// Set world matrix
	DSX->m_pFX->SetMatrix(DSX->m_hWorld, &m_WorldMat);
	DSX->m_pFX->SetMatrix(DSX->m_hWVP, &(m_WorldMat*GCI->GetCamera()->GetViewProj()));

	// Draw Indexed Primitives
	hr = DSX->m_pFX->CommitChanges();
	hr = DXI->GetDevice()->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iInstCount*4, //m_iNumVerts, 
									0, (m_iInstCount*2)); //6)/3);	//m_iNumIndex/3);	

	// Add to the number of objects drawn
	DSX->AddNumObjDrawn( m_iInstCount );

	// Render Siblings if there is any
	if(m_pSibling)
		m_pSibling->Render(shadowMap);
}

void Billboards::BuildInstVertexBuff()
{
	// Create Vertex Buffer
	HRESULT hr;
	m_iNumVerts = 4*m_iInstCount;
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexBuffer(m_iNumVerts* sizeof(VertexInstPNT), 
				D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED, &m_pInstVertexBuffer, 0);

	// Create Quad Vertices
	VertexInstPNT	*quadVerts = 0;

	// Edit Buffer
	hr = m_pInstVertexBuffer->Lock(0, 0, (void**)&quadVerts, 0);
	
	// Front
	for(unsigned int i = 0; i < m_iNumVerts; i+=4)
	{
		quadVerts[i+0].position = D3DXVECTOR3(-0.5f, -0.5f, 0.0f);
		quadVerts[i+1].position = D3DXVECTOR3(-0.5f, 0.5f, 0.0f);
		quadVerts[i+2].position = D3DXVECTOR3(0.5f, 0.5f, 0.0f);
		quadVerts[i+3].position = D3DXVECTOR3(0.5f, -0.5f, 0.0f);
		D3DXVec3Normalize(&quadVerts[i+0].normal, &D3DXVECTOR3(0.0f, 0.0f, 1.0f));
		D3DXVec3Normalize(&quadVerts[i+1].normal, &D3DXVECTOR3(0.0f, 0.0f, 1.0f));
		D3DXVec3Normalize(&quadVerts[i+2].normal, &D3DXVECTOR3(0.0f, 0.0f, 1.0f));
		D3DXVec3Normalize(&quadVerts[i+3].normal, &D3DXVECTOR3(0.0f, 0.0f, 1.0f));
		quadVerts[i+0].texturePos = D3DXVECTOR2(0.0f, 1.0f);
		quadVerts[i+1].texturePos = D3DXVECTOR2(0.0f, 0.0f);
		quadVerts[i+2].texturePos = D3DXVECTOR2(1.0f, 0.0f);
		quadVerts[i+3].texturePos = D3DXVECTOR2(1.0f, 1.0f);

		// IndexID for this instance
		int id = i + (i/4);
		quadVerts[i+0].instID = id;
		quadVerts[i+1].instID = id;
		quadVerts[i+2].instID = id;
		quadVerts[i+3].instID = id;
	}

	hr = m_pInstVertexBuffer->Unlock();
}
void Billboards::BuildInstIndexBuff()
{
	// Create Index Buffer (Dynamic)
	m_iNumIndex = 6*m_iInstCount;
	HRESULT hr = GCI->GetDirectX()->GetDevice()->CreateIndexBuffer(m_iNumIndex *sizeof(WORD), 
					D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_pInstIndexBuffer, 0);
	
	WORD* iQuad = 0;

	// Access Buffer
	hr = m_pInstIndexBuffer->Lock(0,0, (void**)&iQuad, 0);

	// Front face
	unsigned int v = 0;
	for(unsigned int i = 0; i < m_iNumIndex; i+=6)
	{
		iQuad[i+0] = v+0; iQuad[i+1] = v+1; iQuad[i+2] = v+2;		// Triangle 0
		iQuad[i+3] = v+0; iQuad[i+4] = v+2; iQuad[i+5] = v+3;		// Triangle 1
		v += 4;		// Vertex Incrementer
	}

	hr = m_pInstIndexBuffer->Unlock();	// Done accessing
}

void Billboards::SetAlwaysFaceCamera(bool enable)
{	m_bIsImposter = enable;	}
int Billboards::GetNumInstances()
{	return m_iInstCount;	}

void Billboards::SetColor(int id, D3DXCOLOR color)
{
	if(m_Registry.find(id) == m_Registry.end())
		return;
	m_Registry[id]->m_Color = color;
}
BaseObjectPtr Billboards::GetObject(int id)
{	
	if(m_Registry.find(id) == m_Registry.end())
		return 0;
	return m_Registry[id];	
}

// Check for picking
void Billboards::CheckQuadPicking(int hitIDs[], int& count, QuadPicking& pick)
{
	for(auto i = m_Registry.begin();
		i != m_Registry.end() && count < 31; i++)	// Current max count for the message is 31
		if( pick.PickingChecks( (*i).second->m_Tranform.GetTransform() ) )
			hitIDs[count++] = (*i).second->m_SharedMemID;	// All quads that are picked should be stored
}
