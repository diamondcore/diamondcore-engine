#include "ParticleManager.h"
#include "../../GraphicsCore.h"
#include "../../Westley's Lib/RandomVariate.h"
#include "../../MacroTools.h"
#include <MemoryManagement/Module.h>
#include <fstream>

void GetRandomVec(D3DXVECTOR3& out)
{
	out.x = GetRandomFloat(-1.0f, 1.0f);
	out.y = GetRandomFloat(-1.0f, 1.0f);
	out.z = GetRandomFloat(-1.0f, 1.0f);

	// Project onto unit sphere
	D3DXVec3Normalize(&out, &out);
}


/***************************************
		  Particle System Base
****************************************/
ParticleSystemBase::ParticleSystemBase(int id, const std::string& techName, const std::string& texName,
					   const D3DXVECTOR3& accel, float mass, float emitterTime, int numParticles)
	:m_TechName(techName),
	 m_Accel(accel),
	 m_fMass(mass),
	 m_fEmitterTime(emitterTime),
	 m_fTimeRunning(0.0f),
	 m_fDuration(0.0f),
	 m_pTimeAccum(NULL),
	 m_ID(id)
{
	// If max num particles is over MAX_PARTICLES then create more of these particles
	if(numParticles > MAX_PARTICLES)
	{
		m_iNumParticles = numParticles;		// Temporarily set the full ammount of particles
		m_SiblingParticle = Clone();		// Move any extra particles to a sibling
		m_iNumParticles = MAX_PARTICLES;	// Cap the particles to the max
		//m_fInitDelay = m_fEmitterTime*(MAX_PARTICLES/numParticles);	// Delay the particles to run in sequence
	}
	else
	{
		m_SiblingParticle = 0;
		m_iNumParticles   = numParticles;
		m_pTimeAccum      = new float;		// Create the accumulated time
		*m_pTimeAccum     = 0.0f;           // Set time accumulated
	}

#if 0
	// For use with passing world matrix
	m_ParticleInst = new D3DXVECTOR4[m_iNumParticles*5];
#else
	m_ParticleInst = new D3DXVECTOR4[m_iNumParticles];
#endif

	// Allocate memory for max number of particles
	m_ParticleList.resize(m_iNumParticles);
	m_pAliveParticles.reserve(m_iNumParticles);
	m_pDeadParticles.reserve(m_iNumParticles);

	// Start all particles as dead
#if 0
	// For use with passing world matrix
	for(int pIndex = 0; pIndex < m_iNumParticles*5; pIndex+=5)
	{
		for(int i = 0; i < 5; i++)
		{
			m_ParticleList[pIndex+i].instID	= pIndex;
			m_ParticleList[pIndex+i].lifeTime = -1.0f;
			m_ParticleList[pIndex+i].initTime = 0.0f;
		}
		m_ParticleInst[pIndex+4].w = 0;
	}
#else
	// For use with passing in relative local coordinates
	for(int pIndex = 0; pIndex < m_iNumParticles; pIndex++)
	{
		m_ParticleList[pIndex].instID	= pIndex;
		m_ParticleList[pIndex].lifeTime = -1.0f;
		m_ParticleList[pIndex].initTime = 0.0f;
		m_ParticleInst[pIndex].w = 0.0f;	// '0' Represent dead particle
	}
#endif

	// Set world matrix
	D3DXMatrixIdentity(&m_WorldMat);

	// Create the particle texture
	m_TexName = "Particles/";
	m_TexName += texName;
	GCI->GetTextureManager()->LoadTexture(m_TexName);

	// Create Vertex Buffer
	GCI->GetDirectX()->GetDevice()->CreateVertexBuffer((m_iNumParticles)*sizeof(VertexParticle),
		D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY | D3DUSAGE_POINTS,
		0, D3DPOOL_DEFAULT, &m_VertexBuff, 0);
}
ParticleSystemBase::~ParticleSystemBase()
{}
void ParticleSystemBase::PreRestartDevice()
{
	if(m_SiblingParticle)
		m_SiblingParticle->PreRestartDevice();
	SAFE_RELEASE(m_VertexBuff);
}
void ParticleSystemBase::PostRestartDevice()
{
	if(m_SiblingParticle)
		m_SiblingParticle->PostRestartDevice();
	// Recreate Vertex Buffer
	GCI->GetDirectX()->GetDevice()->CreateVertexBuffer((m_iNumParticles)*sizeof(VertexParticle),
		D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY | D3DUSAGE_POINTS, 0,
		D3DPOOL_DEFAULT, &m_VertexBuff, 0);
	
	// Apply allive particles
	VertexParticle* p = 0;
	m_VertexBuff->Lock(0, 0, (void**)&p, D3DLOCK_DISCARD);

	// Add all alive particles to the vertex buffer
	memcpy(p, &m_ParticleList[0], sizeof(VertexParticle)*m_iNumParticles);
	
	m_VertexBuff->Unlock();
}
void ParticleSystemBase::Shutdown()
{
	// Release sibling
	if(m_SiblingParticle)
	{
		m_SiblingParticle->Shutdown();
		m_SiblingParticle = NULL;
	}
	else
		SAFE_DELETE(m_pTimeAccum);

	// Release this particles components
	PreRestartDevice();
	SAFE_DELETE_ARRAY(m_ParticleInst);
}
std::shared_ptr<ParticleSystemBase> ParticleSystemBase::Clone()
{
	std::shared_ptr<ParticleSystemBase> clone = std::shared_ptr<ParticleSystemBase>(
		new ParticleSystemBase(m_ID, m_TechName, m_TexName, m_Accel, m_fMass, m_fEmitterTime, m_iNumParticles-MAX_PARTICLES));	// Decrement the particle count by this particle
	clone->m_fDuration = m_fDuration;
	m_pTimeAccum       = clone->m_pTimeAccum;  // Set to the smallest child's time accumulated
	return clone;
}

void ParticleSystemBase::Update(const float& dt)
{
	m_fTimeRunning += dt;

	// Rebuild Dead and Alive list
	m_pAliveParticles.resize(0);
	m_pDeadParticles.resize(0);
	for(int i = 0; i < m_iNumParticles; i++)
	{
		// Check if the particle is dead
		if((m_fTimeRunning - m_ParticleList[i].initTime) >
			m_ParticleList[i].lifeTime)
		{
			m_pDeadParticles.push_back(&m_ParticleList[i]);
			int id = m_ParticleList[i].instID;
#if 0
			m_ParticleInst[id+4] = D3DXVECTOR4(m_ParticleList[i].initTime, 
				m_ParticleList[i].initSize, m_ParticleList[i].lifeTime, 0);	// 0 Means is dead
#else
			m_ParticleInst[id] = D3DXVECTOR4(m_ParticleList[i].initTime, 
				m_ParticleList[i].initSize, m_ParticleList[i].lifeTime, 0.0f);	// 0 Means is dead
			//m_ParticleInst[id].w = 0;	// 0 Means is dead
#endif
		}
		else
		{
			m_pAliveParticles.push_back(&m_ParticleList[i]);
			int id = m_ParticleList[i].instID;
#if 0
			m_ParticleInst[id+4] = D3DXVECTOR4(m_ParticleList[i].initTime, 
				m_ParticleList[i].initSize, m_ParticleList[i].lifeTime, 1);	// 1 Means is alive
#else
			m_ParticleInst[id] = D3DXVECTOR4(m_ParticleList[i].initTime, 
				m_ParticleList[i].initSize, m_ParticleList[i].lifeTime, 1.0f);	// 1 Means is alive
			//m_ParticleInst[id].w = 1;	// 1 Means is alive
#endif
		}
	}

	// Emit for the particles for the set amount of time
	if(m_fDuration == 0.0f || m_fDuration > m_fTimeRunning)
		if(m_fEmitterTime > 0.0f)
		{
			// Emit Particles
			*m_pTimeAccum = 0.0f;
			*m_pTimeAccum += dt;
			if(*m_pTimeAccum >= m_fEmitterTime)
			{
				AddParticle();
				*m_pTimeAccum -= m_fEmitterTime;
			}
		}

	if(m_SiblingParticle)
		m_SiblingParticle->Update(dt);
}
void ParticleSystemBase::Render()
{
	DeferredShader* DSX = GCI->GetDeferredR();
	DirectXClass* DXI = GCI->GetDirectX();

	D3DXVECTOR3 camPosW = GCI->GetCamera()->m_Position;
	DSX->m_pFX->SetMatrix("g_sWorld", &m_WorldMat);
	DSX->m_pFX->SetMatrix("g_sWVP", &(m_WorldMat*GCI->GetCamera()->GetViewProj()));

	D3DXMATRIX invW;
	D3DXMatrixInverse(&invW, 0, &m_WorldMat);
	D3DXVECTOR3 camPosL;
	D3DXVec3TransformCoord(&camPosL, &camPosW, &invW);
	DSX->m_pFX->SetValue("g_sCameraPosL", &camPosL, sizeof(D3DXVECTOR3));

	DSX->m_pFX->SetFloat("g_sTime", m_fTimeRunning);
	DSX->m_pFX->SetValue("g_sAccel", m_Accel, sizeof(D3DXVECTOR3));
	DSX->m_pFX->SetInt("g_sViewHeight", DXI->GetScreenHeight());
	
	HRESULT hr = DSX->m_pFX->SetTexture("g_sParticleTex", GCI->GetTextureManager()->TextureList[m_TexName]->texture);

#if 0
	// For use when passing in full world matrix
	DSX->m_pFX->SetVectorArray("g_sInstanceData", m_ParticleInst, m_iNumParticles*5);
#else
	DSX->m_pFX->SetVectorArray("g_sInstanceData", m_ParticleInst, m_iNumParticles);
#endif

	DSX->m_pFX->SetTechnique(m_TechName.c_str());
	DSX->m_pFX->CommitChanges();

	UINT numPasses = 0;
	DSX->m_pFX->Begin(&numPasses, 0);
	DSX->m_pFX->BeginPass(0);

	DXI->GetDevice()->SetStreamSource(0, m_VertexBuff, 0, sizeof(VertexParticle));
	DXI->GetDevice()->SetVertexDeclaration(VertexParticle::Decl);

	//if(vIndex > 0)
	DXI->GetDevice()->DrawPrimitive(D3DPT_POINTLIST, 0, (m_iNumParticles));	//m_pAliveParticles.size());

	// Render siblings
	if(m_SiblingParticle)
		m_SiblingParticle->RawRender();

	DSX->m_pFX->EndPass();
	DSX->m_pFX->End();

	// Output the number of particles alive
	#if DISPLAY_ALIVE_COUNT
		DSX->AddNumObjDrawn( m_pAliveParticles.size() );
	#endif
}
void ParticleSystemBase::RawRender()
{
	DeferredShader* DSX = GCI->GetDeferredR();

	// Set the new array of instances
#if 0
	// For use when passing in full world matrix
	DSX->m_pFX->SetVectorArray("g_sInstanceData", m_ParticleInst, m_iNumParticles*5);
#else
	DSX->m_pFX->SetVectorArray("g_sInstanceData", m_ParticleInst, m_iNumParticles);
#endif
	DSX->m_pFX->SetFloat("g_sTime", m_fTimeRunning);
	DSX->m_pFX->CommitChanges();

	// Render Raw verts
	GCI->GetDirectX()->GetDevice()->DrawPrimitive(D3DPT_POINTLIST, 0, (m_iNumParticles));	//m_pAliveParticles.size());

	// Render siblings
	if(m_SiblingParticle)
		m_SiblingParticle->RawRender();

	// Output the number of particles alive
	#if DISPLAY_ALIVE_COUNT
		DSX->AddNumObjDrawn( m_pAliveParticles.size() );
	#endif
}

void ParticleSystemBase::InitParticle(VertexParticle& particleOut)
{
	// Generate at the origin
	particleOut.initPos = D3DXVECTOR3(0.0f,0.0f,0.0f);

	particleOut.initTime = m_fTimeRunning;
	particleOut.lifeTime = GetRandomFloat(5.0f, 6.0f); //4.0f, 5.0f);	//rand()%2 + 5;
	particleOut.initColor = WHITE;
	particleOut.initSize = GetRandomFloat(0.1f, 3.0f);	//(float)(rand()%5 + 9)/10.0f;

	particleOut.initVel.x = GetRandomFloat(-2.5f, 2.5f);	//(float)(rand()%6) - 2.5f;
	particleOut.initVel.y = GetRandomFloat(15.0f, 25.0f);	//rand()%11 + 15.0f;
	particleOut.initVel.z = GetRandomFloat(-2.5f, 2.5f);	//(float)(rand()%6) - 2.5f;

	// Optional (over ride) mass
	particleOut.mass = GetRandomFloat(0.8f, 1.2f);
	//particleOut.mass	= m_fMass;
}

void ParticleSystemBase::InitVBuffer()
{
	// Apply allive particles
	VertexParticle* p = 0;
	m_VertexBuff->Lock(0, 0, (void**)&p, D3DLOCK_DISCARD);
	
	// Initialize all the vertices
	int vIndex = 0;
	for(int i = 0; i < m_iNumParticles; i++)
	{
		InitParticle(m_ParticleList[i]);
		p[i] = m_ParticleList[i];
		
		// Use vector to pass in restart values
#if 0
		// Full world matrix
		D3DXMATRIX W;
		D3DXMatrixTranslation(&W, p[i].initPos.x, p[i].initPos.y, p[i].initPos.z);
		m_ParticleInst[vIndex+0] = W.m[0];
		m_ParticleInst[vIndex+1] = W.m[1];
		m_ParticleInst[vIndex+2] = W.m[2];
		m_ParticleInst[vIndex+3] = W.m[3];
		m_ParticleInst[vIndex+4] = D3DXVECTOR4(p[i].initTime, 
			p[i].initSize, p[i].lifeTime, 1);	// 1 Means is alive
		vIndex += 5;
#else
		m_ParticleInst[vIndex] = D3DXVECTOR4(p[i].initTime, 
			p[i].initSize, p[i].lifeTime, 1.0f);	// 1 Means is alive
		// Only use local coordinates
		//m_ParticleInst[vIndex] = D3DXVECTOR4(p[i].initPos.x, p[i].initPos.y, p[i].initPos.z, 1.0f);
		//m_ParticleInst[vIndex].w =  1;	// 1 Means is alive
		vIndex += 1;
#endif
	}

	m_VertexBuff->Unlock();
}
void ParticleSystemBase::AddParticle()
{
	if(m_pDeadParticles.size() > 0)
	{
		// Reinitialize a particle
		VertexParticle* p = m_pDeadParticles.back();
		int i = (*p).instID;
		InitParticle(*p);
		
		// Use vector to pass in restart values
#if 0
		// Full world matrix
		D3DXMATRIX W;
		D3DXMatrixTranslation(&W, (*p).initPos.x, (*p).initPos.y, (*p).initPos.z);
		m_ParticleInst[i+0] = W.m[0];
		m_ParticleInst[i+1] = W.m[1];
		m_ParticleInst[i+2] = W.m[2];
		m_ParticleInst[i+3] = W.m[3];
		m_ParticleInst[i+4] = D3DXVECTOR4((*p).initTime, 
			(*p).initSize, (*p).lifeTime, 1);	// 1 Means is alive
#else
		m_ParticleInst[i] = D3DXVECTOR4((*p).initTime, 
			(*p).initSize, (*p).lifeTime, 1.0f);	// 1 Means is alive
		// Use Local coordinates
		//m_ParticleInst[i] = D3DXVECTOR4((*p).initPos.x, (*p).initPos.y, (*p).initPos.z, 1.0f);
		//m_ParticleInst[i].w = 1;	// 1 Means is alive
#endif
		// No longer dead
		m_pDeadParticles.pop_back();
		m_pAliveParticles.push_back(p);
	}
}
void ParticleSystemBase::SetWorldMat(D3DXMATRIX world)
{	
	if(m_SiblingParticle)
		m_SiblingParticle->SetWorldMat(world);
	m_WorldMat = world;	
}

// Methods to help manage entities
void ParticleSystemBase::SetMapName(std::string refName)
{	m_MapName = refName;	}
bool ParticleSystemBase::operator== (const std::string& rhs)
{	return (m_MapName == rhs);	}


/***************************************
	  Explosion Particle System
****************************************/
ExplosionParticle::ExplosionParticle(int id, const D3DXVECTOR3& accel, float mass, float emitTime)
	:ParticleSystemBase(id, "SparkTech", "RainTex.dds", accel, mass, emitTime, 20)
{
	m_fInitVelMin	= 18.0f;
	m_fInitVelMax	= 20.0f;
	//InitVBuffer();
}
ExplosionParticle::~ExplosionParticle()
{}
void ExplosionParticle::InitParticle(VertexParticle& particleOut)
{
	particleOut.initTime = m_fTimeRunning;

	// Flare life for 2-4 seconds
	particleOut.lifeTime = GetRandomFloat(3.0f, 4.0f);	//rand()%2 + 3;

	// Init size in pixels between 10 - 15
	particleOut.initSize = GetRandomFloat(11.0f, 15.0f);	//rand()%5 + 11;

	// Give very small init velocity
	//GetRandomVec(particleOut.initVel);
	//particleOut.initVel = D3DXVECTOR3(1.0f/(float)(rand()%10+1), 1.0f/(float)(rand()%10+1), 1.0f/(float)(rand()%10+1));

	// Scaler/amplifier
	particleOut.mass = GetRandomFloat(1.0f, 2.0f);	//rand()%2 + 1;

	// Start color 50% - 100%
	particleOut.initColor = GetRandomFloat(0.6f, 1.0f)*WHITE;	//(float)(1/(rand()%5+6))*WHITE;

	// Random radius and angle in polar coordinates
	float r = GetRandomFloat(0.0f, 0.5f);//(11.0f, 14.0f);		//rand()%4+11;
	float t = GetRandomFloat(0.0f, 2.0f*D3DX_PI);	//rand()%(int)(2.0f*D3DX_PI);

	// Convert to Cartesian coordinates
	particleOut.initPos.x = r * cosf(t);//GetRandomFloat(-1.0f, 1.0f);	//1.0f;	//r*cosf(t);
	particleOut.initPos.y = r * sinf(t);//GetRandomFloat(-1.0f, 1.0f);	//1.0f;	//r*sinf(t);

	// Random depth inbetween -1 and 1
	particleOut.initPos.z = r * atanf(t); //GetRandomFloat(-1.0f, 1.0f);	//rand()%3 - 1;
	D3DXVec3Normalize(&particleOut.initVel, &particleOut.initPos);
	particleOut.initVel *= GetRandomFloat(m_fInitVelMin, m_fInitVelMax); //20.0f;	// 30 or above will cause errors in the shader
	//particleOut.initVel *= (1.0f - r/0.5f ) * 30.0f;
}


/***************************************
		  Fire Particle System
****************************************/
FireParticle::FireParticle(int id, const D3DXVECTOR3& accel, float mass, float emitTime)
	:ParticleSystemBase(id, "FireTech", "FireTex.dds", accel, mass, emitTime, 10)	//5)
{
	m_vInitSize = Vector2(11.0f, 15.0f);
	m_vLifeSpan = Vector2(3.0f, 4.0f);
	//InitVBuffer();
}
FireParticle::~FireParticle()
{}

void FireParticle::InitParticle(VertexParticle& particleOut)
{
	particleOut.initTime = m_fTimeRunning - 2.0f;

	// Flare life for 2-4 seconds
	particleOut.lifeTime = GetRandomFloat(m_vLifeSpan.x, m_vLifeSpan.y);	//rand()%2 + 3;

	// Init size in pixels between 10 - 15
	particleOut.initSize = GetRandomFloat(m_vInitSize.x, m_vInitSize.y);	//rand()%5 + 11;

	// Give very small init velocity
	GetRandomVec(particleOut.initVel);
	//particleOut.initVel = D3DXVECTOR3(1.0f/(float)(rand()%10+1), 1.0f/(float)(rand()%10+1), 1.0f/(float)(rand()%10+1));

	// Scaler/amplifier
	particleOut.mass = GetRandomFloat(1.0f, 2.0f);	//rand()%2 + 1;

	// Start color 50% - 100%
	particleOut.initColor = GetRandomFloat(0.6f, 1.0f)*WHITE;	//(float)(1/(rand()%5+6))*WHITE;

	// Random radius and angle in polar coordinates
	float r = GetRandomFloat(0.0f, 0.5f);//(11.0f, 14.0f);		//rand()%4+11;
	float t = GetRandomFloat(0.0f, 2.0f*D3DX_PI);	//rand()%(int)(2.0f*D3DX_PI);

	// Convert to Cartesian coordinates
	particleOut.initPos.x = r * cosf(t);//GetRandomFloat(-1.0f, 1.0f);	//1.0f;	//r*cosf(t);
	particleOut.initPos.y = r * sinf(t);//GetRandomFloat(-1.0f, 1.0f);	//1.0f;	//r*sinf(t);

	// Random depth inbetween -1 and 1
	particleOut.initPos.z = r * atanf(t); //GetRandomFloat(-1.0f, 1.0f);	//rand()%3 - 1;
}


/***************************************
		 Jet Flame Particle System
****************************************/
JetFlameParticle::JetFlameParticle(int id, const D3DXVECTOR3& accel, float mass, float emitTime, int numParticles)
	:ParticleSystemBase(id, "SmokeTech", "FireTex.dds", accel, mass, emitTime, numParticles)	//5)
{
	m_pSpeed = 0;
	//InitVBuffer();
}
JetFlameParticle::~JetFlameParticle()
{}

void JetFlameParticle::InitParticle(VertexParticle& particleOut)
{
	// Generate at the origin
	particleOut.initPos.x = 0.0f;//GetRandomFloat(-m_vInitPosRange.x, m_vInitPosRange.x);
	particleOut.initPos.y = 0.0f;//GetRandomFloat(-m_vInitPosRange.y, m_vInitPosRange.y);
	particleOut.initPos.z = 0.0f;//GetRandomFloat(-m_vInitPosRange.z, m_vInitPosRange.z);

	particleOut.initTime = m_fTimeRunning - 0.9f;	// Fast forward the very small stage
	particleOut.lifeTime = GetRandomFloat(1.0f, 1.4f);//(5.0f, 6.0f); //4.0f, 5.0f);	//rand()%2 + 5;
	particleOut.initColor = D3DXCOLOR(1.0f, 0.5f, 0.2f, 1.0f);
	particleOut.initSize = GetRandomFloat(0.1f, 3.0f);	//(float)(rand()%5 + 9)/10.0f;

	particleOut.initVel.x = GetRandomFloat(-1.0f, 1.0f); //GetRandomFloat(-2.5f, 2.5f);	//(float)(rand()%6) - 2.5f;
	particleOut.initVel.y = GetRandomFloat(-1.0f, 1.0f);	//rand()%11 + 15.0f;
	particleOut.initVel.z = GetRandomFloat(-20.0f, -15.0f); //GetRandomFloat(-2.5f, 2.5f);	//(float)(rand()%6) - 2.5f;

	// Forward position by time of 0.9f
	particleOut.initPos -= (particleOut.initVel * 0.9f + 0.5f*m_Accel * 0.9f * 0.9f);

	// Optional (over ride) mass
	//particleOut.mass = GetRandomFloat(0.8f, 1.2f);
	particleOut.mass	= m_fMass;
}
void JetFlameParticle::Render()
{
	auto DSX = GCI->GetDeferredR();
	if(m_pSpeed)
	{
		// Scale flames with speed
		DSX->m_pFX->SetBool("f_sUseSpeed", true);
		if(*m_pSpeed > 1.0f)
			DSX->m_pFX->SetFloat("g_sSpeed", *m_pSpeed);
		else
			DSX->m_pFX->SetFloat("g_sSpeed", 1.0f);	// Make sure that speed is not 0, bc we can't divide 0
	}

	ParticleSystemBase::Render();

	DSX->m_pFX->SetBool("f_sUseSpeed", false);
}
std::shared_ptr<ParticleSystemBase> JetFlameParticle::Clone()
{
	std::shared_ptr<JetFlameParticle> clone = std::shared_ptr<JetFlameParticle>(
		new JetFlameParticle(m_ID, m_Accel, m_fMass, m_fEmitterTime, m_iNumParticles-MAX_PARTICLES));	// Decrement the particle count by this particle
	clone->m_fDuration = m_fDuration;
	clone->m_pSpeed	= m_pSpeed;
	return clone;
}


/***************************************
		  Smoke Particle System
****************************************/
SmokeParticle::SmokeParticle(int id, const D3DXVECTOR3& pos, const D3DXVECTOR3& accel, float mass, float emitTime, int numParticles)
	:ParticleSystemBase(id, "SmokeTech", "SmokeTex.dds", accel, mass, emitTime, numParticles)//40)
{
	m_vInitPosRange = pos;
	m_vInitVelMax	= pos;
	m_vInitVelMin	= pos * -1.0f;
	m_vInitVelMin.y	= 15.0f;
	m_vInitVelMax.y	= 20.0f;
	m_vInitSize		= Vector2(0.1f, 3.0f);
	m_bPolarVel		= false;
	//InitVBuffer();
}
SmokeParticle::~SmokeParticle()
{}

void SmokeParticle::InitParticle(VertexParticle& particleOut)
{
	// Generate at the origin
	particleOut.initPos.x = GetRandomFloat(-m_vInitPosRange.x, m_vInitPosRange.x);
	particleOut.initPos.y = GetRandomFloat(-m_vInitPosRange.y, m_vInitPosRange.y);
	particleOut.initPos.z = GetRandomFloat(-m_vInitPosRange.z, m_vInitPosRange.z);

	particleOut.initTime = m_fTimeRunning - 0.9f;	// Fast forward the very small stage
	particleOut.lifeTime = GetRandomFloat(4.0f, 5.0f);//(5.0f, 6.0f); //4.0f, 5.0f);	//rand()%2 + 5;
	particleOut.initColor = WHITE;
	particleOut.initSize = GetRandomFloat(m_vInitSize.x, m_vInitSize.y);	//(float)(rand()%5 + 9)/10.0f;

	if(m_bPolarVel)
	{
		// Random radius and angle in polar coordinates	
		float t = GetRandomFloat(0.0f, 2.0f*D3DX_PI);

		particleOut.initPos.x = GetRandomFloat(-m_vInitPosRange.x, m_vInitPosRange.x) * cosf(t);
		particleOut.initPos.z = GetRandomFloat(-m_vInitPosRange.z, m_vInitPosRange.z) * sinf(t);

		// Convert to Cartesian coordinates
		Vector3 dir = particleOut.initPos;
		dir.Normalize();
		particleOut.initVel.x = dir.x * GetRandomFloat(m_vInitVelMin.x, m_vInitVelMax.x);
		particleOut.initVel.z = dir.z * GetRandomFloat(m_vInitVelMin.z, m_vInitVelMax.z);
		//particleOut.initVel.x = GetRandomFloat(m_vInitVelMin.x, m_vInitVelMax.x) * cosf(t);//GetRandomFloat(-1.0f, 1.0f);	//1.0f;	//r*cosf(t);
		//particleOut.initVel.z = GetRandomFloat(m_vInitVelMin.z, m_vInitVelMax.z) * sinf(t);//GetRandomFloat(-1.0f, 1.0f);	//1.0f;	//r*sinf(t);

		// Random height
		particleOut.initVel.y = GetRandomFloat(m_vInitVelMin.y, m_vInitVelMax.y);// * atanf(t); //GetRandomFloat(-1.0f, 1.0f);	//rand()%3 - 1;
	}
	else
	{
		particleOut.initVel.x = GetRandomFloat(m_vInitVelMin.x, m_vInitVelMax.x); //GetRandomFloat(-2.5f, 2.5f);	//(float)(rand()%6) - 2.5f;
		particleOut.initVel.y = GetRandomFloat(m_vInitVelMin.y, m_vInitVelMax.y);	//rand()%11 + 15.0f;
		particleOut.initVel.z = GetRandomFloat(m_vInitVelMin.z, m_vInitVelMax.z); //GetRandomFloat(-2.5f, 2.5f);	//(float)(rand()%6) - 2.5f;
	}

	// Forward position by time of 0.9f
	particleOut.initPos -= (particleOut.initVel * 0.9f + 0.5f*m_Accel * 0.9f * 0.9f);

	// Optional (over ride) mass
	//particleOut.mass = GetRandomFloat(0.8f, 1.2f);
	particleOut.mass	= m_fMass;
}
std::shared_ptr<ParticleSystemBase> SmokeParticle::Clone()
{
	std::shared_ptr<SmokeParticle> clone = std::shared_ptr<SmokeParticle>(
		new SmokeParticle(m_ID, m_vInitPosRange, m_Accel, m_fMass, m_fEmitterTime, m_iNumParticles-MAX_PARTICLES));	// Decrement the particle count by this particle
	clone->m_fDuration = m_fDuration;
	clone->m_vInitVelMin = m_vInitVelMin;
	clone->m_vInitVelMax = m_vInitVelMax;
	clone->m_vInitSize	 = m_vInitSize;
	clone->m_bPolarVel	 = m_bPolarVel;
	return clone;
}



/***************************************
		  Bullet Particle System
****************************************/
BulletParticle::BulletParticle(int id, const D3DXVECTOR3& accel, float mass, float emitTime)
	:ParticleSystemBase(id, "BulletTech", "BulletTex.dds", accel, mass, emitTime, 5)
{
	InitVBuffer();
}
BulletParticle::~BulletParticle()
{}

void BulletParticle::InitParticle(VertexParticle& particleOut)
{
	// Generate at camera
	D3DXVECTOR3 cam = GCI->GetCamera()->m_Position;
	particleOut.initPos.x = cam.x;
	particleOut.initPos.z = cam.z;
	particleOut.initPos.y = cam.y - 0.5f;	// Lower to gun position

	// Fire in Camera's look direction
	float speed = 500.0f;
	particleOut.initVel = GCI->GetCamera()->m_LookAt;
	particleOut.initVel *= speed;

	particleOut.initTime = m_fTimeRunning;
	particleOut.lifeTime = 4.0f;
	particleOut.initColor = WHITE;
	particleOut.initSize = GetRandomFloat(80.0f, 90.0f);	//rand()%10 + 81;
	particleOut.mass = 1.0f;
}


/***************************************
		  Rain Particle System
****************************************/
RainParticle::RainParticle(int id, const D3DXVECTOR3& accel, float mass, float emitTime)
	:ParticleSystemBase(id, "RainTech", "RainTex.dds", accel, mass, emitTime, 40)
{
	InitVBuffer();
}
RainParticle::~RainParticle()
{}

void RainParticle::InitParticle(VertexParticle& particleOut)
{
	// Generate at camera
	particleOut.initPos = GCI->GetCamera()->m_Position;

	// Spread the particles out on the xz-plane
	particleOut.initPos.x += GetRandomFloat(-100.0f, 100.0f);
	particleOut.initPos.z += GetRandomFloat(-100.0f, 100.0f);

	// Generate above the camera
	particleOut.initPos.y += GetRandomFloat(50.0f, 55.0f);

	particleOut.initTime = m_fTimeRunning;
	particleOut.lifeTime = GetRandomFloat(2.0f, 2.5f);
	particleOut.initColor = WHITE;
	particleOut.initSize = GetRandomFloat(6.0f, 7.0f);	
	
	// Give an intial falling velocity
	particleOut.initVel.x = GetRandomFloat(-1.5f, 0.0f);
	particleOut.initVel.y = GetRandomFloat(-50.0f, -45.0f);
	particleOut.initVel.z = GetRandomFloat(-0.5f, 0.5f);
}



/***************************************
			Particle Manager
****************************************/
ParticleManager::ParticleManager()
{}
ParticleManager::~ParticleManager()
{}
void ParticleManager::PreRestartDevice()
{
	for(auto p = m_ParticleMap.begin();
		p != m_ParticleMap.end();
		p++)
	{
		auto list = &(*p).second;
		for(auto i = list->begin();
			i != list->end(); i++)
			(*i)->PreRestartDevice();
	}
}
void ParticleManager::PostRestartDevice()
{
	for(auto p = m_ParticleMap.begin();
		p != m_ParticleMap.end();
		p++)
	{
		auto list = &(*p).second;
		for(auto i = list->begin();
			i != list->end(); i++)
			(*i)->PostRestartDevice();
	}
}

void ParticleManager::Init()
{	
	// Setup Default Particles
	//m_ParticleMap.insert(std::pair<std::string, std::shared_ptr<ParticleSystemBase>>("DefaultSpark", std::shared_ptr<ParticleSystemBase>(
	//			new ExplosionParticle(D3DXVECTOR3(0.0f, -9.8f, 0.0f), 2500, 0.0025f))));
	//m_ParticleMap.insert(std::pair<std::string, std::shared_ptr<ParticleSystemBase>>("DefaultFire", std::shared_ptr<ParticleSystemBase>(
	//			new FireParticle(D3DXVECTOR3(0.0f, 0.9f, 0.0f), 1500, 0.0025f))));
	//m_ParticleMap.insert(std::pair<std::string, std::shared_ptr<ParticleSystemBase>>("DefaultSmoke", std::shared_ptr<ParticleSystemBase>(
	//			new SmokeParticle(D3DXVECTOR3(-3.0f, -9.8f, 0.0f), 2000, 0.003f))));
}
void ParticleManager::Shutdown()
{
	RVI->Shutdown();
	Clear();
}
void ParticleManager::Clear()
{
	for(auto p = m_ParticleMap.begin();
		p != m_ParticleMap.end();
		p++)
	{
		auto list = &(*p).second;
		for(auto i = list->begin();
			i != list->end(); i++)
			(*i)->Shutdown();
		// Clear the list
		list->clear();
	}
	// Remove all lists
	m_ParticleMap.clear();
}

void ParticleManager::Update(const float& dt)
{
	for(auto p = m_ParticleMap.begin();
		p != m_ParticleMap.end();
		p++)
	{
		auto list = &(*p).second;
		for(auto i = list->begin();
			i != list->end(); i++)
			(*i)->Update(dt);
	}
}
void ParticleManager::Update(Module* sharedMem)
{
	WTW::Transform temp;
	for(auto p = m_ParticleMap.begin();
		p != m_ParticleMap.end(); p++)
	{
		// Update the transforms
		temp.m_Position = sharedMem->GetSharedResource( (*p).first )->GetPosition();
		temp.m_Quaternion = sharedMem->GetSharedResource( (*p).first )->GetOrientation();

		// Set the Particles world transform
		auto list = &(*p).second;
		for(auto i = list->begin();
			i != list->end(); i++)
			(*i)->SetWorldMat( temp.GetTransform() );
	}
}

void ParticleManager::PreRender()
{
	GCI->GetDeferredR()->m_pFX->SetFloat("g_sViewHeight", GCI->GetDirectX()->GetScreenHeight());
}

void ParticleManager::Render()
{
	Update(GCI->GetTimeElapsed());
	PreRender();
	
	for(auto p = m_ParticleMap.begin();
		p != m_ParticleMap.end();
		p++)
	{
		auto list = &(*p).second;
		for(auto i = list->begin();
			i != list->end(); i++)
			(*i)->Render();
	}
}

void ParticleManager::CreateExplosion()
{
/*	m_Particles.push_back(std::shared_ptr<ParticleSystemBase>(
				new SmokeParticle(D3DXVECTOR3(-3.0f, -9.8f, 0.0f), 2000, 0.003f)));
	m_Particles.push_back(std::shared_ptr<ParticleSystemBase>(
				new FireParticle(D3DXVECTOR3(0.0f, 0.9f, 0.0f), 1500, 0.0025f)));
	m_Particles.push_back(std::shared_ptr<ParticleSystemBase>(
				new ExplosionParticle(D3DXVECTOR3(0.0f, -9.8f, 0.0f), 2500, 0.0025f)));*/
}

bool ParticleManager::Load(int id, std::string propertiesFile)
{
	// Holds the lines of input
	std::string	lines[15];

	// Open the file
	std::fstream inFile;
	inFile.open( propertiesFile.c_str() );

	// make sure successfully opened file
	if (inFile.is_open())
	{
		int i = 0;
		// read its contents 
		while (inFile.good() )
		{
			getline(inFile,lines[i++]);
		}
		inFile.close();
	}
	else
		return false;

	// Parse data and Create the Particle
	float timePerParticle;
	float mass = 2000;
	int maxNumParticles;
	D3DXVECTOR3 accel;
	D3DXVECTOR3 orientation;
	D3DXVECTOR3 blendColor;

	// texture path
	std::string texture = lines[0]; //"C:\\MyDirectory\\MyFile.bat";
	
	std::string key ("\\");

	while (1)
	{
		unsigned found = texture.rfind(key);
		if (found != std::string::npos)
		{
			texture.replace (found,key.length(),"/");
			continue;
		}
		break;
	}

	// particle lifetime
	timePerParticle = atof(lines[1].c_str());
	// gravity direction
	accel.x = atof(lines[2].c_str());
	accel.y = atof(lines[3].c_str());
	accel.z = atof(lines[4].c_str());
	// orientation
	orientation = D3DXVECTOR3(atof(lines[5].c_str()),
		atof(lines[6].c_str()),
		atof(lines[7].c_str()) );
	// fx file path
	std::string effect = lines[8];
	
	while (1)
	{
		unsigned found = effect.rfind(key);
		if (found != std::string::npos)
		{
			effect.replace (found,key.length(),"/");
			continue;
		}
		break;
	}

	std::string technique = lines[9];
	maxNumParticles = atof(lines[10].c_str());
	blendColor = D3DXVECTOR3 (atoi(lines[11].c_str()),
		atoi(lines[12].c_str()),
		atoi(lines[13].c_str()) );

	// Now add the particle to the game
/*	ParticleSystemBasePtr temp = std::shared_ptr<ParticleSystemBase>(new ParticleSystemBase(id, technique, texture, accel, mass, timePerParticle, maxNumParticles));
	m_ParticleMap.insert(std::pair<int, std::shared_ptr<ParticleSystemBase>>
		(id, temp));*/
	return true;
}
/*
void ParticleManager::Unload(std::string propertiesFile)
{
	auto iter = m_ParticleMap.find(propertiesFile);
	if( iter == m_ParticleMap.end() )
		return;	// Particle not found

	// Remove Particle from list and map
	int i = 0;
	for(; i < m_Particles.size(); i++)
		if( *m_Particles[i] == propertiesFile )
		{
			m_Particles[i]->Shutdown();
			m_Particles.erase( m_Particles.begin() + i );
		}
	m_ParticleMap.erase( iter );
}*/

void ParticleManager::CreateParticle(int id, std::string particle, void* param)
{
	ParticleSystemBasePtr temp = 0;
	if(particle == "DefaultFire")
	{
		temp = std::shared_ptr<ParticleSystemBase>(new FireParticle(id, 
			D3DXVECTOR3(0.0f, 0.9f, 0.0f), 1500, 0.0025f));
		temp->InitVBuffer();
	}
	else if(particle == "DefaultSmoke")
	{
		temp = std::shared_ptr<ParticleSystemBase>(new SmokeParticle(id, 
			D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(-3.0f, -9.8f, 0.0f), 2000, 0.003f));
		temp->InitVBuffer();
	}
	else if(particle == "DefaultSpark")
	{
		temp = std::shared_ptr<ParticleSystemBase>(new ExplosionParticle(id, 
			D3DXVECTOR3(0.0f, -9.8f, 0.0f), 2500, 0.0025f));
		temp->InitVBuffer();
	}
	else if(particle == "Geyser")
	{
		temp = std::shared_ptr<ParticleSystemBase>(new SmokeParticle(id, 
			D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.5f, -9.8f, 0.0f), 2000, 0.00005f));
		temp->m_fDuration = 8.0f;
		
		Vector3 minV(-0.5f, 15.0f, -0.5f);
		Vector3 maxV(0.5f, 20.0f, 0.5f);
		((SmokeParticle*)(temp.get()))->SetMinVel( minV );
		((SmokeParticle*)(temp.get()))->SetMaxVel( maxV );
		
		temp->InitVBuffer();
		temp->Update(4.0f);	// Forward past the first pass
	}
	else if(particle == "JetFlame")
	{
		temp = std::shared_ptr<ParticleSystemBase>(new JetFlameParticle(id, 
			D3DXVECTOR3(0.0f, 0.0f, 0.0f), 1500, 0.00001f));
		if(param)
			((JetFlameParticle*)(temp.get()))->SetSpeed((float*)param);
		temp->InitVBuffer();
	}
	else if(particle == "SmokeCloud")
	{
		Vector3 initPos(2.0f, 2.0f, 2.0f);
		if(param)
		{
			initPos = (*(Vector3*)(param));
			initPos.y = 2.0f;	
		}
		temp = std::shared_ptr<ParticleSystemBase>(new SmokeParticle(id, 
			initPos, D3DXVECTOR3(0.5f, -9.8f, 0.0f), 2000, 0.0f, 400));

		Vector3 minV(6.0f, 15.0f, 6.0f);
		Vector3 maxV(7.0f, 20.0f, 7.0f);
		((SmokeParticle*)(temp.get()))->SetMinVel( minV );
		((SmokeParticle*)(temp.get()))->SetMaxVel( maxV );
		((SmokeParticle*)(temp.get()))->UsePolarVel( true );
		((SmokeParticle*)(temp.get()))->SetInitSize( Vector2(4.0f, 10.0f) );
		temp->InitVBuffer();
	}
	else if(particle == "SmokeTrail")
	{
		temp = std::shared_ptr<ParticleSystemBase>(new SmokeParticle(id, 
			D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), 2000, 0.003f, 10));
				
		Vector3 minV(-1.5f, -1.5f, -5.0f);
		Vector3 maxV(1.5f, 1.5f, -1.0);
		((SmokeParticle*)(temp.get()))->SetMinVel( minV );
		((SmokeParticle*)(temp.get()))->SetMaxVel( maxV );
		
		temp->InitVBuffer();
		temp->Update(4.0f);	// Forward past the first pass
	}
	else if(particle == "SmallSpark")
	{
		temp = std::shared_ptr<ParticleSystemBase>(new ExplosionParticle(id, 
			D3DXVECTOR3(0.0f, -9.8f, 0.0f), 2500, 0.0025f));
		
		((ExplosionParticle*)(temp.get()))->SetMinVel( 5.0f );
		((ExplosionParticle*)(temp.get()))->SetMaxVel( 10.0f );
		temp->InitVBuffer();
	}
	else if(particle == "SmallFire")
	{
		temp = std::shared_ptr<ParticleSystemBase>(new FireParticle(id, 
			D3DXVECTOR3(0.0f, 0.9f, 0.0f), 1500, 0.0025f));
		
		((FireParticle*)(temp.get()))->SetInitSize( Vector2(0.1f, 0.8f) );
		((FireParticle*)(temp.get()))->SetLifeSpan( Vector2(1.5f, 2.5f) );
		temp->InitVBuffer();
	}
	else if(Load(id, particle))
	{
		return;
	}
	else
		return;		// Particle doesn't exist

	// Check if the vector has been created yet
	if(m_ParticleMap.find(id) == m_ParticleMap.end())
		m_ParticleMap.insert(std::pair<int, std::vector<std::shared_ptr<ParticleSystemBase>>>( id, std::vector<std::shared_ptr<ParticleSystemBase>>() ));
	
	// Insert the particle into the map
	m_ParticleMap[id].push_back(temp);
}
void ParticleManager::RemoveParticle(int id)
{
	auto iter = m_ParticleMap.find(id);	// Make sure the particle is in the map
	if(iter == m_ParticleMap.end())
		return;		// Particle not found 

	// Remove the particles from the list
	auto list = &(*iter).second;
	for(auto i = list->begin();
		i != list->end(); i++)
		(*i)->Shutdown();

	// Remove the list from the map
	m_ParticleMap[id].clear();
	m_ParticleMap.erase( iter );
}