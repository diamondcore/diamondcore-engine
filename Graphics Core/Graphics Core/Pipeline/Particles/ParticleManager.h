#pragma once

#include "../../3D/VertexDecl.h"
//#include <map>
//#include <memory>
//#include <string>
#include <vector>
#include "../../SceneGraph/ObjectList.h"

// Output the number of particles alive with the object count
#define DISPLAY_ALIVE_COUNT	1


/***************************************
		  Particle System Base
****************************************/
#define MAX_PARTICLES 200	// Max number of particles and constant registers that can be used per instance
class ParticleSystemBase
{
	friend class ParticleManager;

protected:
	std::vector<VertexParticle>		m_ParticleList;
	std::vector<VertexParticle*>	m_pAliveParticles;
	std::vector<VertexParticle*>	m_pDeadParticles;
	
	// If particle count is over MAX_PARTICLES
	// then create a list of these particles
	std::shared_ptr<ParticleSystemBase>	m_SiblingParticle;

protected:
	int						m_ID;			// Shared Memory ID
	D3DXMATRIX				m_WorldMat;
	D3DXVECTOR4*			m_ParticleInst;	// World Matrix for vertex instancing
	IDirect3DVertexBuffer9*	m_VertexBuff;

	D3DXVECTOR3	m_Accel;		// Particle Acceleration
	float		m_fMass;
	float		m_fTimeRunning;	// Time this particle has been running
	float		m_fDuration;	// How long the effect lasts before stopping
	float*		m_pTimeAccum;	// Points to outer most child's accumulated time
	float		m_fEmitterTime;	// Time to pass before the next particle can be emitted
	int			m_iNumParticles;
	std::string	m_TechName;
	std::string	m_TexName;
	std::string	m_MapName;
	void	InitVBuffer();		// Only call in derived constructor	

public:
	ParticleSystemBase(int ID, const std::string& techName,
					   const std::string& texName,
					   const D3DXVECTOR3& accel,
					   float mass,
					   float emitterTime,
					   int numParticles);
	virtual ~ParticleSystemBase();
	virtual void	PreRestartDevice();
	virtual void	PostRestartDevice();

	virtual void	InitParticle(VertexParticle& particleOut);
	virtual void	Shutdown();
	virtual std::shared_ptr<ParticleSystemBase>	Clone();

	void	SetWorldMat(D3DXMATRIX world);
	void	Update(const float& dt);
	virtual void	Render();
	virtual void	RawRender();

	virtual void	AddParticle();

	// Methods to help manage entities
	void	SetMapName(std::string refName);
	bool	operator== (const std::string& rhs);
};
typedef std::shared_ptr<ParticleSystemBase>	ParticleSystemBasePtr;


/***************************************
		  Explosion Particle System
****************************************/
class ExplosionParticle	: public ParticleSystemBase
{
	float	m_fInitVelMin;
	float	m_fInitVelMax;

public:
	ExplosionParticle(int id, const D3DXVECTOR3& accel, float mass, float emitTime);
	~ExplosionParticle();
	
	void	SetMinVel(float min){m_fInitVelMin = min;}
	void	SetMaxVel(float max){m_fInitVelMax = max;}
	virtual void	InitParticle(VertexParticle& particleOut);
};


/***************************************
		  Fire Particle System
****************************************/
class FireParticle	: public ParticleSystemBase
{
	Vector2	m_vInitSize;
	Vector2	m_vLifeSpan;

public:
	FireParticle(int id, const D3DXVECTOR3& accel, float mass, float emitTime);
	~FireParticle();
	
	void	SetInitSize(Vector2 size){m_vInitSize = size;}
	void	SetLifeSpan(Vector2 life){m_vLifeSpan = life;}
	virtual void	InitParticle(VertexParticle& particleOut);
};


/***************************************
		  Jet Flame Particle System
****************************************/
class JetFlameParticle	: public ParticleSystemBase
{
	float*	m_pSpeed;

public:
	JetFlameParticle(int id, const D3DXVECTOR3& accel, float mass, float emitTime, int numParticles = 25);
	~JetFlameParticle();
	
	void	SetSpeed(float* speed){m_pSpeed = speed;}

	virtual void	InitParticle(VertexParticle& particleOut);
	virtual void	Render();
	virtual std::shared_ptr<ParticleSystemBase>	Clone();
};


/***************************************
		  Smoke Particle System
****************************************/
class SmokeParticle	: public ParticleSystemBase
{
	Vector3	m_vInitPosRange;
	Vector3 m_vInitVelMin;
	Vector3 m_vInitVelMax;
	Vector2	m_vInitSize;
	bool	m_bPolarVel;

public:
	SmokeParticle(int id, const D3DXVECTOR3& pos, const D3DXVECTOR3& accel, float mass, float emitTime, int numParticles = 200);
	~SmokeParticle();

	void	SetMinVel(Vector3 min){m_vInitVelMin = min;}
	void	SetMaxVel(Vector3 max){m_vInitVelMax = max;}
	void	SetInitSize(Vector2 size){m_vInitSize = size;}
	void	UsePolarVel(bool enable){m_bPolarVel = enable;}

	virtual std::shared_ptr<ParticleSystemBase>	Clone();
	virtual void	InitParticle(VertexParticle& particleOut);
};


/***************************************
		  Bullet Particle System
****************************************/
class BulletParticle	: public ParticleSystemBase
{
public:
	BulletParticle(int id, const D3DXVECTOR3& accel, float mass, float emitTime);
	~BulletParticle();

	virtual void	InitParticle(VertexParticle& particleOut);
};


/***************************************
		  Rain Particle System
****************************************/
class RainParticle	: public ParticleSystemBase
{
public:
	RainParticle(int id, const D3DXVECTOR3& accel, float mass, float emitTime);
	~RainParticle();

	virtual void	InitParticle(VertexParticle& particleOut);
};



/***************************************
			Particle Manager
****************************************/
class ParticleManager
{
	std::map<int, std::vector<std::shared_ptr<ParticleSystemBase>>>	m_ParticleMap;

	// Custom Method
	void	CreateExplosion();

	void	Update(const float &dt);
	void	PreRender();

	bool	Load(int id, std::string propertiesFile);
	//void	Unload(std::string propertiesFile);

public:
	ParticleManager();
	~ParticleManager();
	void	PreRestartDevice();
	void	PostRestartDevice();

	void	Init();
	void	Shutdown();
	void	Clear();

	void	CreateParticle(int id, std::string particle, void* param);
	void	RemoveParticle(int id);

	void	Update(Module* sharedMem);
	void	Render();
};