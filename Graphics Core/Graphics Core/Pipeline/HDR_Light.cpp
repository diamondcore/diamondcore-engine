#include "HDR_Light.h"
#include "../DirectXClass.h"
#include "../MacroTools.h"
#include "DeferredShader.h"
#include "../GraphicsCore.h"
#include <fstream>

// Helper functions
void GetTextureRect(IDirect3DTexture9** pTexture, RECT* pRect);
void GetTextureCoords(IDirect3DTexture9** pSrcTex, RECT* pSrcRect,
				IDirect3DTexture9** pDestTex, RECT* pDestRect, QuadCoord* pCoords);
void SaveToFile(IDirect3DTexture9* m_pTexture, const char* name);

// This technically should help(optimize), but noticing a large 
// amount of jitter on turns when this is enabled.
#define WAIT_FOR_REBUILD 0

HDR_Light::HDR_Light()
	:m_bUseToneMap(true),
	m_bNeedToRebuildLightPos(false),
	m_bNeedToRebuildLights(false),
	m_pFX(0)
{}
HDR_Light::~HDR_Light()
{}
void HDR_Light::PreRestartDevice()
{
	//SAFE_RELEASE(LDR.m_pTopSurface);
	m_pHDRsurface.PreRestartDevice();
	m_pScaledSurface.PreRestartDevice();
	m_pAdaptedLumPrevSurface.PreRestartDevice();
	m_pAdaptedLumCurSurface.PreRestartDevice();
	m_pBloomSurface.PreRestartDevice();
	m_pBrightSurface.PreRestartDevice();
	m_pStarSurface.PreRestartDevice();
	//LDR.PreRestartDevice();
	for(int i = 0; i < NUM_BLOOM_TEX; i++)
		SAFE_RELEASE(m_pBloomTex[i]);
	for(int i = 0; i < NUM_STAR_TEX; i++)
		SAFE_RELEASE(m_pStarTex[i]);
	for(int i = 0; i < NUM_TONEMAP_TEX; i++)
		SAFE_RELEASE(m_pToneMapTex[i]);
	m_pFX->OnLostDevice();
}
void HDR_Light::PostRestartDevice()
{
	m_pFX->OnResetDevice();
	float width = GCI->GetDirectX()->GetScreenWidth();
	float height = GCI->GetDirectX()->GetScreenHeight();
	m_CropWidth = width - (int)width % 8;
	m_CropHeight = height - (int)height % 8;

	m_pHDRsurface.m_iWidth = width;
	m_pHDRsurface.m_iHeight = height;
	m_pHDRsurface.PostRestartDevice();

	m_pScaledSurface.m_iWidth = m_CropWidth/4;
	m_pScaledSurface.m_iHeight = m_CropHeight/4;
	m_pScaledSurface.PostRestartDevice();
	
	m_pBrightSurface.m_iWidth = m_CropWidth/4 + 2;
	m_pBrightSurface.m_iHeight = m_CropHeight/4 + 2;
	m_pBrightSurface.PostRestartDevice();

	m_pStarSurface.m_iWidth = m_CropWidth/4 + 2;
	m_pStarSurface.m_iHeight = m_CropHeight/4 + 2;
	m_pStarSurface.PostRestartDevice();

	m_pBloomSurface.m_iWidth = m_CropWidth/8 + 2;
	m_pBloomSurface.m_iHeight = m_CropHeight/8 + 2;
	m_pBloomSurface.PostRestartDevice();

	m_pAdaptedLumCurSurface.m_iWidth = 1;
	m_pAdaptedLumCurSurface.m_iHeight = 1;
	m_pAdaptedLumCurSurface.PostRestartDevice();

	m_pAdaptedLumPrevSurface.m_iWidth = 1;
	m_pAdaptedLumPrevSurface.m_iHeight = 1;
	m_pAdaptedLumPrevSurface.PostRestartDevice();

	/*LDR.m_iWidth	= width;
	LDR.m_iHeight	= height;
	LDR.PostRestartDevice();*/

	// Tone-Map
	for(int i = 0; i < NUM_TONEMAP_TEX; i++)
	{
		int sampleLen = 1 << (2 * i);

		GCI->GetDirectX()->GetDevice()->CreateTexture(sampleLen, sampleLen, 1,
						D3DUSAGE_RENDERTARGET, m_LumFormat, D3DPOOL_DEFAULT,
						&m_pToneMapTex[i], 0);
	}

	// Bloom
	for(int i = 1; i < NUM_BLOOM_TEX; i++)
		GCI->GetDirectX()->GetDevice()->CreateTexture(m_CropWidth/8 + 2, m_CropHeight/8 + 2, 1,
						D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT,
						&m_pBloomTex[i], 0);
	
	GCI->GetDirectX()->GetDevice()->CreateTexture(m_CropWidth/8, m_CropHeight/8, 1,
								D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT,
								&m_pBloomTex[0], 0);

	// Star
	for(int i = 0; i < NUM_STAR_TEX; i++)
		GCI->GetDirectX()->GetDevice()->CreateTexture(m_CropWidth/4, m_CropHeight/4, 1,
						D3DUSAGE_RENDERTARGET, D3DFMT_A16B16G16R16F, D3DPOOL_DEFAULT,
						&m_pStarTex[i], 0);
	

	// Clear the textures used with scissor rect testing
	GCI->GetDirectX()->GetDevice()->ColorFill(m_pAdaptedLumCurSurface.m_pTopSurface, 0, D3DCOLOR_ARGB(0,0,0,0));
	GCI->GetDirectX()->GetDevice()->ColorFill(m_pAdaptedLumPrevSurface.m_pTopSurface, 0, D3DCOLOR_ARGB(0,0,0,0));
	GCI->GetDirectX()->GetDevice()->ColorFill(m_pBloomSurface.m_pTopSurface, 0, D3DCOLOR_ARGB(0,0,0,0));
	GCI->GetDirectX()->GetDevice()->ColorFill(m_pBrightSurface.m_pTopSurface, 0, D3DCOLOR_ARGB(0,0,0,0));
	GCI->GetDirectX()->GetDevice()->ColorFill(m_pStarSurface.m_pTopSurface, 0, D3DCOLOR_ARGB(0,0,0,0));

	IDirect3DSurface9* pSurface;
	for(int i = 0; i < NUM_BLOOM_TEX; i++)
	{
		m_pBloomTex[i]->GetSurfaceLevel(0, &pSurface);
		GCI->GetDirectX()->GetDevice()->ColorFill(pSurface, 0, D3DCOLOR_ARGB(0,0,0,0));
		SAFE_RELEASE(pSurface);
	}
	
	// Set some effect files
	m_pFX->SetMatrix("g_sProj", &GCI->GetCamera()->GetProj());
	m_pFX->SetFloat("g_sBloomScale", 1.0f);
	m_pFX->SetFloat("g_sStarScale", 0.5f);
	D3DXVECTOR2 screen;
	screen.x = GCI->GetDirectX()->GetScreenWidth();
	screen.y = GCI->GetDirectX()->GetScreenHeight();
	m_pFX->SetValue("g_sScreenSize", &screen, sizeof(D3DXVECTOR2));
}

void HDR_Light::Init()
{
	DSX = GCI->GetDeferredR();

	float width = GCI->GetDirectX()->GetScreenWidth();
	float height = GCI->GetDirectX()->GetScreenHeight();
	m_CropWidth = width - (int)width % 8;
	m_CropHeight = height - (int)height % 8;

	m_pHDRsurface.m_iWidth = width;
	m_pHDRsurface.m_iHeight = height;
	m_pHDRsurface.setDepthStencil(D3DFMT_D24S8, false);
	m_pHDRsurface.setTexture(D3DFMT_A16B16G16R16F, false);
	m_pHDRsurface.Init();
	
	m_pScaledSurface.m_iWidth = m_CropWidth/4;
	m_pScaledSurface.m_iHeight = m_CropHeight/4;
	m_pScaledSurface.setDepthStencil(D3DFMT_D24S8, false);
	m_pScaledSurface.setTexture(D3DFMT_A16B16G16R16F, false);
	m_pScaledSurface.Init();
	
	m_pBrightSurface.m_iWidth = m_CropWidth/4 + 2;
	m_pBrightSurface.m_iHeight = m_CropHeight/4 + 2;
	m_pBrightSurface.setDepthStencil(D3DFMT_D24S8, false);
	m_pBrightSurface.setTexture(D3DFMT_A8R8G8B8, false);
	m_pBrightSurface.Init();
	
	m_pStarSurface.m_iWidth = m_CropWidth/4 + 2;
	m_pStarSurface.m_iHeight = m_CropHeight/4 + 2;
	m_pStarSurface.setDepthStencil(D3DFMT_D24S8, false);
	m_pStarSurface.setTexture(D3DFMT_A8R8G8B8, false);
	m_pStarSurface.Init();
	
	m_pBloomSurface.m_iWidth = m_CropWidth/8 + 2;
	m_pBloomSurface.m_iHeight = m_CropHeight/8 + 2;
	m_pBloomSurface.setDepthStencil(D3DFMT_D24S8, false);
	m_pBloomSurface.setTexture(D3DFMT_X8R8G8B8, false);
	m_pBloomSurface.Init();
	
	// Get the luminance format that is supported
	m_LumFormat = GCI->GetDirectX()->GetLuminanceFormat();
	//m_LumFormat = D3DFMT_X8R8G8B8;	// Debug output format
	
	m_pAdaptedLumCurSurface.m_iWidth = 1;
	m_pAdaptedLumCurSurface.m_iHeight = 1;
	m_pAdaptedLumCurSurface.setDepthStencil(D3DFMT_D24S8, false);
	m_pAdaptedLumCurSurface.setTexture(m_LumFormat, false);
	m_pAdaptedLumCurSurface.Init();
	
	m_pAdaptedLumPrevSurface.m_iWidth = 1;
	m_pAdaptedLumPrevSurface.m_iHeight = 1;
	m_pAdaptedLumPrevSurface.setDepthStencil(D3DFMT_D24S8, false);
	m_pAdaptedLumPrevSurface.setTexture(m_LumFormat, false);
	m_pAdaptedLumPrevSurface.Init();

	/*LDR.m_iWidth	= width;
	LDR.m_iHeight	= height;
	LDR.setDepthStencil(D3DFMT_D24S8, false);
	LDR.setTexture(D3DFMT_A8R8G8B8, false);
	LDR.Init();*/

	// For each scale stage, create a texture to hold the intermediate results of luminance calculation
	for(int i = 0; i < NUM_TONEMAP_TEX; i++)
	{
		int sampleLen = 1 << (2 * i);
		GCI->GetDirectX()->GetDevice()->CreateTexture(sampleLen, sampleLen, 1, D3DUSAGE_RENDERTARGET,
				m_LumFormat, D3DPOOL_DEFAULT, &m_pToneMapTex[i], 0);
	}

	// Create the temporary blooming effect textures
	for(int i = 1; i < NUM_BLOOM_TEX; i++)
		GCI->GetDirectX()->GetDevice()->CreateTexture(m_CropWidth/8 + 2, m_CropHeight/8 + 2, 1,
									D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT,
									&m_pBloomTex[i], 0);
	
	GCI->GetDirectX()->GetDevice()->CreateTexture(m_CropWidth/8, m_CropHeight/8, 1,
								D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT,
								&m_pBloomTex[0], 0);

	for(int i = 0; i < NUM_STAR_TEX; i++)
		GCI->GetDirectX()->GetDevice()->CreateTexture(m_CropWidth/4, m_CropHeight/4, 1,
								D3DUSAGE_RENDERTARGET,  D3DFMT_A16B16G16R16F, D3DPOOL_DEFAULT,
								&m_pStarTex[i], 0);
	
	// Clear the textures used with scissor rect testing
	GCI->GetDirectX()->GetDevice()->ColorFill(m_pAdaptedLumCurSurface.m_pTopSurface, 0, D3DCOLOR_ARGB(0,0,0,0));
	GCI->GetDirectX()->GetDevice()->ColorFill(m_pAdaptedLumPrevSurface.m_pTopSurface, 0, D3DCOLOR_ARGB(0,0,0,0));
	GCI->GetDirectX()->GetDevice()->ColorFill(m_pBloomSurface.m_pTopSurface, 0, D3DCOLOR_ARGB(0,0,0,0));
	GCI->GetDirectX()->GetDevice()->ColorFill(m_pBrightSurface.m_pTopSurface, 0, D3DCOLOR_ARGB(0,0,0,0));
	GCI->GetDirectX()->GetDevice()->ColorFill(m_pStarSurface.m_pTopSurface, 0, D3DCOLOR_ARGB(0,0,0,0));

	IDirect3DSurface9* pSurface;
	for(int i = 0; i < NUM_BLOOM_TEX; i++)
	{
		m_pBloomTex[i]->GetSurfaceLevel(0, &pSurface);
		GCI->GetDirectX()->GetDevice()->ColorFill(pSurface, 0, D3DCOLOR_ARGB(0,0,0,0));
		SAFE_RELEASE(pSurface);
	}

	// Set up Lights
	//AddLight(D3DXVECTOR4(2.0f, 10.0f, -27.0f, 1.0f), 56);	// 52
	//AddLight(D3DXVECTOR4(25.0f, 2.0f, 30.0f, 1.0f), 56);	// 52
	//AddLight(D3DXVECTOR4(75.0f, 10.0f, 75.0f, 1.0f), 56);	// 43
	//AddLight(D3DXVECTOR4(120.0f, 2.0f, 25.0f, 1.0f), 63);	// 59 // 65
	//AddLight(D3DXVECTOR4(25.0f, 2.0f, 125.0f, 1.0f), 50);	// 35
	//AddLight(D3DXVECTOR4(125.0f, 2.0f, 125.0f, 1.0f), 50);	// 35
	
	//AddLight(D3DXVECTOR4(15.0f, 2.0f, 10.0f, 1.0f), D3DXVECTOR4(1.0f, 0.0f, 0.0f, 0.0f), 40);//56);	// 52
	//AddLight(D3DXVECTOR4(15.0f, 10.0f, 15.0f, 1.0f), 56);	// 43
	//AddLight(D3DXVECTOR4(20.0f, 2.0f, 15.0f, 1.0f), 63);	// 59 // 65
	//AddLight(D3DXVECTOR4(25.0f, 2.0f, 25.0f, 1.0f), 50);	// 35
	//AddLight(D3DXVECTOR4(25.0f, 2.0f, 25.0f, 1.0f), 50);	// 35

#if LIGHT_DEMO
	// Change the Light colors
	/*m_LightIntensity[0].x *= 1.0f;
	m_LightIntensity[0].y *= 0.0f;
	m_LightIntensity[0].z *= 0.0f;
	m_LightIntensity[1].x *= 0.0f;
	m_LightIntensity[1].y *= 1.0f;
	m_LightIntensity[1].z *= 0.0f;
	m_LightIntensity[2].x *= 0.765f;
	m_LightIntensity[2].y *= 0.0f;
	m_LightIntensity[2].z *= 0.765f;
	m_LightIntensity[3].x *= 0.0f;
	m_LightIntensity[3].y *= 0.2f;
	m_LightIntensity[3].z *= 1.0f;
	m_LightIntensity[4].x *= 1.0f;
	m_LightIntensity[4].y *= 1.0f;
	m_LightIntensity[4].z *= 0.0f;*/
#endif
	RefreshLights();	// Default way to setup lights

	// Initialize the Lens Glare and Star static libraries
	LensGlare::InitStaticGlareLibs();
	m_GlareDef = LensGlare::GetGlareLib(GLARE_FILTER_SUNNYCROSS_SPECTRAL);
	//m_GlareDef = LensGlare::GetGlareLib(GLARE_FILTER_SNOWCROSS);
	//m_GlareDef = LensGlare::GetGlareLib(GLARE_FILTER_CROSSCREEN);
	//m_GlareDef = LensGlare::GetGlareLib(GLARE_AFTERIMAGE);
	//m_GlareDef = LensGlare::GetGlareLib(GLARE_CINEMACAM_VERTICALSLITS);
}
void HDR_Light::ReloadShader()
{
	// Safely release old shader 
	SAFE_RELEASE( m_pFX );

	// Create the Effect File
	ID3DXBuffer* FX_Errors;	// Stores the Error Codes
#if defined(DEBUG) | defined(_DEBUG)
	D3DXCreateEffectFromFile(GCI->GetDirectX()->GetDevice(), "Content/Shaders/HDR_Light.fx",	//"Graphics/3D/Textures/DeferredShader.fx", 
					0, 0, 0, 0, &m_pFX, &FX_Errors);
#else
	// Load compiled effect
	std::ifstream file( "Content/Shaders/HDR_Light.fxo", std::ios::binary );
	file.seekg(0, std::ios_base::end);
	int size = (int)file.tellg();
	file.seekg(0, std::ios_base::beg);
	std::vector<char> compiledShader(size);

	file.read(&compiledShader[0], size);
	file.close();
	D3DXCreateEffect(GCI->GetDirectX()->GetDevice(), &compiledShader[0], size, 
					0, 0, 0, 0, &m_pFX, &FX_Errors);
#endif
	if(FX_Errors)		// Output if there is any errors
		MessageBoxA(0, (char*)FX_Errors->GetBufferPointer(), 0, 0);

	// Set some effect files
	m_pFX->SetMatrix("g_sProj", &GCI->GetCamera()->GetProj());
	m_pFX->SetFloat("g_sBloomScale", 1.0f);
	m_pFX->SetFloat("g_sStarScale", 0.5f);
	D3DXVECTOR2 screen;
	screen.x = GCI->GetDirectX()->GetScreenWidth();
	screen.y = GCI->GetDirectX()->GetScreenHeight();
	m_pFX->SetValue("g_sScreenSize", &screen, sizeof(D3DXVECTOR2));
}
void HDR_Light::Shutdown()
{
	PreRestartDevice();/*
	for(int i = 0; i < NUM_BLOOM_TEX; i++)
		SAFE_RELEASE(m_pBloomTex[i]);
	for(int i = 0; i < NUM_STAR_TEX; i++)
		SAFE_RELEASE(m_pStarTex[i]);
	for(int i = 0; i < NUM_TONEMAP_TEX; i++)
		SAFE_RELEASE(m_pToneMapTex[i]);*/

	// Clear all of the lists
	m_DynamicLights.clear();
	m_LightPos.clear();
	m_LightPosView.clear();
	m_LightIntensity.clear();
	m_LightLogIntensity.clear();
	m_LightMantissa.clear();
	while(!m_iAvailableIDs.empty())
		m_iAvailableIDs.pop();
	SAFE_RELEASE(m_pFX);
}

void HDR_Light::Render()
{
	//IDirect3DSurface9*	LDR;	// Low dynamic range
	//IDirect3DSurface9*	LDR_DS;	// Low dynamic range depth stencil

	// Store the LDR back buffer
	///GCI->GetDirectX()->GetDevice()->GetRenderTarget(0, &LDR.m_pTopSurface);
	//GCI->GetDirectX()->GetDevice()->GetDepthStencilSurface(&LDR_DS);	
	
	auto camera = GCI->GetCamera();
	D3DXMATRIX view = camera->GetView();
	D3DXMATRIX viewProj = camera->GetViewProj();
	m_pFX->SetMatrix("g_sViewProj", &viewProj);
	m_pFX->SetMatrix("g_sView", &view);
	m_pFX->SetValue("g_sCameraPos", &D3DXVECTOR3(view._41, view._42, view._43), sizeof(D3DXVECTOR3));
	/*D3DXMatrixInverse(&view, 0, &view);
	D3DXMatrixInverse(&viewProj, 0, &viewProj);
	m_pFX->SetMatrix("g_sViewInv", &view);
	m_pFX->SetMatrix("g_sViewProjInv", &viewProj);
	m_pFX->SetValue("g_sCameraPosInv", &D3DXVECTOR3(view._41, view._42, view._43), sizeof(D3DXVECTOR3));
	*/
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, m_pHDRsurface.m_pTopSurface);
	//LDR.SaveToFile("LDR");

	m_pFX->SetTexture("g_sDeferredMtrl", DSX->m_MtrlSurface.m_pTexture);
	m_pFX->SetTexture("g_sDeferredNormW", DSX->m_NormSurface.m_pTexture);
	m_pFX->SetTexture("g_sDeferredPosW", DSX->m_PosSurface.m_pTexture);
	m_pFX->SetTexture("g_sDeferredDepth", DSX->m_DepthSurface.m_pTexture);
	m_pFX->SetTexture("g_sSSAOMap", DSX->m_SSAOsurface.m_pTexture);
	m_pFX->SetBool("f_UseSSAO", DSX->m_bUseSSAO);

#if LIGHT_DEMO
	// Rotate lights
	/*D3DXMATRIX R;
	static float angle = 0.0f;
	angle += 0.01f;
	if(angle > 2.0f*D3DX_PI)
		angle = 0.0f;
	m_LightPos[3].x = 10.0f*sinf(angle) + 10.0f*cosf(angle);
	m_LightPos[3].z = 10.0f*cosf(angle) - 10.0f*sinf(angle);
	m_LightPos[3].x += 25.0f;
	m_LightPos[3].z += 125.0f;
	
	angle += 0.01f;
	if(angle > 2.0f*D3DX_PI)
		angle = 0.0f;
	m_LightPos[4].x = 10.0f*sinf(angle) - 10.0f*cosf(angle);
	m_LightPos[4].z = 10.0f*cosf(angle) + 10.0f*sinf(angle);
	m_LightPos[4].x += 125.0f;
	m_LightPos[4].z += 125.0f;*/
#endif
	
#if WAIT_FOR_REBUILD
	if(m_bNeedToRebuildLights)	// Rebuild light vectors
		RebuildShaderArrays();
	else if(m_bNeedToRebuildLightPos)
	{
		// Only rebuild position array
		m_LightPos.clear();
		for(auto i = m_DynamicLights.begin(); i != m_DynamicLights.end(); i++)
			if((*i).m_bUse)
				m_LightPos.push_back((*i).m_vPos);
		m_bNeedToRebuildLightPos = 0;
	}
#endif

	auto numLights = m_LightPos.size();
	m_pFX->SetInt("g_sNum_Lights", numLights);
	if (numLights > 0)
	{
		// Calculate light position
		for(int light = 0; light < numLights; light++)
		{
			D3DXVec4Transform(&m_LightPosView[light], &m_LightPos[light], &view);
		} 
		m_pFX->SetVectorArray("g_sLightPos", &m_LightPosView[0], numLights);
		//m_pFX->SetVectorArray("g_sLightPos", &m_LightPos[0], numLights);
		m_pFX->SetVectorArray("g_sLightIntensity", &m_LightIntensity[0], numLights);
	}
	
	m_pFX->SetBool("g_sUseToneMap", m_bUseToneMap);
	m_pFX->SetBool("g_sUseBlueShift", true);
	m_pFX->CommitChanges();

	GCI->GetDirectX()->GetDevice()->Clear(0, NULL, D3DCLEAR_TARGET,	// | D3DCLEAR_ZBUFFER, 
						D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f), 1.0f, 0);
						//D3DXCOLOR(0.0f, 0.2f, 0.4f, 1.0f), 1.0f, 0);
	// Render Scene HDR
	RenderScene();

	// Create a scaled copy of the scene
	SceneToScale();
	//m_pHDRsurface.SaveToFile("HDR_Scene");
	
	// If tone-mapping is enabled, then measure luminance
	if(m_bUseToneMap)
		MeasureLuminance();
	//m_pScaledSurface.SaveToFile("Scaled");

	// Calculate the current luminance adaption level
	CalculateAdaptation();	

	// Remove everything except bright light and reflection
	ScaleToBright();

	// Blur the bright-pass filtered image
	BrightToStar();
	//m_pBrightSurface.SaveToFile("Bright");

	// Scale-down the source texture
	StarToBloom();
	//m_pStarSurface.SaveToFile("StarInit");

	// Render Bloom
	RenderBloom();

	// Render Star
	RenderStar();


	// Set technique to the run the final scene pass
	m_pFX->SetTechnique("FinalPassTech");
	m_pFX->SetFloat("g_sMiddleGray", 18.0f/100.0f); // Slider 18/100.0f

	// Set the back buffer as the render target
	//if(DSX->m_bUseBlur)
	//	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, DSX->m_LightSurface.m_pTopSurface);//LDR.m_pTopSurface);
	//else
		GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, DSX->m_pBackBuff);//LDR.m_pTopSurface);
	GCI->GetDirectX()->GetDevice()->SetTexture(0, m_pHDRsurface.m_pTexture);
	GCI->GetDirectX()->GetDevice()->SetTexture(1, m_pBloomTex[0]);
	GCI->GetDirectX()->GetDevice()->SetTexture(2, m_pStarTex[0]);
	GCI->GetDirectX()->GetDevice()->SetTexture(3, m_pAdaptedLumCurSurface.m_pTexture);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(2, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(2, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(3, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(3, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	m_pFX->CommitChanges();

	// Debug Bloom and Star
	//SaveToFile(m_pBloomTex[0], "Bloom");
	//SaveToFile(m_pStarTex[0], "Star");

	UINT numPasses = 0;
	m_pFX->Begin(&numPasses, 0);
	for(UINT pass = 0; pass < numPasses; pass++)
	{
		m_pFX->BeginPass(pass);

		// Draw full screen quad
		DSX->DrawFullScreenQuad(QuadCoord(0.0f, 0.0f, 1.0f, 1.0f));

		m_pFX->EndPass();
	}
	m_pFX->End();


	// Clean Up
	//GCI->GetDirectX()->GetDevice()->SetTexture(0, NULL);
	GCI->GetDirectX()->GetDevice()->SetTexture(1, NULL);
	GCI->GetDirectX()->GetDevice()->SetTexture(2, NULL);
	GCI->GetDirectX()->GetDevice()->SetTexture(3, NULL);

	// Apply a final motion blur pass
	/*if(DSX->m_bUseBlur)
	{
		m_pFX->SetTexture(DSX->m_hDefMtrl, DSX->m_LightSurface.m_pTexture);
		DSX->ApplyBlur(DSX->m_pBackBuff);
	}*/

	SAFE_RELEASE(DSX->m_pBackBuff);//LDR.m_pTopSurface);
//	SAFE_RELEASE(LDR_DS);
}

void HDR_Light::RenderScene()
{
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);

	D3DXMATRIX view = GCI->GetCamera()->GetView();

	m_pFX->SetTechnique("SpecTech");
	//m_pFX->SetMatrix("g_sWVP", &view);
	m_pFX->SetMatrix("g_sView", &view);	// Switched with WVP
	m_pFX->CommitChanges();

	UINT numPasses;
	m_pFX->Begin(&numPasses, D3DXFX_DONOTSAVESTATE);//0);
	for(UINT pass = 0; pass < numPasses; pass++)
	{
		m_pFX->BeginPass(pass);
		////////////////////////////////////////////////////////////////////
		
		D3DXVECTOR4	emissive(0.0f, 0.0f, 0.0f, 0.0f);
		m_pFX->SetVector("g_sEmissive", &emissive);

		// Enable Texture
		m_pFX->SetFloat("g_sPhongExp", 5.0f);
		m_pFX->SetFloat("g_sPhongCoeff", 1.0f);
		m_pFX->SetFloat("g_sDiffuseCoeff", 1.0f);
		//m_pFX->SetTexture("g_sLightMap", DSX->m_LightSurface.m_pTexture);

		//m_pFX->SetTexture("g_sDeferredMtrl", LDR.m_pTexture);//DSX->m_pBlackTex->GetTexture());
		m_pFX->SetTexture(DSX->m_hDefMtrl, DSX->m_MtrlSurface.m_pTexture);
		m_pFX->SetTexture(DSX->m_hDefNormW, DSX->m_NormSurface.m_pTexture);
		m_pFX->SetTexture(DSX->m_hDefPosW, DSX->m_PosSurface.m_pTexture);
		m_pFX->SetTexture(DSX->m_hDefDepth, DSX->m_DepthSurface.m_pTexture);
		m_pFX->SetBool("g_sIsTex", true);
		GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		m_pFX->CommitChanges();

		// Render the Scene
		DSX->DrawFullScreenQuad(QuadCoord(0.0f, 0.0f, 1.0f, 1.0f));
		/*for(auto m = UFX->m_MainList.begin();
			m != UFX->m_MainList.end();
			m++)
		{
			(*m)->PreRender();
			(*m)->Render();
			(*m)->PostRender();
		}*/


		// Render the Lights
		m_pFX->SetFloat("g_sPhongExp", 5.0f);
		m_pFX->SetFloat("g_sPhongCoeff", 1.0f);
		m_pFX->SetFloat("g_sDiffuseCoeff", 1.0f);
		m_pFX->SetBool("g_sIsTex", false);
		//m_pFX->SetTexture("g_sTexture", DSX->m_pDefaultTex);
		for(int light = 0; light < m_LightPos.size(); light++)
		{
			D3DXMATRIXA16 scale, world, toView;
			float fScale = 0.05f;// * m_LightLogIntensity[light];
			D3DXMatrixScaling(&scale, fScale, fScale, fScale);

			//view = GCI->GetCamera()->GetView();
			//world = DSX->m_WorldMat;
			D3DXMatrixTranslation(&world, m_LightPos[light].x, 
						m_LightPos[light].y, m_LightPos[light].z);

			world = scale * world;
			//toView = world * view;
			//m_pFX->SetMatrix("g_sWorld", &world);
			m_pFX->SetMatrix("g_sWVP", &(world*GCI->GetCamera()->GetViewProj()));
			m_pFX->SetMatrix("g_sView", &(world*view));

			// Set Emissive properties of the light
			emissive = EMISSIVE_COEFF * m_LightIntensity[light];
			m_pFX->SetVector("g_sEmissive", &emissive);

			// Render the light Sphere
			m_pFX->CommitChanges();
			GCI->GetMeshManager()->GetMesh("Sphere(33).x")->RawRender();
			//GCI->GetShapeManager()->m_Shape_Sphere->Render();
		} 

		////////////////////////////////////////////////////////////////////
		m_pFX->EndPass();
	}
	m_pFX->End();
}

void HDR_Light::MeasureLuminance()
{
	D3DXVECTOR2 sampleOffsets[MAX_SAMPLES];

	// Get the current texture id
	DWORD currentTex = NUM_TONEMAP_TEX - 1;

	// Sample log average luminance
	IDirect3DSurface9* toneMapSurface[NUM_TONEMAP_TEX] = {0};
	
	// Retrieve the tonemap surface
	HRESULT hr;
	for(int i = 0; i < NUM_TONEMAP_TEX; i++)
	{
		hr = m_pToneMapTex[i]->GetSurfaceLevel(0, &toneMapSurface[i]);
		if(FAILED(hr))
			MessageBox(0, "ToneMaping has crashed.", "Warning", MB_OK);
	}
	D3DSURFACE_DESC desc;
	m_pToneMapTex[currentTex]->GetLevelDesc(0, &desc);

	// Initialize the starting luminance offsets
	float tU, tV;
	tU = 1.0f/(3.0f * desc.Width);
	tV = 1.0f/(3.0f * desc.Height);

	int index = 0;
	for(int x = -1; x <= 1; x++)
		for(int y = -1; y <= 1; y++)
		{
			sampleOffsets[index].x = x * tU;
			sampleOffsets[index].y = y * tV;
			
			index++;
		}

	m_pFX->SetTechnique("StartAvgLumTech");
	m_pFX->SetValue("g_sSampleOffset", sampleOffsets, sizeof(sampleOffsets));

	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, toneMapSurface[currentTex]);
	GCI->GetDirectX()->GetDevice()->SetTexture(0, m_pScaledSurface.m_pTexture);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	m_pFX->CommitChanges();

	UINT numPasses;
	m_pFX->Begin(&numPasses, 0);
	for(UINT pass = 0; pass < numPasses; pass++)
	{
		m_pFX->BeginPass(pass);
		DSX->DrawFullScreenQuad(QuadCoord(0.0f, 0.0f, 1.0f, 1.0f));
		m_pFX->EndPass();
	}
	m_pFX->End();
	currentTex--;

	// Debug the Scaled Gray scale copy of the HDR scene
	//SaveToFile(m_pToneMapTex[NUM_TONEMAP_TEX-1], "ToneMap_GrayScale");

	while(currentTex > 0)
	{
		m_pToneMapTex[currentTex+1]->GetLevelDesc(0, &desc);
		GetDown4x4Offsets(desc.Width, desc.Height, sampleOffsets);

		m_pFX->SetTechnique("NextAvgLumTech");
		m_pFX->SetValue("g_sSampleOffset", sampleOffsets, sizeof(sampleOffsets));

		GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, toneMapSurface[currentTex]);
		GCI->GetDirectX()->GetDevice()->SetTexture(0, m_pToneMapTex[currentTex+1]);
		GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
		GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
		m_pFX->CommitChanges();

		numPasses = 0;
		m_pFX->Begin(&numPasses, 0);
		for(UINT pass = 0; pass < numPasses; pass++)
		{
			m_pFX->BeginPass(pass);
			DSX->DrawFullScreenQuad(QuadCoord(0.0f, 0.0f, 1.0f, 1.0f));
			m_pFX->EndPass();
		}
		m_pFX->End();
		currentTex--;
	}

	// Downsample to 1x1
	m_pToneMapTex[1]->GetLevelDesc(0, &desc);
	GetDown4x4Offsets(desc.Width, desc.Height, sampleOffsets);

	m_pFX->SetTechnique("FinishAvgLumTech");
	m_pFX->SetValue("g_sSampleOffset", sampleOffsets, sizeof(sampleOffsets));

	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, toneMapSurface[0]);
	GCI->GetDirectX()->GetDevice()->SetTexture(0, m_pToneMapTex[1]);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	m_pFX->CommitChanges();
	
	numPasses = 0;
	m_pFX->Begin(&numPasses, 0);
	for(UINT pass = 0; pass < numPasses; pass++)
	{
		m_pFX->BeginPass(pass);
		DSX->DrawFullScreenQuad(QuadCoord(0.0f, 0.0f, 1.0f, 1.0f));
		m_pFX->EndPass();
	}
	m_pFX->End();

	// Clean Up
	for(int i = 0; i < NUM_TONEMAP_TEX; i++)
		SAFE_RELEASE(toneMapSurface[i]);
}

void HDR_Light::CalculateAdaptation()
{
	// Swap current and previous luminance
	IDirect3DTexture9* pSwapTex = m_pAdaptedLumPrevSurface.m_pTexture;
	m_pAdaptedLumPrevSurface.m_pTexture = m_pAdaptedLumCurSurface.m_pTexture;
	m_pAdaptedLumCurSurface.m_pTexture = pSwapTex;

	m_pFX->SetTechnique("CalculateAdaptedLumTech");
	m_pFX->SetFloat("g_sTimeElapsed", GCI->GetTimeElapsed());

	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, m_pAdaptedLumCurSurface.m_pTopSurface);
	GCI->GetDirectX()->GetDevice()->SetTexture(0, m_pAdaptedLumPrevSurface.m_pTexture);
	GCI->GetDirectX()->GetDevice()->SetTexture(1, m_pToneMapTex[0]);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(1, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(1, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	m_pFX->CommitChanges();

	UINT numPasses;
	m_pFX->Begin(&numPasses, 0);
	for(UINT pass = 0; pass < numPasses; pass++)
	{
		m_pFX->BeginPass(pass);

		// Draw fullscreen quad
		DSX->DrawFullScreenQuad(QuadCoord(0.0f, 0.0f, 1.0f, 1.0f));

		m_pFX->EndPass();
	}
	m_pFX->End();
}

void HDR_Light::RenderStar()
{
	// Clear the star texture
	IDirect3DSurface9* pStarSurface = 0;
	m_pStarTex[0]->GetSurfaceLevel(0, &pStarSurface);
	GCI->GetDirectX()->GetDevice()->ColorFill(pStarSurface, 0, D3DCOLOR_ARGB(0,0,0,0));
	SAFE_RELEASE(pStarSurface);
	
	// Check glare to see if star is being used
	if(m_GlareDef.m_fGlareLum <= 0.0f ||
		m_GlareDef.m_fStarLum <= 0.0f)
		return;

	// Initial Constants for the star effect
	const StarLibrary& starDef = m_GlareDef.m_StarDef;
	const float tanFOV = atanf(D3DX_PI/8);
	static const int maxPasses = 3;
	static const int numSamples = 8;
	static D3DXVECTOR4 aaColor[maxPasses][8];
	static const D3DXCOLOR colorWhite(0.63f, 0.63f, 0.63f, 0.0f);

	D3DXVECTOR4 sampleWeights[MAX_SAMPLES];
	D3DXVECTOR2 sampleOffsets[MAX_SAMPLES];

	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

	IDirect3DSurface9* pSourceSurface = 0;
	IDirect3DSurface9* pDestSurface = 0;

	// Create the star surfaces
	IDirect3DSurface9* pStarSurfaces[NUM_STAR_TEX] = {0};
	for(int i = 0; i < NUM_STAR_TEX; i++)
		if(FAILED(m_pStarTex[i]->GetSurfaceLevel(0, &pStarSurfaces[i])))
			goto CleanUp;

	// Get the source texture dimensions
	float srcW = (float)m_pStarSurface.m_iWidth;
	float srcH = (float)m_pStarSurface.m_iHeight;

	for(int p = 0; p < maxPasses; p++)
	{
		float ratio;
		ratio = (float)(p+1)/(float)maxPasses;

		for(int s = 0; s < numSamples; s++)
		{
			D3DXCOLOR chromAberrColor;
			D3DXColorLerp( &chromAberrColor,
							&(StarLibrary::GetChromAberColor(s)),
							&colorWhite,
							ratio);

			D3DXColorLerp((D3DXCOLOR*)&(aaColor[p][s]),
							&colorWhite, &chromAberrColor,
							m_GlareDef.m_fChromaticAberration);
		}
	}

	float radOffset;
	radOffset = m_GlareDef.m_fStarInclination + starDef.m_fInclination;

	IDirect3DTexture9*	pSourceTex;

	// Direction Loop
	for(int d = 0; d < starDef.m_NumStarLines; d++)
	{
		const StarLine& starLine = starDef.m_pStarLines[d];

		pSourceTex = m_pStarSurface.m_pTexture;
		
		float rad = radOffset + starLine.m_fInclination;
		float sn = sinf(rad);
		float cs = cosf(rad);
		D3DXVECTOR2 stepUV;
		stepUV.x = sn / srcW * starLine.m_fSampleLength;
		stepUV.y = cs / srcH * starLine.m_fSampleLength;

		float attnPowScale = (tanFOV + 0.1f)*1.0f *(160.0f+120.0f)/(srcW+srcH)*1.2f;


		// 1 direction expansion loop
		GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		int workTex = 1;
		for(int p = 0; p < starLine.m_NumPasses; p++)
		{
			if(p == starLine.m_NumPasses -1)
				pDestSurface = pStarSurfaces[d+4];	// Last pass move to other work buffer
			else
				pDestSurface = pStarSurfaces[workTex];

			// Sampling configuration for each stage
			for(int i = 0; i < numSamples; i++)
			{
				float lum = powf(starLine.m_fAttenuation, attnPowScale*i);

				sampleWeights[i] = aaColor[starLine.m_NumPasses -1 - p][i] *
						lum * (p+1.0f)*0.5f;

				// Offset of sampling coordinate
				sampleOffsets[i].x = stepUV.x * i;
				sampleOffsets[i].y = stepUV.y * i;
				if(fabs(sampleOffsets[i].x) >= 0.9f ||
					fabs(sampleOffsets[i].y) >= 0.9f)
				{
					sampleOffsets[i].x = 0.0f;
					sampleOffsets[i].y = 0.0f;
					sampleWeights[i] *= 0.0f;
				}
			}
			
			m_pFX->SetTechnique("StarTech");
			m_pFX->SetValue("g_sSampleOffset", sampleOffsets, sizeof(sampleOffsets));
			m_pFX->SetVectorArray("g_sSampleWeight", sampleWeights, numSamples);

			GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, pDestSurface);
			GCI->GetDirectX()->GetDevice()->SetTexture(0, pSourceTex);	
			GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
			GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
			m_pFX->CommitChanges();

			UINT numPasses;
			m_pFX->Begin(&numPasses, 0);
			for(UINT pass = 0; pass < numPasses; pass++)
			{
				m_pFX->BeginPass(pass);

				// Draw fullscreen quad
				DSX->DrawFullScreenQuad(QuadCoord(0.0f, 0.0f, 1.0f, 1.0f));

				m_pFX->EndPass();
			}
			m_pFX->End();

			// Setup next expansion
			stepUV *= numSamples;
			attnPowScale *= numSamples;

			// Set the work draw just before the next texture
			pSourceTex = m_pStarTex[workTex];

			workTex += 1;
			if(workTex > 2)
				workTex = 1;
		}
	}

	pDestSurface = pStarSurfaces[0];
	
	for(int i = 0; i < starDef.m_NumStarLines; i++)
	{
		GCI->GetDirectX()->GetDevice()->SetTexture(i, m_pStarTex[i+4]);
		GCI->GetDirectX()->GetDevice()->SetSamplerState(i, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		GCI->GetDirectX()->GetDevice()->SetSamplerState(i, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);

		sampleWeights[i] = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f)*1.0f/(float)starDef.m_NumStarLines;
	}

	// Get the name of the technique, based on the star lines
	char strTechnique[256];
	sprintf_s(strTechnique, 256, "MergeTex_%dTech", starDef.m_NumStarLines);

	m_pFX->SetTechnique(strTechnique);
	m_pFX->SetVectorArray("g_sSampleWeight", sampleWeights, starDef.m_NumStarLines);
	
	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, pDestSurface);
	m_pFX->CommitChanges();

	UINT numPasses;
	m_pFX->Begin(&numPasses, 0);
	for(UINT pass = 0; pass < numPasses; pass++)
	{
		m_pFX->BeginPass(pass);

		// Draw fullscreen quad
		DSX->DrawFullScreenQuad(QuadCoord(0.0f, 0.0f, 1.0f, 1.0f));

		m_pFX->EndPass();
	}
	m_pFX->End();

	// Clean Up
	for(int i = 0; i < starDef.m_NumStarLines; i++)
		GCI->GetDirectX()->GetDevice()->SetTexture(i, NULL);

CleanUp:
	for(int i = 0; i < NUM_STAR_TEX; i++)
		SAFE_RELEASE(pStarSurfaces[i]);
}

void HDR_Light::RenderBloom()
{
	D3DXVECTOR2 vSampleOffsets[MAX_SAMPLES];
	float fSampleOffsets[MAX_SAMPLES];
	D3DXVECTOR4 vSampleWeights[MAX_SAMPLES];

	// Setup some surfaces
	IDirect3DSurface9* pBloomSurface;
	m_pBloomTex[0]->GetSurfaceLevel(0, &pBloomSurface);

	IDirect3DSurface9* pTempBloomSurface;
	m_pBloomTex[1]->GetSurfaceLevel(0, &pTempBloomSurface);

	IDirect3DSurface9* pBloomSourceSurface;
	m_pBloomTex[2]->GetSurfaceLevel(0, &pBloomSourceSurface);

	// Clear the bloom texture
	GCI->GetDirectX()->GetDevice()->ColorFill(pBloomSurface, NULL, D3DCOLOR_ARGB(0,0,0,0));

	if(m_GlareDef.m_fGlareLum <= 0.0f ||
		m_GlareDef.m_fBloomLum <= 0.0f)
		goto CleanReturn;

	RECT srcRect;
	GetTextureRect(&m_pBloomSurface.m_pTexture, &srcRect);
	InflateRect(&srcRect, -1, -1);

	RECT destRect;
	GetTextureRect(&m_pBloomTex[2], &destRect);
	InflateRect(&destRect, -1, -1);

	QuadCoord coord;
	GetTextureCoords(&m_pBloomSurface.m_pTexture, &srcRect, &m_pBloomTex[2], & destRect, &coord);


	// Pass 1
	D3DSURFACE_DESC desc;
	m_pBloomSurface.m_pTexture->GetLevelDesc(0, &desc);

	m_pFX->SetTechnique("GaussBlur5x5Tech");

	//GetSampleOffsets
	GetGaussOffsets(desc.Width, desc.Height, vSampleOffsets, vSampleWeights, 1.0f);
	m_pFX->SetValue("g_sSampleOffset", vSampleOffsets, sizeof(vSampleOffsets));
	m_pFX->SetValue("g_sSampleWeight", vSampleWeights, sizeof(vSampleWeights));

	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, pBloomSourceSurface);		// m_pBloomTex[2]
	GCI->GetDirectX()->GetDevice()->SetTexture(0, m_pBloomSurface.m_pTexture);
	GCI->GetDirectX()->GetDevice()->SetScissorRect(&destRect);
	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
	m_pFX->CommitChanges();

	UINT numPasses;
	m_pFX->Begin(&numPasses, 0);
	for(UINT pass = 0; pass < numPasses; pass++)
	{
		m_pFX->BeginPass(pass);

		// Draw fullscreen quad
		DSX->DrawFullScreenQuad(coord);

		m_pFX->EndPass();
	}
	m_pFX->End();
	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);


	// Pass 2
	m_pBloomTex[2]->GetLevelDesc(0, &desc);
	
	// Get Sample Offsets
	GetBloomOffsets(desc.Width, fSampleOffsets, vSampleWeights, 3.0f, 2.0f);
	for(int i = 0; i < MAX_SAMPLES; i++)
		vSampleOffsets[i] = D3DXVECTOR2(fSampleOffsets[i], 0.0f);

	m_pFX->SetTechnique("BloomTech");
	m_pFX->SetValue("g_sSampleOffset", vSampleOffsets, sizeof(vSampleOffsets));
	m_pFX->SetValue("g_sSampleWeight", vSampleWeights, sizeof(vSampleWeights));

	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, pTempBloomSurface);	// m_pBloomTex[1]
	GCI->GetDirectX()->GetDevice()->SetTexture(0, m_pBloomTex[2]);
	GCI->GetDirectX()->GetDevice()->SetScissorRect(&destRect);
	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
	m_pFX->CommitChanges();

	numPasses = 0;
	m_pFX->Begin(&numPasses, 0);
	for(UINT pass = 0; pass < numPasses; pass++)
	{
		m_pFX->BeginPass(pass);

		// Draw fullscreenquad
		DSX->DrawFullScreenQuad(coord);

		m_pFX->EndPass();
	}
	m_pFX->End();
	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);


	// Pass 3
	m_pBloomTex[1]->GetLevelDesc(0, &desc);

	// Get Sample Offsets
	GetBloomOffsets(desc.Height, fSampleOffsets, vSampleWeights, 3.0f, 2.0f);
	for(int i = 0; i < MAX_SAMPLES; i++)
		vSampleOffsets[i] = D3DXVECTOR2(0.0f, fSampleOffsets[i]);

	GetTextureRect(&m_pBloomTex[1], &srcRect);
	InflateRect(&srcRect, -1, -1);
	GetTextureCoords(&m_pBloomTex[1], &srcRect, &m_pBloomTex[0], 0, &coord);

	m_pFX->SetTechnique("BloomTech");
	m_pFX->SetValue("g_sSampleOffset", vSampleOffsets, sizeof(vSampleOffsets));
	m_pFX->SetValue("g_sSampleWeight", vSampleWeights, sizeof(vSampleWeights));

	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, pBloomSurface);	// m_pBloomTex[0]
	GCI->GetDirectX()->GetDevice()->SetTexture(0, m_pBloomTex[1]);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
	m_pFX->CommitChanges();

	numPasses = 0;
	m_pFX->Begin(&numPasses, 0);
	for(UINT pass = 0; pass < numPasses; pass++)
	{
		m_pFX->BeginPass(pass);

		// Draw a fullscreen quad
		DSX->DrawFullScreenQuad(coord);

		m_pFX->EndPass();
	}
	m_pFX->End();

CleanReturn:
	SAFE_RELEASE(pBloomSourceSurface);
	SAFE_RELEASE(pTempBloomSurface);
	SAFE_RELEASE(pBloomSurface);
}

void GetTextureRect(IDirect3DTexture9** pTexture, RECT* pRect)
{
	D3DSURFACE_DESC desc;
	(*pTexture)->GetLevelDesc(0, &desc);

	pRect->left		= 0;
	pRect->top		= 0;
	pRect->right	= desc.Width;
	pRect->bottom	= desc.Height;
}

void GetTextureCoords(IDirect3DTexture9** pSrcTex, RECT* pSrcRect,
				IDirect3DTexture9** pDestTex, RECT* pDestRect, QuadCoord* pCoords)
{
	D3DSURFACE_DESC desc;
	float tU, tV;	// Delta texel center offsets

	// Set up default mapping of the complete source surface
	pCoords->m_LeftU	= 0.0f;
	pCoords->m_TopV		= 0.0f;
	pCoords->m_RightU	= 1.0f;
	pCoords->m_BottomV	= 1.0f;

	// If not using the complete sorce surface, adjust coordinates
	if(pSrcRect != 0)
	{
		(*pSrcTex)->GetLevelDesc(0, &desc);

		// Get distance between sorce texel center in texture address space
		tU = 1.0f/desc.Width;
		tV = 1.0f/desc.Height;

		pCoords->m_LeftU	+= pSrcRect->left * tU;
		pCoords->m_TopV		+= pSrcRect->top * tV;
		pCoords->m_RightU	-= (desc.Width - pSrcRect->right) * tU;
		pCoords->m_BottomV	-= (desc.Height - pSrcRect->bottom) * tV;
	}

	// If not drawing to the complete destination surface, adjust coordinates
	if(pDestRect != NULL)
	{
		(*pDestTex)->GetLevelDesc(0, &desc);

		// Get distance between source texel center in address space
		tU = 1.0f/desc.Width;
		tV = 1.0f/desc.Height;

		pCoords->m_LeftU	-= pDestRect->left * tU;
		pCoords->m_TopV		-= pDestRect->top * tV;
		pCoords->m_RightU	+= (desc.Width - pDestRect->right) * tU;
		pCoords->m_BottomV	+= (desc.Height - pDestRect->bottom) * tV;
	}
}

void HDR_Light::SceneToScale()
{
	D3DXVECTOR2 sampleOffsets[MAX_SAMPLES];
	D3DSURFACE_DESC desc;

	// Get the Back Buffer desc
	//IDirect3DSurface9* backBuff;
	//GCI->GetDirectX()->GetDevice()->GetRenderTarget(0, &backBuff);
	//backBuff->GetDesc(&desc);
	//SAFE_RELEASE(backBuff);
	m_pHDRsurface.m_pTopSurface->GetDesc(&desc);

	// Create a 1/4 x 1/4 scale copy
	m_pFX->SetTechnique("DownSample4x4Tech");

	// Place the rectangle in the center of the back buffer surface
	RECT srcRect;
	srcRect.left	= (desc.Width - m_CropWidth)/2;
	srcRect.top		= (desc.Height - m_CropHeight)/2;
	srcRect.right	= srcRect.left + m_CropWidth;
	srcRect.bottom	= srcRect.top + m_CropHeight;

	// Get texture coordinates
	QuadCoord coords;
	GetTextureCoords(&m_pHDRsurface.m_pTexture, &srcRect, &m_pScaledSurface.m_pTexture, 0, &coords);

	// Get the sample offsets
	GetDown4x4Offsets(desc.Width, desc.Height, sampleOffsets);
	m_pFX->SetValue("g_sSampleOffset", sampleOffsets, sizeof(sampleOffsets));

	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, m_pScaledSurface.m_pTopSurface);
	GCI->GetDirectX()->GetDevice()->SetTexture(0, m_pHDRsurface.m_pTexture);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
	m_pFX->CommitChanges();

	UINT numPasses;
	m_pFX->Begin(&numPasses, 0);
	for(UINT pass = 0; pass < numPasses; pass++)
	{
		m_pFX->BeginPass(pass);

		// Draw a fullscreen quad
		DSX->DrawFullScreenQuad(coords);

		m_pFX->EndPass();
	}
	m_pFX->End();
}

void HDR_Light::ScaleToBright()
{
	D3DXVECTOR2 sampleOffsets[MAX_SAMPLES];
	D3DXVECTOR4	sampleWeights[MAX_SAMPLES];

	//D3DSURFACE_DESC desc;

	// Adjust for single pixel black border
	RECT srcRect;
	GetTextureRect(&m_pScaledSurface.m_pTexture, &srcRect);
	InflateRect(&srcRect, -1, -1);

	RECT destRect;
	GetTextureRect(&m_pBrightSurface.m_pTexture, &destRect);
	InflateRect(&destRect, -1, -1);

	// Get texture coordinates for the fullscreen quad
	QuadCoord coords;
	GetTextureCoords(&m_pScaledSurface.m_pTexture, &srcRect, &m_pBrightSurface.m_pTexture, &destRect, &coords);

	// Remove everything except lights and bright reflections
	m_pFX->SetTechnique("BrightPassTech");

	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, m_pBrightSurface.m_pTopSurface);
	GCI->GetDirectX()->GetDevice()->SetTexture(0, m_pScaledSurface.m_pTexture);
	GCI->GetDirectX()->GetDevice()->SetTexture(1, m_pAdaptedLumCurSurface.m_pTexture);
	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	GCI->GetDirectX()->GetDevice()->SetScissorRect(&destRect);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(1, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(1, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
	m_pFX->CommitChanges();

	UINT numPasses;
	m_pFX->Begin(&numPasses, 0);
	for(UINT pass = 0; pass < numPasses; pass++)
	{
		m_pFX->BeginPass(pass);

		// Draw a fullscreen quad
		DSX->DrawFullScreenQuad(coords);

		m_pFX->EndPass();
	}
	m_pFX->End();
	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
}

void HDR_Light::BrightToStar()
{
	D3DXVECTOR2	sampleOffsets[MAX_SAMPLES];
	D3DXVECTOR4	sampleWeights[MAX_SAMPLES];
	
	// Adjust for single pixel black border
	RECT destRect;
	GetTextureRect(&m_pStarSurface.m_pTexture, &destRect);
	InflateRect(&destRect, -1, -1);
	
	// Get texture coordinates for the fullscreen quad
	QuadCoord coords;
	GetTextureCoords(&m_pBrightSurface.m_pTexture, 0, &m_pStarSurface.m_pTexture, &destRect, &coords);

	GetGaussOffsets(m_pBrightSurface.m_iWidth, m_pBrightSurface.m_iHeight, sampleOffsets, sampleWeights);
	m_pFX->SetValue("g_sSampleOffset", sampleOffsets, sizeof(sampleOffsets));
	m_pFX->SetValue("g_sSampleWeight", sampleWeights, sizeof(sampleWeights));

	// Rough the edges to avoid aliasing effects on the star
	m_pFX->SetTechnique("GaussBlur5x5Tech");

	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, m_pStarSurface.m_pTopSurface);
	GCI->GetDirectX()->GetDevice()->SetTexture(0, m_pBrightSurface.m_pTexture);
	GCI->GetDirectX()->GetDevice()->SetScissorRect(&destRect);
	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
	m_pFX->CommitChanges();

	UINT numPasses;
	m_pFX->Begin(&numPasses, 0);
	for(UINT pass = 0; pass < numPasses; pass++)
	{
		m_pFX->BeginPass(pass);

		// Draw a fullscreen quad
		DSX->DrawFullScreenQuad(coords);

		m_pFX->EndPass();
	}
	m_pFX->End();
	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
}

void HDR_Light::StarToBloom()
{
	D3DXVECTOR2	sampleOffsets[MAX_SAMPLES];

	// Adjust for single pixel black border
	RECT srcRect;
	GetTextureRect(&m_pStarSurface.m_pTexture, &srcRect);
	InflateRect(&srcRect, -1, -1);
	
	// Adjust for single pixel black border
	RECT destRect;
	GetTextureRect(&m_pBloomSurface.m_pTexture, &destRect);
	InflateRect(&destRect, -1, -1);

	// Get texture coordinates for the fullscreen quad
	QuadCoord coords;
	GetTextureCoords(&m_pStarSurface.m_pTexture, &srcRect, &m_pBloomSurface.m_pTexture, &destRect, &coords);

	GetDown2x2Offsets(m_pBrightSurface.m_iWidth, m_pBrightSurface.m_iHeight, sampleOffsets);
	m_pFX->SetValue("g_sSampleOffset", sampleOffsets, sizeof(sampleOffsets));

	// Create a 1/2 x 1/2 copy 
	m_pFX->SetTechnique("DownSample2x2Tech");

	GCI->GetDirectX()->GetDevice()->SetRenderTarget(0, m_pBloomSurface.m_pTopSurface);
	GCI->GetDirectX()->GetDevice()->SetTexture(0, m_pStarSurface.m_pTexture);
	GCI->GetDirectX()->GetDevice()->SetScissorRect(&destRect);
	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	GCI->GetDirectX()->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
	m_pFX->CommitChanges();

	UINT numPasses;
	m_pFX->Begin(&numPasses, 0);
	for(UINT pass = 0; pass < numPasses; pass++)
	{
		m_pFX->BeginPass(pass);

		// Draw a fullscreen quad
		DSX->DrawFullScreenQuad(coords);

		m_pFX->EndPass();
	}
	m_pFX->End();
	GCI->GetDirectX()->GetDevice()->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
}


void HDR_Light::GetDown4x4Offsets(float width, float height, D3DXVECTOR2 sampleOffsets[])
{
	float tU = 1.0f/width;
	float tV = 1.0f/height;

	// Sample from the center of 16 surrounding points
	int index = 0;
	for(int y = 0; y < 4; y++)
		for(int x = 0; x < 4; x++)
		{
			sampleOffsets[index].x = (x - 1.5f) * tU;
			sampleOffsets[index].y = (y - 1.5f) * tV;

			index++;
		}
}

void HDR_Light::GetDown2x2Offsets(float width, float height, D3DXVECTOR2 sampleOffsets[])
{
	float tU = 1.0f/width;
	float tV = 1.0f/height;

	// Sample from the center of 4 surronding points
	int index = 0;
	for(int y = 0; y < 2; y++)
		for(int x = 0; x < 2; x++)
		{
			sampleOffsets[index].x = (x - 0.5f) * tU;
			sampleOffsets[index].y = (y - 0.5f) * tV;

			index++;
		}
}

void HDR_Light::GetGaussOffsets(float width, float height, D3DXVECTOR2* pTexCoordOffset, 
							D3DXVECTOR4* pSampleWeight, float multi)
{
	float tU = 1.0f/width;
	float tV = 1.0f/height;

	float totalWeight = 0.0f;
	int index = 0;
	for(int x = -2; x <= 2; x++)
		for(int y = -2; y <= 2; y++)
		{
			// Exclude pixels with a block distance greater than 2 (create 13 sample, 5x5 kernal)
			if(abs(x) + abs(y) > 2)
				continue;

			// Get the unscaled gaussian intensity for this offset
			pTexCoordOffset[index] = D3DXVECTOR2(x*tU, y*tV);
			pSampleWeight[index] = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f) * GaussDistribution((float)x, (float)y, 1.0f);
			totalWeight += pSampleWeight[index].x;

			index++;
		}

	// Make sure the intensity of the blur won't change when the blur occurs
	for(int i = 0; i < index; i++)
	{
		// Divide current weight by the total of all the weights
		pSampleWeight[i] /= totalWeight;
		pSampleWeight[i] *= multi;
	}
}

void HDR_Light::GetBloomOffsets(float texSize, float texCoordOffset[15], D3DXVECTOR4* colorWeight,
							float deviation, float multi)
{
	float tU = 1.0f/texSize;

	// Fill the center texel
	float weight = multi * GaussDistribution(0, 0, deviation);
	colorWeight[0] = D3DXVECTOR4(weight, weight, weight, 1.0f);

	texCoordOffset[0] = 0.0f;

	// Fill the first half
	for(int i = 1; i < 8; i++)
	{
		// Get the gaussian intensity for this offset
		weight = multi * GaussDistribution((float)i, 0, deviation);
		texCoordOffset[i] = i * tU;

		colorWeight[i] = D3DXVECTOR4(weight, weight, weight, 1.0f);
	}

	// Mirror to the second half
	for(int i = 8; i < 15; i++)
	{
		colorWeight[i] = colorWeight[i-7];
		texCoordOffset[i] = -texCoordOffset[i-7];
	}
}

void HDR_Light::GetStarOffsets(float texSize, float texCoordOffset[15], D3DXVECTOR4* colorWeight,
							float deviation)
{
	float tU = 1.0f/texSize;

	// Fill the center texel
	float weight = 1.0f * GaussDistribution(0, 0, deviation);
	colorWeight[0] = D3DXVECTOR4(weight, weight, weight, 1.0f);

	texCoordOffset[0] = 0.0f;

	// Fill the first half
	for(int i = 1; i < 8; i++)
	{
		// Get the guassian intensity for this offset
		weight = 1.0f * GaussDistribution((float)i, 0, deviation);
		texCoordOffset[i] = i *tU;

		colorWeight[i] = D3DXVECTOR4(weight, weight, weight, 1.0f);
	}

	// Mirror to the second half
	for(int i = 8; i < 15; i++)
	{
		colorWeight[i] = colorWeight[i-7];
		texCoordOffset[i] = -texCoordOffset[i-7];
	}
}

float HDR_Light::GaussDistribution(float x, float y, float rho)
{
	float gauss = 1.0f/sqrtf(2.0f*D3DX_PI*rho*rho);
	gauss *= expf(-(x*x+y*y)/(2*rho*rho));
	return gauss;
}

int HDR_Light::AddLight(D3DXVECTOR4 pos, D3DXVECTOR4 intensity, int power)
{
	int id;
	if(!m_iAvailableIDs.empty())
	{
		// Recycle a light
		id = m_iAvailableIDs.front();
		m_iAvailableIDs.pop();
	}
	else
	{
		// Create new light
		id = m_DynamicLights.size();
		m_DynamicLights.push_back(DynamicLight());
		m_DynamicLights[id].m_iID = id;
	}
	// Set the light paramaters
	m_DynamicLights[id].m_bUse = true;
	m_DynamicLights[id].m_vPos = pos;
	m_DynamicLights[id].m_vIntensity = intensity;
	m_DynamicLights[id].m_iLogIntensity = (-4 + power / 9);
	m_DynamicLights[id].m_iMantissa = (1 + power % 9);

	// Set up the lights in the arrays
	m_LightPos.push_back(pos);
	m_LightPosView.push_back(pos);
	m_LightMantissa.push_back(1 + power % 9);
	m_LightLogIntensity.push_back(-4 + power / 9);
	m_LightIntensity.push_back(intensity);

	// Update the lights
	AdjustLight(id, true);
	return id;
}

void HDR_Light::AdjustLight(int light, bool increment)
{
	if(light >= m_LightPos.size())
		return;				// Invalid light ID

	if(increment && m_LightLogIntensity[light] < 7)
	{
		m_LightMantissa[light]++;
		if(m_LightMantissa[light] > 9)
		{
			m_LightMantissa[light] = 1;
			m_LightLogIntensity[light]++;
		}
	}

	if(!increment && m_LightLogIntensity[light] > -4)
	{
		m_LightMantissa[light]--;
		if(m_LightMantissa[light] < 1)
		{
			m_LightMantissa[light] = 9;
			m_LightLogIntensity[light]--;
		}
	}

	RefreshLights();
}

// Set the light intensity to match current log luminance
void HDR_Light::RefreshLights()
{
	char buffer[256] = {0};

	for(int i = 0; i < m_LightPos.size(); i++)
	{
		m_LightIntensity[i].x = m_LightMantissa[i] * (float)pow(10.0f, m_LightLogIntensity[i]);
		m_LightIntensity[i].y = m_LightMantissa[i] * (float)pow(10.0f, m_LightLogIntensity[i]);
		m_LightIntensity[i].z = m_LightMantissa[i] * (float)pow(10.0f, m_LightLogIntensity[i]);
		m_LightIntensity[i].w = 1.0f;

		// Can use for output
		sprintf_s(buffer, 255, "Light %d.0e%d", i, m_LightMantissa[i], m_LightLogIntensity[i]);
	}
}
void HDR_Light::ChangeLightPos(int light, D3DXVECTOR4 pos)
{
	if(m_DynamicLights[light].m_vPos == pos)
		return;
	m_DynamicLights[light].m_vPos = pos;

#if WAIT_FOR_REBUILD
	m_bNeedToRebuildLightPos = 1;
#else
	// Rebuild light position array
	m_LightPos.clear();
	for(auto i = m_DynamicLights.begin(); i != m_DynamicLights.end(); i++)
		if((*i).m_bUse)
			m_LightPos.push_back((*i).m_vPos);
#endif
}
void HDR_Light::ChangeLightPower(int light, int power)
{
	m_DynamicLights[light].m_iLogIntensity = (-4 + power / 9);
	m_DynamicLights[light].m_iMantissa = (1 + power % 9);
	
#if WAIT_FOR_REBUILD
	m_bNeedToRebuildLights = 1;
#else
	RebuildShaderArrays();
#endif
}
void HDR_Light::RebuildShaderArrays()
{
	// Rebuild Array light information
	m_LightPos.clear();
	m_LightPosView.clear();
	m_LightIntensity.clear();
	m_LightLogIntensity.clear();
	m_LightMantissa.clear();
	for(auto i = m_DynamicLights.begin(); i != m_DynamicLights.end(); i++)
		if((*i).m_bUse)
		{
			m_LightPos.push_back((*i).m_vPos);
			m_LightIntensity.push_back((*i).m_vIntensity);
			m_LightLogIntensity.push_back((*i).m_iLogIntensity);
			m_LightMantissa.push_back((*i).m_iMantissa);
			AdjustLight(m_LightPos.size()-1, false);
		}
	m_LightPosView.resize( m_LightPos.size() );
#if WAIT_FOR_REBUILD
	m_bNeedToRebuildLights = 0;
	m_bNeedToRebuildLightPos = 0;
#endif
}
void HDR_Light::RemoveLight(int light)
{
	if(light >= m_DynamicLights.size() || !m_DynamicLights[light].m_bUse)
		return;

	// Add light to Recyling bin
	m_DynamicLights[light].m_bUse = false;
	m_iAvailableIDs.push(light);
	
#if WAIT_FOR_REBUILD
	m_bNeedToRebuildLights = 1;
#else
	RebuildShaderArrays();
#endif
}
void HDR_Light::RemoveAllLights()
{
	m_LightPos.clear();
	m_LightPosView.clear();
	m_LightIntensity.clear();
	m_LightLogIntensity.clear();
	m_LightMantissa.clear();
	m_DynamicLights.clear();
	while( !m_iAvailableIDs.empty() )
		m_iAvailableIDs.pop();
#if WAIT_FOR_REBUILD
	m_bNeedToRebuildLights = 0;
	m_bNeedToRebuildLightPos = 0;
#endif
}

void HDR_Light::SwitchToneMap()
{
	m_bUseToneMap = (!m_bUseToneMap);	// Flips tonemap on/off
}
void HDR_Light::SetGlareType(EGlareType glareType)
{
	m_GlareDef =  LensGlare::GetGlareLib(glareType);
}

void SaveToFile(IDirect3DTexture9* m_pTexture, const char* name)
{
	if(m_pTexture != 0)	// Make sure theirs a texture
	{
		HRESULT hr;
		std::string fileName = std::string(name);
		fileName += ".bmp";	// Setup the picture file name
		hr = D3DXSaveTextureToFileA(fileName.c_str(), D3DXIFF_BMP, m_pTexture, NULL);	// Save the texture to a bmp file
	}
}