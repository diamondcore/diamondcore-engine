#ifndef RENDERSURFACE_H
#define RENDERSURFACE_H

#include "Light.h"

class RenderSurface
{
	ID3DXRenderToSurface*	m_RSurface;			// Render to surface
	D3DXMATRIX				m_TexRenderProj;	// The texture Render Projection View
	D3DXMATRIX				m_TexRenderView;	// The texture Render View
	float					m_CamRotation;		// Give Rotation effect of Texture Render View
	
	int						m_iNumMips;			// Number of Mip map levels

	bool					m_bDepthStencil,	// Identifies to use Depth Stencil or not
							m_bAutoGenMipMaps;	// If hardware supports autoGen mipMaps, then true to use it
	D3DFORMAT				m_TextureFormat,	// Texture format
							m_DepthSFormat;
	D3DVIEWPORT9			m_ViewPort;
	
public:
	IDirect3DSurface9*		m_pTopSurface;		// Top level surface (zero)
	IDirect3DTexture9*		m_pTexture;			// This texture holds the RenderToSurface 	
	int						m_iWidth,			// Width in pixels
							m_iHeight;			// Height in pixels
	
	// Render Camera View
	D3DXVECTOR3			m_vTexViewPos;		// Position of the render camera
	D3DXVECTOR3			m_vTexTarget;		// Look At view of the texture camera
	D3DXVECTOR3			m_vTexUp;
	D3DXVECTOR3			m_vTexRight;

public:
	RenderSurface();
	~RenderSurface();
	void	PreRestartDevice();				// Actions to perform before device restart
	void	PostRestartDevice();			// Actions to perform after device reboot

	void	setDepthStencil(D3DFORMAT format, bool use);	// Set Depth Stencil settings
	void	setTexture(D3DFORMAT format, bool autoGenMips, int numMips = 1);	// Set Texture settings

	void	Init();							// Creates Surface and Texture
	void	Update();		// Pass in what to draw onto the texture's surface
	void	DumpSurface(int posX, int posY, int width, int height);
	void	SaveToFile(const char* name, D3DXIMAGE_FILEFORMAT format = D3DXIFF_BMP);
	void	GetSurface(int level, IDirect3DSurface9** pSurface);
	
	// Alternative to Update()
	void	RenderBegin();					// Call renders to be Drawn to the Texture, after this			
	void	RenderEnd();					// This will end the Draw to Texture
};
#endif	//RENDERSURFACE_H