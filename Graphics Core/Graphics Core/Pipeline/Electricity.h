#ifndef __ELECTRICITY_H
#define __ELECTRICITY_H

#include <d3dx9.h>

#include <map>
#include <vector>

#include "../SceneGraph/GraphicsManager.h"

#define FVF_BOLT (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX0)
#define USEDEFAULTBOLT 1

struct CUSTOMVERTEX_BOLT{D3DXVECTOR3 position;  D3DXVECTOR3 lerpData; D3DXVECTOR2 texC;};

class ElectricBolt
{
protected:

	int m_iID;
	int m_iNumSegments;
	UINT m_iNumVerts;
	UINT m_iNumIndeces;
	UINT m_iPrimCount;
	UINT m_iDistortOffset;

	LPDIRECT3DVERTEXBUFFER9 m_vBuffer;    // the pointer to the vertex buffer
	LPDIRECT3DINDEXBUFFER9 m_iBuffer;    // the pointer to the index buffer

	friend class BoltMgr;

public:
	ElectricBolt();
	~ElectricBolt();
	bool createBolt(int ID, int numSegments);
	void clean();
	void PreRestartDevice(){clean();}
	void PostRestartDevice(){createBolt(m_iID, m_iNumSegments);}
};

class BoltMgr
{
protected:
	std::map<int, std::vector<std::shared_ptr<ElectricBolt>>> m_mvBoltGroupMap;
	std::map<int, std::shared_ptr<ElectricBolt>> m_mBoltMap;
#if USEDEFAULTBOLT
	ElectricBolt m_mDefaultBolt;
	bool m_bDefaultBuilt;
#endif
	int m_iBoltID;

	//Scratch Vars
	D3DXMATRIX world;
		D3DXVECTOR3 look,
					normal,
					right,
					up;

public:
	BoltMgr();
	~BoltMgr(){Clean();}

	bool Init();

	bool AddBolt(int &ID, int groupID, int numSegments, std::vector<int> *additionalGroups = 0);
	bool RemoveBolt(int ID);
	void RemoveGroup(int groupID);

	bool RenderBolt(int ID, Vector3 startPos, Vector3 endPos, Vector3 camUp,
		float boltScale, float ArcScale, int randOffset, float midpoint, Vector4 color);
	bool RenderRandomBolt(int groupID, Vector3 startPos, Vector3 endPos, Vector3 camUp,
		float boltScale, float ArcScale, int randOffset, float midpoint, Vector4 color);

	UINT getGroupSize(int groupID){return m_mvBoltGroupMap[groupID].size();}
	UINT getNumOfBolts(){return m_mBoltMap.size();}

	void Clean();

	void PreRestartDevice();
	void PostRestartDevice();
};

class ElectricityData
{
protected:
	Vector3 m_vStartPos,
			m_vEndPos;
	Vector4 m_vColor;
	float m_fArcScale;
	float m_fBoltScale;
	float m_fMidPoint;
	int m_iOffset;
	int m_iBoltID;
	int m_iID;
	UINT m_iSelectionType;
	bool m_bIsVisible;

public:
	ElectricityData(int ID, float boltSize, float ArcAmount, float ArcMidpoint, int offset);
	~ElectricityData(){}

	void SetStartEnd(Vector3 start, Vector3 end);
	void SetArcScale(float scale){m_fArcScale = scale;}
	void SetBoltScale(float scale){m_fBoltScale = scale;}
	void SetMidpoint(float midpoint){m_fMidPoint = midpoint;}
	void SetOffset(int offset){m_iOffset = offset;}
	void SetColor(Vector4 color){m_vColor.x = color.x;
								m_vColor.y = color.y;
								m_vColor.z = color.z;
								m_vColor.w = color.w;}
	void SetSelectionID(int selectionID);
	void SetBoltChoiceType(UINT i);
	void SetVisible(bool b);
	void Render(BoltMgr *bolts, Vector3 camUp);

	int GetID(){return m_iID;}
};

#endif