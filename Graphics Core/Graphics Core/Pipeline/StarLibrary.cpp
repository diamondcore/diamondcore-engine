#include "StarLibrary.h"
#include "../MacroTools.h"

// Static star library information
static StarDef s_StarLibDef[NUM_BASESTAR_TYPES] = 
{
	// Star Name		lines	passes	length	attn	rotate					bRotate
	{ TEXT("Disable"),		0,		0,	0.0f,	0.0f,	D3DXToRadian(00.0f),	false, },	// STAR_DISABLE

	{ TEXT("Cross"),		4,		3,	1.0f,	0.85f,	D3DXToRadian(0.0f),		true, },	// STAR_CROSS
	{ TEXT("CrossFilter"),	4,		3,	1.0f,	0.95f,	D3DXToRadian(0.0f),		true, },	// STAR_CROSSFILTER
	{ TEXT("SnowCross"),	6,		3,	1.0f,	0.96f,	D3DXToRadian(20.0f),	true, },	// STAR_SNOWCROSS
	{ TEXT("Vertical"),		2,		3,	1.0f,	0.96f,	D3DXToRadian(00.0f),	false, },	// STAR_VERTICAL
};
static int		s_NumStarLibDefs = sizeof(s_StarLibDef)/sizeof(StarDef);


/********************************************
				Star Library
*********************************************/
StarLibrary*	StarLibrary::s_StarLibrary = 0;
D3DXCOLOR		StarLibrary::s_ChromAberColor[];
StarLibrary::StarLibrary()
{
	Create();
}
StarLibrary::StarLibrary(const StarLibrary& copy)
{
	Create();
	Init(copy);
}
StarLibrary::~StarLibrary()
{
	Destroy();
}
StarLibrary& StarLibrary::operator= (const StarLibrary& rhs)
{
	Init(rhs);
	return *this;
}

void StarLibrary::Create()
{
	ZeroMemory(m_StarName, sizeof(m_StarName));

	m_NumStarLines = 0;
	m_pStarLines = 0;
	m_fInclination = 0.0f;

	m_bRotation = false;
}
void StarLibrary::Destroy()
{
	Release();
}
void StarLibrary::Release()
{
	SAFE_DELETE_ARRAY(m_pStarLines);
	m_NumStarLines = 0;
}

void StarLibrary::Init(EStarType type)
{
	Init(s_StarLibrary[type]);
}
void StarLibrary::Init(const StarLibrary& add)
{
	if(&add == this)
		return;

	Release();

	// Copy the data from sorce
	strcpy_s(m_StarName, (char*)add.m_StarName);
	m_NumStarLines = add.m_NumStarLines;
	m_fInclination = add.m_fInclination;
	m_bRotation = add.m_bRotation;

	// Copy over each star lines
	m_pStarLines = DEBUG_NEW StarLine[m_NumStarLines];
	for(int i = 0; i < m_NumStarLines; i++)
		m_pStarLines[i] = add.m_pStarLines[i];
}

void StarLibrary::Init(const TCHAR* starName, int numStarLines, int numPasses,
					float sampleLength, float attenuation, float inclination, bool rotation)
{
	Release();

	// Copy the pramaters into this
	strcpy_s(m_StarName, starName);
	m_NumStarLines	= numStarLines;
	m_fInclination	= inclination;
	m_bRotation = rotation;

	// Create the Star lines
	m_pStarLines = DEBUG_NEW StarLine[m_NumStarLines];
	float fInc = D3DXToRadian(360.0f/(float)m_NumStarLines);
	for(int i = 0; i < m_NumStarLines; i++)
	{
		m_pStarLines[i].m_NumPasses = numPasses;
		m_pStarLines[i].m_fSampleLength = sampleLength;
		m_pStarLines[i].m_fAttenuation = attenuation;
		m_pStarLines[i].m_fInclination = fInc * (float)i;
	}
}

void StarLibrary::Init(const StarDef& add)
{
	Init(add.m_StarName,
		 add.m_NumStarLines,
		 add.m_NumPasses,
		 add.m_fSampleLen,
		 add.m_fAttenuation,
		 add.m_fInclination,
		 add.m_bRotation);
}

void StarLibrary::Init_SunnyCross(const TCHAR* starName, float sampleLength,
					float attenuation, float longAttenuation, float inclination)
{
	Release();

	// Copy the input parameters into this
	strcpy_s(m_StarName, starName);
	m_NumStarLines = 8;
	m_fInclination = inclination;
	m_bRotation = false;

	// Create the Star Lines
	m_pStarLines = DEBUG_NEW StarLine[m_NumStarLines];
	float fInc = D3DXToRadian(360.0f/(float)m_NumStarLines);
	for(int i = 0; i < m_NumStarLines; i++)
	{
		m_pStarLines[i].m_fSampleLength = sampleLength;
		m_pStarLines[i].m_fInclination = fInc*(float)i + D3DXToRadian(0.0f);

		if(0 == (i % 2))
		{
			m_pStarLines[i].m_NumPasses = 3;
			m_pStarLines[i].m_fAttenuation = longAttenuation;
		}
		else
		{
			m_pStarLines[i].m_NumPasses = 3;
			m_pStarLines[i].m_fAttenuation = attenuation;
		}
	}
}

// Static Star Library Methods
void StarLibrary::InitStaticLibrary()
{
	if(s_StarLibrary)	// Check if library already exist
		return;

	s_StarLibrary = DEBUG_NEW StarLibrary[NUM_STAR_TYPES];

	// Create the basic star types
	for(int i = 0; i < NUM_BASESTAR_TYPES; i++)
		s_StarLibrary[i].Init(s_StarLibDef[i]);

	// Create the special star types
	s_StarLibrary[STAR_SUNNYCROSS].Init_SunnyCross();

	D3DXCOLOR color[8] = 
	{
		D3DXCOLOR(0.5f, 0.5f, 0.5f, 0.0f),	// w
		D3DXCOLOR(0.8f, 0.3f, 0.3f, 0.0f),
		D3DXCOLOR(1.0f, 0.2f, 0.2f, 0.0f),	// r
		D3DXCOLOR(0.5f, 0.2f, 0.6f, 0.0f),
		D3DXCOLOR(0.2f, 0.2f, 1.0f, 0.0f),	// b
		D3DXCOLOR(0.2f, 0.3f, 0.7f, 0.0f),
		D3DXCOLOR(0.2f, 0.6f, 0.2f, 0.0f),	// g
		D3DXCOLOR(0.3f, 0.5f, 0.3f, 0.0f),
	};
	memcpy(s_ChromAberColor, color, sizeof(D3DXCOLOR)*8);
}
void StarLibrary::DeleteStaticLibrary()
{
	// Delete all Libraryies
	SAFE_DELETE_ARRAY(s_StarLibrary);
}

const StarLibrary& StarLibrary::GetStarDef(EStarType type)
{
	return s_StarLibrary[type];
}
const D3DXCOLOR& StarLibrary::GetChromAberColor(int id)
{
	return s_ChromAberColor[id];
}