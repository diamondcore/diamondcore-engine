#ifndef __ElectricBolts_H
#define __ElectricBolts_H

#include <list>
#include <vector>
#include <memory>

#include "Electricity.h"



class ElectricityManager
{
protected:
	BoltMgr m_bBolts;
	std::list<ElectricityData> m_lBoltObject;
	std::list<ElectricityData>::iterator m_BoltObjIterator;
	UINT m_iObjID;

public:
	ElectricityManager();
	~ElectricityManager(){Clean();}

	void Init();

	bool AddBolt(int &ID, int groupID, int numSegments, std::vector<int> *additionalGroups = 0);
	bool RemoveBolt(int ID);
	void RemoveBoltGroup(int groupID);

	void AddBoltObject(int &ID, Vector3 startPos, Vector3 endPos, float boltSize, float ArcAmount, float ArcMidpoint,
		UINT selectionType, int selectionID, Vector4 color);
	bool RemoveBoltObject(int ID);

	bool SetBoltObjPositions(int ID, Vector3 startPos, Vector3 endPos);
	bool SetBoltObjArcScale(int ID, float scale);
	bool SetBoltObjThickness(int ID, float thickness);
	bool SetBoltObjMidpoint(int ID, float midpoint);
	bool SetBoltObjRandOffset(int ID, int offset);
	bool SetBoltObjSelectionType(int ID, UINT selectionType);
	bool SetBoltObjSelectionID(int ID, int selectionID);
	bool SetBoltObjVisibility(int ID, bool visible);
	bool SetBoltObjColor(int ID, D3DXVECTOR4 color);

	void RenderAll();

	void Clean();

	void PreRestartDevice(){m_bBolts.PreRestartDevice();}
	void PostRestartDevice(){m_bBolts.PostRestartDevice();}
};

#endif