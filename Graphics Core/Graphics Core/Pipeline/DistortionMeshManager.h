#pragma once

#include "../SceneGraph/ObjectList.h"

class DistMeshList
{
protected:
	std::string						m_MeshName;
	std::map<int, BaseObjectPtr>	m_Objects;
	std::vector<BaseObjectPtr>		m_ObjectVec;
	D3DXMATRIX m_OffsetMat;
	//D3DXVECTOR2 m_Offset1,
	//			m_Offset2,
	//			m_OffsetSpeed1,
	//			m_OffsetSpeed2;
	//float m_DistortScale,
	//		m_BumpSize,
	//		m_Intensity;
	
	//std::map<int, DistortObjInfo>	m_ObjDistInfo;

	void	AddObject(int sharedMemID);

public:
	DistMeshList(std::string meshName);
	~DistMeshList();
	void Clear();

	void	Update(Module *sharedMem);
	void	Render();

	void	CreateObject(int sharedMemID);
	void	RemoveObject(int sharedMemID);

	BaseObjectPtr	GetObject(int  id);

	friend class DistMeshManager;
};

class DistMeshManager
{
	struct distortData{
		std::map<std::string, std::shared_ptr<DistMeshList>> m_Meshes;
		D3DXVECTOR2 m_Offset1,
				m_Offset2,
				m_OffsetSpeed1,
				m_OffsetSpeed2;
		float m_DistortScale1,
				m_DistortScale2,
				m_BumpSize1,
				m_BumpSize2,
				m_Intensity1,
				m_Intensity2;
	};
	std::map<int, distortData>	m_DistortMeshLists;

public:
	DistMeshManager();
	~DistMeshManager();
	void	ClearScene();

	void	Update(float dt, Module* sharedMem);

	void	Render();

	void	CreateMesh(int sharedID, std::string meshName, int groupID);

	void	RemoveMesh(std::string meshName, int sharedID);

	void	SetDistortOffset(int groupID, D3DXVECTOR2 *offset1, D3DXVECTOR2 *offset2 = NULL);
	void	SetDistortSpeed(int groupID, D3DXVECTOR2 *speed1, D3DXVECTOR2 *speed2 = NULL);
	void	SetDistortBump(int groupID, float bump1, float bump2);
	void	SetDistortScale(int groupID, float scale1, float scale2);
	void	SetDistortIntensity(int groupID, float intensity1, float intensity2);
	
	BaseObjectPtr GetObject(int sharedID, std::string meshName, int groupID);
};