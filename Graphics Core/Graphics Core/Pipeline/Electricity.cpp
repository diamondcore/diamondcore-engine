#include "Electricity.h"

#include <vector>
#include <stdlib.h>

#include "../GraphicsCore.h"
#include "../MacroTools.h"

#define MAXBOLTVERTINDEXCOUNT 200


ElectricBolt::ElectricBolt()
{
	m_iNumSegments = 2;
	//pVerts = NULL;
	//pIndeces = NULL;
	m_vBuffer = 0;
	m_iBuffer = 0;

	//m_iPrevDistortOffset = 1;
	m_iDistortOffset = 1;
}

ElectricBolt::~ElectricBolt()
{
	clean();
}

bool ElectricBolt::createBolt(int ID, int numSegments)
{
	if(numSegments <= 0) return false;

	float distShift = 1.0f/(numSegments);
	D3DXVECTOR3 shift;
	D3DXVec3Scale(&shift, &D3DXVECTOR3(1, 0, 0), distShift);

	m_iNumVerts = (numSegments * 4) + 4;
	m_iNumIndeces = 6 + (numSegments * 6);
	m_iPrimCount = 2 * numSegments;
	m_iNumSegments = numSegments;
	//m_fMidPoint = midpoint;
	m_iID = ID;
	//numSegments += 2;

	if(m_iNumVerts > MAXBOLTVERTINDEXCOUNT || m_iNumIndeces > MAXBOLTVERTINDEXCOUNT) return false;

	D3DXVECTOR3 currentPos(0, 0, 0);

	D3DXVECTOR3 boltPos[2];
#if 0
	boltPos[0] = D3DXVECTOR3(0.0f, boltSize, 0.0f);
	boltPos[1] = D3DXVECTOR3(0.0f, -boltSize, 0.0f);
	//boltPos[2] = D3DXVECTOR3(1.0f, 0.0f, boltSize);
	//boltPos[3] = D3DXVECTOR3(1.0f, 0.0f, -boltSize);
#else
	boltPos[0] = D3DXVECTOR3(0.0f, 0, 0.0f);
	boltPos[1] = D3DXVECTOR3(0.0f, 0, 0.0f);
	//boltPos[2] = D3DXVECTOR3(1.0f, 0.0f, boltSize);
	//boltPos[3] = D3DXVECTOR3(1.0f, 0.0f, -boltSize);
#endif

	D3DXVECTOR2 boltTex[4];
	boltTex[0] = D3DXVECTOR2(0.0, 0.0);
	boltTex[1] = D3DXVECTOR2(0.0, 1.0);
	boltTex[2] = D3DXVECTOR2(1.0, 0.0);
	boltTex[3] = D3DXVECTOR2(1.0, 1.0);

	int i;
	float s = 0.0f;

	std::vector<CUSTOMVERTEX_BOLT> bolt;
	CUSTOMVERTEX_BOLT temp;

	D3DXVECTOR3 vShift(0, 0, 0);
#if 0
	for(i = 0; i < numSegments; i++)
	{
		temp.position = boltPos[0] + vShift;
		temp.texC = boltTex[0];
		//temp.lerpPos = vShift;
		temp.lerpData = vShift;
		bolt.push_back(temp);
		temp.position = boltPos[1] + vShift;
		temp.texC = boltTex[1];
		//temp.lerpPos = vShift;
		temp.lerpData = vShift;
		bolt.push_back(temp);
		vShift += shift;
		s+= distShift;
		temp.position = boltPos[0] + vShift;
		temp.texC = boltTex[2];
		//temp.lerpPos = vShift;
		temp.lerpData = vShift;
		bolt.push_back(temp);
		temp.position = boltPos[1] + vShift;
		temp.texC = boltTex[3];
		//temp.lerpPos = vShift;
		temp.lerpData = vShift;
		bolt.push_back(temp);
		//s+= distShift;
		//vShift += shift;
	}
#else
	for(i = 0; i < numSegments; i++)
	{
		temp.position = boltPos[0];
		temp.texC = boltTex[0];
		//temp.lerpPos = vShift;
		temp.lerpData = D3DXVECTOR3(s, 1, 1);
		bolt.push_back(temp);
		temp.position = boltPos[1];
		temp.texC = boltTex[1];
		//temp.lerpPos = vShift;
		temp.lerpData = D3DXVECTOR3(s, -1, 1);
		bolt.push_back(temp);
		vShift += shift;
		s+= distShift;
		temp.position = boltPos[0];
		temp.texC = boltTex[2];
		//temp.lerpPos = vShift;
		temp.lerpData = D3DXVECTOR3(s, 1, 1);
		bolt.push_back(temp);
		temp.position = boltPos[1];
		temp.texC = boltTex[3];
		//temp.lerpPos = vShift;
		temp.lerpData = D3DXVECTOR3(s, -1, 1);
		bolt.push_back(temp);
		//s+= distShift;
		//vShift += shift;
	}
	bolt[0].lerpData.z = 0;
	bolt[1].lerpData.z = 0;
	bolt[bolt.size() - 1].lerpData.z = 0;
	bolt[bolt.size() - 2].lerpData.z = 0;

#endif

	CUSTOMVERTEX_BOLT verts[MAXBOLTVERTINDEXCOUNT];
	int size = bolt.size();

	for(i = 0; i < size; i++)
	{
		verts[i] = bolt[i];
	}

	D3DVERTEXELEMENT9 vertices_Bolt[] = {
		{0,	0,	D3DDECLTYPE_FLOAT4,		D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION,  0},
		{0,	16,	D3DDECLTYPE_FLOAT3,		D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,	0},
		{0,	28,	D3DDECLTYPE_FLOAT2,     D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,   0},
		D3DDECL_END()
	};

	IDirect3DVertexDeclaration9* vertexDecleration = 0;
	GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(vertices_Bolt, &vertexDecleration);

	GCI->GetDirectX()->GetDevice()->CreateVertexBuffer(m_iNumVerts * sizeof(CUSTOMVERTEX_BOLT),
								0,
								FVF_BOLT,
								D3DPOOL_DEFAULT,
								&m_vBuffer,
								NULL);

	VOID* pVoid;

	// lock m_vBuffer and load the vertices into it
	m_vBuffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, verts, (sizeof(verts) / MAXBOLTVERTINDEXCOUNT) * m_iNumVerts);
	m_vBuffer->Unlock();

	short pIndeces[MAXBOLTVERTINDEXCOUNT];
	int mult, mult2;

	for(i = 0; i < numSegments; i++)
	{
		mult = i * 4;
		mult2 = i * 6;
		pIndeces[mult2] = mult;
		pIndeces[mult2 + 1] = mult + 1;
		pIndeces[mult2 + 2] = mult + 2;
		pIndeces[mult2 + 3] = mult + 1;
		pIndeces[mult2 + 4] = mult + 3;
		pIndeces[mult2 + 5] = mult + 2;
	}

	GCI->GetDirectX()->GetDevice()->CreateIndexBuffer(m_iNumIndeces * sizeof(short),
								0,
								D3DFMT_INDEX16,
								D3DPOOL_DEFAULT,
								&m_iBuffer,
								NULL);

	m_iBuffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, pIndeces, (sizeof(pIndeces) / MAXBOLTVERTINDEXCOUNT) * m_iNumIndeces);
	m_iBuffer->Unlock();

	return true;
}

void ElectricBolt::clean()
{
	SAFE_RELEASE(m_vBuffer);
	SAFE_RELEASE(m_iBuffer);

	//delete[] pVerts;
	//delete[] pIndeces;
}

BoltMgr::BoltMgr()
{
	m_bDefaultBuilt = false;
}

bool BoltMgr::Init()
{
	m_iBoltID = 0;
	Clean();
	return true;
}

bool BoltMgr::AddBolt(int &ID, int groupID, int numSegments, std::vector<int> *additionalGroups)
{
	if(numSegments <= 0) return false;
	std::shared_ptr<ElectricBolt> newBolt = std::make_shared<ElectricBolt>();
	if(!newBolt->createBolt(m_iBoltID, numSegments))
		return false;
	m_mvBoltGroupMap[groupID].push_back(newBolt);
	m_mBoltMap[m_iBoltID] = newBolt;
	ID = m_iBoltID;
	m_iBoltID++;

	if(additionalGroups)
	{
		UINT size = additionalGroups->size();
		for(UINT i = 0; i < size; i++)
		{
			m_mvBoltGroupMap[additionalGroups->at(i)].push_back(newBolt);
		}
	}

	return true;
}

bool BoltMgr::RemoveBolt(int ID)
{
	m_mBoltMap.erase(ID);

	std::vector<std::shared_ptr<ElectricBolt>>::iterator vit;

	bool success = false;
	for(std::map<int, std::vector<std::shared_ptr<ElectricBolt>>>::iterator it = m_mvBoltGroupMap.begin();
		it != m_mvBoltGroupMap.end(); it++)
	{
		for(vit = it->second.begin(); vit != it->second.end(); vit++)
		{
			if(vit->get()->m_iID == ID)
			{
				it->second.erase(vit);
				success = true;
			}
		}
	}
	return success;
}

void BoltMgr::RemoveGroup(int groupID)
{
	std::vector<std::shared_ptr<ElectricBolt>>::iterator vit;

	for(std::map<int, std::vector<std::shared_ptr<ElectricBolt>>>::iterator it = m_mvBoltGroupMap.begin();
		it != m_mvBoltGroupMap.end(); it++)
	{
		for(vit = it->second.begin(); vit != it->second.end(); vit++)
		{
			m_mBoltMap.erase(vit->get()->m_iID);
		}
	}
}

bool BoltMgr::RenderBolt(int ID, Vector3 startPos, Vector3 endPos, Vector3 camUp,
							   float boltScale, float ArcScale, int randOffset, float midpoint, Vector4 color)
{
	std::map<int, std::shared_ptr<ElectricBolt>>::iterator it = m_mBoltMap.find(ID);
	
#if USEDEFAULTBOLT
	if(it == m_mBoltMap.end())
	{
		if(m_bDefaultBuilt == false)
		{
			m_bDefaultBuilt = m_mDefaultBolt.createBolt(0, 5);
			if(m_bDefaultBuilt == false)
				return false;
		}
		look = endPos - startPos;
		D3DXVec3Normalize(&normal, &look);
		D3DXVec3Cross(&right, &camUp, &normal);
		D3DXVec3Cross(&up, &normal, &right);

		D3DXMatrixLookAtRH(&world, &startPos, &endPos, &up);
		D3DXMatrixTranslation(&world, startPos.x, startPos.y, startPos.z);

		GCI->GetDeferredR()->ApplyElectricityProperties(&world,
														startPos,
														endPos,
														boltScale,
														ArcScale,
														midpoint,
														randOffset,
														color);


		GCI->GetDirectX()->GetDevice()->SetStreamSource(0, m_mDefaultBolt.m_vBuffer, 0, sizeof(CUSTOMVERTEX_BOLT));
		GCI->GetDirectX()->GetDevice()->SetIndices(m_mDefaultBolt.m_iBuffer);

		GCI->GetDirectX()->GetDevice()->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_mDefaultBolt.m_iNumVerts, 0, m_mDefaultBolt.m_iPrimCount);
		return false;
	}
#else
	if(it == m_mBoltMap.end())
		return false;
#endif

	look = endPos - startPos;
	D3DXVec3Normalize(&normal, &look);
	D3DXVec3Cross(&right, &camUp, &normal);
	D3DXVec3Cross(&up, &normal, &right);

	D3DXMatrixLookAtRH(&world, &startPos, &endPos, &up);
	D3DXMatrixTranslation(&world, startPos.x, startPos.y, startPos.z);

	GCI->GetDeferredR()->ApplyElectricityProperties(&world,
													startPos,
													endPos,
													boltScale,
													ArcScale,
													midpoint,
													randOffset,
													color);


	GCI->GetDirectX()->GetDevice()->SetStreamSource(0, it->second->m_vBuffer, 0, sizeof(CUSTOMVERTEX_BOLT));
	GCI->GetDirectX()->GetDevice()->SetIndices(it->second->m_iBuffer);

	GCI->GetDirectX()->GetDevice()->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, it->second.get()->m_iNumVerts, 0, it->second.get()->m_iPrimCount);

	return true;
}

bool BoltMgr::RenderRandomBolt(int groupID, Vector3 startPos, Vector3 endPos, Vector3 camUp,
							   float boltScale, float ArcScale, int randOffset, float midpoint, Vector4 color)
{
	std::map<int, std::vector<std::shared_ptr<ElectricBolt>>>::iterator it;
	it = m_mvBoltGroupMap.find(groupID);
	if(it == m_mvBoltGroupMap.end())
	{
#if USEDEFAULTBOLT
		if(m_bDefaultBuilt == false)
		{
			m_bDefaultBuilt = m_mDefaultBolt.createBolt(0, 5);
			if(m_bDefaultBuilt == false)
				return false;
		}
		look = endPos - startPos;
		D3DXVec3Normalize(&normal, &look);
		D3DXVec3Cross(&right, &camUp, &normal);
		D3DXVec3Cross(&up, &normal, &right);

		D3DXMatrixLookAtRH(&world, &startPos, &endPos, &up);
		D3DXMatrixTranslation(&world, startPos.x, startPos.y, startPos.z);

		GCI->GetDeferredR()->ApplyElectricityProperties(&world,
														startPos,
														endPos,
														boltScale,
														ArcScale,
														midpoint,
														randOffset,
														color);


		GCI->GetDirectX()->GetDevice()->SetStreamSource(0, m_mDefaultBolt.m_vBuffer, 0, sizeof(CUSTOMVERTEX_BOLT));
		GCI->GetDirectX()->GetDevice()->SetIndices(m_mDefaultBolt.m_iBuffer);

		GCI->GetDirectX()->GetDevice()->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_mDefaultBolt.m_iNumVerts, 0, m_mDefaultBolt.m_iPrimCount);
#endif
		return false;
	}

	std::shared_ptr<ElectricBolt> sBolt = m_mBoltMap[it->second[rand() % (int)it->second.size()]->m_iID];

	look = endPos - startPos;
	D3DXVec3Normalize(&normal, &look);
	D3DXVec3Cross(&right, &camUp, &normal);
	D3DXVec3Cross(&up, &normal, &right);

	D3DXMatrixLookAtRH(&world, &startPos, &endPos, &up);
	D3DXMatrixTranslation(&world, startPos.x, startPos.y, startPos.z);

	GCI->GetDeferredR()->ApplyElectricityProperties(&world,
													startPos,
													endPos,
													boltScale,
													ArcScale,
													midpoint,
													randOffset,
													color);


	GCI->GetDirectX()->GetDevice()->SetStreamSource(0, sBolt->m_vBuffer, 0, sizeof(CUSTOMVERTEX_BOLT));
	GCI->GetDirectX()->GetDevice()->SetIndices(sBolt->m_iBuffer);

	GCI->GetDirectX()->GetDevice()->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, sBolt->m_iNumVerts, 0, sBolt->m_iPrimCount);
	return true;
}

void BoltMgr::Clean()
{
	m_mBoltMap.clear();
	m_mvBoltGroupMap.clear();
#if USEDEFAULTBOLT
	if(m_bDefaultBuilt)
	{
		m_mDefaultBolt.clean();
		m_bDefaultBuilt = false;
	}
#endif
}

void BoltMgr::PreRestartDevice()
{
#if USEDEFAULTBOLT
	m_mDefaultBolt.PreRestartDevice();
#endif
	for(std::map<int, std::shared_ptr<ElectricBolt>>::iterator it = m_mBoltMap.begin();
		it != m_mBoltMap.end(); it++)
	{
		it->second->PreRestartDevice();
	}
}

void BoltMgr::PostRestartDevice()
{
#if USEDEFAULTBOLT
	m_mDefaultBolt.PostRestartDevice();
#endif
	for(std::map<int, std::shared_ptr<ElectricBolt>>::iterator it = m_mBoltMap.begin();
		it != m_mBoltMap.end(); it++)
	{
		it->second->PostRestartDevice();
	}
}

ElectricityData::ElectricityData(int ID, float boltSize, float ArcAmount, float ArcMidpoint, int offset)
{
	m_iID = ID;
	m_fBoltScale = boltSize;
	m_fArcScale = ArcAmount;
	m_fMidPoint = ArcMidpoint;
	m_iOffset = offset;
	m_bIsVisible = true;
}

void ElectricityData::SetStartEnd(Vector3 start, Vector3 end)
{
	m_vStartPos = start;
	m_vEndPos = end;
}

void ElectricityData::SetSelectionID(int selectionID)
{
	m_iBoltID = selectionID;
}

void ElectricityData::SetBoltChoiceType(UINT i)
{
	m_iSelectionType = i;
}

void ElectricityData::SetVisible(bool b)
{
	m_bIsVisible = b;
}

void ElectricityData::Render(BoltMgr *bolts, Vector3 camUp)
{
	if(!m_bIsVisible) return;

	switch(m_iSelectionType)
	{
		case 0:	//render by bolt ID
		{
			bolts->RenderBolt(m_iBoltID, m_vStartPos, m_vEndPos, camUp,
								m_fBoltScale,
								m_fArcScale,
								m_iOffset,
								m_fMidPoint,
								m_vColor);
			m_iOffset = (m_iOffset % 10000) + 1;
			return;
		}
		case 1:	//render a random bolt in a group
		{
			bolts->RenderRandomBolt(m_iBoltID, m_vStartPos, m_vEndPos, camUp,
									m_fBoltScale,
									m_fArcScale,
									m_iOffset,
									m_fMidPoint,
									m_vColor);
			m_iOffset = (m_iOffset % 10000) + 1;
		}
		default:
		{
			assert(false);
		}
	}
}