#include "InstanceManager.h"
#include "../GraphicsCore.h"
#include "../Westley's Lib/Transform.h"

InstanceManager::InstanceManager()
{
}
InstanceManager::~InstanceManager()
{
}

void InstanceManager::Init()
{
}
void InstanceManager::Shutdown()
{
	// Remove all Instances
	m_InstanceList.clear();
	while( !m_InstanceMap.empty() )
	{
		m_InstanceMap.begin()->second->Shutdown();
		m_InstanceMap.begin()->second = 0;
		m_InstanceMap.erase( m_InstanceMap.begin() );
	}
}

void InstanceManager::AddInstance(std::string instName, std::string texture, D3DXMATRIX transforms[], D3DXCOLOR colors[], int count)
{
	m_InstanceMap.insert( std::pair< std::string, std::shared_ptr<Instancing> >
		(instName, std::shared_ptr<Instancing>(new Instancing)) );
	
	// Prepare the instance buffer
	for(int i = 0; i < count; i++)
		m_InstanceMap[instName]->AddBachInstance(transforms[i], colors[i]);

	// Create the instance vertex buffer
	m_InstanceMap[instName]->Create(texture);
	m_InstanceList.push_back( m_InstanceMap[instName] );
}
void InstanceManager::RemoveInstance(std::string instName)
{
	// Remove instance from the list and map
	for(int i = 0; i < m_InstanceList.size(); i++)
		if(m_InstanceList[i] == m_InstanceMap[instName])
		{
			m_InstanceList[i] = 0;
			m_InstanceList.erase( m_InstanceList.begin() + i );
		}
	// Shutdown and remove instance
	m_InstanceMap[instName]->Shutdown();
	m_InstanceMap[instName] = 0;	// Null out pointer
	m_InstanceMap.erase( m_InstanceMap.find( instName ) );
}
void InstanceManager::UpdateInstance(std::string instName, std::vector<BaseObjectPtr> objects)
{
	for(auto o = objects.begin();
		o != objects.end(); o++)
	{
		// Update the objects
	//	m_InstanceMap[instName]->Update(

	}
}
void InstanceManager::UpdateInstance(std::string instName, D3DXMATRIX transforms[], int count)
{
	// Update instances tranforms
	for(int i = 0; i < count; i++)
		m_InstanceMap[instName]->Update(i, transforms[i]);
}
void InstanceManager::UpdateInstance(std::string instName, D3DXCOLOR colors[], int count)
{
	// Update instances colors
	for(int i = 0; i < count; i++)
		m_InstanceMap[instName]->Update(i, colors[i]);
}
void InstanceManager::SetWorldMat(std::string instName, D3DXMATRIX world)
{
	m_InstanceMap[instName]->SetWorldMat( world );
}

void InstanceManager::Render()
{
	DirectXClass* DXI = GCI->GetDirectX();
	DeferredShader* DSX = GCI->GetDeferredR();

	// Set Shader Variables
	DSX->m_pFX->SetValue("g_sCameraPos", &GCI->GetCamera()->m_Position, sizeof(D3DXVECTOR3));

	// Set technique
	HRESULT hr = DSX->m_pFX->SetTechnique("ImposterTech");
	hr = DSX->m_pFX->CommitChanges();
	
	UINT numPasses = 0;
	DSX->m_pFX->Begin(&numPasses, 0);
	DSX->m_pFX->BeginPass(0);

	// Draw all Instance classes
	for(auto i = m_InstanceList.begin(); i != m_InstanceList.end(); i++)
		(*i)->Render();

	DSX->m_pFX->EndPass();
	DSX->m_pFX->End();
}
void InstanceManager::Render(std::string instName)	// Renders specific instances
{
	DirectXClass* DXI = GCI->GetDirectX();
	DeferredShader* DSX = GCI->GetDeferredR();

	// Set Shader Variables
	DSX->m_pFX->SetValue("g_sCameraPos", &GCI->GetCamera()->m_Position, sizeof(D3DXVECTOR3));

	// Set technique
	HRESULT hr = DSX->m_pFX->SetTechnique("ImposterTech");
	hr = DSX->m_pFX->CommitChanges();
	
	UINT numPasses = 0;
	DSX->m_pFX->Begin(&numPasses, 0);
	DSX->m_pFX->BeginPass(0);

	// Draw a specific Instance class
	m_InstanceMap[instName]->Render();

	DSX->m_pFX->EndPass();
	DSX->m_pFX->End();
}
