#pragma once

#include "HDR_Light.h"
#include "../3D/Camera.h"
#include "../SceneGraph/GraphicsManager.h"

struct QuadCoord
{
	QuadCoord()
	{}
	QuadCoord(float leftU, float topV, float rightU, float bottomV)
		:m_LeftU(leftU),
		 m_TopV(topV),
		 m_RightU(rightU),
		 m_BottomV(bottomV) {}
	float m_LeftU, m_TopV;
	float m_RightU, m_BottomV;
};

class DeferredShader
{
	friend class HDR_Light;

private:
	// Screen Space Quad
	float		m_fWidth,
				m_fHeight;
	int			m_iNumObjDrawn;	// Number of Objects Drawn
	float		m_fNumObjDrawn;	// Number of Objects Drawn
	GraphicsManager*	m_pGraphicsMgr;

	bool	CheckDeviceCaps();
	D3DXMATRIX		m_WorldInvTransMat;
	HDR_Light		m_HDR;
	bool			m_bUseHDR;
	bool			m_bUseSSAO;
	bool			m_bUseDOF;
	bool			m_bUseShadowMap;
	bool			m_bUseHeatHaze;
	bool			m_bUseDecal;
	bool			m_bUseBlur;

	IDirect3DTexture9*	m_pBlackTex;
	IDirect3DTexture9*	m_pTex;
	//IDirect3DTexture9*	m_pHeatMtrlTex;
	//IDirect3DTexture9*	m_pHeatMaskTex;

public:
	ID3DXEffect*	m_pFX;
	D3DXMATRIX		m_WorldMat;
	IDirect3DTexture9*	m_pDefaultTex;
	// Effect Parameters
	D3DXHANDLE		m_hWVP;
	D3DXHANDLE		m_hWorldInvTrans;
	D3DXHANDLE		m_hWorld;
	D3DXHANDLE		m_hCameraPos;
	D3DXHANDLE		m_hPixelSize;

	// Light
	D3DXHANDLE		m_hLightPos;
	D3DXHANDLE		m_hAmbientLight;
	D3DXHANDLE		m_hDiffuseLight;
	D3DXHANDLE		m_hSpecLight;
	D3DXHANDLE		m_hLightAttenuation;
	Light			m_Light;

	// Matterial
	D3DXHANDLE		m_hAmbientMtrl;
	D3DXHANDLE		m_hDiffuseMtrl;
	D3DXHANDLE		m_hSpecMtrl;
	D3DXHANDLE		m_hSpecPower;

	// Deffered texture data handles
	D3DXHANDLE		m_hDefMtrl;
	D3DXHANDLE		m_hDefNormW;
	D3DXHANDLE		m_hDefPosW;
	D3DXHANDLE		m_hDefDepth;

	// Technique
	D3DXHANDLE		m_thMain;	// Main Technique
	D3DXHANDLE		m_thLight;	// Lighting Technique
	D3DXHANDLE		m_thDOF;	// Depth of Field Technique
	D3DXHANDLE		m_thSSAO;	// Screen Space Ambient Occlusion
	D3DXHANDLE		m_thHaze;
	D3DXHANDLE		m_thDecals;
	D3DXHANDLE		m_thBlur;	// Motion Blur Technique
	D3DXHANDLE		m_thElectricity;

	D3DXHANDLE		m_hHeatBump1;
	D3DXHANDLE		m_hHeatBump2;
	D3DXHANDLE		m_hHeatOffset1;
	D3DXHANDLE		m_hHeatOffset2;
	D3DXHANDLE		m_hHeatIntensity1;
	D3DXHANDLE		m_hHeatIntensity2;
	D3DXHANDLE		m_hHeatScale1;
	D3DXHANDLE		m_hHeatScale2;

	D3DXHANDLE		m_hDecalViewProj;
	D3DXHANDLE		m_hDecalTexture;
	D3DXHANDLE		m_hDecalNormal;
	D3DXHANDLE		m_hDecalLock;

	D3DXHANDLE		m_hBlurScale;

	D3DXHANDLE		m_hElectricStartPos;
	D3DXHANDLE		m_hElectricEndPos;
	D3DXHANDLE		m_hElectricArcScale;
	D3DXHANDLE		m_hRandOffset;
	D3DXHANDLE		m_hElectricSize;
	D3DXHANDLE		m_hElectricArcMidpoint;
	D3DXHANDLE		m_hElectricTexture;
	D3DXHANDLE		m_hElectricColor;

	// Flags
	D3DXHANDLE		m_hPhong;
	D3DXHANDLE		m_hBlinnPhong;
	D3DXHANDLE		m_hIsTex;
	
private:
	// Render Surface
	RenderSurface	m_MtrlSurface;
	RenderSurface	m_NormSurface;
	RenderSurface	m_PosSurface;
	RenderSurface	m_DepthSurface;
	RenderSurface	m_ShadowSurface;
	//RenderSurface	m_LightSurface;
	RenderSurface	m_SSAOsurface;
	//RenderSurface	m_HazeMaskSurface;
	RenderSurface	m_VelocityBlurSurface;
	IDirect3DSurface9*	m_pBackBuff;
	//IDirect3DSurface9* m_pHazeBuff;

	//D3DXVECTOR2 m_DistortionDirection1,
	//			m_DistortionDirection2;

	D3DXMATRIX m_mPrevViewProj;

	// Post-Processing effects
	void	DrawFullScreenQuad();
	void	DrawFullScreenQuad(QuadCoord coords);
	void	BuildSSAOMap();
	void	BlurTexMap(IDirect3DSurface9* blurTex);
	void	RunDOF(IDirect3DSurface9* modSurface);
	
public:
	DeferredShader();
	~DeferredShader();
	void	PreRestartDevice();
	void	PostRestartDevice();

	// Terrain Demo
	Terrain* m_pTerrain;

	void	Init();
	void	Shutdown();
	void	ReloadShader();

	// Rendering methods
	void	BeginRender(WTW::Camera* camera);
	void	EndRender();

	// Rendering to shadow map
	void	BeginShadowMap();
	void	EndShadowMap();

	// Rendering Heat Distortion
	void	ApplyHeatHaze();

	// Decal Methods
	void	BeginDecals();
	void	EndDecals();
	void	RenderDecal(int SharedID, std::string MeshName, IDirect3DTexture9 *texture, D3DXMATRIX decalMatrix, D3DXVECTOR3 decalLook, bool lockToObject);

	void	ApplyBlur(IDirect3DSurface9* modSurface = 0);
	void	SetBlurScale(float scale);

	void	BeginElectricity();
	void	EndElectricity();
	void	ApplyElectricityProperties(D3DXMATRIX *world, Vector3 startPos, Vector3 endPos,
		float boltScale, float ArcScale, float midpoint, int randOffset,
		D3DXVECTOR4 color);
	//void	RenderElectricity();

	// Graphics statistics
	void	AddNumObjDrawn(unsigned int numObj);
	int*	GetNumObjDrawn();
	float*	GetNumObjDrawnFloat();

	// Switch shaders on/off
	void	UseHDR(bool enable);
	void	UseSSAO(bool enable);
	void	UseDOF(bool enable);
	void	UseShadowMap(bool enable);
	void	UseHeatHaze(bool enable);
	void	UseDecals(bool enable);
	void	UseBlur(bool enable);

	// Get status of shaders
	bool	IsUsingHDR();
	bool	IsUsingSSAO();
	bool	IsUsingDOF();
	bool	IsUsingShadowMap();
	bool	NeedToUpdateShadowMap();
	bool	IsUsingHeatHaze();

	// Alpha Quad Rendering Is tricky in Deferred Shading.
	// We register the Graphics Manager so that we can 
	// render it's alpha Quads at the right time.
	void	RegisterRendering(GraphicsManager* gMgr);

	// Set the glare of the camrera in HDR
	void	SetGlareType(EGlareType glareType);

	// Light Controller
	int		AddLight(D3DXVECTOR4 pos, D3DXVECTOR4 intensity, int power);
	void	ChangeLightPos(int light, D3DXVECTOR4 pos);
	void	ChangeLightPower(int id, int power);
	void	RemoveLight(int light);
	void	RemoveAllLights();

	// Debug Save Buffer to image
	void	PrintBuffer(std::string buffer);
};