#include "LensGlare.h"
#include "../MacroTools.h"

// Static lens glare library information
static	GlareDef	s_GlareLibDef[NUM_GLARE_TYPES] =
{
	// Glare Name								glare	bloom	ghost	distort		star	star type				rotate				C.A		current after	ai lum
	{ TEXT("Disable"),							0.0f,	0.0f,	0.0f,	0.01f,		0.0f,	STAR_DISABLE,		D3DXToRadian(0.0f),		0.5f,	0.00f,	0.00f,	0.0f },	// GLARE_DISABLE
	
	{ TEXT("Camera"),							1.5f,	1.2f,	1.0f,	0.00f,		1.0f,	STAR_CROSS,			D3DXToRadian(0.0f),		0.5f,	0.25f,	0.90f,	1.0f },	// GLARE_CAMERA
	{ TEXT("Natural Bloom"),					1.5f,	1.2f,	0.0f,	0.00f,		0.0f,	STAR_DISABLE,		D3DXToRadian(0.0f),		0.0f,	0.40f,	0.85f,	0.5f },	// GLARE_NATRUAL
	{ TEXT("Cheap Lens Camera"),				1.25f,	2.0f,	1.5f,	0.05f,		2.0f,	STAR_CROSS,			D3DXToRadian(0.0f),		0.5f,	0.18f,	0.95f,	1.0f },	// GLARE_CHEAPLENS
	{ TEXT("Afterimage"),						1.5f,	1.2f,	0.5f,	0.00f,		0.7f,	STAR_CROSS,			D3DXToRadian(0.0f),		0.5f,	0.1f,	0.98f,	2.0f },	// GLARE_AFTERIMAGE
	{ TEXT("Cross Screen Filter"),				1.0f,	2.0f,	1.7f,	0.00f,		1.5f,	STAR_CROSSFILTER,	D3DXToRadian(25.0f),	0.5f,	0.20f,	0.93f,	1.0f },	// GLARE_FILTER_CROSSCREEN
	{ TEXT("Spectral Cross Filter"),			1.0f,	2.0f,	1.7f,	0.00f,		1.8f,	STAR_CROSSFILTER,	D3DXToRadian(70.0f),	1.5f,	0.20f,	0.93f,	1.0f },	// GLARE_FILTER_CROSCREEN_SPECTRAL
	{ TEXT("Snow Cross Filter"),				1.0f,	2.0f,	1.7f,	0.00f,		1.5f,	STAR_SNOWCROSS,		D3DXToRadian(10.0f),	0.5f,	0.20f,	0.93f,	1.0f },	// GLARE_FILTER_SNOWCROSS
	{ TEXT("Spectral Snow Cross"),				1.0f,	2.0f,	1.7f,	0.00f,		1.8f,	STAR_SNOWCROSS,		D3DXToRadian(40.0f),	1.5f,	0.20f,	0.93f,	1.0f },	// GLARE_FILTER_SNOWCROSS_SPECTRAL
	{ TEXT("Sunny Cross Filter"),				1.0f,	2.0f,	1.7f,	0.00f,		1.5f,	STAR_SUNNYCROSS,	D3DXToRadian(0.0f),		0.5f,	0.20f,	0.93f,	1.0f },	// GLARE_FILTER_SUNNYCROSS
	{ TEXT("Spectral Sunny Cross"),				1.0f,	2.0f,	1.7f,	0.00f,		1.8f,	STAR_SUNNYCROSS,	D3DXToRadian(45.0f),	1.5f,	0.20f,	0.93f,	1.0f },	// GLARE_FILTER_SUNNYCROSS_SPECTRAL
	{ TEXT("Cinema Camera Vertical Slits"),		1.0f,	2.0f,	1.5f,	0.00f,		1.0f,	STAR_VERTICAL,		D3DXToRadian(90.0f),	0.5f,	0.20f,	0.93f,	1.0f },	// GLARE_CINEMACAM_VERTICALSLITS
	{ TEXT("Cinema Camera Horizontal Slits"),	1.0f,	2.0f,	1.5f,	0.00f,		1.0f,	STAR_VERTICAL,		D3DXToRadian(0.0f),		0.5f,	0.20f,	0.93f,	1.0f },	// GLARE_CINEMACAM_HORIZONTLSLITS
};
static int		s_NumGlareLibDefs = sizeof(s_GlareLibDef)/sizeof(GlareDef);


/********************************************
			 Lens Glare Library
*********************************************/
LensGlare*	LensGlare::s_GlareLibrary = 0;
LensGlare::LensGlare()
{
	Create();
}
LensGlare::LensGlare(const LensGlare& copy)
{
	Create();
	Init(copy);
}
LensGlare::~LensGlare()
{
	Destroy();
}
LensGlare& LensGlare::operator= (const LensGlare& rhs)
{
	Init(rhs);
	return *this;
}

void LensGlare::Create()
{
	ZeroMemory(m_GlareName, sizeof(m_GlareName));

	m_fGlareLum				= 0.0f;
	m_fBloomLum				= 0.0f;
	m_fGhostLum				= 0.0f;
	m_fStarLum				= 0.0f;
	m_fStarInclination		= 0.0f;
	m_fChromaticAberration	= 0.0f;

	m_fAfterImageSensitivity = 0.0f;
	m_fAfterImageRatio		= 0.0f;
	m_fAfterImageLum		= 0.0f;
}
void LensGlare::Destroy()
{
	m_StarDef.Release();
}
void LensGlare::Release()
{}

void LensGlare::Init(const LensGlare& add)
{
	if(&add == this)
		return;

	Release();

	// Copy the data into this
	strcpy_s(m_GlareName, (char*)add.m_GlareName);
	m_fGlareLum	= add.m_fGlareLum;

	m_fBloomLum	= add.m_fBloomLum;
	m_fGhostLum	= add.m_fGhostLum;
	m_fGhostDistortion = add.m_fGhostDistortion;
	m_fStarLum = add.m_fStarLum;
	m_fStarInclination = add.m_fStarInclination;
	m_fChromaticAberration = add.m_fChromaticAberration;

	m_fAfterImageSensitivity = add.m_fAfterImageSensitivity;
	m_fAfterImageRatio = add.m_fAfterImageRatio;
	m_fAfterImageLum = add.m_fAfterImageLum;

	m_StarDef = add.m_StarDef;
}

void LensGlare::Init(const TCHAR* glareName, float glareLum, float bloomLum,
				 float ghostLum, float ghostDistortion, float starLum, EStarType type,
				 float starInclination, float chromaticAberration, float afterimageSensitivity,	// Current weight
				 float afterimageRatio,			// Afterimage weight
				 float afterimageLum)
{
	Release();

	// Create with the input paramaters
	strcpy_s(m_GlareName, glareName);
	m_fGlareLum	= glareLum;

	m_fBloomLum	= bloomLum;
	m_fGhostLum	= ghostLum;
	m_fGhostDistortion = ghostDistortion;
	m_fStarLum = starLum;
	m_fStarInclination = starInclination;
	m_fChromaticAberration = chromaticAberration;

	m_fAfterImageSensitivity = afterimageSensitivity;
	m_fAfterImageRatio = afterimageRatio;
	m_fAfterImageLum = afterimageLum;

	// Create the star form data
	m_StarDef = StarLibrary::GetStarDef(type);
}
void LensGlare::Init(const GlareDef& glareDef)
{
	Init(glareDef.m_GlareName,
		glareDef.m_fGlareLum,
		glareDef.m_fBloomLum,
		glareDef.m_fGhostLum,
		glareDef.m_fGhostDistortion,
		glareDef.m_fStarLum,
		glareDef.m_eStarType,
		glareDef.m_fStarInclination,
		glareDef.m_fChromaticAberration,
		glareDef.m_fAfterImageSensitivity,
		glareDef.m_fAfterImageRatio,
		glareDef.m_fAfterImageLum);
}
void LensGlare::Init(EGlareType type)
{
	return Init(s_GlareLibrary[type]);
}

void LensGlare::InitStaticGlareLibs()
{
	if(s_GlareLibrary)
		return;

	StarLibrary::InitStaticLibrary();
	s_GlareLibrary = DEBUG_NEW LensGlare[NUM_GLARE_TYPES];
	
	// Create the glare forms
	for(int i = 0; i < NUM_GLARE_TYPES; i++)
		s_GlareLibrary[i].Init(s_GlareLibDef[i]);
}
void LensGlare::DeleteStaticGlareLibs()
{
	// Delte all libraries
	SAFE_DELETE_ARRAY(s_GlareLibrary);
}
const LensGlare& LensGlare::GetGlareLib(EGlareType type)
{
	return s_GlareLibrary[type];
}

// Generate global static object
StaticLight StaticLight::s_StaticLight;