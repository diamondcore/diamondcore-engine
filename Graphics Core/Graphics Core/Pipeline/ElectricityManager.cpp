#include "ElectricityManager.h"

#include "../GraphicsCore.h"

ElectricityManager::ElectricityManager()
{
	m_iObjID = 0;
}

void ElectricityManager::Init()
{
	m_bBolts.Init();
}

bool ElectricityManager::AddBolt(int &ID, int groupID, int numSegments, std::vector<int> *additionalGroups)
{
	return m_bBolts.AddBolt(ID, groupID, numSegments);
}

bool ElectricityManager::RemoveBolt(int ID)
{
	return m_bBolts.RemoveBolt(ID);
}

void ElectricityManager::RemoveBoltGroup(int groupID)
{
	m_bBolts.RemoveGroup(groupID);
}

void ElectricityManager::AddBoltObject(int &ID, Vector3 startPos, Vector3 endPos, float boltSize, float ArcAmount, float ArcMidpoint,
									   UINT selectionType, int selectionID,
									   Vector4 color)
{
	ElectricityData newData(m_iObjID, boltSize, ArcAmount, ArcMidpoint,  1.0f);
	ID = m_iObjID;
	m_iObjID++;
	newData.SetStartEnd(startPos, endPos);
	newData.SetSelectionID(selectionID);
	newData.SetBoltChoiceType(selectionType);
	newData.SetColor(color);
	m_lBoltObject.push_back(newData);
}

bool ElectricityManager::RemoveBoltObject(int ID)
{
	for(m_BoltObjIterator = m_lBoltObject.begin(); m_BoltObjIterator != m_lBoltObject.end(); m_BoltObjIterator++)
	{
		if(m_BoltObjIterator->GetID() == ID)
		{
			m_lBoltObject.erase(m_BoltObjIterator);
			return true;
		}
	}
	return false;
}

bool ElectricityManager::SetBoltObjPositions(int ID, Vector3 startPos, Vector3 endPos)
{
	for(m_BoltObjIterator = m_lBoltObject.begin(); m_BoltObjIterator != m_lBoltObject.end(); m_BoltObjIterator++)
	{
		if(m_BoltObjIterator->GetID() == ID)
		{
			m_BoltObjIterator->SetStartEnd(startPos, endPos);
			return true;
		}
	}
	return false;
}

bool ElectricityManager::SetBoltObjArcScale(int ID, float scale)
{
	for(m_BoltObjIterator = m_lBoltObject.begin(); m_BoltObjIterator != m_lBoltObject.end(); m_BoltObjIterator++)
	{
		if(m_BoltObjIterator->GetID() == ID)
		{
			m_BoltObjIterator->SetArcScale(scale);
			return true;
		}
	}
	return false;
}

bool ElectricityManager::SetBoltObjThickness(int ID, float thickness)
{
	for(m_BoltObjIterator = m_lBoltObject.begin(); m_BoltObjIterator != m_lBoltObject.end(); m_BoltObjIterator++)
	{
		if(m_BoltObjIterator->GetID() == ID)
		{
			m_BoltObjIterator->SetBoltScale(thickness);
			return true;
		}
	}
	return false;
}

bool ElectricityManager::SetBoltObjMidpoint(int ID, float midpoint)
{
	for(m_BoltObjIterator = m_lBoltObject.begin(); m_BoltObjIterator != m_lBoltObject.end(); m_BoltObjIterator++)
	{
		if(m_BoltObjIterator->GetID() == ID)
		{
			m_BoltObjIterator->SetMidpoint(midpoint);
			return true;
		}
	}
	return false;
}

bool ElectricityManager::SetBoltObjRandOffset(int ID, int offset)
{
	for(m_BoltObjIterator = m_lBoltObject.begin(); m_BoltObjIterator != m_lBoltObject.end(); m_BoltObjIterator++)
	{
		if(m_BoltObjIterator->GetID() == ID)
		{
			m_BoltObjIterator->SetOffset(offset);
			return true;
		}
	}
	return false;
}

bool ElectricityManager::SetBoltObjSelectionType(int ID, UINT selectionType)
{
	for(m_BoltObjIterator = m_lBoltObject.begin(); m_BoltObjIterator != m_lBoltObject.end(); m_BoltObjIterator++)
	{
		if(m_BoltObjIterator->GetID() == ID)
		{
			m_BoltObjIterator->SetBoltChoiceType(selectionType);
			return true;
		}
	}
	return false;
}

bool ElectricityManager::SetBoltObjSelectionID(int ID, int selectionID)
{
	for(m_BoltObjIterator = m_lBoltObject.begin(); m_BoltObjIterator != m_lBoltObject.end(); m_BoltObjIterator++)
	{
		if(m_BoltObjIterator->GetID() == ID)
		{
			m_BoltObjIterator->SetSelectionID(selectionID);
			return true;
		}
	}
	return false;
}

bool ElectricityManager::SetBoltObjVisibility(int ID, bool visible)
{
	for(m_BoltObjIterator = m_lBoltObject.begin(); m_BoltObjIterator != m_lBoltObject.end(); m_BoltObjIterator++)
	{
		if(m_BoltObjIterator->GetID() == ID)
		{
			m_BoltObjIterator->SetVisible(visible);
			return true;
		}
	}
	return false;
}

bool ElectricityManager::SetBoltObjColor(int ID, D3DXVECTOR4 color)
{
	for(m_BoltObjIterator = m_lBoltObject.begin(); m_BoltObjIterator != m_lBoltObject.end(); m_BoltObjIterator++)
	{
		if(m_BoltObjIterator->GetID() == ID)
		{
			m_BoltObjIterator->SetColor(color);
			return true;
		}
	}
	return false;
}

void ElectricityManager::RenderAll()
{
	if(m_lBoltObject.size() == 0) return;

	D3DXMATRIX view = GCI->GetCamera()->GetView();
	
	//D3DXMatrixInverse(&view, 0, &view);
	D3DXVECTOR3 up = D3DXVECTOR3(view(0, 1), view(1, 1), view(2, 1));


	DWORD prevFVF;
	GCI->GetDirectX()->GetDevice()->GetFVF(&prevFVF);
	GCI->GetDirectX()->GetDevice()->SetFVF(FVF_BOLT);

	GCI->GetDeferredR()->BeginElectricity();
	for(m_BoltObjIterator = m_lBoltObject.begin(); m_BoltObjIterator != m_lBoltObject.end(); m_BoltObjIterator++)
	{
		m_BoltObjIterator->Render(&m_bBolts, up);
	}
	GCI->GetDeferredR()->EndElectricity();

	GCI->GetDirectX()->GetDevice()->SetFVF(prevFVF);
}

void ElectricityManager::Clean()
{
	m_bBolts.Clean();
	m_lBoltObject.clear();
}