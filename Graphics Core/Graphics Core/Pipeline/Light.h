#pragma once

#include "../3D/VertexDecl.h"
#include <vector>

// Color Definitions
#define fOPAQUE (1.0f)
#define	fTRANSPARENT (0.0f)
const D3DXCOLOR WHITE(1.0f, 1.0f, 1.0f, 1.0f);
const D3DXCOLOR BLACK(0.0f, 0.0f, 0.0f, 1.0f);
const D3DXCOLOR CYAN(0.0f, 1.0f, 1.0f, 1.0f);
const D3DXCOLOR RED(1.0f, 0.0f, 0.0f, 1.0f);
const D3DXCOLOR GREEN(0.0f, 1.0f, 0.0f, 1.0f);
const D3DXCOLOR BLUE(0.0f, 0.0f, 1.0f, 1.0f);
const D3DXCOLOR	YELLOW(1.0f, 1.0f, 0.0f, 1.0f);
const D3DXCOLOR	ALPHA(1.0f, 0.0f, 1.0f, 0.0f);

class Light
{
protected:
	// Light
	D3DXVECTOR3	m_LightPos;
	D3DXVECTOR3	m_LightDir;
	D3DXCOLOR	m_AmbientLight;
	D3DXCOLOR	m_DiffuseLight;
	D3DXCOLOR	m_SpecLight;
	float		m_fLightPower;
	D3DXVECTOR3	m_Attenuation;

	// Light View Projection
	D3DXMATRIX	m_LightViewProj;
	D3DXMATRIX	m_LightView;
	D3DXMATRIX	m_LightProj;
	D3DXVECTOR3	m_LightUp;
	D3DXVECTOR3	m_LightLook;
	D3DXVECTOR3	m_LightRight;
	float		m_fProjRatio;	// Aspect Ratio for Shadow Map
	float		m_fSunDistance;
	bool		m_bNeedLightUpdate;
	float		m_fTime;		// Time Of Day in seconds
	float		m_fHours;

	int			m_GraphicsID;
	bool		m_bIsSkyLight;
	void		BuildView();

public:
	Light();
	~Light(){}
	virtual void	PreRestartDevice();
	virtual void	PostRestartDevice();

	void	PreRender();
	void	Render();

	D3DXMATRIX		GetLightViewProj();
	D3DXMATRIX		GetLightView();
	void	ToggleSkyLight(bool enable);

	// Set the Light with the position of the sun
	bool	NeedToUpdateLight();
	void	LightHasBeenUpdated();					// Call this to reset light update
	void	SetDate(int month, int day, int year);
	void	SetTime(int hour, int min, int sec);
	float*	GetHours();
};