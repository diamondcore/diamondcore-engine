#include "DistortionMeshManager.h"
#include "../GraphicsCore.h"

#define DISTORTSPEED1 D3DXVECTOR2(-0.002f, 0.01f);
#define DISTORTSPEED2 D3DXVECTOR2(0.0093f, -0.0035f);

DistMeshList::DistMeshList(std::string meshName)
	:m_MeshName(meshName)
{
	m_OffsetMat = GCI->GetMeshManager()->GetMesh(m_MeshName)->GetBV()->GetOffset();
}
DistMeshList::~DistMeshList()
{
	m_MeshName.clear();
	Clear();
}
void DistMeshList::Clear()
{
	m_Objects.clear();
	m_ObjectVec.clear();
}
void DistMeshList::AddObject(int sharedMemID)
{
	// Add object to the main list
	m_Objects.insert(std::pair<int, BaseObjectPtr>
		(sharedMemID, std::shared_ptr<BaseObject>(new BaseObject)));
	m_Objects[sharedMemID]->m_SharedMemID = sharedMemID;
	m_Objects[sharedMemID]->m_Tranform.SetOffset( &m_OffsetMat );

	//// Set Shadow Map settings
	//m_Objects[sharedMemID]->m_bShadowMap = false;
}
void DistMeshList::CreateObject(int sharedMemID)
{
	AddObject(sharedMemID);
	m_ObjectVec.push_back(m_Objects[sharedMemID]);
}
void DistMeshList::RemoveObject(int sharedMemID)
{
	auto iter = m_Objects.find(sharedMemID);
	if(iter == m_Objects.end())
		return;

	m_Objects[sharedMemID] = 0;
	m_Objects.erase(iter);
	for(auto i = m_ObjectVec.begin();
		i != m_ObjectVec.end(); i++)
	{
		if((*i)->m_SharedMemID == sharedMemID)
		{
			(*i) = 0;
			m_ObjectVec.erase(i);
			break;
		}
	}
}
void DistMeshList::Render()
{
	GCI->GetMeshManager()->Render(m_MeshName, m_ObjectVec);
}
void DistMeshList::Update(Module *sharedMem)
{
	for(auto i = m_Objects.begin();
			i != m_Objects.end(); i++)
	{
		(*i).second->m_Tranform.m_Position = sharedMem->GetSharedResource((*i).second->m_SharedMemID)->GetPosition();
		(*i).second->m_Tranform.m_Quaternion =  sharedMem->GetSharedResource((*i).second->m_SharedMemID)->GetOrientation();
	}
}
BaseObjectPtr	DistMeshList::GetObject(int id)
{	
	if(m_Objects.find(id) == m_Objects.end())	// Make sure object is valid
		return 0;
	return m_Objects[id];	
}

DistMeshManager::DistMeshManager()
{
}
DistMeshManager::~DistMeshManager()
{
	m_DistortMeshLists.clear();
}
void DistMeshManager::ClearScene()
{
	m_DistortMeshLists.clear();
}
void DistMeshManager::Update(float dt, Module* sharedMem)
{
	for(auto g = m_DistortMeshLists.begin();
		g != m_DistortMeshLists.end();
		g++)
	{
		for(auto i = (*g).second.m_Meshes.begin();
			i != (*g).second.m_Meshes.end(); i++)
		{
			(*i).second->Update(sharedMem);
		}
	}
}
void DistMeshManager::CreateMesh(int sharedID, std::string meshName, int groupID)
{
	auto i = m_DistortMeshLists.find(groupID);
	if(i == m_DistortMeshLists.end())
	{
		m_DistortMeshLists[groupID].m_BumpSize1 = 0.1f;
		m_DistortMeshLists[groupID].m_BumpSize2 = 0.1f;
		m_DistortMeshLists[groupID].m_Offset1 = m_DistortMeshLists[groupID].m_Offset2 = D3DXVECTOR2(0, 0);
		m_DistortMeshLists[groupID].m_OffsetSpeed1 = DISTORTSPEED1;
		m_DistortMeshLists[groupID].m_OffsetSpeed2 = DISTORTSPEED2;
		m_DistortMeshLists[groupID].m_Intensity1 = 5.0f;
		m_DistortMeshLists[groupID].m_Intensity2 = 5.0f;
		m_DistortMeshLists[groupID].m_DistortScale1 = 1.0f;
		m_DistortMeshLists[groupID].m_DistortScale2 = 1.0f;
	}
	//auto i = m_DistortMeshLists[groupID].find(meshName);
	//if(i == m_DistortMeshLists[groupID].end())
	//	return;
	if(m_DistortMeshLists[groupID].m_Meshes.find(meshName) == m_DistortMeshLists[groupID].m_Meshes.end())
	{
		if(!GCI->GetMeshManager()->Load(meshName))
			return;
		m_DistortMeshLists[groupID].m_Meshes.insert(std::pair<std::string, std::shared_ptr<DistMeshList>>
			(meshName, std::shared_ptr<DistMeshList>(new DistMeshList(meshName) )));
	}
	m_DistortMeshLists[groupID].m_Meshes[meshName]->CreateObject(sharedID);
}
void DistMeshManager::Render()
{
	auto DSX = GCI->GetDeferredR();
	for(auto m = m_DistortMeshLists.begin();
		m != m_DistortMeshLists.end(); m++)
	{
		(*m).second.m_Offset1 += (*m).second.m_OffsetSpeed1;
		if((*m).second.m_Offset1.x > 1)
			(*m).second.m_Offset1.x--;
		if((*m).second.m_Offset1.y > 1)
			(*m).second.m_Offset1.y--;
		(*m).second.m_Offset2 += (*m).second.m_OffsetSpeed2;
		if((*m).second.m_Offset2.x > 1)
			(*m).second.m_Offset2.x--;
		if((*m).second.m_Offset2.y > 1)
			(*m).second.m_Offset2.y--;

		DeferredShader* DSX = GCI->GetDeferredR();
		DSX->m_pFX->SetFloatArray(DSX->m_hHeatOffset1, (*m).second.m_Offset1, 2);
		DSX->m_pFX->SetFloatArray(DSX->m_hHeatOffset2, (*m).second.m_Offset2, 2);
		DSX->m_pFX->SetFloat(DSX->m_hHeatBump1, (*m).second.m_BumpSize1);
		DSX->m_pFX->SetFloat(DSX->m_hHeatBump2, (*m).second.m_BumpSize2);
		DSX->m_pFX->SetFloat(DSX->m_hHeatScale1, (*m).second.m_DistortScale1);
		DSX->m_pFX->SetFloat(DSX->m_hHeatScale2, (*m).second.m_DistortScale2);
		DSX->m_pFX->SetFloat(DSX->m_hHeatIntensity1, (*m).second.m_Intensity1);
		DSX->m_pFX->SetFloat(DSX->m_hHeatIntensity2, (*m).second.m_Intensity2);
		for(auto i = (*m).second.m_Meshes.begin();
			i != (*m).second.m_Meshes.end(); i++)
		{
			(*i).second->Render();
		}
	}
}
void DistMeshManager::RemoveMesh(std::string meshName, int sharedID)
{
	for(auto i = m_DistortMeshLists.begin();
		i != m_DistortMeshLists.end(); i++)
	{
		if((*i).second.m_Meshes.find(meshName) == (*i).second.m_Meshes.end())
			return;
		(*i).second.m_Meshes[meshName]->RemoveObject(sharedID);
	}
}
void DistMeshManager::SetDistortOffset(int groupID, D3DXVECTOR2 *offset1, D3DXVECTOR2 *offset2)
{
	if(offset1 != NULL)
		m_DistortMeshLists[groupID].m_Offset1 = *offset1;
	if(offset2 != NULL)
		m_DistortMeshLists[groupID].m_Offset2 = *offset2;
}
void DistMeshManager::SetDistortSpeed(int groupID, D3DXVECTOR2 *speed1, D3DXVECTOR2 *speed2)
{
	if(speed1 != NULL)
		m_DistortMeshLists[groupID].m_OffsetSpeed1 = *speed1;
	if(speed2 != NULL)
		m_DistortMeshLists[groupID].m_OffsetSpeed2 = *speed2;
}
void DistMeshManager::SetDistortBump(int groupID, float bump1, float bump2)
{
	m_DistortMeshLists[groupID].m_BumpSize1 = bump1;
	m_DistortMeshLists[groupID].m_BumpSize2 = bump2;
}
void DistMeshManager::SetDistortScale(int groupID, float scale1, float scale2)
{
	m_DistortMeshLists[groupID].m_DistortScale1 = scale1;
	m_DistortMeshLists[groupID].m_DistortScale2 = scale2;
}
void DistMeshManager::SetDistortIntensity(int groupID, float intensity1, float intensity2)
{
	m_DistortMeshLists[groupID].m_Intensity1 = intensity1;
	m_DistortMeshLists[groupID].m_Intensity2 = intensity2;
}

BaseObjectPtr DistMeshManager::GetObject(int sharedID, std::string meshName, int groupID)
{
	return m_DistortMeshLists[groupID].m_Meshes[meshName]->GetObject(sharedID);
}