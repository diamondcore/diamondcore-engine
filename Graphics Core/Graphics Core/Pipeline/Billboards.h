#pragma once

#include "../SceneGraph/ObjectList.h"
//#include "../3D/VertexDecl.h"
#include "QuadPicking.h"
#include <string>

/********************************
		Quad Instancing
*********************************/
class Billboards
{
	IDirect3DIndexBuffer9*	m_pInstIndexBuffer;
	IDirect3DVertexBuffer9*	m_pInstVertexBuffer;
	D3DXMATRIX				m_WorldMat;

	// Max Array size in shaders 3.0 is 224 Constant Registers(float4) (tested and proven)
	D3DXVECTOR4						m_InstanceData[220];	// Can store WorldMat and Color
	std::map<int, BaseObjectPtr>	m_Registry;				// Register <sharedMemID, BaseObjectPtr>
	unsigned int					m_iVec4Count;			// Count of inst world Vector4
	unsigned int					m_iInstCount;			// Number of instances
	unsigned int					m_iNumVerts;			// Total number of Vertices
	unsigned int					m_iNumIndex;			// Total number of Indices

	std::string					m_TexName;
	bool						m_bIsImposter;
	std::shared_ptr<Billboards>	m_pSibling;				// Pointer to sibling

	void	BuildInstVertexBuff();
	void	BuildInstIndexBuff();

public:
	Billboards(std::string texture);
	~Billboards();
	void	Init();
	void	Shutdown();

	// To Bach, add instances before creating
	void	AddInstance(int sharedMemID, bool shadowMap);	// Add Instance to array
	void	RemoveInstance(int sharedMemID);

	void	Update(Module* sharedMem);
	void	Render(bool shadowMap);

	void	SetAlwaysFaceCamera(bool enable);
	int		GetNumInstances();

	void	SetColor(int id, D3DXCOLOR color);
	BaseObjectPtr	GetObject(int id);

	// Check for picking
	void	CheckQuadPicking(int hitIDs[], int& count, QuadPicking& pick);
};