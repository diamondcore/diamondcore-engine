#pragma once

#include "RenderSurface.h"
#include <memory>

// Basic line of a star
struct StarLine
{
	int		m_NumPasses;
	float	m_fSampleLength;
	float	m_fAttenuation;
	float	m_fInclination;
};

// Basic star definition
struct StarDef
{
	char*	m_StarName;
	int		m_NumStarLines;
	int		m_NumPasses;
	float	m_fSampleLen;
	float	m_fAttenuation;
	float	m_fInclination;
	bool	m_bRotation;
};

// Star definition for a sunny cross filter
struct StarDef_SunnyCross
{
	char*	m_StarName;
	float	m_fSampleLen;
	float	m_fAttenuation;
	float	m_fInclination;
};

enum EStarType{
	STAR_DISABLE = 0,
	
	// Base Star types
	STAR_CROSS,
	STAR_CROSSFILTER,
	STAR_SNOWCROSS,
	STAR_VERTICAL,
	NUM_BASESTAR_TYPES,

	// Advance Star types
	STAR_SUNNYCROSS = NUM_BASESTAR_TYPES,
	NUM_STAR_TYPES
};


/********************************************
				Star Library
*********************************************/
class StarLibrary
{
public:
	char		m_StarName[256];

	int			m_NumStarLines;
	StarLine*	m_pStarLines;		// Array of star lines
	float		m_fInclination;
	bool		m_bRotation;		// Rotation is availble?

private:
	static StarLibrary*	s_StarLibrary;			// Static array of star defs
	static D3DXCOLOR	s_ChromAberColor[8];	// Chromatic Aberation Color

public:
	StarLibrary();
	StarLibrary(const StarLibrary& copy);
	~StarLibrary();
	StarLibrary& operator= (const StarLibrary& rhs);

	// Individual Object Methods
	void	Create();
	void	Destroy();
	void	Release();

	// Initializing Stars
	void	Init(EStarType eType);
	void	Init(const StarLibrary& add);
	void	Init(const StarDef& add);
	void	Init(const TCHAR* starName,
				 int numStarLines,
				 int numPasses,
				 float sampleLength,
				 float attenuation,
				 float inclination,
				 bool rotation);

	// Specific sunny cross filter initialization
	void	Init_SunnyCross(const TCHAR* starName = TEXT("SunnyCross"),
							float sampleLength = 1.0f,
							float attenuation = 0.88f,
							float longAttenuation = 0.95f,
							float inclination = 0.0f);	//D3DXToRadian(0.0f));

public:
	// Static Methods for the Star Library
	static void			InitStaticLibrary();
	static void			DeleteStaticLibrary();
	static const StarLibrary&	GetStarDef(EStarType eType);
	static const D3DXCOLOR&		GetChromAberColor(int id);
};