#include "RenderSurface.h"
#include "../GraphicsCore.h"
#include "../MacroTools.h"

RenderSurface::RenderSurface()
	:m_vTexViewPos(D3DXVECTOR3(1.0f, 34.0f, 0.0f)),
	 m_vTexTarget(D3DXVECTOR3(0.0f, 0.0f, 1.0f)),
	 m_vTexUp(D3DXVECTOR3(0.0f, 1.0f, 0.0f)),
	 m_vTexRight(D3DXVECTOR3(1.0f, 0.0f, 0.0f))
{
	m_RSurface			= 0;
	m_pTexture			= 0;
	m_pTopSurface		= 0;
	m_TextureFormat		= D3DFMT_X8R8G8B8;	// Shadow mapping(D3DFMT_R32F)
	m_DepthSFormat		= D3DFMT_D24X8;	// D3DFMT_D16 (16 bit Z-Buffer)
	m_iWidth			= 256;
	m_iHeight			= 256;
	m_iNumMips			= 1;
	m_CamRotation		= 1.2f * D3DX_PI;
}
RenderSurface::~RenderSurface()
{
	SAFE_RELEASE(m_pTopSurface);
	SAFE_RELEASE(m_pTexture);	
	//SAFE_RELEASE(m_RSurface);
}
void RenderSurface::PreRestartDevice()
{
	SAFE_RELEASE(m_pTopSurface);
	SAFE_RELEASE(m_pTexture);
	//SAFE_RELEASE(m_RSurface);
	//m_RSurface->OnLostDevice();
}
void RenderSurface::PostRestartDevice()
{
	//m_RSurface->OnResetDevice();
	Init();
}

void RenderSurface::Init()
{
	HRESULT hr;
	/*
	hr = D3DXCreateRenderToSurface(GCI->GetDirectX()->GetDevice(), 
						m_iWidth,			// Surface Width (pixel)
						m_iHeight,			// Surface Height (pixel)
						m_TextureFormat,	// Texture Format
						m_bDepthStencil,	// Bool to use Depth Stencil or not
						m_DepthSFormat,		// Depth Stencil format, if used
						&m_RSurface);		// The Surface to render to
	*/
	// Identifies the texture as a render target
	UINT usage = D3DUSAGE_RENDERTARGET;

	// If the hardware supports Auto gen mipmaps, then use them
	if(m_bAutoGenMipMaps)
		usage |= D3DUSAGE_AUTOGENMIPMAP;

	hr = D3DXCreateTexture(GCI->GetDirectX()->GetDevice(), 
						  m_iWidth,			// Surface Width (pixel)
						  m_iHeight,		// Surface Height (pixel)
						  m_iNumMips,		// Number of MipMap levels
						  usage,			// How the texture will be used
						  m_TextureFormat,	// Texture Format
						  D3DPOOL_DEFAULT,	// Render to target in this pool
						  &m_pTexture);

/*	if( FAILED(hr) )
	{
		fprintf(stdout, "RenderSurface::Init - Faild to create texture.\n");
		return;
	}*/

	// Get the top surface level
	hr = m_pTexture->GetSurfaceLevel(0, &m_pTopSurface);	// Zero is the top level
}

void RenderSurface::setDepthStencil(D3DFORMAT format, bool use)
{
	m_DepthSFormat	= format;
	m_bDepthStencil	= use;
}
void RenderSurface::setTexture(D3DFORMAT format, bool autoGenMipMaps, int numMips)
{
	m_TextureFormat		= format;
	m_bAutoGenMipMaps	= autoGenMipMaps;
	m_iNumMips			= numMips;
}

void RenderSurface::Update()
{
	m_ViewPort.X		= 0;
	m_ViewPort.Y		= 0;
	m_ViewPort.Width	= m_iWidth;
	m_ViewPort.Height	= m_iHeight;
	m_ViewPort.MinZ		= 0.0f;
	m_ViewPort.MaxZ		= 1.0f;

	HRESULT hr;

	// Viewport is the subset of the surface to draw to
	hr = m_RSurface->BeginScene(m_pTopSurface, &m_ViewPort);
	//m_pDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,	// Clear the BackBuffer
	//				0xff000000, 1.0f, 0);

	// Update Camera
	/*D3DXMatrixLookAtLH(&m_TexRenderView, &m_vTexViewPos, &m_vTexTarget, &m_vTexUp);
	DMM->m_pCurrentFX->SetValue(DMM->m_hCameraPos, &m_vTexViewPos, sizeof(D3DXVECTOR3));
	
	DMM->m_pCurrentFX->SetMatrix(DMM->m_hWVP, &(m_TexRenderView*m_TexRenderProj));	// Got rid of ProjectionView
	

	UFX->m_pFX->CommitChanges();
	// Apply the Effect Technique
	UINT numPasses = 0;
	DMM->m_pCurrentFX->Begin(&numPasses, 0);
	for(UINT i = 0; i < numPasses; i++)
	{
		UFX->m_pFX->BeginPass(i);
		pDraw->RenderView();	// Use to render to surface
		UFX->m_pFX->EndPass();
	}
	DMM->m_pCurrentFX->End();
*/
	hr = m_RSurface->EndScene(D3DX_FILTER_NONE);	// Attempts to use Hardware auto gen MipMaps
}

void RenderSurface::RenderBegin()
{
	m_ViewPort.X		= 0;
	m_ViewPort.Y		= 0;
	m_ViewPort.Width	= m_iWidth;
	m_ViewPort.Height	= m_iHeight;
	m_ViewPort.MinZ		= 0.0f;
	m_ViewPort.MaxZ		= 1.0f;

	HRESULT hr;

	// Viewport is the subset of the surface to draw to
	hr = m_RSurface->BeginScene(m_pTopSurface, &m_ViewPort);
}

void RenderSurface::RenderEnd()
{
	m_RSurface->EndScene(D3DX_FILTER_NONE);	// Attempts to use Hardware auto gen MipMaps
}

void RenderSurface::DumpSurface(int posX, int posY, int width, int height)
{
	RECT rect;			// Target rectangle
	rect.left	= posX;
	rect.top	= posY;
	rect.right	= posX + width;
	rect.bottom	= posY + height;

	IDirect3DSurface9* backBuffer = 0;

	// Draw surface to screen
	if( !FAILED(GCI->GetDirectX()->GetDevice()->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &backBuffer)))
		GCI->GetDirectX()->GetDevice()->StretchRect(m_pTopSurface, NULL, backBuffer, &rect, D3DTEXF_NONE);

	SAFE_RELEASE(backBuffer);
}

void RenderSurface::SaveToFile(const char* name, D3DXIMAGE_FILEFORMAT format)
{
	if(m_pTexture != 0)	// Make sure theirs a texture
	{
		HRESULT hr;
		std::string fileName = std::string(name);
		fileName += ".bmp";	// Setup the picture file name
		hr = D3DXSaveTextureToFileA(fileName.c_str(), format, m_pTexture, NULL);	// Save the texture to a bmp file
	}
}
void RenderSurface::GetSurface(int level, IDirect3DSurface9** pSurface)
{
	// Get the top surface level
	m_pTexture->GetSurfaceLevel(level, pSurface);	// Zero is the top level
}
