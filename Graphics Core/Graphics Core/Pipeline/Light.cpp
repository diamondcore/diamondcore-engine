#include "Light.h"
#include "../MacroTools.h"
#include "../GraphicsCore.h"

Light::Light()
{
	//m_AmbientLight	= D3DXCOLOR(0.6f, 0.6f, 0.6f, 1.0f);		// Ambient Lighting
	//m_DiffuseLight	= D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);		// Diffuse Lighting
	//m_SpecLight		= D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);		// Specular Lighting
	m_fLightPower		= 32.0f;
	m_bIsSkyLight		= false;
	m_fSunDistance		= 125.0f;//200.0f;
	m_bNeedLightUpdate	= true;
	m_fTime				= 0.0f;
	m_fHours			= 0.0f;

	//m_LightPos		= D3DXVECTOR3(75.0f, -20.0f, 75.0f);	// Under the map
	//m_LightPos		= D3DXVECTOR3(30, 20, -10);		// Use for DOF
	
	//m_AmbientLight	= WHITE;
	m_AmbientLight	= D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);	// Usual
	m_DiffuseLight	= WHITE;//D3DXCOLOR(0.8f, 0.8f, 0.8f, 1.0f);
	m_SpecLight		= D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	m_Attenuation	= D3DXVECTOR3(0.0f, 0.01f, 0.0f);
	//m_LightDir		= D3DXVECTOR3(1.0f, 1.0f, 2.0f);

	// Point light towards the center of the grid
	//m_LightUp	= D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	//m_LightLook	= D3DXVECTOR3(75.0f, 0.01f, 75.0f);	
	//D3DXMatrixLookAtLH(&m_LightView, &m_LightPos, &m_LightLook, &m_LightUp);

	m_LightPos = GCI->GetCamera()->m_Position;
	m_LightLook = GCI->GetCamera()->GetLook();
	m_LightUp = GCI->GetCamera()->GetUp();
	D3DXMATRIX W;
	D3DXMatrixTranslation(&W, m_LightPos.x, m_LightPos.y, m_LightPos.z);
	D3DXMatrixInverse(&W,0,&W);
	D3DXVec3TransformNormal(&m_LightLook, &m_LightLook, &(W));
	D3DXVec3TransformNormal(&m_LightUp, &m_LightUp, &(W));
	D3DXVec3TransformCoord(&m_LightPos, &m_LightPos, &(W));
	//IGH->SetSpherePosition(m_GraphicsID, m_LightPos.x, m_LightPos.y, m_LightPos.z);

	// Calculate Light View Projection
	//D3DXMatrixLookAtLH(&m_LightView, &m_LightPos, &m_LightLook, &m_LightUp);
	//D3DXMatrixPerspectiveOffCenterLH(&m_LightProj, 0, 1, -1, 0, 1, 200);
	//D3DXMatrixPerspectiveOffCenterLH(&m_LightProj, -150, 150, -150, 150, 1, 200);
	m_LightViewProj = m_LightView*m_LightProj;

	float lightFOV = D3DX_PI*0.25f;
	//D3DXMatrixOrthoOffCenterLH(&m_LightProj, 0, 1, -1, 0, 1, 200);
	//D3DXMatrixPerspectiveFovLH(&m_LightProj, lightFOV, 1.0f, 1.0f, 200.0f);
	
	m_LightDir		= m_LightLook - m_LightPos;
	D3DXVec3Normalize(&m_LightDir, &m_LightDir);
	m_fProjRatio = 1.0f;//(GCI->GetDirectX()->GetScreenWidth()*400/800)/(GCI->GetDirectX()->GetScreenHeight()*300/600);
	D3DXMatrixPerspectiveFovLH(&m_LightProj, D3DX_PI*0.25f, m_fProjRatio, 1.0f, 1000.0f);
}

void Light::PreRestartDevice()
{}
void Light::PostRestartDevice()
{
	float lightFOV = D3DX_PI*0.25f;
	//D3DXMatrixOrthoOffCenterLH(&m_LightProj, 0, 1, -1, 0, 1, 200);
	///D3DXMatrixPerspectiveOffCenterLH(&m_LightProj, -150, 150, -150, 150, 1, 200);
	//D3DXMatrixPerspectiveOffCenterLH(&m_LightProj, 0, 1, -1, 0, 1, 200);
	//D3DXMatrixPerspectiveFovLH(&m_LightProj, lightFOV, 1.0f, 1.0f, 200.0f);
	m_fProjRatio = 1.0f;//(GCI->GetDirectX()->GetScreenWidth()*400/800)/(GCI->GetDirectX()->GetScreenHeight()*300/600);
	if(m_bIsSkyLight)
		D3DXMatrixPerspectiveFovLH(&m_LightProj, D3DX_PI*0.25f, m_fProjRatio, 1.0f, 1000.0f);
}

void Light::BuildView()
{
	// Calculate Z-Axis
	D3DXVec3Normalize(&m_LightDir, &m_LightDir);		// Find the LookAt direction

	// Calculate X-Axis
	D3DXVec3Cross(&m_LightRight, &m_LightUp, &m_LightDir);	// Find orthogonal vector of Up and LookAt
	D3DXVec3Normalize(&m_LightRight, &m_LightRight);		// Find the Right direction

	// Calculate Y-Axis
	D3DXVec3Cross(&m_LightUp, &m_LightDir, &m_LightRight);	// Find orthogonal vector of LookAt and Right
	D3DXVec3Normalize(&m_LightUp, &m_LightUp);				// Find the Up direction

	// Recalculate camera's position
	float x = -D3DXVec3Dot(&m_LightPos, &m_LightRight);
	float y = -D3DXVec3Dot(&m_LightPos, &m_LightUp);
	float z = -D3DXVec3Dot(&m_LightPos, &m_LightDir);

	// Set up Camera View Matrix
	m_LightView(0,0) = m_LightRight.x;	m_LightView(0,1) = m_LightUp.x;	m_LightView(0,2) = m_LightDir.x;	m_LightView(0,3) = 0.0f;
	m_LightView(1,0) = m_LightRight.y;	m_LightView(1,1) = m_LightUp.y;	m_LightView(1,2) = m_LightDir.y;	m_LightView(1,3) = 0.0f;
	m_LightView(2,0) = m_LightRight.z;	m_LightView(2,1) = m_LightUp.z;	m_LightView(2,2) = m_LightDir.z;	m_LightView(2,3) = 0.0f;
	m_LightView(3,0) = x;				m_LightView(3,1) = y;			m_LightView(3,2) = z;				m_LightView(3,3) = 1.0f;
}

void Light::PreRender()
{
	//m_LightPos = D3DXVECTOR3(20.0f, 50.0f, -50.0f);	// Overwrite for test
	if(m_bIsSkyLight)	// Sky light
	{
		// Calculate the sky light
		BuildView();
		//D3DXMatrixLookAtLH(&m_LightView, &m_LightPos, &m_LightLook, &m_LightUp);
		m_LightViewProj = m_LightView*m_LightProj;
	}
	else
	{
		// Flash Light
		m_LightPos = GCI->GetCamera()->m_Position;
		m_LightLook = GCI->GetCamera()->GetLook();
		m_LightUp = GCI->GetCamera()->GetUp();
	/*	D3DXMATRIX W;
		D3DXMatrixTranslation(&W, m_LightPos.x, m_LightPos.y, m_LightPos.z);
		//D3DXMatrixInverse(&W,0,&W);
		D3DXVec3TransformNormal(&m_LightLook, &m_LightLook, &(W));
		D3DXVec3TransformNormal(&m_LightUp, &m_LightUp, &(W));
		D3DXVec3TransformCoord(&m_LightPos, &m_LightPos, &(W));
		//IGH->SetSpherePosition(m_GraphicsID, m_LightPos.x, m_LightPos.y, m_LightPos.z);

		// Calculate Light View Projection
		//D3DXMatrixLookAtLH(&m_LightView, &m_LightPos, &m_LightLook, &m_LightUp);
		D3DXMatrixPerspectiveOffCenterLH(&m_LightView, 0, 1, -1, 0, 1, 200);
		D3DXMATRIX V = GCI->GetCamera()->GetView();
		D3DXMatrixInverse(&V, 0,&V);
		m_LightViewProj = (GCI->GetCamera()->GetView())*(V*m_LightView*m_LightProj);
		*/
		m_LightViewProj = GCI->GetCamera()->GetViewProj();
		// Point light towards the center of the grid
		m_LightDir		= GCI->GetCamera()->m_LookAt;//m_LightLook  - m_LightPos;
		D3DXVec3Normalize(&m_LightDir, &m_LightDir);

		m_LightPos = GCI->GetCamera()->m_Position;
		m_bNeedLightUpdate = true;
	}
	
	// Setup shader paramaters
	GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hAmbientLight, &m_AmbientLight, sizeof(D3DXCOLOR));
	GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hDiffuseLight, &m_DiffuseLight, sizeof(D3DXCOLOR));
	GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hSpecLight, &m_SpecLight, sizeof(D3DXCOLOR));
	GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hLightPos, &m_LightPos, sizeof(D3DXVECTOR3));
	GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hLightAttenuation, &m_Attenuation, sizeof(D3DXVECTOR3));
	GCI->GetDeferredR()->m_pFX->SetValue("g_sLightDirW", &m_LightDir, sizeof(D3DXVECTOR3));
	GCI->GetDeferredR()->m_pFX->SetFloat("g_sLightPower", m_fLightPower);
	GCI->GetDeferredR()->m_pFX->SetMatrix("g_sLightVP", &m_LightViewProj);
	//GCI->GetDeferredR()->m_pFX->SetMatrix("g_sLightV", &m_LightView);
}
D3DXMATRIX Light::GetLightViewProj()
{	return m_LightViewProj;	}
D3DXMATRIX Light::GetLightView()
{	return m_LightView;	}


void Light::Render()
{
	auto DSX = GCI->GetDeferredR();

	// Set transform
	D3DXMATRIX transform, invTrans;
	D3DXMatrixTranslation(&transform, m_LightPos.x, m_LightPos.y, m_LightPos.z);
	D3DXMatrixInverse(&invTrans, 0, &transform);
	DSX->m_pFX->SetMatrix(DSX->m_hWorldInvTrans, &invTrans);
	DSX->m_pFX->SetMatrix( DSX->m_hWorld, &transform );
	DSX->m_pFX->SetMatrix("g_sView", &(transform * GCI->GetCamera()->GetView() ));
	DSX->m_pFX->SetMatrix( DSX->m_hWVP, &(transform*GCI->GetCamera()->GetViewProj()) );

	// Set Color
	GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hAmbientMtrl, &WHITE, sizeof(D3DXCOLOR));
	GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hDiffuseMtrl, &WHITE, sizeof(D3DXCOLOR));
	GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hSpecMtrl, &WHITE, sizeof(D3DXCOLOR));
	GCI->GetDeferredR()->m_pFX->SetFloat(GCI->GetDeferredR()->m_hSpecPower, 8.0f);
			

	// Render sphere as the light
	GCI->GetDeferredR()->m_pFX->SetTexture("g_sTexture", GCI->GetDeferredR()->m_pDefaultTex);
	GCI->GetDeferredR()->m_pFX->CommitChanges();
	GCI->GetMeshManager()->GetMesh("Sphere(33).x")->RawRender();
}

void Light::ToggleSkyLight(bool enable)
{	
	m_bIsSkyLight = enable;	
}

// Set the Light with the position of the sun
bool Light::NeedToUpdateLight()
{
	return m_bNeedLightUpdate;
}
void Light::LightHasBeenUpdated()
{
	m_bNeedLightUpdate = false;
}
void Light::SetDate(int month, int day, int year)
{
	// Fake the time of year sun movement
	float maxAngle = 0.25f;					// Only move 25% on the Z-Axis
	float angle = (month * maxAngle)/12.0f;
	m_LightPos.z = m_fSunDistance * sinf( angle );
	m_LightPos.y = m_fSunDistance * -cosf( angle );
	
	// Point light towards the center of the world
	m_LightLook		= D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_LightDir		= m_LightLook  - m_LightPos;
	D3DXVec3Normalize(&m_LightDir, &m_LightDir);
	m_bNeedLightUpdate = true;
}
void Light::SetTime(int hour, int min, int sec)
{
	// Get full time (relavant to seconds)
	m_fTime = (float)hour*60.0f*60.0f + (float)min*60.0f + (float)sec;
	m_fTime = (int)m_fTime % 86400;
	m_fHours = (int)((m_fTime/60.0f)/60.0f) % 24;

	// Fake time of day sun movement
	float angle = ((m_fTime)/86400)*360.0f;	// The sun is on X-Axis 0 at 6 am
	RAD( angle );
	m_LightPos.x = m_fSunDistance * cosf( angle );
	m_LightPos.z = m_fSunDistance * sinf( angle );
	m_LightPos.y = 50.0f;
	
	// Point light towards the center of the world
	m_bNeedLightUpdate = true;
	if(!m_bIsSkyLight)
	{
		D3DXMatrixPerspectiveFovLH(&m_LightProj, D3DX_PI*0.25f, m_fProjRatio, 1.0f, 1000.0f);
		m_bIsSkyLight	= true;
		m_LightLook		= D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		m_LightUp		= D3DXVECTOR3(0.0f, 1.0f, 0.0f);
		m_LightRight	= D3DXVECTOR3(1.0f, 0.0f, 0.0f);	 	
	}
	m_LightDir		= m_LightLook  - m_LightPos;
	D3DXVec3Normalize(&m_LightDir, &m_LightDir);

	// (DEBUG Tool) Test View From Light pos with camera
	//GCI->GetCamera()->m_Position = m_LightPos;
	//GCI->GetCamera()->m_LookAt = m_LightDir;//Look;
}
float* Light::GetHours()
{
	return &m_fHours;
}
