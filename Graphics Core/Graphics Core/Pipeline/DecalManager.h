#pragma once

#include <list>

#include "../3D/SceneMesh.h"
//#include "../3D/MeshManager.h"
#include "../SceneGraph/GraphicsManager.h"
#include "../3D/Camera.h"
//#include "../SceneGraph/ObjectList.h"

class StaticDecal
{
protected:
	IDirect3DTexture9* m_pTexture;
	std::string m_sMeshName;
	D3DXMATRIX m_ViewMatrix;
	//Vector3 m_DecalCamPos;	
	UINT m_ID;
	int m_iSharedID;
	bool m_bIsLocked;
	//SceneMeshPtr m_DecalMesh;


	friend class DecalMgr;
};

class DynamicDecal : public StaticDecal
{
protected:
	float m_iCurrentAge,
			m_iMaxAge;

	friend class DecalMgr;
};

class DecalMgr
{
private:
	/*Contains a decal list of a limited size.
	If too many decals are created, the oldest will be
	removed.*/
	std::list<DynamicDecal> m_lDynamicDecals;
	/*This list should contain all decals that we wish to
	be persistent, i.e. always present.*/
	std::list<StaticDecal> m_lStaticDecals;
	std::list<DynamicDecal>::iterator dit;
	std::list<StaticDecal>::iterator sit;

	WTW::Camera m_DecalCam;

	UINT m_iMaxDecals,	//maximum number of dynamic decals we wish to have
		m_iNumDecals,	//current number of dynamic decals present
		m_iNumStaticDecals;
	UINT
		m_iCurrentID,	//ID of the next dynamic decal to be created
		m_iStaticID;	//ID of the next static decal to be created

public:
	DecalMgr();
	~DecalMgr(){Clean();}

	void Init(UINT iMaxDecals = 10);

	int AddDynamicDecal(IDirect3DTexture9* texture, int SharedID, std::string meshName, Vector3 DecalCamPos, Vector3 DecalTarget,
						bool lockToMesh, float MaxAge, Vector3 up);
	int AddDynamicDecal(IDirect3DTexture9* texture, int SharedID, std::string meshName, D3DXMATRIX matrix,
						bool lockToMesh, float MaxAge);
	int AddStaticDecal(IDirect3DTexture9* texture, int SharedID, std::string meshName, Vector3 DecalCamPos, Vector3 DecalTarget,
						bool lockToMesh, Vector3 up);
	int AddStaticDecal(IDirect3DTexture9* texture, int SharedID, std::string meshName, D3DXMATRIX matrix, bool lockToMesh);

	void RenderAllDecals(float dt = 0.0f);
	//void RenderDynamicDecals();
	//void RenderStaticDecals();
	//bool RenderDynamicDecals(UINT DecalID);
	//bool RenderStaticDecals(UINT DecalID);

	void SetMaxDecalNum(UINT num){m_iMaxDecals = num;}
	bool SetDynamicDecalPosition(UINT DecalID, D3DXVECTOR3 pos);
	bool SetStaticDecalPosition(UINT DecalID, D3DXVECTOR3 pos);
	void ClearDynamicDecals(){m_lDynamicDecals.clear();}
	void ClearStaticDecals(){m_lStaticDecals.clear();}
	bool RemoveDynamicDecal(int DecalID);
	bool RemoveStaticDecal(int DecalID);
	void RemoveDecalsByMeshName(std::string MeshName);
	void Clean();

	UINT GetNumDynamicDecals(){return m_iNumDecals;}
	UINT GetNumStaticDecals(){return m_iNumStaticDecals;}

	//friend GraphicsManager;
	//friend MeshManager;
};