#include "DecalManager.h"

#include "../GraphicsCore.h"
#include "../Utility.h"

DecalMgr::DecalMgr()
{
	m_iCurrentID = 0;
	m_iStaticID = 0;
	m_iNumDecals = 0;
	m_iNumStaticDecals = 0;
}

void DecalMgr::Init(UINT iMaxDecals)
{
	Clean();
	m_iCurrentID = 0;
	m_iMaxDecals = iMaxDecals;

	m_DecalCam.SetProj(D3DX_PI*0.25f, 64/64, 0.0f, 50.0f);
}

int DecalMgr::AddDynamicDecal(IDirect3DTexture9* texture, int SharedID, std::string meshName, Vector3 DecalCamPos, Vector3 DecalTarget, bool lockToMesh, float MaxAge, Vector3 up)
{
	if(m_iNumDecals >= m_iMaxDecals)
	{
		if(m_iMaxDecals == 0)
		{
			m_lDynamicDecals.clear();
			return -1;
		}
		for(UINT i = m_iNumDecals; i >= m_iMaxDecals; i--)
		{
			m_lDynamicDecals.pop_front();
			m_iNumDecals--;
		}
	}
	m_iNumDecals++;

	//D3DXVECTOR3 normDir;

	D3DXVECTOR3 L = DecalTarget - DecalCamPos;
	D3DXVec3Normalize(&L, &L);

	//Calculate right vector based on the passed in up and normalized "look"
	D3DXVECTOR3 R;
	D3DXVec3Cross(&R, &up, &L);
	D3DXVec3Normalize(&R, &R);

	//Calculate up vector based on normalized look and right (orthogonal up)
	D3DXVECTOR3 U;
	D3DXVec3Cross(&U, &L, &R);
	D3DXVec3Normalize(&U, &U);

	float x, y, z;

	x = -D3DXVec3Dot(&DecalCamPos, &R);
	y = -D3DXVec3Dot(&DecalCamPos, &U);
	z = -D3DXVec3Dot(&DecalCamPos, &L);

	D3DXMATRIX m;

	m(0, 0) = R.x;
	m(1, 0) = R.y;
	m(2, 0) = R.z;
	m(3, 0) = x;
	m(0, 1) = U.x;
	m(1, 1) = U.y;
	m(2, 1) = U.z;
	m(3, 1) = y;
	m(0, 2) = L.x;
	m(1, 2) = L.y;
	m(2, 2) = L.z;
	m(3, 2) = z;
	m(0,3) = 0.0f;
	m(1,3) = 0.0f;
	m(2,3) = 0.0f;
	m(3,3) = 1.0f;

	//D3DXMatrixLookAtLH(&m, &DecalCamPos, &DecalTarget, &up);

	//Matrix4 ma;

	//Quaternion q(2, 1, 1, 1);
	//ma.SetOrientationAndPos(q, DecalCamPos);

	//D3DXVECTOR3 t;
	//t = DecalTarget - DecalCamPos;
	//D3DXVec3Normalize(&t, &t);
	//q.SetFromYawPitchRoll(t.x, t.y, t.z);
	//ma.

	DynamicDecal decalData;

	//WTW::Camera tempCam;
	//tempCam.SetView(m);
	//tempCam.SetProj(D3DX_PI*0.25f, 64/64, 0.0f, 50.0f);
	//tempCam.Update(0.0f);

	decalData.m_ViewMatrix = m;
	//decalData.m_DecalCam.SetProj(D3DX_PI*0.25f, 64/64, 0.0f, 50.0f);
	//decalData.m_DecalCam.Update(0.0f);
	//decalData.m_DecalCam = tempCam;
	decalData.m_pTexture = texture;
	decalData.m_ID = m_iCurrentID;
	decalData.m_iMaxAge = MaxAge;
	decalData.m_iCurrentAge = 0.0f;
	decalData.m_iSharedID = SharedID;
	decalData.m_sMeshName = meshName;
	decalData.m_bIsLocked = lockToMesh;

	int thisID = m_iCurrentID;

	m_iCurrentID++;
	m_iCurrentID %= 1000000;

	m_lDynamicDecals.push_back(decalData);

	return thisID;
}

int DecalMgr::AddDynamicDecal(IDirect3DTexture9* texture, int SharedID, std::string meshName, D3DXMATRIX matrix, bool lockToMesh, float MaxAge)
{
	if(m_iNumDecals >= m_iMaxDecals)
	{
		if(m_iMaxDecals == 0)
		{
			m_lDynamicDecals.clear();
			return -1;
		}
		for(UINT i = m_iNumDecals; i >= m_iMaxDecals; i++)
		{
			m_lDynamicDecals.pop_front();
			m_iNumDecals--;
		}
	}
	m_iNumDecals++;

	DynamicDecal decalData;

	decalData.m_ViewMatrix = matrix;

	//decalData.m_DecalCam.SetView(matrix);
	//decalData.m_DecalCam.SetProj(D3DX_PI*0.25f, 64/64, 0.0f, 50.0f);
	//decalData.m_DecalCam.Update(0.0f);

	decalData.m_pTexture = texture;
	decalData.m_ID = m_iCurrentID;
	decalData.m_iMaxAge = MaxAge;
	decalData.m_iCurrentAge = 0.0f;
	decalData.m_iSharedID = SharedID;
	decalData.m_sMeshName = meshName;
	decalData.m_bIsLocked = lockToMesh;

	int thisID = m_iCurrentID;

	m_iCurrentID++;
	m_iCurrentID %= 1000000;

	m_lDynamicDecals.push_back(decalData);

	return thisID;
}

int DecalMgr::AddStaticDecal(IDirect3DTexture9* texture, int SharedID, std::string meshName, Vector3 DecalCamPos, Vector3 DecalTarget, bool lockToMesh, Vector3 up)
{
	D3DXVECTOR3 L = DecalTarget - DecalCamPos;
	D3DXVec3Normalize(&L, &L);

	//Calculate right vector based on the passed in up and normalized "look"
	D3DXVECTOR3 R;
	D3DXVec3Cross(&R, &up, &L);
	D3DXVec3Normalize(&R, &R);

	//Calculate up vector based on normalized look and right (orthogonal up)
	D3DXVECTOR3 U;
	D3DXVec3Cross(&U, &L, &R);
	D3DXVec3Normalize(&U, &U);

	float x, y, z;

	x = -D3DXVec3Dot(&DecalCamPos, &R);
	y = -D3DXVec3Dot(&DecalCamPos, &U);
	z = -D3DXVec3Dot(&DecalCamPos, &L);

	D3DXMATRIX m;

	m(0, 0) = R.x;
	m(1, 0) = R.y;
	m(2, 0) = R.z;
	m(3, 0) = x;
	m(0, 1) = U.x;
	m(1, 1) = U.y;
	m(2, 1) = U.z;
	m(3, 1) = y;
	m(0, 2) = L.x;
	m(1, 2) = L.y;
	m(2, 2) = L.z;
	m(3, 2) = z;
	m(0,3) = 0.0f;
	m(1,3) = 0.0f;
	m(2,3) = 0.0f;
	m(3,3) = 1.0f;

	StaticDecal decalData;

	//WTW::Camera tempCam;
	//tempCam.SetView(m);
	//tempCam.SetProj(D3DX_PI*0.25f, 64/64, 0.0f, 50.0f);
	//tempCam.Update(0.0f);

	decalData.m_ViewMatrix = m;
	//decalData.m_DecalCam.SetProj(D3DX_PI*0.25f, 64/64, 0.0f, 50.0f);
	//decalData.m_DecalCam.Update(0.0f);
	//decalData.m_DecalCam = tempCam;
	decalData.m_pTexture = texture;
	decalData.m_ID = m_iCurrentID;
	decalData.m_iSharedID = SharedID;
	decalData.m_sMeshName = meshName;
	decalData.m_bIsLocked = lockToMesh;

	int thisID = m_iStaticID;

	m_iStaticID++;

	m_lStaticDecals.push_back(decalData);

	return thisID;
}

int DecalMgr::AddStaticDecal(IDirect3DTexture9* texture, int SharedID, std::string meshName, D3DXMATRIX matrix, bool lockToMesh)
{
	StaticDecal decalData;

	decalData.m_ViewMatrix = matrix;

	//decalData.m_DecalCam.SetView(matrix);
	//decalData.m_DecalCam.SetProj(D3DX_PI*0.25f, 64/64, 0.0f, 50.0f);
	//decalData.m_DecalCam.Update(0.0f);

	decalData.m_pTexture = texture;
	decalData.m_ID = m_iCurrentID;
	decalData.m_iSharedID = SharedID;
	decalData.m_sMeshName = meshName;
	decalData.m_bIsLocked = lockToMesh;

	int thisID = m_iCurrentID;

	m_iStaticID++;

	m_lStaticDecals.push_back(decalData);

	return thisID;
}

void DecalMgr::RenderAllDecals(float dt)
{
	D3DXMATRIX temp,
				objMatrix;

	D3DXVECTOR3 look;
	DeferredShader* DSX = GCI->GetDeferredR();
	sit = m_lStaticDecals.begin();
	DSX->BeginDecals();
	for(; sit != m_lStaticDecals.end(); sit++)
	{
		temp = sit->m_ViewMatrix;
		look.x = temp(0, 2);
		look.y = temp(1, 2);
		look.z = temp(2, 2);
		//D3DXMatrixTransformation(&temp, NULL, NULL, &D3DXVECTOR3(1, 1, 1), NULL, NULL, &look);
		//lit->m_DecalCam.Update(0.0f);
		//m_DecalCam.SetView(temp);
		//m_DecalCam.Update(0.0f);
		//DSX->RenderDecal(sit->m_pTexture, m_DecalCam.GetViewProj(), m_DecalCam.GetLook());
		DSX->RenderDecal(sit->m_iSharedID, sit->m_sMeshName, sit->m_pTexture, temp * m_DecalCam.GetProj(), look, sit->m_bIsLocked);
		//GCI->
		//sit->m_DecalMesh->Render();
	}
	dit = m_lDynamicDecals.begin();
	std::list<DynamicDecal>::iterator target;
	for(; dit != m_lDynamicDecals.end();)
	{
		target = dit;
		//if age is turned on and the decal is 'dead', delete it and move to the next decal
		if(dit->m_iMaxAge != 0.0f)
		{
			dit->m_iCurrentAge += dt;
			if(dit->m_iCurrentAge > dit->m_iMaxAge)
			{
				dit++;
				m_lDynamicDecals.erase(target);
				m_iNumDecals--;
				continue;
			}
		}
		temp = dit->m_ViewMatrix;
		look.x = temp(0, 2);
		look.y = temp(1, 2);
		look.z = temp(2, 2);
		//lit->m_DecalCam.Update(0.0f);
		//m_DecalCam.SetView(dit->m_ViewMatrix);
		//m_DecalCam.Update(0.0f);
		//dit->m_DecalMesh->Render();
		//DSX->RenderDecal(dit->m_pTexture, m_DecalCam.GetViewProj(), m_DecalCam.GetLook());
		DSX->RenderDecal(dit->m_iSharedID, dit->m_sMeshName, dit->m_pTexture, temp * m_DecalCam.GetProj(), look, dit->m_bIsLocked);
		dit++;
	}
	DSX->EndDecals();
}

//void DecalMgr::RenderDynamicDecals()
//{
//	DeferredShader* DSX = GCI->GetDeferredR();
//	dit = m_lDynamicDecals.begin();
//	DSX->BeginDecals();
//	for(; dit != m_lDynamicDecals.end(); dit++)
//	{
//		//lit->m_DecalCam.Update(0.0f);
//		m_DecalCam.SetView(dit->m_ViewMatrix);
//		m_DecalCam.Update(0.0f);
//		DSX->RenderDecal(dit->m_pTexture, m_DecalCam.GetViewProj(), m_DecalCam.GetLook());
//	}
//	for(; dit != m_lDynamicDecals.end(); dit++)
//	{
//		//lit->m_DecalCam.Update(0.0f);
//		m_DecalCam.SetView(dit->m_ViewMatrix);
//		m_DecalCam.Update(0.0f);
//		DSX->RenderDecal(dit->m_pTexture, m_DecalCam.GetViewProj(), m_DecalCam.GetLook());
//	}
//	DSX->EndDecals();
//}
//
//void DecalMgr::RenderStaticDecals()
//{
//	DeferredShader* DSX = GCI->GetDeferredR();
//	sit = m_lStaticDecals.begin();
//	DSX->BeginDecals();
//	for(; sit != m_lStaticDecals.end(); sit++)
//	{
//		//lit->m_DecalCam.Update(0.0f);
//		m_DecalCam.SetView(sit->m_ViewMatrix);
//		m_DecalCam.Update(0.0f);
//		DSX->RenderDecal(sit->m_pTexture, m_DecalCam.GetViewProj(), m_DecalCam.GetLook());
//	}
//	DSX->EndDecals();
//}
//
//bool DecalMgr::RenderDynamicDecals(UINT DecalID)
//{
//	DeferredShader* DSX = GCI->GetDeferredR();
//	dit = m_lDynamicDecals.begin();
//
//	for(; dit != m_lDynamicDecals.end(); dit++)
//	{
//		if(dit->m_ID == DecalID)
//		{
//			DSX->BeginDecals();
//			//lit->m_DecalCam.Update(0.0f);
//			m_DecalCam.SetView(dit->m_ViewMatrix);
//			DSX->RenderDecal(dit->m_pTexture, m_DecalCam.GetViewProj(), m_DecalCam.GetLook());
//			DSX->EndDecals();
//			return true;
//		}
//	}
//	return false;
//}
//
//bool DecalMgr::RenderStaticDecals(UINT DecalID)
//{
//	DeferredShader* DSX = GCI->GetDeferredR();
//	sit = m_lStaticDecals.begin();
//
//	for(; sit != m_lStaticDecals.end(); sit++)
//	{
//		if(sit->m_ID == DecalID)
//		{
//			DSX->BeginDecals();
//			//lit->m_DecalCam.Update(0.0f);
//			m_DecalCam.SetView(sit->m_ViewMatrix);
//			DSX->RenderDecal(sit->m_pTexture, m_DecalCam.GetViewProj(), m_DecalCam.GetLook());
//			DSX->EndDecals();
//			return true;
//		}
//	}
//	return false;
//}

bool DecalMgr::SetDynamicDecalPosition(UINT DecalID, D3DXVECTOR3 pos)
{
	for(dit = m_lDynamicDecals.begin(); dit != m_lDynamicDecals.end(); dit++)
	{
		if(dit->m_ID == DecalID)
		{
			m_DecalCam.SetView(dit->m_ViewMatrix);
			m_DecalCam.SetPos(pos);
			m_DecalCam.Update(0.0f);
			dit->m_ViewMatrix = m_DecalCam.GetView();
			return true;
		}
	}
	return false;
}

bool DecalMgr::SetStaticDecalPosition(UINT DecalID, D3DXVECTOR3 pos)
{
	for(sit = m_lStaticDecals.begin(); sit != m_lStaticDecals.end(); sit++)
	{
		if(sit->m_ID == DecalID)
		{
			m_DecalCam.SetView(sit->m_ViewMatrix);
			m_DecalCam.SetPos(pos);
			m_DecalCam.Update(0.0f);
			sit->m_ViewMatrix = m_DecalCam.GetView();
			return true;
		}
	}
	return false;
}

bool DecalMgr::RemoveDynamicDecal(int DecalID)
{
	for(dit = m_lDynamicDecals.begin(); dit != m_lDynamicDecals.end(); dit++)
	{
		if(dit->m_ID == DecalID)
		{
			m_lDynamicDecals.erase(dit);
			m_iNumDecals = m_lDynamicDecals.size();
			return true;
		}
	}
	return false;
}

bool DecalMgr::RemoveStaticDecal(int DecalID)
{
	for(sit = m_lStaticDecals.begin(); sit != m_lStaticDecals.end(); sit++)
	{
		if(sit->m_ID == DecalID)
		{
			m_lStaticDecals.erase(sit);
			m_iNumStaticDecals = m_lStaticDecals.size();
			return true;
		}
	}
	return false;
}

void DecalMgr::RemoveDecalsByMeshName(std::string MeshName)
{
	std::list<StaticDecal>::iterator sTarget;
	for(sit = m_lStaticDecals.begin(); sit != m_lStaticDecals.end();)
	{
		if(sit->m_sMeshName == MeshName)
		{
			sTarget = sit;
			sit++;
			m_lStaticDecals.erase(sTarget);
			continue;
		}
		sit++;
	}
	std::list<DynamicDecal>::iterator dTarget;
	for(dit = m_lDynamicDecals.begin(); dit != m_lDynamicDecals.end();)
	{
		if(dit->m_sMeshName == MeshName)
		{
			dTarget = dit;
			dit++;
			m_lDynamicDecals.erase(dTarget);
			continue;
		}
		dit++;
	}
}

void DecalMgr::Clean()
{
	m_lDynamicDecals.clear();
	m_lStaticDecals.clear();
	m_iNumDecals = 0;
	m_iNumStaticDecals = 0;
}