#pragma once

#define LIGHT_DEMO		1
#if LIGHT_DEMO
//#define NUM_LIGHTS		5
#else
#//define NUM_LIGHTS		2
#endif

#include <queue>
#include "LensGlare.h"
//#define	NUM_LIGHTS		5
#define MAX_SAMPLES		16
#define	EMISSIVE_COEFF	39.78f	// Emissive color multiplier
#define NUM_TONEMAP_TEX	4		// Number of tone-map textures
#define NUM_BLOOM_TEX	3
#define NUM_STAR_TEX	12

class DeferredShader;	// Forward decleration

struct DynamicLight
{
	int			m_iID;
	bool		m_bUse;
	D3DXVECTOR4	m_vPos;
	D3DXVECTOR4	m_vIntensity;
	int			m_iLogIntensity;
	int			m_iMantissa;
};

class HDR_Light
{
	DeferredShader* DSX;	// Direct pointer to the DeferredShader
	ID3DXEffect*	m_pFX;
	float	GaussDistribution(float x, float y, float rho);
	void	CalculateAdaptation();
	void	MeasureLuminance();

	// Calculate Offsets
	void	GetDown4x4Offsets(float width, float height, D3DXVECTOR2 sampleOffsets[]);	// Down scale 4x4
	void	GetDown2x2Offsets(float width, float height, D3DXVECTOR2 sampleOffsets[]);	// Down scale 2x2
	void	GetGaussOffsets(float width, float height, D3DXVECTOR2* pTexCoordOffset, 
							D3DXVECTOR4* pSampleWeight, float multi = 1.0f);
	void	GetBloomOffsets(float texSize, float texCoordOffset[15], D3DXVECTOR4* colorWeight,
							float deviation, float multi = 1.0f);
	void	GetStarOffsets(float texSize, float texCoordOffset[15], D3DXVECTOR4* colorWeight,
							float deviation);

	// Post-Processing Stages
	void	SceneToScale();
	void	ScaleToBright();
	void	BrightToStar();
	void	StarToBloom();

	// Render Stages
	void	RenderScene();
	void	RenderBloom();
	void	RenderStar();

	// Render Surfaces
	RenderSurface	m_pHDRsurface;				// HDR Render target
	RenderSurface	m_pScaledSurface;
	RenderSurface	m_pAdaptedLumPrevSurface;	// Previous adaptive luminance surface
	RenderSurface	m_pAdaptedLumCurSurface;	// Current adaptive luminance surface
	RenderSurface	m_pBloomSurface;
	RenderSurface	m_pBrightSurface;			// Bright pass surface
	RenderSurface	m_pStarSurface;

	IDirect3DTexture9*	m_pBloomTex[NUM_BLOOM_TEX];
	IDirect3DTexture9*	m_pStarTex[NUM_STAR_TEX];
	IDirect3DTexture9*	m_pToneMapTex[NUM_TONEMAP_TEX];
public:
	// Properties
	LensGlare		m_GlareDef;
	EGlareType		m_eGlareType;
	//D3DXVECTOR4		m_LightPos[NUM_LIGHTS];
	//D3DXVECTOR4		m_LightIntensity[NUM_LIGHTS];
	//int				m_LightLogIntensity[NUM_LIGHTS];
	//int				m_LightMantissa[NUM_LIGHTS];
	bool						m_bNeedToRebuildLightPos;
	bool						m_bNeedToRebuildLights;
	std::vector<DynamicLight>	m_DynamicLights;
	std::queue<int>				m_iAvailableIDs;
	
	std::vector<D3DXVECTOR4>	m_LightPos;
	std::vector<D3DXVECTOR4>	m_LightPosView;
	std::vector<D3DXVECTOR4>	m_LightIntensity;
	std::vector<int>			m_LightLogIntensity;
	std::vector<int>			m_LightMantissa;
	float			m_CropWidth;
	float			m_CropHeight;
	bool			m_bUseToneMap;	// Is tone-mapping enabled?
	D3DFORMAT		m_LumFormat;
	
public:
	HDR_Light();
	~HDR_Light();
	void	PreRestartDevice();
	void	PostRestartDevice();

	void	Init();
	void	Render();
	void	Shutdown();
	void	ReloadShader();

	//RenderSurface	LDR;
	int		AddLight(D3DXVECTOR4 pos, D3DXVECTOR4 intensity, int power);
	void	ChangeLightPos(int light, D3DXVECTOR4 pos);
	void	ChangeLightPower(int id, int power);
	void	RemoveLight(int light);
	void	RemoveAllLights();
	void	RebuildShaderArrays();

	void	AdjustLight(int light, bool increment);
	void	RefreshLights();

	void	SwitchToneMap();
	void	SetGlareType(EGlareType glareType);
};