#pragma once

#include "../3D/VertexDecl.h"
#include "../SceneGraph/ObjectList.h"

// You only need one instance of this class
// Just pass in different transforms into 
// PickingChecks() to perform collision checks
class QuadPicking
{
	ID3DXMesh* m_pMesh;
	D3DXVECTOR3 m_originW;
	D3DXVECTOR3 m_dirW;
	void	CreatePickingRayW(D3DXVECTOR3& originW, D3DXVECTOR3& dirW);
	
	// Selecting mesh
	float			m_fSelectedDist;
	int				m_iSelectedObjID;
	std::string		m_cSelectedObjName;
	
public:
	QuadPicking();
	~QuadPicking();

	void	Init();
	void	Shutdown();
	
	void	UpdateRay();
	bool	PickingChecks(D3DXMATRIX transform);
	bool	PickingPos(ID3DXMesh** mesh, D3DXVECTOR3* pos);
	bool	PickingSelect(ID3DXMesh** mesh, D3DXMATRIX transform, 
							std::string objName, int objID);

	// Selecting object tools
	void	GetSelectedObj(std::string& objName, int& objID);

};