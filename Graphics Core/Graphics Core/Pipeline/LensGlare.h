#pragma once

#include "StarLibrary.h"

enum EGlareType{
	GLARE_DISABLE = 0,

	GLARE_CAMERA,
	GLARE_NATURAL,
	GLARE_CHEAPLENS,
	GLARE_AFTERIMAGE, //
	GLARE_FILTER_CROSSCREEN,
	GLARE_FILTER_CROSSCREEN_SPECTRAL,
	GLARE_FILTER_SNOWCROSS,
	GLARE_FILTER_SNOWCROSS_SPECTRAL,
	GLARE_FILTER_SUNNYCROSS,
	GLARE_FILTER_SUNNYCROSS_SPECTRAL,
	GLARE_CINEMACAM_VERTICALSLITS,
	GLARE_CINEMACAM_HORIZONTALSLITS,
	
	NUM_GLARE_TYPES,
	GLARE_USERDEF = -1,
	GLARE_DEFAULT = GLARE_FILTER_CROSSCREEN,
};

// Basic glare definition
struct GlareDef
{
	char*	m_GlareName;
	float	m_fGlareLum;

	float	m_fBloomLum;
	float	m_fGhostLum;
	float	m_fGhostDistortion;

	float		m_fStarLum;
	EStarType	m_eStarType;
	float		m_fStarInclination;

	float	m_fChromaticAberration;

	float	m_fAfterImageSensitivity;	// Current weight
	float	m_fAfterImageRatio;			// Afterimage weight
	float	m_fAfterImageLum;
};


/********************************************
			 Lens Glare Library
*********************************************/
class LensGlare
{
public:
	char	m_GlareName[256];

	float	m_fGlareLum;	// Total glare intensity (not effect to "after image")
	float	m_fBloomLum;
	float	m_fGhostLum;
	float	m_fGhostDistortion;
	float	m_fStarLum;
	float	m_fStarInclination;

	float	m_fChromaticAberration;

	float	m_fAfterImageSensitivity;	// Current weight
	float	m_fAfterImageRatio;			// Afterimage weight
	float	m_fAfterImageLum;

	StarLibrary	m_StarDef;

private:
	static LensGlare*	s_GlareLibrary;

public:
	LensGlare();
	LensGlare(const LensGlare& copy);
	~LensGlare();

	LensGlare& operator= (const LensGlare& rhs);

	void	Create();
	void	Destroy();
	void	Release();

	void	Init(const LensGlare& add);
	void	Init(const TCHAR* glareName,
				 float glareLum,
				 float bloomLum,
				 float ghostLum,
				 float ghostDistortion,
				 float starLum,
				 EStarType type,
				 float starInclination,
				 float chromaticAberration,
				 float afterimageSensitivity,	// Current weight
				 float afterimageRatio,			// Afterimage weight
				 float afterimageLum);
	void	Init(const GlareDef& glareDef);
	void	Init(EGlareType type);

public:
	// Static Library Methods
	static void	InitStaticGlareLibs();
	static void	DeleteStaticGlareLibs();
	static const LensGlare&	GetGlareLib(EGlareType type);
};

class StaticLight
{
public:
	StaticLight()
	{
		StarLibrary::InitStaticLibrary();
		LensGlare::InitStaticGlareLibs();
	}
	~StaticLight()
	{
		LensGlare::DeleteStaticGlareLibs();
		StarLibrary::DeleteStaticLibrary();
	}

	static StaticLight	s_StaticLight;
};