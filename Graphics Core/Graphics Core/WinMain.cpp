
#include "MacroTools.h"
#include "BaseApp.h"
#include <time.h>


//////////////////////////////////////////////////////////////////////////
// Win Main			!!!!!!!!-DO-NOT-CHANGE-!!!!!!!!
//////////////////////////////////////////////////////////////////////////
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PTSTR pCmdLine, int nCmdShow )
{	
	// Enable Memory Leak Detection (in Debug mode)
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
		
		//Uncomment to break on the Xth memory allocation.
		//_CrtSetBreakAlloc(298543);
	#endif

	// Use this msg structure to catch window messages
	MSG msg; 
	ZeroMemory(&msg, sizeof(msg));

	//*************************************************************************
	// Initialize DirectX/Game here (call the Init method of your framwork)
	BaseApp testApp;
	testApp.Init(hInstance, "Graphics Core: Test Bed");
		
	//***********************************************************************
	// Check the Counts per Second 
	_int64 countsPS = 0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPS);
	float SecondsPC = 1.0f/(float)countsPS;

	// Set up previous time for TimeElapsed
	_int64 prevTime = 0; 
	_int64 currTime = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&prevTime);
	//**************************************************************************
	// Seed the Random number
	srand(time(NULL));

	// Main Windows/Game Loop
	while(msg.message != WM_QUIT)
	{
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//*************************************************************************
		// Calulate the Time Elapsed
		currTime = 0;											// Clear the previous "current time"
		QueryPerformanceCounter((LARGE_INTEGER*)&currTime);		// Set new current time
		float TimeElapsed = (currTime - prevTime)*SecondsPC;	// Set the time elapsed between frames
		prevTime = currTime;									// Set previous time for next frame as this frame's "current time"

		//*************************************************************************
		// Main Game Loop
		testApp.Update( TimeElapsed );
		testApp.Render();
	}
	//*************************************************************************
	//Shutdown DirectXFramework/Game here
	testApp.Shutdown();

	//*************************************************************************
	// Return successful
	return msg.wParam;	// Return exit code
}

