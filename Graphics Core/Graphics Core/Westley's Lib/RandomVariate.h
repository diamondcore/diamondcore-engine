#pragma once

#define RVI	RandomVariate::Instance()

class RandomVariate
{
	//// Linear Congruential Generator variables
	//long double m_dZ,	// Seed
	//			m_dM,	// Modulus
	//			m_dA,	// Multiplier
	//			m_dC;	// Increment

	// Linear Congruential Generator variables v2.0
	unsigned long long	m_dZ,	// Seed
						m_dM,	// Modulus
						m_dA,	// Multiplier
						m_dC;	// Increment

	static RandomVariate*	s_Instance;
	RandomVariate(long double z, long double m, long double a, long double c);
	RandomVariate();

public:
	static RandomVariate* Instance();
	~RandomVariate();
	void   Shutdown();

	// Changing these requires object restart
	void setZ(long double z);	// Seed
	void setM(long double m);	// Modulus
	void setA(long double a);	// Multiplier
	void setC(long double c);	// Increment

	long double getZ();
	long double getM();
	long double getA();
	long double getC();

	// Auto Gen next iteration
	long double GetNextLCG();			// Increments LCG values
	long double GetNextUi();			// Increments LCG value
	long double GetCurrentUi();			// Gets the current Ui for graphing purposes
	long double Phi();						// Standard Golden Ratio
	long double Phi(double a, double b);	// Golden Ratio

	long double LCG(int n);				// Zi! to Zn! 
	long double Ui(int iter);			// Random Number (0, 1)
	long double Zi(double a, double b);	// Get Zi from the LCG

	long double Unif(long double a, long double b, int iter);		// Uniform distribution b > a
	long double Expon(double a, double b, int iter);				// Mean of Exponential
	//long double ExponO(double t, double m, int b, int iter);		// Old Exponential
	long double Tri(double xMin, double xMax, double c, int iter);	// Triangular Probality
	long double Weib(double a, double b, double c, int iter);		// Weibull density function
	long double Normal(double mu, double sig, int iter);			// Normal(0,1)
	//long double NormalAR(double mu, double sig, int iter);			// Normal Acceptance-Regection
	//int	   getAR();													// Returns the next AR iterator/

	// Auto Get next iteration 
	long double Unif(long double a, long double b);
	long double Expon(double a, double b);							// Exponential density function
	//long double ExponO(double t, double m, int b);				// Old Exponential
	long double ExponF(double b);									// Exponential distribution function
	long double Tri(double xMin, double xMax, double c);
	long double Pois(long double mu);								// Poisson distribution function
	long double WeibF(double a, double b, double c);				// Weibull distribution function
	long double Weib(double a, double b, double c);					// Weibull density function
	long double Normal(double mu, double sig);
	long double NormalAR(double mu, double sig);
	long double getX();

private:
	//long double		m_PrevZ;									// Previous Zi-1 for const counter
	unsigned long long	m_PrevZ;
	long double		Factorial(long double a);					// Factorial of a!
};


// Quick Substitution to get rid of the previous poor random number generator
inline float GetRandomFloat(float a, float b)
{
	if(a == b)
		return a;
	return static_cast<float>(RVI->Unif(a, b));
	/*float n = RVI->Normal(a, b);
	if(n > b)
		return (int)n % (int)(b+1);
	return n;*/
	/*if(a >= b)
		return a;
	float n = (rand()%10001)*0.0001f;
	return (n*(b-a))+a;	*/
}