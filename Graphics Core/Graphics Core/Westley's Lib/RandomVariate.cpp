#include "RandomVariate.h"
//#include <cmath>
#include <math.h>
#include <assert.h>
#include <time.h>
#include "../MacroTools.h"

RandomVariate* RandomVariate::s_Instance = 0;
RandomVariate* RandomVariate::Instance()
{
	if(!s_Instance)
		s_Instance = DEBUG_NEW RandomVariate(87+time(NULL), 2147483647, 16807, 0);
		//s_Instance = new RandomVariate;	// Debug Version
	return s_Instance;
}

// (A < M), C ,(Zo < M)
RandomVariate::RandomVariate(long double z, long double m, long double a, long double c)
	:m_dZ(z), m_dM(m), m_dA(a), m_dC(c)
{
	m_PrevZ = m_dZ;	// Initialize the first Zo
}
RandomVariate::RandomVariate()		// Default is set to best Choice LCG
	//:m_dZ(7), m_dM(160000000), m_dA(5), m_dC(3)
	:m_dZ(87), m_dM(2147483647), m_dA(16807), m_dC(0)
{
	m_PrevZ = m_dZ;	// Initialize the first Zo
}
RandomVariate::~RandomVariate()
{}
void RandomVariate::Shutdown()
{
	delete s_Instance;
	s_Instance = 0;
}

// Set Member Variables
void RandomVariate::setZ(long double z)	// Seed
{	m_dZ = z;	m_PrevZ = z;}			// Resets m_PrevZ counter
void RandomVariate::setM(long double m)	// Modulus
{	m_dM = m;	}
void RandomVariate::setA(long double a)	// Multiplier
{	m_dA = a;	}
void RandomVariate::setC(long double c)	// Increment
{	m_dC = c;	}

// Get Member Variables
long double RandomVariate::getZ()	// Seed
{	return m_dZ;	}
long double RandomVariate::getM()	// Modulus
{	return m_dM;	}
long double RandomVariate::getA()	// Multiplier
{	return m_dA;	}
long double RandomVariate::getC()	// Increment
{	return m_dC;	}

long double RandomVariate::Factorial(long double a)	// Factorial of a!
{
	if (a > 1)
		return (a * Factorial(a-1));
	else
		return (1);
}


/*******************************************
		Linear Congruential Generator
********************************************/
long double RandomVariate::GetNextLCG()
{
#if 0
	// Calculate the next LCG
	m_PrevZ = (int)(m_dA * m_PrevZ + m_dC) % (int)m_dM;
	return m_PrevZ;
#endif

#if 1
	m_PrevZ = (m_dA * m_PrevZ + m_dC) % m_dM;

	return m_PrevZ;

#endif
}

long double RandomVariate::GetNextUi()
{
	return GetNextLCG() / m_dM;
}

long double RandomVariate::GetCurrentUi()
{
	return m_PrevZ / m_dM;
}

long double RandomVariate::Phi()
{
	return 1.618033987;	// Golden Ratio
}

long double RandomVariate::Phi(double a, double b)
{
	assert(a > b);
	return (a+b)/a;	// Calculate the Golden Ratio
}

long double RandomVariate::LCG(int n)	// Zi! to Zn!
{
	if (n > 0)
		return (int)(m_dA * LCG(n-1) + m_dC) % (int)m_dM;
	else if (n == 0)
		return (m_dZ);	// Starting Z
	else
		return 1;
}

long double RandomVariate::Ui(int iter)
{
	return LCG(iter) / m_dM;
}

long double RandomVariate::Zi(double a, double b)
{
	return (LCG(a)/m_dM) * (b-a) + a;
}


/*******************************************
		Random Variate Generators
********************************************/
/* Distribute RandomVariate as... */
long double RandomVariate::Unif(long double a, long double b, int iter)	// Uniform distribution b > a
{
	assert(b > a);
	return (Ui(iter)) * (b - a) + a;	// (Ui = Zi / m)*(b - a) + a
}

long double RandomVariate::Unif(long double a, long double b)
{
	assert(b > a);
/*	long double x = GetNextUi();
	if(x < a)
		return 0;
	else if (x > b)
		return 1;
	else
		return ((x - a)/(b - a));
*/	return (GetNextUi()) * (b - a) + a;
}


// t = the location where/when they start to fail
// m = (Mean of the Random Variates, scale parameter b > 0)
// b = total amount of iterations
// iter = current iteraion
//long double RandomVariate::ExponO(double t, double m, int b, int iter)			// Old Exponential
//{
//	assert(m > 0);
//	return  t - m *log(Zi(iter + 1, b));
//}
//long double RandomVariate::ExponO(double t, double m, int b)					// Old Exponential
//{
//	assert(m > 0);
//	return t - m *log(Unif(t, b));
//}


// a = (Location parameter (mode), any real number)
// b = (Mean of the Random Variates, scale parameter b > 0)
long double RandomVariate::Expon(double a, double b, int iter)					// Exponential
{
	assert(b > 0);
	return (a - b)*(log( Unif(0.0, 1.0, iter) ));
}

long double RandomVariate::Expon(double a, double b)
{
	assert(b > 0);
	return (a - b)*(log( Unif(0.0, 1.0)));
}

long double RandomVariate::ExponF(double b)
{
	assert(b > 0);
	return -b * log( Unif(0.0, 1.0) );	// -lamda * ln(u)



}

// Probability of event happening k times, when average is mu times
// Event happening k times
// Average is mu
long double RandomVariate::Pois(long double mu)
{
	assert(mu > 0.00000);
	double p = 1.0;
	int k;
	for (k = 0; p >= exp(-mu); k++)
		p *= Unif(0.0, 1.0);
	return k - 1;
}

// (xMin <= x <= C) to (C <= x <= xMax)
long double RandomVariate::Tri(double xMin, double xMax, double C, int iter)	// Trianglular P(x)
{
	assert(xMin < xMax && xMin <= C && C <= xMax);				// C is where the Mode will be
	double p = Unif(0.0, 1.0, iter), q = 1.0 - p;

	if (p <= (C - xMin) / (xMax - xMin))
		return xMin + sqrt((xMax - xMin)*(C - xMin)*p);
	else
		return xMax - sqrt((xMax - xMin)*(xMax - C)*q);
}

long double RandomVariate::Tri(double xMin, double xMax, double C)
{
	assert(xMin < xMax && xMin <= C && C <= xMax);
	double p = Unif(0.0, 1.0), q = 1.0 - p;

	if (p <= (C - xMin) / (xMax - xMin))
		return xMin + sqrt((xMax - xMin)*(C - xMin)*p);
	else
		return xMax - sqrt((xMax - xMin)*(xMax - C)*q);
}

// a (location), b (should be scaled greater then 0), c (shape greater then 0)
// when c == 1, it becomes the exponential function with scale b
long double RandomVariate::Weib(double a, double b, double c, int iter)		// Weibull Probability
{
	assert(b > 0.0 && c > 0.0);
	return a + b * pow((long double)-log(Ui(iter)), (long double)1.0/c);
}

long double RandomVariate::Weib(double a, double b, double c)
{
	assert(b > 0.0 && c > 0.0);
	return a + b * pow((long double)-log(GetNextUi()), (long double)1.0/c);
}

long double RandomVariate::WeibF(double a, double b, double c)
{
	assert(b > 0.0 && c > 0.0);
	long double x = Unif(0.0, 1.0);
	if (x > a)
		return 1.0 - exp(-pow((long double)( (x-a)/b ), (long double)c));
	else
		return 0;
}

// mu(0.0) sig(1.0)
long double RandomVariate::Normal(double mu, double sig, int iter)		// Normal
{
	double y, u, x;
	y = Expon(0.0, 1.0, iter);
	u = Unif(0.0, 1.0, iter);

	if(u <= -((pow(y-1.0, 2))/2.0))
		x = y;
	//else
		//Normal();

	if( u <= 0.5)
		return x;
	else
		return -x;
}

long double RandomVariate::Normal(double mu, double sig)
{
	///////////////////////
	// VARIATE EDIT
	//////////////////////
	assert( sig > 0. );
	double p, p1, p2;
	do {
		p1 = Unif( -1, 1 );
		p2 = Unif( -1, 1 );
		p = p1 * p1 + p2 * p2;
	} while ( p >= 1. );
	double returnVal = mu + sig * p1 * sqrt( -2. * log( p ) / p );

	if (returnVal < 0.0f)
	{
		return returnVal *-1;
	}

	return returnVal;
	
	
	//long double y, u, x;

	//y = Expon(0.0, 1.0);
	//u =  (GetCurrentUi()) * (1.0 - 0.0) + 0.0;	//m_PrevZ;	// Uses the same random number as y

	//if(u <= -((pow(y-1.0, 2))/2.0))
	//	x = y;
	////else
	//	//return Normal(mu, sig);

	//if( u <= 0.5)
	//	return x;
	//else
	//	return -x;
}

// mu(0.0)	sig(1.0)
//long double RandomVariate::NormalAR(double mu, double sig)		// Normal
//{
//	assert(sig > 0);
//	double p, px, py;
//
//	px = Unif(-1.0, 1.0);
//	py = Unif(-1.0, 1.0);
//	p = (px*px) + (py*py);
//	if(p >= 1.0)
//		return NormalAR(mu, sig);
//	
//	return mu + sig * px * sqrt(-2.0*log(p)/p);
//}

// mu(0.0) sig(1.0)
//static int AR = -1;
//long double RandomVariate::NormalAR(double mu, double sig, int iter)		// Normal Acceptance-Rejection
//{
//	double y, u, x, v, prob;
//	y = Expon(0.0, 1.0, ++AR);	// Point in the shape of the graph
//	u = Unif(0.0, 1.0, AR);	// Random probability value
//
//	if(AR > 3200)	// Iterate Random Number
//		AR = 0;	//%= 3200;
//	else
//		++AR;
//
//	prob = exp(-(pow(y-1.0, 2)/2.0));	// Probability of Acceptance
//	if(u <= prob)						// Check if accepted
//		x = y;
//	else
//		x = NormalAR(mu, sig, iter);
//
//	v = Unif(0.0, 1.0, ++AR);
//	if( v <= 0.5)
//		return x;
//	else
//		return -x;
//}
//int RandomVariate::getAR()
//{	return ++AR;	}

long double tempX;
long double RandomVariate::NormalAR(double mu, double sig)
{
	long double y, u, x, v, prob;
	y = -log(Unif(0.0, 1.0));	//Expon(0.0, 1.0);
	//u =  (GetCurrentUi()) * (1.0 - 0.0) + 0.0;
	u = Unif(0.0, 1.0);
	v = Unif(0.0, 1.0);

	prob = exp(-pow(y-1.0, 2)/2.0);
	if(u <= prob)
		x = y;
	else
		return NormalAR(mu, sig);

	tempX = u;	// Stores X for displaying curve

	if(v <= 0.5)
		return x;
	else
		return -x;
}
long double RandomVariate::getX()
{	return tempX;	}
