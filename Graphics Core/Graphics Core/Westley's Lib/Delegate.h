#pragma once

#include <iostream>
namespace WTW
{
	/**************************************
			Method Pointer Template
	***************************************/
	template<class ReturnType, class ObjectType>
	class Delegate
	{
	private:
		ReturnType	(ObjectType::*m_FPointer)();	// Pointer to the linked Method
		ObjectType*	m_DelegateInst;							// Instance of the ObjectType

	public:
		Delegate();
		Delegate( ObjectType* ObjInst, 
				  ReturnType (ObjectType::*FuncP)());
		~Delegate(){}

		// Connect to Delegate instance and it's Method
		void connectToLink( ObjectType* ObjInst, 
							ReturnType (ObjectType::*FuncP)());

		ReturnType operator()();	// Runs the Method Pointer 
	};


	template<class ReturnType, class ObjectType>
	Delegate<ReturnType, ObjectType>::Delegate()
	{
		m_FPointer = NULL;
		//m_DelageInst = NULL;
	}

	template<class ReturnType, class ObjectType>
	Delegate<ReturnType, ObjectType>::Delegate(ObjectType* ObjInst, 
											  ReturnType (ObjectType::*FuncP)())
	{
		m_DelegateInst	= ObjInst;
		m_FPointer		= FuncP;
	}

	template<class ReturnType, class ObjectType>
	void Delegate<ReturnType, ObjectType>::connectToLink(ObjectType* ObjInst, 
														ReturnType (ObjectType::*FuncP)())
	{
		m_DelegateInst	= ObjInst;
		m_FPointer		= FuncP;
	}

	template<class ReturnType, class ObjectType> 
	ReturnType Delegate<ReturnType, ObjectType>::operator()()
	{
		//if(!m_DelateInst)
			//throw new std::string("Delegate needs instance of Object.");
		if(!m_FPointer)
			throw new std::string("Delegate needs method to run.");

		return (m_DelegateInst->*m_FPointer)();
	}
}