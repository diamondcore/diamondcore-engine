#pragma once

#include <sstream>
#include <Windows.h>

#define dout (*DebugOutput::Instance())
class DebugOutput
{
	std::stringstream	m_DStream;	// Output Stream
	HWND				m_hWnd, hEdit;
	HINSTANCE			m_hInst;
	CRITICAL_SECTION	cs_Lock;

	void	Init(std::string title);

	static DebugOutput*	s_Instance;
	DebugOutput();

public:
	static DebugOutput*	Instance();
	~DebugOutput();
	void	Shutdown();

	template<class T>	DebugOutput& operator<< (const T &rhs)
	{
		// Take into stream, so that all data types can be input into a string format
		m_DStream << rhs;
		DebugOutput::s_DStream.str("");
		DebugOutput::s_DStream << ParseEndl(m_DStream);
		
		// Display in the window
		DebugOutput::s_hEdit = hEdit;
		SetWindowText(DebugOutput::s_hEdit, DebugOutput::s_DStream.str().c_str()); 
		SendMessage(DebugOutput::s_hEdit, WM_VSCROLL, SB_BOTTOM, NULL);
		return *this;
	}

	DebugOutput& operator<< (std::ostream& (*funcP)(std::ostream&))
	{
		funcP(s_DStream);
		return *this;
	}
	void	clear();

	static std::stringstream	s_DStream;
	static HWND					s_hEdit;
	static bool					s_bSetupFlag;
	static bool					SetupFlag()
	{
		s_DStream.str("");
		s_hEdit = 0;
		return true;
	}

private:
	std::string ParseEndl(std::stringstream& stream);

	static LRESULT CALLBACK sDoutProc(HWND, UINT, WPARAM, LPARAM);	// Static forwarder to non-static callback
	LRESULT CALLBACK DoutProc(HWND, UINT, WPARAM, LPARAM);			// Non-static callback
};