#include "Transform.h"
#include "../MacroTools.h"

namespace WTW
{
	/********************************************
					Transform
	*********************************************/
	Transform::Transform()
		:m_bHasChanged(true),
		 m_bUpdateInv(true),
		 m_bUseQuat(false),
		 m_bNewOffset(false)
	{
		// Setup Properties
		m_Scale.ReadWrite(this, &Transform::GetScale, &Transform::SetScale);
		m_Rotation.ReadWrite(this, &Transform::GetRot, &Transform::SetRot);
		m_Quaternion.ReadWrite(this, &Transform::GetQuat, &Transform::SetQuat);
		m_Position.ReadWrite(this, &Transform::GetPos, &Transform::SetPos);

		// Initialize values
		m_Position	= D3DXVECTOR3(0, 0, 0);
		m_Rotation	= D3DXVECTOR3(0, 0, 0);
		m_Scale		= D3DXVECTOR3(1, 1, 1);
		m_RotQuat = D3DXQUATERNION(0, 0, 0, 0);
		m_pOffset = new D3DXMATRIX;
		D3DXMatrixIdentity(m_pOffset);
		m_bNewOffset = true;
	}
	Transform::~Transform()
	{
		if(m_bNewOffset)
			SAFE_DELETE(m_pOffset);	// If created then delete
	}
	
	void Transform::SetOffset(D3DXVECTOR3 offset)	// Set World Offset
	{	
		if(!m_bNewOffset)
			m_pOffset = new D3DXMATRIX;
		D3DXMatrixTranslation(m_pOffset, offset.x, offset.y, offset.z);	// (-width/2, -height/2, 0.0f) for Sprites
		m_bHasChanged = true;
		m_bUpdateInv = true;	// Need to Update
		m_bNewOffset = true;
	}
	void Transform::SetOffset(D3DXMATRIX* offset)
	{
		if(m_bNewOffset)
			SAFE_DELETE(m_pOffset);	// Delete any Matrixs stored here
		m_pOffset = offset;
		m_bUpdateInv = true;	// Need to Update
		m_bHasChanged = true;
		m_bNewOffset = false;	// This is not a new Matrix
	}

	// Scale
	void Transform::SetScale(D3DXVECTOR3 scale)
	{	
		// Fix any negative scaling to positive
		if(scale.x < 0.0f)
			scale.x *= -1.0f;
		if(scale.y < 0.0f)
			scale.y *= -1.0f;
		if(scale.z < 0.0f)
			scale.z *= -1.0f;

		// Set Final Scale
		m_ScaleVec = scale;	
		m_bUpdateInv = true;	// Need to Update
		m_bHasChanged = true;
	}
	D3DXVECTOR3 Transform::GetScale()
	{	return m_ScaleVec;	}

	// Rotation
	void Transform::SetRot(D3DXVECTOR3 deg3D)
	{
		if(deg3D.x > 360.0f)	// Cap X
			deg3D.x -= 360.0f;	// * (int)(deg3D.x/360.0f);
		if(deg3D.y > 360.0f)	// Cap Y
			deg3D.y -= 360.0f;	// * (int)(deg3D.y/360.0f);
		if(deg3D.z > 360.0f)	// Cap Z
			deg3D.z -= 360.0f;	// * (int)(deg3D.z/360.0f);

		// Set Final Rotation
		m_RotVec = deg3D;
		m_bUseQuat = false;

		if(!m_bUseQuat)	// Convert degrees to radians
		{
			m_RotVec.x = (m_RotVec.x*D3DX_PI)/180.0f;
			m_RotVec.y = (m_RotVec.y*D3DX_PI)/180.0f;
			m_RotVec.z = (m_RotVec.z*D3DX_PI)/180.0f;
		}

		// Use Quaternion Rotation
		//D3DXQuaternionRotationYawPitchRoll(&m_RotQuat, deg3D.x, deg3D.y, deg3D.z);
		//m_bUseQuat = true;
		m_bUpdateInv = true;	// Need to Update
		m_bHasChanged = true;
	}
	D3DXVECTOR3 Transform::GetRot()
	{	return m_RotVec;	}

	// Quaternion
	void	Transform::SetQuat(D3DXQUATERNION rot)
	{
		m_RotQuat	= rot;
		m_bUseQuat	= true;
		m_bUpdateInv = true;	// Need to Update
		m_bHasChanged = true;
	}
	D3DXQUATERNION	Transform::GetQuat()
	{	return m_RotQuat;	}

	// Position
	void Transform::SetPos(D3DXVECTOR3 pos)
	{
		m_PosVec = pos;
		m_bUpdateInv = true;	// Need to Update
		m_bHasChanged = true;
	}
	D3DXVECTOR3 Transform::GetPos()
	{	return m_PosVec;	}


	// Transform
	D3DXMATRIX& Transform::GetTransform()
	{
		if(!m_bHasChanged)		// No changes have been made
			return m_Transform;
		else					// Update changes
		{
			D3DXMATRIX W, S, R, T;
			//D3DXMatrixTranslation(&W, m_Offset.x, m_Offset.y, m_Offset.z);		// World Offset
			D3DXMatrixScaling(&S, m_ScaleVec.x, m_ScaleVec.y, m_ScaleVec.z);		// Scale
			S = *m_pOffset * S;	// S = W * S;

			if(m_bUseQuat)	// Check whether to use Quaterions or Rotation Vectors
				D3DXMatrixRotationQuaternion(&R, &m_RotQuat);						// Quaternions
			else
				D3DXMatrixRotationYawPitchRoll(&R, m_RotVec.y, m_RotVec.x, m_RotVec.z);	// Rotation along the axis

			D3DXMatrixTranslation(&T, m_PosVec.x, m_PosVec.y, m_PosVec.z);			// Translation
			m_Transform = (S*R)*T;
			//m_Transform = *m_pOffset * ((S*R)*T);

			m_bHasChanged = false;	// Updated
			return m_Transform;		// Return the new Transform
		}
	}
	
	D3DXMATRIX& Transform::GetInverseTrans()
	{
		if(!m_bUpdateInv)
			return m_InverseTrans;	// No changes
		else				// Need to Update Changes
		{
			if(m_bHasChanged)
				GetTransform();	// Update Changes
			
			// Calculate the Inverse Transform
			D3DXMatrixInverse(&m_InverseTrans, 0, &m_Transform);
			D3DXMatrixTranspose(&m_InverseTrans, &m_InverseTrans);	// Not for sure if Needed?
			
			m_bUpdateInv = false;	// Updated
			return m_InverseTrans;
		}
	}
}