#ifndef	ATTRIBUTE_H
#define	ATTRIBUTE_H

#include <iostream>
namespace WTW
{
	/****************************************************
					Attribute Properties:
		Can be used to set and get private DataTypes.
	*****************************************************/
	template <class ObjectType, class AttributeType>
	class Attribute
	{
	private:
		ObjectType*		m_AttributeInst;						// Attribute Instance
		AttributeType	(ObjectType::*m_getFP) ();				// Getter Method
		void			(ObjectType::*m_setFP) (AttributeType);	// Setter Method

		AttributeType	get();									// Run the Getter Method
		void			set(AttributeType type);				// Run the Setter Method

	public:
		void	ReadOnly (ObjectType* ObjInst, 
						  AttributeType (ObjectType::*getter)());		// Pass in Getter Method
		
		void	ReadWrite (ObjectType* ObjInst, 
						   AttributeType (ObjectType::*getter)(),		// Pass in Getter Method
						   void (ObjectType::*setter)(AttributeType));	// Pass in Setter Method
	
		void		operator= (AttributeType);					// Public Set
		operator	AttributeType()		{ return get(); }		// Public Get

		friend std::ostream &operator << (std::ostream &stream, Attribute &a)	// Output to stream
		{
			stream << (AttributeType)a.get();
			return stream;
		}

		friend std::istream &operator >> (std::istream &stream, Attribute &a)	// Input to stream
		{
			AttributeType temp;
			stream >> temp;
			a.set(temp);
			return stream;
		}
	};
	/***********************************************************
						How to implement:
					ExampleClass exInstance;
	Initialize:		Attribute<ExampleClass, string> name;
	Set up:			name.ReadWrite(this, &ExampleClass::getName, &ExampleClass::setName);

	Setting:		exInstance.name = "Bob";
	Getting:		cout << exInstance.name << endl;
	************************************************************/
	// ReadOnly
	template<class ObjectType, class AttributeType>
	void Attribute<ObjectType, AttributeType>::ReadOnly(
				ObjectType* ObjInst,						// Object instance 
				AttributeType (ObjectType::*getter)() )		// Getter Function pointer
	{
		m_AttributeInst = ObjInst;
		m_getFP = getter;
		m_setFP = NULL;
	}

	// ReadWrite
	template<class ObjectType, class AttributeType>
	void Attribute<ObjectType, AttributeType>::ReadWrite(
				ObjectType* ObjInst,						// Object instance
				AttributeType (ObjectType::*getter)(),		// Getter Function pointer
				void (ObjectType::*setter)(AttributeType))	// Setter Function pointer
	{
		m_AttributeInst = ObjInst;
		m_getFP = getter;
		m_setFP = setter;
	}

	template<class ObjectType, class AttributeType>
	AttributeType Attribute<ObjectType, AttributeType>::get()
	{
		return (m_AttributeInst->*m_getFP)();
	}

	template<class ObjectType, class AttributeType>
	void Attribute<ObjectType, AttributeType>::set(AttributeType value)
	{
		return (m_AttributeInst->*m_setFP)(value);
	}

	template<class ObjectType, class AttributeType>
	void Attribute<ObjectType, AttributeType>::operator= (AttributeType value)
	{
		if(m_AttributeInst == NULL)
			throw new std::string("Attribute instance is not set.");
		if(m_setFP == NULL)
			throw new std::string("Read Only! Cannot be set.");
		set(value);
	}
}
#endif	//ATTRIBUTE_H