#ifndef	TRANSFORM_H
#define TRANSFORM_H

#include "Attribute.h"
#include <d3dx9.h>
#include <vector>

/********************************************
			Helper Pointers
*********************************************/
//typedef std::tr1::shared_ptr<IDirect3DTexture9*>	TexturePtr;	// Smart Pointer
//typedef std::vector<TexturePtr>						TextureList;

namespace WTW
{
	/********************************************
					Transform
	*********************************************/
	class Transform
	{
	private:
		D3DXMATRIX		m_Transform;	// World Coordinates
		D3DXMATRIX		m_InverseTrans;	// Local Coordinates
		D3DXMATRIX*		m_pOffset;		// World Offset (may be negative)
		D3DXVECTOR3		m_PosVec;		// Position
		D3DXVECTOR3		m_RotVec;		// Rotation
		D3DXVECTOR3		m_ScaleVec;		// Scaling in x, y, and z
		D3DXQUATERNION	m_RotQuat;		// Quaternion Rotation
		bool			m_bHasChanged;	// If(true) Need to Update m_Transform
		bool			m_bUpdateInv;	// Need to update the m_InverseTrans
		bool			m_bUseQuat;		// Set to true when using quaternions
		bool			m_bNewOffset;	// Created Offset (so must delete)
		
		// Scale
		void	SetScale(D3DXVECTOR3 scale);
		D3DXVECTOR3	GetScale();

		// Rotation
		void	SetRot(D3DXVECTOR3 deg3D);
		D3DXVECTOR3	GetRot();

		// Quaternion
		void	SetQuat(D3DXQUATERNION rot);
		D3DXQUATERNION	GetQuat();
		
		// Position
		void	SetPos(D3DXVECTOR3	pos);
		D3DXVECTOR3	GetPos();

	public:
		Transform();
		~Transform();
		void	SetOffset(D3DXVECTOR3 offset);
		void	SetOffset(D3DXMATRIX* offset);

		// Scale
		Attribute<Transform, D3DXVECTOR3>		m_Scale;

		// Rotation
		Attribute<Transform, D3DXVECTOR3>		m_Rotation;

		// Quaternion
		Attribute<Transform, D3DXQUATERNION>	m_Quaternion;

		// Position
		Attribute<Transform, D3DXVECTOR3>		m_Position;

		// Transform
		D3DXMATRIX&	GetTransform();
		D3DXMATRIX&	GetInverseTrans();
	};
}
#endif	//TRANSFORM_H