#include "DebugOutput.h"
#include "../MacroTools.h"

std::stringstream DebugOutput::s_DStream;
HWND DebugOutput::s_hEdit;
bool DebugOutput::s_bSetupFlag = DebugOutput::SetupFlag();
DebugOutput* DebugOutput::s_Instance = 0;
DebugOutput* DebugOutput::Instance()
{
	if(!s_Instance)
		s_Instance = new DebugOutput;
	return s_Instance;
}
DebugOutput::DebugOutput()
{
	// Initialize Critical Section
	InitializeCriticalSection(&cs_Lock);
	Init("Debug Output");
	clear();
}
DebugOutput::~DebugOutput()
{
	UnregisterClass("Debug Output", m_hInst);

	// Delete Critical Section
	DeleteCriticalSection(&cs_Lock);
	SAFE_DELETE(s_Instance);
}
void DebugOutput::Shutdown()
{
}

void DebugOutput::Init(std::string title)
{
	WNDCLASSEX wndClass;  
	ZeroMemory(&wndClass, sizeof(wndClass));

	// Set up the window
	wndClass.cbSize			= sizeof(WNDCLASSEX);			// size of window structure
	wndClass.lpfnWndProc	= (WNDPROC)sDoutProc;			// message callback
	wndClass.lpszClassName	= title.c_str();				// class name
	wndClass.hInstance		= m_hInst;						// handle to the application
	wndClass.hCursor		= LoadCursor(NULL, IDC_ARROW);	// default cursor
	wndClass.hbrBackground	= (HBRUSH)(COLOR_WINDOWFRAME);	// background brush

	// Register a new type of window
	if(!RegisterClassEx(&wndClass))
	{
		MessageBox(0, "DebugOutput::Init() Failed to RegisterClassEx()", "Error", MB_OK);
		PostQuitMessage(0);
	}

	// Get the screen size
	int screenWidth = GetSystemMetrics(SM_CXSCREEN);
	int screenHeight = GetSystemMetrics(SM_CYSCREEN);

	// Create Window
	m_hWnd = CreateWindowEx(
		WS_EX_CLIENTEDGE, 
		title.c_str(), title.c_str(),							// window class name and title
		WS_SIZEBOX | WS_VISIBLE | ES_MULTILINE | ES_AUTOVSCROLL | ES_AUTOHSCROLL,// window style
		(screenWidth-500), 10,									// x and y coordinates (place on the right side)
		500, (screenHeight-50),									// width and height of window
		NULL, NULL,												// parent window and menu
		m_hInst,												// handle to application
		this);
	if(!m_hWnd)	// Check for Error
	{
		MessageBox(0, "Failed to CreateWindow()", "Error", MB_OK);
		PostQuitMessage(0);
	}

	// Display the window
	ShowWindow(m_hWnd, SW_SHOWNORMAL);
	UpdateWindow(m_hWnd);
}

LRESULT CALLBACK DebugOutput::sDoutProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	// Route from static sDoutProc to local DoutProc
	if(msg == WM_NCCREATE)
		SetWindowLongPtr(hwnd, GWLP_USERDATA, (long)((LPCREATESTRUCT(lParam))->lpCreateParams));

	// Get a pointer to the local non-static callback function
	DebugOutput* output = reinterpret_cast<DebugOutput*>( GetWindowLongPtr(hwnd, GWLP_USERDATA) );

	if(!output)
		return DefWindowProc(hwnd, msg, wParam, lParam);
	else
		return output->DoutProc(hwnd, msg, wParam, lParam);
}

void DebugOutput::clear()
{
	m_DStream.str("");
}

std::string DebugOutput::ParseEndl(std::stringstream& stream)
{
	std::string search("\n");			// Find string
	std::string replace("\r\n");		// Replace srting
	std::string temp = stream.str();	// Create temp string from stringstream
    std::string::size_type pos = 0;		// Set init position

	// Begin string parsing loop to replace \n with \r\n
    while( (pos = temp.find(search, pos)) != std::string::npos )
	{
		temp.replace( pos, search.size(), replace);
        pos += replace.size();
    }
    return temp;	// Return string type
}

LRESULT CALLBACK DebugOutput::DoutProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
	case WM_CREATE:
		{
			HFONT hfDefault; // Use default font
			// Create the edit control as a child of the main window
			hEdit = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "", 
				WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL | ES_MULTILINE | ES_AUTOVSCROLL | ES_AUTOHSCROLL | ES_READONLY, 
				0, 0, 100, 100, hwnd, (HMENU)101, GetModuleHandle(NULL), NULL);
			// Check if the edit control was created properly
			if(hEdit == NULL)
				MessageBox(hwnd, "Could not create edit box.", "Error", MB_OK | MB_ICONERROR);

			hfDefault = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
			SendMessage(hEdit, WM_SETFONT, (WPARAM)hfDefault, MAKELPARAM(FALSE, 0));
			break;
		}
	case WM_SIZE:
		{
			// Resizes the edit control
			RECT rcClient;
			GetClientRect(hwnd, &rcClient);
			hEdit = GetDlgItem(hwnd, 101);
			SetWindowPos(hEdit, NULL, 0, 0, rcClient.right, rcClient.bottom, SWP_NOZORDER);
			break;
		}        
	case WM_CLOSE:
		{
			DestroyWindow(hwnd);
			break;
		}
	case WM_DESTROY:
		{
			PostQuitMessage(0); 
			break;
		}
	default:
		break;
	}
	return DefWindowProc(hwnd, msg, wParam, lParam);	// Pass the message on to default handler
}
