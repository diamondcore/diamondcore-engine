#include "Attribute.h"
/*
namespace WTW
{
	// ReadOnly
	template<class ObjectType, class AttributeType>
	void Attribute<ObjectType, AttributeType>::ReadOnly(
				ObjectType* ObjInst,						// Object instance 
				AttributeType (ObjectType::*getter)() )		// Getter Function pointer
	{
		m_AttributeInst = ObjInst;
		m_getFP = getter;
		m_setFP = NULL;
	}

	// ReadWrite
	template<class ObjectType, class AttributeType>
	void Attribute<ObjectType, AttributeType>::ReadWrite(
				ObjectType* ObjInst,						// Object instance
				AttributeType (ObjectType::*getter)(),		// Getter Function pointer
				void (ObjectType::*setter)(AttributeType))	// Setter Function pointer
	{
		m_AttributeInst = ObjInst;
		m_getFP = getter;
		m_setFP = setter;
	}

	template<class ObjectType, class AttributeType>
	AttributeType Attribute<ObjectType, AttributeType>::get()
	{
		return (m_AttributeInst->*m_getFP)();
	}

	template<class ObjectType, class AttributeType>
	void Attribute<ObjectType, AttributeType>::set(AttributeType value)
	{
		return (m_AttributeInst->*m_setFP)(value);
	}

	template<class ObjectType, class AttributeType>
	void Attribute<ObjectType, AttributeType>::operator= (AttributeType value)
	{
		if(m_AttributeInst == NULL)
			throw new std::string("Attribute instance is not set.");
		if(m_setFP == NULL)
			throw new std::string("Read Only! Cannot be set.");
		set(value);
	}
}*/