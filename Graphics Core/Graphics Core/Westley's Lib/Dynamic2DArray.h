#pragma once

#include <vector>
namespace WTW
{
	/****************************************************
				Dynamic Two Dimentional Array:
		Uses nested vectors to store the DataType info.
	*****************************************************/
	template<class DT>
	class Dynamic2DArray
	{
	public:
		Dynamic2DArray()
		{}
		Dynamic2DArray(int rows, int cols)	
			:m_2DArray(rows, std::vector<DT>(cols)) 
		{}

		std::vector<DT>& operator[] (int i)
		{
			return m_2DArray[i];
		}
		const std::vector<DT>& operator[] (int i) const
		{
			return m_2DArray[i];
		}
		void resize(int rows, int cols) // resize the 2D array
		{
			m_2DArray.resize(rows);
			for(int i = 0; i < rows; ++i)
				m_2DArray[i].resize(cols);
		}
		void clear()	// clear out the 2D array
		{
			for(auto i = m_2DArray.begin();
				i != m_2DArray.end(); i++)
				(*i).clear();
			m_2DArray.clear();
		}
		size_t size()
		{
			size_t size = 0;
			for(auto i = m_2DArray.begin();
				i != m_2DArray.end(); i++)
				size += (*i).size();
			return size;
		}
		bool empty()
		{
			return m_2DArray.empty();
		}

	private:
		std::vector<std::vector<DT>> m_2DArray;
	};
}