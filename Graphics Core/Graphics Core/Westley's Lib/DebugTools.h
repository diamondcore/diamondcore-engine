#pragma once

/*
#ifdef _DEBUG
#include <crtdbg.h>
#endif
*/
#if 0

#include <Windows.h>
#include <list>


	/**********************************
	Output Memory Leack Report
	***********************************/
	#if defined(DEBUG) | defined(_DEBUG)
	typedef struct {
		DWORD	address;
		DWORD	size;
		char	file[64];
		DWORD	line;
	} ALLOC_INFO;

	typedef std::list<ALLOC_INFO*> AllocList;
	AllocList*	allocList;

	void AddTrack(DWORD addr, DWORD asize, const char* fName, DWORD lineNum)
	{
		ALLOC_INFO* info;
	
		if(!allocList)
			allocList = new(AllocList);

		info = new(ALLOC_INFO);
		info->address = addr;
		strncpy_s(info->file, fName, 63);
		info->line = lineNum;
		info->size = asize;
		allocList->insert(allocList->begin(), info);
	};

	void RemoveTrack(DWORD addr)
	{
		AllocList::iterator i;

		if(!allocList)
			return;

		for(i = allocList->begin(); i != allocList->end(); i++)
			if((*i)->address == addr)
			{
				allocList->remove((*i));
				break;
			}
	};

	void DumpUnfreed()
	{
		AllocList::iterator i;
		DWORD totalSize = 0;
		char buf[1024];

		if(!allocList)
			return;

		for(i = allocList->begin(); i != allocList->end(); i++)
		{
			sprintf(buf, "%-50s:\t\tLINE %d,\t\tADDRESS %d\t%d unfreed\n",
				(*i)->file, (*i)->line, (*i)->address, (*i)->size);
			OutputDebugString(buf);
			totalSize += (*i)->size;
		}
		sprintf_s(buf, "-----------------------------------------------------------\n");
		OutputDebugString(buf);
		sprintf_s(buf, "Total Unfreed: %d bytes\n", totalSize);
		OutputDebugString(buf);
	};
	#endif


	/**********************************
	Overload the "new" and "delete"
	functions.
	***********************************/
	#if defined(DEBUG) | defined(_DEBUG)
	inline void* _cdecl operator new(unsigned int size, const char* file, int line)
	{
		void *ptr = (void*)malloc(size);
		AddTrack((DWORD)ptr, size, file, line);
		return (ptr);
	};
	inline void _cdecl operator delete(void* p)
	{
		RemoveTrack((DWORD)p);
		free(p);
	};
	//inline void* _cdecl operator new[](unsigned int size, const char* file, int line)
	//{
	//	void *ptr = (void*)malloc(size);
	//	AddTrack((DWORD)ptr, size, file, line);
	//	return (ptr);
	//};
	//inline void _cdecl operator delete[](void* p)
	//{
	//	RemoveTrack((DWORD)p);
	//	free(p);
	//};
	#endif

/**********************************
Convert one parameter "new" to a 
three parameter "new".  Taking the
current file and line as the other
two parameters.
***********************************/
#if defined(DEBUG) | defined(_DEBUG)
#define DEBUG_NEW new(_NORMAL_BLOCK,__FILE__, __LINE__)
#else
#define DEBUG_NEW new
#endif
#define new DEBUG_NEW
#endif