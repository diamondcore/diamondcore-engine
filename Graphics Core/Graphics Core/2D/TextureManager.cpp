#include "TextureManager.h"
#include "../GraphicsCore.h"
#include "../MacroTools.h"

TextureManager::TextureManager()
{
	// Set the scaled matrix
	D3DXMatrixIdentity(&m_BaseMatrix);
	D3DXVECTOR2 S(((float)GCI->GetDirectX()->GetScreenWidth()/(float)SCREEN_WIDTH),
		((float)GCI->GetDirectX()->GetScreenHeight()/(float)SCREEN_HEIGHT));
	D3DXMatrixTransformation2D(&m_BaseMatrix, NULL, 0.0, &S, &D3DXVECTOR2(0,0), 0.0f, NULL);
}
TextureManager::~TextureManager()
{
}
void TextureManager::Shutdown()
{
	// Unload all Textures
	for(auto m = TextureList.begin(); m != TextureList.end(); m++)
	{
		SAFE_RELEASE(m->second->texture);
	}
	//TextureList.clear();
}
void TextureManager::PostRestartDevice()
{
	// Reset the scaled matrix
	D3DXMatrixIdentity(&m_BaseMatrix);
	D3DXVECTOR2 S(((float)GCI->GetDirectX()->GetScreenWidth()/(float)SCREEN_WIDTH),
		((float)GCI->GetDirectX()->GetScreenHeight()/(float)SCREEN_HEIGHT));
	D3DXMatrixTransformation2D(&m_BaseMatrix, NULL, 0.0, &S, &D3DXVECTOR2(0,0), 0.0f, NULL);
}

TextureInfo* TextureManager::LoadTexture(std::string textureName)
{   
	auto tex = TextureList.find(textureName);
	if( tex != TextureList.end() )
		return (tex->second).get();	// This texture is already loaded
	
	std::shared_ptr<TextureInfo> newTexture = std::shared_ptr<TextureInfo>( new TextureInfo );	

	// Set the path for the scene mesh
	newTexture->TextureName = "Content/Textures/";
	newTexture->TextureName += textureName;

	D3DXGetImageInfoFromFileA(newTexture->TextureName.c_str(), &newTexture->ImageInfo);

	HRESULT hr = D3DXCreateTextureFromFileEx(GCI->GetDirectX()->GetDevice(), newTexture->TextureName.c_str(), 
		newTexture->ImageInfo.Width, newTexture->ImageInfo.Height,
		newTexture->ImageInfo.MipLevels, NULL, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, D3DX_DEFAULT, 
		D3DX_DEFAULT, D3DCOLOR_XRGB(0, 0, 0), 
		&newTexture->ImageInfo, NULL, &newTexture->texture);
		
	if (FAILED(hr))
		return NULL;

	TextureList.insert(std::pair<std::string, std::shared_ptr<TextureInfo>>(textureName, newTexture));
	return TextureList[textureName].get();
}
TextureInfo* TextureManager::LoadTexture(std::string textureName, D3DXCOLOR alpha)
{   
	auto tex = TextureList.find(textureName);
	if( tex != TextureList.end() )
		return (tex->second).get();	// This texture is already loaded
	
	std::shared_ptr<TextureInfo> newTexture = std::shared_ptr<TextureInfo>( new TextureInfo );	

	// Set the path for the scene mesh
	newTexture->TextureName = "Content/Textures/";
	newTexture->TextureName += textureName;

	D3DXGetImageInfoFromFileA(newTexture->TextureName.c_str(), &newTexture->ImageInfo);

	HRESULT hr = D3DXCreateTextureFromFileEx(GCI->GetDirectX()->GetDevice(), newTexture->TextureName.c_str(), 
		newTexture->ImageInfo.Width, newTexture->ImageInfo.Height,
		D3DX_DEFAULT, NULL, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, D3DX_DEFAULT, 
		newTexture->ImageInfo.MipLevels, alpha, 
		&newTexture->ImageInfo, NULL, &newTexture->texture);
		
	if (FAILED(hr))
		return false;

	TextureList.insert(std::pair<std::string, std::shared_ptr<TextureInfo>>(textureName, newTexture));
	return TextureList[textureName].get();
}
TextureInfo* TextureManager::LoadRawTexture(std::string textureName)
{
	auto tex = TextureList.find(textureName);
	if( tex != TextureList.end() )
		return (tex->second).get();	// This texture is already loaded
	
	std::shared_ptr<TextureInfo> newTexture = std::shared_ptr<TextureInfo>( new TextureInfo );	

	// Set the path for the scene mesh
	newTexture->TextureName = textureName;

	D3DXGetImageInfoFromFileA(newTexture->TextureName.c_str(), &newTexture->ImageInfo);

	HRESULT hr = D3DXCreateTextureFromFileEx(GCI->GetDirectX()->GetDevice(), newTexture->TextureName.c_str(), 
		newTexture->ImageInfo.Width, newTexture->ImageInfo.Height,
		D3DX_DEFAULT, NULL, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, D3DX_DEFAULT, 
		newTexture->ImageInfo.MipLevels, D3DCOLOR_XRGB(0, 0, 0), 
		&newTexture->ImageInfo, NULL, &newTexture->texture);
		
	if (FAILED(hr))
	{
		newTexture->texture = 0;
		return false;
	}

	TextureList.insert(std::pair<std::string, std::shared_ptr<TextureInfo>>(textureName, newTexture));
	return TextureList[textureName].get();
}

LPDIRECT3DTEXTURE9 TextureManager::getTexture(std::string textureName)
{
	std::map<std::string, std::shared_ptr<TextureInfo>>::iterator p;
	p = TextureList.find(textureName);

	if (p == TextureList.end())
		LoadTexture(textureName);

	return TextureList[textureName]->texture;
}

void TextureManager::RenderTexture(std::string textureName, std::map<int, BaseObjectPtr> objects)
{
	// Render the list of textures
	for(auto t = objects.begin();
		t != objects.end(); t++)
		if((*t).second->m_Color.a == -1.0f)	// Use default color
			RenderTexture( textureName, (*t).second->m_Tranform.GetTransform() );
		else
			RenderTexture( textureName, (*t).second->m_Tranform.GetTransform(), (*t).second->m_Color);	
}
void TextureManager::RenderTexture(std::string textureName, D3DXMATRIX transform)
{
	transform = transform * m_BaseMatrix;
	GCI->GetDirectX()->GetSprite()->SetTransform(&transform);
	GCI->GetDirectX()->GetSprite()->Draw(getTexture(textureName),NULL,NULL,NULL,D3DCOLOR_ARGB(255,255, 255, 255));
}
void TextureManager::RenderTexture(std::string textureName, D3DXMATRIX transform, D3DXCOLOR color)
{
	transform = transform * m_BaseMatrix;
	GCI->GetDirectX()->GetSprite()->SetTransform(&transform);
	GCI->GetDirectX()->GetSprite()->Draw(getTexture(textureName),NULL,NULL,NULL,color);
}
void TextureManager::RenderTexture(std::string textureName,D3DXMATRIX transform[], int count)
{
	for(int i = 0; i < count; i++)
	{
		GCI->GetDirectX()->GetSprite()->SetTransform(&(transform[i] * m_BaseMatrix));
		GCI->GetDirectX()->GetSprite()->Draw(getTexture(textureName),NULL,NULL,NULL,D3DCOLOR_ARGB(255,255, 255, 255));
	}
}
void TextureManager::RenderTexture(std::string textureName,D3DXMATRIX transform[], D3DXCOLOR color[], int count)
{
	for(int i = 0; i < count; i++)
	{
		GCI->GetDirectX()->GetSprite()->SetTransform(&(transform[i] * m_BaseMatrix));
		GCI->GetDirectX()->GetSprite()->Draw(getTexture(textureName),NULL,NULL,NULL,color[i]);
	}	
}
bool TextureManager::UnloadTexture(std::string textureName)
{    
	std::map<std::string, std::shared_ptr<TextureInfo>>::iterator p;
  
	p = TextureList.find(textureName);

	if(p->second->texture)
		{
			p->second->texture->Release();
			p->second->texture = 0;
			TextureList.erase(p);
			return true;
		}
	else
		return false;
}
