#pragma once

#include "../3D/VertexDecl.h"
#include <vector>

class Lines
{
	IDirect3DVertexBuffer9*		m_VertexBuff;
	IDirect3DIndexBuffer9*		m_IndexBuff;
	std::vector<AA_LineVertex>	m_Lines;
	bool						m_bNeedsUpdate;

public:
	Lines();
	~Lines();
	void	Init();
	void	Shutdown();

	void	AddLine(D3DXVECTOR3 p0, D3DXVECTOR3 p1, float radius, float aspect);

	void	UpdateBuffers();

	void	Render();
};