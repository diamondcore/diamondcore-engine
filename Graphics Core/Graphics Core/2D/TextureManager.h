#pragma once
#include <map>
#include <string>
#include <d3dx9.h>
#include "../SceneGraph/ObjectList.h"
//#define TextureManager TM


class TextureInfo
{
public:
	TextureInfo()
	{
		TextureName = "";
		texture     = 0;
	}
	~TextureInfo()
	{
		TextureName = "";
	}
	std::string TextureName;
	D3DXIMAGE_INFO ImageInfo;
	LPDIRECT3DTEXTURE9 texture;
};

class TextureManager
{
public:
	TextureManager();
	~TextureManager();
	void Shutdown();
	void PostRestartDevice();

	TextureInfo* LoadTexture(std::string textureName);
	TextureInfo* LoadTexture(std::string textureName, D3DXCOLOR alpha);
	TextureInfo* LoadRawTexture(std::string textureName);
	bool UnloadTexture(std::string textureName);	

	// Render Methods
	void RenderTexture(std::string textureName, std::map<int, BaseObjectPtr> objects);
	void RenderTexture(std::string textureName, D3DXMATRIX transform);
	void RenderTexture(std::string textureName, D3DXMATRIX transform, D3DXCOLOR color);
	void RenderTexture(std::string textureName, D3DXMATRIX transform[], int count);
	void RenderTexture(std::string textureName, D3DXMATRIX transform[], D3DXCOLOR color[], int count);

	std::map<std::string, std::shared_ptr<TextureInfo>> TextureList;

private:
	D3DXMATRIX	m_BaseMatrix;

	// Gets texture, loads it if it doesn't exist
	LPDIRECT3DTEXTURE9 getTexture(std::string textureName);
};
