#pragma once

#include <DCMathLib.h>

#include "TextureManager.h"
#include <functional>

class SpriteFont
{
public:

	SpriteFont(std::string fileName);
	~SpriteFont();

    // Describes a single character glyph.
    struct Glyph
    {
        unsigned int Character;
        RECT Subrect;
        float XOffset;
        float YOffset;
        float XAdvance;
    };

	Glyph const* FindGlyph(wchar_t character) const;

	void SetDefaultCharacter(wchar_t character);

	void ForEachGlyph(std::string text, function<void (const Glyph* glyph, float x, float y)> action)
	{
		float x = 0;
		float y = 0;

		for (auto i = 0; i < text.length(); i++)
		{
			auto character = text[i];

			switch (character)
			{
				case '\r':
					// Skip carriage returns.
					continue;

				case '\n':
					// New line.
					x = 0;
					y += lineSpacing;
					break;

				default:
					// Output this character.
					auto glyph = FindGlyph(character);

					x += glyph->XOffset;

					if (x < 0)
						x = 0;

					if (!iswspace(character))
						action(glyph, x, y);

					x += glyph->Subrect.right - glyph->Subrect.left + glyph->XAdvance;
					break;
			}
		}
	}

	// Fields.
	TextureInfo* mTexture;
	std::vector<Glyph> glyphs;
	Glyph const* defaultGlyph;
	float lineSpacing;

    Vector2 MeasureString(std::string text);

    float GetLineSpacing() const;
    void SetLineSpacing(float spacing);

    wchar_t GetDefaultCharacter() const;

    bool ContainsCharacter(wchar_t character) const;
};
