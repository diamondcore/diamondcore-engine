#pragma once

#include "../DirectXClass.h"
#include <map>
#include <DCMathLib.h>

class SpriteFont;
class FontManager
{
	std::map<std::string, ID3DXFont*>	m_FontMap;
	std::map<std::string, SpriteFont*>	m_SpriteFontMap;
	std::string		m_DefaultFont;
	D3DXCOLOR		m_DefaultColor;
	D3DXMATRIX		m_BaseMatrix;
	D3DXVECTOR2		m_ScaleScreen;

public:
	FontManager();
	~FontManager();
	void	PreRestartDevice();
	void	PostRestartDevice();

	void	Init();
	void	Shutdown();

	bool	Load(std::string font);
	bool	Load(std::string font, bool italic);
	bool	Load(std::string font, int height, int width, int weight, bool italic);
	bool	Load(std::string font, int height, int width, int weight, int mipLevels, bool italic,
				DWORD charSet, DWORD outputPrecision, DWORD quality, DWORD pitchAndFamily);
	void	Unload(std::string font);

	ID3DXFont*	getFont(std::string fontName);

	// Use Data Oriented Rendering
	void	Render(std::string text, float x, float y);	// Use default 
	void	Render(std::string text, float x, float y, 	// Use specific font
					std::string font);							
	void	Render(std::string text, float x, float y, 	// Use specific color
					D3DXCOLOR color);			
	void	Render(std::string text, float x, float y, 	// Use specific font and color
					std::string font, D3DXCOLOR color);

	void	Render(std::string text, float x, float y, float z, 
					std::string font, D3DXCOLOR color);
	void	Render(std::string text, D3DXMATRIX transform,
					SpriteFont* font, D3DXCOLOR color);
	
	Vector2 MeasureString(const char* fontName, char* text);

	/*void DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ wchar_t const* text, Vector2 const& position, Vector4 color = Colors::White, float rotation = 0, XMFLOAT2 const& origin = Float2Zero, float scale = 1, SpriteEffects effects = SpriteEffects_None, float layerDepth = 0);
    void DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ wchar_t const* text, Vector2 const& position, Vector4 color, float rotation, XMFLOAT2 const& origin, XMFLOAT2 const& scale, SpriteEffects effects = SpriteEffects_None, float layerDepth = 0);
    void DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ wchar_t const* text, Vector2 position, Vector4 color = Colors::White, float rotation = 0, FXMVECTOR origin = g_XMZero, float scale = 1, SpriteEffects effects = SpriteEffects_None, float layerDepth = 0);
    void DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ wchar_t const* text, Vector2 position, Vector4 color, float rotation, FXMVECTOR origin, GXMVECTOR scale, SpriteEffects effects = SpriteEffects_None, float layerDepth = 0);*/

	// Set Default Rendering Attributes
	void	SetDefaultFont(std::string font);
	void	SetDefaultColor(D3DXCOLOR color);
};