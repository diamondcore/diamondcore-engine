#pragma once

#include <memory>
#include <exception>
#include <type_traits>
#include <Windows.h>
#include <string>

struct handle_closer { void operator()(HANDLE h) { if (h) CloseHandle(h); } };

typedef public std::unique_ptr<void, handle_closer> ScopedHandle;

inline HANDLE safe_handle( HANDLE h ) { return (h == INVALID_HANDLE_VALUE) ? 0 : h; }

// Helper for reading binary data, either from the filesystem a memory buffer.
class BinaryReader
{
public:
    explicit BinaryReader(std::string fileName);
        
    // Reads a single value.
    template<typename T> T const& Read()
    {
        return *ReadArray<T>(1);
    }

	std::string ReadString()
	{
		auto len = Read<unsigned int>();

		auto ch = ReadArray<char>(len);
		auto str = std::string(ch);
		
		return str;
	}


    // Reads an array of values.
    template<typename T> T const* ReadArray(size_t elementCount)
    {
        static_assert(std::is_pod<T>::value, "Can only read plain-old-data types");

        unsigned char const* newPos = mPos + sizeof(T) * elementCount;

        if (newPos > mEnd)
            throw std::exception("End of file");

        T const* result = reinterpret_cast<T const*>(mPos);

        mPos = newPos;

        return result;
    }


    // Lower level helper reads directly from the filesystem into memory.
    static HRESULT ReadEntireFile(std::string fileName, _Inout_ std::unique_ptr<unsigned char[]>& data, _Out_ size_t* dataSize);


private:
    // The data currently being read.
    unsigned char const* mPos;
    unsigned char const* mEnd;

    std::unique_ptr<unsigned char[]> mOwnedData;


    // Prevent copying.
    BinaryReader(BinaryReader const&);
    BinaryReader& operator= (BinaryReader const&);
};
