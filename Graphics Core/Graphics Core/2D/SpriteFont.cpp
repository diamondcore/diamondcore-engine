#include <algorithm>
#include <vector>

#include "SpriteFont.h"
#include "BinaryReader.h"
#include "../GraphicsCore.h"

// Comparison operators make our sorted glyph vector work with std::binary_search and lower_bound.
static inline bool operator< (SpriteFont::Glyph const& left, SpriteFont::Glyph const& right)
{
    return left.Character < right.Character;
}

static inline bool operator< (wchar_t left, SpriteFont::Glyph const& right)
{
    return left < right.Character;
}

static inline bool operator< (SpriteFont::Glyph const& left, wchar_t right)
{
    return left.Character < right;
}

// Sets the missing-character fallback glyph.
void SpriteFont::SetDefaultCharacter(wchar_t character)
{
	defaultGlyph = character ? FindGlyph(character) : nullptr;
}

// Looks up the requested glyph, falling back to the default character if it is not in the font.
SpriteFont::Glyph const* SpriteFont::FindGlyph(wchar_t character) const
{
    auto glyph = std::lower_bound(glyphs.begin(), glyphs.end(), character);

    if (glyph != glyphs.end() && glyph->Character == character)
        return &*glyph;

    if (defaultGlyph)
        return defaultGlyph;

    throw std::exception("Character not in font");
}

// Construct from a binary file created by the MakeSpriteFont utility.
SpriteFont::SpriteFont(std::string fileName)
{
	BinaryReader reader("Content/Textures/" + fileName + ".spritefont");

    // Read the glyph data.
    auto glyphCount = reader.Read<int>();
    auto glyphData = reader.ReadArray<Glyph>(glyphCount);

    glyphs.assign(glyphData, glyphData + glyphCount);

    // Read font properties.
    lineSpacing = reader.Read<float>();

    SetDefaultCharacter((wchar_t)reader.Read<int>());

	mTexture = GCI->GetTextureManager()->LoadTexture(reader.ReadString());
}

// Public destructor.
SpriteFont::~SpriteFont()
{
}

/*void SpriteFont::DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ wchar_t const* text, XMFLOAT2 const& position, FXMVECTOR color, float rotation, XMFLOAT2 const& origin, float scale, SpriteEffects effects, float layerDepth)
{
    DrawString(spriteBatch, text, XMLoadFloat2(&position), color, rotation, XMLoadFloat2(&origin), XMVectorReplicate(scale), effects, layerDepth);
}


void SpriteFont::DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ wchar_t const* text, XMFLOAT2 const& position, FXMVECTOR color, float rotation, XMFLOAT2 const& origin, XMFLOAT2 const& scale, SpriteEffects effects, float layerDepth)
{
    DrawString(spriteBatch, text, XMLoadFloat2(&position), color, rotation, XMLoadFloat2(&origin), XMLoadFloat2(&scale), effects, layerDepth);
}


void SpriteFont::DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ wchar_t const* text, FXMVECTOR position, FXMVECTOR color, float rotation, FXMVECTOR origin, float scale, SpriteEffects effects, float layerDepth)
{
    DrawString(spriteBatch, text, position, color, rotation, origin, XMVectorReplicate(scale), effects, layerDepth);
}


void SpriteFont::DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ wchar_t const* text, FXMVECTOR position, FXMVECTOR color, float rotation, FXMVECTOR origin, GXMVECTOR scale, SpriteEffects effects, float layerDepth)
{
    static_assert(SpriteEffects_FlipHorizontally == 1 &&
                  SpriteEffects_FlipVertically == 2, "If you change these enum values, the following tables must be updated to match");

    // Lookup table indicates which way to move along each axis per SpriteEffects enum value.
    static XMVECTORF32 axisDirectionTable[4] =
    {
        { -1, -1 },
        {  1, -1 },
        { -1,  1 },
        {  1,  1 },
    };

    // Lookup table indicates which axes are mirrored for each SpriteEffects enum value.
    static XMVECTORF32 axisIsMirroredTable[4] =
    {
        { 0, 0 },
        { 1, 0 },
        { 0, 1 },
        { 1, 1 },
    };

    XMVECTOR baseOffset = origin;

    // Draw each character in turn.
    ForEachGlyph(text, [&](Glyph const* glyph, float x, float y)
    {
        XMVECTOR offset = XMVectorMultiplyAdd(XMVectorSet(x, y + glyph->YOffset, 0, 0), axisDirectionTable[effects & 3], baseOffset);
        
        if (effects)
        {
            // For mirrored characters, specify bottom and/or right instead of top left.
            XMVECTOR glyphRect = XMConvertVectorIntToFloat(XMLoadInt4(reinterpret_cast<uint32_t const*>(&glyph->Subrect)), 0);

            // xy = glyph width/height.
            glyphRect = XMVectorSwizzle<2, 3, 0, 1>(glyphRect) - glyphRect;

            offset = XMVectorMultiplyAdd(glyphRect, axisIsMirroredTable[effects & 3], offset);
        }

        spriteBatch->Draw(pImpl->texture.Get(), position, &glyph->Subrect, color, rotation, offset, scale, effects, layerDepth);
    });
}*/


Vector2 SpriteFont::MeasureString(std::string text)
{
    Vector2 result = Vector2();

	ForEachGlyph(text, [&](const Glyph* glyph, float x, float y)
    {
        float w = (float)(glyph->Subrect.right - glyph->Subrect.left);
        float h = (float)(glyph->Subrect.bottom - glyph->Subrect.top) + glyph->YOffset;

        h = max(h, lineSpacing);

		result = Vector2::Max(result, Vector2(x + w, y + h));
    });

    return result;
}


float SpriteFont::GetLineSpacing() const
{
    return lineSpacing;
}


void SpriteFont::SetLineSpacing(float spacing)
{
    lineSpacing = spacing;
}


wchar_t SpriteFont::GetDefaultCharacter() const
{
    return defaultGlyph ? (wchar_t)defaultGlyph->Character : 0;
}


bool SpriteFont::ContainsCharacter(wchar_t character) const
{
    return std::binary_search(glyphs.begin(), glyphs.end(), character);
}
