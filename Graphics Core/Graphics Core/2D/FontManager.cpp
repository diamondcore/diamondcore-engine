#include "FontManager.h"
#include "../GraphicsCore.h"
#include "../MacroTools.h"
#include "SpriteFont.h"

FontManager::FontManager()
{
	D3DXMatrixIdentity(&m_BaseMatrix);
}
FontManager::~FontManager()
{}

void FontManager::PreRestartDevice()
{
	for(auto f = m_FontMap.begin(); f != m_FontMap.end(); f++)
		(*f).second->OnLostDevice();
}
void FontManager::PostRestartDevice()
{
	for(auto f = m_FontMap.begin(); f != m_FontMap.end(); f++)
		(*f).second->OnResetDevice();
	
	// Reset the scaled matrix
	D3DXMatrixIdentity(&m_BaseMatrix);
	m_ScaleScreen = D3DXVECTOR2(((float)GCI->GetDirectX()->GetScreenWidth()/(float)SCREEN_WIDTH),
		((float)GCI->GetDirectX()->GetScreenHeight()/(float)SCREEN_HEIGHT));
	D3DXMatrixTransformation2D(&m_BaseMatrix, NULL, 0.0, &m_ScaleScreen, &D3DXVECTOR2(0,0), 0.0f, NULL);
}

void FontManager::Init()
{
	// Set the Default color to white
	m_DefaultColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	
	// Set the scaled matrix
	D3DXMatrixIdentity(&m_BaseMatrix);
	m_ScaleScreen = D3DXVECTOR2(1.0f, 1.0f);
	m_ScaleScreen = D3DXVECTOR2(((float)GCI->GetDirectX()->GetScreenWidth()/(float)SCREEN_WIDTH),
		((float)GCI->GetDirectX()->GetScreenHeight()/(float)SCREEN_HEIGHT));
	D3DXMatrixTransformation2D(&m_BaseMatrix, NULL, 0.0, &m_ScaleScreen, &D3DXVECTOR2(0,0), 0.0f, NULL);
}
void FontManager::Shutdown()
{
	// Release and Remove all of the fonts
	while( !m_FontMap.empty() )
	{
		SAFE_RELEASE( m_FontMap.begin()->second );
		m_FontMap.erase( m_FontMap.begin() );
	}
}

bool FontManager::Load(std::string font)
{
	auto it = m_SpriteFontMap.find(font);

	if (it != m_SpriteFontMap.end())
		return true;

	m_SpriteFontMap.insert(std::pair<std::string, SpriteFont*>(font, new SpriteFont(font)));

	return true;
}
bool FontManager::Load(std::string font, bool italic)
{
	return Load(font, 18, 0, 675, 0, italic, 
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE );
}
bool FontManager::Load(std::string font, int height, int width, int weight, bool italic)
{
	return Load(font, height, width, weight, 0, italic, 
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE );
}
bool FontManager::Load(std::string font, int height, int width, int weight, int mipLevels, bool italic,
			DWORD charSet, DWORD outputPrecision, DWORD quality, DWORD pitchAndFamily)
{
	auto res = m_FontMap.find(font);
	if (res != m_FontMap.end())
		return true;
	// Make sure the font has an ".otf" extention
	int ext = font.find_last_of('.');
	if( ext == -1 )
		font += ".otf";	// Add the ".otf" extention
	else if( font.substr(ext) != ".otf" )
		return false;	// Not in the supported font format
		

	// Create Font
	ID3DXFont* temp;
	HRESULT hr = D3DXCreateFont(GCI->GetDirectX()->GetDevice(), 
					height, width, weight, mipLevels, italic, 
					charSet, outputPrecision, quality, pitchAndFamily, 
					TEXT( font.c_str() ), &temp);

	// Check if font was created successfully
	if(hr != S_OK)
		return false;
	
	// Add font to the map
	m_FontMap.insert( std::pair<std::string, ID3DXFont*>(font, temp) );

	// Set Default font to the first font loaded
	if( m_FontMap.size() == 1 )
		m_DefaultFont = font;
	return true;
}
void FontManager::Unload(std::string font)
{
	SAFE_RELEASE( m_FontMap[font] );
	m_FontMap.erase( m_FontMap.find(font) );
}

Vector2 FontManager::MeasureString(const char* fontName, char* text)
{
	return m_SpriteFontMap[fontName]->MeasureString(text);
}

// Use Data Oriented Rendering
void FontManager::Render(std::string text, float x, float y)	// Use default
{
	Render(text, x, y, m_DefaultFont, m_DefaultColor);
}
void FontManager::Render(std::string text, float x, float y, std::string font)
{
	Render(text, x, y, font, m_DefaultColor);
}
void FontManager::Render(std::string text, float x, float y, D3DXCOLOR color)
{
	Render(text, x, y, m_DefaultFont, color);
}
void FontManager::Render(std::string text, float x, float y, std::string font, D3DXCOLOR color)
{	
	auto spriteFont = m_SpriteFontMap[font];

	static D3DXMATRIX xform = D3DXMATRIX();
	D3DXMatrixIdentity(&xform);
	xform = m_BaseMatrix * xform;	// Scale based on the screen size

	xform._43 = 0;

	//TODO: Batch when it's available.
	auto sprite = GCI->GetDirectX()->GetSprite();
	
	// Draw each character in turn.
    spriteFont->ForEachGlyph(text, [&](SpriteFont::Glyph const* glyph, float x1, float y1)
    {
		xform._41 = x + x1;
		xform._42 = y + y1 + glyph->YOffset;
		
		xform._41 *= m_ScaleScreen.x;
		xform._42 *= m_ScaleScreen.y;

		sprite->SetTransform(&xform);
		sprite->Draw(spriteFont->mTexture->texture, &glyph->Subrect,NULL,NULL,color);
    });
}
void FontManager::Render(std::string text, float x, float y, float z, std::string font, D3DXCOLOR color)
{	
	auto spriteFont = m_SpriteFontMap[font];

	static D3DXMATRIX xform = D3DXMATRIX();
	D3DXMatrixIdentity(&xform);
	xform = m_BaseMatrix * xform;	// Scale based on the screen size

	xform._43 = z;

	//TODO: Batch when it's available.
	auto sprite = GCI->GetDirectX()->GetSprite();
	
	// Draw each character in turn.
    spriteFont->ForEachGlyph(text, [&](SpriteFont::Glyph const* glyph, float x1, float y1)
    {
		xform._41 = x + x1;
		xform._42 = y + y1 + glyph->YOffset;
		
		xform._41 *= m_ScaleScreen.x;
		xform._42 *= m_ScaleScreen.y;

		sprite->SetTransform(&xform);
		sprite->Draw(spriteFont->mTexture->texture, &glyph->Subrect,NULL,NULL,color);
    });
}

ID3DXFont* FontManager::getFont(std::string fontName)
{
	std::map<std::string, ID3DXFont*>::iterator p;
	p = m_FontMap.find(fontName);

	if (p == m_FontMap.end())
		Load(fontName);

	return m_FontMap[fontName];
}

void FontManager::SetDefaultFont(std::string font)
{
	m_DefaultFont = font;
}
void FontManager::SetDefaultColor(D3DXCOLOR color)
{
	m_DefaultColor = color;
}

