#include "Lines.h"
#include "../MacroTools.h"
#include "../GraphicsCore.h"

Lines::Lines()
{
	m_IndexBuff		= 0;
	m_VertexBuff	= 0;
	m_bNeedsUpdate	= true;
}
Lines::~Lines()
{
	m_Lines.clear();
}

void Lines::Init()
{
	GCI->GetTextureManager()->LoadTexture("AA_Lines.png");
}
void Lines::Shutdown()
{
	SAFE_RELEASE(m_IndexBuff);
	SAFE_RELEASE(m_VertexBuff);
}

void Lines::AddLine(D3DXVECTOR3 p0, D3DXVECTOR3 p1, float radius, float aspect)
{
	m_bNeedsUpdate = true;
	m_Lines.push_back(AA_LineVertex(p0, p1, radius, aspect, 0.0f, 0.0f, 0.0f, 0.0f));
}

void Lines::UpdateBuffers()
{
	// Write Vertex Buffer
	GCI->GetDirectX()->GetDevice()->CreateVertexBuffer(m_Lines.size()*8 * sizeof(AA_LineVertex), 
		D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED, &m_VertexBuff, 0);

	AA_LineVertex* verts = 0;
	m_VertexBuff->Lock(0, 0, (void**)&verts, 0);
	AA_LineVertex* line = 0;
	for( int i = 0; i < m_Lines.size(); i+=8)
	{
		line = &m_Lines[i];
		verts[ i + 0 ] = AA_LineVertex(line->position1, line->position2, line->radius, line->aspect, 1.0f, 0.0f, -1.0f, -1.0f);
		verts[ i + 1 ] = AA_LineVertex(line->position1, line->position2, line->radius, line->aspect, 1.0f, 0.0f, -1.0f,  1.0f);
		verts[ i + 2 ] = AA_LineVertex(line->position1, line->position2, line->radius, line->aspect, 1.0f, 0.0f,  0.0f, -1.0f);
		verts[ i + 3 ] = AA_LineVertex(line->position1, line->position2, line->radius, line->aspect, 1.0f, 0.0f,  0.0f,  1.0f);
		verts[ i + 4 ] = AA_LineVertex(line->position1, line->position2, line->radius, line->aspect, 0.0f, 1.0f,  0.0f, -1.0f);
		verts[ i + 5 ] = AA_LineVertex(line->position1, line->position2, line->radius, line->aspect, 0.0f, 1.0f,  0.0f,  1.0f);
		verts[ i + 6 ] = AA_LineVertex(line->position1, line->position2, line->radius, line->aspect, 0.0f, 1.0f,  1.0f, -1.0f);
		verts[ i + 7 ] = AA_LineVertex(line->position1, line->position2, line->radius, line->aspect, 0.0f, 1.0f,  1.0f,  1.0f);
	}
	m_VertexBuff->Unlock();

	// Write the index buffers
	GCI->GetDirectX()->GetDevice()->CreateIndexBuffer( m_Lines.size() *sizeof(WORD), 
			D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_IndexBuff, 0);
	
	int index = 0;
	WORD* Indices = 0;
	m_IndexBuff->Lock(0, 0, (void**)&Indices, 0);
	for( int i = 0; i < m_Lines.size(); i++ )
    {
        Indices[ index++ ] = i * 8;
        Indices[ index++ ] = i * 8 + 2;
        Indices[ index++ ] = i * 8 + 3;
        Indices[ index++ ] = i * 8;
        Indices[ index++ ] = i * 8 + 3;
        Indices[ index++ ] = i * 8 + 1;

        Indices[ index++ ] = i * 8 + 2;
        Indices[ index++ ] = i * 8 + 4;
        Indices[ index++ ] = i * 8 + 5;
        Indices[ index++ ] = i * 8 + 2;
        Indices[ index++ ] = i * 8 + 5;
        Indices[ index++ ] = i * 8 + 3;

        Indices[ index++ ] = i * 8 + 4;
        Indices[ index++ ] = i * 8 + 6;
        Indices[ index++ ] = i * 8 + 7;
        Indices[ index++ ] = i * 8 + 4;
        Indices[ index++ ] = i * 8 + 7;
        Indices[ index++ ] = i * 8 + 5;
    }
	m_IndexBuff->Unlock();
}
void Lines::Render()
{
	auto DXI = GCI->GetDirectX();
	auto DSX = GCI->GetDeferredR();

	if(m_bNeedsUpdate)
	{
		// Rebuild Buffers
		Shutdown();
		UpdateBuffers();
	}

	DSX->m_pFX->SetVector("g_sColor", (D3DXVECTOR4*)&WHITE);
	DSX->m_pFX->SetTexture("g_sFilterTex", GCI->GetTextureManager()->TextureList["AA_Lines.png"]->texture);

	DSX->m_pFX->SetTechnique("AA_LinesTech");

	// Prepare the index and vertex buffer
	DXI->GetDevice()->SetIndices( m_IndexBuff );
	DXI->GetDevice()->SetStreamSource( 0, m_VertexBuff, 0, sizeof(AA_LineVertex));
	DXI->GetDevice()->SetVertexDeclaration( AA_LineVertex::Decl );

	// Set default world
	D3DXMATRIX W;
	D3DXMatrixIdentity(&W);
	DSX->m_pFX->SetMatrix("g_sWorld", &W);
	DSX->m_pFX->SetMatrix("g_sWVP", &(W*GCI->GetCamera()->GetViewProj()));

	UINT numPass = 0;
	DSX->m_pFX->Begin(&numPass, 0);
	DSX->m_pFX->BeginPass(numPass);

	// Render the lines
	int numVerts = m_Lines.size() * 8;
	DXI->GetDevice()->DrawIndexedPrimitive( D3DPT_TRIANGLELIST, 0, 0, numVerts, 0, numVerts * 8 / 6 );

	DSX->m_pFX->EndPass();
	DSX->m_pFX->End();
}