#include "MultiAnimationControl.h"
#include "../../GraphicsCore.h"

//Animation Instance++++++++++
AnimationInstance::AnimationInstance()
{
	m_pAC = 0;
}
AnimationInstance::AnimationInstance(int animID, LPD3DXANIMATIONCONTROLLER AC)
{
	m_pAC = AC;
	m_ID = animID;
	m_currentTrack = 0;
	m_numberofTracks = m_pAC->GetMaxNumTracks();
	for(int i = 0; i < m_numberofTracks; i++)
		m_pAC->SetTrackEnable(i, false);
}
AnimationInstance::AnimationInstance(int animID,LPD3DXANIMATIONCONTROLLER AC, unsigned int Track,std::string AnimTrack,float Weight, float Speed,float Priority)
{
	m_pAC = AC;
	m_ID = animID;
	m_currentTrack = 0;
	m_totalWeight = Weight;
	m_numberofTracks = m_pAC->GetMaxNumTracks();
	for(int i = 0; i < m_numberofTracks; i++)
		m_pAC->SetTrackEnable(i, false);
	SetAnimationSet(animID,Track,AnimTrack,Weight,Speed,Priority);
}
AnimationInstance::~AnimationInstance()
{
}
void AnimationInstance::Cleanup()
{
    if( m_pAC )
	{
        m_pAC->Release();
		m_pAC = 0;
	}
}
LPD3DXANIMATIONCONTROLLER AnimationInstance::GetAnimController()
{
	if (m_pAC != NULL)
		return m_pAC;
	else
		return NULL;
}
void AnimationInstance::SetWorldTransform( const D3DXMATRIX* pmxWorld)
{
	m_mxWorld = *pmxWorld;
}
void AnimationInstance::SetAnimationSet(int animID, unsigned int Track,std::string AnimTrack,float Weight, float Speed,float Priority)
{
	int m_currentTrack = animID;	// (Think Jonathan means this) //0;

	ID3DXAnimationSet* Animation = 0;

	//Try to get the Track by the passed in name.  If you cant find it then resort to the track id
	if (AnimTrack.c_str() != "")
	{
		m_pAC->GetAnimationSetByName(AnimTrack.c_str(), &Animation);
	}
	if (Animation == NULL)
	{
		m_pAC->GetAnimationSet(Track, &Animation);
	}
	m_pAC->SetTrackAnimationSet( m_currentTrack, Animation);
	m_pAC->SetTrackWeight( m_currentTrack, Weight);
	m_pAC->SetTrackEnable( m_currentTrack, false);
	m_pAC->SetTrackSpeed( m_currentTrack, Speed);
	m_pAC->SetTrackPriority( m_currentTrack, D3DXPRIORITY_HIGH);  //Cast to the d3dx priority enum
	m_pAC->ResetTime();
	
	// Clean Up
	Animation->Release();
	Animation = 0;
}
bool AnimationInstance::SetAnimationToChannel(unsigned int channel, std::string AnimTrack, float Weight, float Speed, float Priority)
{
	//Method is used to add other animations besides the default one
	ID3DXAnimationSet* Animation = 0;

	// Try to get the animation by the passed in animation name.
	if (AnimTrack.c_str() != "")
	{
		m_pAC->GetAnimationSetByName(AnimTrack.c_str(), &Animation);
	}
	if (Animation == NULL)	
		return false;				// Animation was not found
	m_CurrentAnimation = AnimTrack;

	// Set animation
	m_pAC->SetTrackAnimationSet( channel, Animation);
	m_pAC->SetTrackWeight( channel, Weight);
	m_pAC->SetTrackEnable( channel, false);
	m_pAC->SetTrackSpeed( channel, Speed);
	m_pAC->SetTrackPriority( channel, D3DXPRIORITY_HIGH);  //Cast to the d3dx priority enum
	m_pAC->ResetTime();
	
	// Clean Up
	Animation->Release();
	Animation = 0;
	return true;
}
bool AnimationInstance::SetAnimationToChannel(unsigned int channel, unsigned int Track, float Weight, float Speed, float Priority)
{
	//Method is used to add other animations besides the default one
	ID3DXAnimationSet* Animation = 0;

	// Find the animation with the track number
	m_pAC->GetAnimationSet(Track, &Animation);
	if (Animation == NULL)
		return false;				// Animation was not found

	// Set Animation
	m_pAC->SetTrackAnimationSet( channel, Animation);
	m_pAC->SetTrackWeight( channel, Weight);
	m_pAC->SetTrackEnable( channel, false);
	m_pAC->SetTrackSpeed( channel, Speed);
	m_pAC->SetTrackPriority( channel, D3DXPRIORITY_HIGH);  //Cast to the d3dx priority enum
	m_pAC->ResetTime();

	// Clean Up
	Animation->Release();
	Animation = 0;
	return true;
}
void AnimationInstance::AddBlendedAnimation(std::string Animation, float transitionTime)
{
	m_currentTime += GCI->GetTimeElapsed();

	if (!Animation.compare(m_CurrentAnimation))
		return;

	// Remember current animation
	m_CurrentAnimation = Animation;

	// Get the animation set from the controller
	LPD3DXANIMATIONSET set = 0;
	m_pAC->GetAnimationSetByName(Animation.c_str(), &set );	

	if(!set)
		return;	// Animation was not found

	// Note: for a smooth transition between animation sets we can use two tracks and assign the new set to the track
	// not currently playing then insert Keys into the KeyTrack to do the transition between the tracks
	// tracks can be mixed together so we can gradually change into the new animation

	// Alternate tracks
	DWORD newTrack = ( m_currentTrack == 0 ? 1 : 0 );

	// Assign to our track
	m_pAC->SetTrackAnimationSet( newTrack, set );
    set->Release();	

	// Clear any track events currently assigned to our two tracks
	m_pAC->UnkeyAllTrackEvents( m_currentTrack );
    m_pAC->UnkeyAllTrackEvents( newTrack );

	// Add an event key to disable the currently playing track kMoveTransitionTime seconds in the future
    m_pAC->KeyTrackEnable( m_currentTrack, FALSE, m_currentTime + transitionTime );
	// Add an event key to change the speed right away so the animation completes in kMoveTransitionTime seconds
    m_pAC->KeyTrackSpeed( m_currentTrack, 0.0f, m_currentTime, transitionTime, D3DXTRANSITION_LINEAR );
	// Add an event to change the weighting of the current track (the effect it has blended with the secon track)
    m_pAC->KeyTrackWeight( m_currentTrack, 0.0f, m_currentTime, transitionTime, D3DXTRANSITION_LINEAR );

	// Enable the new track
    m_pAC->SetTrackEnable( newTrack, TRUE );
	// Add an event key to set the speed of the track
    m_pAC->KeyTrackSpeed( newTrack, 1.0f, m_currentTime, transitionTime, D3DXTRANSITION_LINEAR );
	// Add an event to change the weighting of the current track (the effect it has blended with the first track)
	// As you can see this will go from 0 effect to total effect(1.0f) in kMoveTransitionTime seconds and the first track goes from 
	// total to 0.0f in the same time.
    m_pAC->KeyTrackWeight( newTrack, 1.0f, m_currentTime, transitionTime, D3DXTRANSITION_LINEAR );

	// Remember current track
    m_currentTrack = newTrack;
}

void AnimationInstance::AddBlendedAnimation(unsigned int Animation, float transitionTime)
{
	m_currentTime += GCI->GetTimeElapsed();

	// Remember current animation
	//m_CurrentAnimation = Animation;

	// Get the animation set from the controller
	LPD3DXANIMATIONSET set = 0;
	m_pAC->GetAnimationSet(Animation, &set );	

	if(!set)
		return;	// Animation was not found

	// Note: for a smooth transition between animation sets we can use two tracks and assign the new set to the track
	// not currently playing then insert Keys into the KeyTrack to do the transition between the tracks
	// tracks can be mixed together so we can gradually change into the new animation

	// Alternate tracks
	DWORD newTrack = ( m_currentTrack == 0 ? 1 : 0 );

	// Assign to our track
	m_pAC->SetTrackAnimationSet( newTrack, set );
    set->Release();	

	// Clear any track events currently assigned to our two tracks
	m_pAC->UnkeyAllTrackEvents( m_currentTrack );
    m_pAC->UnkeyAllTrackEvents( newTrack );

	// Add an event key to disable the currently playing track kMoveTransitionTime seconds in the future
    m_pAC->KeyTrackEnable( m_currentTrack, FALSE, m_currentTime + transitionTime );
	// Add an event key to change the speed right away so the animation completes in kMoveTransitionTime seconds
    m_pAC->KeyTrackSpeed( m_currentTrack, 0.0f, m_currentTime, transitionTime, D3DXTRANSITION_LINEAR );
	// Add an event to change the weighting of the current track (the effect it has blended with the secon track)
    m_pAC->KeyTrackWeight( m_currentTrack, 0.0f, m_currentTime, transitionTime, D3DXTRANSITION_LINEAR );

	// Enable the new track
    m_pAC->SetTrackEnable( newTrack, TRUE );
	// Add an event key to set the speed of the track
    m_pAC->KeyTrackSpeed( newTrack, 1.0f, m_currentTime, transitionTime, D3DXTRANSITION_LINEAR );
	// Add an event to change the weighting of the current track (the effect it has blended with the first track)
	// As you can see this will go from 0 effect to total effect(1.0f) in kMoveTransitionTime seconds and the first track goes from 
	// total to 0.0f in the same time.
    m_pAC->KeyTrackWeight( newTrack, 1.0f, m_currentTime, transitionTime, D3DXTRANSITION_LINEAR );

	// Remember current track
    m_currentTrack = newTrack;
}
void AnimationInstance::NextAnimation()
{	
}
void AnimationInstance::PlayChannel(unsigned int channel)
{
	m_currentTrack = channel;
	m_pAC->SetTrackEnable(channel, true);
}
void AnimationInstance::PauseChannel(unsigned int channel)
{
	if(channel == m_currentTrack)
		m_currentTrack = m_lastTrack;			// This means Current Track is unknown
	m_pAC->SetTrackEnable(channel, false);
}

void AnimationInstance::Update(double dt)
{
	m_pAC->AdvanceTime(dt, 0);
}

void AnimationInstance::SetMeshName(std::string meshName)
{	m_AnimatedMesh = meshName;	}
std::string AnimationInstance::GetMeshName()
{	return m_AnimatedMesh;	}