#pragma once

#include "AnimatedMesh.h"

/*
class AnimationCallbackHandler : public ID3DXAnimationCallbackHandler
{
public:
	HRESULT CALLBACK HandleCallback(
		THIS_ UINT Track, LPVOID pCallbackData);
};*/

class AnimationInstance
{
	std::string m_AnimatedMesh;
    LPD3DXANIMATIONCONTROLLER m_pAC;
    D3DXMATRIX m_mxWorld;
	int m_ID;
	
	// Animation attributes
	unsigned int m_currentAnimationSet;	
	unsigned int m_numAnimationSets;
	unsigned int m_currentTrack;
	unsigned int m_lastTrack;
	unsigned int m_numberofTracks;
	float m_currentTime;
	float m_speedAdjust;
	float m_totalWeight;
	std::string m_CurrentAnimation;

	// Only use this for constructor, Will find animation via AnimTrack or Track
	void SetAnimationSet(int animID, unsigned int Track, std::string AnimTrack, float Weight, float Speed, float Priority);

public:
     AnimationInstance();
	 AnimationInstance(int animID, LPD3DXANIMATIONCONTROLLER AC);
	 AnimationInstance(int animID, LPD3DXANIMATIONCONTROLLER AC, unsigned int Track, 
					std::string AnimTrack, float Weight, float Speed, float Priority); 	 
	 ~AnimationInstance();
    void    Cleanup();

	int GetId(){return m_ID;}
	void SetMeshName(std::string meshName);
	std::string GetMeshName();
	
	void AddBlendedAnimation(unsigned int Animation, float transitionTime);
	void AddBlendedAnimation(std::string animation, float transitionTime);
	bool SetAnimationToChannel(unsigned int channel, std::string AnimTrack, float Weight, float Speed, float Priority);
	bool SetAnimationToChannel(unsigned int channel, unsigned int Track, float Weight, float Speed, float Priority);

	void PlayChannel(unsigned int Track);
	void PauseChannel(unsigned int Track);

	void NextAnimation();
	void Update(double dt);
    LPD3DXANIMATIONCONTROLLER  GetAnimController();
	unsigned int GetCurrentAnimationSet() const {return m_currentAnimationSet;}

	D3DXMATRIX  GetWorldTransform(){return m_mxWorld;}
    void        SetWorldTransform( const D3DXMATRIX* pmxWorld );
};