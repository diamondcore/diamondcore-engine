#include "AnimationManager.h"
#include "../../GraphicsCore.h"
#include "../../Utility.h"
#include "../../MacroTools.h"

const float kMoveTransitionTime=8.0f;

// Constructor
AnimationManager::AnimationManager()
{
}
// Destructor
AnimationManager::~AnimationManager(void)
{
}
void AnimationManager::Shutdown()
{
	for (unsigned int i = 0; i < registry.size(); i ++)
		 registry[i]->Cleanup();
	 registry.clear();

	 for(auto i = m_AnimatedMeshMap.begin();
		 i != m_AnimatedMeshMap.end();
		 i++)
		 (*i).second->Unload();
	 m_AnimatedMeshMap.clear();
}

bool AnimationManager::LoadAnimatedMesh(std::string filename)
{
	if(m_AnimatedMeshMap.find(filename) != m_AnimatedMeshMap.end())
		return true;	// It's already loaded

	// Try and load the animated mesh
	std::shared_ptr<AnimatedMesh> temp = std::shared_ptr<AnimatedMesh>(new AnimatedMesh);
	if(!temp->Load(filename))
		return false;
	// Insert animated mesh into map
	m_AnimatedMeshMap.insert( std::pair<std::string, std::shared_ptr<AnimatedMesh>>(filename, temp) );
	return true;
}
void AnimationManager::UnloadAnimatedMesh(std::string filename)
{
	auto iter = m_AnimatedMeshMap.find(filename);
	if(iter == m_AnimatedMeshMap.end())
		return;		// Mesh was not found in map

	// Unload mesh and remove from map
	iter->second->Unload();
	iter->second = 0;
	m_AnimatedMeshMap.erase( iter );
}

bool AnimationManager::CreateAnimController(std::string AnimMeshName, int id)
{
	auto iter = m_AnimatedMeshMap.find(AnimMeshName);
	if(iter == m_AnimatedMeshMap.end())
		return false;	// Mesh was not found

	if(!iter->second)	// Mesh not valid
		return false;

	
	LPD3DXANIMATIONCONTROLLER mainAC = iter->second->GetAnimationMainController();
	LPD3DXANIMATIONCONTROLLER pNewAC = NULL;

    // Clone the original AC.  This clone is what we will use to animate
    // this mesh; the original never gets used except to clone, since we
    // always need to be able to add another instance at any time.
	HRESULT hr = mainAC->CloneAnimationController(
							mainAC->GetMaxNumAnimationOutputs(),
                            mainAC->GetMaxNumAnimationSets(),
                            mainAC->GetMaxNumTracks(),
                            mainAC->GetMaxNumEvents(),
                                          &pNewAC );	
	if(FAILED(hr))
		return false;
	
	// Need to create a new pointer location for every instance and store it in the registry
	registry.insert(std::pair<int, std::shared_ptr<AnimationInstance>>
		(id, std::shared_ptr<AnimationInstance>(new AnimationInstance(id, pNewAC))));
	registry[id]->SetMeshName(AnimMeshName);
	return true;
}
void AnimationManager::DeleteAnimController(int id)
{
	auto iter = registry.find(id);
	if( iter == registry.end() )	// Check if the id is in the register
		return;

	// Clear controller data
	registry[id]->Cleanup();
	registry[id] = 0;

	// Remove from list
	registry.erase( iter );
}

bool AnimationManager::SetAnimationToChannel(int animID, unsigned int channel, std::string AnimTrack, float Weight, float Speed, float Priority)
{
	if(registry.find(animID) == registry.end())	// Animation ID not found
		return false;
	//registry[animID]->AddBlendedAnimation(AnimTrack, 1.0f);
	registry[animID]->SetAnimationToChannel(channel, AnimTrack, Weight, Speed, Priority);
	return true;
}
bool AnimationManager::SetAnimationToChannel(int animID, unsigned int channel, unsigned int Track, float Weight, float Speed, float Priority)
{
	if(registry.find(animID) == registry.end())	// Animation ID not found
		return false;
	//registry[animID]->AddBlendedAnimation(Track, 1.0f);
	registry[animID]->SetAnimationToChannel(channel, Track, Weight, Speed, Priority);
	return true;
}

void AnimationManager::PlayChannel(int animId, unsigned int channel)
{
	if(registry.find(animId) == registry.end())	// Animation ID not found
		return;
	registry[animId]->PlayChannel(channel);
}
void AnimationManager::PauseChannel(int animId, unsigned int channel)
{
	if(registry.find(animId) == registry.end())	// Animation ID not found
		return;
	registry[animId]->PauseChannel(channel);
}

//void AnimationManager::PlayAllTracks(int animId)
//{
//	//registry[animId]->PlayAllTracks();
//}
//void AnimationManager::PauseAllTracks(int animId)
//{
//	//registry[animId]->PauseAllTracks();
//}

void AnimationManager::PreRender()
{
	DeferredShader* DSX = GCI->GetDeferredR();
	// Use Vertex Blending
	DSX->m_pFX->SetBool("g_sUseVBlend", true);
	DSX->m_pFX->SetBool("g_sIsTex", true);
}
void AnimationManager::PostRender()
{
	// Turn off Vertex Blending
	GCI->GetDeferredR()->m_pFX->SetBool("g_sUseVBlend", false);
}
void AnimationManager::AdvanceTime(int animID, float ElapsedTime)
{
	registry[animID]->Update(ElapsedTime);
}

void AnimationManager::RenderAnimatedMesh(int animID, D3DXMATRIX transform)
{
	DeferredShader* DSX = GCI->GetDeferredR();
	
	// Calculate World Inverse Transform
	D3DXMATRIX worldInvTrans;
	D3DXMatrixInverse(&worldInvTrans, 0, &transform);
	D3DXMatrixTranspose(&worldInvTrans, &worldInvTrans);
	
	// Set the world transform
	DSX->m_pFX->SetMatrix("g_sWorld", &transform);
	DSX->m_pFX->SetMatrix("g_sWorldInvTrans", &worldInvTrans);
	DSX->m_pFX->SetMatrix("g_sWVP", &(transform*GCI->GetCamera()->GetViewProj()));
	DSX->m_pFX->SetMatrix("g_sLightVP", &(transform*DSX->m_Light.GetLightViewProj()));
	//DSX->m_pFX->SetMatrix("g_sLightV", &(transform * DSX->m_Light.GetLightView() ));

	// Update animation
	registry[animID]->Update(GCI->GetTimeElapsed());
	
	// Render animation
	m_AnimatedMeshMap[registry[animID]->GetMeshName()]->Render();
}
void AnimationManager::RenderAnimatedMesh(int animID, float AdvancedTime, D3DXMATRIX transform)
{
	DeferredShader* DSX = GCI->GetDeferredR();
	
	// Calculate World Inverse Transform
	D3DXMATRIX worldInvTrans;
	D3DXMatrixInverse(&worldInvTrans, 0, &transform);
	D3DXMatrixTranspose(&worldInvTrans, &worldInvTrans);
	
	// Set the world transform
	DSX->m_pFX->SetMatrix("g_sWorld", &transform);
	DSX->m_pFX->SetMatrix("g_sWorldInvTrans", &worldInvTrans);
	DSX->m_pFX->SetMatrix("g_sWVP", &(transform*GCI->GetCamera()->GetViewProj()));
	DSX->m_pFX->SetMatrix("g_sLightVP", &(transform*DSX->m_Light.GetLightViewProj()));
	//DSX->m_pFX->SetMatrix("g_sLightV", &(transform * DSX->m_Light.GetLightView() ));

	// Update animation
	registry[animID]->Update(AdvancedTime);
	
	// Render animation
	m_AnimatedMeshMap[registry[animID]->GetMeshName()]->Render();
}
void AnimationManager::RenderAnimatedMesh(int animID, std::string Texture, D3DXMATRIX transform)
{
	DeferredShader* DSX = GCI->GetDeferredR();
	
	// Calculate World Inverse Transform
	D3DXMATRIX worldInvTrans;
	D3DXMatrixInverse(&worldInvTrans, 0, &transform);
	D3DXMatrixTranspose(&worldInvTrans, &worldInvTrans);
	
	// Set the world transform
	DSX->m_pFX->SetMatrix("g_sWorld", &transform);
	DSX->m_pFX->SetMatrix("g_sWorldInvTrans", &worldInvTrans);
	DSX->m_pFX->SetMatrix("g_sWVP", &(transform*GCI->GetCamera()->GetViewProj()));
	DSX->m_pFX->SetMatrix("g_sLightVP", &(transform*DSX->m_Light.GetLightViewProj()));
	//DSX->m_pFX->SetMatrix("g_sLightV", &(transform * DSX->m_Light.GetLightView() ));

	// Update animation
	registry[animID]->Update(GCI->GetTimeElapsed());
	
	// Render animation
	m_AnimatedMeshMap[registry[animID]->GetMeshName()]->Render(Texture);
}
void AnimationManager::RenderAnimatedMesh(int animID, std::string Texture, D3DXMATRIX transform, D3DXCOLOR color)
{
	DeferredShader* DSX = GCI->GetDeferredR();
	
	// Calculate World Inverse Transform
	D3DXMATRIX worldInvTrans;
	D3DXMatrixInverse(&worldInvTrans, 0, &transform);
	D3DXMatrixTranspose(&worldInvTrans, &worldInvTrans);
	
	// Set the world transform
	DSX->m_pFX->SetMatrix("g_sWorld", &transform);
	DSX->m_pFX->SetMatrix("g_sWorldInvTrans", &worldInvTrans);
	DSX->m_pFX->SetMatrix("g_sWVP", &(transform*GCI->GetCamera()->GetViewProj()));
	DSX->m_pFX->SetMatrix("g_sLightVP", &(transform*DSX->m_Light.GetLightViewProj()));
	//DSX->m_pFX->SetMatrix("g_sLightV", &(transform * DSX->m_Light.GetLightView() ));

	// Update animation
	registry[animID]->Update(GCI->GetTimeElapsed());
	
	// Render animation
	m_AnimatedMeshMap[registry[animID]->GetMeshName()]->Render(Texture, color);
}
void AnimationManager::RenderAnimatedMesh(int animID, D3DXMATRIX transform, D3DXCOLOR color)
{
	DeferredShader* DSX = GCI->GetDeferredR();

	// Calculate World Inverse Transform
	D3DXMATRIX worldInvTrans;
	D3DXMatrixInverse(&worldInvTrans, 0, &transform);
	D3DXMatrixTranspose(&worldInvTrans, &worldInvTrans);
	
	// Set the world transform
	DSX->m_pFX->SetMatrix("g_sWorld", &transform);
	DSX->m_pFX->SetMatrix("g_sWorldInvTrans", &worldInvTrans);
	DSX->m_pFX->SetMatrix("g_sWVP", &(transform*GCI->GetCamera()->GetViewProj()));
	DSX->m_pFX->SetMatrix("g_sLightVP", &(transform*DSX->m_Light.GetLightViewProj()));
	//DSX->m_pFX->SetMatrix("g_sLightV", &(transform * DSX->m_Light.GetLightView() ));

	// Update animation
	registry[animID]->Update(GCI->GetTimeElapsed());
	
	// Render animation
	m_AnimatedMeshMap[registry[animID]->GetMeshName()]->Render(color);
}
void AnimationManager::RenderAnimatedMesh(std::vector<BaseObjectPtr> objects, bool shadowMap)					// Default texture render
{
	// Render all animated mesh with the default texture
	for(auto i = objects.begin();
		i != objects.end(); i++)
	{
		if((*i)->m_Color.a == -1.0f)	// Use default color
			RenderAnimatedMesh( (*i)->m_SharedMemID, (*i)->m_Tranform.GetTransform() );
		else
			RenderAnimatedMesh( (*i)->m_SharedMemID, (*i)->m_Tranform.GetTransform(), (*i)->m_Color );
	}
}
void AnimationManager::RenderAnimatedMesh(std::vector<BaseObjectPtr> objects, std::string texture)
{
	DeferredShader* DSX = GCI->GetDeferredR();

	// Prepare the shader with the texture and color
	std::string meshName = registry[(*objects.begin())->m_SharedMemID]->GetMeshName();
	m_AnimatedMeshMap[meshName]->PreRender(texture, (*objects.begin())->m_Color);
	
	float dt = GCI->GetTimeElapsed();

	// Render all animated mesh with the custom set texture
	for(auto i = objects.begin();
		i != objects.end(); i++)
	{
		D3DXMATRIX transform = (*i)->m_Tranform.GetTransform();

		if((*i)->m_Color.a != -1.0f)	// Use custom color
		{
			DSX->m_pFX->SetValue("g_sAmbientMtrl", &(*i)->m_Color, sizeof(D3DXCOLOR));
			DSX->m_pFX->SetValue("g_sDiffuseMtrl", &(*i)->m_Color, sizeof(D3DXCOLOR));
		}
		else // Use default color white
		{
			DSX->m_pFX->SetValue("g_sAmbientMtrl", &WHITE, sizeof(D3DXCOLOR));
			DSX->m_pFX->SetValue("g_sDiffuseMtrl", &WHITE, sizeof(D3DXCOLOR));
		}
		
		// Calculate World Inverse Transform
		D3DXMATRIX worldInvTrans;
		D3DXMatrixInverse(&worldInvTrans, 0, &transform);
		D3DXMatrixTranspose(&worldInvTrans, &worldInvTrans);
	
		// Set the world transform
		DSX->m_pFX->SetMatrix("g_sWorld", &transform);
		DSX->m_pFX->SetMatrix("g_sWorldInvTrans", &worldInvTrans);
		DSX->m_pFX->SetMatrix("g_sWVP", &(transform*GCI->GetCamera()->GetViewProj()));
		DSX->m_pFX->SetMatrix("g_sLightVP", &(transform*DSX->m_Light.GetLightViewProj()));
		//DSX->m_pFX->SetMatrix("g_sLightV", &(transform * DSX->m_Light.GetLightView() ));
		registry[(*i)->m_SharedMemID]->Update(dt);
		m_AnimatedMeshMap[meshName]->RenderRaw();
	}
}

// Getter
std::shared_ptr<AnimatedMesh> AnimationManager::GetAnimMesh(std::string meshName)
{	return m_AnimatedMeshMap[meshName];	}

