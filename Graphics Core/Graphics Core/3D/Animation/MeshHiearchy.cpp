#include "MeshHierarchy.h"
#include "../../GraphicsCore.h"
#include "../../Utility.h"
#include "../../MacroTools.h"

void convertToUnicode(const char* input, char** output)	
{
	if(input)
	{
		UINT length = (UINT)::strlen(input) + 1;
		*output = new char[length];
		::strcpy(*output, input);
	}
	else
		*output = 0;
}
HRESULT CMeshHierarchy::CreateFrame(LPCSTR Name, D3DXFRAME** pNewFrame)
{
	BoneFrame* boneFrame = new BoneFrame;

	// Check if Name is valid
	// Then convert from PCSTR to Unicode String
	if(Name)	
		convertToUnicode(Name, &boneFrame->Name);
	else		
		convertToUnicode("<no name>", &boneFrame->Name);

	boneFrame->pMeshContainer = 0;
	boneFrame->pFrameSibling = 0;
	boneFrame->pFrameFirstChild = 0;
	D3DXMatrixIdentity(&boneFrame->TransformationMatrix);
	D3DXMatrixIdentity(&boneFrame->toRoot);

	*pNewFrame = boneFrame;

	return D3D_OK;	// Returns a 0 if Failed

}

HRESULT CMeshHierarchy::CreateMeshContainer(
				PCSTR Name, 
				const D3DXMESHDATA* pMeshData,		const D3DXMATERIAL*	pMtrls,
				const D3DXEFFECTINSTANCE* pEffect,	DWORD NumMtrls,
				const DWORD *pAdj,					ID3DXSkinInfo* pSkinInfo,
				D3DXMESHCONTAINER** pNewContainer)
{    
	D3DXMESHCONTAINER_EXTENDED* meshContainer = new D3DXMESHCONTAINER_EXTENDED;
	ZeroMemory(meshContainer, sizeof(D3DXMESHCONTAINER_EXTENDED));

	// Check if Name is valid
	// Then convert from PCSTR to Unicode String
	if(Name)	
		convertToUnicode(Name, &meshContainer->Name);	// Set the container's Name
	else		
		convertToUnicode("<no name>", &meshContainer->Name);

	// Set the path to this mesh (used for setting texture path)
	std::string path = meshContainer->Name;
	path = path.substr(0, path.find_last_of("/") + 1 );	// Go all the way to the last folder

	*pNewContainer = (D3DXMESHCONTAINER*)meshContainer;	// Pass back the newly created container

	// Check if the Mesh is Skinned
	if(pSkinInfo == 0 || pMeshData->Type != D3DXMESHTYPE_MESH)
		return D3D_OK;	// Return if plain mesh

	// Finish loading the rest of the Mesh Container
	meshContainer->NumMaterials	= NumMtrls;
	meshContainer->exMaterials	= new D3DMATERIAL9[NumMtrls];
	meshContainer->exTexture	= new IDirect3DTexture9*[NumMtrls];
	for(DWORD i = 0; i < NumMtrls; i++)
	{
		D3DMATERIAL9* mtrl	= meshContainer->exMaterials;
		mtrl[i].Ambient		= pMtrls[i].MatD3D.Diffuse;		// Overwrite the ambient with the diffuse
		mtrl[i].Diffuse		= pMtrls[i].MatD3D.Diffuse;
		mtrl[i].Specular	= pMtrls[i].MatD3D.Specular;
		mtrl[i].Power		= pMtrls[i].MatD3D.Power;

		// Check if there is an Associated Texture with the Material
		// Convert the Texture file name to Unicode string
		char* tex;
		convertToUnicode(pMtrls[i].pTextureFilename, &tex);
		std::string textureName = path;
		textureName += tex;
		SAFE_DELETE_ARRAY(tex);

		// Create Textures
		if(pMtrls[i].pTextureFilename)
		{	
			HRESULT hr = D3DXCreateTextureFromFile(GCI->GetDirectX()->GetDevice(), 
				pMtrls[i].pTextureFilename, &meshContainer->exTexture[i]);
			
			if(FAILED(hr))
				meshContainer->exTexture[i]	= 0;
		}
		else
			meshContainer->exTexture[i]	= 0;
	}

	// Add Effect informatio if needed
	meshContainer->pEffects = 0;
	meshContainer->pAdjacency = 0;
	
	// Save the Mesh and Skin Information
	meshContainer->MeshData.Type	= D3DXMESHTYPE_MESH;
	meshContainer->MeshData.pMesh	= pMeshData->pMesh;
	meshContainer->pSkinInfo		= pSkinInfo;
	pMeshData->pMesh->AddRef();
	pSkinInfo->AddRef();

	return D3D_OK;
}

HRESULT CMeshHierarchy::DestroyFrame(LPD3DXFRAME frameToFree) 
{
	BoneFrame* frame = (BoneFrame*)frameToFree;
	SAFE_DELETE_ARRAY(frame->Name);
	SAFE_DELETE(frame);
	//delete [] frameToFree->Name;
	//delete frameToFree;

	return D3D_OK;
}

HRESULT CMeshHierarchy::DestroyMeshContainer(LPD3DXMESHCONTAINER meshContainerBase)
{
	D3DXMESHCONTAINER_EXTENDED* container = (D3DXMESHCONTAINER_EXTENDED*)(meshContainerBase);
	SAFE_DELETE_ARRAY(container->Name);
	//delete [] meshContainerBase->pAdjacency;
	//delete [] meshContainerBase->pEffects;

	for(DWORD i = 0; i < container->NumMaterials; i++)
	{
		SAFE_RELEASE( container->exTexture[i] );
		//delete [] meshContainerBase->pMaterials[i].pTextureFilename;
	}
	//delete [] meshContainerBase->pMaterials;
	SAFE_DELETE_ARRAY(container->exMaterials);
	SAFE_DELETE_ARRAY(container->exTexture);

	SAFE_RELEASE(container->MeshData.pMesh);
	SAFE_RELEASE(container->pSkinInfo);

	SAFE_DELETE(container);

	return D3D_OK;
}
