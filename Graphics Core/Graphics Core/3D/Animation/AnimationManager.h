#pragma once

#include "MultiAnimationControl.h"
#include "../../SceneGraph/ObjectList.h"
#include <map>
#include <queue>

class AnimationManager
{
	std::map<int, std::shared_ptr<AnimationInstance>>		registry; //holds all the Animation Instances
	std::map<std::string, std::shared_ptr<AnimatedMesh>>	m_AnimatedMeshMap;

public:
	AnimationManager();
	~AnimationManager();
	void Shutdown();  //Clear everything out when the world clears

	bool LoadAnimatedMesh(std::string filename);
	void UnloadAnimatedMesh(std::string filename);
	std::shared_ptr<AnimationInstance>	GetInstancebyID(int id);
	std::shared_ptr<AnimatedMesh>		GetAnimMesh(std::string meshName);
	
	bool CreateAnimController(std::string AnimMeshName, int id);
	void DeleteAnimController(int id);

	bool SetAnimationToChannel(int animID, unsigned int channel, unsigned int Track, float Weight, float Speed, float Priority);
	bool SetAnimationToChannel(int animID, unsigned int channel, std::string AnimTrack, float Weight, float Speed, float Priority);
	void AdvanceTime(int animID, float ElapsedTime);
	void ResetTime(int animID);
	void SetCallback(int animID, float TriggerTime/*in Callback*/);

	void PlayChannel(int animId, unsigned int channel);
	void PauseChannel(int animId, unsigned int channel);
	//void PlayAllTracks(int animId);
	//void PauseAllTracks(int animId);

	// Must use these before and after rendering animations
	void PreRender();
	void PostRender();

	//Render Functions
	void RenderAnimatedMesh(std::vector<BaseObjectPtr> objects, bool shadowMap);					// Default texture render
	void RenderAnimatedMesh(std::vector<BaseObjectPtr> objects, std::string texture); 
	
	void RenderAnimatedMesh(int animID, D3DXMATRIX transform);						// Default texture render
	void RenderAnimatedMesh(int animID, D3DXMATRIX transform, D3DXCOLOR color);		// Use custom color and default texture
	void RenderAnimatedMesh(int animID, float AdvancedTime, D3DXMATRIX transform);	// Default texture render
	void RenderAnimatedMesh(int animID, std::string Texture, D3DXMATRIX transform);	// Use custom texture
	void RenderAnimatedMesh(int animID, std::string Texture, D3DXMATRIX transform, D3DXCOLOR color);	// Use custom texture and color
};