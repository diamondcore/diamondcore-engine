#pragma once

#include <d3dx9.h>

class CMeshHierarchy : public ID3DXAllocateHierarchy
{
public:	
	// Constructors
	HRESULT STDMETHODCALLTYPE	CreateFrame(THIS_ PCSTR Name,
									D3DXFRAME** pNewFrame);

	HRESULT STDMETHODCALLTYPE	CreateMeshContainer(PCSTR Name,
									const D3DXMESHDATA* pMeshData,
									const D3DXMATERIAL* pMtrls,
									const D3DXEFFECTINSTANCE* pEffect,
									DWORD NumMtrls,
									const DWORD *pAdj,
									ID3DXSkinInfo*	pSkinInfo,
									D3DXMESHCONTAINER** pNewContainer);

	// Destructors
	HRESULT STDMETHODCALLTYPE	DestroyFrame(THIS_ D3DXFRAME* pFrameToDestroy);
	
	HRESULT STDMETHODCALLTYPE	DestroyMeshContainer(THIS_ D3DXMESHCONTAINER* pMeshContainer);
};

struct D3DXMESHCONTAINER_EXTENDED: public D3DXMESHCONTAINER
{
	// The base D3DXMESHCONTAINER has a pMaterials member which is a D3DXMATERIAL structure 
	// that contains a texture filename and material data. It is easier to ignore this and 
	// instead store the data in arrays of textures and materials in this extended structure:
	IDirect3DTexture9**	 exTexture;			// Array of textures
	D3DMATERIAL9*		 exMaterials;		// Array of materials
                                
	// Skinned mesh variables
	//ID3DXMesh*           exSkinMesh;			// The skin mesh
	//D3DXMATRIX*			 exBoneOffsets;			// The bone matrix Offsets, one per bone
	//D3DXMATRIX**		 exFrameCombinedMatrixPointer;	// Array of frame matrix pointers
};

struct BoneFrame: public D3DXFRAME
{
    D3DXMATRIX toRoot;
};
