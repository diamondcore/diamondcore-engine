#pragma once

#include "MeshHierarchy.h"
#include <vector>

class AnimatedMesh
{
	std::string					m_FileName;
	ID3DXMesh*					m_pSkinedMesh;
	BoneFrame*					m_pRootMesh;
	ID3DXSkinInfo*				m_pSkinInfo;
	ID3DXAnimationController*	m_animController;	
	DWORD						m_NumBones;			// Total number of bones
	DWORD						m_MaxVertsPerBone;	// Maximum number of bones that can influence a vertex
	static	const int			MAX_NUM_BONES = 42;	// Max number of bones supported
	
	std::vector<D3DXMATRIX>		m_vFinalForm;
	std::vector<D3DXMATRIX*>	m_vToRootPtr;		// To-root Transformation matrix
	
	// Textures and Materials
	D3DXMESHCONTAINER_EXTENDED*	m_MeshAssets;
		
	//Utility Functions
	D3DXFRAME* findOneMeshNode(D3DXFRAME* frame);
	void buildMesh(ID3DXMesh* mesh); //Reformat vertex format to support blend weights and indices
	void CalculateBoundingVolumes();
	bool hasNormals(ID3DXMesh* mesh);
	
	void findToRootXTreePtrs();					// Find the pointers to the toRoot transformation matrix for each node
	void buildToRootXTree(BoneFrame* frame, D3DXMATRIX& parentTrees);

	// Bounding Volume Attributes
	D3DXVECTOR3	m_sphereCentre;
	float		m_sphereRadius;
	D3DXVECTOR3	minBounds, maxBounds;
	D3DXMATRIX	m_OffsetMat;

public:
	AnimatedMesh();
	~AnimatedMesh();

	bool	Load(std::string meshName);
	void	Unload();
	bool	operator== (const std::string& name);

	// Render Functions
	void Render();
	void Render(D3DXCOLOR color);
	void Render(std::string Texture);
	void Render(std::string Texture, D3DXCOLOR color);

	// Custom Render
	void PreRender(std::string Texture, D3DXCOLOR color);
	void RenderRaw();

	// Get Bounding Volume attributes
	D3DXVECTOR3 getBV_HalfSize();
	D3DXVECTOR3 getBV_BoxCenter();
	float		getBV_Radius();
	D3DXVECTOR3 getBV_SphereCenter();
	D3DXMATRIX	getBV_Offset();

	// Get Mesh attributes
	UINT	numBones();
	const	D3DXMATRIX*	getFinalFormList();			// Pointer to m_vFinalForm	
	void	GetBoneList(std::vector<std::string>& boneList);
	D3DXMATRIX* GetCombinedMatrixFrom(std::string boneName) const;
	LPD3DXANIMATIONCONTROLLER GetAnimationMainController();
};