#include "AnimatedMesh.h"
#include "../../GraphicsCore.h"
#include "../../MacroTools.h"
#include "../../Utility.h"

AnimatedMesh::AnimatedMesh()
{
	m_pRootMesh			= 0;
	m_pSkinedMesh		= 0;
	m_pSkinInfo			= 0;
	m_animController	= 0;
}
AnimatedMesh::~AnimatedMesh()
{
}
void AnimatedMesh::Unload()
{	
	if (m_pRootMesh)
	{
		// Create a mesh heirarchy class to control the removal of memory for the frame heirarchy
		CMeshHierarchy memoryAllocator;
		D3DXFrameDestroy(m_pRootMesh, &memoryAllocator);
		m_pRootMesh=0;
	}
	if (m_pSkinedMesh)
	{
		m_pSkinedMesh->Release();
		m_pSkinedMesh =NULL;
	}
	SAFE_RELEASE(m_pSkinInfo);
	SAFE_RELEASE(m_animController);
}

bool AnimatedMesh::Load(std::string name)
{
	// Create our mesh hierarchy class to control the allocation of memory - only used temporarily
	CMeshHierarchy memoryAllocator;

	std::string xfilePath = "Content/Meshes/" + name.substr(0, name.size()-2);	// Add name without the ".x"
	xfilePath += "/";
	xfilePath += name;

	// This is the function that does all the .x file loading. We provide a pointer to an instance of our 
	// memory allocator class to handle memory allocationm during the frame and mesh loading
	HRESULT hr = D3DXLoadMeshHierarchyFromX(xfilePath.c_str(), D3DXMESH_MANAGED, GCI->GetDirectX()->GetDevice(), 
		&memoryAllocator, NULL, (D3DXFRAME**)&m_pRootMesh, &m_animController);

	if (CUtility::FailedHr(hr))
		return false; 

	// Set the file name
	m_FileName = name;

	D3DXFRAME* f = findOneMeshNode(m_pRootMesh);
	if(f == 0)
		 hr = E_FAIL;

	D3DXMESHCONTAINER_EXTENDED* meshContainer = (D3DXMESHCONTAINER_EXTENDED*)f->pMeshContainer;
	m_pSkinInfo = meshContainer->pSkinInfo;
	m_pSkinInfo->AddRef();

	m_NumBones = meshContainer->pSkinInfo->GetNumBones();
	m_vFinalForm.resize(m_NumBones);
	m_vToRootPtr.resize(m_NumBones, 0);


	buildMesh(meshContainer->MeshData.pMesh);
	m_MeshAssets = meshContainer;
	findToRootXTreePtrs();
	return true;
}
D3DXFRAME* AnimatedMesh::findOneMeshNode(D3DXFRAME* frame)
{
	if(frame->pMeshContainer)
		if(frame->pMeshContainer->MeshData.pMesh != 0)
			return frame;

	D3DXFRAME* f = 0;
	if(frame->pFrameSibling)
		if(f = findOneMeshNode(frame->pFrameSibling))	// Set f
			return f;

	if(frame->pFrameFirstChild)
		if(f = findOneMeshNode(frame->pFrameFirstChild)) // Set f
			return f;
	return 0;
}
void AnimatedMesh::buildMesh(ID3DXMesh* mesh)
{
	// Convert Vertex format
	D3DVERTEXELEMENT9	elements[64];
	UINT numElements = 0;
	VertexBlendEx::Decl->GetDeclaration(elements, &numElements);

	ID3DXMesh* tempMesh = 0;
	HRESULT hr = mesh->CloneMesh(D3DXMESH_SYSTEMMEM, elements, GCI->GetDirectX()->GetDevice(), &tempMesh);

	// Add Normals if needed
	if(!hasNormals(tempMesh))
		hr = D3DXComputeNormals(tempMesh, 0);

	// Get Adjacent information
	DWORD* adj = new DWORD[tempMesh->GetNumFaces()*3];	// Number of triangles
	ID3DXBuffer* remap = 0;
	hr = tempMesh->GenerateAdjacency(EPSILON, adj);

	// Optimize the Mesh
	ID3DXMesh* optimizedMesh = 0;
	hr = tempMesh->Optimize(D3DXMESH_SYSTEMMEM | D3DXMESHOPT_VERTEXCACHE |
							D3DXMESHOPT_ATTRSORT, adj, 0, 0, &remap, &optimizedMesh);
	
	// some Clean up
	tempMesh->Release();
	tempMesh = NULL;
	delete [] adj;

	// Compute tangents for Deferred shader
	ID3DXMesh* tangMesh = 0;
	hr = D3DXComputeTangentFrameEx(optimizedMesh,
						D3DDECLUSAGE_TEXCOORD, 0,
						D3DDECLUSAGE_BINORMAL, 0,
						D3DDECLUSAGE_TANGENT, 0,
						D3DDECLUSAGE_NORMAL, 0,
						0, 0, 0.01f, 0.25f, 0.01f,
						&tangMesh, 0);
	// some Clean up
	SAFE_RELEASE(optimizedMesh);

	hr = m_pSkinInfo->Remap(tangMesh->GetNumVertices(),
							(DWORD*)remap->GetBufferPointer());
	// some Clean up
	remap->Release();
	remap = NULL;

	// Make room for Blend weights and bone indices in the vertex format
	DWORD numBoneCombos = 0;
	ID3DXBuffer* boneComboBuffer = 0;
	hr = m_pSkinInfo->ConvertToIndexedBlendedMesh(tangMesh,
							D3DXMESH_MANAGED | D3DXMESH_WRITEONLY,
							MAX_NUM_BONES, 0, 0, 0, 0, &m_MaxVertsPerBone,
							&numBoneCombos, &boneComboBuffer, &m_pSkinedMesh);

	// Calculate Bounding Volume
	CalculateBoundingVolumes();

	// Final Sweep
	SAFE_RELEASE(tangMesh);
	SAFE_RELEASE(boneComboBuffer);
}

void AnimatedMesh::CalculateBoundingVolumes()
{
//+++++++++++Calculate Bounding volumes+++++++++++++++++++++++++++++++++++++++++++++
	D3DXFrameCalculateBoundingSphere(m_pRootMesh, &m_sphereCentre, &m_sphereRadius);
	VertexBlendEx* pVertices=NULL;

	DWORD numBytes = m_pSkinedMesh->GetNumBytesPerVertex();
	m_pSkinedMesh->LockVertexBuffer(D3DLOCK_READONLY, (LPVOID*)&pVertices);

	D3DXComputeBoundingBox(&pVertices[0].position, m_pSkinedMesh->GetNumVertices(), numBytes, &minBounds, &maxBounds);

	m_pSkinedMesh->UnlockVertexBuffer();

	// Calculate offset
	auto pos = getBV_BoxCenter();
	D3DXMatrixTranslation(&m_OffsetMat, pos.x, pos.y, pos.z); 
//++++++++++End Calculate Bounding volumes+++++++++++++++++++++++++++++++++++++++++++
}
bool AnimatedMesh::hasNormals(ID3DXMesh* mesh)
{
	D3DVERTEXELEMENT9	Elements[MAX_FVF_DECL_SIZE];
	HRESULT hr = mesh->GetDeclaration(Elements);	// Get original vertex decleration

	// Check if the Mesh has vertex normals
	bool hasNormal = false;		
	for(int i = 0; i < MAX_FVF_DECL_SIZE; i++)
	{
		if(Elements[i].Stream == 0xff)	// Check for the end of the Elements, "D3DDECL_END()"
			break;

		if(Elements[i].Type			== D3DDECLTYPE_FLOAT3 &&
		   Elements[i].Usage		== D3DDECLUSAGE_NORMAL &&
		   Elements[i].UsageIndex	== 0)
		{
			hasNormal = true;
			break;
		}
	}
	return hasNormal;
}
void AnimatedMesh::findToRootXTreePtrs()
{
	for(UINT i = 0; i < m_NumBones; i++)
	{
		const char* boneName = m_pSkinInfo->GetBoneName(i);
		D3DXFRAME* frame = D3DXFrameFind(m_pRootMesh, boneName);
		if(frame)
		{
			BoneFrame* boneFrame = static_cast<BoneFrame*>(frame);
			m_vToRootPtr[i] = &boneFrame->toRoot;
		}
	}
}
void AnimatedMesh::buildToRootXTree(BoneFrame* frame, D3DXMATRIX& parentTrees)
{
	// Set the toRoot transformation Matrix for this node
	frame->toRoot = frame->TransformationMatrix * parentTrees;

	// Find Sibling and Children Nodes
	BoneFrame* siblingNode		= (BoneFrame*)frame->pFrameSibling;
	BoneFrame* firstChildNode	= (BoneFrame*)frame->pFrameFirstChild;

	// Recurse through sibling nodes
	if(siblingNode)
		buildToRootXTree(siblingNode, parentTrees);

	// Recurse through the first children nodes
	if(firstChildNode)
		buildToRootXTree(firstChildNode, frame->toRoot);
}
const D3DXMATRIX*	AnimatedMesh::getFinalFormList()
{
	return &m_vFinalForm[0];	// Return pointer the final forms
}
UINT AnimatedMesh::numBones()
{
	return m_NumBones;
}

void AnimatedMesh::GetBoneList(std::vector<std::string>& boneList)
{
	for(UINT i = 0; i < m_NumBones; i++)
		boneList.push_back( m_pSkinInfo->GetBoneName(i) );	// Get all the bone names
}
D3DXMATRIX* AnimatedMesh::GetCombinedMatrixFrom(std::string boneName) const
{
	D3DXFRAME* frame = D3DXFrameFind(m_pRootMesh, boneName.c_str());
	if(frame)
	{
		BoneFrame* frameEx = static_cast<BoneFrame*>(frame);
		return &frameEx->toRoot;
	}
	return 0;
}

void AnimatedMesh::Render(std::string Texture)
{
	if (!m_pRootMesh)
		return;
	DeferredShader* DSX = GCI->GetDeferredR();
	
	// Recurse down the tree, generating the transform toRoot matrix for the next frame
	D3DXMATRIX identity;
	D3DXMatrixIdentity(&identity);
	buildToRootXTree((BoneFrame*)m_pRootMesh, identity);

	// Build the final traformation matrix for each bone
	D3DXMATRIX offsetTemp, toRootTemp;
	for(UINT i = 0; i < m_NumBones; i++)
	{
		offsetTemp = *m_pSkinInfo->GetBoneOffsetMatrix(i);
		toRootTemp = *m_vToRootPtr[i];
		m_vFinalForm[i] = offsetTemp * toRootTemp;
	}	
	// Set the bone transforms
	DSX->m_pFX->SetMatrixArray("g_sFinalForms",getFinalFormList(),numBones());
	

	// Loop through all the materials in the mesh rendering each subset
	for (DWORD i = 0; i < m_MeshAssets->NumMaterials; i++)
	{
		// use the material in our extended data rather than the one in meshContainer->pMaterials[iMaterial].MatD3D
		DSX->m_pFX->SetValue("g_sAmbientMtrl", &m_MeshAssets->exMaterials[i].Ambient, sizeof(D3DXCOLOR));
		DSX->m_pFX->SetValue("g_sDiffuseMtrl", &m_MeshAssets->exMaterials[i].Diffuse, sizeof(D3DXCOLOR));
		DSX->m_pFX->SetValue("g_sSpecMtrl", &m_MeshAssets->exMaterials[i].Specular, sizeof(D3DXCOLOR));
		DSX->m_pFX->SetFloat("g_sSpecPower", m_MeshAssets->exMaterials[i].Power);
		if(GCI->GetTextureManager()->TextureList.find(Texture) != GCI->GetTextureManager()->TextureList.end())
			DSX->m_pFX->SetTexture("g_sTexture", GCI->GetTextureManager()->TextureList[Texture]->texture);
		else
			DSX->m_pFX->SetTexture("g_sTexture", DSX->m_pDefaultTex);

		// Finally Call the mesh draw function
		DSX->m_pFX->CommitChanges();
		m_pSkinedMesh->DrawSubset(i);	
	}
}
void AnimatedMesh::Render(std::string Texture, D3DXCOLOR color)
{
	if (!m_pRootMesh)
		return;
	DeferredShader* DSX = GCI->GetDeferredR();
	
	// Recurse down the tree, generating the transform toRoot matrix for the next frame
	D3DXMATRIX identity;
	D3DXMatrixIdentity(&identity);
	buildToRootXTree((BoneFrame*)m_pRootMesh, identity);

	// Build the final traformation matrix for each bone
	D3DXMATRIX offsetTemp, toRootTemp;
	for(UINT i = 0; i < m_NumBones; i++)
	{
		offsetTemp = *m_pSkinInfo->GetBoneOffsetMatrix(i);
		toRootTemp = *m_vToRootPtr[i];
		m_vFinalForm[i] = offsetTemp * toRootTemp;
	}	
	// Set the bone transforms
	DSX->m_pFX->SetMatrixArray("g_sFinalForms",getFinalFormList(),numBones());
	

	// Loop through all the materials in the mesh rendering each subset
	for (DWORD i = 0; i < m_MeshAssets->NumMaterials; i++)
	{
		// use the material in our extended data rather than the one in meshContainer->pMaterials[iMaterial].MatD3D
		DSX->m_pFX->SetValue("g_sAmbientMtrl", &color, sizeof(D3DXCOLOR));
		DSX->m_pFX->SetValue("g_sDiffuseMtrl", &color, sizeof(D3DXCOLOR));
		DSX->m_pFX->SetValue("g_sSpecMtrl", &m_MeshAssets->exMaterials[i].Specular, sizeof(D3DXCOLOR));
		DSX->m_pFX->SetFloat("g_sSpecPower", m_MeshAssets->exMaterials[i].Power);
		if(GCI->GetTextureManager()->TextureList.find(Texture) != GCI->GetTextureManager()->TextureList.end())
			DSX->m_pFX->SetTexture("g_sTexture", GCI->GetTextureManager()->TextureList[Texture]->texture);
		else
			DSX->m_pFX->SetTexture("g_sTexture", DSX->m_pDefaultTex);

		// Finally Call the mesh draw function
		DSX->m_pFX->CommitChanges();
		m_pSkinedMesh->DrawSubset(i);	
	}
}
void AnimatedMesh::Render()
{
	if(!m_pRootMesh)
		return;

	DeferredShader* DSX = GCI->GetDeferredR();
		
	// Recurse down the tree, generating the transform toRoot matrix for the next frame
	D3DXMATRIX identity;
	D3DXMatrixIdentity(&identity);
	buildToRootXTree((BoneFrame*)m_pRootMesh, identity);

	// Build the final traformation matrix for each bone
	D3DXMATRIX offsetTemp, toRootTemp;
	for(UINT i = 0; i < m_NumBones; i++)
	{
		offsetTemp = *m_pSkinInfo->GetBoneOffsetMatrix(i);
		toRootTemp = *m_vToRootPtr[i];
		m_vFinalForm[i] = offsetTemp * toRootTemp;
	}	
	// Set the bone transforms
	DSX->m_pFX->SetMatrixArray("g_sFinalForms",getFinalFormList(),numBones());
	

	// Loop through all the materials in the mesh rendering each subset
	for (DWORD i = 0; i < m_MeshAssets->NumMaterials; i++)
	{
		// use the material in our extended data rather than the one in meshContainer->pMaterials[iMaterial].MatD3D
		DSX->m_pFX->SetValue("g_sAmbientMtrl", &m_MeshAssets->exMaterials[i].Ambient, sizeof(D3DXCOLOR));
		DSX->m_pFX->SetValue("g_sDiffuseMtrl", &m_MeshAssets->exMaterials[i].Diffuse, sizeof(D3DXCOLOR));
		DSX->m_pFX->SetValue("g_sSpecMtrl", &m_MeshAssets->exMaterials[i].Specular, sizeof(D3DXCOLOR));
		DSX->m_pFX->SetFloat("g_sSpecPower", m_MeshAssets->exMaterials[i].Power);
		if(m_MeshAssets->exTexture[i])
			DSX->m_pFX->SetTexture("g_sTexture", m_MeshAssets->exTexture[i]);
		else
			DSX->m_pFX->SetTexture("g_sTexture", DSX->m_pDefaultTex);

		// Finally Call the mesh draw function
		DSX->m_pFX->CommitChanges();
		m_pSkinedMesh->DrawSubset(i);	
	}
}
void AnimatedMesh::Render(D3DXCOLOR color)
{
	if(!m_pRootMesh)
		return;

	DeferredShader* DSX = GCI->GetDeferredR();
		
	// Recurse down the tree, generating the transform toRoot matrix for the next frame
	D3DXMATRIX identity;
	D3DXMatrixIdentity(&identity);
	buildToRootXTree((BoneFrame*)m_pRootMesh, identity);

	// Build the final traformation matrix for each bone
	D3DXMATRIX offsetTemp, toRootTemp;
	for(UINT i = 0; i < m_NumBones; i++)
	{
		offsetTemp = *m_pSkinInfo->GetBoneOffsetMatrix(i);
		toRootTemp = *m_vToRootPtr[i];
		m_vFinalForm[i] = offsetTemp * toRootTemp;
	}	
	// Set the bone transforms
	DSX->m_pFX->SetMatrixArray("g_sFinalForms",getFinalFormList(),numBones());
	

	// Loop through all the materials in the mesh rendering each subset
	for (DWORD i = 0; i < m_MeshAssets->NumMaterials; i++)
	{
		// use the material in our extended data rather than the one in meshContainer->pMaterials[iMaterial].MatD3D
		DSX->m_pFX->SetValue("g_sAmbientMtrl", &color, sizeof(D3DXCOLOR));
		DSX->m_pFX->SetValue("g_sDiffuseMtrl", &color, sizeof(D3DXCOLOR));
		DSX->m_pFX->SetValue("g_sSpecMtrl", &m_MeshAssets->exMaterials[i].Specular, sizeof(D3DXCOLOR));
		DSX->m_pFX->SetFloat("g_sSpecPower", m_MeshAssets->exMaterials[i].Power);
		if(m_MeshAssets->exTexture[i])
			DSX->m_pFX->SetTexture("g_sTexture", m_MeshAssets->exTexture[i]);
		else
			DSX->m_pFX->SetTexture("g_sTexture", DSX->m_pDefaultTex);

		// Finally Call the mesh draw function
		DSX->m_pFX->CommitChanges();
		m_pSkinedMesh->DrawSubset(i);	
	}
}
void AnimatedMesh::PreRender(std::string texture, D3DXCOLOR color)
{
	if(!m_pRootMesh)
		return;

	DeferredShader* DSX = GCI->GetDeferredR();

	// Set the Material paramaters
	DSX->m_pFX->SetValue("g_sAmbientMtrl", &color, sizeof(D3DXCOLOR));
	DSX->m_pFX->SetValue("g_sDiffuseMtrl", &color, sizeof(D3DXCOLOR));
	DSX->m_pFX->SetValue("g_sSpecMtrl", &WHITE, sizeof(D3DXCOLOR));
	DSX->m_pFX->SetFloat("g_sSpecPower", 0.8f);

	// If the texture exist, use texture
	if( GCI->GetTextureManager()->TextureList.find(texture) != GCI->GetTextureManager()->TextureList.end() )
		DSX->m_pFX->SetTexture("g_sTexture", GCI->GetTextureManager()->TextureList[texture]->texture);
	else
		DSX->m_pFX->SetTexture("g_sTexture", DSX->m_pDefaultTex);	// Use Default
}
void AnimatedMesh::RenderRaw()
{
	if(!m_pRootMesh)
		return;

	DeferredShader* DSX = GCI->GetDeferredR();
		
	// Recurse down the tree, generating the transform toRoot matrix for the next frame
	D3DXMATRIX identity;
	D3DXMatrixIdentity(&identity);
	buildToRootXTree((BoneFrame*)m_pRootMesh, identity);

	// Build the final traformation matrix for each bone
	D3DXMATRIX offsetTemp, toRootTemp;
	for(UINT i = 0; i < m_NumBones; i++)
	{
		offsetTemp = *m_pSkinInfo->GetBoneOffsetMatrix(i);
		toRootTemp = *m_vToRootPtr[i];
		m_vFinalForm[i] = offsetTemp * toRootTemp;
	}	
	// Set the bone transforms
	DSX->m_pFX->SetMatrixArray("g_sFinalForms",getFinalFormList(),numBones());
	

	// Loop through all the materials in the mesh rendering each subset
	for (DWORD i = 0; i < m_MeshAssets->NumMaterials; i++)
	{
		// Finally Call the mesh draw function
		DSX->m_pFX->CommitChanges();
		m_pSkinedMesh->DrawSubset(i);	
	}
}


// Getter attributes
D3DXVECTOR3 AnimatedMesh::getBV_HalfSize()
{
	return (maxBounds-minBounds)*0.5f;
}
D3DXVECTOR3 AnimatedMesh::getBV_BoxCenter()
{
	return (maxBounds+minBounds)*0.5f;
}
D3DXVECTOR3 AnimatedMesh::getBV_SphereCenter()
{	return m_sphereCentre;	}
D3DXMATRIX AnimatedMesh::getBV_Offset()
{	return m_OffsetMat;	}
float AnimatedMesh::getBV_Radius()
{	return m_sphereRadius;	}
bool AnimatedMesh::operator== (const std::string& name)
{	return (m_FileName == name);	}
LPD3DXANIMATIONCONTROLLER AnimatedMesh::GetAnimationMainController()
{	return m_animController;	}
