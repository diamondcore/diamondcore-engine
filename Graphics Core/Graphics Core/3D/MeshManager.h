#pragma once

#include "SceneMesh.h"
#include "../SceneGraph/ObjectList.h"
#include <map>
#include <string>

class MeshManager
{
	std::vector<SceneMeshPtr>			m_MeshList;
	std::map<std::string, SceneMeshPtr>	m_MeshMap;
	
public:
	MeshManager();
	~MeshManager();
	void	PreRestartDevice();
	void	PostRestartDevice();
	void	Shutdown();

	bool	Load(std::string fileName);
	void	Unload(std::string fileName);

	void	Render(std::string mesh, std::vector<BaseObjectPtr> meshList);
	void	Render(std::string mesh, std::string texture, std::vector<BaseObjectPtr> meshList);
	void	Render(std::string mesh, D3DXMATRIX transforms[], int count);
	void	Render(std::string mesh, std::string texture, D3DXMATRIX transforms[], int count);
	
	// Mesh Accessors
	D3DXVECTOR3		GetBVHalfExtent(std::string mesh);
	SceneMeshPtr	GetMesh(std::string mesh);
	bool            HasMesh(std::string mesh);
};