#ifndef CAMERA_H
#define CAMERA_H

#include "../Westley's Lib/Attribute.h"
#include "BoundingVolume.h"

namespace WTW
{
	/********************************************
					Camera
	*********************************************/
	class Camera
	{
	private:
		D3DXMATRIX*	m_ViewMat;		// View Matrix
		D3DXMATRIX	m_ProjMat;		// Projection Matrix
		D3DXMATRIX	m_ViewProjMat;	// View Projection Matrix
		D3DXVECTOR3	m_PosVec;		// Position Vector
		D3DXVECTOR3* m_pPosition;
		bool		m_pNeedsDeleting;

		D3DXVECTOR3	m_LookVec;		// Look At Vector
		float		m_fSpeed;		// Speed of Camera Movement
		

		// Frustum
		D3DXPLANE	m_Frustum[6];	// Planes used for frustum culling
		void		BuildFrustum();	// Build the Frustum 

		// Position
		
		D3DXVECTOR3&	GetPos();

		// Look At
		void	SetLook(D3DXVECTOR3& look);

		// Set projection
		bool	m_bCustomProj;

	public:
		Camera();
		~Camera();
		virtual void	PreRestartDevice();
		virtual void	PostRestartDevice();
		virtual void	Update(float dt, bool freeMouseLook = false);
		void			SetLens(float fov);
		void			SetProj(float fov, float aspect, float nearF, float farF);
		void			SetSpeed(float speed);
		bool			IsVisible(BoundingVolume& box, D3DXMATRIX transform);
		void			SetPos(D3DXVECTOR3& pos);
		void			SetPos(D3DXVECTOR3* pos);
		void			SetView(float pitch, float yAxis);
		void			SetView(D3DXMATRIX* view);
		void			SetView(D3DXMATRIX view);
		void			SetPosByDir(D3DXVECTOR3 dir, float dt);

		float		m_fPitch;

		// Position
		Attribute<Camera, D3DXVECTOR3&>			m_Position;

		// Look At
		D3DXVECTOR3&	GetLook();
		Attribute<Camera, D3DXVECTOR3&>			m_LookAt;

		D3DXVECTOR3	m_RightVec;		// Right Vector
		D3DXVECTOR3	m_UpVec;		// Up Vector

		D3DXVECTOR3&	GetUp() { return m_UpVec; }

		D3DXVECTOR3&	GetRight() { return m_RightVec; }
		
		// View
		const D3DXMATRIX&	GetView();

		// Projection
		const D3DXMATRIX&	GetProj();

		// View Projection
		const D3DXMATRIX&	GetViewProj();
	};
}
#endif	//CAMERA_H