#pragma once

#include <d3dx9.h>

void InitAllVertexDeclarations();
void DestroyAllVertexDeclarations();


/***************************************************************
						Vertex: (Position)
****************************************************************/
struct VertexP
{
	VertexP()	
		:position(0.0f, 0.0f, 0.0f) {}
	
	VertexP(float x, float y, float z)	
		:position(x,y,z) {}
	
	VertexP(const D3DXVECTOR3& pos)	
		:position(pos) {}

	D3DXVECTOR3 position;
	static IDirect3DVertexDeclaration9* Decl;
};


/***************************************************************
					Vertex: (Position, Color)
****************************************************************/
struct VertexPC
{
	VertexPC()	
		:position(0.0f, 0.0f, 0.0f), 
		 color(0x00000000) {}
	
	VertexPC(float x, float y, float z, D3DCOLOR color)	
		:position(x,y,z), 
		 color() {}
	
	VertexPC(const D3DXVECTOR3& pos, D3DCOLOR color)	
		:position(pos), 
		 color(color) {}

	D3DXVECTOR3	position;
	D3DCOLOR	color;
	static IDirect3DVertexDeclaration9* Decl;
};


/***************************************************************
					Vertex: (Position, Normal)
****************************************************************/
struct VertexPN
{
	VertexPN ()
		:position(0.0f, 0.0f, 0.0f), 
		 normal(0.0f, 0.0f, 0.0f) {}
	
	VertexPN (float x, float y, float z,
			  float nX, float nY, float nZ)
		:position(x, y, z), 
		 normal(nX, nY, nZ) {}

	VertexPN (const D3DXVECTOR3& pos, const D3DXVECTOR3& normal)
		:position(pos), 
		 normal(normal) {}

	D3DXVECTOR3 position;
	D3DXVECTOR3 normal;
	static IDirect3DVertexDeclaration9* Decl;
};


/***************************************************************
				Vertex: (Position, Normal, TexCoord)
****************************************************************/
struct VertexPNT
{
	VertexPNT ()
		:position(0.0f, 0.0f, 0.0f), 
		 normal(0.0f, 0.0f, 0.0f), 
		 texturePos(0.0f, 0.0f) {}
	
	VertexPNT (float x, float y, float z, float nX, float nY, float nZ, float u, float v)
		:position(x,y,z), 
		 normal(nX, nY, nZ), 
		 texturePos(u, v) {}
	
	VertexPNT (const D3DXVECTOR3& pos, const D3DXVECTOR3& normal, const D3DXVECTOR2& tex)
		:position(pos), 
		 normal(normal), 
		 texturePos(tex) {}

	D3DXVECTOR3 position;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 texturePos;
	static IDirect3DVertexDeclaration9* Decl;
};


/***************************************************************
		Post Process Vertex: (Position, texCoord1, TexCoord2)
****************************************************************/
struct VertexPTT
{
	VertexPTT ()
		:x(0.0f), y(0.0f), z(0.0f), rhw(0.0f), 
		 texturePos1(0.0f, 0.0f), 
		 texturePos2(0.0f, 0.0f) {}
	
	VertexPTT (float px, float py, float pz, float prhw,
			   float u1, float v1, 
			   float u2, float v2)
		:x(px), y(py), z(pz), rhw(prhw), 
		 texturePos1(u1, v1), 
		 texturePos2(u2, v2) {}

	float x, y, z, rhw;
	D3DXVECTOR2 texturePos1;	// Post-process source
	D3DXVECTOR2 texturePos2;	// Origninal Scene
	static IDirect3DVertexDeclaration9* Decl;
};


/***************************************************************
	Blended Vertex: (Position, TexCoord, Weights, Bone Indices)
****************************************************************/
struct BlendedVertex
{
	BlendedVertex()
		:position(0.0f, 0.0f, 0.0f),
		 texturePos(0.0f, 0.0f),
		 weights(0.0f, 0.0f, 0.0f, 0.0f) 
	{}
	BlendedVertex(float x, float y, float z,
				  float u, float v,
				  float Ww, float Wx, float Wy, float Wz)
				  :position(x,y,z),
				   texturePos(u, v),
				   weights(Ww, Wx, Wy, Wz)
	{}
	BlendedVertex(D3DXVECTOR3 pos, D3DXVECTOR2 tex, D3DXVECTOR4 weight)
				  :position(pos),
				   texturePos(tex),
				   weights(weight)
	{}

	D3DXVECTOR3 position;
	D3DXVECTOR2 texturePos;
	D3DXVECTOR4 weights;
	BYTE		boneIndices[4];
	static IDirect3DVertexDeclaration9*	Decl;	// VertexBlend
};


/***************************************************************
	Tween Vertex: (Position1, Normal1, Position2, Normal2)
****************************************************************/
struct TweenVertex
{
	TweenVertex()
		:position1(0.0f, 0.0f, 0.0f),
		 normal1(0.0f, 0.0f, 0.0f),
		 position2(0.0f, 0.0f, 0.0f),
		 normal2(0.0f, 0.0f, 0.0f)
	{}
	TweenVertex(float x1, float y1, float z1,
				float Nx1, float Ny1, float Nz1,
				float x2, float y2, float z2,
				float Nx2, float Ny2, float Nz2)
				:position1(x1, y1, z1),
				 normal1(Nx1, Ny1, Nz1),
				 position2(x2, y2, z2),
				 normal2(Nx2, Ny2, Nz2)
	{}
	TweenVertex(D3DXVECTOR3 pos1, D3DXVECTOR3 norm1, 
				D3DXVECTOR3 pos2, D3DXVECTOR3 norm2)
				:position1(pos1),
				 normal1(norm1),
				 position2(pos2),
				 normal2(norm2)
	{}

	D3DXVECTOR3	position1;
	D3DXVECTOR3	normal1;
	D3DXVECTOR3	position2;
	D3DXVECTOR3	normal2;
	static IDirect3DVertexDeclaration9*	Decl;
};


/***************************************************************
			Vertex Instance: (Position, Normal, TexCoord)
****************************************************************/
struct VertexInstPNT
{
	VertexInstPNT ()
		:instID(0),
		 position(0.0f, 0.0f, 0.0f), 
		 normal(0.0f, 0.0f, 0.0f), 
		 texturePos(0.0f, 0.0f) {}
	
	VertexInstPNT (UINT ID,							// Instance index ID
				   float x, float y, float z,		// Position
				   float nX, float nY, float nZ,	// Normal
				   float u, float v)				// Texture Coord
		:instID(ID),
		 position(x,y,z), 
		 normal(nX, nY, nZ), 
		 texturePos(u, v) {}
	
	VertexInstPNT (const UINT ID, const D3DXVECTOR3& pos, const D3DXVECTOR3& normal, const D3DXVECTOR2& tex)
		:instID(ID),
		 position(pos), 
		 normal(normal), 
		 texturePos(tex) {}

	D3DXVECTOR3 position;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 texturePos;
	UINT		instID;
	static IDirect3DVertexDeclaration9* Decl;
};


/***************************************************************
		Vertex Instance: (Position, Normal, TexCoord, color)
****************************************************************/
struct VertexInstPNTC
{
	VertexInstPNTC ()
		:instID(0),
		 position(0.0f, 0.0f, 0.0f), 
		 normal(0.0f, 0.0f, 0.0f), 
		 texturePos(0.0f, 0.0f),
		 color(1.0f, 1.0f, 1.0f, 1.0f) {}
	
	VertexInstPNTC (UINT ID,						// Instance index ID
				   float x, float y, float z,		// Position
				   float nX, float nY, float nZ,	// Normal
				   float u, float v,				// Texture Coord
				   D3DXCOLOR c)							// Color
		:instID(ID),
		 position(x,y,z), 
		 normal(nX, nY, nZ), 
		 texturePos(u, v),
		 color(c) {}
	
	VertexInstPNTC (const UINT ID, const D3DXVECTOR3& pos, 
		const D3DXVECTOR3& normal, const D3DXVECTOR2& tex, const D3DXCOLOR& c)
		:instID(ID),
		 position(pos), 
		 normal(normal), 
		 texturePos(tex),
		 color(c) {}

	D3DXVECTOR3 position;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 texturePos;
	UINT		instID;
	D3DXCOLOR	color;
	static IDirect3DVertexDeclaration9* Decl;
};


/***************************************************************
	Vertex: (Position, Normal, TexCoord, Binormal, Tangent)
****************************************************************/
struct VertexPNTBT
{
	VertexPNTBT ()
		:position(0.0f, 0.0f, 0.0f), 
		 normal(0.0f, 0.0f, 0.0f), 
		 texturePos(0.0f, 0.0f),
		 tangent(0.0f, 0.0f, 0.0f),
		 binormal(0.0f, 0.0f, 0.0f) {}
	
	VertexPNTBT (float x, float y, float z, 
		float nX, float nY, float nZ, 
		float u, float v,
		float tanX, float tanY, float tanZ,
		float binX, float binY, float binZ)
		:position(x,y,z), 
		 normal(nX, nY, nZ), 
		 texturePos(u, v),
		 tangent(tanX, tanY, tanZ),
		 binormal(binX, binY, binZ) {}
	
	VertexPNTBT (const D3DXVECTOR3& pos, const D3DXVECTOR3& normal, 
		const D3DXVECTOR2& tex, const D3DXVECTOR3& tang, const D3DXVECTOR3& bin)
		:position(pos), 
		 normal(normal), 
		 texturePos(tex),
		 tangent(tang),
		 binormal(bin) {}

	D3DXVECTOR3 position;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 texturePos;
	D3DXVECTOR3 tangent;
	D3DXVECTOR3 binormal;
	static IDirect3DVertexDeclaration9* Decl;
};


/***********************************************************************************
	Vertex: (Position, Normal, TexCoord, Binormal, Tangent) & (weights, boneIndices)
************************************************************************************/
struct VertexBlendEx
{
	VertexBlendEx ()
		:position(0.0f, 0.0f, 0.0f), 
		 normal(0.0f, 0.0f, 0.0f), 
		 texturePos(0.0f, 0.0f),
		 tangent(0.0f, 0.0f, 0.0f),
		 binormal(0.0f, 0.0f, 0.0f),
		 weights(0.0f, 0.0f, 0.0f, 0.0f){}
	
	VertexBlendEx (float x, float y, float z, 
		float nX, float nY, float nZ, 
		float u, float v,
		float tanX, float tanY, float tanZ,
		float binX, float binY, float binZ)
		:position(x,y,z), 
		 normal(nX, nY, nZ), 
		 texturePos(u, v),
		 tangent(tanX, tanY, tanZ),
		 binormal(binX, binY, binZ) {}
	
	VertexBlendEx (const D3DXVECTOR3& pos, const D3DXVECTOR3& normal, 
		const D3DXVECTOR2& tex, const D3DXVECTOR3& tang, const D3DXVECTOR3& bin)
		:position(pos), 
		 normal(normal), 
		 texturePos(tex),
		 tangent(tang),
		 binormal(bin) {}

	D3DXVECTOR3 position;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 texturePos;
	D3DXVECTOR3 tangent;
	D3DXVECTOR3 binormal;
	// Animation Variables
	D3DXVECTOR4 weights;
	BYTE		boneIndices[4];
	static IDirect3DVertexDeclaration9* Decl;
};


/***************************************************************************************
	VertexParticle: (Position, velocity, size, initTime, lifeTime, mass, instID< color)
****************************************************************************************/
struct VertexParticle
{
	VertexParticle ()
		:initPos(0.0f, 0.0f, 0.0f), 
		 initVel(0.0f, 0.0f, 0.0f), 
		 initSize(0.0f),
		 initTime(0.0f),
		 lifeTime(0.0f),
		 mass(0.0f),
		 instID(0),
		 initColor(0.0f, 0.0f, 0.0f, 0.0f) {}

		VertexParticle (const D3DXVECTOR3& startPos, const D3DXVECTOR3& startVel,
			const float& pixelSize, const float& startTime, const float& totalLifeTime,
			const float& particleMass, const UINT& id, const D3DXCOLOR& startColor)
		:initPos(startPos), 
		 initVel(startVel), 
		 initSize(pixelSize),
		 initTime(startTime),
		 lifeTime(totalLifeTime),
		 mass(particleMass),
		 instID(id),
		 initColor(startColor) {}

	D3DXVECTOR3 initPos;
	D3DXVECTOR3 initVel;
	float		initSize;	// Size in pixels
	float		initTime;
	float		lifeTime;	
	float		mass;
	UINT		instID;		// Instance ID
	D3DXCOLOR	initColor;
	static IDirect3DVertexDeclaration9* Decl;
};


/******************************************************************
	AA_LineVertex: (Position1, Position2, Weights, Radius, Aspect)
*******************************************************************/
struct AA_LineVertex
{
	AA_LineVertex()	
		:position1(0.0f, 0.0f, 0.0f),
		 position2(0.0f, 0.0f, 0.0f) {}
	
	AA_LineVertex(float x, float y, float z)	
		:position1(x,y,z),
		 position2(0.0f, 0.0f, 0.0f) {}
	
	AA_LineVertex(const D3DXVECTOR3& pos, const D3DXVECTOR3& pos2, float r, float asp,
		float a, float b, float u, float v)	
		:position1(pos), 
		 position2(pos2),
		 radius(r),
		 aspect(asp),
		 weights(D3DXVECTOR4(a,b,u,v)) {}

	D3DXVECTOR3 position1;
	D3DXVECTOR3 position2;
	D3DXVECTOR4 weights;
	float		radius;
	float		aspect;
	static IDirect3DVertexDeclaration9* Decl;
};

