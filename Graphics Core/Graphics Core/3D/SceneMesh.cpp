#include "SceneMesh.h"
#include "../MacroTools.h"
#include "../GraphicsCore.h"
#include "../Pipeline/DeferredShader.h"
#include <assert.h>
#include "../Utility.h"

/***************************************
			  Mesh Texture
****************************************/
MeshTexture::MeshTexture()
{
	m_pTexture		= 0;
	m_pNormTex		= 0;
	m_pSpecTex		= 0;
	m_fPhongExp		= 5.0f;
	m_fPhongCoeff	= 1.0f;
	m_fDiffuseCoeff	= 1.0f;
}
MeshTexture::~MeshTexture()
{
	//PreRestartDevice();
}
void MeshTexture::PreRestartDevice()
{
	SAFE_RELEASE(m_pTexture);
	SAFE_RELEASE(m_pNormTex);
	SAFE_RELEASE(m_pSpecTex);
}
void MeshTexture::PostRestartDevice()
{	
	// Create texture
	if(m_Path != "NONE")
		D3DXCreateTextureFromFile(GCI->GetDirectX()->GetDevice(), m_Path.c_str(), &m_pTexture);

	if(!m_pTexture)
		m_pTexture = 0;
}
void MeshTexture::Initialize(std::string path)
{
	if(path != "NONE")
	{
		m_Path = path;	// Store path & file name

		// Create Texture
		/*HRESULT hr = D3DXCreateTextureFromFile(GCI->GetDirectX()->GetDevice(), m_Path.c_str(), &m_pTexture);	
		if(CUtility::FailedHr(hr))
			m_pTexture = 0;*/
	
		// Load texture through texture manager
		if( GCI->GetTextureManager()->LoadRawTexture(path) )
		{
			auto textureList = GCI->GetTextureManager()->TextureList;
			textureList[path]->texture->AddRef();
			m_pTexture = textureList[path]->texture;
		}
		else
			m_pTexture = 0;
		
		// Set Custom Maps
		SetNormalMap();
		SetSpecularMap();
	}
	else
	{
		m_Path = path;
		m_pTexture = 0;
	}

	// Set Default Mtrl
	SetMtrl(WHITE, WHITE, WHITE, 8.0f);
}
// Set Custom Maps
void MeshTexture::SetNormalMap()
{
	// Prepare string for normal map
	std::string normMap;
	int p = m_Path.find('.');
	normMap = m_Path.substr(0, p);
	normMap.append("_norm");
	normMap += m_Path.substr(p, m_Path.size()-1);
	
	// Create Normal Map
	/*HRESULT hr = D3DXCreateTextureFromFile(GCI->GetDirectX()->GetDevice(), normMap.c_str(), &m_pNormTex);	
	if(CUtility::FailedHr(hr))
		m_pNormTex = 0;*/
	
	// Load texture through texture manager
	if( GCI->GetTextureManager()->LoadRawTexture(normMap) )
	{
		auto textureList = GCI->GetTextureManager()->TextureList;
		textureList[normMap]->texture->AddRef();
		m_pNormTex = textureList[normMap]->texture;
	}
	else
		m_pNormTex = 0;
}
void MeshTexture::SetSpecularMap()
{
	// Prepare string for specular map
	std::string specMap;
	int p = m_Path.find('.');
	specMap = m_Path.substr(0, p);
	specMap.append("_spec");
	specMap += m_Path.substr(p, m_Path.size()-1);

	// Create Specular Map
	/*HRESULT hr = D3DXCreateTextureFromFile(GCI->GetDirectX()->GetDevice(), specMap.c_str(), &m_pSpecTex);	
	if(CUtility::FailedHr(hr))
		m_pSpecTex = 0;*/
	
	// Load texture through texture manager
	if( GCI->GetTextureManager()->LoadRawTexture(specMap) )
	{
		auto textureList = GCI->GetTextureManager()->TextureList;
		textureList[specMap]->texture->AddRef();
		m_pSpecTex = textureList[specMap]->texture;
	}
	else
		m_pSpecTex = 0;
}
void MeshTexture::SetMtrl(D3DXCOLOR ambient, D3DXCOLOR diffuse, D3DXCOLOR spec, float specPower)
{
	// Set up the Material properties
	m_Ambient		= ambient;
	m_Diffuse		= diffuse;
	m_Specular		= spec;
	m_fSpecPower	= specPower;
}
void MeshTexture::SetLightAttributes(float diffuseCoeff, float phongCoeff, float phongExp)
{
	// Set up lighting attributes
	m_fDiffuseCoeff	= diffuseCoeff;
	m_fPhongCoeff	= phongCoeff;
	m_fPhongExp		= phongExp;
}
void MeshTexture::PreRender()
{
	auto DSX = GCI->GetDeferredR();

	// Set up the Material
	DSX->m_pFX->SetValue(DSX->m_hAmbientMtrl, &m_Ambient, sizeof(D3DXCOLOR));
	DSX->m_pFX->SetValue(DSX->m_hDiffuseMtrl, &m_Diffuse, sizeof(D3DXCOLOR));
	DSX->m_pFX->SetValue(DSX->m_hSpecMtrl, &m_Specular, sizeof(D3DXCOLOR));
	DSX->m_pFX->SetFloat(DSX->m_hSpecPower, m_fSpecPower);

	// Set lighting attributes
	DSX->m_pFX->SetFloat("g_sPhongExp", m_fPhongExp);
	DSX->m_pFX->SetFloat("g_sPhongCoeff", m_fPhongCoeff);
	DSX->m_pFX->SetFloat("g_sDiffuseCoeff", m_fDiffuseCoeff);

	// Check if there is a texture
	if(m_pTexture)
		DSX->m_pFX->SetTexture("g_sTexture", m_pTexture);	// Set Texture
	else
		DSX->m_pFX->SetTexture("g_sTexture", DSX->m_pDefaultTex);	// Use Default Texture (White)
	
	if(m_pNormTex)	// Check if there is a normal map to use
	{
		DSX->m_pFX->SetBool("f_UseNormalTex", true);
		DSX->m_pFX->SetTexture("g_sNormalTex", m_pNormTex);
	}
	else
		DSX->m_pFX->SetBool("f_UseNormalTex", false);

	if(m_pSpecTex)	// Check if there is a specular map to use
	{
		DSX->m_pFX->SetBool("f_UseSpecTex", true);
		DSX->m_pFX->SetTexture("g_sSpecTex", m_pSpecTex);
	}
	else
		DSX->m_pFX->SetBool("f_UseSpecTex", false);
	DSX->m_pFX->CommitChanges();
}
void MeshTexture::PreRender(std::string texture)
{
	auto DSX = GCI->GetDeferredR();

	// Set up the Material
	DSX->m_pFX->SetValue(DSX->m_hAmbientMtrl, &m_Ambient, sizeof(D3DXCOLOR));
	DSX->m_pFX->SetValue(DSX->m_hDiffuseMtrl, &m_Diffuse, sizeof(D3DXCOLOR));
	DSX->m_pFX->SetValue(DSX->m_hSpecMtrl, &m_Specular, sizeof(D3DXCOLOR));
	DSX->m_pFX->SetFloat(DSX->m_hSpecPower, m_fSpecPower);
	
	// Set lighting attributes
	DSX->m_pFX->SetFloat("g_sPhongExp", m_fPhongExp);
	DSX->m_pFX->SetFloat("g_sPhongCoeff", m_fPhongCoeff);
	DSX->m_pFX->SetFloat("g_sDiffuseCoeff", m_fDiffuseCoeff);

	// Check if there is a texture
	if(texture == "DEFAULT")
		DSX->m_pFX->SetTexture("g_sTexture", m_pTexture);	// Set Texture
	else if( GCI->GetTextureManager()->TextureList.find(texture) != GCI->GetTextureManager()->TextureList.end() &&
		GCI->GetTextureManager()->TextureList.find(texture)->first != "" )
			DSX->m_pFX->SetTexture("g_sTexture", GCI->GetTextureManager()->TextureList[texture]->texture );	// Set Texture
	else
		DSX->m_pFX->SetTexture("g_sTexture", DSX->m_pDefaultTex);	// Use Default Texture (White)
}
inline IDirect3DTexture9* MeshTexture::GetTexture()
{
	if(m_pTexture)						// Make sure there is a texture to return
		return m_pTexture;
	else
		return 0;
}


/***************************************
				Scene Mesh
****************************************/
SceneMesh::SceneMesh()
{
	m_Mesh			= 0;
	m_pAdjBuffer	= 0;
	m_pMtrlBuffer	= 0;
	m_pFXBuffer		= 0;
	m_NumMtrls		= 0;
}
SceneMesh::~SceneMesh()
{
}
void SceneMesh::PreRestartDevice()
{
//	for(DWORD i = 0; i < m_TextureList.size(); i++)
//		m_TextureList[i]->PreRestartDevice();
}
void SceneMesh::PostRestartDevice()
{
//	for(DWORD i = 0; i < m_TextureList.size(); i++)
//		m_TextureList[i]->PostRestartDevice();
}

void SceneMesh::Unload()
{
	for(DWORD i = 0; i < m_TextureList.size(); i++)
		m_TextureList[i]->PreRestartDevice();
	m_TextureList.clear();
	
	SAFE_RELEASE(m_Mesh);
	SAFE_RELEASE(m_pAdjBuffer);
	SAFE_RELEASE(m_pMtrlBuffer);
	SAFE_RELEASE(m_pFXBuffer);
}
bool SceneMesh::Load(std::string path)
{
	// Set the path for the scene mesh
	m_Path = "Content/Meshes/";
	m_Path.append( path.substr(0, path.length()-2) );	// Take off the ".x" to get the name of the folder
	m_Path.append( "/" );
	m_Path.append( path );
	
	// Load Mesh and store in static mesh
	HRESULT	hr = D3DXLoadMeshFromX(
				m_Path.c_str(),			// File to Load
				D3DXMESH_MANAGED,		// Memory Management flags
				GCI->GetDirectX()->GetDevice(),		// Device
				&m_pAdjBuffer,			// Returns Adjacent information
				&m_pMtrlBuffer,			// Returns Material data D3DXMATERIAL
				&m_pFXBuffer,			// Effect information
				&m_NumMtrls,			// Number of Materials
				&m_Mesh);				// New Mesh object
	if(CUtility::FailedHr(hr))
		return false;
		//assert(0 && "Mesh failed to load.");
	
	// Configure mesh to vertex decleration
	ConfigureVertexDecl();

	// Load any textures and materials
	LoadAttributes();

	// Calculate the bounding Volume
	m_BoundingVolume.GetBoundingBox(&m_Mesh);
	//m_BoundingVolume.GetBoundingSphere(&m_Mesh);
	return true;
}
void SceneMesh::ConfigureVertexDecl()
{
	D3DVERTEXELEMENT9	Elements[MAX_FVF_DECL_SIZE];
	HRESULT hr = m_Mesh->GetDeclaration(Elements);	// Get original vertex decleration

	// Check if the Mesh has vertex normals
	bool hasNormal = false;		
	for(int i = 0; i < MAX_FVF_DECL_SIZE; i++)
	{
		if(Elements[i].Stream == 0xff)	// Check for the end of the Elements, "D3DDECL_END()"
			break;

		if(Elements[i].Type			== D3DDECLTYPE_FLOAT3 &&
		   Elements[i].Usage		== D3DDECLUSAGE_NORMAL &&
		   Elements[i].UsageIndex	== 0)
		{
			hasNormal = true;
			break;
		}
	}


	// Change Vertex format, to the DirectVertex format
	D3DVERTEXELEMENT9	newElem[64];
	UINT numElements = 0;
	hr = VertexType::Decl->GetDeclaration(newElem, &numElements);	// Using typedef vertex type

	// Clone the System Mesh to the newElem Vertex format
	ID3DXMesh*	temp = 0;		
	hr = m_Mesh->CloneMesh(m_Mesh->GetOptions(),		//D3DXMESH_SYSTEMMEM,		
					newElem, GCI->GetDirectX()->GetDevice(), &temp);
	SAFE_RELEASE(m_Mesh);	// Whipe the System Mesh

	// Generate Normals, if needed
	if(hasNormal == false)
		hr = D3DXComputeNormals(temp, 0);

	// Optimize the Final Mesh for best performance
	ID3DXMesh* optMesh;
	hr = temp->Optimize(D3DXMESH_MANAGED | 
				  	    D3DXMESHOPT_COMPACT | 
						D3DXMESHOPT_ATTRSORT | 
						D3DXMESHOPT_VERTEXCACHE,
						(DWORD*)m_pAdjBuffer->GetBufferPointer(), 
						0, 0, 0, &optMesh);
	SAFE_RELEASE(temp);
	
	// Compute tangents for Deferred shader
	hr = D3DXComputeTangentFrameEx(optMesh,
						D3DDECLUSAGE_TEXCOORD, 0,
						D3DDECLUSAGE_BINORMAL, 0,
						D3DDECLUSAGE_TANGENT, 0,
						D3DDECLUSAGE_NORMAL, 0,
						0, 0, 0.01f, 0.25f, 0.01f,
						&m_Mesh, 0);
	SAFE_RELEASE(optMesh);
}
void SceneMesh::LoadAttributes()
{
	// Get array of Materials/Textures
	D3DXMATERIAL*	d3dxMaterials	= (D3DXMATERIAL*)m_pMtrlBuffer->GetBufferPointer();

	// Resize the Material/Texture list
	m_TextureList.resize(m_NumMtrls);

	// Load Materials and Textures
	for(DWORD i = 0; i < m_NumMtrls; i++)
	{
		// Set the path and texture name
		std::string path = m_Path.substr(0, m_Path.find_last_of( "/" ) + 1 );	// Go all the way to the last folder
		if(d3dxMaterials[i].pTextureFilename)	// Check if their is a texture
			path += d3dxMaterials[i].pTextureFilename;
		else
			path = "NONE";

		// Initialize the Texture
		for(DWORD t = 0; t < i; t++)
			if(m_TextureList[t]->GetPath() == path)
			{
				m_TextureList[i] = m_TextureList[t];
			}
		m_TextureList[i] = std::shared_ptr<MeshTexture>(new MeshTexture);
		m_TextureList[i]->Initialize(path);

		// Set the Material
		m_TextureList[i]->SetMtrl(d3dxMaterials[i].MatD3D.Ambient,
			d3dxMaterials[i].MatD3D.Diffuse, d3dxMaterials[i].MatD3D.Specular,
			d3dxMaterials[i].MatD3D.Power);
	}
}

// Default rendering
void SceneMesh::Render()
{
	for(DWORD i = 0; i < m_NumMtrls; i++)
	{
		// Set the Material and Texture in the shader
		m_TextureList[i]->PreRender();

		// Draw mesh subset
		m_Mesh->DrawSubset(i);
	}
}

// Overloaded texture rendering
void SceneMesh::PreRender(std::string texture)
{
	if(texture == "DEFAULT")	// Use default texture
	{
		m_TextureList[0]->PreRender();
		return;
	}

	// Set the Material and Texture in the shader
	if(m_TextureList.size() > 0)
		m_TextureList[0]->PreRender( texture );
}
void SceneMesh::RawRender()
{
	for(DWORD i = 0; i < m_NumMtrls; i++)	// Draw mesh subset
		m_Mesh->DrawSubset(i);
}

BoundingVolume* SceneMesh::GetBV()
{	return &m_BoundingVolume;	}

bool SceneMesh::operator== (const std::string& fileName)
{
	return (m_Path == fileName);
}

// Set the material for the main texture
void SceneMesh::SetMtrl(D3DXCOLOR ambient, D3DXCOLOR diffuse, D3DXCOLOR spec, float specPower)
{
	m_TextureList[0]->SetMtrl(ambient, diffuse, spec, specPower);
}
void SceneMesh::SetTextue(std::string texName)
{
	// Set the path to the mesh folder and texture name
	std::string path = m_Path.substr(0, m_Path.find_last_of( "/" ) + 1 );	// Go all the way to the last folder
	path += texName;

	if(m_TextureList.size() < 1)	// Make sure a texture exists
		m_TextureList.push_back( std::shared_ptr<MeshTexture>(new MeshTexture) );
		
	// Initialize the Texture
	m_TextureList[0]->Initialize(path);
}
void SceneMesh::SetLightAttributes(float diffuseCoeff, float phongCoeff, float phongExp)
{
	for(auto t = m_TextureList.begin(); t != m_TextureList.end(); t++)
		(*t)->SetLightAttributes(diffuseCoeff, phongCoeff, phongExp);
}