#include "VertexDecl.h"
#include "../MacroTools.h"
#include "../GraphicsCore.h"

// Initialize the static declarations
IDirect3DVertexDeclaration9* VertexP::Decl			= 0;
IDirect3DVertexDeclaration9* VertexPC::Decl			= 0;
IDirect3DVertexDeclaration9* VertexPN::Decl			= 0;
IDirect3DVertexDeclaration9* VertexPNT::Decl		= 0;
IDirect3DVertexDeclaration9* VertexPTT::Decl		= 0;
IDirect3DVertexDeclaration9* BlendedVertex::Decl	= 0;
IDirect3DVertexDeclaration9* TweenVertex::Decl		= 0;
IDirect3DVertexDeclaration9* VertexInstPNT::Decl	= 0;
IDirect3DVertexDeclaration9* VertexInstPNTC::Decl	= 0;
IDirect3DVertexDeclaration9* VertexPNTBT::Decl		= 0;
IDirect3DVertexDeclaration9* VertexBlendEx::Decl	= 0;
IDirect3DVertexDeclaration9* VertexParticle::Decl	= 0;
IDirect3DVertexDeclaration9* AA_LineVertex::Decl	= 0;

void InitAllVertexDeclarations()
{
	/***************************************************************/
	/*					Vertex: (Position)						   */
	/***************************************************************/
	D3DVERTEXELEMENT9 VertexPElements[] = 
	{
		{0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		D3DDECL_END()
	};
	HRESULT hr = GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(VertexPElements, &VertexP::Decl); 

	
	/***************************************************************/
	/*				Vertex: (Position, Color)					   */
	/***************************************************************/
	D3DVERTEXELEMENT9 VertexPCElements[] = 
	{
		{0, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
		D3DDECL_END()
	};
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(VertexPCElements, &VertexPC::Decl); 
	

	/***************************************************************/
	/*				Vertex: (Position, Normal)					   */
	/***************************************************************/
	D3DVERTEXELEMENT9 VertexPNElements[] = 
	{
		{0, 0,	D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
		D3DDECL_END()
	};
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(VertexPNElements, &VertexPN::Decl); 
	

	/***************************************************************/
	/*				Vertex: (Position, Normal, TexCoord)		   */
	/***************************************************************/
	D3DVERTEXELEMENT9 VertexPNTElements[] = 
	{
		{0, 0,	D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
		{0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		D3DDECL_END()
	};
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(VertexPNTElements, &VertexPNT::Decl); 


	/***************************************************************/
	/*		Post Process Vertex: (Position, texCoord1, TexCoord2)  */
	/***************************************************************/
	D3DVERTEXELEMENT9 VertexPTTElements[] = 
	{
		{0, 0,	D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITIONT, 0},
		{0, 12, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		{0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
		D3DDECL_END()
	};
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(VertexPTTElements, &VertexPTT::Decl); 


	/***************************************************************
		Blended Vertex: (Position, TexCoord, Weights, Bone Indices)
	****************************************************************/
	D3DVERTEXELEMENT9 BlendedVertexElements[] = 
	{
		{0, 0,	D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		{0, 24,	D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0},
		{0, 36, D3DDECLTYPE_UBYTE4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0},
		D3DDECL_END()
	};
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(BlendedVertexElements, &BlendedVertex::Decl);


	/***************************************************************
		Tween Vertex: (Position1, Normal1, Position2, Normal2)
	****************************************************************/
	D3DVERTEXELEMENT9 TweenVertexElements[] = 
	{
		{0, 0,	D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
		{0, 24,	D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 1},
		{0, 36, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 1},
		D3DDECL_END()
	};
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(TweenVertexElements, &TweenVertex::Decl);


	/***************************************************************/
	/*			Vertex Instance: (Position, Normal, TexCoord)	   */
	/***************************************************************/
	D3DVERTEXELEMENT9 VertexInstPNTElements[] = 
	{
		{0, 0,	D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
		{0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		{0, 32, D3DDECLTYPE_UBYTE4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0},
		D3DDECL_END()
	};
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(VertexInstPNTElements, &VertexInstPNT::Decl); 

	/***************************************************************/
	/*		Vertex Instance: (Position, Normal, TexCoord, color)   */
	/***************************************************************/
	D3DVERTEXELEMENT9 VertexInstPNTCElements[] = 
	{
		{0, 0,	D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
		{0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		{0, 32, D3DDECLTYPE_UBYTE4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0},
		{0, 36, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
		D3DDECL_END()
	};
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(VertexInstPNTCElements, &VertexInstPNTC::Decl); 
	
	/***************************************************************
		Vertex: (Position, Normal, TexCoord, Binormal, Tangent)
	****************************************************************/
	D3DVERTEXELEMENT9 VertexPNTBTElements[] = 
	{
		{0, 0,	D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
		{0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		{0, 32, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0},
		{0, 44, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BINORMAL, 0},
		D3DDECL_END()
	};
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(VertexPNTBTElements, &VertexPNTBT::Decl); 
	
	/***************************************************************
	 Vertex BlendEx: (Position, Normal, TexCoord, Binormal, Tangent)
	****************************************************************/
	D3DVERTEXELEMENT9 VertexBlendExElements[] = 
	{
		{0, 0,	D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
		{0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		{0, 32, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0},
		{0, 44, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BINORMAL, 0},
		{0, 56, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0},	// Max number of bone influence per vert
		{0, 72, D3DDECLTYPE_UBYTE4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0},		
		D3DDECL_END()
	};
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(VertexBlendExElements, &VertexBlendEx::Decl); 

	/***************************************************************
		VertexParticle: (Position)
	****************************************************************/
	D3DVERTEXELEMENT9 VertexParticleElements[] = 
	{
		{0, 0,	D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0},
		{0, 24, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_PSIZE, 0},
		{0, 28, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		{0, 32, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
		{0, 36, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2},
		{0, 40, D3DDECLTYPE_UBYTE4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0},
		{0, 44, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
		D3DDECL_END()
	};
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(VertexParticleElements, &VertexParticle::Decl); 

	/******************************************************************
	AA_LineVertex: (Position1, Position2, Weights, Radius, Aspect)
	*******************************************************************/
	D3DVERTEXELEMENT9 AA_LineVertexElements[] =
	{
		{0, 0,	D3DDECLTYPE_FLOAT3,	D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12,	D3DDECLTYPE_FLOAT3,	D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 1},
		{0, 24,	D3DDECLTYPE_FLOAT4,	D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		{0, 40,	D3DDECLTYPE_FLOAT1,	D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
		{0, 44,	D3DDECLTYPE_FLOAT1,	D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2},
		D3DDECL_END()
	};
	hr = GCI->GetDirectX()->GetDevice()->CreateVertexDeclaration(AA_LineVertexElements, &AA_LineVertex::Decl);
}

void DestroyAllVertexDeclarations()
{
	SAFE_RELEASE(VertexP::Decl);
	SAFE_RELEASE(VertexPC::Decl);
	SAFE_RELEASE(VertexPN::Decl);
	SAFE_RELEASE(VertexPNT::Decl);
	SAFE_RELEASE(VertexPTT::Decl);
	SAFE_RELEASE(BlendedVertex::Decl);
	SAFE_RELEASE(TweenVertex::Decl);
	SAFE_RELEASE(VertexInstPNT::Decl);
	SAFE_RELEASE(VertexInstPNTC::Decl);
	SAFE_RELEASE(VertexPNTBT::Decl);
	SAFE_RELEASE(VertexBlendEx::Decl);
	SAFE_RELEASE(VertexParticle::Decl);
	SAFE_RELEASE(AA_LineVertex::Decl);
}