#pragma once

#include "TerrainTexture.h"
#include "../Westley's Lib/Dynamic2DArray.h"

class Terrain
{
	//std::vector<VertexBlendEx>	m_VertexList;
	WTW::Dynamic2DArray<VertexBlendEx>	m_VertexList;
	std::vector<WORD>			m_IndexList;
	DWORD*						m_pAdjList;
	std::string					m_cFileName;

	D3DXMATRIX				m_TransMat;			// Transform matrix
	D3DXVECTOR3				m_vSize;			// Terrain Size
	D3DXVECTOR2				m_vSpace;			// Space between each texel
	int						m_iNumTris;
	int						m_iNumRows;
	int						m_iNumCols;
	bool					m_bNeedsUpdate;
	bool					m_bNeedsNormUpdate;

	ID3DXMesh*				m_pMesh;

	// Textures
	std::string			m_pTextures[3];
	TerrainTexture		m_pBlendTexture;
	TerrainTexture		m_pCustomTexture;
	float				m_fTexScale;
	bool				m_bRenderCustomTex;
	
	void	BuildMesh();
	void	UpdateBuffers();
	void	UpdateNormals();

	// Normal & Specular Maps
	std::string			m_pNormTex[3];
	std::string			m_pSpecTex[3];
	void	SetNormalMap(int texture);
	void	SetSpecularMap(int texture);

	// Terrain Brush
	D3DXVECTOR3					m_vBrushPos;
	float*						m_pBrushRadius;
	std::vector<VertexBlendEx*>	m_pSelectedVerts;
	std::vector<float>			m_SelectedDist;
	bool						m_bValidBrush;

	// Utility Variables
	float	m_fHalfPI;
	float	m_fAvgHeight;

	// Set Color variables
	D3DXCOLOR	m_Ambient;
	D3DXCOLOR	m_Diffuse;
	D3DXCOLOR	m_Specular;
	float		m_fSpecPower;
	float		m_fPhongExp;
	float		m_fPhongCoeff;
	float		m_fDiffuseCoeff;

public:
	Terrain();
	~Terrain();
	void	Clear();
	void	Reset();
	void	PreRestartDevice();
	void	PostRestartDevice();

	// Generate grid of this size
	void	Init(unsigned int rows, unsigned int cols, 
				unsigned int sizeZ, unsigned int sizeX, float texScale);
	// Set Textures
	void	SetTexture1(std::string tex1);
	void	SetTexture2(std::string tex2);
	void	SetTexture3(std::string tex3);

	// Terrain Texture Painting
	// 0 = blend texture
	// 1 = custom texture
	void	PaintTex1(int tex, float increment);
	void	PaintTex2(int tex, float increment);
	void	PaintTex3(int tex, float increment);

	// Terrain Morphing
	void	SetBrush(float* radius);
	void	MorphUp(float increment);	// increment [0, 1]
	void	MorphDown(float increment);	// increment [0, 1]
	void	Smooth(float scale);		// scale [0, 1]
	void	SmoothCenter(float scale);	// scale [0, 1]

	void	UpdateBrush();
	void	Render();

	void	Load(char* path, char* fileName);
	void	Save(char* path, char* fileName);

	// Get Terrain Information
	char*	GetFileName();
	int		GetNumVerts();
	int		GetNumIndices();
	void	GetVerts(D3DXVECTOR3 verts[]);
	void	GetIndices(WORD indices[]);
	void	GetSize(D3DXVECTOR3& size);
	void	GetNumRowsCols(int& rows, int& cols);
	void	GetTextureName(int tex, std::string& texName);
	D3DXCOLOR	GetColorAt(Vector3 worldPos);
	void	SetCustomRenderTex(bool renderCustomTex);
};