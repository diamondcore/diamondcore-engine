#include "TerrainTexture.h"
#include "../GraphicsCore.h"
#include "../MacroTools.h"

TerrainTexture::TerrainTexture()
{
	m_pBlendTexture		= 0;
	m_bNeedsBackUp		= 0;
	m_iLastIndex		= -1;
	m_fBlendTexScale	= 0.01f;
	m_fPitch			= 0.0f;
}
TerrainTexture::~TerrainTexture()
{

}
void TerrainTexture::PreRestartDevice()
{
	SAFE_RELEASE(m_pBlendTexture);
}
void TerrainTexture::PostRestartDevice()
{
	// Create Blend texture for the terrain
	HRESULT hr = D3DXCreateTexture(GCI->GetDirectX()->GetDevice(), 
		m_vBlendTexSize.x, m_vBlendTexSize.y, 1, D3DUSAGE_DYNAMIC, 
		D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_pBlendTexture);
	
	int index;
	//D3DXCOLOR color;
	D3DLOCKED_RECT LockedRect;
	hr = m_pBlendTexture->LockRect(0, &LockedRect, NULL, 0);
	m_fPitch = LockedRect.Pitch / 4;
	
	// Set the last index of the texture
	m_iLastIndex = ((m_vBlendTexSize.y-1) * (m_fPitch)) + (m_vBlendTexSize.x-1);

	// Set the default texture to texture 1
	//int backup = 0;
	for(int j = 0; j < m_vBlendTexSize.y; j++)
		for(int i = 0; i < m_vBlendTexSize.x; i++)
		{
			// Find index at position
			index = (j * (m_fPitch)) + i;

			// Find 32-bit color at position
			/*color = ((unsigned int*)LockedRect.pBits)[index];

			// Set from the backup data
			color = m_pBlendTexColor[backup];
			((unsigned int*)LockedRect.pBits)[index] = color;*/
			((unsigned int*)LockedRect.pBits)[index] = m_pBlendTexColor[index];
			//backup++;
		}
	hr = m_pBlendTexture->UnlockRect(0);
}
void TerrainTexture::Clear()
{	
	m_pBlendTexColor.clear();
	SAFE_RELEASE(m_pBlendTexture);
}

void TerrainTexture::Init(Vector3 terrainSize, int scale, D3DXCOLOR defaultColor)
{
	m_fBlendTexScale = scale;	// Set the texture scale
	m_vTerrainSize	 = terrainSize;	// Set the terrain size

	// Set texture size with a scale of how many pixels per quad
	m_vBlendTexSize.x = terrainSize.x * scale;
	m_vBlendTexSize.y = terrainSize.z * scale;

	// Create Blend texture for the terrain
	HRESULT hr = D3DXCreateTexture(GCI->GetDirectX()->GetDevice(), 
		m_vBlendTexSize.x, m_vBlendTexSize.y, 1, D3DUSAGE_DYNAMIC, 
		D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_pBlendTexture);
	
	int index;
	D3DLOCKED_RECT LockedRect;
	hr = m_pBlendTexture->LockRect(0, &LockedRect, NULL, 0);
	m_fPitch = LockedRect.Pitch / 4;

	// Set the last index of the texture
	m_iLastIndex = ((m_vBlendTexSize.y-1) * (m_fPitch)) + (m_vBlendTexSize.x-1);
	m_pBlendTexColor.resize(m_iLastIndex+1);

	// Set the default texture to texture 1
	for(int j = 0; j < m_vBlendTexSize.y; j++)
		for(int i = 0; i < m_vBlendTexSize.x; i++)
		{
			// Find index at position
			index = (j * (m_fPitch)) + i;

			// Find 32-bit color at position
			//color = ((unsigned int*)LockedRect.pBits)[index];

			// Set default texture
			((unsigned int*)LockedRect.pBits)[index] = defaultColor;
			m_pBlendTexColor[index] = ((unsigned int*)LockedRect.pBits)[index];	// Save color to back-up
		}
	hr = m_pBlendTexture->UnlockRect(0);
	m_bNeedsBackUp = 0;
}
void TerrainTexture::BackUp()
{
	if(m_bNeedsBackUp)
		Update(0.0f);
		//BackUpBlendTex();
}

void TerrainTexture::PreRender()
{
	auto DSX = GCI->GetDeferredR();

	// Set shader variables
	if(m_pBlendTexture)
	{
		DSX->m_pFX->SetTexture("g_sBlendMap", m_pBlendTexture);
		DSX->m_pFX->SetBool("f_IsTerrain", true);
	}
}
void TerrainTexture::SetTexture(std::string variable)
{
	if(m_pBlendTexture)
		GCI->GetDeferredR()->m_pFX->SetTexture(variable.c_str(), m_pBlendTexture);
}

void TerrainTexture::Update(float dt)
{
	HRESULT hr;
	D3DLOCKED_RECT LockedRect;
	hr = m_pBlendTexture->LockRect(0, &LockedRect, NULL, 0);
	
	int index = 0;
	for( int i = 0; i < m_pUpdateList.size(); i++)
	{
		index = m_pUpdateList[i];
		((unsigned int*)LockedRect.pBits)[index] = m_pBlendTexColor[index];
	}
	hr = m_pBlendTexture->UnlockRect(0);
	m_pUpdateList.clear();
}

// Terrain Texture Painting
void TerrainTexture::PaintTex1(float increment, float radius, Vector3 position)
{
	//HRESULT hr;
	D3DXCOLOR color;
	//D3DLOCKED_RECT LockedRect;
	//hr = m_pBlendTexture->LockRect(0, &LockedRect, NULL, 0);

	// Find texture position index
	D3DXVECTOR2 texCoord = ConvertToTexCoord(position);
	int index = (texCoord.y * (m_fPitch)) + texCoord.x;
	if(index > m_iLastIndex)
		return;
	int texRadius = (radius) * m_fBlendTexScale;	// Get the radius of the brush in relation to texCoord

	// Apply changes to the texture pixels
	int r2 = texRadius*texRadius;		// Square radius in texture coordinate system
	for(int j = (texCoord.y - texRadius); j < (texCoord.y + texRadius); j++)
		if(j >= 0 && j < m_vBlendTexSize.y)
		for(int i = (texCoord.x - texRadius); i < (texCoord.x + texRadius); i++)
			if(i >= 0 && i < m_vBlendTexSize.x)
			{
				// Get the pixel index
				index = (j * (m_fPitch)) + i;
				if(index > m_iLastIndex || index < 0)
					continue;
			
				// Test if pixel falls inside circle
				int inCircle = (i - texCoord.x)*(i - texCoord.x) + (j - texCoord.y)*(j - texCoord.y);
				if(inCircle > r2)
					continue;		// Pixel not inside circle

				// Find 32-bit color at position
				color = m_pBlendTexColor[index];
				//color = ((unsigned int*)LockedRect.pBits)[index];
				color.r += increment;
				// If the color is over 1.0f, than reduce all the other colors by the difference
				if(color.r > 1.0f)	
				{
					float diff = color.r - 1.0f;
					color.r = 1.0f;
					color.g -= diff;
					color.b -= diff;

					// Make sure the colors stay between [0, 1]
					if(color.g < 0.0f)
						color.g = 0.0f;
					if(color.b < 0.0f)
						color.b = 0.0f;
				}
				else if(color.r < 0.0f)
				{
					// Use the difference to scale all the other colors up
					float diff = color.r;
					color.r = 0.0f;
					color.g += diff;
					color.b += diff;

					// Make sure the colors stay between [0, 1]
					if(color.g > 1.0f)
						color.g = 1.0f;
					if(color.b > 1.0f)
						color.b = 1.0f;
				}
				// Re-apply the color to the texture
				m_pBlendTexColor[index] = color;
				m_pUpdateList.push_back(index);
				//((unsigned int*)LockedRect.pBits)[index] = color;
			}
	
	//hr = m_pBlendTexture->UnlockRect(0);
	//m_bNeedsBackUp = 1; // Back-up needs to be updated
	Update(0.0f);
}
void TerrainTexture::PaintTex2(float increment, float radius, Vector3 position)
{
	//HRESULT hr;
	D3DXCOLOR color;
	//D3DLOCKED_RECT LockedRect;
	//hr = m_pBlendTexture->LockRect(0, &LockedRect, NULL, 0);

	// Find texture position index
	D3DXVECTOR2 texCoord = ConvertToTexCoord(position);
	int index = (texCoord.y * (m_fPitch)) + texCoord.x;
	if(index > m_iLastIndex)
		return;
	int texRadius = (radius) * m_fBlendTexScale;	// Get the radius of the brush in relation to texCoord

	// Apply changes to the texture pixels
	int r2 = texRadius*texRadius;		// Square radius in texture coordinate system
	for(int j = (texCoord.y - texRadius); j < (texCoord.y + texRadius); j++)
		if(j >= 0 && j < m_vBlendTexSize.y)
		for(int i = (texCoord.x - texRadius); i < (texCoord.x + texRadius); i++)
			if(i >= 0 && i < m_vBlendTexSize.x)
			{
				// Get the pixel index
				index = (j * (m_fPitch)) + i;
				if(index > m_iLastIndex || index < 0)
					continue;
			
				// Test if pixel falls inside circle
				int inCircle = (i - texCoord.x)*(i - texCoord.x) + (j - texCoord.y)*(j - texCoord.y);
				if(inCircle > r2)
					continue;		// Pixel not inside circle

				// Find 32-bit color at position
				color = m_pBlendTexColor[index];
				//color = ((unsigned int*)LockedRect.pBits)[index];
				color.g += increment;
				// If the color is over 1.0f, than reduce all the other colors by the difference
				if(color.g > 1.0f)	
				{
					float diff = color.g - 1.0f;
					color.g = 1.0f;
					color.r -= diff;
					color.b -= diff;

					// Make sure the colors stay between [0, 1]
					if(color.r < 0.0f)
						color.r = 0.0f;
					if(color.b < 0.0f)
						color.b = 0.0f;
				}
				else if(color.g < 0.0f)
				{
					// Use the difference to scale all the other colors up
					float diff = color.g;
					color.g = 0.0f;
					color.r += diff;
					color.b += diff;

					// Make sure the colors stay between [0, 1]
					if(color.r > 1.0f)
						color.r = 1.0f;
					if(color.b > 1.0f)
						color.b = 1.0f;
				}
				// Re-apply the color to the texture
				m_pBlendTexColor[index] = color;
				m_pUpdateList.push_back(index);
				//((unsigned int*)LockedRect.pBits)[index] = color;
			}
	
	//hr = m_pBlendTexture->UnlockRect(0);
	//m_bNeedsBackUp = 1; // Back-up needs to be updated
	Update(0.0f);
}
void TerrainTexture::PaintTex3(float increment, float radius, Vector3 position)
{
	//HRESULT hr;
	D3DXCOLOR color;
	//D3DLOCKED_RECT LockedRect;
	//hr = m_pBlendTexture->LockRect(0, &LockedRect, NULL, 0);

	// Find texture position index
	D3DXVECTOR2 texCoord = ConvertToTexCoord(position);
	int index = (texCoord.y * (m_fPitch)) + texCoord.x;
	if( index > m_iLastIndex)
		return;
	int texRadius = (radius) * m_fBlendTexScale;	// Get the radius of the brush in relation to texCoord

	// Apply changes to the texture pixels
	int r2 = texRadius*texRadius;		// Square radius in texture coordinate system
	for(int j = (texCoord.y - texRadius); j < (texCoord.y + texRadius); j++)
		if(j >= 0 && j < m_vBlendTexSize.y)
		for(int i = (texCoord.x - texRadius); i < (texCoord.x + texRadius); i++)
			if(i >= 0 && i < m_vBlendTexSize.x)
			{
				// Get the pixel index
				index = (j * (m_fPitch)) + i;
				if(index > m_iLastIndex || index < 0)
					continue;
			
				// Test if pixel falls inside circle
				int inCircle = (i - texCoord.x)*(i - texCoord.x) + (j - texCoord.y)*(j - texCoord.y);
				if(inCircle > r2)
					continue;		// Pixel not inside circle

				// Find 32-bit color at position
				color = m_pBlendTexColor[index];
				//color = ((unsigned int*)LockedRect.pBits)[index];
				color.b += increment;
				// If the color is over 1.0f, than reduce all the other colors by the difference
				if(color.b > 1.0f)	
				{
					float diff = color.b - 1.0f;
					color.b = 1.0f;
					color.r -= diff;
					color.g -= diff;

					// Make sure the colors stay between [0, 1]
					if(color.r < 0.0f)
						color.r = 0.0f;
					if(color.g < 0.0f)
						color.g = 0.0f;
				}
				else if(color.b < 0.0f)
				{
					// Use the difference to scale all the other colors up
					float diff = color.b;
					color.b = 0.0f;
					color.r += diff;
					color.g += diff;

					// Make sure the colors stay between [0, 1]
					if(color.r > 1.0f)
						color.r = 1.0f;
					if(color.g > 1.0f)
						color.g = 1.0f;
				}
				// Re-apply the color to the texture
				m_pBlendTexColor[index] = color;
				m_pUpdateList.push_back(index);
				//((unsigned int*)LockedRect.pBits)[index] = color;
			}
	
	//hr = m_pBlendTexture->UnlockRect(0);
	//m_bNeedsBackUp = 1; // Back-up needs to be updated
	Update(0.0f);
}

D3DXVECTOR2 TerrainTexture::ConvertToTexCoord(D3DXVECTOR3 worldCoord)
{
	// Move x and z starting coordinates to the top left
	worldCoord.x += m_vTerrainSize.x*0.5f;
	worldCoord.z -= m_vTerrainSize.z*0.5f;
	worldCoord.z *= -1.0f;			// Make positive

	// Scale it to the texture coordinate equivalent
	worldCoord.x *= m_fBlendTexScale;
	worldCoord.z *= m_fBlendTexScale;

	// Adjust texture coordinat for directx9 offset
	worldCoord.x -= m_fBlendTexScale*0.5f;
	worldCoord.z -= m_fBlendTexScale*0.5f;

	return D3DXVECTOR2(round(worldCoord.x), round(worldCoord.z));
}

void TerrainTexture::BackUpBlendTex()
{
	int index;
	D3DLOCKED_RECT LockedRect;
	HRESULT hr = m_pBlendTexture->LockRect(0, &LockedRect, NULL, 0);
	
	//int backup = 0;
	for(int j = 0; j < m_vBlendTexSize.y; j++)
		for(int i = 0; i < m_vBlendTexSize.x; i++)
		{
			// Find index at position
			index = (j * (m_fPitch)) + i;

			// Find 32-bit color at position and back it up
			m_pBlendTexColor[index] = ((unsigned int*)LockedRect.pBits)[index];
			//backup++;
		}

	hr = m_pBlendTexture->UnlockRect(0);
	m_bNeedsBackUp = 0; // Blend texture has been backed up
}

void TerrainTexture::Load(std::string texName, Vector3 terrainSize)
{
	m_vTerrainSize = terrainSize;

	// Get properties of the blended texture
	D3DXIMAGE_INFO info;
	texName += ".bmp";
	HRESULT hr = D3DXGetImageInfoFromFileA(texName.c_str(), &info);
	m_vBlendTexSize.x = info.Width;
	m_vBlendTexSize.y = info.Height;
	m_fBlendTexScale = info.Width / m_vTerrainSize.x;

	// Load the blended texture
	hr = D3DXCreateTextureFromFileEx(GCI->GetDirectX()->GetDevice(), texName.c_str(),
		info.Width, info.Height, info.MipLevels, D3DUSAGE_DYNAMIC, 
		D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
		D3DCOLOR_XRGB(0, 0, 0), &info, 0, &m_pBlendTexture);

	// Set the last index of the texture
	D3DLOCKED_RECT LockedRect;
	hr = m_pBlendTexture->LockRect(0, &LockedRect, NULL, 0);
	m_fPitch = LockedRect.Pitch / 4;
	m_iLastIndex = ((m_vBlendTexSize.y-1) * (m_fPitch)) + (m_vBlendTexSize.x-1);
	hr = m_pBlendTexture->UnlockRect(0);

	// Store the blended texture back-up
	m_pBlendTexColor.resize(m_iLastIndex+1);
	BackUpBlendTex();
}
void TerrainTexture::Save(std::string texName)
{
	if(m_pBlendTexture)	// Make sure theirs a texture
	{
		HRESULT hr;
		texName += ".bmp";	// Setup the picture file name
		hr = D3DXSaveTextureToFileA(texName.c_str(), D3DXIFF_BMP, m_pBlendTexture, NULL);	// Save the texture to a bmp file
	}
}

D3DXCOLOR TerrainTexture::GetColorAt(Vector3 worldCoord)
{
	// Find texture position index
	D3DXVECTOR2 texCoord = ConvertToTexCoord(worldCoord);

	// Make sure the texture coordinate is valid
	if( texCoord.x >= m_vBlendTexSize.x || texCoord.x < 0 || 
		texCoord.y >= m_vBlendTexSize.y || texCoord.y < 0)
		return D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f);	// Returning 0.0 alpha means it's off the texture

	HRESULT hr;
	D3DXCOLOR color;
	D3DLOCKED_RECT LockedRect;
	//hr = m_pBlendTexture->LockRect(0, &LockedRect, NULL, 0);
	
	// Make sure the texture coordinate is valid
	int index = (texCoord.y * (m_fPitch)) + texCoord.x;
	if( index > m_iLastIndex || index < 0)	// Check if position is outside texture
		return D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f);	// Returning 0.0 alpha means it's off the texture
	//color = ((unsigned int*)LockedRect.pBits)[index];	// Get color from the texture
	color = m_pBlendTexColor[index];

	//hr = m_pBlendTexture->UnlockRect(0);
	return color;
}
