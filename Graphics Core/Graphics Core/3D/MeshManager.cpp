#include "MeshManager.h"
#include "../GraphicsCore.h"

MeshManager::MeshManager()
{}
MeshManager::~MeshManager()
{}

void MeshManager::PreRestartDevice()
{
	for(auto m = m_MeshList.begin(); m != m_MeshList.end(); m++)
		(*m)->PreRestartDevice();
}
void MeshManager::PostRestartDevice()
{
	for(auto m = m_MeshList.begin(); m != m_MeshList.end(); m++)
		(*m)->PostRestartDevice();
}

void MeshManager::Shutdown()
{
	// Unload all meshes
	for(auto m = m_MeshList.begin(); m != m_MeshList.end(); m++)
		(*m)->Unload();

	// Clear the map of meshes
	//m_MeshMap.clear();
	//m_MeshList.clear();
}

bool MeshManager::Load(std::string fileName)
{
	if( m_MeshMap.find(fileName) != m_MeshMap.end() )
		return true;	// This mesh is already loaded

	// Create Mesh
	SceneMeshPtr mesh = std::shared_ptr<SceneMesh>(new SceneMesh);
	if(!mesh->Load(fileName))
		return false;
	
	// Link mesh to file name
	m_MeshList.push_back( mesh );
	m_MeshMap.insert( std::pair<std::string, SceneMeshPtr>(fileName, mesh) );
	return true;
}
void MeshManager::Unload(std::string fileName)
{
	std::string path = m_MeshMap[fileName]->GetPath();

	// Unload the mesh
	m_MeshMap[fileName]->Unload();
	m_MeshMap[fileName] = nullptr;		// Set to a Null ptr

	// Remove from the list
	int i = 0;
	for(; i < m_MeshList.size(); i++) 
		if(*m_MeshList[i] == path)
		{
			m_MeshList.erase( m_MeshList.begin() + i );
			break;
		}
	// Remove from the map
	m_MeshMap.erase( m_MeshMap.find(fileName) );
}
void MeshManager::Render(std::string mesh, std::vector<BaseObjectPtr> meshList)
{
	if( m_MeshMap.find(mesh)->first == "" )
		return;

	DeferredShader* DSX = GCI->GetDeferredR();

	int objDrawn = 0;
	for(auto i = meshList.begin(); 
		i != meshList.end(); i++)
	{
		D3DXMATRIX	transform = (*i)->m_Tranform.GetTransform();
		D3DXCOLOR	color	= (*i)->m_Color;
		// Check if mesh is in the frustum
		if( !((*i)->m_bInView = GCI->GetCamera()->IsVisible(*m_MeshMap[mesh]->GetBV(), transform)) )
			continue;
		
		// Set transform
		D3DXMATRIX invTrans;
		D3DXMATRIX invObjNorm;
		//D3DXMatrixInverse(&invObjNorm, 0,&transform);
		D3DXMatrixInverse(&invTrans, 0, &transform);
		D3DXMatrixTransformation(&invObjNorm, NULL, NULL,
								&D3DXVECTOR3(1, 1, 1), NULL, NULL,
								&D3DXVECTOR3(invTrans(0, 2), -invTrans(1, 2),  invTrans(2, 2)));
		DSX->m_pFX->SetMatrix("g_mObjLookMatrix",&invObjNorm);
		DSX->m_pFX->SetMatrix(DSX->m_hWorldInvTrans, &invTrans);
		DSX->m_pFX->SetMatrix( DSX->m_hWorld, &transform );
		DSX->m_pFX->SetMatrix("g_sLightVP", &(transform*DSX->m_Light.GetLightViewProj()));
		//DSX->m_pFX->SetMatrix("g_sLightV", &(transform * DSX->m_Light.GetLightView() ));
		DSX->m_pFX->SetMatrix("g_sView", &(transform * GCI->GetCamera()->GetView() ));
		DSX->m_pFX->SetMatrix( DSX->m_hWVP, &(transform*GCI->GetCamera()->GetViewProj()) );

		// Render Mesh
		if((*i)->m_Color.a == -1)	// Use Default color
			m_MeshMap[mesh]->Render();
		else
		{
			// Setup default textures
			m_MeshMap[mesh]->PreRender("DEFAULT");

			// Set up the custom Material
			GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hAmbientMtrl, &(*i)->m_Color, sizeof(D3DXCOLOR));
			GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hDiffuseMtrl, &(*i)->m_Color, sizeof(D3DXCOLOR));
			GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hSpecMtrl, &WHITE, sizeof(D3DXCOLOR));
			GCI->GetDeferredR()->m_pFX->SetFloat(GCI->GetDeferredR()->m_hSpecPower, 8.0f);
			//GCI->GetDeferredR()->m_pFX->SetTexture("g_sTexture", GCI->GetDeferredR()->m_pDefaultTex);	// Use Default Texture (White)

			DSX->m_pFX->CommitChanges();
			m_MeshMap[mesh]->RawRender();
		}

		objDrawn++;
	}
	DSX->AddNumObjDrawn( objDrawn );	// Increment the number of objects drawn
}
void MeshManager::Render(std::string mesh, std::string texture, std::vector<BaseObjectPtr> meshList)
{
	if( m_MeshMap.find(mesh)->first == "" )
		return;

	DeferredShader* DSX = GCI->GetDeferredR();

	// Set mesh attributes
	m_MeshMap[mesh]->PreRender( texture );
	
	int objDrawn = 0;
	for(auto i = meshList.begin(); 
		i != meshList.end(); i++)
	{
		D3DXMATRIX	transform = (*i)->m_Tranform.GetTransform();
		D3DXCOLOR	color	= (*i)->m_Color;
		// Check if mesh is in the frustum
		if( !((*i)->m_bInView = GCI->GetCamera()->IsVisible(*m_MeshMap[mesh]->GetBV(), transform)) )
			continue;
		
		// Set transform
		D3DXMATRIX invTrans;
		D3DXMatrixInverse(&invTrans, 0, &transform);
		DSX->m_pFX->SetMatrix(DSX->m_hWorldInvTrans, &invTrans);
		DSX->m_pFX->SetMatrix( DSX->m_hWorld, &transform );
		DSX->m_pFX->SetMatrix("g_sLightVP", &(transform*DSX->m_Light.GetLightViewProj()));
		//DSX->m_pFX->SetMatrix("g_sLightV", &(transform * DSX->m_Light.GetLightView() ));
		DSX->m_pFX->SetMatrix("g_sView", &(transform * GCI->GetCamera()->GetView() ));
		DSX->m_pFX->SetMatrix( DSX->m_hWVP, &(transform*GCI->GetCamera()->GetViewProj()) );
		DSX->m_pFX->CommitChanges();
		
		// Render Mesh
		if((*i)->m_Color.a == -1)	// Use Default color
		{
			m_MeshMap[mesh]->PreRender(texture);
			m_MeshMap[mesh]->RawRender();
		}
		else
		{
			// Set up the custom Material
			GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hAmbientMtrl, &(*i)->m_Color, sizeof(D3DXCOLOR));
			GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hDiffuseMtrl, &(*i)->m_Color, sizeof(D3DXCOLOR));
			GCI->GetDeferredR()->m_pFX->SetValue(GCI->GetDeferredR()->m_hSpecMtrl, &WHITE, sizeof(D3DXCOLOR));
			GCI->GetDeferredR()->m_pFX->SetFloat(GCI->GetDeferredR()->m_hSpecPower, 8.0f);
			GCI->GetDeferredR()->m_pFX->SetTexture("g_sTexture", GCI->GetTextureManager()->TextureList[texture]->texture);	// Use Default Texture (White)

			DSX->m_pFX->CommitChanges();
			m_MeshMap[mesh]->RawRender();
		}
		objDrawn++;
	}
	DSX->AddNumObjDrawn( objDrawn );	// Increment the number of objects drawn
}
void MeshManager::Render(std::string mesh, D3DXMATRIX transforms[], int count)
{
	if( m_MeshMap.find(mesh)->first == "" )
		return;

	DeferredShader* DSX = GCI->GetDeferredR();

	int objDrawn = 0;
	for(int i = 0; i < count; i++)
	{
		// Check if mesh is in the frustum
		if( !GCI->GetCamera()->IsVisible(*m_MeshMap[mesh]->GetBV(), transforms[i]) )
			continue;

		// Set transform
		D3DXMATRIX invTrans;
		D3DXMatrixInverse(&invTrans, 0, &transforms[i]);
		DSX->m_pFX->SetMatrix(DSX->m_hWorldInvTrans, &invTrans);
		DSX->m_pFX->SetMatrix( DSX->m_hWorld, &transforms[i] );
		DSX->m_pFX->SetMatrix("g_sLightVP", &(transforms[i]*DSX->m_Light.GetLightViewProj()));
		//DSX->m_pFX->SetMatrix("g_sLightV", &(transforms[i] * DSX->m_Light.GetLightView() ));
		DSX->m_pFX->SetMatrix("g_sView", &(transforms[i] * GCI->GetCamera()->GetView() ));
		DSX->m_pFX->SetMatrix( DSX->m_hWVP, &(transforms[i]*GCI->GetCamera()->GetViewProj()) );

		// Render Mesh
		m_MeshMap[mesh]->Render();
		objDrawn++;
	}
	DSX->AddNumObjDrawn( objDrawn );	// Increment the number of objects drawn
}
void MeshManager::Render(std::string mesh, std::string texture, D3DXMATRIX transforms[], int count)
{
	if( m_MeshMap.find(mesh)->first == "" )
		return;

	DeferredShader* DSX = GCI->GetDeferredR();

	// Set mesh attributes
	m_MeshMap[mesh]->PreRender( texture );
	
	int objDrawn = 0;
	for(int i = 0; i < count; i++)
	{
		// Check if mesh is in the frustum
		if( !GCI->GetCamera()->IsVisible(*m_MeshMap[mesh]->GetBV(), transforms[i]) )
			continue;

		// Set transform
		D3DXMATRIX invTrans;
		D3DXMatrixInverse(&invTrans, 0, &transforms[i]);
		DSX->m_pFX->SetMatrix(DSX->m_hWorldInvTrans, &invTrans);
		DSX->m_pFX->SetMatrix( DSX->m_hWorld, &transforms[i] );
		DSX->m_pFX->SetMatrix("g_sLightVP", &(transforms[i]*DSX->m_Light.GetLightViewProj()));
		//DSX->m_pFX->SetMatrix("g_sLightV", &(transforms[i] * DSX->m_Light.GetLightView() ));
		DSX->m_pFX->SetMatrix("g_sView", &(transforms[i] * GCI->GetCamera()->GetView() ));
		DSX->m_pFX->SetMatrix( DSX->m_hWVP, &(transforms[i]*GCI->GetCamera()->GetViewProj()) );
		DSX->m_pFX->CommitChanges();

		// Render Mesh
		m_MeshMap[mesh]->RawRender();
		objDrawn++;
	}
	DSX->AddNumObjDrawn( objDrawn );	// Increment the number of objects drawn
}

D3DXVECTOR3 MeshManager::GetBVHalfExtent(std::string mesh)
{
	return m_MeshMap[mesh]->GetBV()->GetHalfExtent();
}

SceneMeshPtr MeshManager::GetMesh(std::string mesh)
{
	return m_MeshMap[mesh];
}

bool MeshManager::HasMesh(std::string mesh)
{
	return (m_MeshMap.find(mesh) != m_MeshMap.end());
}
