#include "BoundingVolume.h"
#include "../GraphicsCore.h"
#include "../MacroTools.h"

BoundingVolume::BoundingVolume()
{
	m_Box			= 0;
	m_Sphere		= 0;
}
BoundingVolume::~BoundingVolume()
{
	SAFE_RELEASE(m_Box);
	SAFE_RELEASE(m_Sphere);
}

ID3DXMesh** BoundingVolume::GetBoundingBox(ID3DXMesh** mesh)
{
	if(m_Box)	// Check if the Box was alread created
		return &m_Box;

	VertexType* boxVerts = 0;
	(*mesh)->LockVertexBuffer(0, (void**)&boxVerts);
	
	D3DXComputeBoundingBox(&boxVerts[0].position,
		(*mesh)->GetNumVertices(), sizeof(VertexType),
		&m_MinPoint, &m_MaxPoint);	// Returns the points

	(*mesh)->UnlockVertexBuffer();

	//***************************************************************************
	// Display Bounding Box volume
	float width = m_MaxPoint.x - m_MinPoint.x;
	float height = m_MaxPoint.y - m_MinPoint.y;
	float depth = m_MaxPoint.z - m_MinPoint.z;
	D3DXCreateBox(GCI->GetDirectX()->GetDevice(), width, height, depth, &m_Box, 0);

	// Calculate the box's center
	m_Position = 0.5f*(m_MinPoint + m_MaxPoint);

	// Calculate the box's extent
	m_Extent = (m_MaxPoint-m_MinPoint);//*0.5f;

	// Calculate the box's offset
	D3DXMatrixTranslation(&m_Offset, -m_Position.x,
		-m_Position.y, -m_Position.z);

	return &m_Box;
}

ID3DXMesh** BoundingVolume::GetBoundingSphere(ID3DXMesh** mesh)
{
	if(m_Sphere)	// Check if the Sphere was already created
		return &m_Sphere;

	VertexType* meshVerts = 0;
	(*mesh)->LockVertexBuffer(0, (void**)&meshVerts);
	
	D3DXComputeBoundingSphere(&meshVerts[0].position,
		(*mesh)->GetNumVertices(), sizeof(VertexType),
		&m_Position, &m_Radius);

	(*mesh)->UnlockVertexBuffer();
	
	//***************************************************************************
	// Display Bounding Sphere volume
	D3DXCreateSphere(GCI->GetDirectX()->GetDevice(), m_Radius, 25, 25, &m_Sphere, 0);
	
	// Set the sphere's extent
	m_Extent = D3DXVECTOR3(m_Radius, m_Radius, m_Radius);

	return &m_Sphere;
}

void BoundingVolume::Render()
{
	if(m_Box)
		m_Box->DrawSubset(0);
	if(m_Sphere)
		m_Sphere->DrawSubset(0);
}
D3DXVECTOR3 BoundingVolume::GetMin()
{	return m_MinPoint;	}
D3DXVECTOR3	BoundingVolume::GetMax()
{	return m_MaxPoint;	}
D3DXVECTOR3 BoundingVolume::GetCenter()
{	return m_Position;	}
D3DXVECTOR3 BoundingVolume::GetExtent()
{	return m_Extent;	}
D3DXVECTOR3 BoundingVolume::GetHalfExtent()
{
	return D3DXVECTOR3(m_Extent.x*0.5f, m_Extent.y*0.5f, m_Extent.z*0.5f);
}
D3DXMATRIX	BoundingVolume::GetOffset()
{	return m_Offset;	}
void BoundingVolume::Scale(float scale)
{
	// Scale the min and max values
	m_MinPoint *= scale;
	m_MaxPoint *= scale;

	// Recalculate the box's center
	m_Position = 0.5f*(m_MinPoint + m_MaxPoint);

	// Recalculate the box's extent
	m_Extent = (m_MaxPoint-m_MinPoint)*0.5f;

	// Recalculate the box's offset
	D3DXMatrixTranslation(&m_Offset, m_Position.x,
		m_Position.y, m_Position.z);
}
