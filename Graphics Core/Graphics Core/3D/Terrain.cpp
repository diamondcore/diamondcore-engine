#include "Terrain.h"
#include "../GraphicsCore.h"
#include "../MacroTools.h"
#include <fstream>

Terrain::Terrain()
	:m_bNeedsUpdate(0),
	 m_bNeedsNormUpdate(0),
	 m_iNumTris(0)
{
	m_pMesh			= 0;
	m_pAdjList		= 0;
	m_fTexScale		= 0.01f;
	m_pBrushRadius	= 0;
	m_bValidBrush	= 0;
	m_bRenderCustomTex	= 0;
	m_iNumRows		= 0;
	m_iNumCols		= 0;
	m_fAvgHeight	= 0.0f;
	m_fPhongExp		= 0.0f;		//5.0f;
	m_fPhongCoeff	= 0.03f;	//0.01f;	//1.0f;
	m_fDiffuseCoeff	= 1.0f;
	m_Ambient		= D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.0f);
	m_Diffuse		= D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.0f);
	m_Specular		= WHITE;
	m_fSpecPower	= 8.0f;
	m_fHalfPI		= D3DX_PI/2.0f;
	D3DXMatrixIdentity(&m_TransMat);
}
Terrain::~Terrain()
{
	Clear();
}
void Terrain::Clear()
{
	SAFE_DELETE_ARRAY(m_pAdjList);
	m_iNumTris = 0;
	m_bNeedsUpdate = 0;
	m_bNeedsNormUpdate = 0;
	m_fAvgHeight	= 0.0f;
	m_VertexList.clear();
	m_IndexList.clear();
	m_pBlendTexture.Clear();
	m_pCustomTexture.Clear();
	SAFE_RELEASE(m_pMesh);
}
void Terrain::Reset()
{
	Clear();
	Init(m_iNumRows, m_iNumCols, m_vSize.z, m_vSize.x, m_fTexScale);
}
void Terrain::PreRestartDevice()
{
	m_pBlendTexture.PreRestartDevice();
	m_pCustomTexture.PreRestartDevice();
	SAFE_RELEASE(m_pMesh);
	SAFE_DELETE_ARRAY(m_pAdjList);
}
void Terrain::PostRestartDevice()
{
	m_pCustomTexture.PostRestartDevice();
	m_pBlendTexture.PostRestartDevice();
	BuildMesh();
}

// Generate grid of specified size
void Terrain::Init(unsigned int rows, unsigned int cols, unsigned int sizeZ, unsigned int sizeX, float texScale)
{
	m_iNumTris = (rows - 1) * (cols - 1) * 2;	// Set the number of triangles
	if(m_iNumTris > 65533)	// Maximum number of faces per mesh
	{
		// Set the max num verts relative to the ratio attempted
		float ratio = ((float)cols/(float)rows);
		rows = sqrt((65533.0f * 0.5f)) * ((float)rows/(float)cols);
		cols = sqrt((65533.0f * 0.5f)) * ratio;
		//char error[256];
		//sprintf(error, "Terrain faces exceed maximum limit.\n Terrain has been reduced to:\n Rows: %i\n Cols: %i", rows, cols); 
		//MessageBox(0, error, "Warning", MB_OK);
		m_iNumTris = (rows - 1) * (cols - 1) * 2;	// Set the number of triangles
	}
	m_iNumRows = rows;
	m_iNumCols = cols;
	m_vSize = D3DXVECTOR3(sizeX, 0.0f, sizeZ);	// Set the size of the grid
	//m_VertexList.resize(rows * cols);
	m_VertexList.resize(rows, cols);
	m_IndexList.resize(m_iNumTris * 3);
	m_vSpace.x = m_vSize.x / m_iNumCols;
	m_vSpace.y = m_vSize.z / m_iNumRows;
	
	// Create Texture for the Terrain
	m_fTexScale = texScale;
	m_pBlendTexture.Init(m_vSize, texScale, D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));
	m_pCustomTexture.Init(m_vSize, texScale, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));

	// Find the spacing between each vertex
	float spaceX = (float)sizeX/(float)(cols-1.0f);
	float spaceZ = (float)sizeZ/(float)(rows-1.0f);

	D3DXVECTOR3 center(0.0f, 0.0f,0.0f);

	// Set the vertices' positions
	int iter = 0;
	float startX = sizeX * -0.5f;
	float startZ = sizeZ * 0.5f;
	float w = sizeX; // This is the texture x scale coordinates
	float d = sizeZ; // This is the texture y scale coordinates
	for(int r = 0; r < rows; r++)
		for(int c = 0; c < cols; c++)
		{
			// Calculate grid position
			m_VertexList[r][c].position.x	= startX + spaceX*c;
			m_VertexList[r][c].position.z	= -startZ + spaceZ*r;
			m_VertexList[r][c].position.y	= 0.0f;

			// Set default normal
			m_VertexList[r][c].normal	= D3DXVECTOR3(0.0f, 1.0f, 0.0f);

			// Generate texture coordinates
			m_VertexList[r][c].texturePos.x	= (m_VertexList[r][c].position.x + (0.5f*w))/w;
			m_VertexList[r][c].texturePos.y = (m_VertexList[r][c].position.z - (0.5f*d))/-d;
			iter++;	// Iterate the vertex count
		}

	// Calculate the indices
	int k = 0;
	for(WORD i = 0; i < (WORD)(rows-1); i++)
		for(WORD j = 0; j < (WORD)(cols-1); j++)
		{
			// First Triangle
			m_IndexList[k + 2]	= i * cols + j;			// Bottom left
			m_IndexList[k + 1]	= i * cols + j + 1;		// Bottom Right
			m_IndexList[k]		= (i+1) * cols + j;		// Top Left

			// Second Triangle
			m_IndexList[k + 5]	= (i+1) * cols + j;		// Top Left
			m_IndexList[k + 4]	= i * cols + j + 1;		// Bottom Right
			m_IndexList[k + 3]	= (i+1) * cols + j + 1;	// Top Right

			k += 6;
		}

	// Build the Graphics Object
	BuildMesh();
	m_bNeedsUpdate = 0;
	m_bNeedsNormUpdate = 0;
}

void Terrain::BuildMesh()
{
	SAFE_RELEASE(m_pMesh);

	D3DVERTEXELEMENT9 elems[MAX_FVF_DECL_SIZE];
	UINT numElems = 0;
	HRESULT hr = VertexBlendEx::Decl->GetDeclaration(elems, &numElems);
	hr = D3DXCreateMesh(m_iNumTris, m_VertexList.size(), D3DXMESH_SYSTEMMEM, elems,
		GCI->GetDirectX()->GetDevice(), &m_pMesh);

	// Set Vertex information
	VertexBlendEx* v = 0;
	int size = m_VertexList.size()*sizeof(VertexBlendEx);	// Get size of buffer in bytes
	hr = m_pMesh->LockVertexBuffer(0, (void**)&v);
	int i = 0;
	for(int r = 0; r < m_iNumRows; r++)
		for(int c = 0; c < m_iNumCols; c++)
		{
			v[i].position	= m_VertexList[r][c].position;
			v[i].normal		= m_VertexList[r][c].normal;
			v[i].texturePos	= m_VertexList[r][c].texturePos;
			i++;
		}
	//memcpy_s(&v[0], size, &m_VertexList[0], size);
	hr = m_pMesh->UnlockVertexBuffer();

	// Set Index information
	WORD* k = 0;
	size = m_IndexList.size()*sizeof(WORD);			// Get size of buffer in bytes
	hr = m_pMesh->LockIndexBuffer(0, (void**)&k);
	memcpy_s(&k[0], size, &m_IndexList[0], size);
	hr = m_pMesh->UnlockIndexBuffer();

	DWORD* attBuffer = 0;
	hr = m_pMesh->LockAttributeBuffer(0, &attBuffer);
	memset(&attBuffer[0], 0, m_iNumTris);
	//for(int i = 0; i < m_iNumTris; i++)
	//{
	//	k[i*3+0]	= (WORD)m_IndexList[i*3+0];
	//	k[i*3+1]	= (WORD)m_IndexList[i*3+1];
	//	k[i*3+2]	= (WORD)m_IndexList[i*3+2];
	//	
	//	attBuffer[i]	= 0;	// Always subset 0
	//}	
	hr = m_pMesh->UnlockAttributeBuffer();

	// Generte Normals
	hr = D3DXComputeNormals(m_pMesh, 0);
	m_pAdjList = new DWORD[m_pMesh->GetNumFaces()*3];
	hr = m_pMesh->GenerateAdjacency(EPSILON, m_pAdjList);
	hr = m_pMesh->OptimizeInplace(D3DXMESH_DYNAMIC |   //D3DXMESH_MANAGED | 
				  	    D3DXMESHOPT_COMPACT | 
						D3DXMESHOPT_ATTRSORT | 
						D3DXMESHOPT_VERTEXCACHE,	//D3DXMESHOPT_VERTEXCACHE | D3DXMESHOPT_ATTRSORT,
						m_pAdjList, 0, 0, 0);
	//SAFE_DELETE_ARRAY(adj);
	
	// Compute tangents for Deferred shader
	// Prev way
	hr = D3DXComputeTangentFrameEx(m_pMesh,
						D3DDECLUSAGE_TEXCOORD, 0,
						D3DDECLUSAGE_BINORMAL, 0,
						D3DDECLUSAGE_TANGENT, 0,
						D3DDECLUSAGE_NORMAL, 0,
						0, m_pAdjList, 0.01f, 0.25f, 0.01f,
						&m_pMesh, 0);
	
	// GenerateNormals() way, but modified to generate all
/*	hr = D3DXComputeTangentFrameEx(m_pMesh,
					D3DDECLUSAGE_TEXCOORD, 0,
					D3DDECLUSAGE_BINORMAL, 0,
					D3DDECLUSAGE_TANGENT, 0,
					D3DDECLUSAGE_NORMAL, 0,
					D3DXTANGENT_GENERATE_IN_PLACE | D3DXTANGENT_CALCULATE_NORMALS,  //0, 
					m_pAdjList, 
					//-1.01f, -0.01f, -1.01f, NULL, NULL);
					0.01f, 0.25f, 0.01f, NULL, NULL);*/
	//hr = D3DXSaveMeshToX("Terrain.x", m_pMesh, 0, 0, 0, 0, D3DXF_FILEFORMAT_TEXT); 
}

// Terrain Texture Painting
void Terrain::PaintTex1(int tex, float increment)
{
	if(!m_bValidBrush)
		return;
	if(tex == 0)
		m_pBlendTexture.PaintTex1(increment, *m_pBrushRadius, m_vBrushPos);
	else if(tex == 1)
		m_pCustomTexture.PaintTex1(increment, *m_pBrushRadius, m_vBrushPos);
}
void Terrain::PaintTex2(int tex, float increment)
{
	if(!m_bValidBrush)
		return;
	if(tex == 0)
		m_pBlendTexture.PaintTex2(increment, *m_pBrushRadius, m_vBrushPos);
	else if(tex == 1)
		m_pCustomTexture.PaintTex2(increment, *m_pBrushRadius, m_vBrushPos);
}
void Terrain::PaintTex3(int tex, float increment)
{
	if(!m_bValidBrush)
		return;
	if(tex == 0)
		m_pBlendTexture.PaintTex3(increment, *m_pBrushRadius, m_vBrushPos);
	else if(tex == 1)
		m_pCustomTexture.PaintTex3(increment, *m_pBrushRadius, m_vBrushPos);
}

// Terrain Morphing
void Terrain::SetBrush(float* radius)
{
	// Hide brush if less than or equal to 0.0f radius
	//if(!radius)
	//{
	//	auto DSX = GCI->GetDeferredR();
	//	DSX->m_pFX->SetBool("f_UseBrush", false);	// Turn off brush when point off of the terrain
	//	DSX->m_pFX->CommitChanges();
	//	return;
	//}
	m_pBrushRadius	= radius;
	UpdateBrush();
}
void Terrain::MorphUp(float increment)
{
	for(int i = 0; i < m_pSelectedVerts.size(); i++)
	{
		// Scale the increment with respect to the center of the brush
		float scale = (*m_pBrushRadius - (*m_pBrushRadius - m_SelectedDist[i]))/(*m_pBrushRadius);
		scale = cosf( m_fHalfPI * scale );
		m_pSelectedVerts[i]->position.y += increment * scale;
	}
	m_bNeedsUpdate = 1;
}
void Terrain::MorphDown(float increment)
{
	for(int i = 0; i < m_pSelectedVerts.size(); i++)
	{
		// Scale the increment with respect to the center of the brush
		float scale = (*m_pBrushRadius - (*m_pBrushRadius - m_SelectedDist[i]))/(*m_pBrushRadius);
		scale = cosf( m_fHalfPI * scale );
		m_pSelectedVerts[i]->position.y -= increment * scale;
	}
	m_bNeedsUpdate = 1;
}
void Terrain::Smooth(float scale)
{
	// Smooth with a scale
	for(int i = 0; i < m_pSelectedVerts.size(); i++)
		m_pSelectedVerts[i]->position.y += scale * (m_fAvgHeight - m_pSelectedVerts[i]->position.y);
	m_bNeedsUpdate = 1;
}
void Terrain::SmoothCenter(float scale)
{
	// Smooth with a scale
	for(int i = 0; i < m_pSelectedVerts.size(); i++)
	{
		float dist = (*m_pBrushRadius - (*m_pBrushRadius - m_SelectedDist[i]))/(*m_pBrushRadius);
		m_pSelectedVerts[i]->position.y += scale * ((m_fAvgHeight - m_pSelectedVerts[i]->position.y) * dist);
	}
	m_bNeedsUpdate = 1;
}

void Terrain::UpdateBuffers()
{
	HRESULT hr;
	int size;
	
	// Copy Vertex List into Vertex Buffer
	VertexBlendEx* verts = 0;
	size = m_VertexList.size()*sizeof(VertexBlendEx);	// Get size of buffer in bytes
	hr = m_pMesh->LockVertexBuffer(0,(void**)&verts);
	int i = 0;
	for(int r = 0; r < m_iNumRows; r++)
		for(int c = 0; c < m_iNumCols; c++)
		{
			verts[i].position	= m_VertexList[r][c].position;
			//verts[i].normal		= m_VertexList[r][c].normal;
			//verts[i].texturePos	= m_VertexList[r][c].texturePos;
			i++;
		}
	//	memcpy_s(&verts[0], size, &m_VertexList[0], size);
	hr = m_pMesh->UnlockVertexBuffer();

	// Copy Index List into Index Buffer
	WORD* indices = 0;
	size = m_IndexList.size()*sizeof(WORD);			// Get size of buffer in bytes
	hr = m_pMesh->LockIndexBuffer(0, (void**)&indices);
	memcpy_s(&indices[0], size, &m_IndexList[0], size);
	hr = m_pMesh->UnlockIndexBuffer();

	// Reset the need update flag 
	m_bNeedsUpdate = 0;
	m_bNeedsNormUpdate = 1; // Need to update normals after changing verts
}
void Terrain::UpdateNormals()
{	
	// Generte Normals, binormals, tangent, and texcoord
	HRESULT hr;	
	// Compute tangents for Deferred shader
	hr = D3DXComputeTangentFrameEx(m_pMesh,
						D3DDECLUSAGE_TEXCOORD, 0,
						D3DDECLUSAGE_BINORMAL, 0,
						D3DDECLUSAGE_TANGENT, 0,
						D3DDECLUSAGE_NORMAL, 0,
						D3DXTANGENT_GENERATE_IN_PLACE | D3DXTANGENT_CALCULATE_NORMALS,  //0, 
						m_pAdjList, 
						//-1.01f, -0.01f, -1.01f, NULL, NULL);	// GenerateNormals() way, but modified to generate all
						//0.01f, 0.25f, 0.01f, &m_pMesh, 0);	// Prev way
						0.01f, 0.25f, 0.01f, NULL, NULL);		// Hybrid settings

/*
	// Doesn't generate normals or texcoord
	hr = D3DXComputeTangentFrameEx(m_pMesh, 
					D3DDECLUSAGE_TEXCOORD, 0,
					D3DDECLUSAGE_TANGENT, 0, 
					D3DDECLUSAGE_BINORMAL, 0,
					D3DX_DEFAULT, 0, 
					D3DXTANGENT_GENERATE_IN_PLACE, 
					&m_pAdjList[0], 
					//-1.01f, -0.01f, -1.01f, NULL, NULL);
					0.01f, 0.25f, 0.01f, NULL, NULL);
*/

	/*	
	// Update TBN in the local buffer
	VertexBlendEx* verts = 0;
	int size = m_VertexList.size()*sizeof(VertexBlendEx);	// Get size of buffer in bytes
	hr = m_pMesh->LockVertexBuffer(0,(void**)&verts);
	int i = 0;
	for(int r = 0; r < m_iNumRows; r++)
		for(int c = 0; c < m_iNumCols; c++)
		{
			m_VertexList[r][c].position   = verts[i].position;
			m_VertexList[r][c].normal     = verts[i].normal;
			m_VertexList[r][c].texturePos = verts[i].texturePos;
			m_VertexList[r][c].binormal   = verts[i].binormal;
			m_VertexList[r][c].tangent    = verts[i].tangent;
			i++;
		}
	hr = m_pMesh->UnlockVertexBuffer();*/

	m_bNeedsNormUpdate = 0;
}

// Get distance to brush
float GetDistToBrush(D3DXVECTOR3 vert, D3DXVECTOR3 brush)
{
	D3DXVECTOR3 result;
	result = brush - vert;
	return sqrt(result.x*result.x + result.z*result.z);
}
void Terrain::UpdateBrush()
{
	m_pSelectedVerts.clear();
	m_SelectedDist.clear();
	m_fAvgHeight = 0.0f;

	auto DSX = GCI->GetDeferredR();

	// Update Brush position
	m_bValidBrush = GCI->GetPicker()->PickingPos(&m_pMesh, &m_vBrushPos);
	if( !m_bValidBrush || !m_pBrushRadius )
	{
		DSX->m_pFX->SetBool("f_UseBrush", false);	// Turn off brush when point off of the terrain
		DSX->m_pFX->CommitChanges();
		return;
	}
	DSX->m_pFX->SetValue("g_sBrushPos", m_vBrushPos, sizeof(D3DXVECTOR3));
	DSX->m_pFX->SetFloat("g_sBrushRadius", *m_pBrushRadius);
	DSX->m_pFX->SetBool("f_UseBrush", true);
	DSX->m_pFX->CommitChanges();
	
	// Calculate the vert double array indices
	int indexC = (m_vBrushPos.x + m_vSize.x*0.5f) / m_vSpace.x;
	int indexR = (m_vBrushPos.z + m_vSize.z*0.5f) / m_vSpace.y; 
	int startC = indexC - ((*m_pBrushRadius + m_vSize.x*0.5f) / m_vSpace.x);
	int startR = indexR - ((*m_pBrushRadius + m_vSize.z*0.5f) / m_vSpace.y);
	if(startC < 0)
		startC = 0;
	if(startR < 0)
		startR = 0;

	int endC = indexC + ((*m_pBrushRadius + m_vSize.x*0.5f) / m_vSpace.x);
	int endR = indexR + ((*m_pBrushRadius + m_vSize.z*0.5f) / m_vSpace.y);
	if(endC > m_iNumCols)
		endC = m_iNumCols;
	if(endR > m_iNumRows)
		endR = m_iNumRows;

	// Check selected verts
	for(int r = startR; r < endR; r++)
		for(int c = startC; c < endC; c++)
		{
			float dist = GetDistToBrush(m_VertexList[r][c].position, m_vBrushPos);
			if((*m_pBrushRadius - dist) > 0.0f)	// Within the brush radius
			{
				// Add a pointer to the vertex and the distance from the brush
				m_pSelectedVerts.push_back(&m_VertexList[r][c]);
				m_SelectedDist.push_back(dist);
				m_fAvgHeight += m_VertexList[r][c].position.y;
			}
		}
	m_fAvgHeight /= m_pSelectedVerts.size();	// Get the average height of the verts inside brush
	
	// BackUp the Blend texture periodically
	//m_pBlendTexture.BackUp();
	//m_pCustomTexture.BackUp();
}
void Terrain::Render()
{
	// Check if the vertex buffer needs to be updated
	if(m_bNeedsUpdate)	
		UpdateBuffers();
	else if(m_bNeedsNormUpdate)
		UpdateNormals();
	auto DSX = GCI->GetDeferredR();
	auto DXI = GCI->GetDirectX();
	HRESULT hr;

	// Set Textures
	auto texList = &GCI->GetTextureManager()->TextureList;
	if( !m_pTextures[0].empty() )
		DSX->m_pFX->SetTexture("g_sMultiTex0", (*texList)[m_pTextures[0]]->texture);
	if( !m_pTextures[1].empty() )
		DSX->m_pFX->SetTexture("g_sMultiTex1", (*texList)[m_pTextures[1]]->texture);
	if( !m_pTextures[2].empty() )
		DSX->m_pFX->SetTexture("g_sMultiTex2", (*texList)[m_pTextures[2]]->texture);
	
	// Set Normal Maps
	if(!m_pNormTex[0].empty())
	{
		DSX->m_pFX->SetBool("f_UseNormalTex", true);
		DSX->m_pFX->SetTexture("g_sNormalTex", (*texList)[m_pNormTex[0]]->texture);
	}
	else
		DSX->m_pFX->SetBool("f_UseNormalTex", false);
	if(!m_pNormTex[1].empty())
	{
		DSX->m_pFX->SetBool("f_UseNormalTex2", true);
		DSX->m_pFX->SetTexture("g_sNormalTex2", (*texList)[m_pNormTex[1]]->texture);
	}
	else
		DSX->m_pFX->SetBool("f_UseNormalTex2", false);
	if(!m_pNormTex[2].empty())
	{
		DSX->m_pFX->SetBool("f_UseNormalTex3", true);
		DSX->m_pFX->SetTexture("g_sNormalTex3", (*texList)[m_pNormTex[2]]->texture);
	}
	else
		DSX->m_pFX->SetBool("f_UseNormalTex3", false);

	// Set Specular Maps
	if(!m_pSpecTex[0].empty())
	{
		DSX->m_pFX->SetBool("f_UseSpecTex", true);
		DSX->m_pFX->SetTexture("g_sSpecTex", (*texList)[m_pSpecTex[0]]->texture);
	}
	else
		DSX->m_pFX->SetBool("f_UseSpecTex", false);
	if(!m_pSpecTex[1].empty())
	{
		DSX->m_pFX->SetBool("f_UseSpecTex2", true);
		DSX->m_pFX->SetTexture("g_sSpecTex2", (*texList)[m_pSpecTex[1]]->texture);
	}
	else
		DSX->m_pFX->SetBool("f_UseSpecTex2", false);
	if(!m_pSpecTex[2].empty())
	{
		DSX->m_pFX->SetBool("f_UseSpecTex3", true);
		DSX->m_pFX->SetTexture("g_sSpecTex3", (*texList)[m_pSpecTex[2]]->texture);
	}
	else
		DSX->m_pFX->SetBool("f_UseSpecTex3", false);

	// Set shader variables
	m_pBlendTexture.PreRender();

	// If custom render texture should be rendered, then render
	if(m_bRenderCustomTex)
	{
		DSX->m_pFX->SetBool("f_RenderCustomTex", true);
		m_pCustomTexture.SetTexture("g_sCustomMap");
	}
	else
		DSX->m_pFX->SetBool("f_RenderCustomTex", false);
	
	// Set up the Material
	DSX->m_pFX->SetValue(DSX->m_hAmbientMtrl, &m_Ambient, sizeof(D3DXCOLOR));
	DSX->m_pFX->SetValue(DSX->m_hDiffuseMtrl, &m_Diffuse, sizeof(D3DXCOLOR));
	DSX->m_pFX->SetValue(DSX->m_hSpecMtrl, &m_Specular, sizeof(D3DXCOLOR));
	DSX->m_pFX->SetFloat(DSX->m_hSpecPower, m_fSpecPower);

	// Set lighting attributes
	DSX->m_pFX->SetFloat("g_sPhongExp", m_fPhongExp);
	DSX->m_pFX->SetFloat("g_sPhongCoeff", m_fPhongCoeff);
	DSX->m_pFX->SetFloat("g_sDiffuseCoeff", m_fDiffuseCoeff);
	
	DSX->m_pFX->SetMatrix("g_sWorld", &m_TransMat);
	DSX->m_pFX->SetMatrix("g_sWVP", &(m_TransMat * GCI->GetCamera()->GetViewProj()));
	DSX->m_pFX->SetMatrix("g_sView", &(m_TransMat * GCI->GetCamera()->GetView() ));
	hr = DSX->m_pFX->CommitChanges();

	// Draw Indexed Primitives
	hr = m_pMesh->DrawSubset(0);
	DSX->m_pFX->SetBool("f_IsTerrain", false);
}

// Set Textures
void Terrain::SetTexture1(std::string tex1)
{
	if(m_pTextures[0] == tex1)
		return;

	// Make sure the texture has already been loaded
	auto texList = &GCI->GetTextureManager()->TextureList;
	if(texList->find(tex1) != texList->end())
		m_pTextures[0]	= tex1;
	else
		m_pTextures[0].clear();
	SetNormalMap(0);
	SetSpecularMap(0);
}
void Terrain::SetTexture2(std::string tex2)
{
	if(m_pTextures[1] == tex2)
		return;

	// Make sure the texture has already been loaded
	auto texList = &GCI->GetTextureManager()->TextureList;
	if(texList->find(tex2) != texList->end())
		m_pTextures[1]	= tex2;
	else
		m_pTextures[1].clear();
	SetNormalMap(1);
	SetSpecularMap(1);
}
void Terrain::SetTexture3(std::string tex3)
{
	if(m_pTextures[2] == tex3)
		return;

	// Make sure the texture has already been loaded
	auto texList = &GCI->GetTextureManager()->TextureList;
	if(texList->find(tex3) != texList->end())
		m_pTextures[2]	= tex3;
	else
		m_pTextures[2].clear();
	SetNormalMap(2);
	SetSpecularMap(2);
}

int Terrain::GetNumVerts()
{
	return m_VertexList.size();
}
int Terrain::GetNumIndices()
{
	return m_IndexList.size();
}
void Terrain::GetVerts(D3DXVECTOR3 verts[])
{
	for(int r = 0; r < m_iNumRows; r++)
		for(int c = 0; c < m_iNumCols; c++)
			verts[ (r*m_iNumCols) + c ] = m_VertexList[r][c].position;
}
void Terrain::GetIndices(WORD indices[])
{
	for(int i = 0; i < m_IndexList.size(); i++)
		indices[i] = m_IndexList[i];
}
void Terrain::GetSize(D3DXVECTOR3& size)
{
	size = m_vSize;
}
void Terrain::GetNumRowsCols(int& rows, int& cols)
{
	rows = m_iNumRows;
	cols = m_iNumCols;
}
void Terrain::GetTextureName(int tex, std::string& texName)
{
	if(tex < 3 && tex >= 0)
		texName = m_pTextures[tex];
}
char* Terrain::GetFileName()
{
	return &m_cFileName[0];
}

void Terrain::Load(char* path, char* fileName)
{
	std::string name = fileName;	// Set file name
	m_cFileName = path;
	m_cFileName += name.substr(0, name.find(".")) + "/";
	m_cFileName += name;
	if(m_cFileName.find(".map") == std::string::npos)	// Check if ".obj" is already on the name
		m_cFileName += ".map";

	// Read .obj file
	std::ifstream in(m_cFileName.c_str());
	if(!in.is_open())
	{
		//MessageBox(0, "Could not open file.", "Error", MB_OK);
		return;
	}

	if(!m_VertexList.empty())
		Clear();	// Clear the old data
	char buf[256];
	int n = 0;
	int v = 0;
	int t = 0;
	int f = 0;
	int count = 0;
	D3DXVECTOR3 scratchV;
	D3DXVECTOR2 scratchUV;
	int vIndex[3];	// For faces
	int tIndex[3];
	int nIndex[3];
	std::vector<VertexBlendEx> tempList;
	std::map<int, int> tempMap;
	// Read every line to coord
	while(!in.eof())
	{
		in.getline(buf, 256);
		if(buf[0] == '#')	// Skip over comments
		{
			if(buf[1] == 's')	// Load the size of the terrain
			{
				sscanf_s(&buf[0], "#s %f %f %f", &m_vSize.x, &m_vSize.y, &m_vSize.z); 
			}
			else if(buf[1] == 'r' && buf[2] == 'c')	// Load the Number of rows and columns
			{
				sscanf_s(&buf[0], "#rc %i %i", &m_iNumRows, &m_iNumCols);
				m_VertexList.resize(m_iNumRows, m_iNumCols);
			}
			if(buf[1] == 't')
			{
				char name[256];
				if(buf[2] == '1')	// Load Texture 1 Name
				{
					sscanf_s(&buf[0], "#t1 %s", &name[0], 256);
					m_pTextures[0] = name;
				}
				else if(buf[2] == '2')	// Load Texture 2 Name
				{
					sscanf_s(&buf[0], "#t2 %s", &name[0], 256);
					m_pTextures[1] = name;
				}
				else if(buf[2] == '3')	// Load Texture 3 name
				{
					sscanf_s(&buf[0], "#t3 %s", &name[0], 256);
					m_pTextures[2] = name;
				}
			}
			else
				continue;
		}
		else if(buf[0] == 'v' && buf[1] == ' ')	// Vector
		{
			tempList.push_back( VertexBlendEx() );	// Add Vertex
			sscanf_s(&buf[0], "v %f %f %f", &scratchV.x, &scratchV.y, &scratchV.z);
			//tempList[v++].position = scratchV - m_vSize*0.5f;	// Set Vertex Position
			tempList[v++].position = scratchV;	// Set Vertex Position
		}
		else if(buf[0] == 'v' && buf[1] == 'n')	// Normal
		{
			sscanf_s(&buf[0], "vn %f %f %f", &scratchV.x, &scratchV.y, &scratchV.z);
			tempList[n++].normal = scratchV;	// Set Vertex Normal
		}
		else if(buf[0] == 'v' && buf[1] == 't') // Texture coord
		{
			sscanf_s(&buf[0], "vt %f %f", &scratchUV.x, &scratchUV.y);
			tempList[t++].texturePos = scratchUV;	// Set Vertex Texture Coord
		}
		else if(buf[0] == 'f')	// Face
		{
			sscanf_s(&buf[0], "f %d/%d/%d %d/%d/%d %d/%d/%d", &vIndex[0], &tIndex[0], &nIndex[0], 
															  &vIndex[1], &tIndex[1], &nIndex[1], 
															  &vIndex[2], &tIndex[2], &nIndex[2]);
			
			for(int i = 0; i < 3; i++)
			{
				// Check if the vertex has already been added
				auto tempV = tempMap.find(vIndex[i]);
				if(tempV == tempMap.end())	
				{
					VertexBlendEx temp;
					temp.position = tempList[vIndex[i]].position;
					temp.texturePos = tempList[tIndex[i]].texturePos;
					temp.normal = tempList[nIndex[i]].normal;

					// Add new vertex to list
					tempMap.insert(std::pair<int, int>(vIndex[i], count));
					int r = count / m_iNumCols;
					int c = count % m_iNumCols;
					m_VertexList[r][c] = (temp);
					count++;
				}
				m_IndexList.push_back( tempMap[vIndex[i]] );	// Store the index for the vertex list
			}
		}
	}
	// Clean up 
	tempList.clear();
	tempMap.clear();

	// Build the Mesh
	m_vSpace.x = m_vSize.x / m_iNumCols;
	m_vSpace.y = m_vSize.z / m_iNumRows;
	m_iNumTris = m_IndexList.size()/3;
	BuildMesh();

	// Make sure paint textures are loaded
	auto texList = GCI->GetTextureManager()->TextureList;
	for(int i = 0; i < 3; i++)
	{
		if(texList.find(m_pTextures[i]) == texList.end())
			GCI->GetTextureManager()->LoadTexture(m_pTextures[i]);
		SetNormalMap(i);
		SetSpecularMap(i);
	}

	// Get properties of the blended texture
	std::string texName = m_cFileName;
	texName = texName.substr(0, texName.find("."));
	m_pBlendTexture.Load(texName, m_vSize);

	// Get properties of the custom texture
	texName += "_Custom";
	m_pCustomTexture.Load(texName, m_vSize);
	
	// Get tex scale
	D3DXIMAGE_INFO info;
	texName += ".bmp";
	HRESULT hr = D3DXGetImageInfoFromFileA(texName.c_str(), &info);
	Vector2 texSize;
	texSize.x = info.Width;
	texSize.y = info.Height;
	m_fTexScale = info.Width / m_vSize.x;

	//m_pCustomTexture.Init(m_vSize, m_fTexScale, D3DXCOLOR(0, 0, 0, 0));
}
void Terrain::Save(char* path, char* fileName)
{
	std::string name = fileName;
	m_cFileName = path;	// Set file name
	m_cFileName += name.substr(0, name.find("."));
	// Create folder for level if needed.
	CreateDirectory(m_cFileName.c_str(), NULL);
	m_cFileName += "/" + name;
	if(m_cFileName.find(".map") == std::string::npos)	// Check if ".obj" is already on the name
		m_cFileName += ".map";

	// Update the local buffer
	VertexBlendEx* verts = 0;
	int size = m_VertexList.size()*sizeof(VertexBlendEx);	// Get size of buffer in bytes
	HRESULT hr = m_pMesh->LockVertexBuffer(0,(void**)&verts);
	int i = 0;
	for(int r = 0; r < m_iNumRows; r++)
		for(int c = 0; c < m_iNumCols; c++)
		{
			m_VertexList[r][c].position   = verts[i].position;
			m_VertexList[r][c].normal     = verts[i].normal;
			m_VertexList[r][c].texturePos = verts[i].texturePos;
			i++;
		}
	hr = m_pMesh->UnlockVertexBuffer();

	FILE* out;
	out = fopen(m_cFileName.c_str(), "w");

	// Output size of Terrain
	fprintf(out, "#s %f %f %f\n", m_vSize.x, m_vSize.y, m_vSize.z);
	fprintf(out, "#rc %i %i\n", m_iNumRows, m_iNumCols);

	// Output texture names
	fprintf(out, "#t1 %s\n", m_pTextures[0].c_str());
	fprintf(out, "#t2 %s\n", m_pTextures[1].c_str());
	fprintf(out, "#t3 %s\n", m_pTextures[2].c_str());

	// Outuput Vector positions
	for(int r = 0; r < m_iNumRows; r++)
		for(int c = 0; c < m_iNumCols; c++)
			//fprintf(out, "v %f %f %f\n", m_VertexList[i].position.x + (m_vSize.x*0.5f), m_VertexList[i].position.y + (m_vSize.y*0.5f), m_VertexList[i].position.z + (m_vSize.z*0.5f));
			fprintf(out, "v %f %f %f\n", m_VertexList[r][c].position.x, m_VertexList[r][c].position.y, m_VertexList[r][c].position.z);
	fprintf(out, "\n");

	// Output Normals
	for(int r = 0; r < m_iNumRows; r++)
		for(int c = 0; c < m_iNumCols; c++)
			fprintf(out, "vn %f %f %f\n", m_VertexList[r][c].normal.x, m_VertexList[r][c].normal.y, m_VertexList[r][c].normal.z);
	fprintf(out, "\n");
	
	// Output Texture Positions
	for(int r = 0; r < m_iNumRows; r++)
		for(int c = 0; c < m_iNumCols; c++)
			fprintf(out, "vt %f %f\n", m_VertexList[r][c].texturePos.x, m_VertexList[r][c].texturePos.y);
	fprintf(out, "\n");

	// Output face indices
	for(int i = 0; i < m_IndexList.size(); i+=3)
		fprintf(out, "f %d/%d/%d %d/%d/%d %d/%d/%d\n", 
				m_IndexList[i], m_IndexList[i], m_IndexList[i],
				m_IndexList[i+1], m_IndexList[i+1], m_IndexList[i+1],
				m_IndexList[i+2], m_IndexList[i+2], m_IndexList[i+2]);

	// Close the file
	fclose(out);

	// Save the Terrain blend texture
	std::string texName = std::string(m_cFileName);
	texName = texName.substr(0, texName.find("."));
	m_pBlendTexture.Save(texName);

	// Save the Custom Terrain texture
	texName += "_Custom";
	m_pCustomTexture.Save(texName);
}

D3DXCOLOR Terrain::GetColorAt(Vector3 worldPos)
{
	return m_pCustomTexture.GetColorAt(worldPos);
}

void Terrain::SetCustomRenderTex(bool renderCustomTex)
{
	m_bRenderCustomTex = renderCustomTex; 
}

// Set Custom Maps
void Terrain::SetNormalMap(int i)
{
	// Prepare string for normal map
	std::string normMap;
	int p = m_pTextures[i].find('.');
	normMap = m_pTextures[i].substr(0, p-1);
	normMap.append("N");
	normMap += m_pTextures[i].substr(p, m_pTextures[i].size()-1);
	
	// Load texture through texture manager
	if( GCI->GetTextureManager()->LoadTexture(normMap) )
	{
		auto textureList = GCI->GetTextureManager()->TextureList;
		textureList[normMap]->texture->AddRef();
		m_pNormTex[i] = normMap;
	}
	else
		m_pNormTex[i].clear();
}
void Terrain::SetSpecularMap(int i)
{
	// Prepare string for specular map
	std::string specMap;
	int p = m_pTextures[i].find('.');
	specMap = m_pTextures[i].substr(0, p-1);
	specMap.append("S");
	specMap += m_pTextures[i].substr(p, m_pTextures[i].size()-1);
	
	// Load texture through texture manager
	if( GCI->GetTextureManager()->LoadTexture(specMap) )
	{
		auto textureList = GCI->GetTextureManager()->TextureList;
		textureList[specMap]->texture->AddRef();
		m_pSpecTex[i] = specMap;
	}
	else
		m_pSpecTex[i].clear();
}