#pragma once

#include "../Pipeline/QuadPicking.h"
#include <vector>

class TerrainTexture
{
	// Textures
	std::string			m_pTextures[3];
	IDirect3DTexture9*	m_pBlendTexture;
	D3DXVECTOR2			m_vBlendTexSize;
	float				m_fBlendTexScale;
	float				m_fPitch;

	// Blend Texture Back-Up
	bool						m_bNeedsBackUp;
	int							m_iLastIndex;
	std::vector<unsigned int>	m_pBlendTexColor;	// Backup the color data
	std::vector<int>			m_pUpdateList;		// List of index's to update
	void						BackUpBlendTex();

	// Convert terrain coord
	D3DXVECTOR3				m_vTerrainSize;
	D3DXVECTOR2				ConvertToTexCoord(D3DXVECTOR3 worldCoord);
		
public:
	TerrainTexture();
	~TerrainTexture();
	void	PreRestartDevice();
	void	PostRestartDevice();
	void	Clear();

	void	Init(Vector3 terrainSize, int scale, D3DXCOLOR defaultColor);	// 1 scale is one pixel per quad
	void	BackUp();		// Backs up the texture, if needed
	void	PreRender();
	void	SetTexture(std::string variable);	// Set the texture in the shader
	
	void	Update(float dt);
	void	Load(std::string texName, Vector3 terrainSize);
	void	Save(std::string texName);

	// Terrain Texture Painting
	void	PaintTex1(float increment, float radius, Vector3 position);
	void	PaintTex2(float increment, float radius, Vector3 position);
	void	PaintTex3(float increment, float radius, Vector3 position);

	D3DXCOLOR GetColorAt(Vector3 worldCoord);
};