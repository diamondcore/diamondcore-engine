#pragma once

#include "VertexDecl.h"

typedef VertexBlendEx		VertexType;	// Vertex Decleration type
class BoundingVolume
{
	D3DXVECTOR3	m_Position;

	// Bounding Box
	ID3DXMesh*	m_Box;
	D3DXVECTOR3	m_MinPoint;
	D3DXVECTOR3	m_MaxPoint;
	D3DXVECTOR3	m_Extent;
	D3DXMATRIX	m_Offset;
	bool		m_UseBox;

	// Bounding Sphere
	ID3DXMesh*	m_Sphere;
	float		m_Radius;
	bool		m_UseSphere;	

public:
	BoundingVolume();
	~BoundingVolume();

	void	Render();	// Optional

	D3DXVECTOR3	GetMin();
	D3DXVECTOR3	GetMax();
	D3DXVECTOR3	GetCenter();
	D3DXVECTOR3	GetExtent();
	D3DXVECTOR3 GetHalfExtent();
	D3DXMATRIX	GetOffset();
	void		Scale(float scale);

	ID3DXMesh**	GetBoundingBox(ID3DXMesh** mesh);
	ID3DXMesh** GetBoundingSphere(ID3DXMesh** mesh);
};