#include "Camera.h"
#include "../MacroTools.h"
#include "../GraphicsCore.h"

namespace WTW
{
	/********************************************
					Camera
	*********************************************/
	Camera::Camera()
		:m_fSpeed(10.0f),
		 m_bCustomProj(0)
	{
		m_ViewMat	= new D3DXMATRIX();
		m_pNeedsDeleting = true;
		m_PosVec	= D3DXVECTOR3(0.0f, 10.0f, -30.0f);
		m_pPosition = &m_PosVec;
		m_RightVec	= D3DXVECTOR3(1.0f, 0.0f, 0.0f);
		m_UpVec		= D3DXVECTOR3(0.0f, 1.0f, 0.0f);
		m_LookVec	= D3DXVECTOR3(0.0f, 0.0f, 1.0f);
		m_fPitch = 0.0f;

		// Setup Attributes
		m_Position.ReadWrite(this, &Camera::GetPos, &Camera::SetPos);
		m_LookAt.ReadWrite(this, &Camera::GetLook, &Camera::SetLook);

		// Build Projection Matrix
		PostRestartDevice();

		// Initialize view matrix
		GetView();
	}
	Camera::~Camera()
	{
		if(m_pNeedsDeleting)
			SAFE_DELETE(m_ViewMat);
	}

	void Camera::PreRestartDevice()
	{}
	void Camera::PostRestartDevice()
	{
		// Rebuild Projection Matrix	
		if(!m_bCustomProj)
			SetLens( D3DX_PI*0.25f);
	}

	void Camera::Update(float dt, bool freeMouseLook)
	{
		if(!m_ViewMat)
		{
			// If for some reason the view gets deleted. then recreate default.
			m_ViewMat = new D3DXMATRIX();
			m_pNeedsDeleting = true;
			GetView();
		}

		// Build view Frustum
		m_ViewProjMat = *m_ViewMat * m_ProjMat;
		BuildFrustum();
	}
	void Camera::SetPosByDir(D3DXVECTOR3 dir, float dt)
	{
		if(!dir.x && !dir.y && !dir.z)
			return;

		// Update Camera Movement
		D3DXVECTOR3 dirUp = m_UpVec*dir.y;
		D3DXVECTOR3 dirRight = m_RightVec*dir.x;
		D3DXVECTOR3 dirLook = m_LookVec*dir.z;

		dir = dirUp + dirRight + dirLook;
		D3DXVec3Normalize(&dir, &dir);
		m_PosVec += dir* m_fSpeed * dt;	// Apply directional movement
	}
	void Camera::SetView(float pitch, float yAxis)
	{
		m_fPitch += pitch;
		if (m_fPitch >= 1.7f || 
			m_fPitch <= -1.7f)
		{
			m_fPitch -= pitch;
			pitch = 0.0f;
		}

		// Apply Look Up and Down Rotation
		D3DXMATRIX Rot;
		D3DXMatrixRotationAxis(&Rot, &m_RightVec, pitch);
		D3DXVec3TransformCoord(&m_LookVec, &m_LookVec, &Rot);
		D3DXVec3TransformCoord(&m_UpVec, &m_UpVec, &Rot);

		// Rotate around the yAxis
		D3DXMatrixRotationY(&Rot, yAxis);
		D3DXVec3TransformCoord(&m_RightVec, &m_RightVec, &Rot);
		D3DXVec3TransformCoord(&m_UpVec, &m_UpVec, &Rot);
		D3DXVec3TransformCoord(&m_LookVec, &m_LookVec, &Rot);
	}
	void Camera::SetView(D3DXMATRIX* view)
	{
		if(m_pNeedsDeleting)	// If it is already created locally then delete it and set the pointer
		{
			SAFE_DELETE(m_ViewMat);	// Prevent someone from messing stuff up by going back and forth between setters
			m_pNeedsDeleting = false;
		}
		m_ViewMat = view;
	}
	void Camera::SetView(D3DXMATRIX view)
	{
		if(!m_pNeedsDeleting)	// Store matrix localy
		{
			m_ViewMat = new D3DXMATRIX();
			m_pNeedsDeleting = true;	// Set that it needs to be deleted
		}
		*m_ViewMat = view;
	}

	void Camera::SetLens(float fov)
	{
		// Build Projection 
		m_bCustomProj = 0;	// Set to automated projection (syncs with window)
		D3DXMatrixPerspectiveFovLH(&m_ProjMat, fov, 
			GCI->GetDirectX()->GetScreenWidth()/GCI->GetDirectX()->GetScreenHeight(), SCREEN_NEAR, SCREEN_FAR);
	}
	void Camera::SetProj(float fov, float aspect, float nearF, float farF)
	{
		m_bCustomProj = 1;	// Set that it's using custom projection
		D3DXMatrixPerspectiveFovLH(&m_ProjMat, fov, aspect, nearF, farF);
	}
	void Camera::SetSpeed(float speed)
	{	m_fSpeed = speed;	}

	// Position
	void Camera::SetPos(D3DXVECTOR3& pos)
	{	m_PosVec = pos;	}
	void Camera::SetPos(D3DXVECTOR3* pos)
	{	m_pPosition = pos;	}

	D3DXVECTOR3& Camera::GetPos()
	{	
		return *m_pPosition;
		//return m_PosVec = Vector3((*m_ViewMat)(3,0), (*m_ViewMat)(3,1), (*m_ViewMat)(3,2));
		//return m_PosVec;	
	}

	// Look At
	void Camera::SetLook(D3DXVECTOR3& look)
	{	m_LookVec = look;	}
	D3DXVECTOR3& Camera::GetLook()
	{	
		return Vector3((*m_ViewMat)(0,2), (*m_ViewMat)(1,2), (*m_ViewMat)(2,2));
		//return m_LookVec;	
	}

	// View
	const D3DXMATRIX& Camera::GetView()
	{
		if(!m_pNeedsDeleting)
			return *m_ViewMat;

		// Calculate Z-Axis
		D3DXVec3Normalize(&m_LookVec, &m_LookVec);		// Find the LookAt direction

		// Calculate X-Axis
		D3DXVec3Cross(&m_RightVec, &m_UpVec, &m_LookVec);	// Find orthogonal vector of Up and LookAt
		D3DXVec3Normalize(&m_RightVec, &m_RightVec);		// Find the Right direction

		// Calculate Y-Axis
		D3DXVec3Cross(&m_UpVec, &m_LookVec, &m_RightVec);	// Find orthogonal vector of LookAt and Right
		D3DXVec3Normalize(&m_UpVec, &m_UpVec);				// Find the Up direction

		// Recalculate camera's position
		float x = -D3DXVec3Dot(&m_PosVec, &m_RightVec);
		float y = -D3DXVec3Dot(&m_PosVec, &m_UpVec);
		float z = -D3DXVec3Dot(&m_PosVec, &m_LookVec);

		if (!m_pNeedsDeleting)
			return *m_ViewMat; 

		// Set up Camera View Matrix
		(*m_ViewMat)(0,0) = m_RightVec.x;	(*m_ViewMat)(0,1) = m_UpVec.x;	(*m_ViewMat)(0,2) = m_LookVec.x;	(*m_ViewMat)(0,3) = 0.0f;
		(*m_ViewMat)(1,0) = m_RightVec.y;	(*m_ViewMat)(1,1) = m_UpVec.y;	(*m_ViewMat)(1,2) = m_LookVec.y;	(*m_ViewMat)(1,3) = 0.0f;
		(*m_ViewMat)(2,0) = m_RightVec.z;	(*m_ViewMat)(2,1) = m_UpVec.z;	(*m_ViewMat)(2,2) = m_LookVec.z;	(*m_ViewMat)(2,3) = 0.0f;
		(*m_ViewMat)(3,0) = x;				(*m_ViewMat)(3,1) = y;			(*m_ViewMat)(3,2) = z;				(*m_ViewMat)(3,3) = 1.0f;

		return  *m_ViewMat;
	}

	// Projeciton
	const D3DXMATRIX& Camera::GetProj()
	{	return m_ProjMat;	}

	// View Projection
	const D3DXMATRIX& Camera::GetViewProj()
	{	return m_ViewProjMat;	}
	
	bool Camera::IsVisible(BoundingVolume& box, D3DXMATRIX transform)
	{
		// Transform center of the box
		D3DXVECTOR3 C = box.GetCenter();
		D3DXVec3TransformCoord(&C, &C, &transform);

		// Transform box extent
		D3DXMATRIX extM;
		D3DXMatrixIdentity(&extM);
		extM(0,0) = fabsf(transform(0,0)); extM(0,1) = fabsf(transform(0,1)); extM(0,2) = fabsf(transform(0,2));
		extM(1,0) = fabsf(transform(1,0)); extM(1,1) = fabsf(transform(1,1)); extM(1,2) = fabsf(transform(1,2));
		extM(2,0) = fabsf(transform(2,0)); extM(2,1) = fabsf(transform(2,1)); extM(2,2) = fabsf(transform(2,2));
		D3DXVECTOR3 E = box.GetExtent();
		D3DXVec3TransformNormal(&E, &E, &extM);

		// Convert to AABB representation
		D3DXVECTOR3 min = C - E;
		D3DXVECTOR3	max = C + E;

		D3DXVECTOR3 P, Q;
		for(int i = 0; i < 6; i++)
		{
			// Make P & Q point in the same direction as the plane normal
			for(int j = 0; j < 3; j++)
				if(m_Frustum[i][j] >= 0.0f)
				{
					P[j] = min[j];
					Q[j] = max[j];
				}
				else
				{
					P[j] = max[j];
					Q[j] = min[j];
				}

			// Test if outside the frustum
			if(D3DXPlaneDotCoord(&m_Frustum[i], &Q) < 0.0f)
				return false;
		}
		return true;
	}
	void Camera::BuildFrustum()
	{
		D3DXVECTOR4 col0(m_ViewProjMat(0,0), m_ViewProjMat(1,0), m_ViewProjMat(2,0), m_ViewProjMat(3,0));
		D3DXVECTOR4 col1(m_ViewProjMat(0,1), m_ViewProjMat(1,1), m_ViewProjMat(2,1), m_ViewProjMat(3,1));
		D3DXVECTOR4 col2(m_ViewProjMat(0,2), m_ViewProjMat(1,2), m_ViewProjMat(2,2), m_ViewProjMat(3,2));
		D3DXVECTOR4 col3(m_ViewProjMat(0,3), m_ViewProjMat(1,3), m_ViewProjMat(2,3), m_ViewProjMat(3,3));

		// Face planes inwards
		m_Frustum[0] = (D3DXPLANE)(col2);			// Near
		m_Frustum[1] = (D3DXPLANE)(col3 - col2);	// Far
		m_Frustum[2] = (D3DXPLANE)(col3 + col0);	// Left
		m_Frustum[3] = (D3DXPLANE)(col3 - col0);	// Right
		m_Frustum[4] = (D3DXPLANE)(col3 - col1);	// Top
		m_Frustum[5] = (D3DXPLANE)(col3 + col1);	// Bottom

		for(int i = 0; i < 6; i++)
			D3DXPlaneNormalize(&m_Frustum[i], &m_Frustum[i]);
	}
}