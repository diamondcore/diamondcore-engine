#pragma once

#include <vector>
#include "BoundingVolume.h"
#include <memory>

/***************************************
			  Mesh Texture
****************************************/
class MeshTexture	
{
private:
	std::string			m_Path;
	IDirect3DTexture9*	m_pTexture;
	IDirect3DTexture9*	m_pNormTex;	// Normal Map
	IDirect3DTexture9*	m_pSpecTex;	// Specular Map

	// Material Attributes
	D3DXCOLOR	m_Ambient;
	D3DXCOLOR	m_Diffuse;
	D3DXCOLOR	m_Specular;
	float		m_fSpecPower;

	// Lighting Attributes
	float		m_fDiffuseCoeff;
	float		m_fPhongCoeff;
	float		m_fPhongExp;

	// Set Custom Maps
	void	SetNormalMap();
	void	SetSpecularMap();

public:
	inline	IDirect3DTexture9* GetTexture();
	MeshTexture();
	~MeshTexture();
	void	PreRestartDevice();
	void	PostRestartDevice();
	
	void	Initialize(std::string path);
	void	SetMtrl(D3DXCOLOR ambient, D3DXCOLOR diffuse, 
					D3DXCOLOR spec, float specPower);
	void	SetLightAttributes(float diffuseCoeff, float phongCoeff, float phongExp);
	std::string GetPath(){ return m_Path;	}

	void	PreRender();	// Prepares the shader
	void	PreRender(std::string texture);	// Prepares the shader with substitute texture
};
typedef	std::shared_ptr<MeshTexture>	MeshTexturePtr;	// Smart Pointer


/***************************************
			  Scene Mesh
****************************************/
class SceneMesh
{
private:
	std::string		m_Path;

	ID3DXMesh*		m_Mesh;
	ID3DXBuffer*	m_pAdjBuffer;
	ID3DXBuffer*	m_pMtrlBuffer;
	ID3DXBuffer*	m_pFXBuffer;
	DWORD			m_NumMtrls;

	BoundingVolume	m_BoundingVolume;

	std::vector<MeshTexturePtr>	m_TextureList;

	void	ConfigureVertexDecl();
	void	LoadAttributes();
	
public:
	SceneMesh();
	~SceneMesh();
	void	PreRestartDevice();
	void	PostRestartDevice();

	bool	Load(std::string path);
	void	Unload();

	// Set the material for the main texture
	void	SetMtrl(D3DXCOLOR ambient, D3DXCOLOR diffuse, 
					D3DXCOLOR spec, float specPower);
	void	SetTextue(std::string texName);
	void	SetLightAttributes(float diffuseCoeff, float phongCoeff, float phongExp);

	// Render with default mesh texture
	void	Render();
	
	// Render with substitue the texture
	void	PreRender(std::string texture);
	void	RawRender();	

	BoundingVolume*	GetBV();
	inline ID3DXMesh** GetMesh() { return &m_Mesh;	}
	std::string GetPath() { return m_Path; }
	bool	operator== (const std::string& fileName);
};
typedef std::shared_ptr<SceneMesh>	SceneMeshPtr;	// Smart Pointer