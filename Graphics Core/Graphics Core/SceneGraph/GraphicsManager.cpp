#include "GraphicsManager.h"
#include "../GraphicsCore.h"

GraphicsManager::GraphicsManager()
	:m_bBeginRender(0)
{
	m_CheckPicking.Init();
}
GraphicsManager::~GraphicsManager()
{
	// Clear all lists
	m_CheckPicking.Shutdown();
	m_MeshLists.clear();
	m_TextureLists.clear();
	m_AnimMeshLists.clear();
	m_FontLists.clear();
	m_BillboardLists.clear();
}
void GraphicsManager::ClearScene()
{
	// Clear all lists
	m_MeshLists.clear();
	m_TextureLists.clear();
	m_AnimMeshLists.clear();
	m_FontLists.clear();
	m_BillboardLists.clear();
	GCI->GetParticleManager()->Clear();
	GCI->GetElectricityManager()->Clean();
	GCI->GetDecalManager()->Clean();
	GCI->GetDistortionManager()->ClearScene();
	GCI->GetDeferredR()->RemoveAllLights();
}

void GraphicsManager::CreateMesh(int id, std::string meshName, bool shadowMap)
{
	if(m_MeshLists.find(meshName) == m_MeshLists.end())	// Check if mesh list was not created yet
	{
		if(!GCI->GetMeshManager()->Load(meshName))	// Load the mesh
			return;
		// Create mesh list and add to the map
		m_MeshLists.insert(std::pair<std::string, std::shared_ptr<MeshList>>
			(meshName, std::shared_ptr<MeshList>(new MeshList(meshName) )));
	}
	// Add instance to list
	m_MeshLists[meshName]->CreateObject(id, shadowMap);
}
void GraphicsManager::CreateTexture(int id, std::string texName)
{
	// Make sure mesh list has been created
	if(m_TextureLists.find(texName) == m_TextureLists.end())
	{
		if(!GCI->GetTextureManager()->LoadTexture(texName))	// Load the texture
			return;
		// Create texture list and add to the map
		m_TextureLists.insert(std::pair<std::string, std::shared_ptr<TextureList>>
			(texName, std::shared_ptr<TextureList>(new TextureList(texName))));
	}
	// Add instance to the list
	m_TextureLists[texName]->CreateObject(id);
}
void GraphicsManager::CreateTexture(int id, std::string texName, D3DXCOLOR alpha)
{
	// Make sure mesh list has been created
	if(m_TextureLists.find(texName) == m_TextureLists.end())
	{
		if(!GCI->GetTextureManager()->LoadTexture(texName, alpha))	// Load the texture
			return;
		// Create texture list and add to the map
		m_TextureLists.insert(std::pair<std::string, std::shared_ptr<TextureList>>
			(texName, std::shared_ptr<TextureList>(new TextureList(texName))));
	}
	// Add instance to the list
	m_TextureLists[texName]->CreateObject(id);
}
void GraphicsManager::CreateAnimMesh(int id, std::string animMeshName, bool shadowMap)
{
	// Make sure mesh list has been created
	if(m_AnimMeshLists.find(animMeshName) == m_AnimMeshLists.end())
	{
		if(!GCI->GetAnimationManager()->LoadAnimatedMesh(animMeshName))	// Load the animated mesh
			return;
		// Create animated mesh list and add to the map
		m_AnimMeshLists.insert(std::pair<std::string, std::shared_ptr<AnimMeshList>>
			(animMeshName, std::shared_ptr<AnimMeshList>(new AnimMeshList(animMeshName))));
	}
	// Add instance to the list
	m_AnimMeshLists[animMeshName]->CreateObject(id, shadowMap);
}
void GraphicsManager::CreateMesh(int id, std::string meshName, std::string texName, bool shadowMap)
{
	if(m_MeshLists.find(meshName) == m_MeshLists.end())	// Check if mesh list was not created yet
	{
		if(!GCI->GetMeshManager()->Load(meshName))	// Load the mesh
			return;
		// Create mesh list and add to the map
		m_MeshLists.insert(std::pair<std::string, std::shared_ptr<MeshList>>
			(meshName, std::shared_ptr<MeshList>(new MeshList(meshName) )));
	}	
	// Add instance to list
	m_MeshLists[meshName]->CreateObject(id, texName, shadowMap);
}
void GraphicsManager::CreateAnimMesh(int id, std::string animMeshName, std::string texName, bool shadowMap)
{
	// Make sure mesh list has been created
	if(m_AnimMeshLists.find(animMeshName) == m_AnimMeshLists.end())
	{
		if(!GCI->GetAnimationManager()->LoadAnimatedMesh(animMeshName))	// Load the animated mesh
			return;
		// Create the animated mesh list and add to the map
		m_AnimMeshLists.insert(std::pair<std::string, std::shared_ptr<AnimMeshList>>
			(animMeshName, std::shared_ptr<AnimMeshList>(new AnimMeshList(animMeshName))));
	}
	// Add instance to the list
	m_AnimMeshLists[animMeshName]->CreateObject(id, texName, shadowMap);
}
void GraphicsManager::CreateFont(int id, std::string font, std::string text)
{
	// Make sure mesh list has been created
	if(m_FontLists.find(font) == m_FontLists.end())
	{
		if(!GCI->GetFontManager()->Load(font))	// Load the font
			return;
		// Create the font list and add to the map
		m_FontLists.insert(std::pair<std::string, std::shared_ptr<FontList>>
			(font, std::shared_ptr<FontList>(new FontList(font))));
	}
	// Add instance to the list
	m_FontLists[font]->CreateText(id, text);
}
void GraphicsManager::CreateFont(int id, std::string font, std::string text, float* pData)
{
	// Make sure mesh list has been created
	if(m_FontLists.find(font) == m_FontLists.end())
	{
		if(!GCI->GetFontManager()->Load(font))	// Load the font
			return;
		// Create the font list and add to the map
		m_FontLists.insert(std::pair<std::string, std::shared_ptr<FontList>>
			(font, std::shared_ptr<FontList>(new FontList(font))));
	}
	// Add instance to the list
	m_FontLists[font]->CreateText(id, text, pData);
}
void GraphicsManager::CreateBillboard(int id, std::string texName, bool shadowMap)
{
	// Make sure mesh list has been created
	if(m_BillboardLists.find(texName) == m_BillboardLists.end())
	{
		// Check if the texture already exist
		if(GCI->GetTextureManager()->TextureList.find(texName) == GCI->GetTextureManager()->TextureList.end())
			if(!GCI->GetTextureManager()->LoadTexture(texName))	// Load the texture
				return;
		// Create billboard list and add to the map
		m_BillboardLists.insert(std::pair<std::string, std::shared_ptr<Billboards>>
			(texName, std::shared_ptr<Billboards>(new Billboards(texName))));
	}
	// Add instance to the list
	m_BillboardLists[texName]->AddInstance(id, shadowMap);
}

void GraphicsManager::RemoveMesh(std::string meshName, int ID)
{
	auto iter = m_MeshLists.find(meshName);
	if(iter == m_MeshLists.end())	// Make sure mesh is valid
		return;
	(*iter).second->RemoveObject(ID);
	if( !(*iter).second->GetNumObjects() )
	{
		// If their are no more instances then remove mesh
		(*iter).second = 0;
		m_MeshLists.erase( iter );
	}
}
void GraphicsManager::RemoveTexture(std::string textureName, int ID)
{
	if(m_TextureLists.find(textureName) == m_TextureLists.end())	// Make sure texture is valid
		return;
	m_TextureLists[textureName]->RemoveObject(ID);
}
void GraphicsManager::RemoveAnimMesh(std::string animMeshName, int ID)
{
	if(m_AnimMeshLists.find(animMeshName) == m_AnimMeshLists.end())	// Make sure Animation mesh is valid
		return;
	m_AnimMeshLists[animMeshName]->RemoveObject(ID);
}
void GraphicsManager::RemoveFont(std::string font, int ID)
{
	if(m_FontLists.find(font) == m_FontLists.end())	// Make sure font is valid
		return;
	m_FontLists[font]->RemoveObject(ID);
}
void GraphicsManager::RemoveBillboard(std::string texName, int ID)
{
	if(m_BillboardLists.find(texName) == m_BillboardLists.end())	// Make sure texture billboard is valid
		return;
	m_BillboardLists[texName]->RemoveInstance(ID);
}

void GraphicsManager::Update(float dt, Module* sharedMem)
{
	// Update all of the lists items with the shared memory
	for(auto m = m_MeshLists.begin();
		m != m_MeshLists.end(); m++)
	{
		(*m).second->Update(sharedMem);
	}
	for(auto t = m_TextureLists.begin();
		t != m_TextureLists.end(); t++)
	{
		(*t).second->Update(sharedMem);
	}
	for(auto a = m_AnimMeshLists.begin();
		a != m_AnimMeshLists.end(); a++)
	{
		(*a).second->Update(dt, sharedMem);
	}
	for(auto f = m_FontLists.begin();
		f != m_FontLists.end(); f++)
	{
		(*f).second->Update(sharedMem);
	}
	for(auto b = m_BillboardLists.begin();
		b != m_BillboardLists.end(); b++)
	{
		(*b).second->Update(sharedMem);
	}

	// Update the Particle Manager
	GCI->GetParticleManager()->Update(sharedMem);
	GCI->GetDistortionManager()->Update(dt, sharedMem);
}
void GraphicsManager::RenderShadowMap()
{
	GCI->GetDeferredR()->BeginShadowMap();
			
	// Render Meshes
	for(auto m = m_MeshLists.begin();
		m != m_MeshLists.end(); m++)
		(*m).second->Render(true);

	// Begin Animation Mesh Render
	GCI->GetAnimationManager()->PreRender();
		
	// Render Animation Meshes
	for(auto a = m_AnimMeshLists.begin();
		a != m_AnimMeshLists.end(); a++)
		(*a).second->Render(true);

	GCI->GetAnimationManager()->PostRender();
	
	// Render Billboards
	for(auto b = m_BillboardLists.begin();
		b != m_BillboardLists.end(); b++)
		(*b).second->Render(true);

	GCI->GetDeferredR()->EndShadowMap();
}
void GraphicsManager::BeginRender()
{
	// This will keep you from starting another render without presenting the previous
	if( GCI->BeginRender() && !m_bBeginRender)
	{
		m_bBeginRender = true;

		// Render Shadow Map
		if(GCI->GetDeferredR()->NeedToUpdateShadowMap())	// Checks if the sun properties have recently changed
			RenderShadowMap();

		// Begin 3D Rendering
		GCI->Begin3DRender();
		// Render Meshes
		for(auto m = m_MeshLists.begin();
			m != m_MeshLists.end(); m++)
			(*m).second->Render(false);

		// Begin Animation Mesh Render
		GCI->GetAnimationManager()->PreRender();
		// Render Animation Meshes
		for(auto a = m_AnimMeshLists.begin();
			a != m_AnimMeshLists.end(); a++)
			(*a).second->Render(false);
		GCI->GetAnimationManager()->PostRender();

		// Begin 2D Rendering
		GCI->Begin2DRender();
		// Render 2D Sprites
		for(auto t = m_TextureLists.begin();
			t != m_TextureLists.end(); t++)
			(*t).second->Render();
	}
	else if(m_bBeginRender)
		MessageBox(0, "You have to call EndRender() before you can render again.", "Error", MB_OK);

}
void GraphicsManager::EndRender()
{
	if(!m_bBeginRender)	// Make sure render has already been started
	{
		MessageBox(0, "You have to call BeginRender() before EndRender().", "Error", MB_OK);
		return;
	}

	// Begin Font Rendering
	GCI->GetDirectX()->GetSprite()->SetTransform(&GCI->GetDeferredR()->m_WorldMat);	// Reset the Sprite's transform
	for(auto f = m_FontLists.begin();
		f != m_FontLists.end(); f++)
		(*f).second->Render();

	// End all Rendering
	GCI->EndRender();

	m_bBeginRender = false;	// Reset so we can render again
}
bool GraphicsManager::HasStartedRender()
{	return m_bBeginRender;	}

void GraphicsManager::RenderAllMeshes()
{
	for(auto m = m_MeshLists.begin();
		m != m_MeshLists.end(); m++)
		(*m).second->Render(false);
}

bool GraphicsManager::RenderMesh(int SharedID, std::string MeshName)
{
	//DeferredShader* DSX = GCI->GetDeferredR();
	//MeshManager* ML = GCI->GetMeshManager();

	if(m_MeshLists.find(MeshName) == m_MeshLists.end())
		return false;

	m_MeshLists[MeshName]->Render(false);

	//ML->GetMesh(MeshName)->Render();
	return true;
}

BaseObject* GraphicsManager::GetFontText(std::string font, int ID)
{
	if(m_FontLists.find(font) == m_FontLists.end())	// Make sure font is valid
		return 0;
	return m_FontLists[font]->GetObject(ID).get();
}


// Set Color
void GraphicsManager::SetTextureColor(int id, std::string texName, D3DXCOLOR color)
{
	if(m_TextureLists.find(texName) == m_TextureLists.end())	// Make sure texture is valid
		return;
	m_TextureLists[texName]->SetColor(id, color);
}
void GraphicsManager::SetMeshColor(int id, std::string meshName, D3DXCOLOR color)
{
	if(m_MeshLists.find(meshName) == m_MeshLists.end())	// Make sure mesh is valid
		return;
	m_MeshLists[meshName]->SetColor(id, color);
}
void GraphicsManager::SetAnimMeshColor(int id, std::string animMeshName, D3DXCOLOR color)
{
	if(m_AnimMeshLists.find(animMeshName) == m_AnimMeshLists.end())	// Make sure Animation mesh is valid
		return;
	m_AnimMeshLists[animMeshName]->SetColor(id, color);
}
void GraphicsManager::SetFontColor(int id, std::string fontName, D3DXCOLOR color)
{
	if(m_FontLists.find(fontName) == m_FontLists.end())	// Make sure font is valid
		return;
	m_FontLists[fontName]->SetColor(id, color);
}
void GraphicsManager::SetBillboardColor(int id, std::string texture, D3DXCOLOR color)
{
	if(m_BillboardLists.find(texture) == m_BillboardLists.end())	// Make sure texture billboard is valid
		return;
	m_BillboardLists[texture]->SetColor(id, color);
}

// Set Scale
void GraphicsManager::SetTextureScale(int id, std::string texName, D3DXVECTOR3 scale)
{
	if(m_TextureLists.find(texName) == m_TextureLists.end())
		return;
	auto obj = m_TextureLists[texName]->GetObject(id);
	if(obj)
		obj->m_Tranform.m_Scale = scale;
}
void GraphicsManager::SetMeshScale(int id, std::string meshName, D3DXVECTOR3 scale)
{
	if(m_MeshLists.find(meshName) == m_MeshLists.end())
		return;
	auto obj = m_MeshLists[meshName]->GetObject(id);
	if(obj)
		obj->m_Tranform.m_Scale = scale;
}
void GraphicsManager::SetAnimMeshScale(int id, std::string animMeshName, D3DXVECTOR3 scale)
{
	if(m_AnimMeshLists.find(animMeshName) == m_AnimMeshLists.end())
		return;
	auto obj = m_AnimMeshLists[animMeshName]->GetObject(id);
	if(obj)
		obj->m_Tranform.m_Scale = scale;
}
void GraphicsManager::SetFontScale(int id, std::string fontName, D3DXVECTOR3 scale)
{
	if(m_FontLists.find(fontName) == m_FontLists.end())
		return;
	auto obj = m_FontLists[fontName]->GetObject(id);
	if(obj)
		obj->m_Tranform.m_Scale = scale;
}
void GraphicsManager::SetBillboardScale(int id, std::string texName, D3DXVECTOR3 scale)
{
	if(m_BillboardLists.find(texName) == m_BillboardLists.end())
		return;
	auto obj = m_BillboardLists[texName]->GetObject(id);
	if(obj)
		obj->m_Tranform.m_Scale = scale;
}

// Get Scale
void GraphicsManager::GetMeshScale(int id, std::string meshName, D3DXVECTOR3&scale)
{
	if(m_MeshLists.find(meshName) == m_MeshLists.end())
		return;
	scale = m_MeshLists[meshName]->GetObject(id)->m_Tranform.m_Scale;
}

void GraphicsManager::RenderBillboards()
{
	DirectXClass* DXI = GCI->GetDirectX();
	DeferredShader* DSX = GCI->GetDeferredR();

	// Set Shader Variables
	DSX->m_pFX->SetValue("g_sCameraPos", &GCI->GetCamera()->m_Position, sizeof(D3DXVECTOR3));

	// Set technique
	HRESULT hr = DSX->m_pFX->SetTechnique("ImposterTech");
	hr = DSX->m_pFX->CommitChanges();
	
	UINT numPasses = 0;
	DSX->m_pFX->Begin(&numPasses, 0);
	DSX->m_pFX->BeginPass(0);

	// Render Billboards
	for(auto b = m_BillboardLists.begin();
		b != m_BillboardLists.end(); b++)
		(*b).second->Render(false);

	DSX->m_pFX->EndPass();
	DSX->m_pFX->End();
}

void GraphicsManager::CheckQuadPicking(int hitIDs[], int& count)
{
	count = 0;
	// Update picking position
	m_CheckPicking.UpdateRay();
	
	// Check for picking
	for(auto b = m_BillboardLists.begin();
		b != m_BillboardLists.end(); b++)
		(*b).second->CheckQuadPicking(hitIDs, count, m_CheckPicking);
}

void GraphicsManager::GetSelectedMesh(std::string& meshName, int& id)	// Returns the mesh Name and ID selected
{
	for(auto m = m_MeshLists.begin();
		m != m_MeshLists.end(); m++)
	{
		(*m).second->RunPickingTest();
	}	
	GCI->GetPicker()->GetSelectedObj(meshName, id);
	if(id < 0)
		meshName = "";
}

