#pragma once

#include "AnimMeshList.h"
#include "../Pipeline/Billboards.h"
#include "../Pipeline/QuadPicking.h"
#include "../3D/Terrain.h"
#include "../Pipeline/DistortionMeshManager.h"
#include <map>

class GraphicsManager
{
	// Normal Resource Lists
	std::map<std::string, std::shared_ptr<MeshList>>		m_MeshLists;
	std::map<std::string, std::shared_ptr<TextureList>>		m_TextureLists;
	std::map<std::string, std::shared_ptr<AnimMeshList>>	m_AnimMeshLists;
	std::map<std::string, std::shared_ptr<FontList>>		m_FontLists;
	std::map<std::string, std::shared_ptr<Billboards>>		m_BillboardLists;
	
	// Quad Picking tool
	QuadPicking	m_CheckPicking;

	void	RenderShadowMap();
	bool	m_bBeginRender;


public:
	GraphicsManager();
	~GraphicsManager();
	void	ClearScene();

	void	Update(float dt, Module* sharedMem);

	// Rendering calls
	void	BeginRender();
	void	EndRender();
	bool	HasStartedRender();
	//Renders All Scene Meshes
	void	RenderAllMeshes();
	bool	RenderMesh(int SharedID, std::string MeshName);

	// Create normal resources
	void	CreateTexture(int id, std::string texName);
	void	CreateTexture(int id, std::string texName, D3DXCOLOR alpha);
	void	CreateFont(int id, std::string font, std::string text);
	void	CreateFont(int id, std::string font, std::string text, float* pData);
	void	CreateMesh(int id, std::string meshName, bool shadowMap);
	void	CreateDistortionMesh(int id, string meshName);
	void	CreateAnimMesh(int id, std::string animMeshName, bool shadowMap);
	void	CreateBillboard(int id, std::string texName, bool shadowMap);

	// Create overiden resources
	void	CreateMesh(int id, std::string meshName, std::string texName, bool shadowMap);
	void	CreateAnimMesh(int id, std::string animMeshName, std::string texName, bool shadowMap);

	void	RemoveFont(std::string font, int ID);
	void	RemoveMesh(std::string meshName, int ID);
	void	RemoveTexture(std::string textureName, int ID);
	void	RemoveAnimMesh(std::string animMeshName, int ID);
	void	RemoveBillboard(std::string texName, int ID);

	// Set Color
	void	SetTextureColor(int id, std::string texName, D3DXCOLOR color);
	void	SetMeshColor(int id, std::string meshName, D3DXCOLOR color);
	void	SetAnimMeshColor(int id, std::string animMeshName, D3DXCOLOR color);
	void	SetFontColor(int id, std::string fontName, D3DXCOLOR color);
	void	SetBillboardColor(int id, std::string texture, D3DXCOLOR color);

	// Set Scale
	void	SetTextureScale(int id, std::string texName, D3DXVECTOR3 scale);
	void	SetMeshScale(int id, std::string meshName, D3DXVECTOR3 scale);
	void	SetAnimMeshScale(int id, std::string animMeshName, D3DXVECTOR3 scale);
	void	SetFontScale(int id, std::string fontName, D3DXVECTOR3 scale);
	void	SetBillboardScale(int id, std::string texName, D3DXVECTOR3 scale);

	// Get Scale
	void	GetMeshScale(int id, std::string meshName, D3DXVECTOR3&scale);

	// Accessors
	BaseObject*	GetFontText(std::string font, int ID);

	void	CheckQuadPicking(int hitIDs[], int& count);
	void	GetSelectedMesh(std::string& meshName, int& id);	// Sets the mesh Name and ID selected


	// Billboards have to be rendered at a specific time in the pipline.
	// So we keep this method static so that DeferredShader can call it 
	// at the appropritate time.
	void	RenderBillboards();
};