#include "ObjectList.h"
#include "../GraphicsCore.h"

ObjectList::ObjectList(std::string resourceName)
	:m_ResourceName(resourceName)
{}
ObjectList::~ObjectList()
{
	Clear();
}

void ObjectList::Update(Module* sharedMem)
{
	for(auto i = m_Objects.begin(); 
		i != m_Objects.end(); i++)
	{
		// Update the information with the shared memory
		(*i).second->m_Tranform.m_Position = sharedMem->GetSharedResource((*i).second->m_SharedMemID)->GetPosition();
		(*i).second->m_Tranform.m_Quaternion =  sharedMem->GetSharedResource((*i).second->m_SharedMemID)->GetOrientation();
	}
}

void ObjectList::CreateObject(int id)
{
	m_Objects.insert(std::pair<int, BaseObjectPtr>(id, std::shared_ptr<BaseObject>(new BaseObject)));
	m_Objects[id]->m_bShadowMap = false;
	m_Objects[id]->m_SharedMemID = id;
}
void ObjectList::RemoveObject(int id)
{
	auto iter = m_Objects.find(id);
	if( iter == m_Objects.end() )
		return;		// Object doesn't exist

	// Remove from object list
	m_Objects[id] = 0;
	m_Objects.erase( iter );
}
void ObjectList::Clear()
{
	// Clear the lists
	m_Objects.clear();
}
BaseObjectPtr ObjectList::GetObject(int id)
{	
	if(m_Objects.find(id) == m_Objects.end())	// Make sure object is valid
		return 0;
	return m_Objects[id];	
}
void ObjectList::SetColor(int id, D3DXCOLOR color)
{
	if(m_Objects.find(id) == m_Objects.end())
		return;
	m_Objects[id]->m_Color = color;
}


void TextureList::Render()
{
	GCI->GetTextureManager()->RenderTexture(m_ResourceName, m_Objects);
}


void FontList::CreateText(int id, std::string text)
{
	m_Objects.insert(std::pair<int, BaseObjectPtr>(id, std::shared_ptr<BaseObject>(new BaseObject)));
	m_Objects[id]->m_bShadowMap = false;
	m_Objects[id]->m_SharedMemID = id;
	m_Objects[id]->m_TextureOveride = text;	// Store text to output here
	m_Objects[id]->m_pData = 0;	// No data used in this output
}
void FontList::CreateText(int id, std::string text, float* pData)
{
	m_Objects.insert(std::pair<int, BaseObjectPtr>(id, std::shared_ptr<BaseObject>(new BaseObject)));
	m_Objects[id]->m_bShadowMap = false;
	m_Objects[id]->m_SharedMemID = id;
	m_Objects[id]->m_TextureOveride = text;	// Store text to output here
	m_Objects[id]->m_pData  = pData;	// Use this data when rendering
}
void FontList::Update(Module* sharedMem)
{
	for(auto i = m_Objects.begin(); 
		i != m_Objects.end(); i++)
	{
		// Check if the Object is a Graphically owned object
		if( (*i).second->m_SharedMemID < -1 )
			continue;

		// Update the information with the shared memory
		(*i).second->m_Tranform.m_Position = sharedMem->GetSharedResource((*i).second->m_SharedMemID)->GetPosition();
		(*i).second->m_Tranform.m_Quaternion =  sharedMem->GetSharedResource((*i).second->m_SharedMemID)->GetOrientation();
	}
}
void FontList::Render()
{
	auto fMgr = GCI->GetFontManager();
	for(auto f = m_Objects.begin();
		f != m_Objects.end(); f++)
	{
		// Get the 2D position
		D3DXVECTOR3 pos = (*f).second->m_Tranform.m_Position;
		
		if((*f).second->m_pData)	// If data then create output
		{
			char buff[32];
			sprintf(buff, (*f).second->m_TextureOveride.c_str(), *(float*)((*f).second->m_pData));
			
			// Render the text to the string
			if((*f).second->m_Color.a == -1.0f)
				fMgr->Render(buff, pos.x, pos.y, m_ResourceName);
			else
				fMgr->Render(buff, pos.x, pos.y, m_ResourceName, (*f).second->m_Color);
		}
		else	// Render the text to the string
		{
			if((*f).second->m_Color.a == -1.0f)
				fMgr->Render((*f).second->m_TextureOveride, pos.x, pos.y, m_ResourceName);
			else
				fMgr->Render((*f).second->m_TextureOveride, pos.x, pos.y, m_ResourceName, (*f).second->m_Color);
		}
	}
}