#pragma once

#include "..\Westley's Lib\Transform.h"
#include <string>
#include <map>
#include <memory>

#include <MemoryManagement/Module.h>

struct BaseObject
{
	BaseObject()
		:m_SharedMemID(-1),
		 m_bShadowMap(0),
		 m_bInView(0),
		 m_Color(1.0f, 1.0f, 1.0f, -1.0f),	// This negative one means to use default loaded textures 
		 m_pData(0) {};
	~BaseObject(){};

	// Basic Data
	int				m_SharedMemID;
	bool			m_bShadowMap;
	bool			m_bInView;
	WTW::Transform	m_Tranform;
	D3DXCOLOR		m_Color;
	std::string		m_TextureOveride;	//(optional) Use for overiding textures on render
	void*			m_pData;
};
typedef std::shared_ptr<BaseObject>	BaseObjectPtr;	// Smart Pointer


class ObjectList
{
protected:
	std::string						m_ResourceName;
	std::map<int, BaseObjectPtr>	m_Objects;

public:
	ObjectList(std::string resourceName);
	virtual ~ObjectList();
	void Clear();

	virtual void Update(Module* sharedMem);
	virtual void Render() = 0;

	void CreateObject(int id);
	void RemoveObject(int id);

	void SetColor(int id, D3DXCOLOR color);
	BaseObjectPtr	GetObject(int id);
};


class TextureList	: public ObjectList
{
public:
	TextureList(std::string meshName)
		:ObjectList(meshName) {};
	~TextureList(){};

	virtual void Render();
};

class FontList	: public ObjectList
{
public:
	FontList(std::string fontName)
		:ObjectList(fontName) {};
	~FontList(){};

	void CreateText(int id, std::string text);
	void CreateText(int id, std::string text, float* pData);	// Create an output font with data
	virtual void Update(Module* sharedMem);
	virtual void Render();
};