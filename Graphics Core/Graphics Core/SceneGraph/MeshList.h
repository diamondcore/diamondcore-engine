#pragma once

#include "ObjectList.h"


class MeshList
{
protected:
	std::string															m_MeshName;
	std::map<int, BaseObjectPtr>										m_Objects;
	std::vector<BaseObjectPtr>											m_ShadowObjects;
	std::map<std::string, std::shared_ptr<std::vector<BaseObjectPtr>>>	m_ObjectTexMap;
	D3DXMATRIX m_OffsetMat;

	void	AddObject(int sharedMemID, bool shadowMap);

public:
	MeshList(std::string meshName);
	~MeshList();
	void	Clear();

	void	Update(Module* sharedMem);
	void	Render(bool shadowMap);
	
	void	CreateObject(int sharedMemID, bool shadowMap);	// Use Default texture (faster)
	void	CreateObject(int sharedMemID, std::string texture, bool shadowMap);
	void	RemoveObject(int sharedMemID);

	void	SetColor(int id, D3DXCOLOR color);
	BaseObjectPtr	GetObject(int id);
	void	RunPickingTest();
	int     GetNumObjects() { return m_Objects.size(); }
};