#include "AnimMeshList.h"
#include "../GraphicsCore.h"


AnimMeshList::AnimMeshList(std::string meshName)
	:m_MeshName(meshName)
{
	m_OffsetMat = GCI->GetAnimationManager()->GetAnimMesh(meshName)->getBV_Offset();
}
AnimMeshList::~AnimMeshList()
{
	Clear();
}
void AnimMeshList::Clear()
{
	// Remove all linking animation controllers
	for(auto i = m_Objects.begin();
		i != m_Objects.end(); i++)
		GCI->GetAnimationManager()->DeleteAnimController( (*i).second->m_SharedMemID );

	// Clear all lists
	m_ObjectTexMap.clear();
	m_ShadowObjects.clear();
	m_Objects.clear();
}
void AnimMeshList::AddObject(int sharedMemID, bool shadowMap)
{
	// Add object to the main list
	m_Objects.insert(std::pair<int, BaseObjectPtr>
		(sharedMemID, std::shared_ptr<BaseObject>(new BaseObject)));
	m_Objects[sharedMemID]->m_SharedMemID = sharedMemID;
	//m_Objects[sharedMemID]->m_Tranform.SetOffset( &m_OffsetMat );
	
	// Create Animation Controller for this instance
	GCI->GetAnimationManager()->CreateAnimController(m_MeshName, sharedMemID);

	// Set Shadow Map settings
	m_Objects[sharedMemID]->m_bShadowMap = shadowMap;
	if(shadowMap)
			m_ShadowObjects.push_back( m_Objects[sharedMemID] );
}
void AnimMeshList::CreateObject(int sharedMemID, bool shadowMap)
{
	// Create Object with default textures
	CreateObject(sharedMemID, "DEFAULT", shadowMap);
}
void AnimMeshList::CreateObject(int sharedMemID, std::string texture, bool shadowMap)
{
	// Create basic object
	AddObject(sharedMemID, shadowMap);
	
	// Check if texture map has been added yet
	if( m_ObjectTexMap.find(texture) == m_ObjectTexMap.end() )	
	{
		TextureManager* tMgr = GCI->GetTextureManager();
		bool needToAdd = true;
		// Check if the texture is loaded
		if(texture != "DEFAULT" && tMgr->TextureList.find(texture) == tMgr->TextureList.end())
			if(!tMgr->LoadTexture(texture))	// Texture not found, try loading it
			{
				texture = "DEFAULT";	// Texture not found so set to default
				// Use Default, and make sure Default list has been created
				if(m_ObjectTexMap.find(texture) != m_ObjectTexMap.end())
					needToAdd = false;	// Default list has already been added
			}

		// If texture needs to be added to the map, then add it
		if(needToAdd)
			m_ObjectTexMap.insert(std::pair<std::string, std::shared_ptr<std::vector<BaseObjectPtr>>>
				(texture, std::shared_ptr<std::vector<BaseObjectPtr>>(new std::vector<BaseObjectPtr>)));
	}
	
	// Add to texture list
	m_Objects[sharedMemID]->m_TextureOveride = texture;
	m_ObjectTexMap[texture]->push_back( m_Objects[sharedMemID] );
}
void AnimMeshList::RemoveObject(int sharedMemID)
{
	// Make sure object is in the list
	auto iter = m_Objects.find(sharedMemID);
	if( iter == m_Objects.end() )
		return;		// Object not found

	// Remove from texture list
	auto list = m_ObjectTexMap[m_Objects[sharedMemID]->m_TextureOveride];
	for(auto i = list->begin();
		i != list->end(); i++)
		if( (*i)->m_SharedMemID == sharedMemID )
		{
			(*i) = 0;
			i = list->erase( i );
			break;
		}

	// Check if we should remove texture list
	if( list->empty() )
	{
		auto listIter = m_ObjectTexMap.find( m_Objects[sharedMemID]->m_TextureOveride );
		m_ObjectTexMap.erase( listIter );	// Remove this texture list
	}

	// Remove from Shadow Map (if applicable)
	if(m_Objects[sharedMemID]->m_bShadowMap)
		for(auto i = m_ShadowObjects.begin();
			i != m_ShadowObjects.end(); i++)
			if( (*i)->m_SharedMemID == sharedMemID )
			{
				(*i) = 0;
				i = m_ShadowObjects.erase( i );
				break;
			}

	// Destroy the linking animation controller
	GCI->GetAnimationManager()->DeleteAnimController( sharedMemID );

	// Remove Object from main list
	m_Objects[sharedMemID] = 0;
	m_Objects.erase( iter );
}

void AnimMeshList::Render(bool shadowMap)
{
	auto aMgr = GCI->GetAnimationManager();
	if(shadowMap)
			aMgr->RenderAnimatedMesh(m_ShadowObjects, shadowMap);	// Use default render for all shadow objects
	else
		for(auto t = m_ObjectTexMap.begin();	// Iterate through every texture and draw
			t != m_ObjectTexMap.end(); t++)
			if( (*t).first == "DEFAULT" )
				aMgr->RenderAnimatedMesh(*(*t).second, shadowMap);	// Use default textures
			else
				aMgr->RenderAnimatedMesh(*(*t).second, (*t).first);	// Use mesh with different texture 
}

void AnimMeshList::Update(float dt, Module* sharedMem)
{
	// Update animations to these times
	auto aMgr = GCI->GetAnimationManager();
	for(auto i = m_Objects.begin();
		i != m_Objects.end(); i++)
	{
		// Update Transform
		(*i).second->m_Tranform.m_Position = sharedMem->GetSharedResource((*i).second->m_SharedMemID)->GetPosition();
		(*i).second->m_Tranform.m_Quaternion =  sharedMem->GetSharedResource((*i).second->m_SharedMemID)->GetOrientation();
	}
}

BaseObjectPtr	AnimMeshList::GetObject(int id)
{	
	if(m_Objects.find(id) == m_Objects.end())	// Make sure object is valid
		return 0;
	return m_Objects[id];	
}

void AnimMeshList::SetColor(int id, D3DXCOLOR color)
{
	if(m_Objects.find(id) == m_Objects.end())
		return;
	m_Objects[id]->m_Color = color;
}

void AnimMeshList::RunPickingTest()
{
	//std::shared_ptr<AnimatedMesh> mesh = GCI->GetAnimationManager()->GetAnimMesh(m_MeshName);
	//QuadPicking* picker = GCI->GetPicker();

	//for(auto i = m_Objects.begin();
	//	i != m_Objects.end(); i++)
	//{
	//	if((*i).second->m_bInView)
	//		picker->PickingSelect(mesh->, (*i).second->m_Tranform.GetTransform(), m_MeshName, (*i).second->m_SharedMemID);

	//}
}
