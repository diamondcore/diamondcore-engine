#include "MeshList.h"
#include "../GraphicsCore.h"

MeshList::MeshList(std::string meshName)
	:m_MeshName(meshName)
{
	m_OffsetMat = GCI->GetMeshManager()->GetMesh(m_MeshName)->GetBV()->GetOffset();
}
MeshList::~MeshList()
{
	m_MeshName.clear();
	Clear();
}
void MeshList::Clear()
{
	// Clear all lists
	m_ObjectTexMap.clear();
	m_ShadowObjects.clear();
	m_Objects.clear();
}
void MeshList::AddObject(int sharedMemID, bool shadowMap)
{
	// Add object to the main list
	m_Objects.insert(std::pair<int, BaseObjectPtr>
		(sharedMemID, std::shared_ptr<BaseObject>(new BaseObject)));
	m_Objects[sharedMemID]->m_SharedMemID = sharedMemID;
	
	// Optional to use auto calculated mesh offset
	m_Objects[sharedMemID]->m_Tranform.SetOffset( &m_OffsetMat );

	// Set Shadow Map settings
	m_Objects[sharedMemID]->m_bShadowMap = shadowMap;
	if(shadowMap)
			m_ShadowObjects.push_back( m_Objects[sharedMemID] );
}
void MeshList::CreateObject(int sharedMemID, bool shadowMap)
{
	// Create Object with default textures
	CreateObject(sharedMemID, "DEFAULT", shadowMap);
}
void MeshList::CreateObject(int sharedMemID, std::string texture, bool shadowMap)
{
	// Create basic object
	AddObject(sharedMemID, shadowMap);
	
	// Check if texture map has been added yet
	if( m_ObjectTexMap.find(texture) == m_ObjectTexMap.end() )	
	{
		TextureManager* tMgr = GCI->GetTextureManager();
		bool needToAdd = true;
		// Check if the texture is loaded
		if(texture != "DEFAULT" && tMgr->TextureList.find(texture) == tMgr->TextureList.end())
			if(!tMgr->LoadTexture(texture))	// Texture not found, try loading it
			{
				texture = "DEFAULT";	// Texture not found so set to default
				// Use Default, and make sure Default list has been created
				if(m_ObjectTexMap.find(texture) != m_ObjectTexMap.end())
					needToAdd = false;	// Default list has already been added
			}
			
		// If texture needs to be added to the map, then add it
		if(needToAdd)
			m_ObjectTexMap.insert(std::pair<std::string, std::shared_ptr<std::vector<BaseObjectPtr>>>
				(texture, std::shared_ptr<std::vector<BaseObjectPtr>>(new std::vector<BaseObjectPtr>)));
	}
	
	// Add to texture list
	m_Objects[sharedMemID]->m_TextureOveride = texture;
	m_ObjectTexMap[texture]->push_back( m_Objects[sharedMemID] );
}
void MeshList::RemoveObject(int sharedMemID)
{
	// Make sure object is in the list
	auto iter = m_Objects.find(sharedMemID);
	if( iter == m_Objects.end() )
		return;		// Object not found

	// Remove from texture list
	auto list = m_ObjectTexMap[m_Objects[sharedMemID]->m_TextureOveride];
	for(auto i = list->begin();
		i != list->end(); i++)
		if( (*i)->m_SharedMemID == sharedMemID )
		{
			(*i) = 0;
			i = list->erase( i );
			break;
		}

	// Check if we should remove texture list
	if( list->empty() )
	{
		auto listIter = m_ObjectTexMap.find( m_Objects[sharedMemID]->m_TextureOveride );
		m_ObjectTexMap.erase( listIter );	// Remove this texture list
	}

	// Remove from Shadow Map (if applicable)
	if(m_Objects[sharedMemID]->m_bShadowMap)
		for(auto i = m_ShadowObjects.begin();
			i != m_ShadowObjects.end(); i++)
			if( (*i)->m_SharedMemID == sharedMemID )
			{
				(*i) = 0;
				i = m_ShadowObjects.erase( i );
				break;
			}

	// Remove Object from main list
	m_Objects[sharedMemID] = 0;
	m_Objects.erase( iter );
}

void MeshList::Render(bool shadowMap)
{
	auto mMgr = GCI->GetMeshManager();
	if(shadowMap)
		mMgr->Render(m_MeshName, m_ShadowObjects);
	else
		for(auto t = m_ObjectTexMap.begin();	// Iterate through every texture and draw
			t != m_ObjectTexMap.end(); t++)
			if( (*t).first == "DEFAULT" )
				mMgr->Render(m_MeshName, *(*t).second);	// Use mesh with default texture
			else
				mMgr->Render(m_MeshName, (*t).first, *(*t).second);	// Use mesh with different texture 
}

void MeshList::Update(Module* sharedMem)
{
	for(auto i = m_Objects.begin(); 
		i != m_Objects.end(); i++)
	{
		// Update the information with the shared memory
		(*i).second->m_Tranform.m_Position = sharedMem->GetSharedResource((*i).second->m_SharedMemID)->GetPosition();
		(*i).second->m_Tranform.m_Quaternion =  sharedMem->GetSharedResource((*i).second->m_SharedMemID)->GetOrientation();
	}
}
BaseObjectPtr	MeshList::GetObject(int id)
{	
	if(m_Objects.find(id) == m_Objects.end())	// Make sure object is valid
		return 0;
	return m_Objects[id];	
}
void MeshList::SetColor(int id, D3DXCOLOR color)
{
	if(m_Objects.find(id) == m_Objects.end())
		return;
	m_Objects[id]->m_Color = color;
}

void MeshList::RunPickingTest()
{
	SceneMeshPtr mesh = GCI->GetMeshManager()->GetMesh(m_MeshName);
	QuadPicking* picker = GCI->GetPicker();

	for(auto i = m_Objects.begin();
		i != m_Objects.end(); i++)
	{
		if((*i).second->m_bInView)
			picker->PickingSelect((mesh->GetMesh()), (*i).second->m_Tranform.GetTransform(), m_MeshName, (*i).second->m_SharedMemID);
	}
}
