#pragma once

#include "SceneGraph/GraphicsManager.h"
#include "3D/Terrain.h"

/****************************
**			BaseApp
** Used to create the window and 
** initialize the Graphics Core.
*/
class BaseApp
{
	HINSTANCE	m_hInst;
	HWND		m_hWnd;
	std::string	m_Title;

	void InitWindow();

private:
	// Display Stat flags
	bool		m_bEnableFPS;
	bool		m_bEnableNumObjs;
	bool		m_bEnableTimeOfDay;

	// FPS Stats Timer
	float		m_fClock;
	float		m_fFPS;
	float		m_fMsPF;
	float		m_fAccumTime;
	float		m_fNumFrames;
	void FPS(float dt);
	Module*		m_pSharedMem;
	void		(*cb_Resize)();

	// Demo Terrain
	Terrain		m_Terrain;

public:
	BaseApp();
	~BaseApp();

	void Init(HINSTANCE hInst, const char* windowTitle);
	void Shutdown();

	void Update(float dt, Module* sharedMem);
	void Render();

	// Stats Output
	void DisplayFPS(bool enable);
	void DisplayNumObjs(bool enable);
	void DisplayTimeOfDay(bool enable);

	// Get Frame Stats
	float GetFPS() {return m_fFPS;}
	float GetAccumTime() {return m_fAccumTime;}
	float GetNumFrames() {return m_fNumFrames;}
	float GetMsPF() {return m_fMsPF;}

	// Graphics entity manager
	GraphicsManager*	m_pGraphicsMgr;	
	int					m_iCameraID;		// Camera SharedMemID

	// Pausing Methods
	void	SetMemMgr(Module* sharedMem);
	void	SetPause(bool pause);
	void	SetResizeCallBack(void (*funcP)());
	void	UseResizeCallBack();
};
static LRESULT CALLBACK WndProc(
	HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);