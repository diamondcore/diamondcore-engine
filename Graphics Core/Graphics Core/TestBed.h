#pragma once

#include "GraphicsCore.h"
#include "Westley's Lib\Transform.h"

/****************************
**			TestBed
** Used to simulate how the Main Core
** will use the Graphics Core.
*/
class TestBed
{
	HINSTANCE	m_hInst;
	HWND		m_hWnd;
	std::string	m_Title;

	void InitWindow();
private:
	// Timer
	float		m_fClock;
	float		m_fFPS;
	float		m_fMsPF;
	void FPS(float dt);

private:
	D3DXMATRIX	m_EntityList[5];	// Example of mesh entity list
	D3DXMATRIX	m_TextureEntity;	// Example of texture
	D3DXMATRIX	m_IdentityMat;		// Sets the Matrix back to Identity matrix for the Font
	D3DXMATRIX	m_AnimationMat[2];		// Example of animation
	bool		m_bInstanceDemo;

public:
	TestBed();
	~TestBed();

	void Init(HINSTANCE hInst);
	void Shutdown();

	void Update(float dt);
	void Render();
};
static LRESULT CALLBACK WndProc(
	HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);