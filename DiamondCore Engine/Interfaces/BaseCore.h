#pragma once
#include "../MemoryManagement/MemoryManager.h"
#include "../MemoryManagement/Module.h"
#include "../Messaging System/MessageDispatcher.h"

class MainCore;
class BaseCore
{
public:
	BaseCore(void){}
	~BaseCore(void){}

	virtual void Update(float dt) = 0;
	virtual void Initialize() = 0;
	virtual void Shutdown() = 0;
	virtual void Pause() = 0;
	virtual void ProcessMessages() = 0;
	virtual int GetCorePriority() = 0;


	// sets the message manager for the core
	void SetMessageMgr(MessageDispatcher * &pMsgMgr) { m_pMessageMgr = pMsgMgr; }

	// sets the memory module ptr for the core
	void SetMemoryModule(Module* pMem) 
	{ 
		m_pSharedMemory = pMem; 
	}

	// memory vars
	Module* m_pSharedMemory;	// Hacked as public #TODO Move back to Protected
protected:

	// Allow MainCore to  get access to the stuff it'll need to.
	friend class MainCore;
	// messaging vars
	MessageDispatcher* m_pMessageMgr;
	//std::vector<Mail*> m_sMyMessages;
	MailInbox	m_sMyMessages;

	// window handle
	HWND m_hWnd;

};

