#pragma once
#include "BaseCore.h"

enum InputMessages {
	// Create Resources
	msg_GetButtonA,
	msg_GetButtonB,
	msg_GetButtonX,
	msg_GetButtonY

};

enum ControllerButton
{
	DPAD_UP = 0x0001,
	DPAD_DOWN = 0x0002,
	DPAD_LEFT = 0x0004,
	DPAD_RIGHT = 0x0008,
	START = 0x0010,
	BACK = 0x0020,
	LEFT_STICK = 0x0040,
	RIGHT_STICK = 0x0080,
	LEFT_BUMPER = 0x0100,
	RIGHT_BUMPER = 0x0200,
	A  = 0x1000,
	B = 0x2000,
	X = 0x4000,
	Y = 0x8000,
};

class SharedInputResource;
class IInputCore : public BaseCore
{
public:
	IInputCore ()
		: BaseCore() {};
	virtual ~IInputCore (){};	

	
	// VIRTUAL OVERRIDING
	virtual void Update(float dt) = 0;
	virtual void Initialize() = 0;
	virtual void Shutdown() = 0;
	virtual void Pause() = 0;
	virtual void ProcessMessages() = 0;
	virtual void Initialize(HWND hwnd, HINSTANCE hinst) = 0;

	int GetCorePriority() { return 0; }

	inline void SetSharedInputResource(SharedInputResource* res) { m_siInputResource = res; }

	SharedInputResource* m_siInputResource;

	// Keyboard
	/*bool Register(char key,std::string label, void(*CallBack)());
	bool Register(char key,std::string label, void(*CallBack)(),float delay);
	std::string GetRegister(char);	
	void RemoveRegister(char key);*/
	virtual bool	IsKeyDown(char key) = 0;		// Key is down
	virtual bool	IsNewKeyRelease(char key) = 0;	// Key has been released
	virtual std::vector<int>* GetReleasedKeys() = 0;
	virtual char* GetKeyboardState() = 0;

	// Controller
	virtual bool IsNewButtonRelease(unsigned int btn) = 0;
	virtual bool IsButtonDown(unsigned int btn) = 0;
	virtual Vector2 GetTriggers() = 0;
	virtual Vector2 GetTriggerDelta() = 0;
	virtual Vector2 GetLeftStick() = 0;
	virtual Vector2 GetRightStick() = 0;
	virtual void VibrateController(float duration, Vector2 strength = Vector2()) = 0;
	virtual bool GetControllerIsConnected() = 0;
	// Mouse
	virtual bool	IsNewMouseClick(int button) = 0;
	virtual bool	IsMouseButtonDown(int button) = 0;
	virtual bool	IsMouseInBounds(RECT bounds) = 0;

	virtual Vector2 GetMousePos() = 0;
	virtual float	GetMouseWheel() = 0;

	// change in x,y,z axis for mouse
	virtual float GetMouseDX() = 0;
	virtual float GetMouseDY() = 0;
	virtual float GetMouseDZ() = 0;
};