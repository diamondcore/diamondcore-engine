#pragma once
#include "BaseCore.h"

enum AI_message
{
	MSG_CreateEntity_Pos,
	MSG_CreateEntity_PosVel,
	MSG_SetEntity_Pos,
	MSG_SetEntity_Vel,
	MSG_RemoveEntity,
	MSG_SetPathingUse,
	MSG_SetPotentialUse,
	MSG_CreatePotential,
	MSG_AddPotentialData,
	MSG_RemovePotential,
	MSG_SetPotential,
	MSG_SetAIPriority,
	MSG_AddForce,
	MSG_RemoveForce,
	MSG_ClearForces,
	MSG_FindPath,
	MSG_CreateNavGraph,
	MSG_SetNavNodes,
	MSG_Pause,
	MSG_SetTarget,
	MSG_SetState,
	MSG_Clear
};

enum AI_forceType
{
	Force_Seek,
	Force_Flee,
	Force_Arrive,
};

enum AI_StateType
{
	State_Idle,
	State_Wander,
	State_Potential,
	State_Flee,
	State_Pathing
};

class IAICore : public BaseCore
{

	public:
		virtual void Pathing() = 0;
		virtual void Steering() = 0;
		virtual void Potential() = 0;

		virtual int AddEntity(int &iSharedID, float fPositionX, float fPositionY, float fPositionZ,
			float fVelocityX, float fVelocityY, float fVelocityZ) = 0;

		virtual int AddPotentialElement(float fPositionX, float fPositionY, float fPositionZ,
			float fVelocityX, float fVelocityY, float fVelocityZ, unsigned int dTeamName) = 0;

		virtual void AddSeekForce(int iEntityID, float *fTarget, float fMagnitude,
			float fPriority, bool bUseDynamicTarget, unsigned int iRegistrationID) = 0;

		virtual void AddFleeForce(int iEntityID, float *fTarget, float fMagnitude,
			float fPriority, bool bUseDynamicTarget, unsigned int iRegistrationID) = 0;

		virtual void AddArriveForce(int iEntityID, float *fTarget, float fInnerRange, float fOuterRange, float fMagnitude,
			float fPriority, bool bUseDynamicTarget, unsigned int iRegistrationID) = 0;

		virtual void SetState(int iEntityID, int iState) = 0;

		/*virtual void Update(float dt) = 0;
		virtual void Initialize() = 0;
		virtual void Shutdown() = 0;
		virtual void Pause() = 0;
		virtual void ProcessMessages() = 0;*/



};