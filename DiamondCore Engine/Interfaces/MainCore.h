#pragma once
#include <Windows.h>
#include "../DataTypes.h"
#include "../Messaging System\MessageDispatcher.h"

#include "../Interfaces.h"

//*******************************************

/*
	This is the Main Core. It coordinates all communication between
	engine core DLL's, housing the shared memory manager and 
	messaging system.
*/

class MainCore
{
public:
	MainCore(HINSTANCE inst);
	~MainCore(void);

	//Accessors for core handles
	int LoadCoreDLL(char *cFilePath, CoreType eType);
	int ReleaseCoreDLL(CoreType eType);
	
	// core specific accessors
	IAICore* GetAICore();
	IGraphicsCore* GetGraphicsCore();
	IInputCore* GetInputCore();
	ISoundCore* GetSoundCore();

	////////////////////
	// SCRATCH PAD VARS
	////////////////////
	HRESULT hr; 

	// Temporarily public while engine is restructured
	Module* m_mMainMemory;
	
	// message dispatcher
	MessageDispatcher* m_pMessageDisp;

	inline MemoryManager* GetMemoryManager() { return m_pMemManager; }

	void Update(float dt);

	inline float GetElapsedTime() { return m_fTimeElapsed; };

	void Shutdown();

	vector<BaseCore*> m_vActiveCores;

	struct EngineUpdateInfo
	{
	public:

		EngineUpdateInfo(MainCore* mainCore, BaseCore* coreToUpdate) :
			m_MainCore(mainCore), m_bCoreToUpdate(coreToUpdate) {}

		MainCore* m_MainCore;
		BaseCore* m_bCoreToUpdate;
	};

private:

	// Core Handles
	HMODULE m_hGraphicsDLL;
	HMODULE m_hAIDLL;
	HMODULE m_hInputDLL;
	HMODULE m_hSoundDLL;
	HMODULE m_hScriptingDLL;

	HINSTANCE hInst;
	
	// Core Interface Pointers
	IGraphicsCore *m_pIGraphics;
	IAICore *m_pIAI;
	IInputCore *m_pIInput;
	ISoundCore *m_pISound;

	// memory manager
	MemoryManager* m_pMemManager;

	
	
	// setting Core Variables
	void SetCoreVars(CoreType eType, BaseCore* pointerToCore);

	//TODO: This WILL run out of precision eventually.
	// consider making a XNA/.NET style GameTime/DateTime object
	// for larger precision
	float m_fTimeElapsed;

	void BeginUpdateLoop();
};

// externing (in C-style) all the creation and release of the COM objects
extern "C"
{
	// create and release of graphics
	HRESULT CreateGraphicsObject(HINSTANCE hDLL, IGraphicsCore **pInterface);
	HRESULT ReleaseGraphicsObject(IGraphicsCore **pInterface);

	// create and release of AI
	HRESULT CreateAIObject(HINSTANCE hDLL, IAICore **pInterface);
	HRESULT ReleaseAIObject(IAICore **pInterface);

	// create and release of Input
	HRESULT CreateInputObject(HINSTANCE hDLL, IInputCore **pInterface);
	HRESULT ReleaseInputObject(IInputCore **pInterface);

	// create and release of Sound
	HRESULT CreateSoundObject(HINSTANCE hDLL, ISoundCore **pInterface);
	HRESULT ReleaseSoundObject(ISoundCore **pInterface);
}

// function pointers to allow for address of creation and release functions
//	to be acquired
typedef HRESULT (*CREATEAIOBJECT)(HINSTANCE hDLL, IAICore **pInterface);
typedef HRESULT(*RELEASEAIOBJECT)(IAICore **pInterface);

typedef HRESULT (*CREATEINPUTOBJECT)(HINSTANCE hDLL, IInputCore **pInterface);
typedef HRESULT(*RELEASEINPUTOBJECT)(IInputCore **pInterface);

typedef HRESULT (*CREATEGRAPHICSOBJECT)(HINSTANCE hDLL, IGraphicsCore **pInterface);
typedef HRESULT(*RELEASEGRAPHICSOBJECT)(IGraphicsCore **pInterface);

typedef HRESULT (*CREATESOUNDOBJECT)(HINSTANCE hDLL, ISoundCore **pInterface);
typedef HRESULT(*RELEASESOUNDOBJECT)(ISoundCore **pInterface);

DWORD WINAPI UpdateCore(void* data);