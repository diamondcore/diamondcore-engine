#pragma once
#include "BaseCore.h"

enum GraphicsMessages {
	// Create Resources
	msg_CreateMesh,
	msg_CreateTexMesh,
	msg_CreateTexture,
	msg_CreateAnimMesh,
	msg_CreateAnimMeshTex,
	msg_CreateFontText,

	// Remove Resources
	msg_RemoveMesh,
	msg_RemoveTexture,
	msg_RemoveAnimMesh,
	msg_RemoveFontText,

	// Modify Graphic Effects
	msg_UseDOF,
	msg_UseSSAO,
	msg_UseHDR,
	msg_UseShadowMap,
	msg_SetTimeOfDay,	// Set's the Sun's position

	// Stats Output
	msg_DisplayFPS,
	msg_DisplayNumObjs,

	// Animation Options
	msg_Anim_Play,
	msg_Anim_Pause,
	msg_Anim_Set,

	// Camera Controls
	msg_SetCameraID,		// Set's the camera sharedMemID

	// Instancing
	msg_CreateBillboard,
	msg_RemoveBillboard,

	// Particles
	msg_CreateParticle,
	msg_RemoveParticle,

	// Set Color
	msg_SetMeshColor,
	msg_SetTextColor,
	msg_SetAnimMeshColor,
	msg_SetFontColor,
	msg_SetBillboardColor,

	msg_ClearScene,		// Removes all entities
	msg_CheckQuadPick,	// Checks pick ray against quad

	// Scaling Messages
	msg_ScaleMesh,
	msg_ScaleTexture,
	msg_ScaleAnimMesh,
	msg_ScaleBillboard,

};

/*enum GlareType{
	GLARE_DISABLE = 0,

	GLARE_CAMERA						= 1,
	GLARE_NATURAL						= 2,
	GLARE_CHEAPLENS						= 3,
	GLARE_AFTERIMAGE					= 4, 
	GLARE_FILTER_CROSSCREEN				= 5,
	GLARE_FILTER_CROSSCREEN_SPECTRAL	= 6,
	GLARE_FILTER_SNOWCROSS				= 7,
	GLARE_FILTER_SNOWCROSS_SPECTRAL		= 8,
	GLARE_FILTER_SUNNYCROSS				= 9,
	GLARE_FILTER_SUNNYCROSS_SPECTRAL	= 10,
	GLARE_CINEMACAM_VERTICALSLITS		= 11,
	GLARE_CINEMACAM_HORIZONTALSLITS		= 12,
	
	NUM_GLARE_TYPES,
	GLARE_USERDEF = -1,
	GLARE_DEFAULT = GLARE_FILTER_CROSSCREEN,
};*/

class SpriteFont;
class IGraphicsCore : public BaseCore
{
public:
	IGraphicsCore()
		: BaseCore() {};
	virtual ~IGraphicsCore(){};


	// Exposing Methods ;)
	// Create Methods
	virtual void CreateMesh(int id, std::string meshName, bool shadowMap) = 0;
	virtual void CreateMesh(int id, std::string meshName, std::string texName, bool shadowMap) = 0;
	virtual void CreateTexture(int id, std::string texName) = 0;
	virtual void CreateTexture(int id, std::string texName, D3DXCOLOR alpha) = 0;
	virtual void CreateFont(int id, char* font, char* text) = 0;
	virtual void CreateFont(int id, char* font, char* text, float* pData) = 0;
	virtual void CreateAnimMesh(int id, char* animMeshName, bool shadosMap) = 0;
	virtual void CreateAnimMesh(int id, char* animMeshName, char* texName, bool shadowMap) = 0;
	virtual void CreateBillboard(int id, char* texName, bool shadowMap) = 0;
	virtual void CreateParticle(int id, char* particle, void* param) = 0;

	// Delete Methods
	virtual void RemoveMesh(int id, char* meshName) = 0;
	virtual void RemoveTextrue(int id, char* textureName) = 0;
	virtual void RemoveFont(int id, char* font) = 0;
	virtual void RemoveAnimMesh(int id, char* animMeshName) = 0;
	virtual void RemoveBillboard(int id, char* texName) = 0;
	virtual void RemoveParticle(int id) = 0;

	// Set Color
	virtual void SetMeshColor(int id, char* meshName, D3DXCOLOR color) = 0;
	virtual void SetTextureColor(int id, char* texName, D3DXCOLOR color) = 0;
	virtual void SetFontColor(int id, char* fontName, D3DXCOLOR color) = 0;
	virtual void SetAnimMeshColor(int id, char* animMeshName, D3DXCOLOR color) = 0;
	virtual void SetBillboardColor(int id, char* texture, D3DXCOLOR color) = 0;

	// Set Scale
	virtual void SetMeshScale(int id, char* meshName, D3DXVECTOR3 scale) = 0;
	virtual void SetTextureScale(int id, char* texName, D3DXVECTOR3 scale) = 0;
	virtual void SetFontScale(int id, char* fontName, D3DXVECTOR3 scale) = 0;
	virtual void SetAnimMeshScale(int id, char* animMeshName, D3DXVECTOR3 scale) = 0;
	virtual void SetBillboardScale(int id, char* texName, D3DXVECTOR3 scale) = 0;
	
	// Get Scale
	virtual void GetMeshScale(int id, char* meshName, D3DXVECTOR3& scale) = 0;


	// Utility Methods
	virtual void SetCameraID(int id) = 0;
	virtual void SetCameraPos(void* viewPos) = 0;
	virtual void SetCameraView(void* viewMat) = 0;
	virtual void SetCameraViewByCopy(void* viewMat) = 0;
	virtual void SetCameraGlareType(int glareType) = 0;
	virtual void SetTimeOfDay(int hour, int min, int sec) = 0;
	virtual void DisplayFPS(bool enable) = 0;
	virtual float GetFPS() = 0;
	virtual void DisplayNumObjs(bool enable) = 0;
	virtual void UseDOF(bool enable) = 0;
	virtual void UseSSAO(bool enable) = 0;
	virtual void UseHDR(bool enable) = 0;
	virtual void UseShadowMap(bool enable) = 0;
	virtual void UseDistortion(bool enable) = 0;
	virtual void UseBlur(bool enable) = 0;
	virtual void UseDecals(bool enable) = 0;
	virtual void ClearScene() = 0;
	virtual void CheckQuadPicking(int hitIDs[], int& count) = 0;	// Will fill the array with picked IDs and set the count that were picked
	virtual HWND GetWindow() = 0;
	virtual void GetImageInfo(const char* texName, D3DXIMAGE_INFO& info) = 0;
	virtual void GetMeshBV(char* meshName, Vector3& halfExtent) = 0;

	// Must be in the same folder as the mesh. 
	//This will also auto load normal and spec maps with the same base name
	virtual void SetDefaultMeshTexture(char* meshName, char* texName) = 0;	
	virtual void SetDefaultMeshLightAttributes(char* meshName, float diffuseCoeff, float phongCoeff, float phongExp) = 0;
	
	// Lighting controlers
	virtual void AddLight(int& returnID, D3DXVECTOR4 pos, D3DXVECTOR4 intensity, int power) = 0;
	virtual void ChangeLightPos(int id, D3DXVECTOR4 pos) = 0;
	virtual void ChangeLightPower(int id, int power) = 0;
	virtual void RemoveLight(int id) = 0;

	// Distortion controllers
	virtual void CreateDistortionMesh(int sharedID, char* meshName, int groupID) = 0;
	virtual void RemoveDistortionMesh(int sharedID, char* meshName) = 0;
	virtual void SetDistortOffset(int groupID, D3DXVECTOR2 *offset1, D3DXVECTOR2 *offset2) = 0;
	virtual void SetDistortSpeed(int groupID, D3DXVECTOR2 *speed1, D3DXVECTOR2 *speed2) = 0;
	virtual void SetDistortBump(int groupID, float bump1, float bump2) = 0;
	virtual void SetDistortScale(int groupID, float scale1, float scale2) = 0;
	virtual void SetDistortIntensity(int groupID, float intensity1, float intensity2) = 0;
	virtual void SetDistortionMeshScale(int id, char* meshName, int groupID, D3DXVECTOR3 scale) = 0;

	// Blur Controllers
	virtual void SetBlurScale(float scale) = 0;

	// Decal Controllers
	virtual void AddDecal(int& returnID, int iSharedID, char*  meshName, char* textureName, bool isDynamic, bool lockToObject, float maxAge,
							D3DXVECTOR3 pos, D3DXVECTOR3 target, D3DXVECTOR3 up) = 0;
	virtual void AddDecal(int& returnID, int iSharedID, char*  meshName, char* textureName, bool isDynamic, bool lockToObject, float maxAge,
							D3DXVECTOR3 target, D3DXVECTOR3 normal, float size, D3DXVECTOR3 up) = 0;
	virtual bool SetDecalPosition(UINT id, bool lookInDynamic, D3DXVECTOR3 pos) = 0;
	virtual void RemoveDecalsByMesh(char* MeshName) = 0;

	virtual void AddBolt(int &ID, int groupID, int numSegments, std::vector<int> *additionalGroups) = 0;
	virtual bool RemoveBolt(int ID) = 0;
	virtual void RemoveBoltGroup(int groupID) = 0;

	virtual void AddBoltObject(int &ID, D3DXVECTOR3 startPos, D3DXVECTOR3 endPos, float boltSize, float ArcAmount, float ArcMidpoint,
		UINT selectionType, int selectionID, D3DXVECTOR4 color) = 0;
	virtual bool RemoveBoltObject(int ID) = 0;

	// Electricity Controllers
	virtual bool SetBoltObjPositions(int ID, D3DXVECTOR3 startPos, D3DXVECTOR3 endPos) = 0;
	virtual bool SetBoltObjArcScale(int ID, float scale) = 0;
	virtual bool SetBoltObjThickness(int ID, float thickness) = 0;
	virtual bool SetBoltObjMidpoint(int ID, float midpoint) = 0;
	virtual bool SetBoltObjRandOffset(int ID, int offset) = 0;
	virtual bool SetBoltObjSelectionType(int ID, UINT selectionType) = 0;
	virtual bool SetBoltObjSelectionID(int ID, int selectionID) = 0;
	virtual bool SetBoltObjVisibility(int ID, bool visible) = 0;
	virtual bool SetBoltObjColor(int ID, D3DXVECTOR4 color) = 0;

	// Terrain Morphing
	virtual void InitTerrain(unsigned int rows, unsigned int cols, unsigned int sizeZ, unsigned int sizeX, float texScale) = 0;
	virtual void LoadTerrain(char* path, char* fileName) = 0;
	virtual void SaveTerrain(char* path, char* fileName) = 0;
	virtual void ResetTerrain() = 0;
	virtual void RemoveTerrain() = 0;
	virtual void SetBrush(float* radius) = 0;
	virtual void MorphUp(float increment) = 0;		// increment [0, 1]
	virtual void MorphDown(float increment) = 0;	// increment [0, 1]
	virtual void Smooth(float scale) = 0;			// scale [0, 1]
	virtual void SmoothCenter(float scale) = 0;		// scale [0, 1]
	virtual void UpdateBrush() = 0;
	virtual int  GetNumVerts() = 0;
	virtual int  GetNumIndices() = 0;
	virtual void GetTerrainVerts(Vector3 verts[]) = 0;
	virtual void GetTerrainIndices(WORD indices[]) = 0;
	virtual void GetTerrainSize(Vector3& size) = 0;
	virtual void GetTerrainNumRowsCols(int& rows, int& cols) = 0;

	// Terrain Painting
	virtual void SetTerrainTex1(char* texName) = 0;
	virtual void SetTerrainTex2(char* texName) = 0;
	virtual void SetTerrainTex3(char* texName) = 0;
	virtual void PaintTex1(float increment) = 0;
	virtual void PaintTex2(float increment) = 0;
	virtual void PaintTex3(float increment) = 0;
	virtual void GetTexture(int tex, char* texName) = 0;

	// Cusom Terrain Coloring
	virtual void PaintCustomTex1(float increment) = 0;
	virtual void PaintCustomTex2(float increment) = 0;
	virtual void PaintCustomTex3(float increment) = 0;
	virtual void GetColorAt(Vector3 worldPos, D3DXCOLOR& color) = 0; 
	virtual void RenderCustomTextue(bool render) = 0;
	
	// Selecting Objects
	virtual void GetSelectedObj(char* name, int& id) = 0;

	// Manual Load/Unload Texture Calls
	virtual void LoadTexture(const char* textureName) = 0;
	virtual void LoadTexture(const char* textureName, D3DXCOLOR alpha) = 0;
	virtual void UnloadTexture(const char* textureName) = 0;

	// Manual 2D Rendering
	virtual void BeginRender() = 0;
	
	// Rendering 2D Calls
	virtual void Render2D(char* textureName, D3DXMATRIX transform) = 0;	// Render 2D Texture
	virtual void Render2D(char* textureName, D3DXMATRIX transform, D3DXCOLOR color) = 0; // Render 2D Texture with colors

	virtual void LoadFont(const char* fontName) = 0;
	virtual void UnloadFont(const char* fontName) = 0;

	virtual Vector2 MeasureString(const char* fontName, char* text) = 0;
	virtual void RenderString(const char* fontname, const char* text, Vector2 position, D3DXCOLOR color) = 0;
	virtual void RenderString(const char* fontname, const char* text, Vector2 position, float depth, D3DXCOLOR color) = 0;
	
	// Batch Rendering
	virtual void Render2D(char* textureName, D3DXMATRIX transform[], int count) = 0;
	virtual void Render2D(char* textureName, D3DXMATRIX transform[], D3DXCOLOR color[], int count) = 0;

	virtual void EndRender() = 0;
	virtual void ReloadShader() = 0;
	virtual void PrintBuffer(const char* buffer) = 0;

	virtual Vector2 GetScreenSize() = 0;
	virtual void SetResizeCallBack(void (*funcP)()) = 0;
	virtual bool IsDeviceLost() = 0;

	// Create Window (pass in the hinstance givent by the WinMain() function)
	virtual void Initialize(HINSTANCE hInst, const char* windowTitle) = 0;
};