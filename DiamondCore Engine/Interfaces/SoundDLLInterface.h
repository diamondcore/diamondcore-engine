#pragma once
#include "BaseCore.h"

enum SoundMessages
{
	msg_GetMasterVolume,
	msg_SetMasterVolume,

	msg_PlayOnce2D,
	msg_PlayOnce3D,
	msg_StopAll,
	msg_ResumeAll,
	msg_PauseAll,
	msg_PlayBGM,
	msg_SetChannelVolume
};

enum SoundState
{
	PLAYING,
	STOPPED,
	PAUSED
};

class ISoundCore : public BaseCore
{

public:

	virtual void SetMasterVolume(float newVolume) = 0;
	virtual float GetMasterVolume() = 0;

	virtual void SetChannelVolume(int channelIndex, float newVolume) = 0;
	virtual void GetChannelVolume(int channelIndex, float* val) = 0;

	virtual void Apply3D(Vector3 pos, Vector3 forward, Vector3 up, Vector3 velocity) = 0;
	virtual void PlayBGM(string fileName, float fadeDuration = 0.0f) = 0;

	virtual void PauseAll(int channelGroup) = 0;
	virtual void ResumeAll(int channelGroup) = 0;
	virtual void StopAll(int channelGroup) = 0;

	virtual SoundState GetState(int id) = 0;
	
	virtual void PlayOnce2D(string soundToPlay, int channelGroup, bool loop) = 0;
	virtual void PlayOnce3D(string soundToPlay, float posx, float posy, float posZ, int channelGroup, bool loop) = 0;
	virtual unsigned int CreateInstance3D(string soundToPlay, Vector3 position, bool paused, int channelGroup, bool loop) = 0;
	virtual unsigned int CreateInstance2D(string soundToCreate, bool paused, int channelGroup, bool loop) = 0;

	virtual void PlaySFX(unsigned int instance, bool loop, int channelIdx) = 0;
	virtual void PauseSFX(unsigned int instance) = 0;
	virtual void StopSFX(unsigned int instance) = 0;

	virtual void Initialize() = 0;
};