#include "MainCore.h"
#include <iostream>

#define CONSOLEDEBUG 1

// setting Core Variables
void MainCore::SetCoreVars(CoreType eType, BaseCore* pointerToCore)
{
	pointerToCore->SetMessageMgr(m_pMessageDisp);
	m_pMessageDisp->RegisterCore(eType, &pointerToCore->m_sMyMessages);
}

MainCore::MainCore(HINSTANCE inst)
{
	// Set all Core Handles to NULL
	m_hGraphicsDLL		= NULL;
	m_hAIDLL			= NULL;
	m_hInputDLL			= NULL;
	m_hSoundDLL			= NULL;
	m_hScriptingDLL		= NULL;
	
	// Set all Core Interface Pointers to NULL
	m_pIGraphics	= NULL;
	m_pIAI			= NULL;
	m_pIInput		= NULL;
	m_pISound		= NULL;

	// create a message dispatcher
	m_pMessageDisp = new MessageDispatcher();

	// create a memory manager
	m_pMemManager = new MemoryManager();

	m_fTimeElapsed = 0.0f;

	hInst = inst;


	m_mMainMemory = new Module();

	m_mMainMemory->SetMemMgr(m_pMemManager);

}

//Accessors for core handles
int MainCore::LoadCoreDLL(char *cFilePath, CoreType eType)
{
	// entering loadcore 

#if CONSOLEDEBUG 
	OutputDebugString("Entering MainCore::LoadCoreDLL");

#endif
	switch(eType)
	{
	case CT_AI:	// AI ***************************************************
	{
#if CONSOLEDEBUG 
		OutputDebugString("\n  Attempting to Load AI DLL");
	
#endif

		m_hAIDLL = LoadLibrary(cFilePath);

	// if it fails
		if (!m_hAIDLL)
		{
#if CONSOLEDEBUG 
			OutputDebugString("Loading AI DLL Failed!");
#endif
			return 0;
		}

#if CONSOLEDEBUG 
		OutputDebugString("AI DLL was loaded");
		OutputDebugString("\t\tQuerying the DLL to get a pointer to the CreateAIObject Function");
#endif
		CREATEAIOBJECT _CreateAIObject = 0;
		
		_CreateAIObject = (CREATEAIOBJECT)GetProcAddress(m_hAIDLL, "CreateAIObject");

		//Verify the Method was set
		if(!_CreateAIObject)
		{
#if CONSOLEDEBUG
			OutputDebugString("          Failed finding a pointer to the CreateAIObject Function in AI DLL");
#endif
			return 0;
		}

		OutputDebugString("     Creating the AI Object via the CreateAIObject Method in AI DLL");
		
		//Create the AI Object
		hr = _CreateAIObject(m_hAIDLL, &m_pIAI);


		if(FAILED(hr))
		{
			//MessageBox(NULL, L"Creating AI Object from lib failed", L"Engine -  Error", MB_OK|MB_ICONERROR);
			OutputDebugString("          Creating AI Object from Lib failed");
		
			m_pIAI = NULL;
			return 0;
		}

		OutputDebugString("  Leaving MainCore::LoadCoreDLL");
		
		// sets messenger
		SetCoreVars(CT_AI, m_pIAI);

		// gives the core the pointer to the module's they'll be using.
		m_pIAI->SetMemoryModule(m_mMainMemory);

		m_pIAI->Initialize();
		return 1;

		break;
	}
	case CT_GRAPHICS:	//GRAPHICS********************************************
	{
#if CONSOLEDEBUG 
	OutputDebugString("\n  Attempting to Load GRAPHICS DLL!!");
#endif

	m_hGraphicsDLL = LoadLibrary(cFilePath);

	// if it fails
	if (!m_hGraphicsDLL)
	{
#if CONSOLEDEBUG 
		OutputDebugString("  Loading Graphics DLL Failed!");
#endif
		return 0;
	}

#if CONSOLEDEBUG 
	OutputDebugString("\tGraphics DLL was loaded");
	OutputDebugString("\t\tQuerying the DLL to get a pointer to the CreateAIObject Function");
#endif

	CREATEGRAPHICSOBJECT _CreateGraphicsObject = 0;

	_CreateGraphicsObject = (CREATEGRAPHICSOBJECT)GetProcAddress(m_hGraphicsDLL, "CreateGraphicsObject");

	// verify the method was set
	if(!_CreateGraphicsObject)
	{
#if CONSOLEDEBUG
		OutputDebugString("\t\t\tFailed finding a pointer to the CreateGraphicsObject Function in GraphicsDLL");
#endif
		return 0;
	}

		OutputDebugString("\t\t\tCreating the Graphics Object via the CreateGraphicsObject Method in GraphicsDLL");

		hr = _CreateGraphicsObject(m_hGraphicsDLL, &m_pIGraphics);

		if(FAILED(hr))
		{
			//MessageBox(NULL, L"Creating AI Object from lib failed", L"Engine -  Error", MB_OK|MB_ICONERROR);
			OutputDebugString("\t\t\t\t\tCreating Graphics Object from Lib failed");
		
			m_pIGraphics = NULL;
			return 0;
		}

		OutputDebugString("\tLeaving MainCore::LoadCoreDLL");


		SetCoreVars(CT_GRAPHICS, m_pIGraphics);

		// gives the core the pointer to the module's they'll be using.
		m_pIGraphics->SetMemoryModule(m_mMainMemory );

		// TEMP WINDOW SETTING HERE
		// TODO: Setting the window title should be a seperate function call.
		m_pIGraphics->Initialize(m_hGraphicsDLL, "Turbine");
		return 1;


		break;
	}
	case CT_INPUT:	//INPUT**********************************************
	{
#if CONSOLEDEBUG 
	OutputDebugString("\tAttempting to Load INPUT DLL");

#endif

	m_hInputDLL = LoadLibrary(cFilePath);

	// if it fails
	if (!m_hInputDLL)
	{
#if CONSOLEDEBUG 
		OutputDebugString("\tLoading Input DLL Failed!");
#endif
		return 0;
	}

#if CONSOLEDEBUG 
	std::cout << "\n  Input DLL was loaded" << std::endl;
	std::cout << "     Querying the DLL to get a pointer to the CreateInputObject Function" << std::endl;
	
#endif

	CREATEINPUTOBJECT _CreateInputObject = 0;

	_CreateInputObject = (CREATEINPUTOBJECT)GetProcAddress(m_hInputDLL, "CreateInputObject");

	if(!_CreateInputObject)
	{
#if CONSOLEDEBUG
		std::cout << "          Failed finding a pointer to the CreateInputObject Function in InputDLL" << std::endl;
#endif
		return 0;
	}
		std::cout << "     Creating the Input Object via the CreateInputObject Method in InputDLL" << std::endl;

		hr = _CreateInputObject(m_hInputDLL, &m_pIInput);

		if(FAILED(hr))
		{
			//MessageBox(NULL, L"Creating AI Object from lib failed", L"Engine -  Error", MB_OK|MB_ICONERROR);
			std::cout << "          Creating Input Object from Lib failed" << std::endl;
		
			m_pIInput = NULL;
			return 0;
		}

		std::cout << "  Leaving MainCore::LoadCoreDLL" << std::endl;
		SetCoreVars(CT_INPUT, m_pIInput);

		// gives the core the pointer to the module's they'll be using.
		m_pIInput->SetMemoryModule(m_mMainMemory);
		//TODO: Initialize
		m_pIInput->Initialize(m_pIGraphics->GetWindow(), hInst);
		return 1;

		break;
	}
	case CT_SOUND:	//SOUND***********************************************
	{
#if CONSOLEDEBUG 
	std::cout << "\n  Attempting to Load Sound DLL" << std::endl;

#endif

	m_hSoundDLL = LoadLibrary(cFilePath);

	// if it fails
	if (!m_hSoundDLL)
	{
#if CONSOLEDEBUG 
		std::cout << "\n  Loading Sound DLL Failed!" << std::endl;
#endif
		return 0;
	}

#if CONSOLEDEBUG 
	std::cout << "\n  Sound DLL was loaded" << std::endl;
	std::cout << "     Querying the DLL to get a pointer to the CreateSoundObject Function" << std::endl;
	
#endif

	CREATESOUNDOBJECT _CreateSoundObject = 0;

	_CreateSoundObject = (CREATESOUNDOBJECT)GetProcAddress(m_hSoundDLL, "CreateSoundObject");

	if(!_CreateSoundObject)
	{
#if CONSOLEDEBUG
		std::cout << "          Failed finding a pointer to the CreateSoundObject Function in SoundDLL" << std::endl;
#endif
		return 0;
	}
		std::cout << "     Creating the Sound Object via the CreateSoundObject Method in SoundDLL" << std::endl;

		hr = _CreateSoundObject(m_hSoundDLL, &m_pISound);

		if(FAILED(hr))
		{
			std::cout << "          Creating Sound Object from Lib failed" << std::endl;
		
			m_pISound = NULL;
			return 0;
		}

		std::cout << "  Leaving MainCore::LoadCoreDLL" << std::endl;
		SetCoreVars(CT_SOUND, m_pISound);

		// gives the core the pointer to the module's they'll be using.
		m_pISound->SetMemoryModule(m_mMainMemory);

		m_pISound->Initialize();
		return 1;

		break;
	}

	default:
		throw;

};// END SWITCH

}

int MainCore::ReleaseCoreDLL(CoreType eType)
{
	// entering loadcore 

#if CONSOLEDEBUG 
	std::cout << "\n  Entering MainCore::LoadCoreDLL" << std::endl;

#endif
	switch(eType)
	{
	case 0:	// AI ***************************************************
	{
		
		RELEASEAIOBJECT _ReleaseAIObject = 0;
#if CONSOLEDEBUG 
		std::cout << "\n  Attempting to Release AI DLL" << std::endl;

		std::cout << "     Testing if AI DLL exist" << std::endl;	
#endif

		if(m_hAIDLL)
		{
			std::cout << "     Getting pointer to ReleaseAIObjet in AIDLL" << std::endl;
			_ReleaseAIObject = (RELEASEAIOBJECT)GetProcAddress(m_hAIDLL, "ReleaseAIObject");
		}

		std::cout << "     Testing if IAI Interface exist" << std::endl;
		if(m_pIAI)
		{
			std::cout << "     Calling ReleaseAIObject DLL" << std::endl;
			hr = _ReleaseAIObject(&m_pIAI);

			if(FAILED(hr))
			{
				std::cout << "     IAI Object FAILED to Release" << std::endl;
				m_pIAI = NULL;
			}
		}

		break;
	}
	case 1:	//GRAPHICS********************************************
#if CONSOLEDEBUG 
	std::cout << "\n  Attempting to Release GRAPHICS DLL!!" << std::endl;

#endif

		break;

	case 2:	//INPUT**********************************************
#if CONSOLEDEBUG 
	std::cout << "\n  Attempting to Release INPUT DLL" << std::endl;

#endif

	

		break;

	case 3:	//SOUND***********************************************
#if CONSOLEDEBUG 
	std::cout << "\n  Attempting to Release SOUND DLL" << std::endl;

#endif

	
		break;

	case 4:	// SCRIPTING********************************
#if CONSOLEDEBUG 
	std::cout << "\n  Attempting to Release SCRIPTING DLL" << std::endl;

#endif
		break;
	}

	return 0;
}

void MainCore::Update(float dt)
{
	static bool init = 0;
	// I'll probably have to do this more ellegantly in the future... 
	if (!init)
	{
		BeginUpdateLoop();
		init = true;
	}

	m_fTimeElapsed += dt;

	// update all cores in order of priority.
	for (auto i = m_vActiveCores.begin(); i != m_vActiveCores.end(); i++)
		(*i)->Update(dt);
}

bool PrioritySorter(BaseCore* c1, BaseCore* c2)
{
	return c1->GetCorePriority() < c2->GetCorePriority();
}

void MainCore::BeginUpdateLoop()
{
	if (m_pIGraphics)
		m_vActiveCores.push_back(m_pIGraphics);

	if (m_pIAI)
		m_vActiveCores.push_back(m_pIAI);

	if (m_pIInput)
		m_vActiveCores.push_back(m_pIInput);

	if (m_pISound)
		m_vActiveCores.push_back(m_pISound);

	sort(m_vActiveCores.begin(), m_vActiveCores.end(), PrioritySorter);
}
	
// core specific accessors
IAICore* MainCore::GetAICore()
{
	return m_pIAI;
}

IGraphicsCore* MainCore::GetGraphicsCore()
{
	return m_pIGraphics;
}

IInputCore* MainCore::GetInputCore()
{
	return m_pIInput;
}

ISoundCore* MainCore::GetSoundCore()
{
	return m_pISound;
}

void MainCore::Shutdown()
{
	for (auto x = 0; x < m_vActiveCores.size(); x++)
		m_vActiveCores[x]->Shutdown();

	m_vActiveCores.clear();
}

MainCore::~MainCore()
{

}
