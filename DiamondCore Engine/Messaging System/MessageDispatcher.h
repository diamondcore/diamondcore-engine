#pragma once
#include "Mail.h"
#include "../Multithreading/CriticalSection.h"
#include "../DataTypes.h"
#include <set>
#include <memory>
#include <map>



class BaseGameEntity;

//singleton message dispatcher
//#define MMI MessageDispatcher::Instance()

// This class is for protection
class MailInbox
{
	CriticalSection		m_csCondom;
	std::vector<Mail*>	m_sMyMessages;

public:
	MailInbox(){}
	~MailInbox(){}
	void	clear()
	{
		m_csCondom.Lock();
		m_sMyMessages.clear();
		m_csCondom.Unlock();
	}
	int		size()
	{
		ScopedCriticalSection cs(m_csCondom);
		return m_sMyMessages.size();
	}

	void	AddMessage(Mail* message)
	{
		m_csCondom.Lock();
		m_sMyMessages.push_back( message );
		m_csCondom.Unlock();
	}

	void	GetNextMessage(Mail** message)
	{
		m_csCondom.Lock();
		*message = (*m_sMyMessages.begin());
		m_sMyMessages.erase( m_sMyMessages.begin() );
		m_csCondom.Unlock();
	}
};


class MessageDispatcher
{
private:
	//singleton, so ctor is private
	

	// Primary dispatch list - what all cores send messages to 
	//	to be sorted into inboxes
	std::vector<Mail*> m_sDispatchList;

	std::map<int, MailInbox*> m_MailRegistry;

	// critical section protecting the dispatch list, which 
	//	is the entry point for all messages
	CriticalSection m_csDispatchList;

	int m_iNextID;
	// message id system
	inline int GetNextID() { return m_iNextID++; };

	std::vector<Mail*> m_vMessagePool;

	static const int MAX_MESSAGES;


public:

	MessageDispatcher();

	// initializes the messaging system, removing all old messages
	//	for, example, an engine restart during runtime
	void Init();

	void RegisterCore(int coreType, MailInbox* mailBox);
	inline void UnregisterCore(int coreType) { m_MailRegistry.erase(coreType); }

	//send a message
	void DispatchMsg(int sender, int receiver, 
		int msg, MessageData* messageData = NULL, bool bRequiresResponse = 0, double delay = 0.0, int iID = -1);

	// puts message into the right mailbox based on the receiver
	inline void SortMsg(Mail* mMessage)
	{
		m_MailRegistry[mMessage->m_iReceiver]->AddMessage(mMessage);//push_back(mMessage);
	}

	// Called when a core is finished with a message for good. Returns it to the pool
	inline void ReleaseMessage(Mail* message) 
	{ 
		//m_vMessagePool.push_back(message); 
	}

	//goes through the dispatch list to see if any messages need to be sent
	//	if so, sends them to the appropriate mailboxes
	void DispatchDelayedMessages();
	
	~MessageDispatcher();

	// causes memory manager to delete a message from a core's
	//	inbox
	void DeleteMessageByID(CoreType eType, int iID);
};

