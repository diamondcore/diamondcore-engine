#include "MessageDispatcher.h"
#include "ClockTimer.h"

const int MessageDispatcher::MAX_MESSAGES = 300;

MessageDispatcher::MessageDispatcher()
{
	Init();

	m_iNextID = 0;

	for (auto x = 0; x < MAX_MESSAGES; x++)
	{
		auto mail = new Mail();
		mail->m_iID = GetNextID();
		m_vMessagePool.push_back(mail);
	}


}

void MessageDispatcher::Init()
{
	OutputDebugString("Initializing message system");

	m_iNextID = 0;

	m_sDispatchList.clear();
	
	for (auto it = m_MailRegistry.begin(); it != m_MailRegistry.end(); it++)
		(*it).second->clear();

	m_MailRegistry.clear();
}

void MessageDispatcher::RegisterCore(int coreType, MailInbox* mailBox)
{
	assert(m_MailRegistry.find(coreType) == m_MailRegistry.end());

	m_MailRegistry.insert(std::make_pair(coreType, mailBox));
}

//send a message
void MessageDispatcher::DispatchMsg(int sender, int receiver, int msg, MessageData* msgData, 
									bool respRequired, double delay, int iID)
{
	Mail* message;

	if (m_vMessagePool.size() == 0)
	{
		OutputDebugString("Ran out of messages! Allocating 10% more.\n");

		auto amountToGen = (int)((MAX_MESSAGES * 1.0f) + 0.5f);

		for (auto x = 0; x < amountToGen; x++)
		{
			auto mail = new Mail();
			mail->m_iID = GetNextID();
			m_vMessagePool.push_back(mail);
		}
	}

	auto it = m_vMessagePool.begin();
	message =  *it;
	m_vMessagePool.erase(it);
	message->SetData(delay, sender, receiver, msg, respRequired, message->m_iID, msgData);

	//if there is no delay, process the message immediately
	if (delay <= 0.00)
	{
		//send the telegram to the recipient
		std::stringstream ss; 
		ss <<"Immediately Sorting Message to: " << receiver << "'s inbox.\n";
		OutputDebugString(ss.str().c_str());
		SortMsg(message);
		return;
	}

	//else calculate the time when the message should be dispatched
	// This is likely incorrect now... shouldn't we be getting this from the engine?
	double CurrentTime = Clock->GetTickCount();

	message->m_dDispatchTime = CurrentTime + delay;

	//and slap that into the queue
	m_sDispatchList.push_back(message);
	//DEBUG INFO

	std::stringstream debugOutput;		
	debugOutput << "\nDelayed telegram from " << sender << " recorded at time " 
        << CurrentTime << " for " << receiver
        << ". Dispatch time is: "<< message->m_dDispatchTime << ". Msg is " << message->m_iMsg;

	OutputDebugString(debugOutput.str().c_str());
}

void MessageDispatcher::DispatchDelayedMessages()
{
	//get current time
	double CurrentTime = Clock->GetTickCount();

	/*if (m_sDispatchList.size() <= 0)
		return;*/

	//go through dispatch list to see if anything needs dispatching
	while(	(m_sDispatchList.size() > 0 ) &&
			((*m_sDispatchList.begin())->m_dDispatchTime < CurrentTime) &&
			((*m_sDispatchList.begin())->m_dDispatchTime > 0)	)
	{
		//read the message from the front of the list
		auto message = (*m_sDispatchList.begin());

		
		std::stringstream debugOutput;
		debugOutput << "Sending Delayed Message ID: " << message->m_iID << 
			" sorting to inbox: " << message->m_iReceiver;
		OutputDebugString(debugOutput.str().c_str());

		SortMsg(message);

		//and remove it from the list
		m_sDispatchList.erase(m_sDispatchList.begin());
	}
}
	
MessageDispatcher::~MessageDispatcher()
{
	auto it = m_vMessagePool.begin();
	while (it != m_vMessagePool.end())
	{
		delete (*it);
		it = m_vMessagePool.erase(it);
	}
}
