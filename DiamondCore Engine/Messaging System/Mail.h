#pragma once
#include <iostream>
#include <math.h>

#include "../DataTypes.h"


union MessageData;

struct Mail
{
	// id of the message (for access and deletion)
	int			m_iID;

	//the entity that sent this Mail
	int          m_iSender;

	//the entity that is to receive this Mail
	int          m_iReceiver;

	//the message itself. These are all enumerated in the file
	//"MessageTypes.h"
	int          m_iMsg;

	//messages can be dispatched immediately or delayed for a specified amount
	//of time. If a delay is necessary this field is stamped with the time 
	//the message should be dispatched.
	double       m_dDispatchTime;

	// response flag - reuses 
	bool		m_bRequiresResponse;


	Mail():m_dDispatchTime(-1),
					m_iSender(-1),
					m_iReceiver(-1),
					m_iMsg(-1),
					m_bRequiresResponse(0),
					m_iID(-1)
	{
		m_mMessageData = new MessageData();
	}


	Mail(	double	time,
			int		iSender,
			int		iReceiver,
			int		iMsg,
			bool	bRequiresResponse,
			int		iID,
			void*  info): m_dDispatchTime(time),
								m_iSender(iSender),
								m_iReceiver(iReceiver),
								m_iMsg(iMsg),
								m_bRequiresResponse(bRequiresResponse),
								m_iID(iID)
	{
		m_mMessageData = new MessageData();
	}

	void SetData(double	time,
			int		iSender,
			int		iReceiver,
			int		iMsg,
			bool	bRequiresResponse,
			int		iID,
			MessageData*  info = NULL)
	{
		m_dDispatchTime = time;
		m_iSender = iSender;
		m_iReceiver = iReceiver;
		m_iMsg = iMsg;
		m_bRequiresResponse = bRequiresResponse;
		m_iID = iID;

		if (info != NULL)
			memcpy(m_mMessageData, info, sizeof(MessageData));
	}

	~Mail() 
	{
		if (m_mMessageData == NULL)
			return;

		delete m_mMessageData;
		m_mMessageData = NULL;
	}


	//any additional information that may accompany the message
	MessageData*        m_mMessageData;

 
};


//these Mails will be stored in a priority queue. Therefore the >
//operator needs to be overloaded so that the PQ can sort the Mails
//by time priority. Note how the times must be smaller than
//SmallestDelay apart before two Mails are considered unique.
const double SmallestDelay = 0.25;


inline bool operator==(const Mail& t1, const Mail& t2)
{
  return ( fabs(t1.m_dDispatchTime-t2.m_dDispatchTime) < SmallestDelay) &&
          (t1.m_iSender == t2.m_iSender)        &&
          (t1.m_iReceiver == t2.m_iReceiver)    &&
          (t1.m_iMsg == t2.m_iMsg);
}

inline bool operator<(const Mail& t1, const Mail& t2)
{
  if (t1 == t2)
  {
    return false;
  }

  else
  {
    return  (t1.m_dDispatchTime < t2.m_dDispatchTime);
  }
}

inline std::ostream& operator<<(std::ostream& os, const Mail& t)
{
  os << "time: " << t.m_dDispatchTime << "  m_iSender: " << t.m_iSender
     << "   m_iReceiver: " << t.m_iReceiver << "   m_iMsg: " << t.m_iMsg;

  return os;
}

//handy helper function for dereferencing the ExtraInfo field of the Mail 
//to the required type.
template <class T>
inline T DereferenceToType(void* p)
{
  return *(T*)(p);
}



