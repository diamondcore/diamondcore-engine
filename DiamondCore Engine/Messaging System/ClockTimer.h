#pragma once


//this library must be included
#pragma comment(lib, "winmm.lib")

#include <windows.h>



#define Clock ClockTimer::Instance()

class ClockTimer
{
private:
  

  //set to the time (in seconds) when class is instantiated
  double m_dStartTime;

  //set the start time
  ClockTimer(){m_dStartTime = timeGetTime() * 0.001;}

  //copy ctor and assignment should be private
  ClockTimer(const ClockTimer&);
  ClockTimer& operator=(const ClockTimer&);
  
public:

  static ClockTimer* Instance();

  //returns how much time has elapsed since the timer was started
  double GetCurrentTime(){return timeGetTime() * 0.001 - m_dStartTime;}

};

