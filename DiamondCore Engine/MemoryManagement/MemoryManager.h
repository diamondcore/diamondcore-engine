#pragma once

#include <list>
#include <map>
#include "../Multithreading/CriticalSection.h"


// Forward Declarations
class Module;
class SharedResource;
class BaseCore;
class SharedInputResource;

class MemoryManager
{
public:
	const static unsigned int MAX_ENTITIES;
	static void ListCopy(std::list<SharedResource>* dst, std::list<SharedResource>* source, unsigned int count, unsigned int start = 0);

	MemoryManager(void);
	~MemoryManager(void);	

	void FreeSharedResource(int resourceID);
	int CreateSharedResource();
	
	inline Module* GetModule(void* pointerToCore) { return m_mMemoryRegistry[pointerToCore]; }
	std::list<SharedResource> GetReadWriteBuffer(void* pointerToCore);
	std::list<SharedResource>*  GetPrimaryBuffer() { return &m_lPrimaryBuffer; }
	void SwapBuffer(void* pointerToCore);
	void SynchronizePrimaryBuffer(void* pointerToCore);

	void SynchronizeCores(BaseCore* primaryCore, BaseCore* secondaryCore);

	void Release(void* pointerToCore);

	void SyncInputMemory();

	void SetIsActiveWindow(bool bSetWindow)
	{
		m_bIsActiveWindow = bSetWindow;
	}

	bool GetIsActiveWindow()
	{
		return m_bIsActiveWindow;
	}

private:

	//SharedInputResource* m_siSharedInput;

	std::list<SharedResource>  m_lPrimaryBuffer;
	std::map<void*, Module*> m_mMemoryRegistry;

	CriticalSection m_csMainMemoryLock;

	unsigned int m_iActiveResources;
	
	unsigned int m_iNextID;
	inline int GetNextID() { return m_iNextID++; }

	template <typename T>
	bool ResolveDirtyResource(int dataSize, bool primaryDirty, T* primaryData, bool secondaryDirty, T* secondaryData)
	{
		if ((primaryDirty && secondaryDirty) || primaryDirty)
			memcpy(secondaryData, primaryData, dataSize);
		else if (secondaryDirty)
			memcpy(primaryData, secondaryData, dataSize);

		return primaryDirty || secondaryDirty;
	}

	CriticalSection m_csIsActiveWindow;
	bool m_bIsActiveWindow;
};

