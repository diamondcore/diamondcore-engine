#include "MemoryManager.h"
#include "Module.h"
#include "../Interfaces/BaseCore.h"
#include "../DataTypes.h"

#include <assert.h>

const unsigned int MemoryManager::MAX_ENTITIES = 500;

MemoryManager::MemoryManager(void)
{
	m_lPrimaryBuffer.resize(MAX_ENTITIES);

	m_iActiveResources = 0;

	m_iNextID = 0;

	//m_siSharedInput = new SharedInputResource();

	m_bIsActiveWindow = true;
}

void MemoryManager::SwapBuffer(void* pointerToCore)
{
	m_mMemoryRegistry[pointerToCore]->SwapBuffers();
}

void MemoryManager::SynchronizePrimaryBuffer(void* pointerToCore)
{
	auto numResources = m_mMemoryRegistry[pointerToCore]->GetActiveResourceCount();

	ListCopy(&m_lPrimaryBuffer, m_mMemoryRegistry[pointerToCore]->GetInactiveBuffer(), numResources);
}

std::list<SharedResource> MemoryManager::GetReadWriteBuffer(void* pointerToCore)
{
	return m_mMemoryRegistry[pointerToCore]->GetReadWriteBuffer();
}

int MemoryManager::CreateSharedResource()
{
	assert(m_iActiveResources + 1 < MAX_ENTITIES);

	auto it = m_lPrimaryBuffer.begin();
	std::advance(it, m_iActiveResources);

	// If this isnt -1, the math/indices got messed up somewhere.
	assert((*it).GetID() == -1);

	(*it).SetID(GetNextID());

	return m_iActiveResources++;
}

void MemoryManager::SynchronizeCores(BaseCore* primaryCore, BaseCore* secondaryCore)
{
	primaryCore->m_pSharedMemory->SwapBuffers();
	secondaryCore->m_pSharedMemory->SwapBuffers();

	auto primMod = primaryCore->m_pSharedMemory->GetInactiveBuffer();
	auto seconMod = secondaryCore->m_pSharedMemory->GetInactiveBuffer();

	auto it1 = primMod->begin();
	auto it2 = seconMod->begin();

	auto primCount = primaryCore->m_pSharedMemory->GetActiveResourceCount();
	auto seconCount = secondaryCore->m_pSharedMemory->GetActiveResourceCount();

	auto objectsUpdated = 0;
	auto primaryDirty = false;
	auto secondaryDirty = false;
	while(it1 != primMod->end() && it2 != seconMod->end())
	{
		if (!(*it1).ResourceIsDirty() && !(*it2).ResourceIsDirty())
			goto loop;

		ResolveDirtyResource<int>(sizeof(int), 
								 (*it1).IdDirty(), &(*it1).m_iEntityID, 
								 (*it2).IdDirty(), &(*it2).m_iEntityID);

		ResolveDirtyResource<Vector3>(sizeof(Vector3),
									 (*it1).PositionDirty(), &(*it1).m_vPosition, 
									 (*it2).PositionDirty(), &(*it2).m_vPosition);

		ResolveDirtyResource<Quaternion>(sizeof(Quaternion),
										(*it1).OrientationDirty(), &(*it1).m_qOrientation, 
										(*it2).OrientationDirty(), &(*it2).m_qOrientation);

		// If a module was dirty, then the other one
		// very likely got written to. Flag it for a buffer sync.
		if (!secondaryDirty && (*it1).ResourceIsDirty())
			secondaryDirty = true;

		if (!primaryDirty && (*it2).ResourceIsDirty())
			primaryDirty = true;

		(*it1).ResourceUpdated();
		(*it2).ResourceUpdated();

loop:
		objectsUpdated++;

		if (objectsUpdated >= primCount && objectsUpdated >= seconCount)
		{
			primaryCore->m_pSharedMemory->SetActiveObjects(objectsUpdated);
			secondaryCore->m_pSharedMemory->SetActiveObjects(objectsUpdated);
			break;
		}

		if (it1 != primMod->end())
			it1++;

		if (it2 != seconMod->end())
			it2++;
	}
	

	primaryCore->m_pSharedMemory->CopyBuffers();
	
	secondaryCore->m_pSharedMemory->CopyBuffers();
}

//void MemoryManager::SyncInputMemory() { m_siSharedInput->Synchronize(); }

void MemoryManager::FreeSharedResource(int id)
{
	m_csMainMemoryLock.Lock();
	//NOTE: Not thread safe. Should it be...?
	for (auto it = m_lPrimaryBuffer.begin(); it != m_lPrimaryBuffer.end(); it++)
	{
		if ((*it).GetID() != id)
			continue;

		// Clear and move it to the back.
		(*it).Clear();

		m_lPrimaryBuffer.push_back(*it);
		m_lPrimaryBuffer.erase(it);

		m_iActiveResources--;

		return;
	}
	m_csMainMemoryLock.Unlock();
}

void MemoryManager::ListCopy(std::list<SharedResource>* dst, std::list<SharedResource>* source, unsigned int count, unsigned int start)
{
	static int itemSize = sizeof(SharedResource);
	// TODO: Align CoreDataStruct so that it can be mem
	auto sourceIt = source->begin();
	auto dstIt = dst->begin();

	for (auto x = start; x < count; )
	{
		// Ray- This is seriously the hackiest solution I've writen in over 5 years.
		if (sourceIt->m_iEntityID != -1 && sourceIt->m_vPosition.z == 0 && dstIt->m_vPosition.z != 0)
			memcpy((&*sourceIt), (&*dstIt), itemSize);
		else
			memcpy((&*dstIt), (&*sourceIt), itemSize);

		dstIt++;
		sourceIt++;
		x++;
	}
}

inline void MemoryManager::Release(void* pointerToCore)
{
	delete m_mMemoryRegistry[pointerToCore];
	//delete m_siSharedInput;
}

MemoryManager::~MemoryManager(void)
{
	//TODO: Does this need to be done.
	for (auto it = m_mMemoryRegistry.begin(); it != m_mMemoryRegistry.end(); it++)
		delete &(*it).second;

	m_mMemoryRegistry.clear();
}