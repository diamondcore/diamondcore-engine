#pragma once

#include "../DataTypes.h"
#include "MemoryManager.h"
#include "../Multithreading/CriticalSection.h"

#include <list>
#include <algorithm>

class MainCore;

class Module
{
public:

	Module();
	~Module(void);
	
	friend class MemoryManager; // Allow MemoryManager access to private functions
	friend class MainCore;

	const static unsigned int MAX_ENTITIES;

	void Synchronize(std::list<SharedResource>* source, unsigned int size);

	inline std::list<SharedResource> GetReadWriteBuffer()
	{
		return m_lActiveBuffer;
	}

	inline int GetActiveResourceCount() { return m_iActiveObjects; }

	// access inside the module
	SharedResource* GetSharedResource(int resID)
	{
		for (auto it = m_lActiveBuffer.begin(); it != m_lActiveBuffer.end(); it++)
		{
			if ((*it).GetID() != resID)
			{
				if ((*it).GetID() == -1)
				{
					(*it).SetID(resID);
					return &(*it);
				}
				continue;
			}

			return &(*it);
		}

		// Resource didn't exist.
		throw;
	}

	// This shouldn't be called by the memory manager...
	// Make it a friend function later
	int CreateSharedResource()
	{
		auto id = GetNextID();
		// has to be m_iActiveObjects-1 because now using the 
		// post incremented activeobjects value
		auto it = m_lActiveBuffer.begin();
		std::advance(it, m_iActiveObjects);

		(*it).SetID(id);

		m_iActiveObjects++;

		return id;
	}

	void FreeSharedResource(int id)
	{

		for (auto it = m_lActiveBuffer.begin(); it != m_lActiveBuffer.end(); it++)
		{
			if ((*it).GetID() != id)
				continue;

			// Clear and move it to the back.
			(*it).Clear();

			m_lActiveBuffer.push_back(*it);
			m_lActiveBuffer.erase(it);

			m_iActiveObjects--;

			return;
		}

	}
	void SetActiveObjects(int newCount)
	{
		m_iActiveObjects = newCount;
	}

	unsigned int m_iNextID;
	inline int GetNextID() { return m_iNextID++; }

	MemoryManager* GetMemMgr(){return m_mMemManager;}
	void SetMemMgr(MemoryManager* mgr){m_mMemManager = mgr;}

private:

	void SwapBuffers();
	void CopyBuffers();
	inline std::list<SharedResource>* GetInactiveBuffer() { return &m_lInactiveBuffer; }
	std::list<SharedResource> m_lActiveBuffer;
	std::list<SharedResource> m_lInactiveBuffer;

	// I hate having items cross reference eachother... but this looks like the right way to go.
	MemoryManager* m_mMemManager;

	int m_iActiveObjects;
};

