#include "Module.h"

const unsigned int Module::MAX_ENTITIES = 500;

Module::Module()
{
	m_lActiveBuffer.resize(MAX_ENTITIES);

	m_iActiveObjects = 0;
	
	m_iNextID = 1;
}


void Module::Synchronize(std::list<SharedResource>* source, unsigned int size)
{
	MemoryManager::ListCopy(&m_lInactiveBuffer, source, size);
}

void Module::CopyBuffers()
{ 	
	MemoryManager::ListCopy(&m_lActiveBuffer, &m_lInactiveBuffer, m_iActiveObjects);
}

void Module::SwapBuffers()
{ 	
	std::swap(m_lActiveBuffer, m_lInactiveBuffer);
}

Module::~Module(void)
{
	m_lActiveBuffer.clear();
	m_lInactiveBuffer.clear();
}
