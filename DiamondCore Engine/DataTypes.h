#pragma once

enum CoreType
{
	CT_AI = 0,
	CT_GRAPHICS,
	CT_INPUT,
	CT_SOUND
};

#include <DCMathLib.h>

#include "Multithreading\CriticalSection.h"

class MemoryManager;
/// This class works differently. Ask Ray.

class SharedResource
{
public:

	SharedResource(void) :
		m_iEntityID(-1),
		m_vPosition(0.0f, 0.0f, 0.0f),
		m_qOrientation(),
		m_bPositionDirty(false),
		m_bIDDirty(false),
		m_bOrientationDirty(false)

	{
	}
	~SharedResource(void) {}


	friend class MemoryManager;

	inline bool OrientationDirty() { return m_bOrientationDirty; }
	inline bool PositionDirty() { return m_bPositionDirty; }
	inline bool IdDirty() { return m_bIDDirty; }
	inline bool ResourceIsDirty() { return m_bIDDirty || m_bPositionDirty || m_bOrientationDirty; }

	inline int GetID(){ return m_iEntityID; };
	void SetID(int newID)	// Pass in the buffer's(module's) critical section
	{
		if (newID == m_iEntityID)
			return;

		m_bIDDirty = true;
		m_iEntityID = newID;
	}

	inline Vector3 GetPosition() 
	{ 
		return m_vPosition; 
	};
	void SetPosition(Vector3 newPos)
	{
		if (newPos == m_vPosition)
			return;

		m_bPositionDirty = true;
		m_vPosition = newPos;
	}

	inline Quaternion GetOrientation() 
	{
		return m_qOrientation; 
	}
	void SetOrientation(Quaternion newOrientation)
	{
		if (newOrientation == m_qOrientation)
			return;

		m_bOrientationDirty = true;
		m_qOrientation = newOrientation;
	}	

	void Clear()
	{
		m_iEntityID = -1;
		m_vPosition.x = m_vPosition.y = m_vPosition.z = 0;

		m_qOrientation.w = 1;
		m_qOrientation.x = m_qOrientation.y = m_qOrientation.z = 0;

		m_bIDDirty = false;
		m_bOrientationDirty = false;
		m_bPositionDirty = false;
	}

private:

	

	void ResourceUpdated() { m_bIDDirty = m_bOrientationDirty = m_bPositionDirty = false; }


	int m_iEntityID;
	Vector3 m_vPosition;
	Quaternion m_qOrientation;
	bool m_bOrientationDirty;
	bool m_bPositionDirty;
	bool m_bIDDirty;
};


union MessageData
{
public:

	MessageData() {}

	char m_cCharData[128];
	int m_iIntData[32];
	float m_ifloatData[32];


	// Graphics Structs
	struct MeshParam
	{
		bool m_bUseShadowMap;
		char m_cMeshname[127];
		
	} m_mMeshData;

	struct MeshTexParam
	{
		bool m_bUseShadowMap;
		char m_cMeshname[63];
		char m_cTexname[64];
		
	} m_mMeshTexData;

	struct ScaleParam
	{
		float m_fScale[3];			// (x, y, z)
		char  m_cResourceName[116];
	} m_mScaleData;

	struct ColorParam		// Used when changing colors
	{
		float m_fColor[4];			// (r, g, b, a)
		char  m_cResourceName[112];	// Mesh, texture, animated mesh, font name
	} m_mColorData;

	struct AnimTrackParam
	{
		int		channel;
		float	weight;
		float	speed;
		float	priority;			// Range (0.0f to 1.0f)
		int		track;				// Use track or m_cAnimTrack, but not both
		char	m_cAnimTrack[108];	// If not using set it to ""
	} m_mAnimTrackData;

	// @Wes- Adjust the sizes here as you see fit.
	struct FontParam
	{
		char m_cFontType[32];
		char m_cText[96];
		
	} m_mFontData;

	// Graphics sends this message data as a result of a pick check
	struct QuadPicking
	{
		int	m_iPickedIDs[31];
		int m_iCount;
	} m_mQuadPickData;

	//Scripting Structs
	struct CoreData
	{
		char m_cCore[32];
		char m_cJavaScriptFile[96];
		
	} m_mCoreData;

	struct JsFunction
	{
		char m_cCore[32];
		char m_cFunctionName[96];
		
	} m_mJsFunction;

	struct FunctionData
	{
		int m_cCore;
		char m_cFunctionName[120];
		int  m_iArgCount ;
		
	} m_mFunctionData;

	// AI Structs
	struct AIEntity	//General entity creation/modification struct
	{
		bool m_bUsePotential;
		bool m_bUsePathfinding;
		int m_iID;				//Entity ID for modifying existing entities
		int m_iSharedID;		//Id of a shared memory element for the entity to use upon creation.  Setting to -1 will tell the entity to make its own 
		int m_iState;			//The Id of a state we want to set the entity to
		float m_fPosition[3];	//Used for setting the position of a new or existing entity
		float m_fVelocity[3];	//Used for setting the velocity of a new or existing entity
		float m_fTarget[3];		//Used for setting the target of an entity
		char m_cEntity[78];

	} m_mAIEntityData;

	struct PotentialData	//General Potential Element creation/modification struct
	{
		int m_iElementID;		//When modifying an element, set this to the ID# of the element you want to edit
		UINT m_iTeamID;			//Sets a team for the element.  Currently does not work, always set to 0
		float m_fPosition[3];	//Used for setting the position of a new or existing element
		float m_fStrength;		//Used for setting the potential strength when adding potential data to the element
		float m_fRange;			//Used for setting the potential range when adding potential data to the element.  Currently not used
		char m_cPotential[100];

	} m_mAIPotentialData;

	struct PotentialPriorities	//Struct for setting entity priorities in regards to maps in the potential system
	{
		int m_iAgentID;			//ID of the entity we want to modify
		UINT m_iMapNum;			//ID of the map we want to modify the priority to
		UINT m_iTeamNum;		//ID of the map team we want to modify the priority to.  Currently not used
		float m_fPriorityValue;	//New priority value
		char m_cPriority[102];

	} m_mAIPotentialPriority;

	struct PathingData	//Struct for getting a path for an entity
	{
		int m_iAgentID;				//The ID of the entity we want to get a path for
		float m_fDestination[3];	//The destination of the path
		float m_fMaxDistance;		//When finding a path, how far we are willing to go.  Setting to a negative number will assume no limit
		char m_cPathingData[108];

	} m_mAIPathingData;

	struct PathMapData	//General Nav Map creation/modification struct
	{
		bool m_bNodeToggle;			//When editing nodes, used to determine if they are going to be blocked off or made accessible
		int m_iMapSize;				//When creating the nav map, used to determine the size of the map
		int m_iMaxNumOfNodes;		//When creating the nav map, used to determine the node density of the map
		float m_fTogglePosition[3];	//When blocking off or exposing nodes, used to determine the point of origin
		float m_fToggleSize[3];		//When blocking off or exposing nodes, used to determine that area that will be modified
		char m_cPathMap[111];

	} m_mAIPathMapData;

	struct SteeringData	//General Steering force creation/destruction struct
	{
		int m_iForceType;		//When creating a force, what type of force we will be creating
		int m_iEntityID;		//When creating a force, the ID of the AI entity we want to assign it to
		UINT m_iRegistrationID;	//When creating a force, setting this to a unique value will allow us the force later
		float m_fTarget[3];		//When creating a force, the force target.
		float m_fPriority;		//When creating a force, determines when it is called when the entity updates all its forces.
		float m_fMagnitude;		//When creating a force, determines its strength.
		float m_fData[24];		//When creating a force, all extra data goes in here.
		bool m_bFiller[2];

	} m_mAISteeringData;

	struct SoundPlayOnceData
	{
		int m_iChannelGroup;
		bool m_bLoop;
		char m_cFileName[124];
		float m_cPos[3];

	} m_SoundPlayOnceData;

	struct SoundPlayBGMData
	{
		bool m_bIsLooping;
		bool m_bCrossFade;
		float m_fduration;
		char m_cFileName[122];

	} m_mSoundBGMData;
};

	
