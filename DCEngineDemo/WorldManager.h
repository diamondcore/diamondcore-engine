#pragma once
#include "FSM/FSM.h"
#include <DiamondCoreInclude.h>
#include "Game Objects\PhysicsEntity.h"
#include "FSM\FSM.h"
#include "StatTracker.h"



/*
	This is the Game World Manager. It handles all state changes
	within actual gameplay parts of the game. The screen-manager
	will call selected methods

	Init()
	Update(dt)
	Shutdown()

*/


enum Pod_Type
{
	MKIII_POD,
	MKIV_POD,
	X95_POD
};
enum Engine_Type
{
	MKIII_ENGINE,
	MKIV_ENGINE,
	X95_ENGINE
};// forward declarations

class LevelLoader;
class SaveManager;
class Module;
class HavokManager;
class Turbine;
class Terrain;
class IGameScreen;

#define GPM WorldManager::Instance()

class WorldManager
{
private:

	// singleton world manager - should never be two
	WorldManager(void);
	WorldManager(MainCore* &pMain);

	// pointer to the main core, allowing access to the engine
	MainCore* m_pMainCore;

	// pointer to the shared module for creating/deleting entities
	Module* m_pSharedMemory;

	// pointer to the finite state machine
	FiniteStateMachine<WorldManager>* m_pFSM;

	// level and asset loader
	LevelLoader* m_pLevelLoader;

	// save manager (passed in from app manager)
	SaveManager* m_pSaveManager;
	
	// player's turbine
	Turbine *m_pTurbine;

	// spawn point
	Vector3 m_vSpawnPosition;

	// spawn orientation
	Quaternion m_qSpawnOrientation;
	
	// Terrain Brush
	float	m_fBrushRadius;

	// is turbine dead
	bool m_bTurbineIsDead;

	// screen that is running the GPM
	IGameScreen* m_pGameScreen;

	// end game camera id
	int m_iEndGameCamID;

	// bool letting me know if stats are up
	bool m_bStatsShowing;

	
	// sounds
	std::vector<int> m_vIntroSounds;
	int m_iFinishSound;
	int m_iDeathSound;

	/////////////////
	// SCRATCH VARS
	////////////////
	int i,j,k;
	int boltObjID;
	float l,m,n;
	Vector3 vScratch;
	Quaternion qScratch;
	D3DXMATRIX mScratch;

public:
	
	static WorldManager* Instance();
	~WorldManager(void);

	// initializes the game world
	void Init(MainCore* &pMain);
	// performs all game logic checks, state changes, etc
	void Update(float dt);

	void HandleInput(IInputCore* inputState, float dt);
	// cleans up all created memory, leaving gameplay
	void Shutdown();




	/////////////////
	// UTILITIES
	/////////////////
	// loads the turbine and places at starting point
	void LoadTurbine(Pod_Type ePodType, Engine_Type eEngineType);
	// turbine died, placing back at starting position
	void RestartTurbine();
	// finish race method, for trigger volume
	void FinishRace();
	// throws up new screen similar to pause
	void Death();


	/////////////////
	// ACCESSORS
	/////////////////
	MainCore* GetEngine() {return m_pMainCore;}
	Vector3* GetPlayerPos();
	FiniteStateMachine<WorldManager>* GetFSM(){return m_pFSM;}
	Turbine* GetTurbine(){return m_pTurbine;}
	bool GetTurbineIsDead(){return m_bTurbineIsDead;}
	IGameScreen* GetScreen(){return m_pGameScreen;}
	int GetEndGameCamID(){return m_iEndGameCamID;}
	bool IsStatsShowing(){return m_bStatsShowing;}
	

	/////////////////
	// MUTATORS
	/////////////////
	void SetTurbineIsDead(bool bIsDead){m_bTurbineIsDead = bIsDead;}
	void SetScreen(IGameScreen* pScreen){m_pGameScreen = pScreen;}
	void SetEndGameCamID(int id){m_iEndGameCamID = id;}
	void SetStatsShowing(bool bValue){m_bStatsShowing = bValue;}
};

