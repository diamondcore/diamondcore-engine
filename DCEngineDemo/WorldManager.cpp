#include "WorldManager.h"
#include "FSM/FSM.h"
#include "Controls/ControlsInclude.h"
#include <Windows.h>
#include <dinput.h>
#include "FSM/LoadGameState.h"
#include "FSM/PreRaceState.h"
#include "FSM/RaceState.h"
#include "FSM/EndRaceState.h"
#include "../DCEngineDemo/UI/ScreenManager.h"
#include "../DCEngineDemo/Havok/HavokManager.h"
#include "Game Objects/Engine.h"
#include "Game Objects/Turbine.h"
#include "Game Objects/Terrain.h"
#include "Game Objects/Turret.h"
#include "Game Objects/Pod.h"
#include "Game Objects/SmokeTrap.h"
#include "../../DCEngineDemo/Havok/TriggerVolumes.h"
#include "UI/Screens/DeathScreen.h"
#include <random>
#include <time.h>
#include "DebugLogger.h"

#define GFX m_pMainCore->GetGraphicsCore()
#define AI m_pMainCore->GetAICore()
#define SND m_pMainCore->GetSoundCore()
#define MEM m_pSharedMemory
#define INP m_pMainCore->GetInputCore()
#define SND m_pMainCore->GetSoundCore()

WorldManager::WorldManager(void)
{
	assert ("Default World Manager Constructor Called. Call the Para Const.");
	m_pFSM = 0;
}

WorldManager::WorldManager(MainCore* &pMain)
{
	// init finite state machine
	//m_pFSM = new FiniteStateMachine<WorldManager>(this);
	m_pFSM = 0;
}

WorldManager* WorldManager::Instance()
{
	static WorldManager instance;

	return &instance;
}


WorldManager::~WorldManager(void)
{
}

// initializes the game world
void WorldManager::Init(MainCore* &pMain)
{	
	DbgLog->Write("\n\nInitializing World...");
	// init maincore pointer
	m_pMainCore = pMain;

	// setting ptr to shared mem
	m_pSharedMemory = pMain->m_mMainMemory;


	// Init FSM
	if(!m_pFSM)
	{
		m_pFSM = new FiniteStateMachine<WorldManager>(this);
		m_pFSM->SetInitialState(LOADGAME);
	}
	
	// Extra camera for testing
	int id = CamMgr->CreateCamera(FREECAM);
	CamMgr->GetCameraControlByID(id)->SetPosition(0, 8, -20);
	

	// Set the camera glare type
	GFX->SetCameraGlareType(10);
	GFX->UseSSAO(true);


	// initialize all level intro sounds
	m_vIntroSounds.push_back( SND->CreateInstance2D("Content/Sounds/Music/mb00aquilarisintro.wav",true,0,false) );
	m_vIntroSounds.push_back( SND->CreateInstance2D("Content/Sounds/Music/me00spiceintro.wav",true,0, false) );
	m_vIntroSounds.push_back( SND->CreateInstance2D("Content/Sounds/Music/mt01desert.wav",true,0,false) );
	m_vIntroSounds.push_back( SND->CreateInstance2D("Content/Sounds/Music/mx091lavacaves.wav",true,0,false) );

	// init the finish sounds
	m_iFinishSound = SND->CreateInstance2D("Content/Sounds/Music/m092didgood.wav",true,0,false) ;
	m_iDeathSound = SND->CreateInstance2D("Content/Sounds/Music/m032wipeout3.wav",true,0,false) ;
	
	SND->StopAll(0);
	SND->StopAll(1);

	// pick a random slot and play that intro
	int idx = rand() % m_vIntroSounds.size();
	SND->PlaySFX(m_vIntroSounds[idx],false,0);
	SND->PlaySFX(m_pTurbine->iStartupSound,false,0);



}

void WorldManager::LoadTurbine(Pod_Type ePodType, Engine_Type eEngineType)
{
	// Spawn the player
	DbgLog->Write("Getting Next Spawn Point");
	ScreenMan->GetEntityManager()->GetNextSpawnPoint(&m_vSpawnPosition, &m_qSpawnOrientation);
	//spawnPosition.y = 30.0f;
	std::string pPodType;
	std::string pEngineType;
	
	// determine which pod was selected
	switch (ePodType)
	{
	case MKIII_POD:
		pPodType = "MKIII_Pod.X";
		break;
	case MKIV_POD:
		pPodType = "MKIV_Pod.X";
		break;
	case X95_POD:
		pPodType = "X95_Pod.X";
		break;
	default:
		pPodType = "MKIII_Pod.X";
		break;
	};
	DbgLog->Write("Pod Type = " + pPodType);

	// determine which engine was selected
	switch (eEngineType)
	{
	case MKIII_ENGINE:
			pEngineType = "MKIII_Engine.X";
			break;
	case MKIV_ENGINE:
		pEngineType = "MKIV_Engine.X";
		break;
	case X95_ENGINE:
		pEngineType = "X95_Engine.X";
		break;
	default:
		pEngineType = "MKIII_Engine.X";
		break;
	};
	DbgLog->Write("Engine Type = " + pEngineType);
	DbgLog->Write("Creating Turbine...");

	// create turbine and set its pos/orientation
	m_pTurbine = new Turbine(m_vSpawnPosition,Vector3(-2,0,6),Vector3(2,0,6),Vector3(0,0,-1),Vector3(.5,.5,3),
		Vector3(.5,.5,3),Vector3(1,.25,1),&pPodType[0],&pEngineType[0]);
	DbgLog->Write("Orienting Turbine");
	m_pTurbine->SetOrientation(m_qSpawnOrientation);
	
	DbgLog->Write("Setting Active Cam Control to " + std::to_string((_Longlong)m_pTurbine->GetCamID()));
	// set the active cam to the player's vehicle
	CamMgr->SetActiveCamControl(m_pTurbine->GetCamID());
	DbgLog->Write("Done Creating Turbine");
}

// turbine died, placing back at starting position
void WorldManager::RestartTurbine()
{
	m_pTurbine->SetPosition(m_vSpawnPosition);
	m_pTurbine->Reset();
	m_pTurbine->SetOrientation(m_qSpawnOrientation);
}

// finish race method, for trigger volume
void WorldManager::FinishRace()
{

	SND->StopAll(0);
	SND->StopAll(1);

	SND->PlaySFX(m_iFinishSound, false, 0);

	//Increment the races Finished Stat
	SStats->SetNumRacesWon(1);

	// transition to the end race state
	m_pFSM->ChangeState(ENDRACE);

	

	// shutdown the turbine
	//m_pTurbine->Shutdown();


}
// throws up new screen similar to pause
void WorldManager::Death()
{
	SND->StopAll(0);
	SND->StopAll(1);

	SND->PlaySFX(m_iDeathSound,false,0);

	ScreenMan->AddScreen(new DeathScreen(m_pGameScreen) );

	SStats->WriteStatsToFile();
	
}
// performs all game logic checks, state changes, etc
void WorldManager::Update(float dt)
{
	CamMgr->Update(dt);

	m_pFSM->Update(dt);
		
}

void WorldManager::HandleInput(IInputCore* inputState, float dt)
{
	// testing damage
	/*if (inputState->IsNewKeyRelease(DIK_1) )
		m_pTurbine->ApplyDamage(10.0f,0,e_SelfDamage);

	if (inputState->IsNewKeyRelease(DIK_2) )
		m_pTurbine->ApplyDamage(10.0f,1,e_SelfDamage);

	if (inputState->IsNewKeyRelease(DIK_3) )
		m_pTurbine->ApplyDamage(10.0f,2,e_SelfDamage);

	if (inputState->IsNewKeyRelease(DIK_0) )
		FinishRace();*/

	m_pFSM->HandleInput(inputState, dt);
	

	// HandleInput returns void for now
	// but in the future it could be used in the same fashion
	// as the UI does.
	CamMgr->HandleInput(inputState, dt);

}

// cleans up all created memory, leaving gameplay
void WorldManager::Shutdown()
{
	if(m_pTurbine)
	{
		m_pTurbine->Shutdown();
		delete m_pTurbine;
		m_pTurbine = 0;
	}

	SND->StopAll(0);
	SND->StopAll(1);

	ScreenMan->GetEntityManager()->Shutdown();

	HM->Shutdown();

	if(m_pFSM)
	{
		m_pFSM->Shutdown();
		delete m_pFSM;
		m_pFSM = 0;
	}
}

Vector3* WorldManager::GetPlayerPos() 
{
	return m_pTurbine->GetPodPosition(); 
}

/*
enum ControllerButton
{
	DPAD_UP = 0x0001,
	DPAD_DOWN = 0x0002,
	DPAD_LEFT = 0x0004,
	DPAD_RIGHT = 0x0008,
	START = 0x0010,
	BACK = 0x0020,
	LEFT_STICK = 0x0040,
	RIGHT_STICK = 0x0080,
	LEFT_BUMPER = 0x0100,
	RIGHT_BUMPER = 0x0200,
	A  = 0x1000,
	B = 0x2000,
	X = 0x4000,
	Y = 0x8000,
};
*/

/*

//HM->Init();
	
	// create a resource
	//int id = MEM->CreateSharedResource();
	
	// give id to mesh
	//GFX->LoadTexture("tile010.jpg");
	//GFX->CreateMesh(id, "Box.x", true);
	//GFX->SetDefaultMeshTexture("Box.x", "tile010.jpg");
	//GFX->SetMeshColor(id, "Box.x", D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	//GFX->SetDefaultMeshLightAttributes("Box.x", 1.0f, 0.01f, 100.0f);

	//// Get Mesh Bounding Volume's Half Extents demo
	//Vector3 halfExtent;
	//GFX->GetMeshBV("Box.x", halfExtent);

	//// Create Barrel
	//id = MEM->CreateSharedResource();
	//GFX->CreateMesh(id, "barrel1.x", true);
	//m_pSharedMemory->GetSharedResource(id)->SetPosition(Vector3(0, 0, 0));
	//GFX->SetDefaultMeshLightAttributes("barrel1.x", 1.0f, 0.01f, 0.0f);

	// Create Room
	//id = MEM->CreateSharedResource();
	//GFX->CreateMesh(id, "Box.x", true);
	//GFX->SetMeshScale(id, "Box.x", D3DXVECTOR3(500.0f, 500.0f, 500.0f));
	//GFX->SetMeshColor(id, "Box.x", D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	
	//HM->AddFixedSurface(-1,0,-5,0,500,1,500,0,0,0,1);
	
	

	/*Quaternion temp(Vector3(0,1,0), 3.14f), temp2(Vector3(0,0,1),3.14f);*/
	
	///*
	//	LEFT ENGINE
	//*/
	//
	//id = MEM->CreateSharedResource();
	//MEM->GetSharedResource(id)->SetPosition(Vector3(2,0,6));
	//MEM->GetSharedResource(id)->SetOrientation(temp*temp2);
	//GFX->CreateMesh(id, "MKIII_Engine.X","Floor.bmp",false);
	//GFX->SetMeshScale(id, "MKIII_Engine.X", Vector3(.01,.01,.01) );
	//
	//
	//
	///*
	//	RIGHT ENGINE
	//*/
	//id = MEM->CreateSharedResource();
	//MEM->GetSharedResource(id)->SetPosition(Vector3(-2,0,6));
	//MEM->GetSharedResource(id)->SetOrientation(temp);
	//GFX->CreateMesh(id, "MKIII_Engine.X","Floor.bmp",false);
	//GFX->SetMeshScale(id, "MKIII_Engine.X", Vector3(.01,.01,.01) );
/*	
	auto object = std::shared_ptr<PhysicsEntity>(new PhysicsEntity(ET_PHYSICSOBJECT));
	object->SetMeshName("barrel1.x");
	object->SetRenderable(true);
	// Set Default Mesh attributes
	object->Init();
	object->SetPosition(-3.0f, 0.0f, -3.0f);
	GFX->SetDefaultMeshLightAttributes("barrel1.x", 1.0f, 0.01f, 0.0f);
	ScreenMan->GetEntityManager()->AddEntity(object->GetID(), object);

	// Box
	auto box = std::shared_ptr<PhysicsEntity>(new PhysicsEntity(ET_PHYSICSOBJECT));
	box->SetMeshName("Box.x");
	box->SetRenderable(true);
	// Set Default Mesh attributes
	box->Init();
	box->SetPosition(3.0f, 0.0f, -3.0f);
	GFX->SetDefaultMeshTexture("Box.x", "tile010.jpg");
	GFX->SetDefaultMeshLightAttributes("Box.x", 1.0f, 0.01f, 100.0f);
	ScreenMan->GetEntityManager()->AddEntity(box->GetID(), box);
	//CamMgr->SetActiveCamControl(id);
	
	// Demo the turret tracking 
/*	auto turret = std::shared_ptr<Turret>(new Turret());
	turret->SetColor(D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.0f));
	turret->Init();
	turret->SetPosition(-6.0f, -4.0f, -3.0f);
	turret->SetTarget(m_pTurbine->GetPodPosition());
	//turret->SetTarget((Vector3*)(&CamMgr->GetCameraControlByID(id)->GetPosition()));
	ScreenMan->GetEntityManager()->AddEntity(turret->GetID(), turret);

	// Demo the turret tracking 
	turret = std::shared_ptr<Turret>(new Turret());
	turret->SetColor(D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.0f));
	turret->Init();
	turret->SetPosition(6.0f, -4.0f, -3.0f);
	turret->SetTarget(m_pTurbine->GetPodPosition());
	//turret->SetTarget((Vector3*)(&CamMgr->GetCameraControlByID(id)->GetPosition()));
	ScreenMan->GetEntityManager()->AddEntity(turret->GetID(), turret);
*/
	///*
	//	POD
	//*/

// Demo on how to get texture information
	//D3DXIMAGE_INFO info;
	//GFX->GetImageInfo("tile010.jpg", info);	


	/////////////////
	// CAM MANAGER
	////////////////
	//CamMgr->CreateCamera(FREECAM);

	//CamMgr->SetPosition(0,5,-112);
	
	
