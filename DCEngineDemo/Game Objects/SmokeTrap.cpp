#include "SmokeTrap.h"
#include "../UI/ScreenManager.h"
#include "../WorldManager.h"
#include "Pod.h"
#include "Engine.h"

#define GFX ScreenMan->GetGraphics()
#define MEM ScreenMan->GetMainCore()->m_mMainMemory

SmokeTrap::SmokeTrap()
	:BaseGameEntity(ET_SMOKETRAP)
{
	m_pTarget		= 0;
	m_iSmokeID		= -1;
	m_vBurstDir		= Vector3(0.0f, 1.0f, 0.0f);
	m_fBurstPower	= 0.05f;
	m_fBurstRadius	= 5.0f;
	m_vTimerRange	= Vector2(5.0f, 10.0f);
	m_fTimer		= 0.0f;
	m_pMeshName		= "Sphere(10).X";
	m_bIsRenderable	= true;
	m_bSmokeCreated	= false;
	m_vColor		= D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	m_vScale		= Vector3(2.0f, 0.5f, 2.0f);
	m_pBody         = NULL;
	m_iBodyID       = -1;
	m_fCurrentForce = 0.0f;
}
SmokeTrap::SmokeTrap(std::string mesh)
	:BaseGameEntity(ET_SMOKETRAP)
{
	m_pTarget		= 0;
	m_iSmokeID		= -1;
	m_vBurstDir		= Vector3(0.0f, 1.0f, 0.0f);
	m_fBurstPower	= 0.05f;
	m_fBurstRadius	= 5.0f;
	m_vTimerRange	= Vector2(5.0f, 10.0f);
	m_fTimer		= 0.0f;
	m_pMeshName		= mesh;
	m_bIsRenderable	= true;
	m_bSmokeCreated	= false;
	m_pBody         = NULL;
	m_iBodyID       = -1;
	m_fCurrentForce = 0.0f;
	m_fDuration		= 0.0f;
}
SmokeTrap::~SmokeTrap()
{
}

void SmokeTrap::Update(float dt)
{
	// Do general update
	BaseGameEntity::Update(dt);

	// Do Trap update
	if((m_pBody || m_pTarget) && m_fTimer <= 0.0f)
	{
		// Add smoke graphics
		if(!m_bSmokeCreated)
		{
			GFX->CreateParticle(m_iSmokeID, "Geyser", 0);
			m_bSmokeCreated = true;
			m_fDuration = 6.5f;
			//GFX->SetMeshColor(m_ID, "Sphere(10).X", D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));	// Set vent color to red
		}

		// Turn off the physics force as the graphics smoke starts fading away
		if(m_fDuration > 0.5f)
		{
#if 1
			// Adjust power over time
			//if(m_fCurrentForce < m_fBurstPower)
			m_fCurrentForce = m_fBurstPower;//*(duration/m_vTimerRange.y); 
			((ForceVolume*)(HM->m_TriggerVolumes[m_iBodyID]))->m_bActive  = true;
#else
			// Check each part of the pod for intersection
			auto pod = m_pTarget->GetPod();
			auto left = m_pTarget->GetLeftEngine();
			auto right = m_pTarget->GetRightEngine();
		
			// Pod
			Vector3 toTarget = pod->GetPosition() - m_vPosition;
			if(toTarget.Magnitude() <= m_fBurstRadius)
			{
				HM->ApplyForce(pod->GetBody()->getUid(), dt, m_vBurstDir.x*m_fBurstPower, m_vBurstDir.y*m_fBurstPower, m_vBurstDir.z*m_fBurstPower,
					m_vPosition.x, m_vPosition.y, m_vPosition.z);
			}

			// Left Engine
			toTarget = left->GetPosition() - m_vPosition;
			if(toTarget.Magnitude() <= m_fBurstRadius)
			{
				HM->ApplyForce(left->GetBody()->getUid(), dt, m_vBurstDir.x*m_fBurstPower, m_vBurstDir.y*m_fBurstPower, m_vBurstDir.z*m_fBurstPower,
					m_vPosition.x, m_vPosition.y, m_vPosition.z);
			}

			// Right Engine
			toTarget = right->GetPosition() - m_vPosition;
			if(toTarget.Magnitude() <= m_fBurstRadius)
			{
				HM->ApplyForce(right->GetBody()->getUid(), dt, m_vBurstDir.x*m_fBurstPower, m_vBurstDir.y*m_fBurstPower, m_vBurstDir.z*m_fBurstPower,
					m_vPosition.x, m_vPosition.y, m_vPosition.z);
			}
#endif
		}
		else
			((ForceVolume*)(HM->m_TriggerVolumes[m_iBodyID]))->m_bActive  = false;
				
		// Count down duration
		if(m_fDuration <= 0.0f)
		{
			// Reset the timer 
			m_fTimer = (float)(rand() % (int)m_vTimerRange.y) + m_vTimerRange.x;
		}
		else
			m_fDuration -= dt;
	}
	else
	{
		// Remove the graphics object if its still there
		if(m_bSmokeCreated)
		{
			GFX->RemoveParticle(m_iSmokeID);
			m_bSmokeCreated = false;
			//GFX->SetMeshColor(m_ID, "Sphere(10).X", m_vColor);	// Reset color of vent
		}
	}
	m_fTimer -= dt;
}

void SmokeTrap::Load(TiXmlElement* node)
{
	// Gather and Set object information
	BaseGameEntity::Load(node);
	
	// Load smoke trap information
	node->QueryFloatAttribute("BurstPower", &m_fBurstPower);

	TiXmlElement* timer;
	timer = node->FirstChildElement("Timer")->ToElement();
	timer->QueryFloatAttribute("min", &m_vTimerRange.x);
	timer->QueryFloatAttribute("range", &m_vTimerRange.y);

}
void SmokeTrap::Save(TiXmlElement* obj)
{
	// Create the base object info
	BaseGameEntity::Save(obj);
	
	// Save Turret information
	obj->SetDoubleAttribute("BurstPower", m_fBurstPower);

	// Create burst timer min and max
	TiXmlElement* timer = new TiXmlElement("Timer");
	timer->SetDoubleAttribute("min", m_vTimerRange.x);
	timer->SetDoubleAttribute("range", m_vTimerRange.y);
	obj->LinkEndChild(timer);	// Link attribute to the object
}

std::shared_ptr<BaseGameEntity>	SmokeTrap::Clone()	// Clone these attributes into a new BaseGameEntity
{
	// Check if we've reached our limit on copies
	if(m_iNumCopies >= m_iMaxNumCopies)
		return 0;

	// Create clone
	std::shared_ptr<SmokeTrap> clone = std::shared_ptr<SmokeTrap>(new SmokeTrap());
	
	// Set all the attributes for the clone that are not unique to a specific instance.
	// This will be used to create instances of this object in the level designer.
	// Brief example:
	clone->SetMeshName(&m_pMeshName[0]);
	if(!m_pTextureName.empty())
		clone->SetTextureName(&m_pTextureName[0]);
	clone->SetRenderable(m_bIsRenderable);
	clone->SetColor(m_vColor);
	clone->SetScale(m_vScale);
	clone->SetOffset(m_vOffset);

	// Set Attributes particular to SmokeTrap
	clone->m_vTimerRange = m_vTimerRange;
	clone->m_fBurstPower = m_fBurstPower;
	
	// Setup the copy counter to point to the original counter
	clone->m_pNumCopies = &m_iNumCopies;
	m_iNumCopies++;

	// Initialize the object
	clone->Init();
	return clone;
}

void SmokeTrap::Init()
{
	BaseGameEntity::Init();
	
	// Set up the burst direction with this function
	SetOrientation(m_qOrientation);

	// Create shared memory for the graphics smoke particles
	m_iSmokeID = MEM->CreateSharedResource();
	MEM->GetSharedResource(m_iSmokeID)->SetPosition(m_vPosition);
	MEM->GetSharedResource(m_iSmokeID)->SetOrientation(m_qOrientation);

	// Check if we are in a mode that has Havok initialized
	if(!ScreenMan->GetEntityManager()->DisplayHiddenObjects())
	{
		// Set the player
		m_pTarget = GPM->GetTurbine();

		// Set trigger volume
		Vector3 halfExtent;
		ScreenMan->GetGraphics()->GetMeshBV(&m_pMeshName[0], halfExtent);
		halfExtent = halfExtent.ComponentProduct(m_vScale);	// Scale the halfExtent by the specified scale
		halfExtent.y *= 10.0f;

		// init body here w/ havok manager
		m_pBody = HM->CreateTriggerVolume(-1,_Force, m_vPosition.x, m_vPosition.y, m_vPosition.z, 
											//m_fBurstRadius*0.5f, halfExtent.y, m_fBurstRadius*0.5f);
											halfExtent.x, halfExtent.y, halfExtent.z);

		// set body id
		m_iBodyID = m_pBody->getUid();

		// Set body attributes
		HM->SetBodyPosition(m_pBody, m_vPosition.x, m_vPosition.y, m_vPosition.z);
		Vector3 rot = m_qOrientation.GetYawPitchRoll();
		HM->RotateBody(m_pBody, rot.y, rot.x, rot.z);

		HM->m_TriggerVolumes[m_iBodyID]->m_pParam = &m_fCurrentForce;
	}
	m_fTimer = (float)(rand() % (int)m_vTimerRange.y) + m_vTimerRange.x;
}

void SmokeTrap::Shutdown()
{
	BaseGameEntity::Shutdown();

	// Remove smoke graphics object
	if(m_iSmokeID >= 0)
	{
		if(m_bSmokeCreated)
			GFX->RemoveParticle(m_iSmokeID);
		MEM->FreeSharedResource(m_iSmokeID);
		m_iSmokeID = -1;
		m_bSmokeCreated = false;
	}

	// Remove body if it exists
	if(m_pBody)
	{
		HM->RemoveTriggerVolume(m_iBodyID);
		m_iBodyID = -1;
		m_pBody = 0;
	}
}

// Sets entity's orientation
void SmokeTrap::SetOrientation(Quaternion pOrientation)
{
	// Restrict rotation to yaw only
	pOrientation.Normalize();
	m_qOrientation = pOrientation;
	
	// Set the burst direction with the quaternion
	D3DXMATRIX O;
	D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
	D3DXVec3TransformNormal(&m_vBurstDir, &Vector3(0.0f, 1.0f, 0.0f), &O);
	m_vBurstDir.Normalize();
}
// sets entity's orientation by yaw/pitch/roll
void SmokeTrap::SetOrientation(float yaw, float pitch, float roll)
{
	D3DXQuaternionRotationYawPitchRoll(&qScratch, yaw, pitch,roll);
	SetOrientation(qScratch);
}
