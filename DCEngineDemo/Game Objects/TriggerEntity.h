#pragma once
#include "BaseGameEntity.h"
#include "../Havok/HavokManager.h"

/*
	Physics entity - has havok body, etc

*/

class TriggerEntity : public BaseGameEntity
{
protected:
	// entity's rigid body
	hkpRigidBody* m_pBody;

	// rigid body's ID
	int m_iBodyID;

    TriggerType m_tType;
	
	/////////////////
	// SCRATCH VARS
	////////////////
	hkVector4 hkvScratch;
	hkQuaternion hkqScratch;

public:
	TriggerEntity(void);
    TriggerEntity(TriggerType tType);
    TriggerEntity(EntityType eType,TriggerType tType);
	TriggerEntity(EntityType eType, char* pMeshName , 
		char* pTexName = " ", Vector3 vPodGFXScale = Vector3 (1,1,1) );
	virtual ~TriggerEntity(void);
	
	// compare entities
	virtual bool operator== (const BaseGameEntity& entity);

	//////////////////////
	// UTILITIES
	/////////////////////
	// update and sync graphics,physics, and game logic
	virtual void Update(float dt);
	virtual void Load(TiXmlElement* node);		// Loads the attributes
	virtual void Save(TiXmlElement* parent);	// Call this original function at the begining of the function that overwrites this
	virtual std::shared_ptr<BaseGameEntity>	Clone();	// Clone these attributes into a new BaseGameEntity
	virtual void Init();						// called at the end of load
	virtual void Shutdown();					// cleans the entity

	//////////////////////
	// ACCESSORS
	/////////////////////
	// returns entity's body
	hkpRigidBody* GetBody(){return m_pBody;}

	//////////////////////
	// MUTATORS
	/////////////////////
	// Sets entity's body
	void SetBody(hkpRigidBody* pBody){m_pBody = pBody;}
	// Sets position of entity 
	virtual void SetPosition(Vector3 vPos);
	// Sets position of entity by component
	virtual void SetPosition(float x, float y, float z);
	// sets a single component of the entity's position
	virtual void SetPosition(int idx, float fValue);
	// sets position of entity with a havok vec4
	virtual void SetPosition(const hkVector4 &vPos);
	// Sets entity's orientation
	virtual void SetOrientation(Quaternion pOrientation){m_qOrientation = pOrientation;}
	// sets entity's orientation by yaw/pitch/roll
	virtual void SetOrientation(float yaw, float pitch, float roll);
	// set entity's orientation from hkQuaternion
	virtual void SetOrientation(const hkQuaternion* qOrientation);
	// rotates entity's position by yaw/pitch/roll
	virtual void RotateBody(float yaw, float pitch, float roll);
	// sets the entity's transform from havok body
	void SetTransform(const hkTransform* mTransform);
};

