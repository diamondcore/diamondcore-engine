#pragma once

#include "BaseGameEntity.h"

class Laser;

class Turret	: public BaseGameEntity
{
	Vector3*	m_pTarget;
	Vector3		m_vDirection;
	float		m_fFOV;
	float		m_fDetectionDist;

	// Laser Attributes
	Laser*		m_pLeftLaser;
	Laser*		m_pRightLaser;
	Vector3		m_vLeftLaser;
	Vector3		m_vRightLaser;
	Vector3		m_vLaserDir;

	// Barrel Attributes
	int			m_iBarrelID;
	Vector3		m_vBarrelDir;
	Vector3		m_vBarrelPos;
	Vector3		m_vBarrelOffset;
	Quaternion	m_qBarrelOrientation;
	float		m_fBarrelLength;
	std::string m_cBarrelMesh;
	
	// Turret Swival Attributes
	int			m_iSwivalID;
	Vector3		m_vSwivalOffset;
	std::string m_cSwivalMesh;

public:
	Turret();
	Turret(std::string turretMesh, std::string swivalMesh, std::string barrelMesh);
	~Turret();

	//////////////////////
	// UTILITIES
	/////////////////////
	// update and sync graphics,physics, and game logic
	virtual void Update(float dt);
	virtual void Load(TiXmlElement* node);		// Loads the attributes
	virtual void Save(TiXmlElement* obj);	// Call this original function at the begining of the function that overwrites this
	virtual std::shared_ptr<BaseGameEntity>	Clone();	// Clone these attributes into a new BaseGameEntity
	virtual void Init();						// called at the end of load
	virtual void Shutdown();					// cleans the entity
	
	//////////////////////
	// MUTATORS
	/////////////////////
	// Sets entity's orientation
	virtual void SetOrientation(Quaternion pOrientation);
	// sets entity's orientation by yaw/pitch/roll
	virtual void SetOrientation(float yaw, float pitch, float roll);
	//sets entity's scale
	virtual void SetScale(Vector3 scale);
	//set Target to detect
	void SetTarget(Vector3* pos){ m_pTarget = pos; }
	//set Field Of View
	void SetFOV(float fov){ m_fFOV = fov*0.5f; }
	//set the max detection distance
	void SetDetectionDistance(float dist){ m_fDetectionDist = dist; }
};

// This is the laser beam projectile
class Laser
{
	std::string	m_cMeshName;
	int			m_iID;
	Vector3*	m_vStartPos;
	Vector3*	m_vEndPos;
	Vector3*	m_vDirection;
	Quaternion*	m_qOrientation;
	float		m_fLength;
	Vector3		m_vScale;

public:
	Laser(Vector3* start, Vector3* end, Vector3* direction, Quaternion* orientation, std::string mesh = std::string("Cylinder.X"));
	~Laser();

	void	Update(float dt);

	void	Init();
	void	Shutdown();

	void	SetColor(Vector4 color);
	void	SetScale(Vector3 scale);
	void	SetStart(Vector3* start){m_vStartPos = start;}
	void	SetEnd(Vector3* end){m_vEndPos = end;}
};