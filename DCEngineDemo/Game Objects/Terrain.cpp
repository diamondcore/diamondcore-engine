#include "Terrain.h"
#include "../Havok/HavokManager.h"

Terrain::Terrain(Vector3 vertList[], WORD indexList[], Vector2 dimentions, Vector2 size)
{
	m_vMaxPos = size*0.5f;
	HM->CreateTerrain(vertList, indexList, dimentions, size, &mHeightField, &m_pBody);
}

bool Terrain::IsPosValid(Vector3 pos)
{
	// Simple calculation for determining if we are outside the level
	if(pos.x < 0.0f)
		pos.x *= -1.0f;
	if(pos.z < 0.0f)
		pos.z *= -1.0f;
	if(pos.x >= m_vMaxPos.x || pos.z >= m_vMaxPos.y)
		return false;
	return true;
}

float Terrain::GetHeightAt(Vector3 pos)
{
	if(	!IsPosValid(pos) )
		return FLT_MIN;
	
	// TODO: This can likely be optimized
	auto xform = m_pBody->getTransform();	
	xform.setInverse(xform);

	auto entPos = hkTransform();
	entPos.setIdentity();
	entPos.setTranslation(hkVector4(pos.x, pos.y, pos.z));

	// Transform world to local
	xform.setMulEq(entPos);

	hkIntUnion64 out;
	hkVector4Util::convertToUint16(xform.getTranslation(), mHeightField->m_floatToIntOffsetFloorCorrected, mHeightField->m_floatToIntScale, out );
	
	return mHeightField->getHeightAt(out.u16[0] , out.u16[2]);
}

Terrain::~Terrain(void)
{
	mHeightField->removeReference();
	m_pBody->removeReference();
}
