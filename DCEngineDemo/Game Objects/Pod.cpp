#include "Pod.h"
#include "../UI/ScreenManager.h"

#define GFX ScreenMan->GetGraphics()
#define MEM ScreenMan->GetMainCore()->m_mMainMemory

Pod::Pod(void)
{
}

Pod::Pod(Vector3 vOffset, Vector3 vHalfLengths, char* pMeshName )
	:PhysicsEntity(ET_POD, pMeshName, "")
{
	m_pMeshName = pMeshName;

	m_vOffset = vOffset;

	m_vPosition = Vector3(0,0,0) + vOffset;

	m_vHalfLengths = vHalfLengths;

	// init body here w/ havok manager
	m_pBody = HM->AddMovingBox(-1,m_vPosition.x, m_vPosition.y, m_vPosition.z,
				m_vHalfLengths.x, m_vHalfLengths.y, m_vHalfLengths.z, 10);

	// set body id
	m_iBodyID = m_pBody->getUid();

	// init orientation
	m_qOrientation = Quaternion(1,0,0,0);

	// by default mesh will be arrow 
	//GFX->CreateMesh(m_ID, &m_pMeshName[0],false);
	//GFX->SetMeshScale(m_ID, &m_pMeshName[0], Vector3(m_vHalfLengths.x * 2, m_vHalfLengths.y * 2, m_vHalfLengths.z * 2) );
	GFX->SetMeshScale(m_ID,&m_pMeshName[0], Vector3(.005f, .005f, .005f) );
	MEM->GetSharedResource(m_ID)->SetPosition(m_vPosition);
	MEM->GetSharedResource(m_ID)->SetOrientation(m_qOrientation);
	
	HM->SetBodyPosition(m_pBody, m_vPosition.x, m_vPosition.y, m_vPosition.z);
	HM->SetBodyOrientation(m_pBody, 0,0,0);
	SetAngDamining(1.0f);	// Set damping on pod rotation

	// initialize health
	InitHealth();
}

Pod::~Pod(void)
{
}

void Pod::Init()
{
	PhysicsEntity::Init();

	// Set graphics specific attributes
	//SetColor(D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f));
	//GFX->SetMeshColor(m_ID, &m_pMeshName[0], m_vColor);
	//GFX->SetDefaultMeshTexture(&m_pMeshName[0], &m_pTextureName[0]);
	//GFX->SetDefaultMeshLightAttributes(&m_pMeshName[0], 4.0f, 0.01f, 10.0f);
	GFX->SetDefaultMeshLightAttributes(&m_pMeshName[0], 1.0f, 1.0f, 0.0f);
}

void Pod::InitHealth()
{
	if (strcmp(&m_pMeshName[0], "MKIII_Pod.X") == 0 )
	{
		m_fMaxHealth = m_fHealth = MKIII_POD_HEALTH;
		m_fMaxEnergy = m_fCurrentEnergy = MKIII_POD_ENERGY;
	}
	else if (strcmp(&m_pMeshName[0], "MKIV_Pod.X") == 0 )
	{
		m_fMaxHealth = m_fHealth = MKIV_POD_HEALTH;
		m_fMaxEnergy = m_fCurrentEnergy = MKIV_POD_ENERGY;
	}
	else if (strcmp(&m_pMeshName[0], "X95_Pod.X") == 0 )
	{
		m_fMaxHealth = m_fHealth = X95_POD_HEALTH;
		m_fMaxEnergy = m_fCurrentEnergy = X95_POD_ENERGY;
	}
	else
	{

	}


}

void Pod::Shutdown()
{
	// Remove the particle
	GFX->RemoveParticle(m_ID);
	
	PhysicsEntity::Shutdown();
}

void Pod::ApplyDamage(float damage)
{
	float newHealth = m_fHealth - damage;
	if(m_fHealth >  m_fMaxHealth * .50f && newHealth <=  m_fMaxHealth * .50f)
	{
		// Health just fell below 50%
		// Create sparks coming from the pod
		GFX->CreateParticle(m_ID, "SmallSpark", 0);
	}
	else if(m_fHealth <  m_fMaxHealth * .50f && newHealth >=  m_fMaxHealth * .50f)
	{
		// Health just increased above 50%
		// Remove sparks from the pod
		GFX->RemoveParticle(m_ID);
	}
	m_fHealth = newHealth;
}

void Pod::ResetAllDamage()
{
	GFX->RemoveParticle(m_ID);
}

void Pod::UseEnergy(float energy)
{
	float newEnergy = m_fCurrentEnergy - energy;
	if(energy >= 0.0f && energy <= m_fMaxEnergy)
		m_fCurrentEnergy = newEnergy;
}
