#include "LightningTrap.h"
#include "../UI/ScreenManager.h"
#include "../WorldManager.h"
#include "../Havok/HavokManager.h"
#include "../EntityManager.h"
#include "Pod.h"
#include "Engine.h"

#define GFX ScreenMan->GetGraphics()
#define MEM ScreenMan->GetMainCore()->m_mMainMemory

#define ELECTRICDAMAGE_PHYSICAL 15
#define ELECTRICDAMAGE_POWER -10
#define ELECTRICDAMAGE_RANGE 10
#define BOLT_OFFSET Vector3(0, -105, 0)

LightningTrap::LightningTrap()
	:BaseGameEntity(ET_LIGHTNINGTRAP)
{
	m_pTarget			= NULL;
	m_iLightningBoltID	= -1;
	m_vTimerRange		= Vector2(100.0f, 20.0f);
	m_fTimer			= 0.0f;
	m_fDamageRange		= ELECTRICDAMAGE_RANGE;
	m_pMeshName			= "Sphere(10).X";
	m_bIsRenderable		= true;
	m_bLightningOn		= false;
	m_vColor			= D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	m_vScale			= Vector3(1.0f, 1.0f, 1.0f);

	m_pConnectPoint		= std::make_shared<LightningTrapMast>(LightningTrapMast());
	m_pConnectPoint->SetParent(this);
	m_vBoltOffset = BOLT_OFFSET;

	m_fLightningPower = ELECTRICDAMAGE_POWER;
}

LightningTrap::LightningTrap(std::string mesh)
	:BaseGameEntity(ET_LIGHTNINGTRAP)
{
	m_pTarget			= NULL;
	m_iLightningBoltID	= -1;
	m_vTimerRange		= Vector2(100.0f, 20.0f);
	m_fTimer			= 0.0f;
	m_fDamageRange		= ELECTRICDAMAGE_RANGE;
	m_pMeshName			= mesh;
	m_bIsRenderable		= true;
	m_bLightningOn		= false;
	m_vColor			= D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	m_vScale			= Vector3(1.0f, 1.0f, 1.0f);
	m_pConnectPoint		= std::make_shared<LightningTrapMast>(LightningTrapMast(mesh));
	m_pConnectPoint->SetParent(this);
	m_vBoltOffset = BOLT_OFFSET;
	m_fLightningPower = ELECTRICDAMAGE_POWER;
}

LightningTrap::~LightningTrap()
{
}

float LightningTrap::getDistance(Vector3 pos)
{
	Vector3 v = m_pConnectPoint->GetBoltOffset() - m_vBoltOffset;
	Vector3 w = pos - m_vBoltOffset;

	float b;
	float c1 = w * v;
	if(c1 <= 0)
	{
		b = (pos - m_vBoltOffset).Magnitude();
		return b;
	}
	float c2 = v * v;
	if(c2 <= c1)
	{
		b = (pos - m_pConnectPoint->GetBoltOffset()).Magnitude();
		return b;
	}
	b = c1 / c2;
	Vector3 p = m_vBoltOffset + v * b;
	b = (pos - p).Magnitude();
	return b;
}

void LightningTrap::SetPosition(Vector3 pos)
{
	//Vector3 temp = pos - m_vPosition;
	m_vPosition = pos;

	D3DXMATRIX m(m_mTransform);
	D3DXMatrixTransformation(&m, 0, 0, &m_vScale, 0, &m_qOrientation, &m_vPosition);
	//D3DXMatrixScaling(&m, scale.x, scale.y, scale.z);
	D3DXMatrixInverse(&m, 0, &m);

	D3DXMATRIX temp;
	D3DXMatrixTransformation(&temp, 0, 0, &Vector3(1, 1, 1), 0, 0, &BOLT_OFFSET);

	D3DXMatrixMultiply(&m, &m, &temp);
	D3DXMatrixInverse(&m, 0, &m);

	m_vBoltOffset = Vector3(m._41, m._42, m._43);

	if(m_pConnectPoint)
	{
		//m_pConnectPoint->SetPosition(m_pConnectPoint->GetPosition() + temp);
		SetBolt();
	}
	
}

void LightningTrap::SetOrientation(Quaternion pOrientation)
{
	pOrientation.Normalize();
	m_qOrientation = pOrientation;

	D3DXMATRIX m(m_mTransform);
	D3DXMatrixTransformation(&m, 0, 0, &m_vScale, 0, &m_qOrientation, &m_vPosition);
	//D3DXMatrixScaling(&m, scale.x, scale.y, scale.z);
	D3DXMatrixInverse(&m, 0, &m);

	D3DXMATRIX temp;
	D3DXMatrixTransformation(&temp, 0, 0, &Vector3(1, 1, 1), 0, 0, &BOLT_OFFSET);

	D3DXMatrixMultiply(&m, &m, &temp);
	D3DXMatrixInverse(&m, 0, &m);

	m_vBoltOffset = Vector3(m._41, m._42, m._43);

	SetBolt();
}

void LightningTrap::SetOrientation(float yaw, float pitch, float roll)
{
	D3DXQuaternionRotationYawPitchRoll(&qScratch, yaw, pitch,roll);
	SetOrientation(qScratch);
}

void LightningTrap::SetScale(Vector3 scale)
{
	m_vScale = scale;
	D3DXMATRIX m(m_mTransform);
	D3DXMatrixTransformation(&m, 0, 0, &m_vScale, 0, &m_qOrientation, &m_vPosition);
	//D3DXMatrixScaling(&m, scale.x, scale.y, scale.z);
	D3DXMatrixInverse(&m, 0, &m);

	D3DXMATRIX temp;
	D3DXMatrixTransformation(&temp, 0, 0, &Vector3(1, 1, 1), 0, 0, &BOLT_OFFSET);

	D3DXMatrixMultiply(&m, &m, &temp);
	D3DXMatrixInverse(&m, 0, &m);

	m_vBoltOffset = Vector3(m._41, m._42, m._43);

	if(m_pConnectPoint)
	{	
		m_pConnectPoint->m_vScale = scale;
		D3DXMATRIX m(m_mTransform);
		D3DXMatrixTransformation(&m, 0, 0, &m_pConnectPoint->m_vScale, 0, &m_pConnectPoint->m_qOrientation, &m_pConnectPoint->m_vPosition);
		//D3DXMatrixScaling(&m, scale.x, scale.y, scale.z);
		D3DXMatrixInverse(&m, 0, &m);

		D3DXMATRIX temp;
		D3DXMatrixTransformation(&temp, 0, 0, &Vector3(1, 1, 1), 0, 0, &BOLT_OFFSET);

		D3DXMatrixMultiply(&m, &m, &temp);
		D3DXMatrixInverse(&m, 0, &m);

		m_pConnectPoint->m_vBoltOffset = Vector3(m._41, m._42, m._43);
		SetBolt();
		GFX->SetMeshScale(m_pConnectPoint->m_ID, &m_pConnectPoint->m_pMeshName[0], m_pConnectPoint->m_vScale);
	}
	GFX->SetMeshScale(m_ID, &m_pMeshName[0], m_vScale);
}

void LightningTrap::SetMeshName(char* pMeshName)
{
	m_pMeshName = pMeshName;
	if(m_pConnectPoint)
		m_pConnectPoint->SetMeshName(pMeshName);
}

void LightningTrap::SetBolt()
{
	GFX->SetBoltObjPositions(m_iLightningBoltID, m_vBoltOffset, m_pConnectPoint->GetBoltOffset());
}

void LightningTrap::Update(float dt)
{
	BaseGameEntity::Update(dt);

	if(m_pConnectPoint)
		m_pConnectPoint->Update(dt);

	m_fTimer++;
	if(m_bLightningOn)
	{
		if(m_fTimer >= m_vTimerRange.y)
		{
			m_bLightningOn = !m_bLightningOn;
			m_fTimer -= m_vTimerRange.y;
			GFX->SetBoltObjVisibility(m_iLightningBoltID, m_bLightningOn);
		}
		else if(!ScreenMan->GetEntityManager()->DisplayHiddenObjects())
		{
			m_pTarget = GPM->GetTurbine();
			if(m_pTarget)
			{
				static float damage;
				bool isContact = false;

				if(getDistance(m_pTarget->GetPod()->GetPosition()) <= m_fDamageRange)
				{
					m_pTarget->ApplyDamage(ELECTRICDAMAGE_PHYSICAL * dt, 0, e_ElectricDamage);
					isContact = true;
				}
				if(getDistance(m_pTarget->GetLeftEngine()->GetPosition()) <= m_fDamageRange)
				{
					m_pTarget->ApplyDamage(ELECTRICDAMAGE_PHYSICAL * dt, 1, e_ElectricDamage);
					isContact = true;
				}
				if(getDistance(m_pTarget->GetRightEngine()->GetPosition()) <= m_fDamageRange)
				{
					m_pTarget->ApplyDamage(ELECTRICDAMAGE_PHYSICAL * dt, 1, e_ElectricDamage);
					isContact = true;
				}
				if(isContact)
				{
					m_pTarget->ModifyCurrentEnergy(ELECTRICDAMAGE_POWER * dt);

				}
			}
		}
	}
	else
	{
		if(m_fTimer >= m_vTimerRange.x)
		{
			m_bLightningOn = !m_bLightningOn;
			m_fTimer -= m_vTimerRange.x;
			GFX->SetBoltObjVisibility(m_iLightningBoltID, m_bLightningOn);
		}
	}
		
}

void LightningTrap::Load(TiXmlElement* node)
{
	BaseGameEntity::Load(node);
	node->QueryFloatAttribute("LightningPower", &m_fLightningPower);

	TiXmlElement* timer;
	timer = node->FirstChildElement("Timer")->ToElement();
	timer->QueryFloatAttribute("chargeTime", &m_vTimerRange.x);
	timer->QueryFloatAttribute("dischargeTime", &m_vTimerRange.y);
	Vector3 v;
	timer->QueryFloatAttribute("connectPosX", &v.x);
	timer->QueryFloatAttribute("connectPosY", &v.y);
	timer->QueryFloatAttribute("connectPosZ", &v.z);
	m_pConnectPoint->SetPosition(v);
	timer->QueryFloatAttribute("connectScaleX", &v.x);
	timer->QueryFloatAttribute("connectScaleY", &v.y);
	timer->QueryFloatAttribute("connectScaleZ", &v.z);
	m_pConnectPoint->SetScale(v);
	Quaternion q;
	timer->QueryFloatAttribute("connectRotW", &q.w);
	timer->QueryFloatAttribute("connectRotX", &q.x);
	timer->QueryFloatAttribute("connectRotY", &q.y);
	timer->QueryFloatAttribute("connectRotZ", &q.z);
	m_pConnectPoint->SetOrientation(q);
	timer->QueryFloatAttribute("thisboltOffsetX", &v.x);
	timer->QueryFloatAttribute("thisboltOffsetY", &v.y);
	timer->QueryFloatAttribute("thisboltOffsetZ", &v.z);
	m_vBoltOffset = v;
	timer->QueryFloatAttribute("connectorboltOffsetX", &v.x);
	timer->QueryFloatAttribute("connectorboltOffsetY", &v.y);
	timer->QueryFloatAttribute("connectorboltOffsetZ", &v.z);
	m_pConnectPoint->SetBoltOffset(v);
	timer->QueryBoolAttribute("connectRenderable", &m_pConnectPoint->m_bIsRenderable);

	std::string meshName = timer->Attribute("connectName");
	if (!meshName.empty())
		m_pConnectPoint->m_pMeshName = meshName;

	std::string connectTex = timer->Attribute("connectTexture");
	if (!connectTex.empty())
		m_pConnectPoint->m_pTextureName = connectTex;


	SetBolt();
	//SetPosition(m_vPosition);
}

void LightningTrap::Save(TiXmlElement* obj)
{
	BaseGameEntity::Save(obj);

	obj->SetDoubleAttribute("LightningPower", m_fLightningPower);

	TiXmlElement* timer = new TiXmlElement("Timer");
	timer->SetDoubleAttribute("chargeTime", m_vTimerRange.x);
	timer->SetDoubleAttribute("dischargeTime", m_vTimerRange.y);
	Vector3 v = m_pConnectPoint->GetPosition();
	timer->SetDoubleAttribute("connectPosX", v.x);
	timer->SetDoubleAttribute("connectPosY", v.y);
	timer->SetDoubleAttribute("connectPosZ", v.z);
	v = m_pConnectPoint->GetScale();
	timer->SetDoubleAttribute("connectScaleX", v.x);
	timer->SetDoubleAttribute("connectScaleY", v.y);
	timer->SetDoubleAttribute("connectScaleZ", v.z);
	Quaternion q = m_pConnectPoint->GetOrientation();
	timer->SetDoubleAttribute("connectRotW", q.w);
	timer->SetDoubleAttribute("connectRotX", q.x);
	timer->SetDoubleAttribute("connectRotY", q.y);
	timer->SetDoubleAttribute("connectRotZ", q.z);
	v = m_vBoltOffset;
	timer->SetDoubleAttribute("thisboltOffsetX", v.x);
	timer->SetDoubleAttribute("thisboltOffsetY", v.y);
	timer->SetDoubleAttribute("thisboltOffsetZ", v.z);
	v = m_pConnectPoint->GetBoltOffset();
	timer->SetDoubleAttribute("connectorboltOffsetX", v.x);
	timer->SetDoubleAttribute("connectorboltOffsetY", v.y);
	timer->SetDoubleAttribute("connectorboltOffsetZ", v.z);
	timer->SetAttribute("connectName", m_pConnectPoint->m_pMeshName.c_str());
	timer->SetAttribute("connectTexture", m_pConnectPoint->m_pMeshName.c_str());
	timer->SetAttribute("connectRenderable", m_pConnectPoint->m_bIsRenderable);
	obj->LinkEndChild(timer);
}

std::shared_ptr<BaseGameEntity>	LightningTrap::Clone()
{
	// Check if we've reached our limit on copies
	if(m_iNumCopies >= m_iMaxNumCopies)
		return 0;

	// Create clone
	std::shared_ptr<LightningTrap> clone = std::shared_ptr<LightningTrap>(new LightningTrap());
	
	// Set all the attributes for the clone that are not unique to a specific instance.
	// This will be used to create instances of this object in the level designer.
	// Brief example:
	clone->SetMeshName(&m_pMeshName[0]);
	if(!m_pTextureName.empty())
		clone->SetTextureName(&m_pTextureName[0]);
	clone->SetRenderable(m_bIsRenderable);
	clone->SetColor(m_vColor);
	clone->SetScale(m_vScale);
	clone->SetOffset(m_vOffset);

	// Set Attributes particular to SmokeTrap
	clone->m_vTimerRange = m_vTimerRange;
	clone->m_fLightningPower = m_fLightningPower;
	
	// Setup the copy counter to point to the original counter
	clone->m_pNumCopies = &m_iNumCopies;
	m_iNumCopies++;

	// Initialize the object
	clone->Init();
	return clone;
}

void LightningTrap::Init()
{
	BaseGameEntity::Init();

	SetOrientation(m_qOrientation);

	

	//m_iTrapID = MEM->CreateSharedResource();
	//MEM->GetSharedResource(m_iTrapID)->SetPosition(m_vPosition);
	//MEM->GetSharedResource(m_iTrapID)->SetOrientation(m_qOrientation);

	if(!ScreenMan->GetEntityManager()->DisplayHiddenObjects())
	{
		m_pTarget = GPM->GetTurbine();
	}
	m_fTimer = (float)(rand() % (int)m_vTimerRange.y) + m_vTimerRange.x;
	if(m_pConnectPoint)
	{
		m_pConnectPoint->Init();
		ScreenMan->GetEntityManager()->AddEntity(m_pConnectPoint->GetID(), m_pConnectPoint);
		GFX->AddBoltObject(m_iLightningBoltID, m_vBoltOffset, m_pConnectPoint->GetBoltOffset(),
							0.4f,
							3.0f,
							0.5,
							0,
							0,
							D3DXVECTOR4(1, 1, 0, 1));
	}
}

void LightningTrap::Shutdown()
{
	

	if(m_iLightningBoltID >= 0)
	{
		GFX->RemoveBoltObject(m_iLightningBoltID);
		m_iLightningBoltID = -1;
	}
	if(m_pConnectPoint)
	{
		m_pConnectPoint->Shutdown();
		m_pConnectPoint = 0;	// Remove pointer
	}
	BaseGameEntity::Shutdown();
}

void LightningTrap::LinkedShutdown()
{
	BaseGameEntity::Shutdown();

	if(m_iLightningBoltID >= 0)
	{
		GFX->RemoveBoltObject(m_iLightningBoltID);
		m_iLightningBoltID = -1;
	}
	m_pConnectPoint = 0;	// Remove pointer
}

void LightningTrap::SetConnectorPosition(Vector3 pos)
{
	if(m_pConnectPoint)
	{
		m_pConnectPoint->SetPosition(pos);
		D3DXMATRIX m;
		D3DXMatrixLookAtRH(&m, &m_vPosition, &pos, &D3DXVECTOR3(0, 1, 0));
		D3DXQuaternionRotationMatrix(&m_qOrientation, &m);
	}
}

LightningTrapMast::LightningTrapMast()
	:BaseGameEntity(ET_LIGHTNINGTRAPCONNECTOR)
{
	m_pTrapParent = 0;

	m_pMeshName			= "Sphere(10).X";
	m_bIsRenderable		= true;
	m_vColor			= D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	m_vScale			= Vector3(1.0f, 1.0f, 1.0f);
	m_vPosition			= Vector3(0, 0, 0);
}

LightningTrapMast::LightningTrapMast(std::string mesh)
	:BaseGameEntity(ET_LIGHTNINGTRAPCONNECTOR)
{
	m_pTrapParent		= 0;
	m_pMeshName			= mesh;
	m_bIsRenderable		= true;
	m_vColor			= D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	m_vScale			= Vector3(1.0f, 1.0f, 1.0f);
	m_vPosition			= Vector3(0, 0, 0);
}

void LightningTrapMast::SetPosition(Vector3 pos)
{
	m_vPosition = pos;

	D3DXMATRIX m(m_mTransform);
	D3DXMatrixTransformation(&m, 0, 0, &m_vScale, 0, &m_qOrientation, &m_vPosition);
	//D3DXMatrixScaling(&m, scale.x, scale.y, scale.z);
	D3DXMatrixInverse(&m, 0, &m);

	D3DXMATRIX temp;
	D3DXMatrixTransformation(&temp, 0, 0, &Vector3(1, 1, 1), 0, 0, &BOLT_OFFSET);

	D3DXMatrixMultiply(&m, &m, &temp);
	D3DXMatrixInverse(&m, 0, &m);

	m_vBoltOffset = Vector3(m._41, m._42, m._43);

	if(m_pTrapParent)
	//{
	//	D3DXMATRIX m;
	//	D3DXMatrixLookAtLH(&m, &pos, &m_pTrapParent->GetPosition(), &D3DXVECTOR3(0, 1, 0));
	//	D3DXQuaternionRotationMatrix(&m_qOrientation, &m);
		m_pTrapParent->SetBolt();
	//}	
}

void LightningTrapMast::SetOrientation(Quaternion pOrientation)
{
	pOrientation.Normalize();
	m_qOrientation = pOrientation;

	D3DXMATRIX m(m_mTransform);
	D3DXMatrixTransformation(&m, 0, 0, &m_vScale, 0, &m_qOrientation, &m_vPosition);
	//D3DXMatrixScaling(&m, scale.x, scale.y, scale.z);
	D3DXMatrixInverse(&m, 0, &m);

	D3DXMATRIX temp;
	D3DXMatrixTransformation(&temp, 0, 0, &Vector3(1, 1, 1), 0, 0, &BOLT_OFFSET);

	D3DXMatrixMultiply(&m, &m, &temp);
	D3DXMatrixInverse(&m, 0, &m);

	m_vBoltOffset = Vector3(m._41, m._42, m._43);

	if(m_pTrapParent)
		m_pTrapParent->SetBolt();
}

void LightningTrapMast::SetOrientation(float yaw, float pitch, float roll)
{
	D3DXQuaternionRotationYawPitchRoll(&qScratch, yaw, pitch,roll);
	SetOrientation(qScratch);
}

void LightningTrapMast::SetScale(Vector3 scale)
{
	if(!m_pTrapParent)
	{
		m_vScale = scale;
		D3DXMATRIX m(m_mTransform);
		D3DXMatrixTransformation(&m, 0, 0, &m_vScale, 0, &m_qOrientation, &m_vPosition);
		//D3DXMatrixScaling(&m, scale.x, scale.y, scale.z);
		D3DXMatrixInverse(&m, 0, &m);

		D3DXMATRIX temp;
		D3DXMatrixTransformation(&temp, 0, 0, &Vector3(1, 1, 1), 0, 0, &BOLT_OFFSET);

		D3DXMatrixMultiply(&m, &m, &temp);
		D3DXMatrixInverse(&m, 0, &m);

		m_vBoltOffset = Vector3(m._41, m._42, m._43);
	}
	else
		m_pTrapParent->SetScale(scale);
}

void LightningTrapMast::Init()
{
	BaseGameEntity::Init();
}

void LightningTrapMast::Shutdown()
{
	BaseGameEntity::Shutdown();
	if(m_pTrapParent)
	{
		m_pTrapParent->LinkedShutdown();
		m_pTrapParent = 0;	// Remove pointer
		ScreenMan->GetEntityManager()->RemoveEntity(m_ID);	// Make sure to remove this from the entity list
	}
}

void LightningTrapMast::SetParent(LightningTrap *trapParent)
{
	m_pTrapParent = trapParent;
	m_bIsRenderable = m_pTrapParent->GetRenderable();
	m_vColor = m_pTrapParent->GetColor();
	m_vScale = m_pTrapParent->GetScale();
}