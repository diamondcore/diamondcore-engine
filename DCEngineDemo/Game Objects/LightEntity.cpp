#include "LightEntity.h"
#include "../UI/ScreenManager.h"


LightEntity::LightEntity()
	:BaseGameEntity(ET_LIGHT)
{
	m_vIntensity	= Vector4(1.0f, 0.0f, 0.0f, 0.0f);
	m_iPower		= 40;
	m_iLightID		= -1;
	m_bIsRenderable	= false;
	m_eType			= ET_LIGHT;
	m_pMeshName		= "Sphere(33).x";
	m_vColor		= D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);	// Set Mesh Color
}
LightEntity::LightEntity(Vector4 intensity)
	:BaseGameEntity(ET_LIGHT)
{
	m_vIntensity	= intensity;
	m_iPower		= 40;
	m_iLightID		= -1;
	m_bIsRenderable	= false;
	m_eType			= ET_LIGHT;
	m_pMeshName		= "Sphere(33).x";
	m_vColor		= D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);	// Set Mesh Color
}
LightEntity::LightEntity(Vector4 intensity, int power)
	:BaseGameEntity()
{
	m_vIntensity	= intensity;
	m_iPower		= power;
	m_iLightID		= -1;
	m_bIsRenderable	= false;
	m_eType			= ET_LIGHT;
	m_pMeshName		= "Sphere(33).x";
	m_vColor		= D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);	// Set Mesh Color
}
LightEntity::~LightEntity()
{}

void LightEntity::Load(TiXmlElement* node)
{
	// Gather and Set object information
	BaseGameEntity::Load(node);

	// Gather and Set Light specific information
	TiXmlElement* Light;
	Light = node->FirstChildElement("Light")->ToElement();
		Light->QueryIntAttribute("Power", &m_iPower);

		TiXmlElement* intensity;
		intensity = Light->FirstChildElement("Intensity")->ToElement();
		intensity->QueryFloatAttribute("x", &m_vIntensity.x);
		intensity->QueryFloatAttribute("y", &m_vIntensity.y);
		intensity->QueryFloatAttribute("z", &m_vIntensity.z);
		intensity->QueryFloatAttribute("w", &m_vIntensity.w);
}
void LightEntity::Save(TiXmlElement* obj)
{
	// Store Base information
	BaseGameEntity::Save(obj);

	// Store Light information
	TiXmlElement* light = new TiXmlElement("Light");
		light->SetAttribute("Power", m_iPower);
	
		// Create intensity 
		TiXmlElement* intensity = new TiXmlElement("Intensity");
		intensity->SetDoubleAttribute("x", m_vIntensity.x);
		intensity->SetDoubleAttribute("y", m_vIntensity.y);
		intensity->SetDoubleAttribute("z", m_vIntensity.z);
		intensity->SetDoubleAttribute("w", m_vIntensity.w);
		light->LinkEndChild(intensity);	// Link attribute to the light
	obj->LinkEndChild(light);			// Link attribute to the object
}

std::shared_ptr<BaseGameEntity> LightEntity::Clone()
{
	// Check if we've reached our limit on copies
	if(m_iNumCopies >= m_iMaxNumCopies)
		return 0;

	// Light variabes must be set in the constructor
	auto clone = std::shared_ptr<LightEntity>( new LightEntity(m_vIntensity, m_iPower) );
	
	// Setup the copy counter to point to the original counter
	clone->m_pNumCopies = &m_iNumCopies;
	m_iNumCopies++;

	clone->Init();
	return clone;
}

void LightEntity::Init()
{
	// Create a shared resource if it's needed
	if( m_ID < 0 && ScreenMan->GetEntityManager()->DisplayHiddenObjects() )
		m_ID = ScreenMan->GetMainCore()->m_mMainMemory->CreateSharedResource();
	BaseGameEntity::Init();

	Vector4 pos(m_vPosition.x, m_vPosition.y, m_vPosition.z, 1.0f);
	ScreenMan->GetGraphics()->AddLight(m_iLightID, pos, m_vIntensity, 40);
	ScreenMan->GetGraphics()->ChangeLightPower(m_iLightID, m_iPower);
}

void LightEntity::Shutdown()
{
	BaseGameEntity::Shutdown();

	if(m_iLightID >= 0)
		ScreenMan->GetGraphics()->RemoveLight(m_iLightID);
}

void LightEntity::SetPosition(Vector3 vPos)
{
	m_vPosition = vPos;
	if(m_iLightID >= 0)
		ScreenMan->GetGraphics()->ChangeLightPos(m_iLightID, D3DXVECTOR4(vPos.x, vPos.y, vPos.z, 1.0f));
}
void LightEntity::SetPosition(float x, float y, float z)
{
	SetPosition(Vector3(x,y,z));
}
void LightEntity::SetPosition(int idx, float fValue)
{
	switch(idx)
	{
	case 0:
		m_vPosition.x = fValue;
	case 1:
		m_vPosition.y = fValue;
	case 2:
		m_vPosition.z = fValue;
	default:
		assert ("BaseGameEntity::SetPosition failed - idx must be a value between 0 and 2"); 
		return;
	};
	SetPosition(m_vPosition);
}
void LightEntity::SetScale(Vector3 vScale)
{
	m_vScale = vScale;
	m_iPower = 40 + vScale.Magnitude();
	if(m_iLightID >= 0)
		ScreenMan->GetGraphics()->ChangeLightPower(m_iLightID, m_iPower);
}
