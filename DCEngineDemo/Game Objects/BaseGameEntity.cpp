#include "BaseGameEntity.h"
#include "../UI/ScreenManager.h"

#define MEM ScreenMan->GetMainCore()->m_mMainMemory
#define GFX ScreenMan->GetGraphics()

BaseGameEntity::BaseGameEntity(void)
{
	m_ID = -1;
	m_vPosition = Vector3(-1,-1,-1);
	m_vScale = Vector3(1.0f, 1.0f, 1.0f);
	m_vColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, -1.0f);	// "-1.0f" alpha uses the default from the loaded mesh
	m_qOrientation = Quaternion(1,0,0,0);
	m_eType = ET_UNASSIGNED;
	m_bIsRenderable = false;
	m_pTextureName = "";
	m_pMeshName = "";
	m_iNumCopies	= 0;
	m_pNumCopies	= 0;
	m_iMaxNumCopies = 100;
}

// para const
BaseGameEntity::BaseGameEntity(EntityType eType )
{
	
	m_ID = MEM->CreateSharedResource();
	m_vPosition = Vector3(-1,-1,-1);
	m_vScale = Vector3(1.0f, 1.0f, 1.0f);
	m_vColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, -1.0f);	// "-1.0f" alpha uses the default from the loaded mesh
	
	m_qOrientation = Quaternion(1,0,0,0);
	m_eType = eType;
	m_bIsRenderable = false;
	m_pTextureName = "";
	m_pMeshName = "";
	m_iNumCopies	= 0;
	m_pNumCopies	= 0;
	m_iMaxNumCopies = 100;
}

BaseGameEntity::BaseGameEntity(EntityType eType, char* pMeshName, char* pTexName, Vector3 vMeshScale)
{
	m_ID = MEM->CreateSharedResource();
	m_vPosition = Vector3(-1,-1,-1);
	m_vScale = vMeshScale;
	m_vColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, -1.0f);	// "-1.0f" alpha uses the default from the loaded mesh
	
	m_qOrientation = Quaternion(1,0,0,0);
	m_eType = eType;
	m_bIsRenderable = true;
	m_pTextureName = pTexName;
	m_pMeshName = pMeshName ;
	m_iNumCopies	= 0;
	m_pNumCopies	= 0;
	m_iMaxNumCopies = 100;

	Init();
}

BaseGameEntity::~BaseGameEntity(void)
{
}

//////////////////////
// UTILITIES
/////////////////////
// update and sync graphics,physics, and game logic
void BaseGameEntity::Update(float dt)
{
	// Update shared mem's position, which should update all 
	MEM->GetSharedResource(m_ID)->SetPosition(m_vPosition);

	// Update shared mem's orientation
	MEM->GetSharedResource(m_ID)->SetOrientation(m_qRotationOffset * m_qOrientation);
		

}

void BaseGameEntity::Load(TiXmlElement* node)
{
	// Gather and Set object information
	TiXmlElement* mesh;
	mesh = node->FirstChildElement("Mesh")->ToElement();
		m_pMeshName = mesh->Attribute("Name");
		m_pTextureName = mesh->Attribute("Texture");
		mesh->QueryBoolAttribute("Renderable", &m_bIsRenderable);

		TiXmlElement* color;
		color = mesh->FirstChildElement("Color")->ToElement();
		color->QueryFloatAttribute("r", &m_vColor.r);
		color->QueryFloatAttribute("g", &m_vColor.g);
		color->QueryFloatAttribute("b", &m_vColor.b);
		color->QueryFloatAttribute("a", &m_vColor.a);

	TiXmlElement* pos;
	pos = node->FirstChildElement("Position")->ToElement();
	pos->QueryFloatAttribute("x", &m_vPosition.x);
	pos->QueryFloatAttribute("y", &m_vPosition.y);
	pos->QueryFloatAttribute("z", &m_vPosition.z);

	TiXmlElement* quat;
	quat = node->FirstChildElement("Orientation")->ToElement();
	quat->QueryFloatAttribute("x", &m_qOrientation.x);
	quat->QueryFloatAttribute("y", &m_qOrientation.y);
	quat->QueryFloatAttribute("z", &m_qOrientation.z);
	quat->QueryFloatAttribute("w", &m_qOrientation.w);

	TiXmlElement* scale;
	scale = node->FirstChildElement("Scale")->ToElement();
	scale->QueryFloatAttribute("x", &m_vScale.x);
	scale->QueryFloatAttribute("y", &m_vScale.y);
	scale->QueryFloatAttribute("z", &m_vScale.z);

	TiXmlElement* offset;
	offset = node->FirstChildElement("Offset")->ToElement();
	offset->QueryFloatAttribute("x", &m_vOffset.x);
	offset->QueryFloatAttribute("y", &m_vOffset.y);
	offset->QueryFloatAttribute("z", &m_vOffset.z);
}
void BaseGameEntity::Save(TiXmlElement* obj)
{
	// Create Mesh
	TiXmlElement* mesh = new TiXmlElement("Mesh");
		mesh->SetAttribute("Name", m_pMeshName.c_str());
		mesh->SetAttribute("Texture", m_pTextureName.c_str());
		mesh->SetAttribute("Renderable", m_bIsRenderable);

		// Create Color 
		TiXmlElement* color = new TiXmlElement("Color");
		color->SetDoubleAttribute("r", m_vColor.r);
		color->SetDoubleAttribute("g", m_vColor.g);
		color->SetDoubleAttribute("b", m_vColor.b);
		color->SetDoubleAttribute("a", m_vColor.a);
		mesh->LinkEndChild(color);	// Link attribute to the mesh
	obj->LinkEndChild(mesh);	// Link attribute to the object

	// Create position
	TiXmlElement* pos = new TiXmlElement("Position");
	pos->SetDoubleAttribute("x", m_vPosition.x);
	pos->SetDoubleAttribute("y", m_vPosition.y);
	pos->SetDoubleAttribute("z", m_vPosition.z);
	obj->LinkEndChild(pos);		// Link attribute to the object

	// Create Orientation 
	TiXmlElement* quat = new TiXmlElement("Orientation");
	quat->SetDoubleAttribute("x", m_qOrientation.x);
	quat->SetDoubleAttribute("y", m_qOrientation.y);
	quat->SetDoubleAttribute("z", m_qOrientation.z);
	quat->SetDoubleAttribute("w", m_qOrientation.w);
	obj->LinkEndChild(quat);	// Link attribute to the object

	// Create scale
	TiXmlElement* scale = new TiXmlElement("Scale");
	scale->SetDoubleAttribute("x", m_vScale.x);
	scale->SetDoubleAttribute("y", m_vScale.y);
	scale->SetDoubleAttribute("z", m_vScale.z);
	obj->LinkEndChild(scale);	// Link attribute to the object

	// Create offset
	TiXmlElement* offset = new TiXmlElement("Offset");
	offset->SetDoubleAttribute("x", m_vOffset.x);
	offset->SetDoubleAttribute("y", m_vOffset.y);
	offset->SetDoubleAttribute("z", m_vOffset.z);
	obj->LinkEndChild(offset);	// Link attribute to the object
}

std::shared_ptr<BaseGameEntity>	BaseGameEntity::Clone()	// Clone these attributes into a new BaseGameEntity
{
	// Check if we've reached our limit on copies
	if(m_iNumCopies >= m_iMaxNumCopies)
		return 0;

	// Create clone
	std::shared_ptr<BaseGameEntity> clone = std::shared_ptr<BaseGameEntity>(new BaseGameEntity(m_eType));
	
	// Set all the attributes for the clone that are not unique to a specific instance.
	// This will be used to create instances of this object in the level designer.
	// Brief example:
	clone->SetMeshName(&m_pMeshName[0]);
	if(!m_pTextureName.empty())
		clone->SetTextureName(&m_pTextureName[0]);
	clone->SetRenderable(m_bIsRenderable);
	clone->SetColor(m_vColor);
	clone->SetScale(m_vScale);
	clone->SetOffset(m_vOffset);

	// Setup the copy counter to point to the original counter
	clone->m_pNumCopies = &m_iNumCopies;
	m_iNumCopies++;

	// Initialize the object
	clone->Init();
	return clone;
}

void BaseGameEntity::Init()
{
	// Check if object can be rendered
	if(!m_bIsRenderable && !ScreenMan->GetEntityManager()->DisplayHiddenObjects())
		return;	// Do not render this object

	// If it needs an ID and hasn't been created yet, then create an id for it
	if(m_ID < 0)
		m_ID = MEM->CreateSharedResource();
	
	if(m_pTextureName.empty())
		GFX->CreateMesh(m_ID,&m_pMeshName[0],false);
	else
	{
		// Load texture if it has one
		GFX->LoadTexture(GetTextureName());					
		GFX->CreateMesh(m_ID,&m_pMeshName[0],&m_pTextureName[0],false);
	}
	// Set graphics properties
	GFX->SetMeshScale(m_ID, GetMeshName(), m_vScale);
	GFX->SetMeshColor(m_ID, GetMeshName(), m_vColor);
}

void BaseGameEntity::Shutdown()
{
	GFX->RemoveMesh(m_ID, &m_pMeshName[0]);
	MEM->FreeSharedResource(m_ID);
	m_ID = -1;
	
	// Decrment the original counter
	if(m_pNumCopies)
	{
		(*m_pNumCopies)--;
		m_pNumCopies = NULL;
	}
}

// compare entities
bool BaseGameEntity::operator== (const BaseGameEntity& entity)
{
	if( m_pMeshName == entity.m_pMeshName &&
		m_pTextureName == entity.m_pTextureName &&
		m_bIsRenderable == entity.m_bIsRenderable &&
		m_eType == entity.m_eType &&
		m_vColor == entity.m_vColor &&
		m_vOffset == entity.m_vOffset)
		return true;
	return false;
}

//////////////////////
// ACCESSORS
/////////////////////


// returns requested component of position
// 0 = X, 1 = Y, 2 = Z
float BaseGameEntity::GetPosition(int idx)
{
	switch(idx)
	{
	case 0:
		return m_vPosition.x;
	case 1:
		return m_vPosition.y;
	case 2:
		return m_vPosition.z;
	default:
		return -1.0f;
	};
}


//////////////////////
// MUTATORS
/////////////////////
void BaseGameEntity::SetPosition(Vector3 vPos)
{
	m_vPosition = vPos;
	
}

// Sets position of entity by component
void BaseGameEntity::SetPosition(float x, float y, float z)
{
	m_vPosition.x = x;
	m_vPosition.y = y;
	m_vPosition.z = z;

}

// sets a single component of the entity's position
// 0 = X, 1 = Y, 2 = Z
void BaseGameEntity::SetPosition(int idx, float fValue)
{
	switch(idx)
	{
	case 0:
		m_vPosition.x = fValue;
	case 1:
		m_vPosition.y = fValue;
	case 2:
		m_vPosition.z = fValue;
	default:
		assert ("BaseGameEntity::SetPosition failed - idx must be a value between 0 and 2"); 
		return;
	};

	
}

// sets entity's orientation by yaw/pitch/roll
void BaseGameEntity::SetOrientation(float yaw, float pitch, float roll)
{
	D3DXQuaternionRotationYawPitchRoll(&m_qOrientation, yaw, pitch,roll);
}



// rotates entity's position by yaw/pitch/roll
void BaseGameEntity::RotateBody(float yaw, float pitch, float roll)
{
	D3DXQuaternionRotationYawPitchRoll(&qScratch, yaw, pitch,roll);
	m_qOrientation *= qScratch;
}

void BaseGameEntity::Transform(Matrix4 mTransform)
{
	m_mTransform = m_mTransform * mTransform;
}
