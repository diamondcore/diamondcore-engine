#include "PhysicsEntity.h"
#include "../UI/ScreenManager.h"
#include "../DCEngineDemo/Havok/HavokManager.h"

#define MEM ScreenMan->GetMainCore()->m_mMainMemory


PhysicsEntity::PhysicsEntity(void)
	:BaseGameEntity()
{
		m_pBody = NULL;
		m_iBodyID = -1;
		m_fMass = 10;
		m_bIsSphere = false;
		m_bUseGravity = true;
		m_fAngDamping = -1.0f;
}

PhysicsEntity::PhysicsEntity(EntityType eType)
	:BaseGameEntity(eType)
{
	m_pBody = NULL;
	m_iBodyID = -1;
	m_fMass = 10;
	m_bIsSphere = false;
	m_bUseGravity = true;
	m_fAngDamping = -1.0f;
}

PhysicsEntity::PhysicsEntity(EntityType eType, char* pMeshName, char* pTexName, Vector3 vMeshScale)
	:BaseGameEntity(eType, pMeshName,pTexName,vMeshScale)
{
	m_pBody = NULL;
	m_iBodyID = -1;
	m_fMass = 10;
	m_bIsSphere = false;
	m_bUseGravity = true;
	m_fAngDamping = -1.0f;

	

}


PhysicsEntity::~PhysicsEntity(void)
{
}

// compare entities
bool PhysicsEntity::operator== (const BaseGameEntity& entity)
{
	// Check if the root attributes match up
	if( ((BaseGameEntity)(*this)) == entity )
	{
		// Compare attributes specific to the trigger entity
		PhysicsEntity* tEntity = (PhysicsEntity*)(&entity);
		if( m_bIsSphere == tEntity->m_bIsSphere &&
			m_fMass == tEntity->m_fMass )
			return true;
	}
	return false;
}

//////////////////////
// UTILITIES
/////////////////////
// update and sync graphics,physics, and game logic
void PhysicsEntity::Update(float dt)
{
	if(m_pBody)
	{
		// get new position from physics body
		SetPosition(m_pBody->getPosition() );

		// get new orientation from physics body
		SetOrientation(&m_pBody->getRotation() );

		// get transform matrix from physics body
		SetTransform(&m_pBody->getTransform() );
	}
	BaseGameEntity::Update(dt);
}

void PhysicsEntity::Load(TiXmlElement* node)
{
	// Gather and Set object information
	BaseGameEntity::Load(node);

	// Load physics information
	node->QueryFloatAttribute("Mass", &m_fMass);
	node->QueryBoolAttribute("IsSphere", &m_bIsSphere);
	node->QueryBoolAttribute("UseGravity", &m_bUseGravity);
	node->QueryFloatAttribute("AngularDampining", &m_fAngDamping);
}
void PhysicsEntity::Save(TiXmlElement* obj)
{
	// Create the base object info
	BaseGameEntity::Save(obj);

	// Save physics information
	obj->SetDoubleAttribute("Mass", m_fMass);
	obj->SetAttribute("IsSphere", m_bIsSphere);
	obj->SetAttribute("UseGravity", m_bUseGravity);
	obj->SetDoubleAttribute("AngularDampining", m_fAngDamping);
}


std::shared_ptr<BaseGameEntity>	PhysicsEntity::Clone()	// Clone these attributes into a new BaseGameEntity
{
	// Check if we've reached our limit on copies
	if(m_iNumCopies >= m_iMaxNumCopies)
		return 0;

	// Create clone
	std::shared_ptr<PhysicsEntity> clone = std::shared_ptr<PhysicsEntity>(new PhysicsEntity(m_eType));
	
	// Set all the attributes for the clone that are not unique to a specific instance.
	// This will be used to create instances of this object in the level designer.
	// Brief example:
	clone->SetMeshName(&m_pMeshName[0]);
	if(!m_pTextureName.empty())
		clone->SetTextureName(&m_pTextureName[0]);
	clone->SetRenderable(m_bIsRenderable);
	clone->SetColor(m_vColor);
	clone->SetScale(m_vScale);
	clone->SetOffset(m_vOffset);
	clone->SetIsSphere(m_bIsSphere);
	clone->SetGravity(m_bUseGravity);

	// Set physics attributes
	clone->m_fMass       = m_fMass;
	clone->m_fAngDamping = m_fAngDamping;
	
	// Setup the copy counter to point to the original counter
	clone->m_pNumCopies = &m_iNumCopies;
	m_iNumCopies++;

	// Initialize the object
	clone->Init();
	return clone;
}

void PhysicsEntity::Init()
{
	BaseGameEntity::Init();

	// Create physics specific attributes
	Vector3 halfExtent;
	ScreenMan->GetGraphics()->GetMeshBV(&m_pMeshName[0], halfExtent);
	halfExtent = halfExtent.ComponentProduct(m_vScale);	// Scale the halfExtent by the specified scale

	// Check if we are in a mode that has Havok initialized
	if(!ScreenMan->GetEntityManager()->DisplayHiddenObjects())
	{
		// init body here w/ havok manager
		if(m_bIsSphere)
		{
			float r = (halfExtent.x+halfExtent.y+halfExtent.z)/3.0f;
			m_pBody = HM->AddMovingSphere(-1, m_vPosition.x, m_vPosition.y, m_vPosition.z, r, m_fMass);
		}
		else
		{
			if(!m_bUseGravity)
				m_pBody = HM->AddMovingBox(-1, m_vPosition.x, m_vPosition.y, m_vPosition.z, halfExtent.x, halfExtent.y, halfExtent.z, m_fMass, 2);	// Disable collision with terrain
			else
				m_pBody = HM->AddMovingBox(-1, m_vPosition.x, m_vPosition.y, m_vPosition.z, halfExtent.x, halfExtent.y, halfExtent.z, m_fMass);
		}

		// set body id
		m_iBodyID = m_pBody->getUid();

		// Remvoe gravity
		if(!m_bUseGravity)
			HM->SetGravityFactor(m_pBody->getUid());

		// Set angular dampining
		if( m_fAngDamping >= 0.0f )
			HM->SetAngularDampining(m_pBody, m_fAngDamping);

		// Set body attributes
		HM->SetBodyPosition(m_pBody, m_vPosition.x, m_vPosition.y, m_vPosition.z);
		Vector3 rot = m_qOrientation.GetYawPitchRoll();
		RotateBody(rot.x, rot.y, rot.z);
	}
}

void PhysicsEntity::Shutdown()
{
	BaseGameEntity::Shutdown();

	// Remove body if it exists
	if(m_pBody)
	{
		HM->RemoveEntity(m_iBodyID);
		m_iBodyID = -1;
		m_pBody = 0;
	}
}

//////////////////////
// MUTATORS
/////////////////////

void PhysicsEntity::SetPosition(Vector3 vPos)
{
	m_vPosition = vPos;
	if(m_pBody)
		HM->SetBodyPosition(m_pBody,vPos.x, vPos.y, vPos.z);	
}
// Sets position of entity by component
void PhysicsEntity::SetPosition(float x, float y, float z)
{
	m_vPosition.x = x;
	m_vPosition.y = y;
	m_vPosition.z = z;
	if(m_pBody)
		HM->SetBodyPosition(m_pBody,x, y, z);
}

// sets a single component of the entity's position
// 0 = X, 1 = Y, 2 = Z
void PhysicsEntity::SetPosition(int idx, float fValue)
{
	switch(idx)
	{
	case 0:
		m_vPosition.x = fValue;
	case 1:
		m_vPosition.y = fValue;
	case 2:
		m_vPosition.z = fValue;
	default:
		assert ("PhysicsEntity::SetPosition failed - idx must be a value between 0 and 2"); 
		return;
	};
	if(m_pBody)
		HM->SetBodyPosition(m_pBody,m_vPosition.x, m_vPosition.y, m_vPosition.z);
}

// ONLY SETS LOCAL POSITION, DOES NOT UPDATE BODY
void PhysicsEntity::SetPosition(const hkVector4 &vPos)
{
	m_vPosition.x = vPos(0);
	m_vPosition.y = vPos(1);
	m_vPosition.z = vPos(2);

}

// Sets entity's orientation
void PhysicsEntity::SetOrientation(Quaternion qOrientation)
{
	m_qOrientation = qOrientation;
	if (m_pBody)
	{
		Vector3 vYawPitchRoll = qOrientation.GetYawPitchRoll();
		HM->SetBodyOrientation(m_pBody, vYawPitchRoll.y,vYawPitchRoll.x,vYawPitchRoll.z);
	}

}

// sets entity's orientation by yaw/pitch/roll
void PhysicsEntity::SetOrientation(float yaw, float pitch, float roll)
{
	if(m_pBody)
		HM->SetBodyOrientation(m_pBody, pitch,yaw,roll);
}
// set entity's orientation from hkQuaternion
void PhysicsEntity::SetOrientation(const hkQuaternion* qOrientation)
{
	m_qOrientation.x = qOrientation->getComponent<0>();
	m_qOrientation.y = qOrientation->getComponent<1>();
	m_qOrientation.z = qOrientation->getComponent<2>();
	m_qOrientation.w = qOrientation->getComponent<3>();

}

// rotates entity's position by yaw/pitch/roll
void PhysicsEntity::RotateBody(float yaw, float pitch, float roll)
{
	if(m_pBody)
		HM->RotateBody(m_pBody, pitch,yaw,roll);
}

// sets the entity's transform from havok body
void PhysicsEntity::SetTransform(const hkTransform* mTransform)
{
	
	m_mTransform(0,0) = mTransform->getElement<0,0>();
	m_mTransform(0,1) = mTransform->getElement<0,1>();
	m_mTransform(0,2) = mTransform->getElement<0,2>();
	m_mTransform(0,3) = mTransform->getElement<0,3>();
	m_mTransform(1,0) = mTransform->getElement<1,0>();
	m_mTransform(1,1) = mTransform->getElement<1,1>();
	m_mTransform(1,2) = mTransform->getElement<1,2>();
	m_mTransform(1,3) = mTransform->getElement<1,3>();
	m_mTransform(2,0) = mTransform->getElement<2,0>();
	m_mTransform(2,1) = mTransform->getElement<2,1>();
	m_mTransform(2,2) = mTransform->getElement<2,2>();
	m_mTransform(2,3) = mTransform->getElement<2,3>();
	m_mTransform(3,0) = mTransform->getElement<3,0>();
	m_mTransform(3,1) = mTransform->getElement<3,1>();
	m_mTransform(3,2) = mTransform->getElement<3,2>();
	m_mTransform(3,3) = mTransform->getElement<3,3>();

}

// sets gravity on/off
void PhysicsEntity::SetGravity(bool enable)
{
	if(m_bUseGravity == enable)
		return;

	m_bUseGravity = enable;
	// Only lets you disable gravity
	if(!m_bUseGravity && m_pBody)
	{
		HM->AddToCollisionGroup(m_pBody, 2);	// Disable collision with the terrain
		HM->SetGravityFactor(m_pBody->getUid());
	}
}