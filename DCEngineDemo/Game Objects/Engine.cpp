#include "Engine.h"
#include "../UI/ScreenManager.h"
#include "../Havok/HavokManager.h"

#define GFX ScreenMan->GetGraphics()
#define MEM ScreenMan->GetMainCore()->m_mMainMemory


Engine::Engine(void)
{
	m_eType = ET_UNASSIGNED;
	m_iLightID = -1;
	m_iSparkID = -1;
}

Engine::Engine(Vector3 vOffset, Vector3 vHalfLengths, char* pMeshName )
	:PhysicsEntity(ET_ENGINE, pMeshName, "")
{
	m_iLightID = -1;
	m_iSparkID = -1;

	m_pMeshName = pMeshName;

	m_vOffset = vOffset;

	m_vPosition = Vector3(0,0,0) + vOffset;

	m_vHalfLengths = vHalfLengths;
	
	// engine speed
	m_fEngineSpeed = 0.0f;
	// capped engine speed
	m_fCappedSpeed = 0.0f;

	// init body here w/ havok manager
	m_pBody = HM->AddMovingBox(-1,m_vPosition.x, m_vPosition.y, m_vPosition.z,
				m_vHalfLengths.x, m_vHalfLengths.y, m_vHalfLengths.z, 10);

	// set body id
	m_iBodyID = m_pBody->getUid();

	// init orientation
	m_qOrientation = Quaternion(1,0,0,0);

	// by default mesh will be arrow 
	//GFX->CreateMesh(m_ID, &m_pMeshName[0],false);
	//GFX->SetMeshScale(m_ID,&m_pMeshName[0], Vector3(m_vHalfLengths.x * 2, m_vHalfLengths.y * 2, m_vHalfLengths.z * 2) );
	if (strcmp(&m_pMeshName[0], "MKIV_Engine.X") == 0)
		m_vScale = Vector3(.002f, .002f, .002f);
	else
		m_vScale = Vector3(.01f, .01f, .01f);
	GFX->SetMeshScale(m_ID,&m_pMeshName[0], m_vScale );
	MEM->GetSharedResource(m_ID)->SetPosition(m_vPosition);
	MEM->GetSharedResource(m_ID)->SetOrientation(m_qOrientation);
	
	HM->SetBodyPosition(m_pBody, m_vPosition.x, m_vPosition.y, m_vPosition.z);
	HM->SetBodyOrientation(m_pBody, 0,0,0);

	// initialize health
	InitHealth();
}


Engine::~Engine(void)
{
	
}

void Engine::Update(float dt)
{
	PhysicsEntity::Update(dt);

	if(m_iSparkID >= 0)
	{		
		MEM->GetSharedResource(m_iSparkID)->SetPosition(m_vPosition);
	}
	if(m_iLightID >= 0)
	{
		Vector3 pos = m_vPosition;
		pos += m_qOrientation.GetForward() * -(m_vHalfLengths.z * 0.5f);
		GFX->ChangeLightPos(m_iLightID, Vector4(pos.x, pos.y, pos.z, 1.0f));
	}
}

void Engine::Init()
{
//	PhysicsEntity::Init();

	// Set graphics specific attributes
	//SetColor(D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f));
	//GFX->SetMeshColor(m_ID, &m_pMeshName[0], m_vColor);
	//GFX->SetDefaultMeshTexture(&m_pMeshName[0], &m_pTextureName[0]);
	//GFX->SetDefaultMeshLightAttributes(&m_pMeshName[0], 5.0f, 1.0f, 1.0f);
	GFX->SetDefaultMeshLightAttributes(&m_pMeshName[0], 1.0f, 1.0f, 0.0f);

	// Setup Particles
	GFX->CreateParticle(m_ID, "JetFlame", (void*)(&m_fCappedSpeed));
	
	// Create Light
	if(m_iLightID < 0)
	{
		Vector3 pos = m_vPosition;
		pos += m_qOrientation.GetForward() * -(m_vHalfLengths.z * 0.5f);
		GFX->AddLight(m_iLightID, Vector4(pos.x, pos.y, pos.z, 0.0f), Vector4(1.0f, 0.7f, 0.1f, 1.0f), 0.0f);
	}
	m_fPrevEngineSpeed = 0.0f;
}

void Engine::InitHealth()
{
	if (strcmp(&m_pMeshName[0], "MKIII_Engine.X") == 0 )
	{
		m_fMaxHealth = m_fHealth = MKIIIHEALTH;
		m_fMaxEnergy = m_fCurrentEnergy = MKIIIENERGY;
	}
	else if (strcmp(&m_pMeshName[0], "MKIV_Engine.X") == 0 )
	{
		m_fMaxHealth = m_fHealth = MKIVHEALTH;
		m_fMaxEnergy = m_fCurrentEnergy = MKIVENERGY;
	}
	else if (strcmp(&m_pMeshName[0], "X95_Engine.X") == 0 )
	{
		m_fMaxHealth = m_fHealth = X95HEALTH;
		m_fMaxEnergy = m_fCurrentEnergy = X95ENERGY; 
	}
	else
	{

	}


}

void Engine::Shutdown()
{
	// Remove the particle
	GFX->RemoveParticle(m_ID);
	if(m_iSparkID >= 0)
	{
		GFX->RemoveParticle(m_iSparkID);
		MEM->FreeSharedResource(m_iSparkID);
		m_iSparkID = -1;
	}
	if(m_iLightID >= 0)
	{
		GFX->RemoveLight(m_iLightID);
		m_iLightID = -1;
	}
	// Remove any possible distortion meshes
	GFX->RemoveDistortionMesh(m_ID, &GetMeshName()[0]);

	PhysicsEntity::Shutdown();
}

void Engine::SetSpeed(float speed)
{
	if(m_fHealth <= 0.0f)
	{
		// This engine has blown out
		// Don't want to completely stop the engine and go in circles,
		// so go at 25% speed
		speed *= 0.25f;
	}
	m_fEngineSpeed = m_fCappedSpeed = speed;
	if(m_fCappedSpeed > 100.0f)	// Cap the speed for the jet flames shader
		m_fCappedSpeed = 100.0f;
	// Adjust the light from the engine with the thrust
	if(m_iLightID >= 0 && m_fEngineSpeed != m_fPrevEngineSpeed)
	{
		int boost = 0;
		if(m_fEngineSpeed > m_fCappedSpeed)
			boost = (m_fEngineSpeed - m_fCappedSpeed)/6; // /8;
		GFX->ChangeLightPower(m_iLightID, (m_fCappedSpeed/3) + boost);
		m_fPrevEngineSpeed = m_fEngineSpeed;
	}
}
// setting amount of brake used
void Engine::SetBrake(float fAmount)
{
	if(m_fHealth <= 0.0f)
	{
		// This engine has blown out
		// Don't want to completely stop the engine and go in circles,
		// so go at 25% speed
		fAmount *= 0.25f;
	}
	m_fEngineBrakeAmount = fAmount;
}
void Engine::ApplyDamage(float damage)
{
	float newHealth = m_fHealth - damage;

	// Test a decrease in health
	if(newHealth < m_fHealth)
	{
		if(m_fHealth >= m_fMaxHealth * .80f && newHealth < m_fMaxHealth * .80f)
		{
			// Health just fell below 80%
			// Create sparks coming from the engine
			if(m_iSparkID < 0)
			{
				m_iSparkID = MEM->CreateSharedResource();
				MEM->GetSharedResource(m_iSparkID)->SetPosition(m_vPosition);
			}
			GFX->CreateParticle(m_iSparkID, "SmallSpark", 0);
		}
		else if(m_fHealth >= m_fMaxHealth *.50f && newHealth < m_fMaxHealth * .50f)
		{
			// Health just fell below 50%
			// Create fire and smoke coming from the engine
			GFX->CreateParticle(m_iSparkID, "SmallFire", 0);
			GFX->CreateParticle(m_ID, "SmokeTrail", 0);
		}
		else if(m_fHealth >= m_fMaxHealth *.10f && newHealth < m_fMaxHealth * .10f)
		{
			// Health just fell below 10%
			// Create more smoke coming from the engine
			GFX->CreateParticle(m_ID, "SmokeTrail", 0);
		}
	}
	else  // Test for an increase in health
	{
		
		if(m_fHealth < m_fMaxHealth * .10f && newHealth >= m_fMaxHealth * .10f)
		{
			// The health just increased above 10%
			// Remove some smoke from the burning engine
			GFX->RemoveParticle(m_ID);
			GFX->CreateParticle(m_ID, "JetFlame", (void*)(&m_fCappedSpeed));
			GFX->CreateParticle(m_ID, "SmokeTrail", 0);
		}
		else if(m_fHealth < m_fMaxHealth * .50f && newHealth >= m_fMaxHealth * .50f)
		{
			// The health just increased above 50%
			// Remove the fire and the smoke from the engine
			GFX->RemoveParticle(m_ID);
			GFX->RemoveParticle(m_iSparkID);
			GFX->CreateParticle(m_ID, "JetFlame", (void*)(&m_fCappedSpeed));
			GFX->CreateParticle(m_iSparkID, "SmallSpark",0);
		}
		else if(m_fHealth < m_fMaxHealth * .80f && newHealth >= m_fMaxHealth * .80f)
		{
			// The health just increased above 80%
			// Remove the sparks from the engine
			GFX->RemoveParticle(m_iSparkID);
			if(m_iSparkID >= 0)
			{
				MEM->FreeSharedResource(m_iSparkID);
				m_iSparkID	= -1;
			}
		}
	}
	m_fHealth = newHealth;
}

void Engine::ResetAllDamage()
{
	GFX->RemoveParticle(m_ID);
	GFX->RemoveParticle(m_iSparkID);
	GFX->CreateParticle(m_ID, "JetFlame", (void*)(&m_fCappedSpeed));
}

void Engine::UseEnergy(float energy)
{
	float newEnergy = m_fCurrentEnergy - energy;
	if(energy >= 0.0f && energy <= m_fMaxEnergy)
		m_fCurrentEnergy = newEnergy;
}
