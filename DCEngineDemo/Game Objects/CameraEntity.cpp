#include "CameraEntity.h"
#include "../UI/ScreenManager.h"
#include "../Controls/Camera/CameraManager.h"

CameraEntity::CameraEntity()
	:BaseGameEntity(ET_CAMERA)
{
	m_iCameraID		= -1;
	m_bIsRenderable	= false;
	m_pMeshName		= "Camera.X";
}

CameraEntity::~CameraEntity()
{
}

void CameraEntity::Update(float dt)
{
	// Update Camera Mesh
	BaseGameEntity::Update(dt);

	// Update actual Camera
	//CamMgr->Update(dt);
}

void CameraEntity::Load(TiXmlElement* node)
{
	// Gather and Set object information
	BaseGameEntity::Load(node);
}
void CameraEntity::Save(TiXmlElement* obj)
{
	// Store Base information
	BaseGameEntity::Save(obj);
}

std::shared_ptr<BaseGameEntity> CameraEntity::Clone()
{
	// Check if we've reached our limit on copies
	if(m_iNumCopies >= m_iMaxNumCopies)
		return 0;

	// Camera variabes must be set in the constructor
	auto clone = std::shared_ptr<CameraEntity>( new CameraEntity() );
	
	// Set all the attributes for the clone that are not unique to a specific instance.
	// This will be used to create instances of this object in the level designer.
	// Brief example:
	clone->SetMeshName(&m_pMeshName[0]);
	if(!m_pTextureName.empty())
		clone->SetTextureName(&m_pTextureName[0]);
	clone->SetRenderable(m_bIsRenderable);
	clone->SetColor(m_vColor);
	clone->SetScale(m_vScale);
	clone->SetOffset(m_vOffset);
	
	// Setup the copy counter to point to the original counter
	clone->m_pNumCopies = &m_iNumCopies;
	m_iNumCopies++;

	clone->Init();
	return clone;
}

void CameraEntity::Init()
{
	BaseGameEntity::Init();

	m_iCameraID = CamMgr->CreateCamera(FREECAM);
	CamMgr->GetCameraControlByID(m_iCameraID)->SetPosition(m_vPosition.x, m_vPosition.y, m_vPosition.z);
	((GameplayCam*)(CamMgr->GetCameraControlByID(m_iCameraID)))->SetLookAt(m_qOrientation);
}

void CameraEntity::Shutdown()
{
	BaseGameEntity::Shutdown();

	if(m_iCameraID >= 0)
		CamMgr->DeleteCameraControl(m_iCameraID);
}

void CameraEntity::SetPosition(Vector3 vPos)
{
	m_vPosition = vPos;
	if(m_iCameraID >= 0)
		CamMgr->GetCameraControlByID(m_iCameraID)->SetPosition(m_vPosition.x, m_vPosition.y, m_vPosition.z);
}
void CameraEntity::SetPosition(float x, float y, float z)
{
	SetPosition(Vector3(x,y,z));
}
void CameraEntity::SetPosition(int idx, float fValue)
{
	switch(idx)
	{
	case 0:
		m_vPosition.x = fValue;
	case 1:
		m_vPosition.y = fValue;
	case 2:
		m_vPosition.z = fValue;
	default:
		assert ("BaseGameEntity::SetPosition failed - idx must be a value between 0 and 2"); 
		return;
	};
	SetPosition(m_vPosition);
}

void CameraEntity::SetOrientation(Quaternion pOrientation)
{
	m_qOrientation = pOrientation;
	
	// Set camera orientation
	if(m_iCameraID >= 0)
		((GameplayCam*)(CamMgr->GetCameraControlByID(m_iCameraID)))->SetLookAt(m_qOrientation);
}
void CameraEntity::SetOrientation(float yaw, float pitch, float roll)
{
	D3DXQuaternionRotationYawPitchRoll(&m_qOrientation, yaw, pitch, roll);
	SetOrientation(m_qOrientation);
}