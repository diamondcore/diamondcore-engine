#pragma once
#include "BaseGameEntity.h"
#include "../FSM/FSM.h"

/*
	Turbine class - this is the aggregate combined racer. 
*/

//forward declarations
class Engine;
class Pod;
class Laser;


enum eDamageType
{
	e_SelfDamage = 0,
	e_MeteorDamage,
	e_TurretDamage,
	e_GeyserDamage,
	e_ElectricDamage,
	e_CrushDamage	
};

class Turbine : public BaseGameEntity
{
protected:

	

	// two engines - this could change depending on how
	//	awesome we are.
	Engine* m_pLeftEngine;
	Engine* m_pRightEngine;
	
	// one pod
	Pod* m_pPod;

	// two engine-pod connectors
	Vector3 m_vLeftConnector;
	Vector3 m_vRightConnector;
	Laser*	m_pLeftConnector;
	Laser*	m_pRightConnector;

	// max linear speed
	float m_fMaxLinearSpeed;
	// current speed
	float m_fCurrentSpeed;
	// left engine brake amount
	float m_fLeftEngineBrake;
	// right engine brake amount
	float m_fRightEngineBrake;
	// max turning speed
	float m_fMaxTurningSpeed;
	// armor rating
	float m_fArmorRating;
	// heat resistance
	float m_fEndurance;
	// boost multiplier
	float m_fBoostMultiplier;
	// repair rate
	float m_fRepairRate;
	// cam Rig ID
	int m_iCamID;
	// pod's size in 3d
	Vector3 m_vPodSize;
	// turbine's health
	float m_fHealth;
	// turbine's power coupling
	int m_iPowerCouplingID;
	// updates the hover effect
	void UpdateHover(float dt);
	// max health of the turbine
	float m_fMaxHealth;
	// max energy
	float m_fMaxEnergy;
	// current energy
	float m_fCurrentEnergy;
	// energy recharge rate
	float m_fEnergyRechargeRate;
	// fake friciton
	float m_fFriction;

	// flags death animation
	bool m_bDeathAnimation;
	// death animation stage
	int m_iDeathStage;
	// keep track of the death animation
	float m_fDeathAnimTimer;
	// limp connector
	int	m_iLimpConnector;	// 1: right engine, -1: left engine

	int m_eLastDamageType;

	////////////////
	// SCRATCH VARS
	////////////////
	Vector3 vScratch1, vScratch2, vScratch3;
	Matrix4 mScratch1;

public:
	Turbine(void);
	Turbine(Vector3 vPosition, Vector3 vLeftEngineOffset,
		Vector3 vRightEngineOffset, Vector3 vPodOffset,
		Vector3 vLeftEngineHalfLengths, Vector3 vRightEngineHalfLengths,
		Vector3 vPodHalfLengths, char* pPodMeshName, char* pEngineMeshName);

	~Turbine(void);

	// finite state machine
	FiniteStateMachine<Turbine>* m_pFSM;

	// sounds
	int iStartupSound,
		iIdleEngineSound,
		iShiftSound,
		iLowGearSound;

	//////////////////////
	// UTILITIES
	/////////////////////
	void Init(Vector3 vPosition, Vector3 vLeftEngineOffset,
		Vector3 vRightEngineOffset, Vector3 vPodOffset,
		Vector3 vLeftEngineHalfLengths, Vector3 vRightEngineHalfLengths,
		Vector3 vPodHalfLengths, char* pPodMeshName, char* pEngineMeshName);
	// update and sync graphics,physics, and game logic
	virtual void Update(float dt);
	virtual void Shutdown();
	void UpdateTransform();
	// aligns the engines and pod with turbine position
	void AlignComponentsWithPosition();
	// initializes the health of the turbine via the pod + engine hp
	virtual void InitHealth();
	// resets all pertinent turbine data
	void Reset();
	// recharges the energy bar at recharge rate
	void EnergyRecharge(float dt);

	//////////////////////
	// ACCESSORS
	/////////////////////
	
	// get max linear speed
	float GetMaxLinearSpeed(){return m_fMaxLinearSpeed;}
	// get current speed
	float GetCurrentSpeed(){return m_fCurrentSpeed;}
	// get current velocity speed
	float GetCurrentVelocitySpeed();
	// get max turning speed
	float GetMaxTurningSpeed(){return m_fMaxTurningSpeed;}
	// get armor rating
	float GetArmorRating(){return m_fArmorRating;}
	// get heat resistance
	float GetHeatResistance(){return m_fEndurance;}
	// get boost multiplier
	float GetBoostMultiplier(){return m_fBoostMultiplier;}
	// get repair rate
	float GetRepairRate(){return m_fRepairRate;}
	// get camera id
	int GetCamID(){return m_iCamID;}
	// get pod position
	Vector3* GetPodPosition();
	// get pod
	Pod* GetPod();
	// get left engine
	Engine* GetLeftEngine();
	// get right engine
	Engine* GetRightEngine();
	// gets health of turbine 
	float GetHealth(){return m_fHealth;}
	// gets Id of power coupling
	int GetPowerCouplingID(){return m_iPowerCouplingID;}
	// gets max energy
	float GetMaxEnergy(){return m_fMaxEnergy;}
	// gets current energy
	float GetCurrentEnergy(){return m_fCurrentEnergy;}
	// gets energy recharge rate
	float GetEnergyRechargeRate(){return m_fEnergyRechargeRate;}
	// gets last damage type
	int GetLastDamageType(){return m_eLastDamageType;}

	//////////////////////
	// MUTATORS
	/////////////////////
	// Sets position of entity 
	virtual void SetPosition(Vector3 vPos);
	// Sets position of entity by component
	virtual void SetPosition(float x, float y, float z);
	// sets a single component of the entity's position
	virtual void SetPosition(int idx, float fValue);
	// Set max linear speed
	void SetMaxLinearSpeed(float fValue){m_fMaxLinearSpeed = fValue;}
	// Set current speed
	void SetCurrentSpeed(float fValue);
	// set left engine speed
	void SetLeftEngineSpeed(float fValue);
	// set right engine speed
	void SetRightEngineSpeed(float fValue);
	// Set max turning speed
	void SetMaxTurningSpeed(float fValue){m_fMaxTurningSpeed = fValue;}
	// Set armor rating
	void SetArmorRating(float fValue){m_fArmorRating = fValue;}
	// Set heat resistance
	void SetHeatResistance(float fValue){m_fEndurance = fValue;}
	// Set boost multiplier
	void SetBoostMultiplier(float fValue){m_fBoostMultiplier = fValue;}
	// Set repair rate
	void SetRepairRate(float fValue){m_fRepairRate = fValue;}
	// brake on left side
	void BrakeLeftEngine(float fValue);
	// brake on right side
	void BrakeRightEngine(float fValue);
	// sets entity's orientation by yaw/pitch/roll
	virtual void SetOrientation(float yaw, float pitch, float roll);
	// sets entity's orientation by quaternion
	virtual void SetOrientation(Quaternion qOrientation);
	// rotates entity's position by yaw/pitch/roll
	virtual void RotateBody(float yaw, float pitch, float roll);		
	// damages turbine - all components equally (divides damage by 3)
	void ApplyDamage(float fAmount, int damageType);
	// damages an individual component - 0 = pod, 1= left engine, 2= right engine
	void ApplyDamage(float fAmount, int iComponent, int damageType);
	// sets health to an absolute value
	void SetHealth(float fValue){m_fHealth = fValue;}
	// lets you modify health - adds this value to health
	void ModifyHealth(float fValue);
	// repairs the turbine components
	void Repair(float dt);
	// sets max energy
	void SetMaxEnergy(float fEnergy){m_fMaxEnergy = fEnergy;}
	// sets current energy
	void SetCurrentEnergy(float fEnergy){m_fCurrentEnergy = fEnergy;}
	// allows you to add to the current energy
	void ModifyCurrentEnergy(float fAmount){m_fCurrentEnergy += fAmount;}
	// sets the energy recharge rate
	void SetEnergyRechargeRate(float fAmount){m_fEnergyRechargeRate = fAmount;}
	// Lets you push the turbine down along y axis
	void PushTurbineDown(float fAmount, float dt);
	// lets you roll the turbine left or right
	void RotateTurbine(float fAmount, float dt);
	// set the fake friction
	void SetFriction(float friction){m_fFriction = friction;}
};

