#include "MKIII.h"


MKIII::MKIII(Vector3 vPosition, Vector3 vLeftEngineOffset,
		Vector3 vRightEngineOffset, Vector3 vPodOffset,
		Vector3 vLeftEngineHalfLengths, Vector3 vRightEngineHalfLengths,
		Vector3 vPodHalfLengths, char* pPodMeshName, char* pEngineMeshName)
{		

	// init all values
	// max linear speed
	m_fMaxLinearSpeed = 0.0f;
	// current speed
	m_fCurrentSpeed = 0.0f;
	// max turning speed
	m_fMaxTurningSpeed = 0.0f;
	// armor rating
	m_fArmorRating = 0.0f;
	// heat resistance
	m_fEndurance = 0.0f;
	// boost multiplier
	m_fBoostMultiplier = 0.0f;
	// repair rate
	m_fRepairRate = 0.0f;
	
	m_vPosition =  + vPosition;
	// create left engine
	//m_pLeftEngine = new Engine(vLeftEngineOffset + vPosition, vLeftEngineHalfLengths, pEngineMeshName);
	//// create right engine
	//m_pRightEngine = new Engine(vRightEngineOffset  + vPosition ,vRightEngineHalfLengths, pEngineMeshName);
	//// create pod
	//m_pPod = new Pod(vPodOffset + vPosition, vPodHalfLengths, pPodMeshName);

	////// connect two engines with rods
	//HM->AddFixedConstraint(m_pLeftEngine->GetBody()->getUid(),m_pRightEngine->GetBody()->getUid(),&hkVector4(.5,0,0,1),&hkVector4(-.5,0,0,1)  );
	//HM->AddFixedConstraint(m_pRightEngine->GetBody()->getUid(),m_pLeftEngine->GetBody()->getUid(),&hkVector4(-.5,0,0,1),&hkVector4(.5,0,0,1)  );

	//// connect engines to pod with ball-socket
	//HM->AddBallSocketConstraint(m_pLeftEngine->GetBody()->getUid(),m_pPod->GetBody()->getUid() );
	//HM->AddBallSocketConstraint(m_pRightEngine->GetBody()->getUid(),m_pPod->GetBody()->getUid() );
}


MKIII::~MKIII(void)
{
}
