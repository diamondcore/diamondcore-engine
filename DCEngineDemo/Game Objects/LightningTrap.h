#ifndef __LIGHTNINGTRAP_H
#define __LIGHTNINGTRAP_H

#include "Turbine.h"

class LightningTrapMast;

class LightningTrap : public BaseGameEntity
{
protected:
	Turbine *m_pTarget;


	std::shared_ptr<LightningTrapMast> m_pConnectPoint;
	Vector2 m_vTimerRange;
	float m_fTimer;
	float m_fLightningPower;
	float m_fDamageRange;
	int m_iTrapID;
	int m_iLightningBoltID;
	bool m_bLightningOn;

	float getDistance(Vector3 point);

	Vector3 m_vBoltOffset;

public:
	LightningTrap();
	LightningTrap(std::string mesh);
	~LightningTrap();

	virtual void SetPosition(Vector3 pos);
	// Sets entity's orientation
	virtual void SetOrientation(Quaternion pOrientation);
	// sets entity's orientation by yaw/pitch/roll
	virtual void SetOrientation(float yaw, float pitch, float roll);
	// sets entity's scale
	virtual void SetScale(Vector3 scale);

	virtual void SetMeshName(char* pMeshName);

	virtual void Update(float dt);
	virtual void Load(TiXmlElement* node);		// Loads the attributes
	virtual void Save(TiXmlElement* obj);	// Call this original function at the begining of the function that overwrites this
	virtual std::shared_ptr<BaseGameEntity>	Clone();	// Clone these attributes into a new BaseGameEntity
	virtual void Init();						// called at the end of load
	virtual void Shutdown();					// cleans the entity

	void SetLightningBoltID(int ID){m_iLightningBoltID = ID;}
	void SetLightningPower(float power){m_fLightningPower = power;}
	void SetTimerRange(float chargeTime, float dischargeTime){m_vTimerRange = Vector2(chargeTime, dischargeTime);}
	void SetConnectorPosition(Vector3 pos);
	int GetLightningID(){return m_iLightningBoltID;}

	Vector3 GetBoltOffset(){return m_vBoltOffset;}
	void SetBolt();

	void LinkedShutdown();
};

//This class serves as the 2nd mesh thats connected to LightningTrap.
//Unless explicitly necessary, I do not recommend directly modifying this class.
//Do that through LightningTrap

class LightningTrapMast : public BaseGameEntity
{
protected:
	LightningTrap* m_pTrapParent;
	int m_iMastID;

	Vector3 m_vBoltOffset;

public:
	LightningTrapMast();
	LightningTrapMast(std::string mesh);


	virtual void SetPosition(Vector3 pos);
	// Sets entity's orientation
	virtual void SetOrientation(Quaternion pOrientation);
	// sets entity's orientation by yaw/pitch/roll
	virtual void SetOrientation(float yaw, float pitch, float roll);

	virtual void SetScale(Vector3 scale);

	// Remove the loading and saving functionality of this class.
	// This is handled in the parent Lightning Trap.
	virtual void Load(TiXmlElement* node){}		// Loads the attributes
	virtual void Save(TiXmlElement* obj){}	// Call this original function at the begining of the function that overwrites this
	
	virtual void Init();						// called at the end of load
	virtual void Shutdown();					// cleans the entity

	void SetParent(LightningTrap *trapParent);

	Vector3 GetBoltOffset(){return m_vBoltOffset;}
	void SetBoltOffset(Vector3 v){m_vBoltOffset = v;}

	friend class LightningTrap;
};

#endif