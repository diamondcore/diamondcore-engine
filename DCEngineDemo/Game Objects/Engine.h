#pragma once
#include "PhysicsEntity.h"


/*
	Engine Class - racers can have different amounts of engines, 
	however, there will only be two that actually have properties.
	The rest will be graphics-only.
*/
// Health
#define MKIVHEALTH 75.0f
#define MKIIIHEALTH 60.0f
#define X95HEALTH 45.0f
// Energy
#define MKIVENERGY 40.0f
#define MKIIIENERGY 60.0f
#define X95ENERGY 70.0f

// Largest Possible Values
#define MAX_ENGINE_HEALTH max(MKIVHEALTH,max(MKIIIHEALTH,X95HEALTH))
#define MAX_ENGINE_ENERGY max(MKIVENERGY,max(MKIIIENERGY,X95ENERGY))

class Engine : public PhysicsEntity
{
private:
	
	// half lengths of the engine
	Vector3 m_vHalfLengths;
	
	// engine speed
	float m_fEngineSpeed;
	// capped engine speed
	float m_fCappedSpeed;

	// id for sparks and fire
	int		m_iSparkID;
	int		m_iLightID;
	float	m_fPrevEngineSpeed;

	// brake amount
	float m_fEngineBrakeAmount;
	
	// max energy
	float m_fMaxEnergy;
	// current energy
	float m_fCurrentEnergy;
	
public:
	Engine(void);
	Engine(Vector3 vOffset, Vector3 vHalfLengths, char* pMeshName);
	~Engine(void);

	virtual void Update(float dt);
	virtual void Init();						// called at the end of load
	virtual void InitHealth();
	
	virtual void Shutdown();

	float GetSpeed(){return m_fEngineSpeed;}
	void  SetSpeed(float speed);
	float GetBrake(){return m_fEngineBrakeAmount;}
	void SetBrake(float fAmount);
	
	void	ApplyDamage(float damage);
	void	ResetAllDamage();
	float	GetHealth(){return m_fHealth;}
	
	void	SetEnergy(float energy){m_fCurrentEnergy = energy;}
	void	UseEnergy(float energy);
	float	GetEnergy(){return m_fCurrentEnergy;}
	float	GetMaxEnergy(){return m_fMaxEnergy;}

	Vector3 GetHalfLengths(){return m_vHalfLengths;}
	



};

