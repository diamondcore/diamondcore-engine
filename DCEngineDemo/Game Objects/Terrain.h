#pragma once
#include "PhysicsEntity.h"

class Vector3;

class Terrain :
	public PhysicsEntity
{
	Vector2 m_vMaxPos;

public:

	Terrain(Vector3 vertList[], WORD indexList[], Vector2 dimentions, Vector2 size);
	~Terrain(void);

	void Init() {}

	float GetHeightAt(Vector3 position);

private:

	bool IsPosValid(Vector3 pos);
	MySampledHeightFieldShape* mHeightField;

};

