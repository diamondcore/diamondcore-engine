#pragma once

#include "BaseGameEntity.h"

class LightEntity	: public BaseGameEntity
{
	Vector4	m_vIntensity;
	int		m_iPower;
	int		m_iLightID;

public:
	LightEntity();
	LightEntity(Vector4 intentsity);
	LightEntity(Vector4 intentsity, int power);
	virtual ~LightEntity();

	//////////////////////
	// UTILITIES
	/////////////////////
	// update and sync graphics,physics, and game logic
	virtual void Load(TiXmlElement* node);		// Loads the attributes
	virtual void Save(TiXmlElement* obj);	// Call this original function at the begining of the function that overwrites this
	virtual std::shared_ptr<BaseGameEntity>	Clone();	// Clone these attributes into a new BaseGameEntity
	virtual void Init();						// called at the end of load
	virtual void Shutdown();					// cleans the entity

	//////////////////////
	// MUTATORS
	/////////////////////
	// Sets position of entity 
	virtual void SetPosition(Vector3 vPos);
	// Sets position of entity by component
	virtual void SetPosition(float x, float y, float z);
	// sets a single component of the entity's position
	virtual void SetPosition(int idx, float fValue);
	virtual void SetScale(Vector3 vScale);
};