#pragma once
#include "PhysicsEntity.h"

/*
	Pod class - the is where the driver sits, its pulled
	by the engines.
*/
// Health
#define MKIV_POD_HEALTH 50.0f
#define MKIII_POD_HEALTH 30.0f
#define X95_POD_HEALTH 10.0f
// Energy
#define MKIV_POD_ENERGY 40.0f
#define MKIII_POD_ENERGY 60.0f
#define X95_POD_ENERGY 70.0f

// Largest Possible Values
#define MAX_POD_HEALTH max(MKIV_POD_HEALTH,max(MKIII_POD_HEALTH,X95_POD_HEALTH))
#define MAX_POD_ENERGY max(MKIV_POD_ENERGY,max(MKIII_POD_ENERGY,X95_POD_ENERGY))

class Pod : public PhysicsEntity
{
private:
	// half lengths of the pod
	Vector3 m_vHalfLengths;
	
	// max energy
	float m_fMaxEnergy;
	// current energy
	float m_fCurrentEnergy;

public:
	Pod(void);
	Pod(Vector3 vOffset, Vector3 vHalfLengths, char* pMeshName );
	~Pod(void);
	void Shutdown();
	
	virtual void Init();						// called at the end of load
	virtual void InitHealth();

	// get pointer to position
	Vector3* GetPositionAddr(){ return &m_vPosition; }
	
	// get health
	float	GetHealth(){return m_fHealth;}
	void	ApplyDamage(float damage);
	void	ResetAllDamage();
	
	void	SetEnergy(float energy){m_fCurrentEnergy = energy;}
	void	UseEnergy(float energy);
	float	GetEnergy(){return m_fCurrentEnergy;}
	float	GetMaxEnergy(){return m_fMaxEnergy;}

	Vector3 GetHalfLengths(){return m_vHalfLengths;}

};

