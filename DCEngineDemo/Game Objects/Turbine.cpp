#include "Turbine.h"
#include "Engine.h"
#include "Pod.h"
#include "Turret.h"
#include "../Controls/ControlsInclude.h"
#include "../Controls/Camera/CameraRig.h"
#include "../WorldManager.h"
#include "../UI/ScreenManager.h"
#include "Terrain.h"
#include "../FSM/FSM.h"
#include "../FSM/IdleGearState.h"

#define SND ScreenMan->GetMainCore()->GetSoundCore()


Turbine::Turbine(void)
{
	
}

Turbine::Turbine(Vector3 vPosition, Vector3 vLeftEngineOffset,
		Vector3 vRightEngineOffset, Vector3 vPodOffset,
		Vector3 vLeftEngineHalfLengths, Vector3 vRightEngineHalfLengths,
		Vector3 vPodHalfLengths, char* pPodMeshName, char* pEngineMeshName)
{
	Init(vPosition, vLeftEngineOffset,
		vRightEngineOffset, vPodOffset,
		vLeftEngineHalfLengths,vRightEngineHalfLengths,
		vPodHalfLengths, pPodMeshName, pEngineMeshName);
}
void Turbine::Init(Vector3 vPosition, Vector3 vLeftEngineOffset,
		Vector3 vRightEngineOffset, Vector3 vPodOffset,
		Vector3 vLeftEngineHalfLengths, Vector3 vRightEngineHalfLengths,
		Vector3 vPodHalfLengths, char* pPodMeshName, char* pEngineMeshName)
{

	// init all values
	// create fsm
	m_pFSM = new FiniteStateMachine<Turbine>(this);
	// max linear speed
	m_fMaxLinearSpeed = 0.0f;
	// current speed
	m_fCurrentSpeed = 0.0f;
	// left engine brake
	m_fLeftEngineBrake = 0.0f;
	// right engine brake
	m_fRightEngineBrake = 0.0f;
	// max turning speed
	m_fMaxTurningSpeed = 0.0f;
	// armor rating
	m_fArmorRating = 0.0f;
	// heat resistance
	m_fEndurance = 0.0f;
	// boost multiplier
	m_fBoostMultiplier = 0.0f;
	// repair rate
	m_fRepairRate = 10.0f;
	// recharge rate
	m_fEnergyRechargeRate = 10.0f;
	// max energy
	m_fMaxEnergy = m_fCurrentEnergy = 100.0f;
	// death animation
	m_bDeathAnimation = false;
	// death stage
	m_iDeathStage = 0;
	// death timer
	m_fDeathAnimTimer = 0.0f;
	// fake friction
	m_fFriction = -40.0f;
	
	
	m_vPosition =  + vPosition;
	// create left engine
	m_pLeftEngine = new Engine(vLeftEngineOffset, vLeftEngineHalfLengths, pEngineMeshName);
	m_pLeftEngine->SetPosition(vLeftEngineOffset + m_vPosition);
	m_pLeftEngine->SetRotationOffset(Quaternion(Vector3(0,0,1),3.14));
	// create right engine
	m_pRightEngine = new Engine(vRightEngineOffset,vRightEngineHalfLengths, pEngineMeshName);
	m_pRightEngine->SetPosition(vRightEngineOffset + m_vPosition);
	//m_pRightEngine->SetRotationOffset(Quaternion(Vector3(0,1,0),3.14)  );
	// create pod
	m_vPodSize = vPodHalfLengths * 2.0f;
	m_pPod = new Pod(vPodOffset, vPodHalfLengths, pPodMeshName);
	m_pPod->SetPosition(vPodOffset + m_vPosition);
	//m_pPod->SetRotationOffset(Quaternion(Vector3(0,1,0),3.14));
	
#if 1
	//// connect two engines with rods
	HM->AddFixedConstraint(m_pLeftEngine->GetBody()->getUid(),m_pRightEngine->GetBody()->getUid(),&hkVector4(.5,0,0,1),&hkVector4(-.5,0,0,1) );
	HM->AddFixedConstraint(m_pRightEngine->GetBody()->getUid(),m_pLeftEngine->GetBody()->getUid(),&hkVector4(-.5,0,0,1),&hkVector4(.5,0,0,1) );

	// connect engines to pod with ball-socket
	//HM->AddBallSocketConstraint(m_pLeftEngine->GetBody()->getUid(),m_pPod->GetBody()->getUid() );
	//HM->AddBallSocketConstraint(m_pRightEngine->GetBody()->getUid(),m_pPod->GetBody()->getUid() );
	
	// create hinge from engine to pod
	HM->AddLimitedHingeConstraint(m_pLeftEngine->GetBody()->getUid(),m_pPod->GetBody()->getUid(), &hkVector4(1.0f, 0.0f, 0.0f, 0.0f), -HK_REAL_PI/2.0f, HK_REAL_PI/2.0f);
	HM->AddLimitedHingeConstraint(m_pRightEngine->GetBody()->getUid(),m_pPod->GetBody()->getUid(), &hkVector4(1.0f, 0.0f, 0.0f, 0.0f), -HK_REAL_PI/2.0f, HK_REAL_PI/2.0f);

	////////////////////////
	// CAMERA CREATION
	////////////////////////
	m_iCamID = CamMgr->CreateCamera(RIG);
	CameraRig* cam = (CameraRig*)CamMgr->GetCameraControlByID(m_iCamID);

	// chase cam
	cam->AddCamera(0,5,-15,m_vPosition );
	// first person
	//cam->AddCamera(0,2.5,-7.7, m_vPosition );
	//cam->AddCamera(m_pPod->GetOffset().x,m_pPod->GetOffset().y,m_pPod->GetOffset().z, m_vPosition);
	// side cam
	cam->AddCamera(15,5,0,m_vPosition );

	// attach rig
	cam->AttachRig(this);
	cam->GetCurrentCamView()->CalculateViewMatrix();

#endif
	m_pLeftEngine->Init();
	m_pRightEngine->Init();

	// Create the connectors between the engines and the pod
	m_vLeftConnector = m_pLeftEngine->GetPosition();
	m_pLeftConnector = new Laser(&m_vLeftConnector, m_pPod->GetPositionAddr(), 0, 0,"Box.x");
	m_pLeftConnector->Init();
	m_pLeftConnector->SetColor(Vector4(0.05f, 0.05f, 0.05f, 1.0f));
	m_pLeftConnector->SetScale(Vector3(0.05f, 0.05f, 1.0f));
	
	m_vRightConnector = m_pRightEngine->GetPosition();
	m_pRightConnector = new Laser(&m_vRightConnector, m_pPod->GetPositionAddr(), 0, 0,"Box.x");
	m_pRightConnector->Init();
	m_pRightConnector->SetColor(Vector4(0.05f, 0.05f, 0.05f, 1.0f));
	m_pRightConnector->SetScale(Vector3(0.05f, 0.05f, 1.0f));
	m_iLimpConnector = 0;

	// initialize health
	InitHealth();

	ScreenMan->GetGraphics()->AddBoltObject(m_iPowerCouplingID,
											vLeftEngineOffset + m_vPosition,
											vRightEngineOffset + m_vPosition,
											0.05f,
											0.3f,
											0.5f,
											0,
											0,
											D3DXVECTOR4(1, 0, 1, 1)
											);

	InitHealth();

	m_fMaxHealth = m_fHealth;
	
	// Set the max energy based on the pods and engines
	m_fMaxEnergy = m_pPod->GetMaxEnergy() + m_pLeftEngine->GetMaxEnergy() + m_pRightEngine->GetMaxEnergy();

	//////////////////////
	// INIT SOUNDS
	//////////////////////

	// initialize all level intro sounds
	iStartupSound = SND->CreateInstance2D("Content/Sounds/FX/22K/sfx_pod_ani_start.wav",true,1,false) ;
	iIdleEngineSound = SND->CreateInstance2D("Content/Sounds/FX/22K/sfx_pod_maw_slow_loop.wav",true,1, true) ;
	iShiftSound = SND->CreateInstance2D("Content/Sounds/FX/22K/sfx_pod_ani_shift1.wav",true,1,false) ;
	iLowGearSound = SND->CreateInstance2D("Content/Sounds/FX/22K/sfx_pod_jet_steady_loop.wav",true,1,true) ;
	
	// set initial state
	m_pFSM->SetInitialState(IDLEGEAR);
}


Turbine::~Turbine(void)
{

}

//////////////////////
// UTILITIES
/////////////////////
// update and sync graphics,physics, and game logic
void Turbine::Update(float dt)
{
	if(m_bDeathAnimation)
	{
		m_fDeathAnimTimer += dt;
		switch(m_iDeathStage)
		{
		case 0:
			{
				if(m_fDeathAnimTimer > 2)//0.5f)
				{
					// Disable the constriants between the engines
					HM->m_HavokWorld->getWorld()->lock();
					HM->m_ConstraintList[m_pLeftEngine->GetBody()->getUid()][m_pRightEngine->GetBody()->getUid()]->disable();
					HM->m_ConstraintList[m_pRightEngine->GetBody()->getUid()][m_pLeftEngine->GetBody()->getUid()]->disable();
					HM->m_HavokWorld->getWorld()->unlock();
					
					m_iDeathStage++;		// Move to the next stage
					m_fDeathAnimTimer = 0.0f; // Reset timer
				}
				break;
			}
		case 1:
			{
				if(m_fDeathAnimTimer > 2.0f)
				{
					// Randomly disable/disconnect one of the engines from the pod
					int r = rand() % 100 + 1;
					HM->m_HavokWorld->getWorld()->lock();
					if(r > 50)
					{
						HM->m_ConstraintList[m_pLeftEngine->GetBody()->getUid()][m_pPod->GetBody()->getUid()]->disable();
						m_iLimpConnector = -1;	// Stop updating this connector
					}
					else
					{
						HM->m_ConstraintList[m_pRightEngine->GetBody()->getUid()][m_pPod->GetBody()->getUid()]->disable();
						m_iLimpConnector = 1;	// stop updating this connector
					}
					HM->m_HavokWorld->getWorld()->unlock();

					// Make the power coupling more unstable
					auto GFX = ScreenMan->GetGraphics();
					GFX->SetBoltObjThickness(m_iPowerCouplingID, 0.1f);
					GFX->SetBoltObjColor(m_iPowerCouplingID, D3DXVECTOR4(1, .5, .1, 1));
					GFX->SetBoltObjArcScale(m_iPowerCouplingID, 0.1f);

					m_iDeathStage++;		// Move to the next stage
					m_fDeathAnimTimer = 0.0f; // Reset timer
				}
				break;
			}
		case 2:
			{
				if(m_fDeathAnimTimer > 1.5f)
				{
					// Remove the Power Coupling
					ScreenMan->GetGraphics()->RemoveBoltObject(m_iPowerCouplingID);
					m_iDeathStage++;		// Move to the next stage
					m_fDeathAnimTimer = 0.0f; // Reset timer
				}
				break;
			}
		default:
			break;
		};

		// Apply force
		m_pLeftEngine->SetSpeed(140.0f);
		m_pRightEngine->SetSpeed(140.0f);
		//if(m_iDeathStage > 1)
		//{
			// Apply upwards force
			Vector3 pos = m_pLeftEngine->GetPosition();
			HM->ApplyForce(m_pLeftEngine->GetBody()->getUid(), dt, 0.0f, 2.0f, 0.0f, pos.x, pos.y, pos.z);
			pos = m_pRightEngine->GetPosition();
			HM->ApplyForce(m_pRightEngine->GetBody()->getUid(), dt, 0.0f, 2.0f, 0.0f, pos.x, pos.y, pos.z);
		//}
	}
	// Normal alive Update
	UpdateHover(dt);

	// update turbine's position with pod
	// get forward vec, scale by negative offset
	m_vPosition = m_pPod->GetOrientation().GetForward().ScaleReturn(-m_pPod->GetOffset().z);
	// translate by position
	m_vPosition = m_vPosition + m_pPod->GetPosition();

	// orientation should be same as pods
	m_qOrientation = m_pPod->GetOrientation();

	// update transform
	UpdateTransform();

	// recharge energy
	if(!m_bDeathAnimation)
		EnergyRecharge(dt);

	/////////////////////
	// FORWARD MOVEMENT
	/////////////////////
	// left engine
	HM->GoForward(m_pLeftEngine->GetBody()->getUid(),dt,m_pLeftEngine->GetSpeed()-m_pLeftEngine->GetBrake());
	// right engine
	HM->GoForward(m_pRightEngine->GetBody()->getUid(),dt,m_pRightEngine->GetSpeed()-m_pRightEngine->GetBrake());
	///////////////////////
	//// BRAKES
	///////////////////////
	//// left brake
	//HM->GoForward(m_pLeftEngine->GetBody()->getUid(), dt, -m_fLeftEngineBrake);
	//// right brake
	//HM->GoForward(m_pRightEngine->GetBody()->getUid(), dt, -m_fRightEngineBrake);
	
	
	/////////////////////
	// COMPONENT UPDATES
	/////////////////////
	m_pLeftEngine->Update(dt);
	m_pRightEngine->Update(dt);
	m_pPod->Update(dt);
	ScreenMan->GetGraphics()->SetBoltObjPositions(m_iPowerCouplingID, m_pLeftEngine->GetPosition(), m_pRightEngine->GetPosition());
	if(m_iLimpConnector != -1)
		m_vLeftConnector = m_pLeftEngine->GetPosition() + m_pLeftEngine->GetOrientation().GetRight() * m_pLeftEngine->GetHalfLengths().x;	
	else
	{
		float diffY = m_vLeftConnector.y - m_vRightConnector.y;
		m_vLeftConnector = m_pRightEngine->GetPosition() + m_pRightEngine->GetOrientation().GetRight().NormalizeRet() * (m_pLeftEngine->GetOffset().x - m_pLeftEngine->GetHalfLengths().x);
		m_vLeftConnector += m_pRightEngine->GetOrientation().GetUp().NormalizeRet() * diffY;
	}
	if(m_iLimpConnector != 1)
		m_vRightConnector = m_pRightEngine->GetPosition() - m_pRightEngine->GetOrientation().GetRight() * m_pRightEngine->GetHalfLengths().x;
	else
	{
		float diffY = m_vRightConnector.y - m_vLeftConnector.y;
		m_vRightConnector = m_pLeftEngine->GetPosition() + m_pLeftEngine->GetOrientation().GetRight().NormalizeRet() * (m_pRightEngine->GetOffset().x + m_pRightEngine->GetHalfLengths().x);
		m_vRightConnector += m_pLeftEngine->GetOrientation().GetUp().NormalizeRet() * diffY;
	}
	m_pLeftConnector->Update(dt);
	m_pRightConnector->Update(dt);

	// speed update
	m_fCurrentSpeed = (m_pLeftEngine->GetSpeed() + m_pRightEngine->GetSpeed()) / 2.0f;

	///////////////
	// FSM UPDATE
	///////////////
	m_pFSM->Update(dt);
}

void Turbine::UpdateHover(float dt)
{
	float boatMass = m_pPod->GetBody()->getMass();
	float boatDensity = 0.25f;
	float fluidDensity = 1.5f;
	//figure out the dimensions of our boat
	float width = m_vPodSize.x * 2;
	float depth = m_vPodSize.z * 2;
	float height = m_vPodSize.y * 2;	

	// Get height of hover plane relative to the object we're lifting.
	auto pos = m_pPod->GetPosition();
	auto terrainPos = ScreenMan->GetEntityManager()->GetTerrain()->GetHeightAt(pos);
	float hoverHeight = terrainPos + 2.0f;

	// Apply damage if we are out of the level
	if(terrainPos == FLT_MIN)
		ApplyDamage(100.0f * dt, e_SelfDamage);

	float displacedVolume = width *  depth * MathUtil::Clamp(hoverHeight - (pos.y - height * 0.5f), 0.0f, height);
	//calculate the boat's mass - we are only using part of the volume
	// as we assume the rest is filled with air
	boatMass = boatDensity * (width * depth * height * 0.75f);

	float gravForce = boatMass * -9.8f;
	float buoyancyForce = displacedVolume * fluidDensity * 9.8f;
	float dragforce = 0;

	auto vel = m_pPod->GetBody()->getLinearVelocity();
	auto yVel = (float)vel.getComponent(1);
	if (pos.y < hoverHeight)
	{
		// TODO: Scale the resulting force by how submerged the object is.
		// Technically, the ship should never be completely underneath the plane.

		//Fd = (1/2) * v ^ 2 * fluidDensity * ( area ) * dragCoefficient
		dragforce = -0.5f * (yVel * yVel) * fluidDensity * (width * depth) * 1.33f;

		// Push the object in the opposite direction
		if (yVel < 0)
			dragforce *= -1;
	}

	float netForce = gravForce + buoyancyForce + dragforce;

	// Experimental code to hover based on the vehicle's velocity.
	//netForce *= MathUtil::Clamp(((float)vel.length3() - yVel * yVel) / 2.0f, 0.0f, 1.0f);

	auto fakeFriction = Vector2(vel.getComponent(0), vel.getComponent(2));
	fakeFriction.Normalize();

	fakeFriction *= m_fFriction;//-40.0f;

	HM->ApplyForce(m_pPod->GetBody()->getUid(), dt, fakeFriction.x, netForce * 0.5f, fakeFriction.y);
	HM->ApplyForce(m_pLeftEngine->GetBody()->getUid(), dt, fakeFriction.x, netForce * 0.45f, fakeFriction.y);
	HM->ApplyForce(m_pRightEngine->GetBody()->getUid(), dt, fakeFriction.x, netForce * 0.45f, fakeFriction.y);

}

void Turbine::Shutdown()
{
	// Remove constriants
	HM->RemoveConstraint(m_pLeftEngine->GetBody()->getUid(),m_pRightEngine->GetBody()->getUid());
	HM->RemoveConstraint(m_pRightEngine->GetBody()->getUid(),m_pLeftEngine->GetBody()->getUid());
	HM->RemoveConstraint(m_pLeftEngine->GetBody()->getUid(),m_pPod->GetBody()->getUid());
	HM->RemoveConstraint(m_pRightEngine->GetBody()->getUid(),m_pPod->GetBody()->getUid());

	// Remove the connectors
	m_pLeftConnector->Shutdown();
	m_pRightConnector->Shutdown();
	delete m_pLeftConnector;
	delete m_pRightConnector;

	// Remove the engines and pod
	m_pLeftEngine->Shutdown();
	m_pRightEngine->Shutdown();
	m_pPod->Shutdown();
	delete m_pLeftEngine;
	delete m_pRightEngine;
	delete m_pPod;
	CamMgr->DeleteCameraControl(m_iCamID);

	ScreenMan->GetGraphics()->RemoveBoltObject(m_iPowerCouplingID);
}

// get pod position
Vector3* Turbine::GetPodPosition()
{
	return m_pPod->GetPositionAddr();
}

void Turbine::UpdateTransform()
{
	// get rotation matrix from orientation
	D3DXMatrixRotationQuaternion(&m_mTransform,&m_qOrientation);

	// set translation part
	m_mTransform._14 = m_vPosition.x;
	m_mTransform._24 = m_vPosition.y;
	m_mTransform._34 = m_vPosition.z;

}

// aligns the engines and pod with turbine position
void Turbine::AlignComponentsWithPosition()
{
	// set the component's position
	m_pPod->SetPosition(m_vPosition + m_pPod->GetOffset());
	m_pLeftEngine->SetPosition(m_vPosition + m_pLeftEngine->GetOffset());
	m_pRightEngine->SetPosition(m_vPosition + m_pRightEngine->GetOffset());
}

// initializes the health of the turbine via the pod + engine hp
void Turbine::InitHealth()
{	
	m_fHealth = m_pPod->GetHealth() + m_pLeftEngine->GetHealth() + m_pRightEngine->GetHealth();

	// Check if we are dead
	if(m_fHealth <= 0.0f)
	{
		m_bDeathAnimation = true;

		// Overload the couplink effect
		auto GFX = ScreenMan->GetGraphics();
		GFX->SetBoltObjThickness(m_iPowerCouplingID, 0.15f);
		GFX->SetBoltObjColor(m_iPowerCouplingID, D3DXVECTOR4(1, 0, 0, 1));
		GFX->SetBoltObjArcScale(m_iPowerCouplingID, 1.0f);
		((CameraRig*)CamMgr->GetCameraControlByID(m_iCamID))->SetLooseRig(true);
	}
}

void Turbine::Reset()
{
	HM->m_HavokWorld->m_pPhysicsWorld->lock();
	HM->m_HavokWorld->getWorld()->lock();

	m_pLeftEngine->GetBody()->setLinearVelocity(hkVector4(0,0,0) );
	m_pRightEngine->GetBody()->setLinearVelocity(hkVector4(0,0,0) );
	m_pPod->GetBody()->setLinearVelocity(hkVector4(0,0,0) );
	
	m_pLeftEngine->GetBody()->setAngularVelocity(hkVector4(0,0,0) );
	m_pRightEngine->GetBody()->setAngularVelocity(hkVector4(0,0,0) );
	m_pPod->GetBody()->setAngularVelocity(hkVector4(0,0,0) );

	HM->m_HavokWorld->m_pPhysicsWorld->unlock();
	HM->m_HavokWorld->getWorld()->unlock();

	// Enable the constraints
	HM->m_HavokWorld->getWorld()->lock();
	HM->m_ConstraintList[m_pLeftEngine->GetBody()->getUid()][m_pRightEngine->GetBody()->getUid()]->enable();
	HM->m_ConstraintList[m_pRightEngine->GetBody()->getUid()][m_pLeftEngine->GetBody()->getUid()]->enable();
	HM->m_ConstraintList[m_pLeftEngine->GetBody()->getUid()][m_pPod->GetBody()->getUid()]->enable();
	HM->m_ConstraintList[m_pRightEngine->GetBody()->getUid()][m_pPod->GetBody()->getUid()]->enable();	
	HM->m_HavokWorld->getWorld()->unlock();

	// Recreate the bolt object
	ScreenMan->GetGraphics()->RemoveBoltObject(m_iPowerCouplingID);
	ScreenMan->GetGraphics()->AddBoltObject(m_iPowerCouplingID,
						m_pLeftEngine->GetOffset() + m_vPosition,
						m_pRightEngine->GetOffset() + m_vPosition,
											0.05f,
											0.3f,
											0.5f,
											0,
											0,
											D3DXVECTOR4(1, 0, 1, 1)
											);
	((CameraRig*)CamMgr->GetCameraControlByID(m_iCamID))->SetLooseRig(false);

	m_pPod->SetEnergy(m_pPod->GetMaxEnergy());
	m_pPod->SetHealth(m_pPod->GetMaxHealth());
	m_pPod->ResetAllDamage();
	/*m_pPod->GetBody()->setAngularVelocity(hkVector4(0,0,0,0));
	m_pPod->GetBody()->setLinearVelocity(hkVector4(0,0,0,0));*/
	m_pLeftEngine->SetEnergy(m_pLeftEngine->GetMaxEnergy());
	m_pLeftEngine->SetHealth(m_pLeftEngine->GetMaxHealth());
	m_pLeftEngine->ResetAllDamage();
	//m_pLeftEngine->GetBody()->setAngularVelocity(hkVector4(0,0,0,0));
	//m_pLeftEngine->GetBody()->setLinearVelocity(hkVector4(0,0,0,0));
	m_pRightEngine->SetEnergy(m_pRightEngine->GetMaxEnergy());
	m_pRightEngine->SetHealth(m_pRightEngine->GetMaxHealth());
	m_pRightEngine->ResetAllDamage();
	//m_pRightEngine->GetBody()->setAngularVelocity(hkVector4(0,0,0,0));
	//m_pRightEngine->GetBody()->setLinearVelocity(hkVector4(0,0,0,0));
	InitHealth();
	// Reset max energy
	m_fMaxEnergy = m_pPod->GetMaxEnergy() + m_pLeftEngine->GetMaxEnergy() + m_pRightEngine->GetMaxEnergy();

	// Reset Death animation variables
	m_bDeathAnimation = false;
	m_fDeathAnimTimer = 0.0f;
	m_iDeathStage = 0;
	m_iLimpConnector = 0;	// Start updating both connectors
}


//////////////////////
// ACCESSORS
/////////////////////


//////////////////////
// MUTATORS
/////////////////////
// Sets position of entity 
void Turbine::SetPosition(Vector3 vPos)
{
	// set turbine's position
	m_vPosition = vPos;

	AlignComponentsWithPosition();

}
// Sets position of entity by component
void Turbine::SetPosition(float x, float y, float z)
{
	// set turbine's position
	m_vPosition.x = x;
	m_vPosition.y = y;
	m_vPosition.z = z;

	AlignComponentsWithPosition();
	
}
// sets a single component of the entity's position
void Turbine::SetPosition(int idx, float fValue)
{
	switch(idx)
	{
	case 0:
		m_vPosition.x = fValue;
	case 1:
		m_vPosition.y = fValue;
	case 2:
		m_vPosition.z = fValue;
	default:
		assert ("PhysicsEntity::SetPosition failed - idx must be a value between 0 and 2"); 
		return;
	};

	AlignComponentsWithPosition();

}

// brake on left side
void Turbine::BrakeLeftEngine(float fValue)
{
	m_pLeftEngine->SetBrake(fValue);
}
// brake on right side
void Turbine::BrakeRightEngine(float fValue)
{
	m_pRightEngine->SetBrake(fValue);
}


// sets entity's orientation by yaw/pitch/roll
void Turbine::SetOrientation(float yaw, float pitch, float roll)
{
	
	m_pPod->SetOrientation(yaw, pitch, roll);
	// step 1: calculate final transforms for pod and engines
	// get rotation matrix
	D3DXMatrixRotationYawPitchRoll(&mScratch, yaw, pitch,roll);
	// multiply by turbine's transform to get turbine's final transform
	
	m_mTransform = mScratch;
	m_mTransform._14 = m_vPosition.x;
	m_mTransform._24 = m_vPosition.y;
	m_mTransform._34 = m_vPosition.z;
	m_mTransform._44 = 1.0f;

	// calc pod's position
	// get forward(z) axis from transform (local coords)
	vScratch = m_pPod->GetOffset();
	vScratch = m_mTransform.Transform(vScratch);
	// set the position of the pod
	m_pPod->SetPosition(vScratch);
	m_pPod->SetOrientation(yaw,pitch,roll);


	// calc left engine's position
	vScratch = m_pLeftEngine->GetOffset();
	vScratch = m_mTransform.Transform(vScratch);
	m_pLeftEngine->SetPosition(vScratch);
	m_pLeftEngine->SetOrientation(yaw,pitch,roll);

	// calc right engine's pos
	vScratch = m_pRightEngine->GetOffset();
	vScratch = m_mTransform.Transform(vScratch);
	m_pRightEngine->SetPosition(vScratch);
	m_pRightEngine->SetOrientation(yaw,pitch,roll);

}

void Turbine::SetOrientation(Quaternion qOrientation)
{
	// first step - set turbine's internal vars
	m_qOrientation = qOrientation;

	// calc pod's position
	// get forward(z) axis from transform (local coords)
	vScratch = m_pPod->GetOffset();
	vScratch = m_vPosition + m_qOrientation.GetForward()*vScratch.z + m_qOrientation.GetRight()*vScratch.x + m_qOrientation.GetUp()*vScratch.y;
	// set the position of the pod
	m_pPod->SetPosition(vScratch);
	m_pPod->SetOrientation(qOrientation);


	// calc left engine's position
	vScratch = m_pLeftEngine->GetOffset();
	vScratch = m_vPosition + m_qOrientation.GetForward()*vScratch.z + m_qOrientation.GetRight()*vScratch.x + m_qOrientation.GetUp()*vScratch.y;
	m_pLeftEngine->SetPosition(vScratch);
	m_pLeftEngine->SetOrientation(qOrientation);

	// calc right engine's pos
	vScratch = m_pRightEngine->GetOffset();
	vScratch = m_vPosition + m_qOrientation.GetForward()*vScratch.z + m_qOrientation.GetRight()*vScratch.x + m_qOrientation.GetUp()*vScratch.y;
	m_pRightEngine->SetPosition(vScratch);
	m_pRightEngine->SetOrientation(qOrientation);
}
// rotates entity's position by yaw/pitch/roll
void Turbine::RotateBody(float yaw, float pitch, float roll)
{
	// translate pod and engines to turbine's position(pseudo - origin)
	vScratch1 = m_pPod->GetPosition() - m_vPosition;
	vScratch2 = m_pLeftEngine->GetPosition() - m_vPosition;
	vScratch3 = m_pRightEngine->GetPosition() - m_vPosition;

	// rotate each of them by the given amount
	D3DXMatrixRotationYawPitchRoll(&mScratch1, yaw,pitch,roll);
	vScratch1 = mScratch1.Transform(vScratch1);
	vScratch2 = mScratch1.Transform(vScratch2);
	vScratch3 = mScratch1.Transform(vScratch3);

	// translate them back
	vScratch1 += m_vPosition;
	vScratch2 += m_vPosition;
	vScratch3 += m_vPosition;
}
// Set current speed
void Turbine::SetCurrentSpeed(float fValue)
{
	m_fCurrentSpeed = fValue;
	m_pLeftEngine->SetSpeed(fValue); 
	m_pRightEngine->SetSpeed(fValue);
}
// set left engine speed
void Turbine::SetLeftEngineSpeed(float fValue)
{
	m_pLeftEngine->SetSpeed(fValue);
}
// set right engine speed
void Turbine::SetRightEngineSpeed(float fValue)
{
	m_pRightEngine->SetSpeed(fValue);
}

// get pod
Pod* Turbine::GetPod()
{
	return m_pPod;
}
// get left engine
Engine* Turbine::GetLeftEngine()
{
	return m_pLeftEngine;
}
// get right engine
Engine* Turbine::GetRightEngine()
{
	return m_pRightEngine;
}

// damages turbine
void Turbine::ApplyDamage(float fAmount, int damageType)
{
	// vibrates the controller based on how significant the damage is.
	auto inputCore = ScreenMan->GetMainCore()->GetInputCore();
	inputCore->VibrateController(.5f,Vector2(fAmount/m_fMaxHealth,fAmount/m_fMaxHealth) );

	ApplyDamage(fAmount/3.0f,0, damageType);
	ApplyDamage(fAmount/3.0f,1, damageType);
	ApplyDamage(fAmount/3.0f,2, damageType);
	
	/*m_fHealth -= fAmount;*/
}
// damages an individual component - 0 = pod, 1= left engine, 2= right engine
void Turbine::ApplyDamage(float fAmount, int iComponent, int damageType)
{
	// get health from components
	InitHealth();

	// vibrates the controller based on how significant the damage is.
	auto inputCore = ScreenMan->GetMainCore()->GetInputCore();
	inputCore->VibrateController(1.0,Vector2(fAmount/m_fMaxHealth,fAmount/m_fMaxHealth) );

	switch (iComponent)
	{
	case 0:	// pod
		GetPod()->ApplyDamage(fAmount);	
		// clamp the damage
		if (GetPod()->GetHealth() < 0)
			GetPod()->SetHealth(0);
		break;
	case 1:
		GetLeftEngine()->ApplyDamage(fAmount);
		// clamp the damage
		if (GetLeftEngine()->GetHealth() < 0)
			GetLeftEngine()->SetHealth(0);
		break;
	case 2:
		GetRightEngine()->ApplyDamage(fAmount);
			// clamp the damage
		if (GetRightEngine()->GetHealth() < 0)
			GetRightEngine()->SetHealth(0);
		break;
	default:
		break;
	}

	SStats->SetTotalDamageTaken(fAmount);
	m_eLastDamageType = damageType;
	
	// get new health
	InitHealth();

}
// repairs all components at the repair rate
void Turbine::Repair(float dt)
{
	if (m_fCurrentEnergy >= m_fRepairRate * 4 * dt)
	{
		GetPod()->ApplyDamage(-m_fRepairRate*dt);
		GetLeftEngine()->ApplyDamage(-m_fRepairRate*dt);
		GetRightEngine()->ApplyDamage(-m_fRepairRate*dt);
		m_fHealth = m_pPod->GetHealth() + m_pLeftEngine->GetHealth() + m_pRightEngine->GetHealth();

		// currently repair rate is equal to the decrease in energy
		ModifyCurrentEnergy(-m_fRepairRate * 4 * dt);
	}

	
}

void Turbine::EnergyRecharge(float dt)
{
	// if its less than max energy, add recharge rate to it
	if (m_fCurrentEnergy < m_fMaxEnergy)
		m_fCurrentEnergy += m_fEnergyRechargeRate * dt; 

	if (m_fCurrentEnergy > m_fMaxEnergy)
		m_fCurrentEnergy = m_fMaxEnergy;

	if (m_fCurrentEnergy < 0.0f)
		m_fCurrentEnergy = 0.0f;
}

float Turbine::GetCurrentVelocitySpeed()
{
	// Get average speed of the engines
	hkVector4 velA = m_pRightEngine->GetBody()->getLinearVelocity();
	hkVector4 velB = m_pLeftEngine->GetBody()->getLinearVelocity();
	float a = Vector3(velA.getComponent<0>(), 0, velA.getComponent<2>()).Magnitude();
	float b =  Vector3(velB.getComponent<0>(), 0, velB.getComponent<2>()).Magnitude();
	return (a + b) / 2.0f;
}

// Lets you push the turbine down along y axis
void Turbine::PushTurbineDown(float fAmount, float dt)
{
	

	Vector3 vLeftEngine = m_pLeftEngine->GetPosition(),
			vRightEngine = m_pRightEngine->GetPosition();

	vLeftEngine.z -= fAmount * dt;
	vRightEngine.z -= fAmount * dt;

	m_pLeftEngine->SetPosition(vLeftEngine);
	m_pRightEngine->SetPosition(vRightEngine);



}

void Turbine::RotateTurbine(float fAmount, float dt)
{
	//// pod
	//HM->ApplyForce(m_pPod->GetBody()->getUid(),dt,0, fAmount,0,m_pPod->GetPosition().x,m_pPod->GetPosition().y,m_pPod->GetPosition().z);
	//// left engine
	//HM->ApplyForce(m_pLeftEngine->GetBody()->getUid(),dt,0, fAmount,0,m_pLeftEngine->GetPosition().x,m_pLeftEngine->GetPosition().y,m_pLeftEngine->GetPosition().z);
	// right engine
	HM->ApplyForce(m_pRightEngine->GetBody()->getUid(),dt,0, fAmount,0,m_pRightEngine->GetPosition().x,m_pRightEngine->GetPosition().y,m_pRightEngine->GetPosition().z);

}