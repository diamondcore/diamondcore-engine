#pragma once

#include "Turbine.h"
#include "../Havok/TriggerVolumes.h"

class SmokeTrap	: public BaseGameEntity
{
	Turbine*	m_pTarget;	// Player's turbine
	
	// Trap Attributes
	Vector3		m_vBurstDir;
	float		m_fBurstRadius;
	float		m_fBurstPower;
	Vector2		m_vTimerRange;
	float		m_fTimer;
	float		m_fDuration;
	int			m_iSmokeID;
	bool		m_bSmokeCreated;

	// trigger entity's rigid body
	hkpRigidBody* m_pBody;

	// trigger rigid body's ID
	int   m_iBodyID;
	float m_fCurrentForce;


public:
	SmokeTrap();
	SmokeTrap(std::string mesh);
	~SmokeTrap();
	
	//////////////////////
	// UTILITIES
	/////////////////////
	// update and sync graphics,physics, and game logic
	virtual void Update(float dt);
	virtual void Load(TiXmlElement* node);		// Loads the attributes
	virtual void Save(TiXmlElement* obj);	// Call this original function at the begining of the function that overwrites this
	virtual std::shared_ptr<BaseGameEntity>	Clone();	// Clone these attributes into a new BaseGameEntity
	virtual void Init();						// called at the end of load
	virtual void Shutdown();					// cleans the entity
	

	//////////////////////
	// ACCESSORS
	/////////////////////


	//////////////////////
	// MUTATORS
	/////////////////////
	// Sets entity's orientation
	virtual void SetOrientation(Quaternion pOrientation);
	// sets entity's orientation by yaw/pitch/roll
	virtual void SetOrientation(float yaw, float pitch, float roll);
	// sets burst power
	void SetBurstPower(float power){ m_fBurstPower = power; }
	// Sets timer range between each geyser burst
	void SetTimerRange(float min, float max){m_vTimerRange = Vector2(min, min - max); }

};