#pragma once

#include "BaseGameEntity.h"
#include "../Havok/HavokManager.h"
#include "Turbine.h"

class DestructableObjects	: public BaseGameEntity
{
protected:
	// entity's rigid body
	hkpRigidBody* m_pBody;

	// rigid body's ID
	int m_iBodyID;

    TriggerType m_tType;

	float	m_fMass;
	float	m_fSpawnTimer;
	float	m_fRespawnDelay;
	bool	m_bIsAlive;
	Vector3	m_vStartPosition;
	int		m_iSpawnCounter;
	Vector3	m_vHalfExt;
	int		m_iExplosionID;

	/////////////////
	// SCRATCH VARS
	////////////////
	hkVector4 hkvScratch;
	hkQuaternion hkqScratch;

public:
	DestructableObjects();
    DestructableObjects(TriggerType tType);
	virtual ~DestructableObjects(void);

	//////////////////////
	// UTILITIES
	/////////////////////
	// update and sync graphics,physics, and game logic
	virtual void Update(float dt);
	virtual void Load(TiXmlElement* node);		// Loads the attributes
	virtual void Save(TiXmlElement* parent);	// Call this original function at the begining of the function that overwrites this
	virtual std::shared_ptr<BaseGameEntity>	Clone();	// Clone these attributes into a new BaseGameEntity
	virtual void Init();						// called at the end of load
	virtual void Shutdown();					// cleans the entity
	void Destroy();								// destroy the meteor

	//////////////////////
	// ACCESSORS
	/////////////////////
	// returns entity's body
	hkpRigidBody* GetBody(){return m_pBody;}

	//////////////////////
	// MUTATORS
	/////////////////////
	// Sets entity's body
	void SetBody(hkpRigidBody* pBody){m_pBody = pBody;}
	// Sets position of entity 
	virtual void SetPosition(Vector3 vPos);
	// Sets position of entity by component
	virtual void SetPosition(float x, float y, float z);
	// sets a single component of the entity's position
	virtual void SetPosition(int idx, float fValue);
	// sets position of entity with a havok vec4
	virtual void SetPosition(const hkVector4 &vPos);
	// Sets entity's orientation
	virtual void SetOrientation(Quaternion pOrientation){m_qOrientation = pOrientation;}
	// sets entity's orientation by yaw/pitch/roll
	virtual void SetOrientation(float yaw, float pitch, float roll);
	// set entity's orientation from hkQuaternion
	virtual void SetOrientation(const hkQuaternion* qOrientation);
	// rotates entity's position by yaw/pitch/roll
	virtual void RotateBody(float yaw, float pitch, float roll);
	// sets the entity's transform from havok body
	void SetTransform(const hkTransform* mTransform);
	// sets the mass of the movable object
	void SetMass(float mass){m_fMass = mass;}
	// sets the respawn delay time
	void SetRespawnDelay(float delay){m_fRespawnDelay = delay;}
};