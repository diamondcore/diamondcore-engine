#pragma once
#include "BaseGameEntity.h"
#include "../Havok/HavokManager.h"
#include "Turbine.h"

/*
	Physics entity - has havok body, etc

*/

class Smasher : public BaseGameEntity
{
protected:

	// entity's trigger volume
	hkpRigidBody* m_pTriggerBody;
	// entity's rigid body
	hkpRigidBody* m_pBody;
	// entity's base body
	hkpRigidBody* m_pBaseBody;

	float	m_fMass;

	// rigid body's ID
	int m_iBodyID;

	// health for bodies that need it
	float m_fHealth;

	// max health for when health needs displayed
	float m_fMaxHealth;
	
	// Use sphere rigid body
	bool m_bIsSphere;

    float m_fSpawnTimer;

    float 	m_SmasherLength;
	Vector3 m_vEndOffset;
    Vector3 pout;
    Vector3 m_vStartPosition;
	Vector3 m_vDirection;
	int		m_iMoveDir;

	// Smasher base graphics ID
	int		m_iSmasherBaseID;
	Vector3 m_vSmasherBaseOffset;

	// half extents
	Vector3 m_vHalfExt;

	/////////////////
	// SCRATCH VARS
	////////////////
	hkVector4 hkvScratch;
	hkQuaternion hkqScratch;

public:
	Smasher(void);
	Smasher(EntityType eType);
	Smasher(EntityType eType, char* pMeshName , 
		char* pTexName = " ", Vector3 vPodGFXScale = Vector3 (1,1,1) );
	virtual ~Smasher(void);
	
	// compare entities
	virtual bool operator== (const BaseGameEntity& entity);

	//////////////////////
	// UTILITIES
	/////////////////////
	// update and sync graphics,physics, and game logic
	virtual void Update(float dt);
	virtual void Load(TiXmlElement* node);		// Loads the attributes
	virtual void Save(TiXmlElement* parent);	// Call this original function at the begining of the function that overwrites this
	virtual std::shared_ptr<BaseGameEntity>	Clone();	// Clone these attributes into a new BaseGameEntity
	virtual void Init();						// called at the end of load
	virtual void Shutdown();					// cleans the entity
	// initializes the health of the ent
	virtual void InitHealth(){}		

	//////////////////////
	// ACCESSORS
	/////////////////////
	// returns entity's body
	hkpRigidBody* GetBody(){return m_pBody;}
	// returns rigidbody is sphere
	bool GetIsSphere(){return m_bIsSphere;}
	// returns the entity's health
	float GetHealth(){return m_fHealth;}
	// returns entity's max health
	float GetMaxHealth(){return m_fMaxHealth;}
    float getDistance(Vector3 pos);
	//////////////////////
	// MUTATORS
	/////////////////////
	// Sets entity's body
	void SetBody(hkpRigidBody* pBody){m_pBody = pBody;}
	// Sets if rigidbody is sphere
	void SetIsSphere(bool enable){m_bIsSphere = enable;}
	// Sets position of entity 
	virtual void SetPosition(Vector3 vPos);
	// Sets position of entity by component
	virtual void SetPosition(float x, float y, float z);
	// sets a single component of the entity's position
	virtual void SetPosition(int idx, float fValue);
	// sets position of entity with a havok vec4
	virtual void SetPosition(const hkVector4 &vPos);
	// Sets entity's orientation
	virtual void SetOrientation(Quaternion qOrientation);
	// sets entity's orientation by yaw/pitch/roll
	virtual void SetOrientation(float yaw, float pitch, float roll);
	// set entity's orientation from hkQuaternion
	virtual void SetOrientation(const hkQuaternion* qOrientation);
	// rotates entity's position by yaw/pitch/roll
	virtual void RotateBody(float yaw, float pitch, float roll);
	// sets the entity's transform from havok body
	void SetTransform(const hkTransform* mTransform);
	// sets the mass of the object
	void SetMass(float mass){m_fMass = mass;}
	// set's the health to the specified var
	void SetHealth(float fHealth){m_fHealth = fHealth;}
	// allows you to add to the health
	void ModifyHealth(float fHealth){m_fHealth += fHealth;}
	// set the scale
	void SetScale(Vector3 scale);
};

