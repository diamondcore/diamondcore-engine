#include "Smasher.h"
#include "../UI/ScreenManager.h"
#include "../DCEngineDemo/Havok/HavokManager.h"
#include "../WorldManager.h"
#include "Pod.h"
#include "Engine.h"
#include "TriggerEntity.h"
#define MEM ScreenMan->GetMainCore()->m_mMainMemory
#define SMASHER_DAMAGE 15
#define SMASHER_SPEED_IN  5.0f
#define SMASHER_SPEED_OUT 10.0f

Smasher::Smasher(void)
    :BaseGameEntity(ET_Smasher)
{
		m_pBody = NULL;
		m_pBaseBody = NULL;
		m_iBodyID = -1;
		m_fMass = 50000;
        m_fSpawnTimer = 0.0f;
		m_bIsSphere = false;
        m_pTriggerBody = NULL;
		m_iSmasherBaseID = -1;
		m_iMoveDir	= 0;
}

Smasher::Smasher(EntityType eType)
	:BaseGameEntity(ET_Smasher)
{
	m_pBody = NULL;
	m_pBaseBody = NULL;
	m_iBodyID = -1;
	m_fMass = 50000;
    m_fSpawnTimer = 0.0f;
	m_bIsSphere = false;
    m_pTriggerBody = NULL;
	m_iSmasherBaseID = -1;
	m_iMoveDir	= 0;
}

Smasher::Smasher(EntityType eType, char* pMeshName, char* pTexName, Vector3 vMeshScale)
	:BaseGameEntity(ET_Smasher, pMeshName,pTexName,vMeshScale)
{
	m_pBody = NULL;
	m_pBaseBody = NULL;
	m_iBodyID = -1;
	m_fMass = 50000;
    m_fSpawnTimer = 0.0f;
	m_bIsSphere = false;
    m_pTriggerBody = NULL;
	m_iSmasherBaseID = -1;
	m_iMoveDir	= 0;
}


Smasher::~Smasher(void)
{
}

// compare entities
bool Smasher::operator== (const BaseGameEntity& entity)
{
	// Check if the root attributes match up
	if( ((BaseGameEntity)(*this)) == entity )
	{
		// Compare attributes specific to the trigger entity
		Smasher* tEntity = (Smasher*)(&entity);
		if( m_bIsSphere == tEntity->m_bIsSphere &&
			m_fMass == tEntity->m_fMass )
			return true;
	}
	return false;
}

//////////////////////
// UTILITIES
/////////////////////
// update and sync graphics,physics, and game logic
float Smasher::getDistance(Vector3 pos)
{
	Vector3 w = pos - m_vPosition;

	float b;

    b = (pos - m_vPosition).Magnitude();
	return b;

}
void Smasher::Update(float dt)
{

	if(m_pBody)
	{
		
		//Test to see if you can put a smasher in the air without it falling
       HM->SetGravityFactor(m_iBodyID);
	   HM->SetGravityFactor(m_pTriggerBody->getUid());
       if (m_iMoveDir == 1)//m_fSpawnTimer <= 20)
           {
           
           m_fSpawnTimer++;
		   
		   Vector3 tempPos = m_vEndOffset - m_vPosition;
		   if(tempPos.MagnitudeSquared() <= 0.1f)
			   m_iMoveDir = -1;	// Signal to move back

		   tempPos.Normalize();
            // translate by position
            m_vPosition += tempPos*(dt * SMASHER_SPEED_OUT);
           }
	   else if (m_iMoveDir == -1)//m_fSpawnTimer <= 50)
           {
           m_fSpawnTimer++;
           
           Vector3 tempPos =  m_vStartPosition - m_vPosition;
		   if(tempPos.MagnitudeSquared() <= 0.1f)
			   m_iMoveDir = 1;	// Signal to move out

		   tempPos.Normalize();
            // translate by position
            m_vPosition += tempPos*(dt * SMASHER_SPEED_IN);
           }
       else
           m_fSpawnTimer = 0.0f;

	   
		// Update the trigger volume position
		Vector3 pos = m_vPosition + m_vDirection * (m_vHalfExt.z - 0.5f);
		HM->SetBodyPosition(m_pTriggerBody, pos.x, pos.y, pos.z);

       HM->SetBodyPosition(m_pBody,m_vPosition.x,m_vPosition.y,m_vPosition.z);
        // get new position from physics body
		SetPosition(m_pBody->getPosition() );

		// get new orientation from physics body
		SetOrientation(&m_pBody->getRotation() );

		// get transform matrix from physics body
		SetTransform(&m_pBody->getTransform() );
		
	}
	BaseGameEntity::Update(dt);
}

void Smasher::Load(TiXmlElement* node)
{
	// Gather and Set object information
	BaseGameEntity::Load(node);

	// Load physics information
	node->QueryFloatAttribute("Mass", &m_fMass);
	node->QueryBoolAttribute("IsSphere", &m_bIsSphere);
}
void Smasher::Save(TiXmlElement* obj)
{
	// Create the base object info
	BaseGameEntity::Save(obj);

	// Save physics information
	obj->SetDoubleAttribute("Mass", m_fMass);
	obj->SetAttribute("IsSphere", m_bIsSphere);
}


std::shared_ptr<BaseGameEntity>	Smasher::Clone()	// Clone these attributes into a new BaseGameEntity
{
	// Check if we've reached our limit on copies
	if(m_iNumCopies >= m_iMaxNumCopies)
		return 0;

	// Create clone
	std::shared_ptr<Smasher> clone = std::shared_ptr<Smasher>(new Smasher(m_eType));
	
	// Set all the attributes for the clone that are not unique to a specific instance.
	// This will be used to create instances of this object in the level designer.
	// Brief example:
	clone->SetMeshName(&m_pMeshName[0]);
	if(!m_pTextureName.empty())
		clone->SetTextureName(&m_pTextureName[0]);
	clone->SetRenderable(m_bIsRenderable);
	clone->SetColor(m_vColor);
	clone->SetScale(m_vScale);
	clone->SetOffset(m_vOffset);
	clone->SetIsSphere(m_bIsSphere);

	// Set physics attributes
	clone->m_fMass = m_fMass;
	
	// Setup the copy counter to point to the original counter
	clone->m_pNumCopies = &m_iNumCopies;
	m_iNumCopies++;

	// Initialize the object
	clone->Init();
	return clone;
}

void Smasher::Init()
{
	BaseGameEntity::Init();

	// Create physics specific attributes
	ScreenMan->GetGraphics()->GetMeshBV(&m_pMeshName[0], m_vHalfExt);
	m_vHalfExt = m_vHalfExt.ComponentProduct(m_vScale);	// Scale the halfExtent by the specified scale
	//m_SmasherLength = m_vHalfExt.z;
   
	// Create the smasher base
	auto GFX = ScreenMan->GetGraphics();
	m_iSmasherBaseID = MEM->CreateSharedResource();
	GFX->CreateMesh(m_iSmasherBaseID, "Smasher_Base.X", false);
	Vector3 baseHalfExt;
	GFX->GetMeshBV("Smasher_Base.X", baseHalfExt);
	m_vSmasherBaseOffset = baseHalfExt = baseHalfExt.ComponentProduct(m_vScale);
	m_vSmasherBaseOffset.x = 0.0f;
	m_vSmasherBaseOffset.y -= m_vHalfExt.y * 1.12f;
	m_vSmasherBaseOffset.z = m_vHalfExt.z * -0.025f;
	GFX->SetMeshScale(m_iSmasherBaseID, "Smasher_Base.X", m_vScale);
	m_SmasherLength = baseHalfExt.z;

	// Check if we are in a mode that has Havok initialized
	if(!ScreenMan->GetEntityManager()->DisplayHiddenObjects())
	{
		// init body here w/ havok manager
		m_pBody = HM->AddMovingBox(-1, m_vPosition.x, m_vPosition.y, m_vPosition.z, m_vHalfExt.x, m_vHalfExt.y, m_vHalfExt.z - 1.0f, m_fMass, 2); // Disable collision with the terrain

		// set body id
		m_iBodyID = m_pBody->getUid();

		// Set body attributes
		HM->SetBodyPosition(m_pBody, m_vPosition.x, m_vPosition.y, m_vPosition.z);
         pout = m_vPosition;
        m_vStartPosition = m_vPosition;
		Vector3 rot = m_qOrientation.GetYawPitchRoll();
		RotateBody(rot.x, rot.y, rot.z);
		
		// init base body
		m_pBaseBody = HM->AddMovingBox(-1, m_vPosition.x, m_vPosition.y, m_vPosition.z, baseHalfExt.x, baseHalfExt.y, baseHalfExt.z, m_fMass, 4);
		HM->RotateBody(m_pBaseBody, rot.y, rot.x, rot.z);
		HM->SetGravityFactor(m_pBaseBody->getUid());

		// Calculate smasher end offset
		D3DXMATRIX O;
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&m_vDirection, &Vector3(0.0f, 0.0f, 1.0f), &O);
		m_vDirection.Normalize();
		m_vEndOffset = m_vPosition + m_vDirection * m_SmasherLength;// * 2.0f;

		// Create the trigger entity
		Vector3 pos = m_vPosition + m_vDirection * (m_vHalfExt.z - 0.5f);
		m_pTriggerBody = HM->CreateMovingTriggerVolume(-1, _Crush, pos.x, pos.y, pos.z, m_vHalfExt.x, m_vHalfExt.y, 0.5f, m_fMass);
		HM->RotateBody(m_pTriggerBody, rot.y, rot.x, rot.z);// Set collision group 
		HM->AddToCollisionGroup(m_pTriggerBody, 2);	// Disable collision with the terrain

		// Set move direction to move out
		m_iMoveDir	= 1;
	}

	// Calculate the smasher base offset with respect to direction
	D3DXMATRIX O;
	Vector3 dir;
	// Get y axis offset
	D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
	D3DXVec3TransformNormal(&dir, &Vector3(0.0f, 1.0f, 0.0f), &O);
	dir.Normalize();
	Vector3 offset = dir * m_vSmasherBaseOffset.y;
	// Get z axis offset
	D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
	D3DXVec3TransformNormal(&dir, &Vector3(0.0f, 0.0f, 1.0f), &O);
	dir.Normalize();
	offset += dir * m_vSmasherBaseOffset.z;
	MEM->GetSharedResource(m_iSmasherBaseID)->SetPosition(m_vPosition + offset);
	MEM->GetSharedResource(m_iSmasherBaseID)->SetOrientation(m_qOrientation);
}

void Smasher::Shutdown()
{
	BaseGameEntity::Shutdown();

	// Remove body if it exists
	if(m_pBody)
	{
		HM->RemoveEntity(m_iBodyID);
		m_iBodyID = -1;
		m_pBody = 0;
	}
	if(m_iSmasherBaseID > -1)
	{
		ScreenMan->GetGraphics()->RemoveMesh(m_iSmasherBaseID, "Smasher_Base.X");
		MEM->FreeSharedResource(m_iSmasherBaseID);
		m_iSmasherBaseID = -1;
	}
	if(m_pTriggerBody)
	{
		HM->RemoveTriggerVolume(m_pTriggerBody->getUid());
		m_pTriggerBody = 0;
	}
	if(m_pBaseBody)
	{
		HM->RemoveEntity(m_pBaseBody->getUid());
		m_pBaseBody = 0;
	}
}

//////////////////////
// MUTATORS
/////////////////////

void Smasher::SetPosition(Vector3 vPos)
{
	m_vPosition = vPos;
	if(m_pBody)
	{
		HM->SetBodyPosition(m_pBody,vPos.x, vPos.y, vPos.z);	
	
		// Calculate smasher end offset
		D3DXMATRIX O;
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&m_vDirection, &Vector3(0.0f, 0.0f, 1.0f), &O);
		m_vDirection.Normalize();
		m_vEndOffset = m_vPosition + m_vDirection * m_SmasherLength;// * 2.0f;
	}
	if(m_iSmasherBaseID > -1)
	{
		// Calculate the smasher base offset with respect to direction
		D3DXMATRIX O;
		Vector3 dir;
		// Get y axis offset
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&dir, &Vector3(0.0f, 1.0f, 0.0f), &O);
		dir.Normalize();
		Vector3 offset = dir * m_vSmasherBaseOffset.y;
		// Get z axis offset
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&dir, &Vector3(0.0f, 0.0f, 1.0f), &O);
		dir.Normalize();
		offset += dir * m_vSmasherBaseOffset.z;
		MEM->GetSharedResource(m_iSmasherBaseID)->SetPosition(m_vPosition + offset);
		MEM->GetSharedResource(m_iSmasherBaseID)->SetOrientation(m_qOrientation);
	}
}
// Sets position of entity by component
void Smasher::SetPosition(float x, float y, float z)
{
	m_vPosition.x = x;
	m_vPosition.y = y;
	m_vPosition.z = z;
	if(m_pBody)
	{
		HM->SetBodyPosition(m_pBody,x, y, z);
		
		// Calculate smasher end offset
		D3DXMATRIX O;
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&m_vDirection, &Vector3(0.0f, 0.0f, 1.0f), &O);
		m_vDirection.Normalize();
		m_vEndOffset = m_vPosition + m_vDirection * m_SmasherLength;// * 2.0f;
	}
	if(m_iSmasherBaseID > -1)
	{
		// Calculate the smasher base offset with respect to direction
		D3DXMATRIX O;
		Vector3 dir;
		// Get y axis offset
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&dir, &Vector3(0.0f, 1.0f, 0.0f), &O);
		dir.Normalize();
		Vector3 offset = dir * m_vSmasherBaseOffset.y;
		// Get z axis offset
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&dir, &Vector3(0.0f, 0.0f, 1.0f), &O);
		dir.Normalize();
		offset += dir * m_vSmasherBaseOffset.z;
		MEM->GetSharedResource(m_iSmasherBaseID)->SetPosition(m_vPosition + offset);
		MEM->GetSharedResource(m_iSmasherBaseID)->SetOrientation(m_qOrientation);
	}
}

// sets a single component of the entity's position
// 0 = X, 1 = Y, 2 = Z
void Smasher::SetPosition(int idx, float fValue)
{
	switch(idx)
	{
	case 0:
		m_vPosition.x = fValue;
	case 1:
		m_vPosition.y = fValue;
	case 2:
		m_vPosition.z = fValue;
	default:
		assert ("Smasher::SetPosition failed - idx must be a value between 0 and 2"); 
		return;
	};
	if(m_pBody)
	{
		HM->SetBodyPosition(m_pBody,m_vPosition.x, m_vPosition.y, m_vPosition.z);
		
		// Calculate smasher end offset
		D3DXMATRIX O;
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&m_vDirection, &Vector3(0.0f, 0.0f, 1.0f), &O);
		m_vDirection.Normalize();
		m_vEndOffset = m_vPosition + m_vDirection * m_SmasherLength;// * 2.0f;
	}
	if(m_iSmasherBaseID > -1)
	{
		// Calculate the smasher base offset with respect to direction
		D3DXMATRIX O;
		Vector3 dir;
		// Get y axis offset
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&dir, &Vector3(0.0f, 1.0f, 0.0f), &O);
		dir.Normalize();
		Vector3 offset = dir * m_vSmasherBaseOffset.y;
		// Get z axis offset
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&dir, &Vector3(0.0f, 0.0f, 1.0f), &O);
		dir.Normalize();
		offset += dir * m_vSmasherBaseOffset.z;
		MEM->GetSharedResource(m_iSmasherBaseID)->SetPosition(m_vPosition + offset);
		MEM->GetSharedResource(m_iSmasherBaseID)->SetOrientation(m_qOrientation);
	}
}

// ONLY SETS LOCAL POSITION, DOES NOT UPDATE BODY
void Smasher::SetPosition(const hkVector4 &vPos)
{
	m_vPosition.x = vPos(0);
	m_vPosition.y = vPos(1);
	m_vPosition.z = vPos(2);

}

// Sets entity's orientation
void Smasher::SetOrientation(Quaternion qOrientation)
{
	m_qOrientation = qOrientation;
	if (m_pBody)
	{
		Vector3 vYawPitchRoll = qOrientation.GetYawPitchRoll();
		HM->SetBodyOrientation(m_pBody, vYawPitchRoll.y,vYawPitchRoll.x,vYawPitchRoll.z);
	
		// Calculate smasher end offset
		D3DXMATRIX O;
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&m_vDirection, &Vector3(0.0f, 0.0f, 1.0f), &O);
		m_vDirection.Normalize();
		m_vEndOffset = m_vPosition + m_vDirection * m_SmasherLength;// * 2.0f;
	}
	if(m_iSmasherBaseID > -1)
	{
		// Calculate the smasher base offset with respect to direction
		D3DXMATRIX O;
		Vector3 dir;
		// Get y axis offset
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&dir, &Vector3(0.0f, 1.0f, 0.0f), &O);
		dir.Normalize();
		Vector3 offset = dir * m_vSmasherBaseOffset.y;
		// Get z axis offset
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&dir, &Vector3(0.0f, 0.0f, 1.0f), &O);
		dir.Normalize();
		offset += dir * m_vSmasherBaseOffset.z;
		MEM->GetSharedResource(m_iSmasherBaseID)->SetPosition(m_vPosition + offset);
		MEM->GetSharedResource(m_iSmasherBaseID)->SetOrientation(m_qOrientation);
	}
}

// sets entity's orientation by yaw/pitch/roll
void Smasher::SetOrientation(float yaw, float pitch, float roll)
{
	if(m_pBody)
		HM->SetBodyOrientation(m_pBody, pitch,yaw,roll);
}
// set entity's orientation from hkQuaternion
void Smasher::SetOrientation(const hkQuaternion* qOrientation)
{
	m_qOrientation.x = qOrientation->getComponent<0>();
	m_qOrientation.y = qOrientation->getComponent<1>();
	m_qOrientation.z = qOrientation->getComponent<2>();
	m_qOrientation.w = qOrientation->getComponent<3>();

}

// rotates entity's position by yaw/pitch/roll
void Smasher::RotateBody(float yaw, float pitch, float roll)
{
	if(m_pBody)
		HM->RotateBody(m_pBody, pitch,yaw,roll);
}

// sets the entity's transform from havok body
void Smasher::SetTransform(const hkTransform* mTransform)
{
	
	m_mTransform(0,0) = mTransform->getElement<0,0>();
	m_mTransform(0,1) = mTransform->getElement<0,1>();
	m_mTransform(0,2) = mTransform->getElement<0,2>();
	m_mTransform(0,3) = mTransform->getElement<0,3>();
	m_mTransform(1,0) = mTransform->getElement<1,0>();
	m_mTransform(1,1) = mTransform->getElement<1,1>();
	m_mTransform(1,2) = mTransform->getElement<1,2>();
	m_mTransform(1,3) = mTransform->getElement<1,3>();
	m_mTransform(2,0) = mTransform->getElement<2,0>();
	m_mTransform(2,1) = mTransform->getElement<2,1>();
	m_mTransform(2,2) = mTransform->getElement<2,2>();
	m_mTransform(2,3) = mTransform->getElement<2,3>();
	m_mTransform(3,0) = mTransform->getElement<3,0>();
	m_mTransform(3,1) = mTransform->getElement<3,1>();
	m_mTransform(3,2) = mTransform->getElement<3,2>();
	m_mTransform(3,3) = mTransform->getElement<3,3>();

}

// set the scale
void Smasher::SetScale(Vector3 scale)
{
	m_vScale = scale;
	if(m_iSmasherBaseID > -1)
	{		
		// Reset physics specific attributes
		ScreenMan->GetGraphics()->GetMeshBV(&m_pMeshName[0], m_vHalfExt);
		m_vHalfExt = m_vHalfExt.ComponentProduct(m_vScale);	// Scale the halfExtent by the specified scale
		//m_SmasherLength = m_vHalfExt.z;

		// Reset graphics scale
		auto GFX = ScreenMan->GetGraphics();
		Vector3 baseHalfExt;
		GFX->GetMeshBV("Smasher_Base.X", baseHalfExt);
		m_vSmasherBaseOffset = baseHalfExt = baseHalfExt.ComponentProduct(m_vScale);
		m_vSmasherBaseOffset.x = 0.0f;
		m_vSmasherBaseOffset.y -= m_vHalfExt.y * 1.12f;
		m_vSmasherBaseOffset.z = m_vHalfExt.z * -0.025f;
		GFX->SetMeshScale(m_iSmasherBaseID, "Smasher_Base.X", m_vScale);
		m_SmasherLength = baseHalfExt.z;

		// Calculate the smasher base offset with respect to direction
		D3DXMATRIX O;
		Vector3 dir;
		// Get y axis offset
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&dir, &Vector3(0.0f, 1.0f, 0.0f), &O);
		dir.Normalize();
		Vector3 offset = dir * m_vSmasherBaseOffset.y;
		// Get z axis offset
		D3DXMatrixRotationQuaternion(&O, &m_qOrientation);
		D3DXVec3TransformNormal(&dir, &Vector3(0.0f, 0.0f, 1.0f), &O);
		dir.Normalize();
		offset += dir * m_vSmasherBaseOffset.z;
		MEM->GetSharedResource(m_iSmasherBaseID)->SetPosition(m_vPosition + offset);
		MEM->GetSharedResource(m_iSmasherBaseID)->SetOrientation(m_qOrientation);
	}

}