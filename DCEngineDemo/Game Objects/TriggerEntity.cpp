#include "TriggerEntity.h"
#include "../UI/ScreenManager.h"
#include "../DCEngineDemo/Havok/HavokManager.h"

#define MEM ScreenMan->GetMainCore()->m_mMainMemory


TriggerEntity::TriggerEntity(void)
	:BaseGameEntity()
{
		m_pBody = NULL;
		m_iBodyID = -1;
        m_tType = _Damage;
        m_bIsRenderable = false;
}

TriggerEntity::TriggerEntity(TriggerType tType)
	:BaseGameEntity(ET_TRIGGERVOLUME)
{
	m_pBody = NULL;
	m_iBodyID = -1;
    m_tType = tType;
    m_bIsRenderable = false;

}

TriggerEntity::TriggerEntity(EntityType eType,TriggerType tType)
	:BaseGameEntity(eType)
{
	m_pBody = NULL;
	m_iBodyID = -1;
    m_tType = tType;
    m_bIsRenderable = false;

}

TriggerEntity::TriggerEntity(EntityType eType, char* pMeshName, char* pTexName, Vector3 vMeshScale)
	:BaseGameEntity(eType, pMeshName,pTexName,vMeshScale)
{
	m_pBody = NULL;
	m_iBodyID = -1;
    m_bIsRenderable = false;
}


TriggerEntity::~TriggerEntity(void)
{
}

// compare entities
bool TriggerEntity::operator== (const BaseGameEntity& entity)
{
	// Check if the root attributes match up
	if( ((BaseGameEntity)(*this)) == entity )
	{
		// Compare attributes specific to the trigger entity
		TriggerEntity* tEntity = (TriggerEntity*)(&entity);
		if( m_tType == tEntity->m_tType )
			return true;
	}
	return false;
}

//////////////////////
// UTILITIES
/////////////////////
// update and sync graphics,physics, and game logic
void TriggerEntity::Update(float dt)
{
	if(m_pBody)
	{
		// get new position from physics body
		SetPosition(m_pBody->getPosition() );

		// get new orientation from physics body
		SetOrientation(&m_pBody->getRotation() );

		// get transform matrix from physics body
		SetTransform(&m_pBody->getTransform() );
	}
	BaseGameEntity::Update(dt);
}

void TriggerEntity::Load(TiXmlElement* node)
{
	// Gather and Set object information
	BaseGameEntity::Load(node);
    // Load physics information
    int temp = -1;
    node->QueryIntAttribute("t_Type", &temp);
	m_tType = (TriggerType)temp;
}
void TriggerEntity::Save(TiXmlElement* obj)
{
	// Create the base object info
	BaseGameEntity::Save(obj);
    obj->SetAttribute("t_Type", m_tType);

}


std::shared_ptr<BaseGameEntity>	TriggerEntity::Clone()	// Clone these attributes into a new BaseGameEntity
{
	// Check if we've reached our limit on copies
	if(m_iNumCopies >= m_iMaxNumCopies)
		return 0;

	// Create clone
    std::shared_ptr<TriggerEntity> clone = std::shared_ptr<TriggerEntity>(new TriggerEntity(m_eType,m_tType));
	
	// Set all the attributes for the clone that are not unique to a specific instance.
	// This will be used to create instances of this object in the level designer.
	// Brief example:
	clone->SetMeshName(&m_pMeshName[0]);
	if(!m_pTextureName.empty())
		clone->SetTextureName(&m_pTextureName[0]);
	clone->SetRenderable(m_bIsRenderable);
	clone->SetColor(m_vColor);
	clone->SetScale(m_vScale);
	clone->SetOffset(m_vOffset);
	
	// Setup the copy counter to point to the original counter
	clone->m_pNumCopies = &m_iNumCopies;
	m_iNumCopies++;

	// Initialize the object
	clone->Init();
	return clone;
}

void TriggerEntity::Init()
{
	BaseGameEntity::Init();

	// Check if we are in a mode that has Havok initialized
	if(!ScreenMan->GetEntityManager()->DisplayHiddenObjects())
	{
		// Create physics specific attributes
		ScreenMan->GetGraphics()->CreateMesh(INT_MAX,"Box.x",true);

		Vector3 halfExtent;
		ScreenMan->GetGraphics()->GetMeshBV("Box.x", halfExtent);
		halfExtent = halfExtent.ComponentProduct(m_vScale);	// Scale the halfExtent by the specified scale

		ScreenMan->GetGraphics()->RemoveMesh(INT_MAX,"Box.x");

		// init body here w/ havok manager
        m_pBody = HM->CreateTriggerVolume(-1,m_tType, m_vPosition.x, m_vPosition.y, m_vPosition.z, halfExtent.x, halfExtent.y, halfExtent.z);

		// set body id
		m_iBodyID = m_pBody->getUid();

		// Set body attributes
		HM->SetBodyPosition(m_pBody, m_vPosition.x, m_vPosition.y, m_vPosition.z);
		Vector3 rot = m_qOrientation.GetYawPitchRoll();
		RotateBody(rot.x, rot.y, rot.z);
	}
}

void TriggerEntity::Shutdown()
{
	BaseGameEntity::Shutdown();

	// Remove body if it exists
	if(m_pBody)
	{
		HM->RemoveTriggerVolume(m_iBodyID);
		m_iBodyID = -1;
		m_pBody = 0;
	}
}

//////////////////////
// MUTATORS
/////////////////////

void TriggerEntity::SetPosition(Vector3 vPos)
{
	m_vPosition = vPos;
	if(m_pBody)
		HM->SetBodyPosition(m_pBody,vPos.x, vPos.y, vPos.z);	
}
// Sets position of entity by component
void TriggerEntity::SetPosition(float x, float y, float z)
{
	m_vPosition.x = x;
	m_vPosition.y = y;
	m_vPosition.z = z;
	if(m_pBody)
		HM->SetBodyPosition(m_pBody,x, y, z);
}

// sets a single component of the entity's position
// 0 = X, 1 = Y, 2 = Z
void TriggerEntity::SetPosition(int idx, float fValue)
{
	switch(idx)
	{
	case 0:
		m_vPosition.x = fValue;
	case 1:
		m_vPosition.y = fValue;
	case 2:
		m_vPosition.z = fValue;
	default:
		assert ("TriggerEntity::SetPosition failed - idx must be a value between 0 and 2"); 
		return;
	};
	if(m_pBody)
		HM->SetBodyPosition(m_pBody,m_vPosition.x, m_vPosition.y, m_vPosition.z);
}

// ONLY SETS LOCAL POSITION, DOES NOT UPDATE BODY
void TriggerEntity::SetPosition(const hkVector4 &vPos)
{
	m_vPosition.x = vPos(0);
	m_vPosition.y = vPos(1);
	m_vPosition.z = vPos(2);

}

// sets entity's orientation by yaw/pitch/roll
void TriggerEntity::SetOrientation(float yaw, float pitch, float roll)
{
	if(m_pBody)
		HM->SetBodyOrientation(m_pBody, pitch,yaw,roll);
}
// set entity's orientation from hkQuaternion
void TriggerEntity::SetOrientation(const hkQuaternion* qOrientation)
{
	m_qOrientation.x = qOrientation->getComponent<0>();
	m_qOrientation.y = qOrientation->getComponent<1>();
	m_qOrientation.z = qOrientation->getComponent<2>();
	m_qOrientation.w = qOrientation->getComponent<3>();

}

// rotates entity's position by yaw/pitch/roll
void TriggerEntity::RotateBody(float yaw, float pitch, float roll)
{
	if(m_pBody)
		HM->RotateBody(m_pBody, pitch,yaw,roll);
}

// sets the entity's transform from havok body
void TriggerEntity::SetTransform(const hkTransform* mTransform)
{
	
	m_mTransform(0,0) = mTransform->getElement<0,0>();
	m_mTransform(0,1) = mTransform->getElement<0,1>();
	m_mTransform(0,2) = mTransform->getElement<0,2>();
	m_mTransform(0,3) = mTransform->getElement<0,3>();
	m_mTransform(1,0) = mTransform->getElement<1,0>();
	m_mTransform(1,1) = mTransform->getElement<1,1>();
	m_mTransform(1,2) = mTransform->getElement<1,2>();
	m_mTransform(1,3) = mTransform->getElement<1,3>();
	m_mTransform(2,0) = mTransform->getElement<2,0>();
	m_mTransform(2,1) = mTransform->getElement<2,1>();
	m_mTransform(2,2) = mTransform->getElement<2,2>();
	m_mTransform(2,3) = mTransform->getElement<2,3>();
	m_mTransform(3,0) = mTransform->getElement<3,0>();
	m_mTransform(3,1) = mTransform->getElement<3,1>();
	m_mTransform(3,2) = mTransform->getElement<3,2>();
	m_mTransform(3,3) = mTransform->getElement<3,3>();

}