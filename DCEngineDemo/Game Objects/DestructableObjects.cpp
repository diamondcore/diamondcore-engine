#include "DestructableObjects.h"
#include "../UI/ScreenManager.h"
#include "../WorldManager.h"
#include "Terrain.h"
#include "../DCEngineDemo/Havok/HavokManager.h"

#define MEM ScreenMan->GetMainCore()->m_mMainMemory


DestructableObjects::DestructableObjects(void)
	:BaseGameEntity(ET_DESTRUCTABLE)
{
		m_pBody = NULL;
		m_iBodyID = -1;
		m_iExplosionID = -1;
        m_tType = _Explosion;
        m_bIsRenderable = true;
		m_bIsAlive	= false;
		m_iSpawnCounter	= 0;
		m_fMass = 10.0f;
		m_fSpawnTimer	= 0.0f;
		m_fRespawnDelay	= 10.0f;
		m_vHalfExt = Vector3(1.0f, 1.0f, 1.0f);
}

DestructableObjects::DestructableObjects(TriggerType tType)
	:BaseGameEntity(ET_DESTRUCTABLE)
{
	m_pBody = NULL;
	m_iBodyID = -1;
	m_iExplosionID = -1;
    m_tType = tType;
    m_bIsRenderable = true;
	m_bIsAlive		= false;
	m_iSpawnCounter	= 0;
	m_fMass = 10.0f;
	m_fSpawnTimer	= 0.0f;
	m_fRespawnDelay	= 10.0f;
	m_vHalfExt = Vector3(1.0f, 1.0f, 1.0f);
}

DestructableObjects::~DestructableObjects(void)
{
}

//////////////////////
// UTILITIES
/////////////////////
// update and sync graphics,physics, and game logic
void DestructableObjects::Update(float dt)
{
	if(m_pBody && m_bIsAlive)
	{
		// get new position from physics body
		SetPosition(m_pBody->getPosition() );

		// get new orientation from physics body
		SetOrientation(&m_pBody->getRotation() );

		// get transform matrix from physics body
		SetTransform(&m_pBody->getTransform() );
	}
	BaseGameEntity::Update(dt);

	if(!m_pBody && m_bIsAlive)
		return;

	// Check for collision with the ground
	if(m_bIsAlive)
	{
		float gHeight = ScreenMan->GetEntityManager()->GetTerrain()->GetHeightAt(m_vPosition);
		
		if( (m_vPosition.y - gHeight) < m_vHalfExt.Magnitude() * 0.25f || gHeight == FLT_MIN )
		{
			if(m_iExplosionID <= -1)
			{
				// Create Explosion effect
				auto GFX = ScreenMan->GetGraphics();
				m_iExplosionID = MEM->CreateSharedResource();
				MEM->GetSharedResource(m_iExplosionID)->SetOrientation(Quaternion());
				Vector3 pos = m_vPosition;
				pos.y -= m_vHalfExt.y;// * 0.9f;
				MEM->GetSharedResource(m_iExplosionID)->SetPosition(pos);
				GFX->CreateParticle(m_iExplosionID, "SmokeCloud", &m_vHalfExt);
				GFX->CreateParticle(m_iExplosionID, "DefaultFire", 0);
				GFX->CreateParticle(m_iExplosionID, "DefaultSpark", 0);
			}
		
			// Destroy and begin the timer until the next respawn
			MEM->GetSharedResource(m_iExplosionID)->SetPosition(m_vPosition);
			if(gHeight == FLT_MIN)
				m_fSpawnTimer = 0.0f;
			else
				m_fSpawnTimer = m_fRespawnDelay;
			m_bIsAlive = false;
			Destroy();
		}
	}
	else if(m_fSpawnTimer <= 0.0f)
	{
		// Remove Explosion
		ScreenMan->GetGraphics()->RemoveParticle(m_iExplosionID);
		ScreenMan->GetGraphics()->RemoveParticle(m_ID);
		MEM->FreeSharedResource(m_iExplosionID);
		m_iExplosionID = -1;

		// Respawn the object
		m_fSpawnTimer = 0.0f;
		//Destroy();
		Init();
	}
	else
		m_fSpawnTimer -= dt;	// Count down until next respawn
}

void DestructableObjects::Load(TiXmlElement* node)
{
	// Gather and Set object information
	BaseGameEntity::Load(node);
    // Load physics informationss
    int temp = -1;
    node->QueryIntAttribute("t_Type", &temp);
	m_tType = (TriggerType)temp;
	node->QueryFloatAttribute("Mass", &m_fMass);
	node->QueryFloatAttribute("RespawnDelay", &m_fRespawnDelay);
}
void DestructableObjects::Save(TiXmlElement* obj)
{
	// Create the base object info
	BaseGameEntity::Save(obj);
    obj->SetAttribute("t_Type", m_tType);
	obj->SetDoubleAttribute("Mass", m_fMass);
	obj->SetDoubleAttribute("RespawnDelay", m_fRespawnDelay);

}


std::shared_ptr<BaseGameEntity>	DestructableObjects::Clone()	// Clone these attributes into a new BaseGameEntity
{
	// Check if we've reached our limit on copies
	if(m_iNumCopies >= m_iMaxNumCopies)
		return 0;

	// Create clone
    std::shared_ptr<DestructableObjects> clone = std::shared_ptr<DestructableObjects>(new DestructableObjects(m_tType));
	
	// Set all the attributes for the clone that are not unique to a specific instance.
	// This will be used to create instances of this object in the level designer.
	// Brief example:
	clone->SetMeshName(&m_pMeshName[0]);
	if(!m_pTextureName.empty())
		clone->SetTextureName(&m_pTextureName[0]);
	clone->SetRenderable(m_bIsRenderable);
	clone->SetColor(m_vColor);
	clone->SetScale(m_vScale);
	clone->SetOffset(m_vOffset);

	// Set destructable object attributes
	clone->SetMass(m_fMass);
	clone->SetRespawnDelay(m_fRespawnDelay);
	
	// Setup the copy counter to point to the original counter
	clone->m_pNumCopies = &m_iNumCopies;
	m_iNumCopies++;

	// Initialize the object
	clone->Init();
	return clone;
}

void DestructableObjects::Init()
{
	// Check if we are in a mode that has Havok initialized
	if(!ScreenMan->GetEntityManager()->DisplayHiddenObjects())
	{
		// Set the starting position
		if(m_iSpawnCounter == 0)	
		{
			m_vStartPosition = m_vPosition;
			BaseGameEntity::Init();

			ScreenMan->GetGraphics()->GetMeshBV(&m_pMeshName[0], m_vHalfExt);
			m_vHalfExt = m_vHalfExt.ComponentProduct(m_vScale);	// Scale the halfExtent by the specified scale
		}
		else
			m_vPosition = m_vStartPosition;

		// init body here w/ havok manager
		m_pBody = HM->CreateMovingTriggerVolume(-1, m_tType, m_vPosition.x, m_vPosition.y, m_vPosition.z, m_vHalfExt.x, m_vHalfExt.y, m_vHalfExt.z, m_fMass);
        
		// set body id
		m_iBodyID = m_pBody->getUid();

		// Set body attributes
		HM->SetBodyPosition(m_pBody, m_vPosition.x, m_vPosition.y, m_vPosition.z);
		Vector3 rot = m_qOrientation.GetYawPitchRoll();
		RotateBody(rot.x, rot.y, rot.z);
		m_iSpawnCounter++;	// Increment the spawn count
	}
	else
		BaseGameEntity::Init();
	m_bIsAlive = true;
}

void DestructableObjects::Shutdown()
{
	if(m_iExplosionID > -1)
	{
		ScreenMan->GetGraphics()->RemoveParticle(m_iExplosionID);
		ScreenMan->GetGraphics()->RemoveParticle(m_ID);
		MEM->FreeSharedResource(m_iExplosionID);
		m_iExplosionID = -1;
	}
	BaseGameEntity::Shutdown();

	// Remove body if it exists
	if(m_pBody)
	{
		HM->RemoveTriggerVolume(m_iBodyID);
		m_iBodyID = -1;
		m_pBody = 0;
	}
}
void DestructableObjects::Destroy()
{
	// Remove body if it exists
	if(m_pBody)
	{
		HM->RemoveTriggerVolume(m_iBodyID);
		m_iBodyID = -1;
		m_pBody = 0;
	}
	m_bIsAlive = false;
}

//////////////////////
// MUTATORS
/////////////////////

void DestructableObjects::SetPosition(Vector3 vPos)
{
	m_vPosition = vPos;
	if(m_pBody)
		HM->SetBodyPosition(m_pBody,vPos.x, vPos.y, vPos.z);	
}
// Sets position of entity by component
void DestructableObjects::SetPosition(float x, float y, float z)
{
	m_vPosition.x = x;
	m_vPosition.y = y;
	m_vPosition.z = z;
	if(m_pBody)
		HM->SetBodyPosition(m_pBody,x, y, z);
}

// sets a single component of the entity's position
// 0 = X, 1 = Y, 2 = Z
void DestructableObjects::SetPosition(int idx, float fValue)
{
	switch(idx)
	{
	case 0:
		m_vPosition.x = fValue;
	case 1:
		m_vPosition.y = fValue;
	case 2:
		m_vPosition.z = fValue;
	default:
		assert ("TriggerEntity::SetPosition failed - idx must be a value between 0 and 2"); 
		return;
	};
	if(m_pBody)
		HM->SetBodyPosition(m_pBody,m_vPosition.x, m_vPosition.y, m_vPosition.z);
}

// ONLY SETS LOCAL POSITION, DOES NOT UPDATE BODY
void DestructableObjects::SetPosition(const hkVector4 &vPos)
{
	m_vPosition.x = vPos(0);
	m_vPosition.y = vPos(1);
	m_vPosition.z = vPos(2);

}

// sets entity's orientation by yaw/pitch/roll
void DestructableObjects::SetOrientation(float yaw, float pitch, float roll)
{
	if(m_pBody)
		HM->SetBodyOrientation(m_pBody, pitch,yaw,roll);
}
// set entity's orientation from hkQuaternion
void DestructableObjects::SetOrientation(const hkQuaternion* qOrientation)
{
	m_qOrientation.x = qOrientation->getComponent<0>();
	m_qOrientation.y = qOrientation->getComponent<1>();
	m_qOrientation.z = qOrientation->getComponent<2>();
	m_qOrientation.w = qOrientation->getComponent<3>();

}

// rotates entity's position by yaw/pitch/roll
void DestructableObjects::RotateBody(float yaw, float pitch, float roll)
{
	if(m_pBody)
		HM->RotateBody(m_pBody, pitch,yaw,roll);
}

// sets the entity's transform from havok body
void DestructableObjects::SetTransform(const hkTransform* mTransform)
{
	
	m_mTransform(0,0) = mTransform->getElement<0,0>();
	m_mTransform(0,1) = mTransform->getElement<0,1>();
	m_mTransform(0,2) = mTransform->getElement<0,2>();
	m_mTransform(0,3) = mTransform->getElement<0,3>();
	m_mTransform(1,0) = mTransform->getElement<1,0>();
	m_mTransform(1,1) = mTransform->getElement<1,1>();
	m_mTransform(1,2) = mTransform->getElement<1,2>();
	m_mTransform(1,3) = mTransform->getElement<1,3>();
	m_mTransform(2,0) = mTransform->getElement<2,0>();
	m_mTransform(2,1) = mTransform->getElement<2,1>();
	m_mTransform(2,2) = mTransform->getElement<2,2>();
	m_mTransform(2,3) = mTransform->getElement<2,3>();
	m_mTransform(3,0) = mTransform->getElement<3,0>();
	m_mTransform(3,1) = mTransform->getElement<3,1>();
	m_mTransform(3,2) = mTransform->getElement<3,2>();
	m_mTransform(3,3) = mTransform->getElement<3,3>();

}