#pragma once
#include "Turbine.h"

class MKIII : public Turbine
{
public:
	MKIII(void);
	MKIII(Vector3 vPosition, Vector3 vLeftEngineOffset,
		Vector3 vRightEngineOffset, Vector3 vPodOffset,
		Vector3 vLeftEngineHalfLengths, Vector3 vRightEngineHalfLengths,
		Vector3 vPodHalfLengths, char* pPodMeshName, char* pEngineMeshName);
	~MKIII(void);
};

