#include "Turret.h"
#include "../UI/ScreenManager.h"
#include "../WorldManager.h"
#include "Turbine.h"

#define MEM ScreenMan->GetMainCore()->m_mMainMemory
#define GFX ScreenMan->GetGraphics()

// Flags to focus lasers at center target point or to do straight line lasers from the barrels
#define TARGET_POINT 1

Turret::Turret()
	:BaseGameEntity(ET_TURRET)
{
	m_pTarget		= 0;
	m_pLeftLaser	= 0;
	m_pRightLaser	= 0;
	m_iBarrelID		= -1;
	m_iSwivalID		= -1;
	m_fBarrelLength	= 0.0f;
	m_fDetectionDist = 60.0f;
	m_fFOV			= 130.0f * 0.5f;	// Full 130 degrees FOV
	m_bIsRenderable	= true;
	m_pMeshName		= "Sphere(10).X";//"Box.x";
	m_cBarrelMesh	= "Cylinder.X";
	m_cSwivalMesh	= "Box.x";//"Sphere(10).X";
	m_vScale		= Vector3(2.0f, 2.0f, 2.0f);
	m_vDirection	= Vector3(0.0f, 0.0f, 1.0f);
}
Turret::Turret(std::string turretMesh, std::string swivalMesh, ::string barrelMesh)
	:BaseGameEntity(ET_TURRET)
{
	m_pTarget		= 0;
	m_pLeftLaser	= 0;
	m_pRightLaser	= 0;
	m_iBarrelID		= -1;
	m_iSwivalID		= -1;
	m_fBarrelLength	= 0.0f;
	m_fDetectionDist = 60.0f;
	m_fFOV			= 130.0f * 0.5f;	// Full 130 degrees FOV
	m_bIsRenderable	= true;
	m_pMeshName		= turretMesh;
	m_cBarrelMesh	= barrelMesh;
	m_cSwivalMesh	= swivalMesh;
	m_vDirection	= Vector3(0.0f, 0.0f, 1.0f);
}
Turret::~Turret()
{
}

void Turret::Update(float dt)
{
	// Do general update
	BaseGameEntity::Update(dt);

	if(m_pTarget)	// Check if a target has been set yet
	{
		// Detect if target is within range
		Vector3 toTarget = *m_pTarget - (m_vPosition + m_vBarrelOffset);

		// Make sure the target is close enough to be detected
		if(toTarget.Magnitude() <= m_fDetectionDist)
		{
			toTarget.Normalize();

			// Get the angle between the two vectors
			float angle = acosf(m_vDirection * toTarget) * 180.0f /  D3DX_PI;

			// Start moving barrel towards the target
			if( angle <= m_fFOV )
			{
				// Interpolate the direction to face the target
				m_vBarrelDir += (toTarget - m_vBarrelDir)*dt;
				m_vBarrelDir.Normalize();
		
				// Get the angle between the two vectors
				angle = acosf(m_vBarrelDir * toTarget) * 180.0f / D3DX_PI;
		
				//#TODO - Remove: Demo by showing yellow as beeing spotted by the turret
				//GFX->SetMeshColor(m_iBarrelID, &m_cBarrelMesh[0], D3DXCOLOR(0.5f, 0.2f, 0.0f, 1.0f));
				//GFX->SetMeshColor(m_iSwivalID, &m_cSwivalMesh[0], D3DXCOLOR(0.5f, 0.2f, 0.0f, 1.0f));

				// If barrel is pointing near the target, begin shooting
				if(angle <= 5.0f)
				{
					// Apply damage to the pod over time
					GPM->GetTurbine()->ApplyDamage( 10.0f * dt, e_TurretDamage);
				}
				if(angle <= 10.0f )
				{
					//#TODO - Remove: Demo by showing red as beeing in the turret's shooting zone
					//GFX->SetMeshColor(m_iBarrelID, &m_cBarrelMesh[0], D3DXCOLOR(0.5f, 0.0f, 0.0f, 1.0f));
					//GFX->SetMeshColor(m_iSwivalID, &m_cSwivalMesh[0], D3DXCOLOR(0.5f, 0.0f, 0.0f, 1.0f));
					

					// Set laser offset
					Quaternion rot;
#if TARGET_POINT
					rot.QuatFromVectors(Vector3(0.0f, 0.0f, 1.0f), m_vBarrelDir);
#else
					// Calcluate laser offset
					if(!m_pLeftLaser)
						m_vLaserDir = m_vBarrelDir;
					Vector3 fromLaser = *m_pTarget - (m_vPosition + m_vSwivalOffset + m_vBarrelOffset);
					fromLaser.Normalize();
					m_vLaserDir += (fromLaser - m_vLaserDir)*dt;
					m_vLaserDir.Normalize();
					rot.QuatFromVectors(Vector3(0.0f, 0.0f, 1.0f), m_vLaserDir);
#endif
					rot.x = 0.0f;
					rot.z = 0.0f;
					rot.Normalize();
					m_vLeftLaser = rot.GetRight();
					m_vLeftLaser.Normalize();
					m_vRightLaser = m_vLeftLaser;
#if TARGET_POINT
					m_vLeftLaser = m_vPosition + m_vSwivalOffset + m_vBarrelOffset*0.5f + m_vBarrelDir * m_fBarrelLength*2.0f + m_vLeftLaser * 2.0f;
					m_vRightLaser = m_vPosition + m_vSwivalOffset + m_vBarrelOffset*0.5f + m_vBarrelDir * m_fBarrelLength*2.0f - m_vRightLaser * 2.0f;
#else
					m_vLeftLaser = m_vPosition + m_vSwivalOffset + m_vBarrelOffset + m_vLeftLaser * 2.0f;
					m_vRightLaser = m_vPosition + m_vSwivalOffset + m_vBarrelOffset - m_vRightLaser * 2.0f;
#endif
					// Create laser beam
					if(!m_pLeftLaser)
					{
#if TARGET_POINT
						m_pLeftLaser = new Laser(&m_vLeftLaser, m_pTarget, 0, 0);//&m_vLaserDir, 0);//&m_qBarrelOrientation);
						m_pLeftLaser->Init();
						m_pRightLaser = new Laser(&m_vRightLaser, m_pTarget, 0, 0);//&m_vLaserDir, 0);//&m_qBarrelOrientation);
						m_pRightLaser->Init();
#else
						m_pLeftLaser = new Laser(&m_vLeftLaser, m_pTarget, &m_vLaserDir, 0);//&m_qBarrelOrientation);
						m_pLeftLaser->Init();
						m_pRightLaser = new Laser(&m_vRightLaser, m_pTarget, &m_vLaserDir, 0);//&m_qBarrelOrientation);
						m_pRightLaser->Init();
#endif
					}
					else
					{
						m_pLeftLaser->Update(dt);
						m_pRightLaser->Update(dt);
					}
				}
				else if(m_pLeftLaser)
				{
					// Remove laser beam
					m_pLeftLaser->Shutdown();
					m_pRightLaser->Shutdown();
					delete m_pLeftLaser;
					delete m_pRightLaser;
					m_pLeftLaser = 0;
					m_pRightLaser = 0;
				}
			}
			else
			{
				// If target is out of FOV
				// Rotate back to origiinal position
				m_vBarrelDir += (m_vDirection - m_vBarrelDir)*dt;
				m_vBarrelDir.Normalize();
			
				//#TODO - Remove: Demo by showing original color when out of turret's fov
				//GFX->SetMeshColor(m_iBarrelID, &m_cBarrelMesh[0], m_vColor);
				//GFX->SetMeshColor(m_iSwivalID, &m_cSwivalMesh[0], m_vColor);
				
				// Remove laser beam
				if(m_pLeftLaser)
				{
					m_pLeftLaser->Shutdown();
					m_pRightLaser->Shutdown();
					delete m_pLeftLaser;
					delete m_pRightLaser;
					m_pLeftLaser = 0;
					m_pRightLaser = 0;
				}
			}
		}
		else
		{
			// If target is out of range
			// Rotate back to origiinal position
			m_vBarrelDir += (m_vDirection - m_vBarrelDir)*dt;
			m_vBarrelDir.Normalize();
			
			//#TODO - Remove: Demo by showing original color when out of turret's fov
			//GFX->SetMeshColor(m_iBarrelID, &m_cBarrelMesh[0], m_vColor);
			//GFX->SetMeshColor(m_iSwivalID, &m_cSwivalMesh[0], m_vColor);
			
			// Remove laser beam
			if(m_pLeftLaser)
			{
				m_pLeftLaser->Shutdown();
				m_pRightLaser->Shutdown();
				delete m_pLeftLaser;
				delete m_pRightLaser;
				m_pLeftLaser = 0;
				m_pRightLaser = 0;
			}
		}
	}

	// Update barrel graphics
	if(m_iBarrelID >= 0)
	{
		// Update quaternion with interpolated direction
		m_qBarrelOrientation.QuatFromVectors(Vector3(0.0f, 0.0f, 1.0f), m_vBarrelDir);
		Vector3 eular = m_qBarrelOrientation.GetYawPitchRoll();
		m_qBarrelOrientation = Quaternion(Vector3(1.0f, 0.0f, 0.0f), eular.y) * Quaternion(Vector3(0.0f, 1.0f, 0.0f), eular.x);
		m_qBarrelOrientation.Normalize();

		// Update barrel position
		m_vBarrelPos = m_vPosition  + m_vSwivalOffset + m_vBarrelDir * m_fBarrelLength;

		// Update graphics barrel object
		MEM->GetSharedResource(m_iBarrelID)->SetPosition(m_vBarrelPos + m_vBarrelOffset);
		MEM->GetSharedResource(m_iBarrelID)->SetOrientation(m_qBarrelOrientation);
	}
	if(m_iSwivalID >= 0)
	{
		Quaternion offset = m_qBarrelOrientation;
		offset.x = 0.0f;
		offset.z = 0.0f;
		offset.Normalize();

		// Update graphics turret swival object
		MEM->GetSharedResource(m_iSwivalID)->SetPosition(m_vPosition + m_vSwivalOffset);
		MEM->GetSharedResource(m_iSwivalID)->SetOrientation(offset);
	}
}

void Turret::Load(TiXmlElement* node)
{
	// Gather and Set object information
	BaseGameEntity::Load(node);

	// Load turret information
	m_cBarrelMesh = node->Attribute("BarrelMesh");
	m_cSwivalMesh = node->Attribute("SwivalMesh");
	node->QueryFloatAttribute("FOV", &m_fFOV);
	node->QueryFloatAttribute("DetectionDist", &m_fDetectionDist);
}
void Turret::Save(TiXmlElement* obj)
{
	// Create the base object info
	BaseGameEntity::Save(obj);

	// Save Turret information
	obj->SetAttribute("BarrelMesh", m_cBarrelMesh.c_str());
	obj->SetAttribute("SwivalMesh", m_cSwivalMesh.c_str());
	obj->SetDoubleAttribute("FOV", m_fFOV);
	obj->SetDoubleAttribute("DetectionDist", m_fDetectionDist);
}

std::shared_ptr<BaseGameEntity> Turret::Clone()
{
	// Check if we've reached our limit on copies
	if(m_iNumCopies >= m_iMaxNumCopies)
		return 0;

	// Create clone
	std::shared_ptr<Turret> clone = std::shared_ptr<Turret>(new Turret());
	
	// Set all the attributes for the clone that are not unique to a specific instance.
	// This will be used to create instances of this object in the level designer.
	// Brief example:
	clone->SetMeshName(&m_pMeshName[0]);
	if(!m_pTextureName.empty())
		clone->SetTextureName(&m_pTextureName[0]);
	clone->SetRenderable(m_bIsRenderable);
	clone->SetColor(m_vColor);
	clone->SetScale(m_vScale);
	clone->SetOffset(m_vOffset);

	// Set turret attributes
	clone->m_cBarrelMesh = m_cBarrelMesh;
	clone->m_cSwivalMesh = m_cSwivalMesh;
	clone->m_fFOV = m_fFOV;
	clone->m_fDetectionDist = m_fDetectionDist;
	
	// Setup the copy counter to point to the original counter
	clone->m_pNumCopies = &m_iNumCopies;
	m_iNumCopies++;

	// Initialize the object
	clone->Init();
	return clone;
}

void Turret::Init()
{
	BaseGameEntity::Init();

	// Initialize turret attributes
	SetOrientation(m_qOrientation);	// Make sure Direction vector has been created

	// Create Graphics Barrel
	m_iBarrelID = MEM->CreateSharedResource();
	GFX->CreateMesh(m_iBarrelID, &m_cBarrelMesh[0], false);
	GFX->SetMeshColor(m_iBarrelID, &m_cBarrelMesh[0], m_vColor);
	Vector3 barrelScale;
	if(m_cBarrelMesh == "Cylinder.X")
		barrelScale = Vector3(0.1f, 0.1f, 0.1f);
	else
		barrelScale = m_vScale;
	GFX->SetMeshScale(m_iBarrelID, &m_cBarrelMesh[0], barrelScale);

	// Get the length of the barrel
	Vector3 halfExt;
	GFX->GetMeshBV(&m_cBarrelMesh[0], halfExt);
	m_fBarrelLength = halfExt.z * barrelScale.z;
	m_vBarrelOffset = halfExt.ComponentProduct(barrelScale);
	m_vBarrelOffset.x = 0.0f;
	m_vBarrelOffset.z = 0.0f;

	// Update quaternion with direction
	m_qBarrelOrientation.QuatFromVectors(Vector3(0.0f, 0.0f, 1.0f), m_vBarrelDir);

	// Update barrel position
	m_vBarrelPos = m_vPosition + m_vBarrelDir * m_fBarrelLength;
	MEM->GetSharedResource(m_iBarrelID)->SetPosition(m_vBarrelPos);
	MEM->GetSharedResource(m_iBarrelID)->SetOrientation(m_qBarrelOrientation);

	// Create swival object
	m_iSwivalID = MEM->CreateSharedResource();
	GFX->CreateMesh(m_iSwivalID, &m_cSwivalMesh[0], false);
	GFX->SetMeshColor(m_iSwivalID, &m_cSwivalMesh[0], m_vColor);
	GFX->SetMeshScale(m_iSwivalID, &m_cSwivalMesh[0], m_vScale);

	// Get the length of the swival object
	GFX->GetMeshBV(&m_cSwivalMesh[0], halfExt);
	m_vSwivalOffset.y = halfExt.y * m_vScale.y;
	m_vSwivalOffset.x = 0.0f;
	m_vSwivalOffset.z = 0.0f;
	
	if(m_pMeshName == "Turret_GroundBase.X")
	{
		// Set up the custom positions for this mesh
		GFX->GetMeshBV(&m_pMeshName[0], halfExt);
		//m_vSwivalOffset.z -= 0.875f;
		//m_vSwivalOffset.x += 0.1f;
		m_vSwivalOffset.y += (halfExt.y * m_vScale.y)*0.5f;

		// Custom barrel position
		//m_vBarrelOffset.y += halfExt.y;//m_vSwivalOffset;
	}
	
	// Update graphics turret swival object
	MEM->GetSharedResource(m_iSwivalID)->SetPosition(m_vPosition + m_vSwivalOffset);
	MEM->GetSharedResource(m_iSwivalID)->SetOrientation(m_qBarrelOrientation);
}
void Turret::Shutdown()
{
	BaseGameEntity::Shutdown();

	// Shutdown turret attributes
	if(m_iBarrelID >= 0)
	{
		GFX->RemoveMesh(m_iBarrelID, &m_cBarrelMesh[0]);
		MEM->FreeSharedResource(m_iBarrelID);
	}
	if(m_iSwivalID >= 0)
	{
		GFX->RemoveMesh(m_iSwivalID, &m_cSwivalMesh[0]);
		MEM->FreeSharedResource(m_iSwivalID);
	}
	if(m_pLeftLaser)
	{
		m_pLeftLaser->Shutdown();
		m_pRightLaser->Shutdown();
		delete m_pLeftLaser;
		delete m_pRightLaser;
		m_pLeftLaser = 0;
		m_pRightLaser = 0;
	}
}

void Turret::SetOrientation(Quaternion pOrientation)
{
	// Restrict rotation to yaw only
	pOrientation.x = 0.0f;
	pOrientation.z = 0.0f;
	pOrientation.Normalize();
	m_qOrientation = pOrientation;
	m_qBarrelOrientation = m_qOrientation;
	
	// Set the normal direction with the quaternion
	D3DXMATRIX O;
	D3DXMatrixRotationQuaternion(&O, &m_qBarrelOrientation);
	D3DXVec3TransformNormal(&m_vDirection, &Vector3(0.0f, 0.0f, 1.0f), &O);
	m_vDirection.Normalize();
	m_vBarrelDir = m_vDirection;
	m_vLaserDir = m_vBarrelDir;
}
void Turret::SetOrientation(float yaw, float pitch, float roll)
{
	D3DXQuaternionRotationYawPitchRoll(&qScratch, yaw, pitch,roll);
	SetOrientation(qScratch);
}
void Turret::SetScale(Vector3 scale)
{
	if(m_vScale == scale)
		return;

	m_vScale = scale;

	if(ScreenMan->GetEntityManager()->DisplayHiddenObjects() && m_iBarrelID > 0)
	{
		auto GCI = ScreenMan->GetGraphics();
		GCI->SetMeshScale(m_iBarrelID, &m_cBarrelMesh[0], m_vScale);
		GCI->SetMeshScale(m_iSwivalID, &m_cSwivalMesh[0], m_vScale);
		
		// Get the length of the barrel
		Vector3 halfExt;
		GFX->GetMeshBV(&m_cBarrelMesh[0], halfExt);
		m_fBarrelLength = halfExt.z * m_vScale.z;
		m_vBarrelOffset = halfExt.ComponentProduct(m_vScale);
		m_vBarrelOffset.x = 0.0f;
		m_vBarrelOffset.z = 0.0f;

		// Update quaternion with direction
		m_qBarrelOrientation.QuatFromVectors(Vector3(0.0f, 0.0f, 1.0f), m_vBarrelDir);

		// Update barrel position
		m_vBarrelPos = m_vPosition + m_vBarrelDir * m_fBarrelLength;
		MEM->GetSharedResource(m_iBarrelID)->SetPosition(m_vBarrelPos);
		MEM->GetSharedResource(m_iBarrelID)->SetOrientation(m_qBarrelOrientation);

		// Get the length of the swival object
		GFX->GetMeshBV(&m_cSwivalMesh[0], halfExt);
		m_vSwivalOffset.y = halfExt.y * m_vScale.y;
		m_vSwivalOffset.x = 0.0f;
		m_vSwivalOffset.z = 0.0f;
	
		if(m_pMeshName == "Turret_GroundBase.X")
		{
			// Set up the custom positions for this mesh
			GFX->GetMeshBV(&m_pMeshName[0], halfExt);
			//m_vSwivalOffset.z -= 0.875f;
			//m_vSwivalOffset.x += 0.1f;
			m_vSwivalOffset.y += (halfExt.y * m_vScale.y)*0.5f;

			// Custom barrel position
			//m_vBarrelOffset.y += halfExt.y;//m_vSwivalOffset;
		}
	
		// Update graphics turret swival object
		MEM->GetSharedResource(m_iSwivalID)->SetPosition(m_vPosition + m_vSwivalOffset);
		MEM->GetSharedResource(m_iSwivalID)->SetOrientation(m_qBarrelOrientation);
		Update(0.0f);
	}
}


// This is the laser beam projectile
Laser::Laser(Vector3* start, Vector3* end, Vector3* direction, Quaternion* orientation, std::string mesh)
	:m_vStartPos(start),
	m_vEndPos(end),
	m_vDirection(direction),
	m_qOrientation(orientation),
	m_cMeshName(mesh)
{
	m_iID = -1;
	m_vScale = Vector3(0.02f, 0.02f, 0.1f);
}
Laser::~Laser()
{
}

void Laser::Update(float dt)
{
	if(m_iID >= 0)
	{
		Vector3 pos = *m_vEndPos - *m_vStartPos;
		float scale = pos.Magnitude() / m_fLength;
		GFX->SetMeshScale(m_iID, &m_cMeshName[0], Vector3(m_vScale.x, m_vScale.y, m_vScale.z*scale));
		if(m_vDirection)
			MEM->GetSharedResource(m_iID)->SetPosition(*m_vStartPos + *m_vDirection * (pos.Magnitude()*0.5f));
		else
			MEM->GetSharedResource(m_iID)->SetPosition(*m_vStartPos + pos * 0.5f);
		if(m_qOrientation)
			MEM->GetSharedResource(m_iID)->SetOrientation(*m_qOrientation);
		else if(m_vDirection)
		{
			Quaternion rot;
			rot.QuatFromVectors(Vector3(0.0f, 0.0f, 1.0f), *m_vDirection);
			MEM->GetSharedResource(m_iID)->SetOrientation(rot);
		}
		else
		{
			Quaternion rot;
			pos.Normalize();
			rot.QuatFromVectors(Vector3(0.0f, 0.0f, 1.0f), pos);
			MEM->GetSharedResource(m_iID)->SetOrientation(rot);
		}
	}
}

void Laser::Init()
{
	m_iID = MEM->CreateSharedResource();
	GFX->CreateMesh(m_iID, &m_cMeshName[0], false);
	GFX->SetMeshColor(m_iID, &m_cMeshName[0], D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));

	// Calculate the length of the cylinder
	Vector3 halfExt;
	GFX->GetMeshBV(&m_cMeshName[0], halfExt);
	m_fLength = (halfExt.z * m_vScale.z) * 2.0f;

	// Calculate scale, position, and rotation
	Vector3 pos = *m_vEndPos - *m_vStartPos;
	float scale = pos.Magnitude() / m_fLength;
	GFX->SetMeshScale(m_iID, &m_cMeshName[0], Vector3(m_vScale.x, m_vScale.y, scale*m_vScale.z));
	if(m_vDirection)
		MEM->GetSharedResource(m_iID)->SetPosition(*m_vStartPos + *m_vDirection * (pos.Magnitude()*0.5f));
	else
		MEM->GetSharedResource(m_iID)->SetPosition(*m_vStartPos + pos * 0.5f);
	if(m_qOrientation)
		MEM->GetSharedResource(m_iID)->SetOrientation(*m_qOrientation);
	else if(m_vDirection)
	{
		Quaternion rot;
		rot.QuatFromVectors(Vector3(0.0f, 0.0f, 1.0f), *m_vDirection);
		MEM->GetSharedResource(m_iID)->SetOrientation(rot);
	}
	else
	{
		Quaternion rot;
		pos.Normalize();
		rot.QuatFromVectors(Vector3(0.0f, 0.0f, 1.0f), pos);
		MEM->GetSharedResource(m_iID)->SetOrientation(rot);
	}
}
void Laser::Shutdown()
{
	if(m_iID >= 0)
	{
		GFX->RemoveMesh(m_iID, &m_cMeshName[0]);
		MEM->FreeSharedResource(m_iID);
	}
}

void Laser::SetColor(Vector4 color)
{
	if(m_iID >= 0)
		GFX->SetMeshColor(m_iID, &m_cMeshName[0], D3DXCOLOR(color.x, color.y, color.z, color.w));
}

void Laser::SetScale(Vector3 scale)
{
	m_vScale = scale;
	if(m_iID >= 0)
	{
		// Calculate the length of the cylinder
		Vector3 halfExt;
		GFX->GetMeshBV(&m_cMeshName[0], halfExt);
		m_fLength = (halfExt.z * scale.z) * 2.0f;
		
		Vector3 pos = *m_vEndPos - *m_vStartPos;
		scale.z *= pos.Magnitude() / m_fLength;
		GFX->SetMeshScale(m_iID, &m_cMeshName[0], scale);
	}
}
