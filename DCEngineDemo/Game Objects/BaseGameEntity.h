#pragma once
#include <DCMathLib.h>
#include "../Controls/ThirdParty/TinyXML/tinyxml.h"

/*
	BaseGameEntity class - VIRTUAL class that all created
	entities derive from. Racers, AIRacers, LevelObjects all 
	share this common interface

*/

enum EntityType
{
	ET_UNASSIGNED,
	ET_LEVELOBJECT,
	ET_TRIGGERVOLUME,
	ET_RACER,
	ET_AIRACER,
	ET_ENGINE,
	ET_POD,
	ET_LIGHT,
	ET_CAMERA,
	ET_PHYSICSOBJECT,
	ET_SPAWNPOINT,
	ET_TURRET,
	ET_SMOKETRAP,
	ET_DESTRUCTABLE,
	ET_LIGHTNINGTRAP,
	ET_LIGHTNINGTRAPCONNECTOR,
    ET_Smasher
};

class BaseGameEntity
{
protected:
	// entity's shared ID
	int m_ID;

	// entity's position in world coordinates
	Vector3 m_vPosition;

	// entity's offset (position in local coordinates)
	Vector3 m_vOffset;

	// entity's orientation
	Quaternion m_qOrientation;

	// entity's rotational offset (in case model is off)
	Quaternion m_qRotationOffset;

	// entity's scale
	Vector3	m_vScale;

	// entity's mesh name
	std::string m_pMeshName;

	// entity's mesh is renderal in-game
	bool m_bIsRenderable;

	// entity's texture name
	std::string m_pTextureName;

	// entity's color
	D3DXCOLOR	m_vColor;

	// type of entity this is
	EntityType m_eType;

	// transform matrix
	Matrix4 m_mTransform;

	// number of copies
	int		m_iNumCopies;
	int*	m_pNumCopies;	// Points to the original counter
	// max number of copies allowed
	int		m_iMaxNumCopies;

	/////////////////
	// SCRATCH VARS
	////////////////
	int i,j,k;
	float a,b,c;
	Vector3 vScratch;
	D3DXMATRIX mScratch;
	Quaternion qScratch;

public:
	// default constructor - sets all values to -1 or NULL
	BaseGameEntity(void);
	// para const
	BaseGameEntity(	EntityType eType);

	BaseGameEntity(EntityType eType, char* pMeshName, char* pTexName, Vector3 vMeshScale);

	// virtual destructor to keep class virtual
	virtual ~BaseGameEntity(void);
	
	// compare entities
	virtual bool operator== (const BaseGameEntity& entity);

	//////////////////////
	// UTILITIES
	/////////////////////
	// update and sync graphics,physics, and game logic
	virtual void Update(float dt);
	virtual void Load(TiXmlElement* node);		// Loads the attributes
	virtual void Save(TiXmlElement* obj);	// Call this original function at the begining of the function that overwrites this
	virtual std::shared_ptr<BaseGameEntity>	Clone();	// Clone these attributes into a new BaseGameEntity
	virtual void Init();						// called at the end of load
	virtual void Shutdown();					// cleans the entity

	//////////////////////
	// ACCESSORS
	/////////////////////
	// returns shared id of entity 
	int GetID(){return m_ID;}
	// returns position of entity 
	Vector3 GetPosition(){return m_vPosition;}
	// returns requested component of position
	float GetPosition(int idx);
	// returns entity's offset 
	Vector3 GetOffset(){return m_vOffset;}
	// returns entity's scale
	Vector3 GetScale(){return m_vScale;}
	// returns entity's orientation
	Quaternion GetOrientation(){return m_qOrientation;}
	// returns entity's mesh name
	char* GetMeshName(){if(m_pMeshName.empty()) return 0; else return &m_pMeshName[0];}
	// returns entity's texture name
	char* GetTextureName(){if(m_pTextureName.empty())return 0; else return &m_pTextureName[0];}
	// returns entity's color
	D3DXCOLOR GetColor(){return m_vColor; }
	// gets the entity's type
	EntityType GetEntityType(){return m_eType;}
	// gets if the entity can be rendered in gameplay
	bool GetRenderable(){return m_bIsRenderable;}
	// returns transform matrix
	Matrix4 GetTransform(){return m_mTransform;}
	// returns the max number of copies that can be made
	int	GetMaxNumberCopies(){return m_iMaxNumCopies;}
	// returns the current number of copies
	int& GetNumberCopies(){return m_iNumCopies;}

	//////////////////////
	// MUTATORS
	/////////////////////
	// Sets shared id of entity 
	void SetID(int ID){m_ID = ID;}
	// Sets position of entity 
	virtual void SetPosition(Vector3 vPos);
	// Sets position of entity by component
	virtual void SetPosition(float x, float y, float z);
	// sets a single component of the entity's position
	virtual void SetPosition(int idx, float fValue);
	// sets an entity's offset
	void SetOffset(Vector3 vOffset){m_vOffset = vOffset;}
	// set an entity's offset via components
	void SetOffset(float x, float y, float z){m_vOffset.x = x; m_vOffset.y = y; m_vOffset.z = z;}
	// set an entity's scale
	virtual void SetScale(Vector3 vScale){m_vScale = vScale;}
	// Sets entity's orientation
	virtual void SetOrientation(Quaternion pOrientation){m_qOrientation = pOrientation;}
	// sets entity's orientation by yaw/pitch/roll
	virtual void SetOrientation(float yaw, float pitch, float roll);
	// rotates entity's position by yaw/pitch/roll
	virtual void RotateBody(float yaw, float pitch, float roll);
	// Sets entity's mesh name
	void SetMeshName(char* pMeshName){m_pMeshName = pMeshName;}
	// Sets entity's texture name
	void SetTextureName(char* pTextureName){m_pTextureName = pTextureName;}
	// Sets entity's color
	void SetColor(D3DXCOLOR color){m_vColor = color;}
	// Sets the entity's type
	void SetEntityType(EntityType eType){m_eType = eType; }
	// Sets if the entity can be rendered in gameplay
	void SetRenderable(bool enableRender){m_bIsRenderable = enableRender; }
	// sets rotational offset
	void SetRotationOffset(Quaternion qOffset){m_qRotationOffset = qOffset;}
	// transforms entity's transform matrices by another matrix
	void Transform(Matrix4 mTransform);
	// sets the max number of copies allowed
	void SetMaxNumCopies(int maxCopies){m_iMaxNumCopies = maxCopies;}
	// sets the parent to copy from
	void SetNumCopies(int* parent){m_pNumCopies = parent;}

};

