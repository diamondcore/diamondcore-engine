#include "CameraRig.h"
#include "GameplayCam.h"
#include "../../Game Objects/BaseGameEntity.h"
#include "../../WorldManager.h"
#include "../../UI/ScreenManager.h"
#include <dinput.h>

CameraRig::CameraRig(void)
{
	m_iNextID = 0;

	m_iNumCams = 0;

	m_bAttached = false;

	m_bLooseRig = false;

	m_pCurrentCamView = 0;

	m_vPosition = Vector3(0,0,0);
}


CameraRig::~CameraRig(void)
{

}

/////////////////
// UTILITIES
/////////////////
// updates the current cam view
void CameraRig::Update(float dt)
{

	// set the cam's position relative to the rig
	// step 1:get cam local offset
	vScratch = m_pCurrentCamView->GetOffset();
	// step 2: multiply it by the rig's transform
	Matrix4 inv;
	D3DXMatrixInverse(&inv, 0, &m_mTransform);
	inv._14	= m_mTransform._14;
	inv._24	= m_mTransform._24;
	inv._34	= m_mTransform._34;
	vScratch = inv * vScratch;
	// step 3: set the cam's final position
	// camera error checking if it goes into trash values, reset to scratch position
	if ( Vector3(m_pCurrentCamView->GetPosition()).MagnitudeSquared() > 0)
		m_pCurrentCamView->SetPos(LerpMovement(vScratch,dt,5.0f, 20.0f) );
	else
		m_pCurrentCamView->SetPos(vScratch);
	
	// if rig is attached to something
	if (m_bAttached)
	{
		// update RIG position based on that
		m_vPosition = m_pAttachedTo->GetPosition();
		// update RIG orientation
		if(!m_bLooseRig)
			SetOrientation(m_pAttachedTo->GetOrientation() );
		// set the gameplay cam parent xform
		//m_pCurrentCamView->SetParentsTransform(m_mTransform);
		// set the cam's look at
		m_pCurrentCamView->SetLookAt(m_pAttachedTo->GetPosition(), Vector3(0,1,0)  );		
		
	}
	
	

	
	

	
	
	// update the current cam view
	m_pCurrentCamView->Update(dt);
}

// adds a camera to the rig, position is in local coordinates
// SPECIAL NOTE: build the camera while the rig is in its initial
// position. Otherwise you're just asking for trouble.
void CameraRig::AddCamera(float x, float y, float z, Vector3 vLookat)
{
	// create a new camera
	GameplayCam* temp = new GameplayCam();

	// set its id
	temp->SetID(m_iNextID++);

	// increment number of cams
	++m_iNumCams;

	// set its type
	temp->SetType(RIGCAM);

	// store its offset
	vScratch.x = x;
	vScratch.y = y;
	vScratch.z = z;
	temp->SetOffset(vScratch);

	// set its lookat / up
	temp->SetLookAt(vLookat,Vector3(0,1,0) );

	// if there's no active camera (this is the first one created)
	//	make it the active view
	if (m_pCurrentCamView == NULL)
	{
		m_pCurrentCamView = temp;
		// give graphics the new view matrix
		//ScreenMan->GetGraphics()->SetCameraView((void*)(&m_pCurrentCamView->GetView()) );
		//ScreenMan->GetGraphics()->SetCameraPos((void*)(&m_pCurrentCamView->GetPosition()) );
	}
	
	// push onto camera vec
	m_vCams.push_back(temp);

		
}
// cycles to the next camera on the rig
void CameraRig::NextCamView()
{
	// get current cam's id
	i = m_pCurrentCamView->GetID();

	// increment i
	++i;

	// if i is greater than numcams-1, change to 0
	if (i > (m_iNumCams - 1))
		m_pCurrentCamView = m_vCams[0];
	else
		m_pCurrentCamView = m_vCams[i];

	// give graphics the new view matrix
	ScreenMan->GetGraphics()->SetCameraView((void*)(&m_pCurrentCamView->GetView()) );
	ScreenMan->GetGraphics()->SetCameraPos((void*)(&m_pCurrentCamView->GetPosition()) );
}

// attaches rig to an object
void CameraRig::AttachRig(BaseGameEntity* pAttachTo)
{
	m_pAttachedTo = pAttachTo;

	m_bAttached = true;
}

void CameraRig::HandleInput(IInputCore* inputState, float dt)
{
	//// move rig
	//if (inputState->IsKeyDown(DIK_I))
	//	m_vPosition.z += 1 * dt;
	//if (inputState->IsKeyDown(DIK_K))
	//	m_vPosition.z -= 1 * dt;
	//if (inputState->IsKeyDown(DIK_J))
	//	m_vPosition.x -= 1 * dt;
	//if (inputState->IsKeyDown(DIK_L))
	//	m_vPosition.x += 1 * dt;
	
	m_pCurrentCamView->HandleInput(inputState, dt);
	
	//// if rig has velocity, update position based on that
	//if (m_vVelocity.MagnitudeSquared() > 0)
	//	m_vPosition.AddScaledVector(m_vVelocity,dt);

	// check to see if need to switch cams
	if (inputState->IsNewKeyRelease(DIK_N) || inputState->IsNewButtonRelease(BACK) )
		NextCamView();
	
}

// moving average for cam movement
Vector3 CameraRig::CalculateNewPosition(Vector3* vNextPosition, int iNumPositionsToKeep)
{
	vScratch1.x = 0;
	vScratch1.y = 0;
	vScratch1.z = 0;

	// add next position to the list of previous positions
	m_vPreviousPositions.push_back(vNextPosition);

	// remove unneeded nodes from front
	while (m_vPreviousPositions.size() > iNumPositionsToKeep)
	{
		m_vPreviousPositions.erase(m_vPreviousPositions.begin());
	}

	// sum all positions
	for (i = m_vPreviousPositions.size()-1; i >= 0; --i)
	{
		vScratch1 += *m_vPreviousPositions[i];
	}

	// average them 
	vScratch1.Average(m_vPreviousPositions.size());

	// return value
	return vScratch1;
}

// linear interpolation
Vector3 CameraRig::LerpMovement(Vector3 vTarget, float dt, float fLowSpeed, float fHighSpeed)
{
	// get vec to target from current 
	vScratch1 = vTarget - m_pCurrentCamView->GetPosition();

	// store magnitude squared for later use
	float fMagnitudeSquared = vScratch1.MagnitudeSquared();
	
	// see if we're at our target
	if (fMagnitudeSquared <= 10.0f)
		return m_pCurrentCamView->GetPosition();
	
	// get direction of our target
	vScratch1.Normalize();

	// if we are really far away , need to catch up
/*	if (fMagnitudeSquared > 100.0f)
	{
		vScratch1.Scale(dt * fHighSpeed);
	}
	else
	{
		// Scale direction by speed over dt
		vScratch1.Scale(dt * fLowSpeed);
	}	
*/
	//// if speed is too high, we clamp it
	//if (vScratch.Magnitude() > fLowSpeed
	
	vScratch1.Scale(dt * (fHighSpeed * (fMagnitudeSquared / 100.0f)));

	/*float fMaxSpeed = 10.0f;

	if (vScratch1.Magnitude() > fMaxSpeed)
	{
		vScratch1.Normalize();
		vScratch1.Scale(fMaxSpeed);
	}*/

	// return current pos + step
	return (m_pCurrentCamView->GetPosition() + vScratch1);
}

/////////////////
// ACCESSORS
/////////////////
const D3DXMATRIX& CameraRig::GetView() const
 {
	 return m_pCurrentCamView->GetView();
 }

const D3DXVECTOR3& CameraRig::GetLookAt() const
{
	return m_pCurrentCamView->GetLookAt();
}
const D3DXVECTOR3& CameraRig::GetUp() const
{
	return m_pCurrentCamView->GetUp();
}
D3DXVECTOR3& CameraRig::GetPosition() 
{
	return m_pCurrentCamView->GetPosition();
}


/////////////////
// MUTATORS
/////////////////
void CameraRig::SetPosition(float x, float y, float z)
{
	m_vPosition.x = x;
	m_vPosition.y = y;
	m_vPosition.z = z;
}
void CameraRig::SetPosition(Vector3 vPos)
{
	m_vPosition = vPos;
}
void CameraRig::SetVelocity(float x, float y, float z)
{
	m_vVelocity.x = x;
	m_vVelocity.y = y;
	m_vVelocity.z = z;
}
void CameraRig::SetVelocity(Vector3 vVel)
{
	m_vVelocity = vVel;
}
void CameraRig::SetOrientation(Quaternion qOrientation)
{
	m_qOrientation = qOrientation;

	D3DXMatrixIdentity(&m_mTransform);

	// creating transform from the quaternion
	D3DXMatrixRotationQuaternion(&m_mTransform, &m_qOrientation);

	// setting pos by hand
	m_mTransform(0,3) = m_vPosition.x;
	m_mTransform(1,3) = m_vPosition.y;
	m_mTransform(2,3) = m_vPosition.z;
	m_mTransform(3,3) = 1.0f;

}
void CameraRig::SetTransform(D3DXMATRIX mTransform)
{
	m_mTransform = mTransform;
}

void CameraRig::SetLookAt(D3DXVECTOR3& target, D3DXVECTOR3& up)
{
	m_pCurrentCamView->SetLookAt(target,up);
}