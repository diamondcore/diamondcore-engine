#include "GameplayCam.h"
#include "../../UI/ScreenManager.h"
#include "../../WorldManager.h"
#include <dinput.h>

#define INP GPM->GetEngine()->GetInputCore()


GameplayCam::GameplayCam()
{
	//default cam, backed up on z looking down +z - up in world up.
	m_vPosition = D3DXVECTOR3(0,0,-50);
	//its free cam, not locked on a object so its origin is its own
	m_vOrigin = m_vPosition;

	//starts looking at the origin
	m_vLookAt = D3DXVECTOR3(0,0,0) - m_vPosition;

	m_vRight = D3DXVECTOR3(1,0,0);
	m_vUp = D3DXVECTOR3(0,1,0);

	m_fSpeed = 5.0f;

	m_eMode = FREE;

	m_vLockTarget = D3DXVECTOR3(0,0,0);
	
	CalculateViewMatrix();

	
	//ScreenMan->GetGraphics()->SetCameraView((void*)(&m_mViewMatrix) );
	//ScreenMan->GetGraphics()->SetCameraPos((void*)(&m_vPosition) );
	


}

GameplayCam::GameplayCam(D3DXVECTOR3 pos, D3DXVECTOR3 look, D3DXVECTOR3 up)
{
	
}

GameplayCam::GameplayCam(float xPos, float yPos, float zPos, float xLook, float yLook, float zLook, 
		float xUp, float yUp, float zUp)
{
	
}

GameplayCam::~GameplayCam()
{

}

void GameplayCam::HandleInput(IInputCore* inputState, float dt)
{
	// TODO: move input code here.
	// Find the net direction the camera is traveling in (since the
	// camera could be running and strafing).
	D3DXVECTOR3 dir(0.0f, 0.0f, 0.0f);

	if( inputState->IsKeyDown(DIK_W) )
		dir += m_vLookAt;
	if( inputState->IsKeyDown(DIK_S) )
		dir -= m_vLookAt;
	if( inputState->IsKeyDown(DIK_D) )
		dir += m_vRight;
	if( inputState->IsKeyDown(DIK_A) )
		dir -= m_vRight;
	if( inputState->IsKeyDown(DIK_Q) )
		dir -= m_vUp;
	if( inputState->IsKeyDown(DIK_E) )
		dir += m_vUp;
	
	// Controller movement
	if (ScreenMan->GetEntityManager()->DisplayHiddenObjects())
	{
		Vector2 trigger = inputState->GetTriggers();
		if( trigger.x > 0.0f )
			dir -= m_vLookAt;
		if( trigger.y > 0.0f )
			dir += m_vLookAt;
		if( inputState->IsButtonDown(LEFT_BUMPER) )
			dir -= m_vRight;
		if( inputState->IsButtonDown(RIGHT_BUMPER) )
			dir += m_vRight;

	}
	
	// Move at mSpeed along net direction.
	D3DXVec3Normalize(&dir, &dir);
	D3DXVECTOR3 newPos = m_vPosition + dir * m_fSpeed * dt;

	float pitch = 0.0;
	float yAngle = 0.0;
	Vector2 stickPos = inputState->GetRightStick();
	if (inputState->IsMouseButtonDown(1) )
	{
		// We rotate at a fixed speed if rmb is down
		pitch  = inputState->GetMouseDY() / 150.0f;
		yAngle = inputState->GetMouseDX() / 150.0f;
	}
	else if(stickPos.LengthSquared() > 0.0f)
	{
		// We rotate at a fixed speed if right thumb stick is moved
		pitch  = stickPos.y * -dt;
		yAngle = stickPos.x * dt;
	}

	if (m_eMode == FREE)
	{
		// Rotate camera's look and up vectors around the camera's right vector.
		D3DXMATRIX R;
		D3DXMatrixRotationAxis(&R, &m_vRight, pitch);
		D3DXVec3TransformCoord(&m_vLookAt, &m_vLookAt, &R);
		D3DXVec3TransformCoord(&m_vUp, &m_vUp, &R);


		// Rotate camera axes about the world's y-axis.
		D3DXMatrixRotationY(&R, yAngle);
		D3DXVec3TransformCoord(&m_vRight, &m_vRight, &R);
		D3DXVec3TransformCoord(&m_vUp, &m_vUp, &R);
		D3DXVec3TransformCoord(&m_vLookAt, &m_vLookAt, &R);
	}
	else
	{
		//set the camera's position to an offset from its target
		m_vPosition = D3DXVECTOR3(m_vLockTarget.x,m_vLockTarget.y + 20, m_vLockTarget.z + 20);
		//set the camera to lock onto the target
		LockCamera(m_vLockTarget);

		// Rotate camera's look and up vectors around the camera's right vector.
		D3DXMATRIX R;
		D3DXMatrixRotationAxis(&R, &m_vRight, pitch);
		D3DXVec3TransformCoord(&m_vLookAt, &m_vLookAt, &R);
		D3DXVec3TransformCoord(&m_vUp, &m_vUp, &R);


		// Rotate camera axes about the world's y-axis.
		D3DXMatrixRotationY(&R, yAngle);
		D3DXVec3TransformCoord(&m_vRight, &m_vRight, &R);
		D3DXVec3TransformCoord(&m_vUp, &m_vUp, &R);
		D3DXVec3TransformCoord(&m_vLookAt, &m_vLookAt, &R);
	}

	m_vPosition = newPos;
}

void GameplayCam::Update(float dt)
{
	//if cam is locked onto an object/position
	//view can change every frame - update this
	CalculateViewMatrix();

	//m_mViewProj = m_mViewMatrix * m_mProjMatrix;
}

void GameplayCam::CalculateViewMatrix()
{

	// Keep camera's axes orthogonal to each other and of unit length.
	D3DXVec3Normalize(&m_vLookAt, &m_vLookAt);

	D3DXVec3Cross(&m_vUp, &m_vLookAt, &m_vRight);
	D3DXVec3Normalize(&m_vUp, &m_vUp);

	D3DXVec3Cross(&m_vRight, &m_vUp, &m_vLookAt);
	D3DXVec3Normalize(&m_vRight, &m_vRight);

	// Fill in the view matrix entries.

	float x = -D3DXVec3Dot(&m_vPosition, &m_vRight);
	float y = -D3DXVec3Dot(&m_vPosition, &m_vUp);
	float z = -D3DXVec3Dot(&m_vPosition, &m_vLookAt);

	m_mViewMatrix(0,0) = m_vRight.x; 
	m_mViewMatrix(1,0) = m_vRight.y; 
	m_mViewMatrix(2,0) = m_vRight.z; 
	m_mViewMatrix(3,0) = x  ;   

	m_mViewMatrix(0,1) = m_vUp.x;
	m_mViewMatrix(1,1) = m_vUp.y;
	m_mViewMatrix(2,1) = m_vUp.z;
	m_mViewMatrix(3,1) = y ;  

	m_mViewMatrix(0,2) = m_vLookAt.x; 
	m_mViewMatrix(1,2) = m_vLookAt.y; 
	m_mViewMatrix(2,2) = m_vLookAt.z; 
	m_mViewMatrix(3,2) = z ;   

	m_mViewMatrix(0,3) = 0.0f;
	m_mViewMatrix(1,3) = 0.0f;
	m_mViewMatrix(2,3) = 0.0f;
	m_mViewMatrix(3,3) = 1.0f;

	

	//if (m_eType == RIGCAM)
	//{
	//	
	//	/*m_mViewMatrix = m_mParentTransform;*/

	//	/*m_mViewMatrix(0,0) *= m_mParentTransform(0,0); 
	//	m_mViewMatrix(1,0) *= m_mParentTransform(1,0); 
	//	m_mViewMatrix(2,0) *= m_mParentTransform(2,0); */

	//	/*m_mViewMatrix(0,1) *= m_mParentTransform(0,1);
	//	m_mViewMatrix(1,1) *= m_mParentTransform(1,1);
	//	m_mViewMatrix(2,1) *= m_mParentTransform(2,1);*/
	//	

	//	//m_mViewMatrix(0,2) *= m_mParentTransform(0,2); 
	//	//m_mViewMatrix(1,2) *= m_mParentTransform(1,2); 
	//	//m_mViewMatrix(2,2) *= m_mParentTransform(2,2);
	//}
}

//void GameplayCam::CalculateProjectionMatrix()
//{
//	D3DXMatrixPerspectiveFovLH(&m_mProjMatrix,
//                               D3DXToRadian(45),    // the horizontal field of view
//							   (FLOAT)D3DAPPI->GetD3Dpp().BackBufferWidth / (FLOAT)D3DAPPI->GetD3Dpp().BackBufferHeight, // aspect ratio
//                               1.0f,    // the near view-plane
//                               1000.0f);    // the far view-plane
//}

void GameplayCam::OnLostDevice()
{

}

void GameplayCam::OnResetDevice()
{
	//CalculateProjectionMatrix();
}



//inline const D3DXMATRIX& GameplayCam::GetView() const
//{
//	return m_mViewMatrix;
//}

//const D3DXMATRIX& GameplayCam::GetProj() const
//{
//	return m_mProjMatrix;
//}
//
//const D3DXMATRIX& GameplayCam::GetViewProj() const
//{
//	return m_mViewProj;
//}

const D3DXVECTOR3& GameplayCam::GetRight() const
{
	return m_vRight;
}

const D3DXVECTOR3& GameplayCam::GetUp() const
{
	return m_vUp;
}

const D3DXVECTOR3& GameplayCam::GetLookAt() const
{
	return m_vLookAt;
}

D3DXVECTOR3& GameplayCam::GetPosition()
{
	return m_vPosition;
}

void GameplayCam::SetLookAt(D3DXVECTOR3& pos, D3DXVECTOR3& target, D3DXVECTOR3& up)
{
	D3DXVECTOR3 L = target - pos;
	D3DXVec3Normalize(&L, &L);

	D3DXVECTOR3 R;
	D3DXVec3Cross(&R, &up, &L);
	D3DXVec3Normalize(&R, &R);

	D3DXVECTOR3 U;
	D3DXVec3Cross(&U, &L, &R);
	D3DXVec3Normalize(&U, &U);

	m_vPosition   = pos;
	m_vRight = R;
	m_vUp    = U;
	m_vLookAt  = L;

	CalculateViewMatrix();
	
	m_mViewProj = m_mViewMatrix * m_mProjMatrix;
}

// assumes default configuration, y is up
void GameplayCam::SetLookAt(float x, float y, float z)
{
	D3DXVECTOR3 target(x,y,z);
	D3DXVECTOR3 L = target - m_vPosition ;
	D3DXVec3Normalize(&L, &L);

	D3DXVECTOR3 R;
	D3DXVec3Cross(&R, &D3DXVECTOR3(0,1,0), &L);
	D3DXVec3Normalize(&R, &R);

	D3DXVECTOR3 U;
	D3DXVec3Cross(&U, &L, &R);
	D3DXVec3Normalize(&U, &U);

	//m_vPosition   = pos;
	m_vRight = R;
	m_vUp    = U;
	m_vLookAt  = L;

	CalculateViewMatrix();
	
	//m_mViewProj = m_mViewMatrix * m_mProjMatrix;
}

void GameplayCam::SetLookAt(D3DXVECTOR3& target, D3DXVECTOR3& up)
{
	D3DXVECTOR3 L = target - m_vPosition;
	D3DXVec3Normalize(&L, &L);

	D3DXVECTOR3 R;
	D3DXVec3Cross(&R, &up, &L);
	D3DXVec3Normalize(&R, &R);

	D3DXVECTOR3 U;
	D3DXVec3Cross(&U, &L, &R);
	D3DXVec3Normalize(&U, &U);

	m_vRight = R;
	m_vUp    = U;
	m_vLookAt  = L;

	CalculateViewMatrix();
	
	m_mViewProj = m_mViewMatrix * m_mProjMatrix;
}

void GameplayCam::SetLookAt(Quaternion orientation)
{
	D3DXMATRIX O;
	D3DXMatrixRotationQuaternion(&O, &orientation);
	D3DXVec3TransformNormal(&m_vLookAt, &Vector3(0.0f, 0.0f, 1.0f), &O);
	D3DXVec3TransformNormal(&m_vUp, &Vector3(0.0f, 1.0f, 0.0f), &O);

	D3DXVECTOR3 R;
	D3DXVec3Cross(&R, &m_vUp, &m_vLookAt);
	D3DXVec3Normalize(&R, &R);
	m_vRight = R;

	CalculateViewMatrix();

	m_mViewProj = m_mViewMatrix * m_mProjMatrix;
}

void GameplayCam::SetSpeed(float s)
{
	m_fSpeed = s;
}

void GameplayCam::LockCamera(D3DXVECTOR3& target)
{
	
	D3DXVECTOR3 L = target - m_vPosition;
	D3DXVec3Normalize(&L, &L);

	D3DXVECTOR3 R;
	D3DXVec3Cross(&R, &D3DXVECTOR3(0,1,0), &L);
	D3DXVec3Normalize(&R, &R);

	D3DXVECTOR3 U;
	D3DXVec3Cross(&U, &L, &R);
	D3DXVec3Normalize(&U, &U);

	m_vRight = R;
	m_vUp    = U;
	m_vLookAt  = L;

	CalculateViewMatrix();
	
	m_mViewProj = m_mViewMatrix * m_mProjMatrix;
}

void GameplayCam::SetPos(D3DXVECTOR3& pos)
{
	m_vPosition = pos;


}
void GameplayCam::SetPosition(float x, float y, float z)
{
	m_vPosition = D3DXVECTOR3(x,y,z);
}

void GameplayCam::SetMainCore(MainCore* &pMain)
{
	m_pMainCore = pMain;

	// Give graphics my viewmatrix ptr data
	m_pMainCore->GetGraphicsCore()->SetCameraView((void*)(&m_mViewMatrix));
	m_pMainCore->GetGraphicsCore()->SetCameraPos((void*)(&m_vPosition));
}