#include "CameraControl.h"


CameraControl::CameraControl(void)
{
	// initialize id to -1 meaning it hasn't been added to the camera
	//	manager yet. This is bad, fix this.
	m_ID = -1;
}


CameraControl::~CameraControl(void)
{
}

void CameraControl::Arrive(Vector3 vSeekPoint)
{
	//arrive algorithm from AI

	//dist to target position
	Vector3 toTarget;

	toTarget = vSeekPoint - m_vPosition;

	//find dist to target
	float dist = toTarget.Magnitude();

	//only calc if we're far away from target
	if (dist > 1)
	{
		const double decelerationTweaker = 1;

		double speed = dist / ((double)10 * decelerationTweaker);

		//make sure we dont' go too fast
		if (speed > m_fMaxSpeed)
			speed = m_fMaxSpeed;
		
		//calc desired velocity
		toTarget.Normalize();
		Vector3 desiredVelocity = toTarget.ScaleReturn(speed);

		//assign velocity
		m_vSeekForce = desiredVelocity - m_vVelocity;
	}
	else
	{
		m_vSeekForce.Clear();
		m_vVelocity.Clear();
	}
}
