#pragma once
#include <DCMathLib.h>
#include "CameraControl.h"

/*
	Gameplay Camera Class:

	I need significant and ready controls for the camera.
	Since now the cam is just being updated by passing in the
	view matrix, I need to be able to create,delete and rig
	cameras as needed. 
*/



// forward declarations
class MainCore;
class IInputCore;

class GameplayCam : public CameraControl
{
private:
	
	

	//D3DXVECTOR3 m_vPosition;	// CAM'S POSITION
	D3DXVECTOR3 m_vOrigin;		// Where Cam rotates around
	float		m_fSpeed;		// Cam's speed
	CameraMode	m_eMode;		// camera mode - free or locked onto a target
	D3DXVECTOR3 m_vLockTarget;	// camera's target while in locked mode
	CameraType	m_eCamType;		// type of camera - chase, free, etc
	MainCore*	m_pMainCore;	// Link to input core for button checking - will move to cam mgr

	//////////////////
	// CAMERA RIGGING
	//////////////////
	Vector3 m_vOffset;
	
	
		
	/////////////////
	// LOCAL NORMALS
	////////////////

	D3DXVECTOR3 m_vLookAt;
	D3DXVECTOR3 m_vRight;
	D3DXVECTOR3 m_vUp;


	D3DXQUATERNION m_qOrientation;

	/////////////
	//	MATRICES
	/////////////
	D3DXMATRIX m_mViewProj;		//view and projection matrix already multiplied
	D3DXMATRIX m_mViewMatrix;
	D3DXMATRIX m_mProjMatrix;
	D3DXMATRIX m_mParentTransform;	// parent's transform matrix


	////////////
	//	VIEWS
	////////////
	
public:
	
	
	GameplayCam();
	GameplayCam(D3DXVECTOR3 pos, D3DXVECTOR3 look, D3DXVECTOR3 up);
	GameplayCam(float xPos, float yPos, float zPos, float xLook, float yLook, float zLook, 
		float xUp = 0.0f, float yUp = 1.0f, float zUp = 0.0f);
	~GameplayCam();

	void Update(float dt);
	void HandleInput(IInputCore* inputState, float dt);
	void CalculateViewMatrix();
	void OnLostDevice();	
	void OnResetDevice();
	

	///////////////////
	// ACCESSORS
	//////////////////
	inline const D3DXMATRIX& GetView() const {return m_mViewMatrix;}
	inline const D3DXVECTOR3& GetRight() const;
	inline const D3DXVECTOR3& GetUp() const;
	inline const D3DXVECTOR3& GetLookAt() const;
	inline D3DXVECTOR3& GetPosition();
	const Vector3& GetOffset() const {return m_vOffset;}
	

	
	/////////////////
	//	MUTATORS
	////////////////
	void SetLookAt(D3DXVECTOR3& pos, D3DXVECTOR3& target, D3DXVECTOR3& up);
	void SetLookAt(float x, float y, float z);
	void SetLookAt(D3DXVECTOR3& target, D3DXVECTOR3& up);
	void SetLookAt(Quaternion orientation);
	void SetSpeed(float s);
	void LockCamera(D3DXVECTOR3& target);
	void SetPos(D3DXVECTOR3& pos);
	void SetPosition(float x, float y, float z);
	void SetCamType(CameraType type){m_eCamType = type;}
	void SetTarget(D3DXVECTOR3 target){m_vLockTarget = target;}
	void SetCamMode(CameraMode mode){m_eMode = mode;}
	void SetMainCore(MainCore* &pMain);
	void SetView(D3DXMATRIX mView) {m_mViewMatrix = mView;}
	void SetOffset(Vector3 vOffset){m_vOffset = vOffset;}
	void SetParentsTransform(D3DXMATRIX mTransform){m_mParentTransform = mTransform;}
	



};

