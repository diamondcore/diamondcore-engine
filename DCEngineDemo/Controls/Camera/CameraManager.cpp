#include "CameraManager.h"
#include "GameplayCam.h"
#include "CameraRig.h"
#include "../../UI/ScreenManager.h"
#include <dinput.h>
#include "../../DebugLogger.h"


CameraManager::CameraManager(void)
{
	m_iNextID = 0;
}


CameraManager::~CameraManager(void)
{
}

CameraManager* CameraManager::Instance()
{
	static CameraManager instance;

	return &instance;
}

//////////////////
// UTILITIES
/////////////////
void CameraManager::Update(float dt)
{
	
	
	m_pCurrentCamControl->Update(dt);
	

}

int CameraManager::CreateCamera(CameraType eType)
{
	switch(eType)
	{
	case FREECAM:
		{
			// create a new camera
			GameplayCam *temp = new GameplayCam();
			
			// set its id
			temp->SetID(GetNextID());

			// set its type
			temp->SetType(FREECAM);

			temp->SetLookAt(Vector3(0,0,0),Vector3(0,1,0) );

			temp->SetSpeed(100.0f);

			// if there's no current cam view, set it to temp
			if(m_pCurrentCamControl == NULL)
			{
				m_pCurrentCamControl = temp;
				ScreenMan->GetGraphics()->SetCameraView((void*)(&m_pCurrentCamControl->GetView()) );
				ScreenMan->GetGraphics()->SetCameraPos((void*)(&m_pCurrentCamControl->GetPosition()) );
			}
			
			// push temp cam onto cam controls vec

			m_vCamControls.push_back(temp);

			// increment number of cam controls
			m_iNumCamControls ++;

			// return the id of the created cam control
			return temp->GetID();
						
			break;
		}
	case RIG:
		{
			// create new rig
			CameraRig* temp = new CameraRig();

			// set its id
			temp->SetID(GetNextID());

			// set its type
			temp->SetType(RIG);

			if(m_pCurrentCamControl == NULL)
			{
				m_pCurrentCamControl = temp;
				ScreenMan->GetGraphics()->SetCameraView((void*)(&m_pCurrentCamControl->GetView()) );
				ScreenMan->GetGraphics()->SetCameraPos((void*)(&m_pCurrentCamControl->GetPosition()) );
			}
			// push temp cam onto cam controls vec

			m_vCamControls.push_back(temp);

			// increment number of cam controls
			m_iNumCamControls ++;

			// return id of created cam control
			return temp->GetID();
			
			break;
		}
	};

	
	
	
	
	

}

void CameraManager::NextCameraView()
{
	// Search for id relative to the camera vector
	for(unsigned int n = 0; n < m_vCamControls.size(); n++)
		if(m_vCamControls[n]->GetID() == m_pCurrentCamControl->GetID())
		{
			i = n;
			break;
		}

		// increment i
		++i;

		// if i is greater than numcontrols-1, change to 0
		if (i > (m_iNumCamControls - 1) )
			m_pCurrentCamControl = m_vCamControls[0];
		else
			m_pCurrentCamControl = m_vCamControls[i];

		// give gfx new view mantrix
		ScreenMan->GetGraphics()->SetCameraView((void*)(&m_pCurrentCamControl->GetView()) );
		ScreenMan->GetGraphics()->SetCameraPos((void*)(&m_pCurrentCamControl->GetPosition()) );
	
}

void CameraManager::AddCameraToRig(float x, float y, float z, Vector3 vLookat)
{
	// make sure current control is a RIG camera
	if (m_pCurrentCamControl->GetType() != RIG)
		return;

	// cast control to camera rig
	pRigScratch = (CameraRig*) m_pCurrentCamControl;

	// add the camera
	pRigScratch->AddCamera(x,y,z,vLookat);

}

void CameraManager::AttachRigToEntity(BaseGameEntity* pEntity)
{
	// make sure current control is a RIG camera
	if (m_pCurrentCamControl->GetType() != RIG)
		return;

	// cast control to camera rig
	pRigScratch = (CameraRig*) m_pCurrentCamControl;

	// attach to entity
	pRigScratch->AttachRig(pEntity);
}

void CameraManager::HandleInput(IInputCore* inputState, float dt)
{

	// check to see if need to switch cams
	if (inputState->IsNewKeyRelease(DIK_C))
		NextCameraView();

	m_pCurrentCamControl->HandleInput(inputState, dt);
	
}

void CameraManager::DeleteCameraControl(int id)
{
	// find the cam
	for (j = m_vCamControls.size() -1; j >= 0; --j)
	{
		if (m_vCamControls[j]->GetID() == id)
		{
			// if found, swap to next view
			if(id == m_pCurrentCamControl->GetID())
				NextCameraView();
			
			// erase the object
			m_vCamControls.erase(m_vCamControls.begin() + j);

			// decrement the number of controls
			--m_iNumCamControls;

			// return
			return;
		}
	}

	// didn't find the cam, nothing happens
}

void CameraManager::SetActiveCamControl(int id)
{
	bool found = false;
	for (k = m_vCamControls.size()-1; k >= 0; --k)
	{
		if (m_vCamControls[k]->GetID() == id)
		{
			found = true;
			m_pCurrentCamControl = m_vCamControls[k];
		
			// give gfx new view mantrix
			DbgLog->Write("Setting Camera View.");
			ScreenMan->GetGraphics()->SetCameraView((void*)(&m_pCurrentCamControl->GetView()) );
			DbgLog->Write("Setting Camera Position.");
			ScreenMan->GetGraphics()->SetCameraPos((void*)(&m_pCurrentCamControl->GetPosition()) );
		}
	}
	if(!found)
		DbgLog->Write("Could not find camera controler.");
}

void CameraManager::DeleteAllCamControls()
{
	for (i = m_vCamControls.size()-1; i >= 0; --i)
	{
		// free up memory
		delete m_vCamControls[i];
		// delete slot
		m_vCamControls.erase(m_vCamControls.begin() + i);
	}

	// set num of cam controls to 0
	m_iNumCamControls = 0;
}

///////////////////
// ACCESSORS
//////////////////
// retrieves the requested camera control by its id.
CameraControl* CameraManager::GetCameraControlByID(int id)
{
	for (k = m_vCamControls.size()-1; k >= 0; --k)
	{
		if (m_vCamControls[k]->GetID() == id)
			return m_vCamControls[k];
	}

	// id not found
	return NULL;
}
Vector3 CameraManager::GetPosition() 
{
	return m_pCurrentCamControl->GetPosition();
}

/////////////////
//	MUTATORS
////////////////
void CameraManager::SetPosition(float x, float y, float z)
{
	m_pCurrentCamControl->SetPosition(x,y,z);
}