#pragma once
#include <vector>
#include "GameplayCam.h"
#include "CameraControl.h"

/*
	Controls all camera views, updates, etc. Can create rigs or 
	other cameras. 

*/

#define CamMgr CameraManager::Instance()

class CameraRig;
class BaseGameEntity;

class CameraManager
{
private:
	// vec of cameras
	std::vector<CameraControl*> m_vCamControls;
	CameraManager(void);

	// id system
	int m_iNextID;

	int GetNextID(){return m_iNextID++;}

	// number of cameras
	int m_iNumCamControls;

	/////////////////
	// SCRATCH VARS
	////////////////
	int i,j,k;
	Vector3 vScratch;
	D3DXMATRIX mScratch;
	CameraRig* pRigScratch;

	
public:

	// public cam views for easy access
	// ptr to current cam view
	CameraControl* m_pCurrentCamControl;

	// instance method
	static CameraManager* Instance();

	~CameraManager(void);

	//////////////////
	// UTILITIES
	/////////////////
	void Update(float dt);
	int CreateCamera(CameraType eType);
	void NextCameraView();
	void AddCameraToRig(float x, float y, float z, Vector3 vLookat);
	void AttachRigToEntity(BaseGameEntity* pEntity);
	void HandleInput(IInputCore* inputState, float dt);
	void DeleteCameraControl(int id);
	void SetActiveCamControl(int id);
	// used to keep the cams clean
	void DeleteAllCamControls();

	///////////////////
	// ACCESSORS
	//////////////////
	// retrieves the requested camera control by its id.
	CameraControl* GetCameraControlByID(int id);
	Vector3 GetPosition();

	/////////////////
	//	MUTATORS
	////////////////
	void SetPosition(float x, float y, float z);

};

