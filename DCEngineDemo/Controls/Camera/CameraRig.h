#pragma once
#include <DCMathLib.h>
#include "CameraControl.h"


/*
	Camera Rig - This is a special aggregation of cameras that 
	move multiple cameras as a unit, similar to how a camera rig
	in a movie works. It has its own position, and you "attach"
	cameras onto it, and the camera gets its final position
	by multiplying its transform by its parents, or the rig's.

	Very Beneficial as you can attach the rig to an object and it
	will take care of all the updates, and you can cycle through
	the cameras by registering a buttonpress.
*/

// forward declarations
class GameplayCam;
class BaseGameEntity;

class CameraRig : public CameraControl
{
private:

	// rig's position is world coordinates
	/*Vector3 m_vPosition;*/

	// rig's previous position
	Vector3 m_vLastPos;

	// rig's next position
	Vector3 m_vNextPos;

	// vec of previous positions
	std::vector<Vector3*> m_vPreviousPositions;

	// rig's velocity (in case its in chase mode)
	Vector3 m_vVelocity;

	// rig's orientation 
	Quaternion m_qOrientation;

	// transform matrix
	Matrix4 m_mTransform;

	// number of camera's attached
	int m_iNumCams;

	// flags for loose camera rig
	bool m_bLooseRig;

	// vector of cameras, used for accessing and updating
	std::vector<GameplayCam*> m_vCams;

	// current cam view
	GameplayCam* m_pCurrentCamView ;

	// cam id system
	int m_iNextID;

	// is the cam system attached to something?
	bool m_bAttached;

	// the entity is camera is attached to
	BaseGameEntity* m_pAttachedTo;

	/////////////////
	// SCRATCH VARS
	////////////////
	int i,j,k;
	Vector3 vScratch, vScratch1, vScratch2, vScratch3;
	D3DXMATRIX mScratch;

public:
	CameraRig(void);
	~CameraRig(void);

	/////////////////
	// UTILITIES
	/////////////////
	// updates the current cam view
	void Update(float dt);

	// adds a camera to the rig, position is in local coordinates
	void AddCamera(float x, float y, float z, Vector3 vLookat = Vector3(0,0,0) );
	// cycles to the next camera on the rig
	void NextCamView();
	// attaches rig to an object
	void AttachRig(BaseGameEntity* pAttachTo);
	
	// overwrite virtual handleinput
	virtual void HandleInput(IInputCore* inputState, float dt);

	// moving average for cam movement
	Vector3 CalculateNewPosition(Vector3* vNextPosition, int iNumPositionsToKeep);

	// linear interpolation
	Vector3 LerpMovement(Vector3 vTarget, float dt, float fLowSpeed, float fHighSpeed);

	/////////////////
	// ACCESSORS
	/////////////////
	D3DXVECTOR3& GetTargetPosition() {return m_vPosition;}
	D3DXVECTOR3& GetPosition();
	Vector3 GetVelocity() {return m_vVelocity;}
	Quaternion GetOrientation() {return m_qOrientation;}
	GameplayCam* GetCurrentCamView() {return m_pCurrentCamView;}
	const D3DXMATRIX& GetView() const;
	virtual const D3DXVECTOR3& GetLookAt() const;
	virtual const D3DXVECTOR3& GetUp() const;

	/////////////////
	// MUTATORS
	/////////////////
	void SetPosition(float x, float y, float z);
	void SetPosition(Vector3 vPos);
	void SetVelocity(float x, float y, float z);
	void SetVelocity(Vector3 vVel);
	void SetOrientation(Quaternion qOrientation);
	void SetTransform(D3DXMATRIX mTransform);
	virtual void SetLookAt(D3DXVECTOR3& target, D3DXVECTOR3& up);
	void SetLooseRig(bool enable){m_bLooseRig = enable;}
	
};

