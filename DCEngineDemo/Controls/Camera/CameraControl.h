#pragma once
#include <DCMathLib.h>

/*
	Base Camera control class - pure virtual

	This is to allow control to be passed from camera systems
	with ease and cleanliness. 
*/

enum CameraType 
{
	FREECAM, RIG, RIGCAM
};

enum CameraMode
{
	FREE,
	LOCKED,
	RIGGED
};

// forward declarations
class IInputCore;

class CameraControl
{
protected:

	
	// control's position in world coords
	Vector3 m_vPosition;

	// control's velocity
	Vector3 m_vVelocity;

	// force set by the seek algorithm
	Vector3 m_vSeekForce;

	// id for control allowing for easy identification
	int m_ID;

	CameraType m_eType;

	float m_fMaxSpeed;
	

public:
	CameraControl(void);
	virtual ~CameraControl(void);

	//////////////
	// UTILITIES
	//////////////
	virtual void Update(float dt) = 0;
	virtual const D3DXMATRIX& GetView() const = 0;
	virtual void HandleInput(IInputCore* inputState, float dt) = 0;
	void Arrive(Vector3 vSeekPoint);

	//////////////
	// ACCESSORS
	//////////////
	virtual D3DXVECTOR3& GetPosition() = 0;
	virtual const D3DXVECTOR3& GetLookAt() const = 0;
	virtual const D3DXVECTOR3& GetUp() const = 0;
	int GetID(){
		return m_ID;
	}
	CameraType GetType(){
		return m_eType;
	}
	
	

	//////////////
	// MUTATORS
	//////////////
	void SetType(CameraType eType){m_eType = eType;}
	void SetID(int ID){m_ID = ID;}
	void SetMaxSpeed(float fSpeed){m_fMaxSpeed = fSpeed;}
	virtual void SetLookAt(D3DXVECTOR3& target, D3DXVECTOR3& up) = 0;
	virtual void SetPosition(float x, float y, float z) = 0;

};

