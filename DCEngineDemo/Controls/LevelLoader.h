#pragma once
#include <fstream>
/*
	Level loader: Lives in Worldmanager. Loads all static and dynamic assets 
	as well as controls. Reads in a file, probably .bin, and loads the 
	appropriate items for the level. 
	TODO: build with the idea of having the loader run in its own thread.
*/

class LevelLoader
{
public:
	LevelLoader(void);
	~LevelLoader(void);
};

