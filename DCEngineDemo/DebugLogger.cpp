#include "DebugLogger.h"
#include <string>


DebugLogger::DebugLogger(void)
{
}


DebugLogger::~DebugLogger(void)
{
}

void DebugLogger::Write(std::string message)
{
#if USEDEBUGLOG
	m_sLogFile.open("DebugLog.txt", std::ios::out | std::ios::app);
	m_sLogFile << message + '\n';
	m_sLogFile.close();
#endif
}
