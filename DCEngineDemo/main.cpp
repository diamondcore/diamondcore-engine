#include <DiamondCoreInclude.h>

#include <iostream>
#include <vector>

#include "WorldManager.h"
#include "UI\ScreenManager.h"
#include "UI\Screens\MainMenuScreen.h"

MainCore * pMain;

void initializeCores(HINSTANCE hInstance)
{	
	OutputDebugString("Creating Main Core.");
	pMain = new MainCore(hInstance);
	
	OutputDebugString(" Requesting to Create AI Core from Main.");
	pMain->LoadCoreDLL("AICore.dll", CT_AI);

	OutputDebugString(" Requesting to Create Graphics Core from Main.");
	pMain->LoadCoreDLL("Graphics Core.dll", CT_GRAPHICS);

	OutputDebugString(" Requesting to Create Sound Core from Main.");
	pMain->LoadCoreDLL("Sound.dll", CT_SOUND);

	OutputDebugString(" Requesting to Create Input Core from Main.");
	pMain->LoadCoreDLL("InputCore.dll", CT_INPUT);
}


int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nShowCmd)
{
	initializeCores(hInstance);
	srand(time(NULL));

	__int64 ticksPerSec = 0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&ticksPerSec);
	float secsPerTick = 1.0f / (float)ticksPerSec;

	__int64 prevStamp = 0;
	__int64 currTimeStamp = 0;

	QueryPerformanceCounter((LARGE_INTEGER*)&currTimeStamp);
	prevStamp = currTimeStamp;

	ScreenMan->Initialize(pMain);

	ScreenMan->AddScreen(new MainMenuScreen());
	
	//Begin the application by adding our first screen.

	MSG msg;
	msg.message = WM_NULL;
	Vector3 one = Vector3(1, 1, 1);

	auto tickDuration = 1 / 60.0f;
	float tickAmount = 0.0f;
	while(msg.message != WM_QUIT)
	{
        while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
			DispatchMessage(&msg);
        }

		QueryPerformanceCounter((LARGE_INTEGER*)&currTimeStamp);
		float dt = float(currTimeStamp - prevStamp)*secsPerTick;

		tickAmount += max(dt, 0.0f);
		if (tickAmount >= tickDuration)
		{
			ScreenMan->Update(tickAmount);
			ScreenMan->Draw();

			tickAmount = 0.0f;
		}
		else
			Sleep((tickAmount - tickDuration)*0.2f); // Free up the core for a bit

		prevStamp = currTimeStamp;
	}

	ScreenMan->ShutDown();
	//pMain->GetGraphicsCore()->ClearScene();
	pMain->GetGraphicsCore()->Shutdown();
	pMain->Shutdown();

	return msg.message;
}