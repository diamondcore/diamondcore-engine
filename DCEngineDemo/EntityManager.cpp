#include "EntityManager.h"
#include "Game Objects/LightEntity.h"
#include "Game Objects/PhysicsEntity.h"
#include "Game Objects/TriggerEntity.h"
#include "Game Objects/CameraEntity.h"
#include "Game Objects/Turret.h"
#include "UI\Screens\MessageBox.h"
#include "Game Objects\Terrain.h"
#include "Game Objects\SmokeTrap.h"
#include "Game Objects\DestructableObjects.h"
#include "UI\Screens\LevelDesignerScreen.h"
#include "UI\ScreenManager.h"
#include "Game Objects\LightningTrap.h"
#include "Game Objects\Smasher.h"
#include "UI\Screens\StatsScreen.h"
#include <fstream>
#include <shlobj.h>

EntityManager::EntityManager(MainCore* pMainCore)
	:m_pMainCore(pMainCore)
{
	m_bDisplayHiddenObj	= 0;
	m_pDefaultCamera	= 0;

	// Set directory to the user's personal folder (My Documents)
	char buf[MAX_PATH] = {0};
    HRESULT result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, 
									 SHGFP_TYPE_CURRENT, buf);
	if( !FAILED(result) )
	{
		m_sLevelsDirectory  = buf;
		m_sLevelsDirectory += "\\Turbine Levels\\";
	}
	else // Fall back to old relative path
		m_sLevelsDirectory = "Content/Levels/";
}
EntityManager::~EntityManager()
{
}

void EntityManager::Shutdown()
{
	for(auto i = m_EntityList.begin();
		i != m_EntityList.end(); i++)
		(*i).second->Shutdown();
	
	for(auto i = m_NonUpdateEntityList.begin();
		i != m_NonUpdateEntityList.end(); i++)
		(*i).second->Shutdown();
	ClearEntityList();
}
void EntityManager::ClearEntityList()
{
	m_pDefaultCamera = 0;
	m_EntityList.clear();
	m_NonUpdateEntityList.clear();
	m_SpawnPointList.clear();
}

void EntityManager::InitHavokTerrain()
{
	//Demo Getting verts
	Vector3* vertList = new Vector3[ScreenMan->GetGraphics()->GetNumVerts()];
	ScreenMan->GetGraphics()->GetTerrainVerts(vertList);

	// Demo getting indices
	WORD* indexList = new WORD[ScreenMan->GetGraphics()->GetNumIndices()];
	ScreenMan->GetGraphics()->GetTerrainIndices(indexList);
	
	Vector3 size;
	ScreenMan->GetGraphics()->GetTerrainSize(size);
	int rows, cols;
	ScreenMan->GetGraphics()->GetTerrainNumRowsCols(rows, cols);

	m_pTerrain = new Terrain(vertList, indexList, Vector2(rows, cols), Vector2(size.z, size.x));

	delete [] vertList;	// Clean up demo
	delete [] indexList;	// Clean up demo
}

void EntityManager::Update(float dt)
{
	for(auto i = m_EntityList.begin();
		i != m_EntityList.end(); i++)
		(*i).second->Update(dt);
}

bool EntityManager::LoadLevel(char* levelName)
{
	// Clear the previous scene
	Shutdown();

	// Get the directory to the level .xml file
	std::string rename = m_sLevelsDirectory;
	rename += levelName;	
	rename += "/";
	rename += levelName;
	rename += ".xml";
	TiXmlDocument doc( rename.c_str() );
	if( doc.LoadFile() != 0 )	// Check if file exists
	{
		// Load stats file
		SStats->ReadStatsFromFile(rename.substr(0, rename.find('.')) + "Stats.bin");

		// Load in the Terrain
		m_pMainCore->GetGraphicsCore()->LoadTerrain(&m_sLevelsDirectory[0], levelName);
		
		// Load in the objects
		TiXmlHandle hDoc(&doc);
		TiXmlElement* pElem;
		TiXmlHandle	hRoot(0);
		pElem = hDoc.FirstChildElement().ToElement();
		hRoot = TiXmlHandle(pElem);
		
		// Load Object
		for(TiXmlElement* node = hRoot.ToElement()->FirstChildElement("Object");	// Iterate through the nodes
			node; node = node->NextSiblingElement("Object"))
		{
			// Get the object type
			int eType;
			node->QueryIntAttribute("Type", &eType);
			
			// Create a instance of that type of object.
			// Then call it's loader.
			switch(eType)
			{
			case ET_UNASSIGNED:
				{
					break;
				}
			case ET_LEVELOBJECT:
				{
					// Example:  This is a sudo example of how to load a wold object
					std::shared_ptr<BaseGameEntity> temp = std::shared_ptr<BaseGameEntity>(new BaseGameEntity(ET_LEVELOBJECT));
					temp->Load( node );
					temp->Init();
					temp->Update(0.0f);		// Apply any changes in position
					AddEntity(temp->GetID(), temp);		// Add to world entity list
					
					// Add entity to current Level Designer counters
					if(m_bDisplayHiddenObj)
						((LevelDesignerScreen*)(ScreenMan->GetCurrentScreen()))->IncrementObjectList( temp );
					break;
				}
			case ET_TRIGGERVOLUME:
				{
                    int type;
                    node->QueryIntAttribute("t_Type", &type);
                    std::shared_ptr<TriggerEntity> temp = std::shared_ptr<TriggerEntity>(new TriggerEntity(ET_TRIGGERVOLUME,(TriggerType)type));
					temp->Load( node );
					//temp->Init();
					//temp->Update(0.0f);		// Apply any changes in position
					AddEntity(temp->GetID(), temp);		// Add to world entity list
					
					// Add entity to current Level Designer counters
					if(m_bDisplayHiddenObj)
						((LevelDesignerScreen*)(ScreenMan->GetCurrentScreen()))->IncrementObjectList( temp );
					break;
				}
			case ET_RACER:
				{
					break;
				}
			case ET_AIRACER:
				{
					break;
				}
			case ET_ENGINE:
				{
					break;
				}
			case ET_POD:
				{
					break;
				}
			case ET_LIGHT:
				{
					std::shared_ptr<LightEntity> temp = std::shared_ptr<LightEntity>(new LightEntity);
					temp->Load( node );
					temp->Init();
					temp->Update(0.0f);		// Apply any changes in position
					AddEntity(temp->GetID(), temp);		// Add to world entity list
					
					// Add entity to current Level Designer counters
					if(m_bDisplayHiddenObj)
						((LevelDesignerScreen*)(ScreenMan->GetCurrentScreen()))->IncrementObjectList( temp );
					break;
				}
			case ET_CAMERA:
				{
					std::shared_ptr<CameraEntity> temp = std::shared_ptr<CameraEntity>(new CameraEntity);
					temp->Load( node );
					temp->Init();
					temp->Update(0.0f);		// Apply any changes in position
					AddEntity(temp->GetID(), temp);		// Add to world entity list

					// Setup the default level camera for this level
					m_pDefaultCamera = temp;
					
					// Add entity to current Level Designer counters
					if(m_bDisplayHiddenObj)
						((LevelDesignerScreen*)(ScreenMan->GetCurrentScreen()))->IncrementObjectList( temp );
					break;
				}
			case ET_PHYSICSOBJECT:
				{
					std::shared_ptr<PhysicsEntity> temp = std::shared_ptr<PhysicsEntity>(new PhysicsEntity(ET_PHYSICSOBJECT));
					temp->Load( node );
					//temp->Init();
					//temp->Update(0.0f);		// Apply any changes in position
					AddEntity(temp->GetID(), temp);		// Add to world entity list
					
					// Add entity to current Level Designer counters
					if(m_bDisplayHiddenObj)
						((LevelDesignerScreen*)(ScreenMan->GetCurrentScreen()))->IncrementObjectList( temp );
					break;
				}
			case ET_SPAWNPOINT:
				{
					std::shared_ptr<BaseGameEntity> temp = std::shared_ptr<BaseGameEntity>(new BaseGameEntity(ET_SPAWNPOINT));
					temp->Load( node );
					temp->Init();
					temp->Update(0.0f);		// Apply any changes in position
					AddEntity(temp->GetID(), temp);		// Add to world entity list
					
					// Add entity to current Level Designer counters
					if(m_bDisplayHiddenObj)
						((LevelDesignerScreen*)(ScreenMan->GetCurrentScreen()))->IncrementObjectList( temp );
					break;
				}
			case ET_TURRET:
				{
					std::shared_ptr<Turret> temp = std::shared_ptr<Turret>(new Turret());
					temp->Load( node );
					temp->Init();
					temp->Update(0.0f);		// Apply any changes in position
					AddEntity(temp->GetID(), temp);		// Add to world entity list
					
					// Add entity to current Level Designer counters
					if(m_bDisplayHiddenObj)
						((LevelDesignerScreen*)(ScreenMan->GetCurrentScreen()))->IncrementObjectList( temp );
					break;
				}
			case ET_SMOKETRAP:
				{
					std::shared_ptr<SmokeTrap> temp = std::shared_ptr<SmokeTrap>(new SmokeTrap());
					temp->Load( node );
					//temp->Init();
					//temp->Update(0.0f);		// Apply any changes in position
					AddEntity(temp->GetID(), temp);		// Add to world entity list
					
					// Add entity to current Level Designer counters
					if(m_bDisplayHiddenObj)
						((LevelDesignerScreen*)(ScreenMan->GetCurrentScreen()))->IncrementObjectList( temp );
					break;
				}
			case ET_DESTRUCTABLE:
				{
					std::shared_ptr<DestructableObjects> temp = std::shared_ptr<DestructableObjects>(new DestructableObjects());
					temp->Load( node );
					//temp->Init();
					//temp->Update(0.0f);		// Apply any changes in position
					AddEntity(temp->GetID(), temp);		// Add to world entity list
					
					// Add entity to current Level Designer counters
					if(m_bDisplayHiddenObj)
						((LevelDesignerScreen*)(ScreenMan->GetCurrentScreen()))->IncrementObjectList( temp );
					break;
				}
			case ET_LIGHTNINGTRAP:
				{
					std::shared_ptr<LightningTrap> temp = std::shared_ptr<LightningTrap>(new LightningTrap());
					
					temp->Load(node);
					temp->Init();
					temp->Update(0.0f);
					AddEntity(temp->GetID(), temp);
					if(m_bDisplayHiddenObj)
						((LevelDesignerScreen*)(ScreenMan->GetCurrentScreen()))->IncrementObjectList( temp );
					break;
				}
            case ET_Smasher:
				{
					std::shared_ptr<Smasher> temp = std::shared_ptr<Smasher>(new Smasher());
					temp->Load( node );
					//temp->Init();
					//temp->Update(0.0f);		// Apply any changes in position
					AddEntity(temp->GetID(), temp);		// Add to world entity list

					// Add entity to current Level Designer counters
					if(m_bDisplayHiddenObj)
						((LevelDesignerScreen*)(ScreenMan->GetCurrentScreen()))->IncrementObjectList( temp );
					break;
				}

			default:
				break;
			};
		}
		// Update the texture names in the LevelDesigner
		if(m_bDisplayHiddenObj)
			((LevelDesignerScreen*)(ScreenMan->GetCurrentScreen()))->UpdateLoadedTextures();
	}
	else
	{
		// Output error
		std::string msg = "Could not find the level \"";
		msg += levelName;
		msg += "\".";
		MessageBoxScreen::Show("Error", &msg[0]);
		return false;
	}
	return true;
}
void EntityManager::SaveLevel(char* levelName)
{
	// Save the Terrain 
	m_pMainCore->GetGraphicsCore()->SaveTerrain(&m_sLevelsDirectory[0], levelName);

	// Prepare the header
	TiXmlDocument	m_SaveMapData;
	TiXmlDeclaration* dec = new TiXmlDeclaration( "1.0", "", "" );
	m_SaveMapData.LinkEndChild(dec);

	// Setup the root node
	TiXmlElement* root = new TiXmlElement("World");
	m_SaveMapData.LinkEndChild(root);

	// Write comment to show where global map data is
/*	TiXmlComment* cmt = new TiXmlComment();
	cmt->SetValue("SAVE MAP DATA");
	root->LinkEndChild(cmt);
	
	// Save Map Data, like size
	char temp[256];
	TiXmlElement* length = new TiXmlElement("Length");
	sprintf(temp,"%f", 150.0f);		// Convert float to char*
	length->LinkEndChild(new TiXmlText(temp));
	root->LinkEndChild(length);

	TiXmlElement* width = new TiXmlElement("Width");
	sprintf(temp,"%f", 150.0f);		// Convert float to char*
	width->LinkEndChild(new TiXmlText(temp));
	root->LinkEndChild(width);*/

	// Write comment to show where building data begins
	TiXmlComment* cmt2 = new TiXmlComment();
	cmt2->SetValue("OBJECT DATA");
	root->LinkEndChild(cmt2);
		
	// Create Mesh Elements
	for(auto i = m_EntityList.begin(); i != m_EntityList.end(); i++)
	{
		// Create this object
		TiXmlElement* obj = new TiXmlElement("Object");
		obj->SetAttribute("Type", (*i).second->GetEntityType());
		root->LinkEndChild(obj);	// Link obj to its parent
		
		// Set the object paramaters
		(*i).second->Save( obj );	// Cycle through each object saving it
	}

	// Save the data
	// Set up directory and file name
	std::string rename = m_sLevelsDirectory;
	rename += levelName;
	rename += "/";
	rename += levelName;
	rename += ".xml";
	m_SaveMapData.SaveFile(rename.c_str());

	// Add level to the Gameplay list of levels
	std::fstream file;
	std::string file_path = m_sLevelsDirectory;
	file_path += "LevelList.list";
	file.open(file_path.c_str(), fstream::in | fstream::out | fstream::app);	// Append to the end of the file
	if(file.is_open())
	{
		bool found = false;
		std::string line;
		// Check if level is already in the list
		while(!file.eof())
		{
			getline(file, line);
			if(line.find(levelName, 0) != string::npos)
				found = true;
		}
		// If level was not found then add it to the end of the list
		if(!found)
		{
			file.clear();			// Reset the eofbit
			file.seekp(file.beg);	// Set the current stream position to the begining of the file
			file << levelName << std::endl;	// Append the new level name to the end of the file
		}
		file.close();
	}

	// Let user know that the level was saved
	MessageBoxScreen::Show("Successful", "Your map was saved successfully.");
}

void EntityManager::InitializeGameObjects(Vector3* playerPos)
{
	// Used to initialize physics objects
	for(auto i = m_EntityList.begin();
		i != m_EntityList.end(); i++)
		if((*i).second->GetEntityType() == ET_PHYSICSOBJECT || 
			(*i).second->GetEntityType() == ET_TRIGGERVOLUME ||
			(*i).second->GetEntityType() == ET_DESTRUCTABLE ||
			(*i).second->GetEntityType() == ET_SMOKETRAP ||
            (*i).second->GetEntityType() == ET_Smasher)
		{
			(*i).second->Init();
			(*i).second->Update(0.0f);
		}
		else if((*i).second->GetEntityType() == ET_TURRET)
		{
			// Set the target for all the AI turrets
			((Turret*)((*i).second).get())->SetTarget(playerPos);
		}
}

// Accessors to the list of world entities
void EntityManager::AddEntity(int id, std::shared_ptr<BaseGameEntity> pEntity)
{
	auto type = pEntity->GetEntityType();
	// Add entities into seperate list based on if they are updateable
	if(type == ET_CAMERA || type == ET_SPAWNPOINT)
	{
		if(type == ET_SPAWNPOINT)	// Store spawn points for later use
			m_SpawnPointList.push_back(pEntity);
		if(m_bDisplayHiddenObj)		// If it should be displayed then add it to an updating list
			m_EntityList.insert(std::pair<int, std::shared_ptr<BaseGameEntity>>(id, pEntity));
		else
			m_NonUpdateEntityList.insert(std::pair<int, std::shared_ptr<BaseGameEntity>>(id, pEntity));
	}
	else
		m_EntityList.insert(std::pair<int, std::shared_ptr<BaseGameEntity>>(id, pEntity));
}
void EntityManager::RemoveEntity(int id)
{
	auto iter = m_EntityList.find(id);
	if(iter != m_EntityList.end())
	{
		// Null the smart pointer
		iter->second->Shutdown();
		iter->second = 0;
		m_EntityList.erase( iter );	// Remove from the list
	}
	else
	{
		iter = m_NonUpdateEntityList.find(id);
		if(iter != m_NonUpdateEntityList.end())
		{
			// If it's the default camera, then null the pointer
			if(iter->second->GetID() == m_pDefaultCamera->GetID())
				m_pDefaultCamera = 0;

			// Null the smart pointer
			iter->second->Shutdown();
			iter->second = 0;
			m_NonUpdateEntityList.erase( iter );	// Remove from the list
		}
	}
}
std::shared_ptr<BaseGameEntity>	EntityManager::GetEntity(int id)
{
	auto iter = m_EntityList.find(id);
	if(iter != m_EntityList.end())
		return iter->second;
	else
	{
		iter = m_NonUpdateEntityList.find(id);
		if(iter != m_NonUpdateEntityList.end())
			return iter->second;
	}
	return 0;		// Entity id not found
}
std::shared_ptr<BaseGameEntity>	EntityManager::GetEntityByBodyID(int id)
{
	// Find the object
	for(auto i = m_EntityList.begin(); i != m_EntityList.end(); i++)
		if((*i).second->GetEntityType() == ET_PHYSICSOBJECT)
		{
			if( ((PhysicsEntity*)((*i).second.get()))->GetBody()->getUid() == id)
				return (*i).second;
		}
		else if((*i).second->GetEntityType() == ET_TRIGGERVOLUME)
		{
			if( ((TriggerEntity*)((*i).second.get()))->GetBody()->getUid() == id)
				return (*i).second;
		}
        else if((*i).second->GetEntityType() == ET_Smasher)
		{
            if( ((Smasher*)((*i).second.get()))->GetBody()->getUid() == id)
				return (*i).second;
		}
	return 0;
}

// Spawn point funcitons
void EntityManager::GetNextSpawnPoint(Vector3* pos, Quaternion* orientation)
{
	if(m_SpawnPointList.empty())	// Make sure there is spawn points in the list
		return;

	auto iter = m_SpawnPointList.begin();
	*pos = (*iter)->GetPosition();				// Get the next position
	*orientation = (*iter)->GetOrientation();	// Get the next orientation

	// Remove from avaliable spawn point list
	(*iter) = 0;
	m_SpawnPointList.erase(iter);
}
void EntityManager::ResetSpawnPoints()
{
	// Clear the spawn point list
	m_SpawnPointList.clear();

	// Recreate spawn point list
	for(auto i = m_NonUpdateEntityList.begin();
		i != m_NonUpdateEntityList.end(); i++)
		if( (*i).second->GetEntityType() == ET_SPAWNPOINT )
			m_SpawnPointList.push_back( (*i).second );	// Add spawn point to list
}

// Display hidden objects Flag
bool EntityManager::DisplayHiddenObjects()
{
	return m_bDisplayHiddenObj;
}
void EntityManager::SetDisplayHiddenObject(bool enable)
{
	m_bDisplayHiddenObj = enable;
}

// Get default camera
void EntityManager::GetDefaultCamera(Vector3& pos, Quaternion& orientation)
{
	if(m_pDefaultCamera)
	{
		// Use default camera
		pos = m_pDefaultCamera->GetPosition();
		orientation = m_pDefaultCamera->GetOrientation();
	}
	else if(m_bDisplayHiddenObj && !m_SpawnPointList.empty())
	{
		// If we are in level designer mode
		// Use raised spawn points
		pos = m_SpawnPointList[0]->GetPosition() + Vector3(0.0f, 3.0f, 0.0f);
		orientation = m_SpawnPointList[0]->GetOrientation();
	}
	else
	{
		// Use default position
		pos = Vector3(0.0f, 0.0f, 0.0f);
		orientation = Quaternion(1.0f, 0.0f, 0.0f, 0.0f);
	}
}

std::string EntityManager::GetLevelSelection()	
{
	// Only returns once, then it clears the selection
	std::string temp = m_sLevelSelection;
	m_sLevelSelection.clear();
	return temp;
}
