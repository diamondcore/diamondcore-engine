/*********************************************************************
						Shader Output Types
*********************************************************************/
struct VS_DSHADOW						// Vertex Shader						
{
	float4 posH		: POSITION0;
};


/*********************************************************************
						Vertex Shader Effects
*********************************************************************/	
VS_DSHADOW	DynamicShadowVS(float3 posL	: POSITION0)
{
	VS_DSHADOW outVS = (VS_DSHADOW)0;	// Zero out the output

	// Transform vertex position to world space
	float3 posW = mul(float4(posL, 1.0f), g_sWorld).xyz;
	//posW = g_sCameraPos - posW;		// Pi
	
	// Get Ray from the light to vert
	float3 V = normalize(posW-g_sLightPosW);	// For Single Light Only

	// Plane Normal
	float3 norm = float3(0.0f, 1.0f, 0.0f);

	// Point on the Plane
	float3 Po = float3(0.0f, 0.01f, 0.0f);
	
	// Scale of Light Ray
	float t = dot(norm, (Po-posW))/dot(norm,V);
	
	// Calculate the new point for the plane vert
	float3 P = posW + t*V;

	// Transform to homogeneous clip space
	outVS.posH = mul(float4(P, 1.0f), g_sWVP);
	
	return outVS;
}


/*********************************************************************
						Pixel Shader Effects
*********************************************************************/
float4	DynamicShadowPS()	: COLOR
{
	return float4(0.0f, 0.0f, 0.0f, 1.0f);
}