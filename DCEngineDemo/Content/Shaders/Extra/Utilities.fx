uniform extern float4x4 g_sWVP;				// WorldViewProj Matrix
uniform extern float4x4 g_sWorldInvTrans;	// WorldInverseTranspose Matrix
uniform extern float4x4 g_sWorld;			// World Matrix
uniform extern float4x4	g_sViewProj;		// View Projection Matrix
uniform extern float4x3 g_sViewInv;			// View inverse
uniform extern float3	g_sCameraPos;		// Eye Position (camera in world coordinates)

// Lighting
uniform	extern float3	g_sLightPosW;		// Light's position (world coordinates)
uniform extern float4	g_sAmbientLight;	// Ambient Light
uniform extern float4	g_sDiffuseLight;	// Diffuse Light
uniform extern float4	g_sSpecLight;		// Specular Light
uniform extern float3	g_sAttenuation;		// Attenuation012

// Materials
uniform extern float4	g_sAmbientMtrl;		// Ambient Material
uniform extern float4	g_sDiffuseMtrl;		// Diffuse Material
uniform extern float4	g_sSpecMtrl;		// Specular Material
uniform extern float	g_sSpecPower;		// Specular Power

uniform extern float2	g_sPixelSize;		// Size of the screen (1/width, 1/height)


// Useing float4 array allows for Color in the same array as the WorldMat
bool f_IsFog = true;
float startFog = 10;
float Shininess = 10.0f; // Higher the number, smoother the light surface 
float4 fogColor = {0.96, 0.96, 0.96, 1};
float PI = 3.1415926536f;
float focalPlane = 10.0f;
const float MAP_SIZE = 150.0f;	// Map max size is 150.0f 

// Textures
//uniform extern texture		g_sTexture;	
uniform extern texture2D	g_sDeferredMtrl;
uniform extern texture2D	g_sDeferredNormW;
uniform extern texture2D	g_sDeferredPosW;
uniform extern texture2D	g_sDeferredDepth;
uniform extern texture2D	g_sLightMap;
uniform extern texture2D	g_sSSAOMap;
uniform extern texture2D	g_sNormalTex;	// Used for input of normals by texture

// Texture Sample
//sampler	TextureS = sampler_state			
//{
	//Texture		= <g_sTexture>;		
	//AddressU	= Wrap;
	//AddressV	= Wrap;		
	//MinFilter	= Linear;			
	//MagFilter	= Linear;
	//MipFilter	= Linear;
//};

/*********************************************************************
						Deferred Shader Data
*********************************************************************/
// Material
sampler DeferredMtrl = sampler_state
{
	Texture		= <g_sDeferredMtrl>;
	MagFilter	= Point;
	MinFilter	= Point;
};

// World Normal
sampler DeferredNormW = sampler_state
{
	Texture		= <g_sDeferredNormW>;
	MagFilter	= Point;
	MinFilter	= Point;
};

// World Position (X, Y)
sampler DeferredPosW = sampler_state
{
	Texture		= <g_sDeferredPosW>;
	MagFilter	= Point;
	MinFilter	= Point;
};

// Depth
sampler DeferredDepth = sampler_state
{
	Texture		= <g_sDeferredDepth>;
	MagFilter	= Point;
	MinFilter	= Point;
};

sampler LightMap = sampler_state
{
	Texture		= <g_sLightMap>;
	MagFilter	= Point;
	MinFilter	= Point;
};

sampler SSAOMap = sampler_state
{
	Texture		= <g_sSSAOMap>;
	MagFilter	= Point;
	MinFilter	= Point;
};

// Post Processing
sampler PostProcess = sampler_state
{
	Texture		= <g_sDeferredMtrl>;
	MagFilter	= Linear;
	MinFilter	= Linear;
};

// Linear Depth
sampler LinearDepth = sampler_state
{
	Texture		= <g_sDeferredDepth>;
	MagFilter	= Linear;
	MinFilter	= Linear;
};

// Normal Texture Input
sampler NormalMap = sampler_state
{
	Texture		= <g_sNormalTex>;
	MagFilter	= Point;	//Linear;
	MinFilter	= Point;	//Linear;
};


/*********************************************************************
						Uber Shader Switches
*********************************************************************/
uniform extern bool	f_Phong;
uniform extern bool	f_BlinnPhong;
uniform extern bool f_IsTex;
uniform extern bool f_UseDOF;
uniform extern bool f_UseSSAO;
uniform extern bool f_UseNormalTex;	// If we should use g_sNormalTex to get the normal

struct PS_OUTPUT_DEF
{
	float4 mtrl		: COLOR0;
	float4 normW	: COLOR1;
	float4 posW		: COLOR2;	// (X,Y)
	float4 depth	: COLOR3;	// (Z)
};

