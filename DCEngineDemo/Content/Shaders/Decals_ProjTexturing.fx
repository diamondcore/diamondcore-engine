float4x4 g_mDecalMatrix;
//float4x4 g_mCamLookMatrix;
float4x4 g_mObjLookMatrix;
float3 g_vDecalDir;

bool m_DecalLock;

texture m_gDecalTexture;
sampler DecalSampler = sampler_state
{
	Texture = <m_gDecalTexture>;
#if 1
	AddressU = BORDER;
	AddressV = BORDER;
#else
	AddressU = Wrap;
	AddressV = Wrap;
#endif
};

struct VS_OUT
{
	float4 posW : POSITION;
	float4 diffuse : TEXCOORD0;
	float4 texCoordProj : TEXCOORD1;
	float norm : TEXCOORD2;
};

VS_OUT DecalVS(float4 pos : POSITION, float3 norm : NORMAL)
{
	VS_OUT OUT = (VS_OUT)0;
#if 1
	//float4x4 localLook = mul(g_mCamLookMatrix, g_mObjLookMatrix);
	OUT.posW = mul(mul(pos, g_mObjLookMatrix), g_sWVP);

	if(m_DecalLock)
		OUT.texCoordProj = mul(pos, g_mDecalMatrix);
	else
		OUT.texCoordProj = mul(mul(pos, g_sWorld), g_mDecalMatrix);
	OUT.norm = dot(norm, g_vDecalDir);
	OUT.diffuse = float4(0,0,0,0);
#else
	OUT.posW = mul(OUT.posW, g_sWVP);
	//OUT.posW += float4(0, 0, 1/OUT.posW.z/100, 0);
	//OUT.posW -= mul(OUT.posW, float4(0, 0, OUT.posW.z*1000, 0));
	//OUT.posW.zw -= 0.05f;
	if(m_DecalLock)
		OUT.texCoordProj = mul(pos, g_mDecalMatrix);
	else
		OUT.texCoordProj = mul(mul(pos, g_sWorld), g_mDecalMatrix);
	OUT.norm = dot(norm, g_vDecalDir);
	OUT.diffuse = float4(0,0,0,0);
#endif

	return OUT;
}

float4 DecalPS(VS_OUT IN) : COLOR
{
	float4 projColor;
#if 0
	
	projColor = tex2Dproj(DecalSampler, IN.texCoordProj);
	
#else	
	float2 texProjC;
	texProjC.x = IN.texCoordProj.x / IN.texCoordProj.w / 2.0f + 0.5f;
	texProjC.y = IN.texCoordProj.y / IN.texCoordProj.w / 2.0f + 0.5f;
	projColor = tex2D(DecalSampler, texProjC);
#endif
	projColor.w *= 2.0f;	
	//return lerp(IN.diffuse, projColor, saturate(IN.texCoordProj.w));
	return lerp(IN.diffuse, projColor, saturate(IN.norm));
	//return projColor;
}

float4 DecalPS_NoFalloff(VS_OUT IN) : COLOR
{
		float4 projColor;
#if 0
	
	projColor = tex2Dproj(DecalSampler, IN.texCoordProj);
	
#else	
	float2 texProjC;
	texProjC.x = IN.texCoordProj.x / IN.texCoordProj.w / 2.0f + 0.5f;
	texProjC.y = IN.texCoordProj.y / IN.texCoordProj.w / 2.0f + 0.5f;
	projColor = tex2D(DecalSampler, texProjC);
	if(IN.norm <= 0.0f)
		return IN.diffuse;
#endif
	projColor.w *= 2.0f;	
	//return lerp(IN.diffuse, projColor, saturate(IN.texCoordProj.w));
	return projColor;
	//return projColor;
}

technique DecalTech
{
	pass p1_NoFalloff
	{
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		ZwriteEnable = false;
		//ZEnable = false;
		VertexShader = compile vs_3_0 DecalVS();
		PixelShader = compile ps_3_0 DecalPS_NoFalloff();	
	}
	pass p0_FalloffBySurfaceNorm
	{
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		ZwriteEnable = false;
		ZFunc			= LessEqual;
		//ZEnable = false;
		VertexShader = compile vs_3_0 DecalVS();
		PixelShader = compile ps_3_0 DecalPS();	
	}
}