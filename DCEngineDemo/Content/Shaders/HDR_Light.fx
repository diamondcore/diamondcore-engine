uniform extern float4x4	g_sView;
uniform extern float4x4	g_sViewInv;
uniform extern float4x4	g_sProj;
uniform extern float4x4 g_sProjInv;
uniform extern float4x4 g_sViewProjInv;
uniform extern float4x4 g_sWVP;				// WorldViewProj Matrix
uniform extern float3	g_sCameraPos;		// Eye Position (camera in world coordinates)
uniform extern float3	g_sCameraPosInv;
uniform extern float2	g_sScreenSize;		// Width & Height

// Lighting
uniform extern int		g_sNum_Lights;			// Number of Lights
static const int		NUM_LIGHTS = 22;	//5;	// Set the maximum number of lights
//static const int		NUM_LIGHTS	= 2;			// Number of Lights
uniform extern	float4	g_sLightPos[NUM_LIGHTS];
uniform extern	float4	g_sLightIntensity[NUM_LIGHTS];
uniform extern	float	g_sPhongExp;
uniform extern	float	g_sPhongCoeff;
uniform extern	float	g_sDiffuseCoeff;	
uniform extern	float4	g_sEmissive;		// Emissive intensity of current light

// Tone Mapping
uniform extern	float	g_sMiddleGray;
//uniform extern	float	g_sWhiteCutOff;
uniform extern	float	g_sTimeElapsed;
uniform extern	float	g_sBloomScale;
uniform extern	float	g_sStarScale;

uniform extern	bool	g_sUseBlueShift;
uniform extern	bool	g_sUseToneMap;
uniform extern  bool    f_UseSSAO;

// Constants
static const int	MAX_SAMPLES			= 16;
static const float	BRIGHT_THRESHOLD	= 5.0f;
static const float	BRIGHT_OFFSET		= 10.0f;
static const float3	LUMINANCE			= float3(0.2125f, 0.7154f, 0.0721f);
static const float3	BLUE_SHIFT			= float3(1.05f, 0.97f, 1.27f);
float PI = 3.1415926536f;
#define ONE_OVER_PI 0.31831h
#define ONE_OVER_TWO_PI 0.159155h
float gamma = 1.0f; // Default = 2.2 (Average Range(2.0 - 2.4);
float FogStart = 950.0f;
float FogDensity = 0.033f; // Default = 0.33f;

uniform extern	float2	g_sSampleOffset[MAX_SAMPLES];
uniform extern	float4	g_sSampleWeight[MAX_SAMPLES];

//#include "DynamicShadows.fx"
uniform extern texture2D	g_sDeferredMtrl;
uniform extern texture2D	g_sDeferredNormW;
uniform extern texture2D	g_sDeferredPosW;
uniform extern texture2D	g_sDeferredDepth;
uniform extern texture2D	g_sSSAOMap;

sampler SSAOMap = sampler_state
{
	Texture		= <g_sSSAOMap>;
	MagFilter	= Point;
	MinFilter	= Point;
	AddressU	= Clamp;
	AddressV	= Clamp;
};

// Material
sampler DeferredMtrl = sampler_state
{
	Texture		= <g_sDeferredMtrl>;
	MagFilter	= Point;
	MinFilter	= Point;
	AddressU	= Clamp;
	AddressV	= Clamp;
};
// World Normal
sampler DeferredNormW = sampler_state
{
	Texture		= <g_sDeferredNormW>;
	MagFilter	= Point;
	MinFilter	= Point;
	AddressU	= Clamp;
	AddressV	= Clamp;
};

// World Position (X, Y)
sampler DeferredPosW = sampler_state
{
	Texture		= <g_sDeferredPosW>;
	MagFilter	= Point;
	MinFilter	= Point;
	AddressU	= Clamp;
	AddressV	= Clamp;
};

// Depth
sampler DeferredDepth = sampler_state
{
	Texture		= <g_sDeferredDepth>;
	MagFilter	= Point;
	MinFilter	= Point;
	AddressU	= Clamp;
	AddressV	= Clamp;
};

// Textures
uniform extern bool		g_sIsTex;	// Use textures?
uniform extern bool		g_sIsGrass;
uniform extern texture	g_sTexture;
sampler TextureS = sampler_state
{
	Texture		= <g_sTexture>;		
	AddressU	= Wrap;
	AddressV	= Wrap;		
	MinFilter	= Linear;			
	MagFilter	= Linear;
	MipFilter	= Linear;
};
/*
sampler GrassS = sampler_state	// Grass Texture
{
	Texture		= <g_sTexture>;		
	MinFilter	= Anisotropic;				
	MagFilter	= Linear;
	MipFilter	= Linear;
	MaxAnisotropy = 8;
};*/

// HDR Texture Samples
sampler s0 : register(s0);
sampler s1 : register(s1);
sampler s2 : register(s2);
sampler s3 : register(s3);
sampler s4 : register(s4);
sampler s5 : register(s5);
sampler s6 : register(s6);
sampler s7 : register(s7);

// Materials
float3 Silver = float3(0.971519, 0.959915, 0.915324);
float3 Aluminium = float3(   0.913183,    0.921494  ,  0.924524);
float3 Gold = float3(        1        ,   0.765557  ,  0.336057);
float3 Copper = float3(      0.955008 ,   0.637427  ,  0.538163);
float3 Chromium = float3(    0.549585 ,   0.556114  ,  0.554256);
float3 Nickel = float3(      0.659777 ,   0.608679  ,  0.525649);
float3 Titanium = float3(    0.541931 ,   0.496791  ,  0.449419);
float3 Cobalt = float3(      0.662124 ,   0.654864  ,  0.633732);
float3 Platinum = float3(    0.672411 ,   0.637331  ,  0.585456);

// Refration values
float Quartz   = 0.045593921f;
float ice      = 0.017908907f;
float Water    = 0.020373188f;
float Alcohol  = 0.01995505f;
float Glass    = 0.04f;
float Milk     = 0.022181983f;
float Ruby     = 0.077271957f;
float Crystal  = 0.111111111f;
float Diamond  = 0.171968833f;
float Skin     = 0.028f;

/*********************************************************************
						Shader Output Types
*********************************************************************/
struct VS_HDR_OUTPUT
{
	float4 posH		: POSITION;	// Position in Screen Space
	float2 texC		: TEXCOORD0;
	float3 posV		: TEXCOORD1;	// Position in View Space
	float3 normV	: TEXCOORD2;	// Normal in View Space
};


/*********************************************************************
						Vertex Shader Effects
*********************************************************************/	
VS_HDR_OUTPUT	HDR_LightVS(float3 posL		: POSITION0, 
							float3 normL	: NORMAL0,
							float2 tex		: TEXCOORD0)
{
	VS_HDR_OUTPUT outVS = (VS_HDR_OUTPUT)0;

	//float4 viewPos = mul(float4(posL, 1.0f), g_sView);
	//float3 viewNorm = normalize(mul(normL, (float3x3)g_sView));

	outVS.posH = mul(float4(posL, 1.0f), g_sWVP);//mul(viewPos, g_sProj);

	outVS.texC = tex;
	
	//outVS.posV = viewPos.xyz;
	//outVS.normV	= viewNorm;
	
	return outVS;
}	


/*********************************************************************
						Pixel Shader Effects
*********************************************************************/
const float fovy = 0.25 * 3.14159265; 
float3 uv_to_eye(float2 uv, float eye_z)
{
	float invFocalLenX   = tan(fovy * 0.5) * (g_sScreenSize.x / g_sScreenSize.y);
	float invFocalLenY   = tan(fovy * 0.5);
	//uv = float2(uv.x * 2.0 - 1.0, (uv.y-1.0/g_sScreenSize.y) * -2.0 + 1.0);
	uv = (uv * float2(2.0, -2.0) - float2(1.0, -1.0));
	return float3(uv * float2(invFocalLenX, invFocalLenY) * eye_z, eye_z);
}
float3 fetch_eye_pos(float2 uv)
{
	float z = tex2D(DeferredDepth, uv);//*1000.0f; // Depth/Normal buffer
	return uv_to_eye(uv, z);
}

float4	PointLightPS(VS_HDR_OUTPUT inVS)	:COLOR
{
	float4 miscBuff = tex2D(DeferredPosW, inVS.texC);
	float3 posV = fetch_eye_pos(inVS.texC);
	float3 toEye = normalize(-posV);

	// Unpack Normals
	float4 normW = tex2D(DeferredNormW, inVS.texC);
	float2 norm = (normW.xy*2.0f)-1.0f;
	float2 sct;
	sincos(norm.x*PI, sct.x, sct.y);
	float2 scphi = float2(sqrt(1.0 - norm.y*norm.y), norm.y);
	float3 normal = float3(sct.y*scphi.x, sct.x*scphi.x, scphi.y); 
	normal = normalize(normal);

	// Start with ambient
	//float3 intensity = float3(0.02f, 0.02f, 0.02f);
	float3 intensity = float3(0.1f, 0.1f, 0.1f);
	
	float weight = 1.0f;
	if(f_UseSSAO)	// Check if SSAO is enabled
	{
		weight = (float)(tex2D(SSAOMap, inVS.texC));
		intensity *= weight;
	}
	else
		intensity *= 0.5f;

	///////////////////////////////////////////////////
	// Debug: Display Raw Input Maps
	///////////////////////////////////////////////////
	// Uncomment bellow to see View Position
	//return float4(posV*0.0001f, 1.0f);

	// Uncomment bellow to see View Normal Map
	//return float4(normal.xyz*0.01f, 1.0f);

	// Uncomment bellow to see Specular Map
	//return float4(normW.a*0.01,normW.a*0.01,normW.a*0.01,1.0);

	// Uncomment bellow to see SSAO Map
	//return float4(weight*0.01f, weight*0.01f, weight*0.01f, 1.0f);
	///////////////////////////////////////////////////

	// Add emissive
	intensity += g_sEmissive.rgb;

	for(int light = 0; light < g_sNum_Lights; light++)
	{
		// Calculate illumination
		float3 lightDir = normalize(posV - g_sLightPos[light].xyz);
        float3 r = reflect(lightDir, normal); 
        float phongValue = saturate(dot(r,toEye)); 
		phongValue = (r == 0.0f) ? 0.0f : phongValue;

		// Calculate diffuse
		float NdotL = saturate(dot(normal, -lightDir));
		float diffuseCoeff = 0.1f; //miscBuff.z;	// 'z' stores the Diffuse coeff
		float diffuse = diffuseCoeff * NdotL; 

		float dist = distance(g_sLightPos[light].xyz, posV);
		float3 cK = weight * (g_sLightIntensity[light].rgb/(dist*dist));

		// Calculate specular
		float phongExp = normW.b*127 + 1;
		float phongCoeff = normW.a;	

		float nF = ((phongExp * ONE_OVER_TWO_PI) + ONE_OVER_PI);
		float3 H = normalize(toEye - lightDir);
		float spec = phongCoeff * (( nF * pow(saturate(dot(normal,H)),phongExp) * NdotL)); 

		// Scale with the distance from the light
		intensity += (diffuse*cK) + (spec*cK);
	}

	// Apply texture if enabled
	if(g_sIsTex)
		intensity.rgb *= tex2D(DeferredMtrl, inVS.texC).rgb * weight;

	// Apply fog
	float fog = saturate(exp((FogStart-length(posV))*FogDensity));
	return float4(intensity.rgb*fog, 1.0f);
}

// Initial Sample
float4	LumInitSample(float2 tex	: TEXCOORD0)	:COLOR
{
	float3	sample = 0.0f;
	float	sumLogLum = 0.0f;	// Sum of log luminance

	for(int i = 0; i < 9; i++)
	{
		sample = tex2D(s0, tex + g_sSampleOffset[i]);
		sumLogLum += log(dot(sample, LUMINANCE) + 0.0001f);
	}
	// Get the average
	sumLogLum /= 9;
	return float4(sumLogLum, sumLogLum, sumLogLum, 1.0f);
}

// Iteration Sample
float4	LumIterSample(float2 tex	: TEXCOORD0)	:COLOR
{
	float sum = 0.0f;	// Resample sum
	
	for(int i = 0; i < 16; i++)
	{
		sum += tex2D(s0, tex + g_sSampleOffset[i]);
	}
	// Get the average
	sum /= 16;
	return float4(sum, sum, sum, 1.0f);
}

// Final Sample
float4	LumFinalSample(float2 tex	:TEXCOORD0)	:COLOR
{
	float sum = 0.0f;	// Resample sum

	for(int i = 0; i < 16; i++)
	{
		sum += tex2D(s0, tex + g_sSampleOffset[i]);
	}
	// Calculate the average luminance
	sum = exp(sum/16);
	return float4(sum, sum, sum, 1.0f);
}

float4	CalculateAdaptedLumPS(float2 tex	:TEXCOORD0)	:COLOR
{
	float adaptedLum = tex2D(s0, float2(0.5f, 0.5f));
	float currentLum = tex2D(s1, float2(0.5f, 0.5f));

	// Adapt to new light by 2% every frame, based on 30 fps rate
	float newAdaptation = adaptedLum + (currentLum - adaptedLum) * (1 - pow(0.98f, 30 * g_sTimeElapsed));
	return float4(newAdaptation, newAdaptation, newAdaptation, 1.0f);
}

float4	FinalScenePS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 sample = tex2D(s0, tex);
	float4 bloom = tex2D(s1, tex);
	float4 star = tex2D(s2, tex);
	float adaptedLum = tex2D(s3, float2(0.5f, 0.5f));

	//return sample;
	if(g_sUseBlueShift)
	{
		// Get the lerp amount for the blue shift
		float blueShiftCoeff = 1.0f - (adaptedLum + 1.5)/4.1;
		blueShiftCoeff = saturate(blueShiftCoeff);

		// Lerp between current color and blue (desaturate)
		float3 rodColor = dot((float3)sample, LUMINANCE)*BLUE_SHIFT;
		sample.rgb = lerp((float3)sample, rodColor, blueShiftCoeff);
	}

	// Calculate the high range map color value
	if(g_sUseToneMap)
	{
		sample.rgb *= g_sMiddleGray/(adaptedLum + 0.001f);
		sample.rgb /= (1.0f+sample);
	}

	// Add the Star and Bloom effects
	sample += g_sStarScale * star;
	sample += g_sBloomScale * bloom;
	//return sample;
	return float4(pow(abs(sample.rgb), 1.0 / gamma),sample.a);	// Gamma correction
}

// Scale the source texture down to 1/16 scale
float4	DownSample4x4PS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 sample = 0.0f;
	for(int i = 0; i < 16; i++)
	{
		sample += tex2D(s0, tex + g_sSampleOffset[i]);
	}
	return sample/16;
}

// Scale the source texture down to 1/4 scale
float4	DownSample2x2PS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 sample = 0.0f;
	for(int i = 0; i < 4; i++)
	{
		sample += tex2D(s0, tex + g_sSampleOffset[i]);
	}
	return sample/4;
}

// 5x5 Gaussian Blur
float4	GaussBlur5x5PS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 sample = 0.0f;
	for(int i = 0; i < 12; i++)
	{
		sample += g_sSampleWeight[i] * tex2D(s0, tex + g_sSampleOffset[i]);
	}
	return sample;
}

// Bright pass for the source texture
float4	BrightFilterPS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 sample = tex2D(s0, tex);
	float adaptedLum = tex2D(s1, float2(0.5f, 0.5f));

	// Calculate for tone mapping
	sample.rgb *= g_sMiddleGray/(adaptedLum + 0.001f);

	// Clamp to 0
	sample = max(sample, 0.0f);

	sample.rgb /= (BRIGHT_OFFSET+sample);
	return sample;
}

// Blur the sorce image along one axis, 
// need two passes for horizontal and vertical
float4	BloomPS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 sample = 0.0f;
	float4 color = 0.0f;
	float2 samplePos;

	// One directional gaussian blur
	for(int i = 0; i < 15; i++)
	{
		samplePos = tex + g_sSampleOffset[i];
		color = tex2D(s0, samplePos);
		sample += g_sSampleWeight[i]*color;
	}
	return sample;
}

// A star has 8 lines and it takes three passes to draw a full star
float4	StarPS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 sample = 0.0f;
	float4 color = 0.0f;
	float2 samplePos;

	// Sample from eight point within the star line
	for(int i = 0; i < 8; i++)
	{
		samplePos = tex + g_sSampleOffset[i];
		sample = tex2D(s0, samplePos);
		color += g_sSampleWeight[i] * sample;
	}
	return color;
}


// Returns the average of 1 input texture
float4	MergeTextures_1PS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 color = 0.0f;
	color += g_sSampleWeight[0] * tex2D(s0, tex);
	return color;
}

// Returns the average of 2 input textures
float4	MergeTextures_2PS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 color = 0.0f;
	color += g_sSampleWeight[0] * tex2D(s0, tex);
	color += g_sSampleWeight[1] * tex2D(s1, tex);
	return color;
}

// Returns the average of 3 input textures
float4	MergeTextures_3PS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 color = 0.0f;
	color += g_sSampleWeight[0] * tex2D(s0, tex);
	color += g_sSampleWeight[1] * tex2D(s1, tex);
	color += g_sSampleWeight[2] * tex2D(s2, tex);
	return color;
}

// Returns the average of 4 input textures
float4	MergeTextures_4PS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 color = 0.0f;
	color += g_sSampleWeight[0] * tex2D(s0, tex);
	color += g_sSampleWeight[1] * tex2D(s1, tex);
	color += g_sSampleWeight[2] * tex2D(s2, tex);
	color += g_sSampleWeight[3] * tex2D(s3, tex);
	return color;
}

// Returns the average of 5 input textures
float4	MergeTextures_5PS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 color = 0.0f;
	color += g_sSampleWeight[0] * tex2D(s0, tex);
	color += g_sSampleWeight[1] * tex2D(s1, tex);
	color += g_sSampleWeight[2] * tex2D(s2, tex);
	color += g_sSampleWeight[3] * tex2D(s3, tex);
	color += g_sSampleWeight[4] * tex2D(s4, tex);
	return color;
}

// Returns the average of 6 input textures
float4	MergeTextures_6PS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 color = 0.0f;
	color += g_sSampleWeight[0] * tex2D(s0, tex);
	color += g_sSampleWeight[1] * tex2D(s1, tex);
	color += g_sSampleWeight[2] * tex2D(s2, tex);
	color += g_sSampleWeight[3] * tex2D(s3, tex);
	color += g_sSampleWeight[4] * tex2D(s4, tex);
	color += g_sSampleWeight[5] * tex2D(s5, tex);
	return color;
}

// Returns the average of 7 input textures
float4	MergeTextures_7PS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 color = 0.0f;
	color += g_sSampleWeight[0] * tex2D(s0, tex);
	color += g_sSampleWeight[1] * tex2D(s1, tex);
	color += g_sSampleWeight[2] * tex2D(s2, tex);
	color += g_sSampleWeight[3] * tex2D(s3, tex);
	color += g_sSampleWeight[4] * tex2D(s4, tex);
	color += g_sSampleWeight[5] * tex2D(s5, tex);
	color += g_sSampleWeight[6] * tex2D(s6, tex);
	return color;
}

// Returns the average of 8 input textures
float4	MergeTextures_8PS(float2 tex	:TEXCOORD0)	:COLOR
{
	float4 color = 0.0f;
	color += g_sSampleWeight[0] * tex2D(s0, tex);
	color += g_sSampleWeight[1] * tex2D(s1, tex);
	color += g_sSampleWeight[2] * tex2D(s2, tex);
	color += g_sSampleWeight[3] * tex2D(s3, tex);
	color += g_sSampleWeight[4] * tex2D(s4, tex);
	color += g_sSampleWeight[5] * tex2D(s5, tex);
	color += g_sSampleWeight[6] * tex2D(s6, tex);
	color += g_sSampleWeight[7] * tex2D(s7, tex);
	return color;
}


/*********************************************************************
						Techniques
*********************************************************************/
technique SpecTech
{
	pass p0	// Front Facing
	{
		VertexShader 	= compile vs_3_0	HDR_LightVS();
		PixelShader		= compile ps_3_0	PointLightPS();
		//FillMode		= Solid;
		//CullMode		= None;
		ZEnable			= false;//true;
	/*	AlphaBlendEnable = true;
		Srcblend		= SrcAlpha;	//One;
		Destblend		= InvSrcAlpha;	//One;
		AlphaTestEnable	= false;  
	
		ZEnable			= true;
		ZWriteEnable	= true;
		ZFunc			= lessequal;
		CullMode		= CCW;
		StencilEnable	= true;
		StencilMask		= 0xffffffff;
		StencilWriteMask = 0xffffffff;
		StencilPass		= Keep;
		StencilFail		= Keep;
		StencilZFail 	= DecrSat;
		StencilFunc		= Always;*/
		//ColorWriteEnable = 0;
	}
/*	pass p1	// Back Facing
	{
		VertexShader 	= compile vs_3_0	HDR_LightVS();
		PixelShader		= compile ps_3_0	PointLightPS();		
		AlphaBlendEnable = true;
		Srcblend		= SrcAlpha;	//One;
		Destblend		= InvSrcAlpha;	//One;
		AlphaTestEnable	= false; 

		ZEnable			= true;
		ZWriteEnable	= 0;//true;
		ZFunc			= lessequal;
		CullMode		= CW;
		StencilEnable	= true;
		StencilMask		= 0xffffffff;
		StencilWriteMask = 0xffffffff;
		StencilPass		= IncrSat;
		StencilFail		= IncrSat;
		StencilZFail 	= IncrSat;
		StencilFunc		= Always;
		ColorWriteEnable = 0;
	}
	pass p2	// Draw Lights
	{
		VertexShader 	= compile vs_3_0	HDR_LightVS();
		PixelShader		= compile ps_3_0	PointLightPS();		
		AlphaBlendEnable = true;
		Srcblend		= SrcAlpha;	//One;
		Destblend		= InvSrcAlpha;	//One;
		AlphaTestEnable	= false; 

		ZEnable			= true;
		ZWriteEnable	= 0;//true;
		//ZFunc			= lessequal;
		CullMode		= None;
		StencilEnable	= true;
		StencilMask		= 0xffffffff;
		StencilWriteMask = 0xffffffff;
		StencilPass		= Keep;
		StencilFail		= Keep;
		StencilZFail 	= Keep;
		StencilRef		= 0;
		StencilFunc		= Equal;
		ColorWriteEnable = red | green | blue;
	}*/
}

technique BloomTech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	BloomPS();
	}
}


technique StarTech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	StarPS();
	}
}

// Starts calculating the avergae luminance
technique StartAvgLumTech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	LumInitSample();
	}
}

// Scale down the luminance texture
technique NextAvgLumTech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	LumIterSample();
	}
}

// Complete the evaluation
technique FinishAvgLumTech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	LumFinalSample();
	}
}

// Caclulate the light adaption level
technique CalculateAdaptedLumTech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	CalculateAdaptedLumPS();
	}
}

// Scale the source texture down to 1/16 scale
technique DownSample4x4Tech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	DownSample4x4PS();
	}
}

// Scale the source texture down to 1/4 scale
technique DownSample2x2Tech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	DownSample2x2PS();
	}
}

// 5x5 gaussian blur
technique GaussBlur5x5Tech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	GaussBlur5x5PS();
	}
}

// Run a high bright filter pass
technique BrightPassTech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	BrightFilterPS();
	}
}

technique FinalPassTech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	FinalScenePS();
	}
}

technique MergeTex_1Tech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	MergeTextures_1PS();
	}
}

technique MergeTex_2Tech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	MergeTextures_2PS();
	}
}

technique MergeTex_3Tech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	MergeTextures_3PS();
	}
}

technique MergeTex_4Tech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	MergeTextures_4PS();
	}
}

technique MergeTex_5Tech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	MergeTextures_5PS();
	}
}

technique MergeTex_6Tech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	MergeTextures_6PS();
	}
}

technique MergeTex_7Tech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	MergeTextures_7PS();
	}
}

technique MergeTex_8Tech
{
	pass p0
	{
		PixelShader		= compile ps_3_0	MergeTextures_8PS();
	}
}
