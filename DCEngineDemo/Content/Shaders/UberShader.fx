uniform extern float4x4 g_sWVP;				// WorldViewProj Matrix
uniform extern float4x4 g_sWorldInvTrans;	// WorldInverseTranspose Matrix
uniform extern float4x4 g_sWorld;			// World Matrix
uniform extern float3	g_sCameraPos;		// Eye Position (camera in world coordinates)

// Lighting
uniform	extern float3	g_sLightPosW;		// Light's position (world coordinates)
uniform extern float4	g_sAmbientLight;	// Ambient Light
uniform extern float4	g_sDiffuseLight;	// Diffuse Light
uniform extern float4	g_sSpecLight;		// Specular Light
uniform extern float3	g_sAttenuation;		// Attenuation012

// Materials
uniform extern float4	g_sAmbientMtrl;		// Ambient Material
uniform extern float4	g_sDiffuseMtrl;		// Diffuse Material
uniform extern float4	g_sSpecMtrl;		// Specular Material
uniform extern float	g_sSpecPower;		// Specular Power

// Useing float4 array allows for Color in the same array as the WorldMat
uniform extern float4	g_sInstanceData[200]; 

// Textures
uniform extern texture g_sTexture;	

// Texture Sample
sampler	TextureS = sampler_state			
{
	Texture		= <g_sTexture>;				
	MinFilter	= Linear;			
	MagFilter	= Linear;
	MipFilter	= Linear;
};

/*********************************************************************
						Uber Shader Switches
*********************************************************************/
uniform extern bool	f_Phong;
uniform extern bool	f_BlinnPhong;
uniform extern bool f_IsTex;


/*********************************************************************
						Shader Output Types
*********************************************************************/
struct VS_OUTPUT						// Vertex Shader						
{
	float4 posH		: POSITION0;
	float3 normW	: TEXCOORD0;
	float3 posW		: TEXCOORD1;
	float2 texC		: TEXCOORD2;
	float3 toEyeW	: TEXCOORD3;
};


/*********************************************************************
						Vertex Shader Effects
*********************************************************************/	
/*
VS_OUTPUT	InstancingVS(float3 posL	: POSITION0,
					     float3 normL	: NORMAL0,
					     float2 texC	: TEXCOORD0,
						 int	index	: BLENDINDICES0)
{
	VS_OUTPUT outVS = (VS_OUTPUT)0;	// Zero out the output
	
	// Get the Vertex Instance Index
	int InstIndex = index; 	//((int[4])(index))[0];	
	
	float4x4 instWorldMat;	// Build the WorldMat for this instance
	instWorldMat[0] = g_sInstanceData[InstIndex];
	instWorldMat[1] = g_sInstanceData[InstIndex + 1];
	instWorldMat[2] = g_sInstanceData[InstIndex + 2];
	instWorldMat[3] = g_sInstanceData[InstIndex + 3];

	float4 worldPos = mul(float4(posL, 1.0f), instWorldMat);
	float3 worldNor = mul(normL, (float3x3)(instWorldMat)).xyz;	// WorldInvTrans

	outVS.normW = mul(float4(worldNor, 0.0f), g_sWVP);
	outVS.posH = mul(worldPos, g_sWVP);

	outVS.tex0 = texC;	
	//outVS.color = g_sInstanceData[InstIndex + 4];	// Color of this instance

	return outVS;
}
*/

VS_OUTPUT	MainVS (float3 posL	: POSITION0,
					float3 normL : NORMAL0,
					float2 texC	: TEXCOORD0)
{
	VS_OUTPUT outVS = (VS_OUTPUT)0;	// Zero-out the output

	// Transform normal to world space
	float3 normalW = mul(float4(normL, 1.0f), g_sWorldInvTrans).xyz;
	outVS.normW = normalize(normalW);

	// Transform vertex position to world space
	outVS.posW = mul(float4(posL, 1.0f), g_sWorld).xyz;

	// Transform to homogeneous clip space
	outVS.posH = mul(float4(posL, 1.0f), g_sWVP);

	// Vector to eye
	outVS.toEyeW = g_sCameraPos - outVS.posW;

	outVS.texC = texC;	// Pass texture for rasterization

	// Uber Code Here


	return outVS;
}


/*********************************************************************
						Pixel Shader Effects
*********************************************************************/
float4	BlinnPhongPS(VS_OUTPUT outVS)				// Blinn Phong
{
	// Normalizing in the pixel shader, has a much smoother finish
	outVS.normW = normalize(outVS.normW);
	float3 toEye = normalize(outVS.toEyeW);	

	float3 lightDir = normalize(g_sLightPosW - outVS.posW);
	
	// Blinn Phong calculation
	float3 h = normalize(lightDir + normalize(toEye));
	float t = pow(max(dot(h, outVS.normW), 0.0f), g_sSpecPower);
	
	float s = max(dot(lightDir, outVS.normW), 0.0f);

	float3 spec = t * (g_sSpecMtrl * g_sSpecLight).rgb;
	float3 diffuse = s * (g_sDiffuseMtrl  * g_sDiffuseLight).rgb;
	float3 ambient = (g_sAmbientMtrl * g_sAmbientLight);

	float d = distance(g_sLightPosW, outVS.posW);
	float a = g_sAttenuation.x + g_sAttenuation.y*d + g_sAttenuation.z*d*d;
	
	if(f_IsTex)	// If their is a texture
	{
		// Add lighting and texture color
		float4 texColor = tex2D(TextureS, outVS.texC);
		float3 color = ((diffuse+spec)*(texColor.rgb+ 1/a) + ambient);
		return float4(color, texColor.a * g_sDiffuseMtrl.a);
	}
	// Else just use lighting
	return float4(ambient + (diffuse + spec)/a, g_sDiffuseMtrl.a);
}

float4	PhongPS (VS_OUTPUT outVS)				// Phong
{
	// Normalize per pixel
	outVS.normW = normalize(outVS.normW);
	float3 toEye = normalize(outVS.toEyeW);

	// Phong calculation
	float3 r = reflect(-g_sLightPosW, outVS.normW);
	float t = pow(max(dot(r, toEye), 0.0f), g_sSpecPower);

	float s = max(dot(g_sLightPosW, outVS.normW), 0.0f);

	float3 spec = t*(g_sSpecMtrl*g_sSpecLight).rgb;
	float3 diffuse = s*(g_sDiffuseMtrl*g_sDiffuseLight).rgb;
	float3 ambient = g_sAmbientMtrl*g_sAmbientLight;

	if(f_IsTex)	// If their is a texture
	{
		// Add lighting and texture color
		float4 texColor = tex2D(TextureS, outVS.texC);
		float3 color = (ambient + diffuse)*texColor.rgb + spec;
		return float4(color, texColor.a * g_sDiffuseMtrl.a);
	}
	// Else just use lighting
	return float4(ambient + diffuse + spec, g_sDiffuseMtrl.a);
}

float4	MainPS (VS_OUTPUT outVS)	: COLOR
{
	if(f_Phong)			//(UsePhong)
		return PhongPS(outVS);
	
	if(f_BlinnPhong)	//(UseBlinnPhong)
		return BlinnPhongPS(outVS);

	return float4(1.0f, 1.0f, 1.0f, 0.5f);	// Default Color
}

/*********************************************************************
						Techniques
*********************************************************************/
technique MainTech
{
	pass p0
	{
		VertexShader	= compile vs_3_0	MainVS();
		PixelShader		= compile ps_3_0	MainPS();
		//Lighting		= true;
		//LightEnable[0]	= true;
		FillMode		= Solid;
		ZEnable			= true;
	}
}
