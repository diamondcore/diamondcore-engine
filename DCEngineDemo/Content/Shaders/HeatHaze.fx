#define TWOPASSPETHOD 0

float g_fBump1;
float g_fBump2;
float2 g_fNormTexOffset1;
float2 g_fNormTexOffset2;
float g_fHeatIntensity1;
float g_fHeatIntensity2;
float g_fHeatScale1;
float g_fHeatScale2;
float g_fDistFalloff;
/*
texture NormalMapTexture;
sampler2D NormalMapSampler=sampler_state 
{
    Texture = <NormalMapTexture>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};*/

texture HeatTexture;
sampler2D HeatSampler=sampler_state 
{
    Texture = <HeatTexture>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
    AddressU = Clamp;
    AddressV = Clamp;
};

#if TWOPASSPETHOD

texture MaskTexture;
sampler2D MaskSampler=sampler_state 
{
    Texture = <MaskTexture>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
    AddressU = Clamp;
    AddressV = Clamp;
};

float4 DrawMaskPS(float2 ScreenCoord : TEXCOORD0) : COLOR 
{
	return float4(255, 255, 255, 1.0);
}

float4 DrawMaskScreenPS(float2 ScreenCoord : VPOS) : COLOR 
{
	//return float4(255, 255, 255, 1.0);

	float3 c = tex2D(ScreenSampler, ScreenCoord / g_fScreenSize);
	return float4(c.xyz, 1.0f);
}

float4 Haze_DefaultPS(float2 TexCoord : TEXCOORD0) : COLOR 
{
	float3 distort = tex2D(NormalMap, (TexCoord + g_fNormTexOffset1) * g_fHeatScale) * 2.0 - 1.0;
	distort=normalize(distort)*g_fBump;
	
	float3 Refract = tex2D(HeatSampler, TexCoord + (distort.xy * 0.5f));
	
	float3 Scene = tex2D(HeatSampler, TexCoord);
	
	float4 Mask = tex2D(MaskSampler, TexCoord);	
	float Alpha = saturate(dot(Mask,(float3)(1.0/3.0*g_fHeatIntensity)));

	float3 output = lerp(Refract, Scene, 1 - Alpha);
	return float4(output, 1.0);
}

float4 Haze_BumpByMaskPS(float2 TexCoord : TEXCOORD0) : COLOR 
{
	float3 Mask = tex2D(MaskSampler, TexCoord);	

	float3 distort = tex2D(NormalMap, (TexCoord + g_fNormTexOffset1) * g_fHeatScale) * 2.0 - 1.0;
	distort=normalize(distort)*(g_fBump * length(Mask));
	
	float3 Refract = tex2D(DeferredMtrl, TexCoord + (distort.xy * 0.5f));

	return float4(Refract, 1.0);
}

#endif

float4 Haze_ScenePS(float2 TexCoord : TEXCOORD0) : COLOR 
{
	float3 distort = lerp(normalize((tex2D(NormalMap, (TexCoord + g_fNormTexOffset1) * g_fHeatScale1) * g_fBump1)),
		normalize((tex2D(NormalMap, (TexCoord + g_fNormTexOffset2) * g_fHeatScale2) * g_fBump2)), 0.5f)* 2.0 - 1.0;
	//float3 distort = tex2D(NormalMapSampler, (TexCoord + g_fNormTexOffset1) * g_fHeatScale1) * 2.0 - 1.0;
		//return float4(distort.xyz, 1.0);
	//distort=normalize(distort)*g_fBump1;
	
	float3 Refract = tex2D(DeferredMtrl, TexCoord + (distort.xy * 0.5f));

	return float4(Refract.xyz, 1.0);
}

//This variation is for scaling the haze based on the depth of the distortion mesh
#if 1
struct HazeVertOut
{
	float4 posH : POSITION0;
	float2 depth : TEXCOORD0;
};

HazeVertOut Haze_SinglePassVS(float3 posL : POSITION)
{
	HazeVertOut outVS = (HazeVertOut)0;

	outVS.posH = mul(float4(posL, 1.0), g_sWVP);
	outVS.depth = outVS.posH.zw;

	return outVS;
}


#else

float4 Haze_SinglePassVS(float3 posL : POSITION, float3 norm : NORMAL) : POSITION0
{
	float4 posH;

	posH = mul(float4(posL + (norm * 0.001f), 1.0), g_sWVP);

	return posH;
}
#endif

//This variation is for scaling the haze based on the depth of the distortion mesh
#if 1

float4 Haze_SinglePassPS(float2 posV : VPOS, float2 texC : TEXCOORD0) : COLOR
{
#if 1
	float3 distort = lerp(normalize((tex2D(NormalMap, (posV * g_sPixelSize) + g_fNormTexOffset1 * g_fHeatScale1)) * g_fBump1) * 2.0 - 1.0,
		normalize((tex2D(NormalMap, (posV * g_sPixelSize) + g_fNormTexOffset2 * g_fHeatScale2)) * g_fBump2) * 2.0 - 1.0, 0.5f);
	distort=normalize(distort)*g_fBump1;
#elif 0
	float3 distort = lerp(tex2D(NormalMap, (posV * g_sPixelSize) + g_fNormTexOffset1 * g_fHeatScale1),
		tex2D(NormalMap, (posV * g_sPixelSize) + g_fNormTexOffset2 * g_fHeatScale2), 0.5f)* 2.0 - 1.0;
	distort=normalize(distort)*g_fBump1;
#else
	float3 distort = lerp(normalize(tex2D(NormalMap, (posV * g_sPixelSize) + g_fNormTexOffset1 * g_fHeatScale1) * g_fBump1),
		normalize(tex2D(NormalMap, (posV * g_sPixelSize) + g_fNormTexOffset2 * g_fHeatScale2) * g_fBump2), 0.5f)* 2.0 - 1.0;
	//distort=normalize(distort)*g_fBump;


#endif
	
#if 0
	float3 Refract = tex2D(DeferredMtrl, (posV * g_sPixelSize) + distort.xy);
#else
	//float dist = tex2D(DeferredDepth, posV * g_sPixelSize).r * 1000;
	float3 Refract = tex2D(DeferredMtrl, (posV * g_sPixelSize) + (distort.xy / texC));
#endif
	return float4(Refract, 1.0);
}

//This variation is for scaling the haze based on the depth buffer, or no depth scaling
#else

float4 Haze_SinglePassPS(float2 posV : VPOS) : COLOR
{
	float3 distort = tex2D(NormalMap, (posV * g_sPixelSize) + g_fNormTexOffset1 * g_fHeatScale) * 2.0 - 1.0;
	distort=normalize(distort)*g_fBump;
	
#if 1
	float3 Refract = tex2D(DeferredMtrl, (posV * g_sPixelSize) + (distort.xy * 0.5f));
#else
	//float dist = tex2D(DeferredDepth, posV * g_sPixelSize).r * 1000;
	float3 Refract = tex2D(DeferredMtrl, (posV * g_sPixelSize) + ((distort.xy) / (tex2D(DeferredDepth, posV * g_sPixelSize).r * 1000)));
#endif
	return float4(Refract, 1.0);
}
#endif



// Input: It uses texture coords as the random number seed.
// Output: Random number: [0,1), that is between 0.0 and 0.999999... inclusive.
// Author: Michael Pohoreski
// Copyright: Copyleft 2012 :-)

//float random( float2 p )
//{
//  // We need irrationals for pseudo randomness.
//  // Most (all?) known transcendental numbers will (generally) work.
// float2 r = float2(
//    23.1406926327792690,  // e^pi (Gelfond's constant)
//     2.6651441426902251); // 2^sqrt(2) (Gelfond�Schneider constant)
//  return frac( cos( fmod( 123456789., 1e-7 + 256. * dot(p,r) ) ) );  
//}


//float4 NoisePS(float2 TexCoord : TEXCOORD0) : COLOR 
//{
//	float2 Coord;
//	float2 Flip;
//
//	Coord.x = TexCoord[0];
//	Coord.y = TexCoord[1];
//
//	Flip.x = TexCoord[1];
//	Flip.y = TexCoord[0];
//
//	float3 distort = tex2D(ScreenSampler, float2(random(Coord), random(Flip)) + float2(g_fNormTexOffsetX, g_fNormTexOffsetY)) * 2.0 - 1.0;
//	distort=normalize(distort)*g_fBump;
//	
//	float3 Refract = tex2D(ScreenSampler, TexCoord + (distort.xy * g_fHeatIntensity));
//	
//	return float4(Refract.xyz, 1.0f);
//}

technique Haze
{
	
	pass p0_FullscreenMask
	{
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		ZEnable = false;
		PixelShader  = compile ps_3_0 Haze_ScenePS();	
	}
	pass p1_SinglePass
	{
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		//ZEnable = false;
		//ZWriteEnable = false;
		VertexShader	= compile vs_3_0 Haze_SinglePassVS();
		PixelShader		= compile ps_3_0 Haze_SinglePassPS();
	}
#if TWOPASSPETHOD
	pass p2_BuildMask
	{
		ZEnable = false;
		AlphaBlendEnable = true;
		PixelShader  = compile ps_3_0 DrawMaskPS();
	}
	pass p3_GivenMask
	{	
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		ZEnable = false;
		PixelShader  = compile ps_3_0 Haze_DefaultPS();	
	}
#endif
}
