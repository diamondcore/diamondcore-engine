#define NumBlurSamples 5
uniform extern float4x4 g_sPrevViewProj;
uniform extern float4x4 g_sPrevWorld;
uniform extern bool f_UseDeferredBlur;	// Multiple buffers

uniform extern float g_fVelScale = 1.0f;	// Scale the velocity

texture g_VelocityBlurTex;
sampler VelBlur = sampler_state
{
	Texture = <g_VelocityBlurTex>;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

struct BasicVertOut
{
	float4 PosH : POSITION;
	float4 Color : COLOR;
};
struct PS_OUTPUT_DEF_BLUR
{
	float4 mtrl		: COLOR0;
	float4 normW	: COLOR1;
	float4 posW		: COLOR2;	// (X,Y)
	float4 depth	: COLOR3;	// (Z)
};

BasicVertOut BuildVelocityBuffVS(float3 posL : POSITION0)
{
	BasicVertOut outVS = (BasicVertOut)0;

	outVS.PosH = mul(float4(posL, 1.0), g_sWVP);
	float4 currentPos = outVS.PosH;
	currentPos /= currentPos.w;
	float4 prevPos = mul(float4(posL, 1.0), mul(g_sWorld, g_sPrevViewProj));
	prevPos /= prevPos.w;
	outVS.Color = saturate((currentPos - prevPos) + float4(0.5f, 0.5f, 0, 1));

	return outVS;
}
float4 BuildVelocityBuffPS(BasicVertOut input)	:COLOR0
{
	return input.Color;
}

PS_OUTPUT_DEF_BLUR BlurDeffPS(float2 texCoord : TEXCOORD0)// : COLOR
{
	PS_OUTPUT_DEF_BLUR outPS = (PS_OUTPUT_DEF_BLUR)0;

	float2 texC = texCoord;
	float4 color = tex2D(DeferredMtrl, texCoord);
	float4 posW = tex2D(DeferredPosW, texCoord);
	float4 normW = tex2D(DeferredNormW, texCoord);
	float4 depth = tex2D(DeferredDepth, texCoord);

#if 1
	float4 velocity = tex2D(VelBlur, texC) - float4(0.5f, 0.5f,0, 0);
	
	//Y-value needs to be flipped, or objects will blur in the wrong direction.
	velocity.y*= -1;
	float4 currentColor;

	for(int i = 1; i < NumBlurSamples; ++i, texC += ((velocity.xy*g_fVelScale * velocity.w) / NumBlurSamples))
	{
		currentColor = tex2D(DeferredMtrl, texC);
		color += currentColor;

		posW += tex2D(DeferredPosW, texC);
		normW += tex2D(DeferredNormW, texC);
		depth += tex2D(DeferredDepth, texC);
	}
	color /= NumBlurSamples;
	posW /= NumBlurSamples;
	normW /= NumBlurSamples;
	depth /= NumBlurSamples;
	outPS.mtrl = color;
	outPS.normW = normW;
	outPS.posW = posW;
	outPS.depth = depth;
#else
	color = tex2D(DeferredPosW, texC);
#endif

	return outPS;	//color;
}


float4 BlurPS(float2 texCoord : TEXCOORD0) : COLOR
{
	float2 texC = texCoord;
	float4 color = tex2D(DeferredMtrl, texCoord);

#if 1
	float4 velocity = tex2D(VelBlur, texC) - float4(0.5f, 0.5f,0, 0);
	
	//Y-value needs to be flipped, or objects will blur in the wrong direction.
	velocity.y*= -1;
	float4 currentColor;

	for(int i = 1; i < NumBlurSamples; ++i, texC += ((velocity.xy*g_fVelScale * velocity.w) / NumBlurSamples))
	{
		currentColor = tex2D(DeferredMtrl, texC);
		color += currentColor;
	}
	color /= NumBlurSamples;
#else
	color = tex2D(DeferredPosW, texC);
#endif

	return color;
}

technique MotionBlurDeffTech
{
	pass p0
	{
		VertexShader	= compile vs_3_0 BuildVelocityBuffVS();
		PixelShader		= compile ps_3_0 BuildVelocityBuffPS();
		CullMode		= ccw;
		FillMode		= Solid;
		ZEnable			= true;
		ZWriteEnable	= true;
		ZFunc			= less;
		StencilEnable	= false;
		AlphaBlendEnable = false;
		AlphaTestEnable	= false;
		//ZEnable		= false;
	}
	pass p1
	{
		//AlphaBlendEnable = true;
		//SrcBlend = SrcAlpha;
		//DestBlend = InvSrcAlpha;
		ZEnable			= false;
		ZWriteEnable	= false;
		VertexShader	= NULL;
		PixelShader		= compile ps_3_0 BlurDeffPS();
		
	}
}

technique MotionBlurTech
{
	pass p0
	{
		VertexShader	= compile vs_3_0 BuildVelocityBuffVS();
		PixelShader		= compile ps_3_0 BuildVelocityBuffPS();
		CullMode		= ccw;
		FillMode		= Solid;
		ZEnable			= true;
		ZWriteEnable	= true;
		ZFunc			= less;
		StencilEnable	= false;
		AlphaBlendEnable = false;
		AlphaTestEnable	= false;
		//ZEnable		= false;
	}
	pass p1
	{
		//AlphaBlendEnable = true;
		//SrcBlend = SrcAlpha;
		//DestBlend = InvSrcAlpha;
		ZEnable			= false;
		ZWriteEnable	= false;
		VertexShader	= NULL;
		PixelShader		= compile ps_3_0 BlurPS();
		
	}
}