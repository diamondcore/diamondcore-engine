// Textures
uniform extern texture	g_sMultiTex0;		// Texture 0
uniform extern texture	g_sMultiTex1;		// Texture 1
uniform extern texture	g_sMultiTex2;		// Texture 2

uniform extern texture	g_sNormalTex2;		// Used for input of specular lighting (shininess) by texture
uniform extern texture	g_sNormalTex3;		// Used for input of specular lighting (shininess) by texture
uniform extern texture	g_sSpecTex2;		// Used for input of specular lighting (shininess) by texture
uniform extern texture	g_sSpecTex3;		// Used for input of specular lighting (shininess) by texture
uniform extern texture	g_sBlendMap;		// Blended Texture Map
uniform extern texture	g_sCustomMap;		// Custom Texture Map

// Setup Samplers
sampler MultiS0 = sampler_state
{
	Texture		= <g_sMultiTex0>;
	MinFilter	= Anisotropic;
	MagFilter	= Linear;
	MipFilter	= Linear;
	MaxAnisotropy = 8;
};
sampler MultiS1 = sampler_state
{
	Texture		= <g_sMultiTex1>;
	MinFilter	= Anisotropic;
	MagFilter	= Linear;
	MipFilter	= Linear;
	MaxAnisotropy = 8;
};
sampler MultiS2 = sampler_state
{
	Texture		= <g_sMultiTex2>;
	MinFilter	= Anisotropic;
	MagFilter	= Linear;
	MipFilter	= Linear;
	MaxAnisotropy = 8;
};

// Nomral Maps
sampler NormalMap2 = sampler_state
{
	Texture		= <g_sNormalTex2>;
	MinFilter	= Anisotropic;
	MagFilter	= Linear;
	//MipFilter	= Linear;
	AddressU	= Wrap;
	AddressV	= Wrap;
	MaxAnisotropy = 8;
};
sampler NormalMap3 = sampler_state
{
	Texture		= <g_sNormalTex3>;
	MinFilter	= Anisotropic;
	MagFilter	= Linear;
	//MipFilter	= Linear;
	AddressU	= Wrap;
	AddressV	= Wrap;
	MaxAnisotropy = 8;
};
// Specular Map
sampler SpecMap2 = sampler_state
{
	Texture		= <g_sSpecTex2>;
	MinFilter	= Anisotropic;
	MagFilter	= Linear;
	//MipFilter	= Linear;
	AddressU	= Wrap;
	AddressV	= Wrap;
	MaxAnisotropy = 8;
};
sampler SpecMap3 = sampler_state
{
	Texture		= <g_sSpecTex3>;
	MinFilter	= Anisotropic;
	MagFilter	= Linear;
	//MipFilter	= Linear;
	AddressU	= Wrap;
	AddressV	= Wrap;
	MaxAnisotropy = 8;
};

// Blend Maps
sampler BlendMapS = sampler_state
{
	Texture		= <g_sBlendMap>;
	MinFilter	= Linear;
	MagFilter	= Linear;
	MipFilter	= Linear;
};
sampler CustomMapS = sampler_state
{
	Texture		= <g_sCustomMap>;
	MinFilter	= Linear;
	MagFilter	= Linear;
	MipFilter	= Linear;
};

/*********************************************************************
						Shader Output Types
*********************************************************************/
struct VS_OUTPUT_TERRAIN
{
	float4 posH		: POSITION0;
	float3 norm		: NORMAL0;
	//float4 color	: COLOR0;
	float2 tiledC	: TEXCOORD0;
	float2 texC		: TEXCOORD1;
};


/*********************************************************************
						Vertex Shader
*********************************************************************/	
VS_OUTPUT_TERRAIN TerrainVS (float3 posL : POSITION0, 
					 float3 normL : NORMAL0, 
					 float2 texC	: TEXCOORD0)
{
	VS_OUTPUT_TERRAIN outVS = (VS_OUTPUT_TERRAIN)0;	// Zero out the output
	
	// Transorm normal to world space
	float3 normalW = mul(float4(normL, 0.0f), g_sWorld);
	normalW = normalize(normalW);
	outVS.norm = normalW;

	// Transform vertex position to world space
	float3 posW = mul(float4(posL, 1.0f), g_sWorld).xyz;

	// Transfomr to homogeneous clip space
	outVS.posH = mul(float4(posL, 1.0f), g_sWVP);

	// Pass on texture coordinates
	outVS.tiledC = texC * 16.0f;
	outVS.texC = texC;
	//outVS.color = color;

	return outVS;
}


/*********************************************************************
						Pixel Shader
*********************************************************************/	
float4 TerrainPS (VS_OUTPUT_TERRAIN vsIn)	: COLOR0
{
	// Layered textures
	float3 c0 = tex2D(MultiS0, vsIn.tiledC).rgb;
	float3 c1 = tex2D(MultiS1, vsIn.tiledC).rgb;
	float3 c2 = tex2D(MultiS2, vsIn.tiledC).rgb;

	// Blended map showing distribution between textures
	float3 B = tex2D(BlendMapS, vsIn.texC).rgb;
	//float3 B = color.rgb;		// Use vertex color 

	// Scale the total color to the range [0, 1]
	float totalInverse = 1.0f/ (B.r + B.g + B.b);

	c0 *= B.r * totalInverse;	// Red	 (texture 0)
	c1 *= B.g * totalInverse;	// Green (texture 1)
	c2 *= B.b * totalInverse;	// Blue  (texture 2)

	// Sum the colors and modulate with the lighting color
	float3 final = (c0 + c1 + c2) * g_sDiffuseMtrl.rgb;

	return float4(final + g_sSpecMtrl, g_sDiffuseMtrl.a);
}


/*********************************************************************
						Techniques
*********************************************************************/
technique TerrainTech
{
	pass p0
	{
		VertexShader	= compile vs_3_0	TerrainVS();
		PixelShader		= compile ps_3_0	TerrainPS();
		//Lighting		= true;
		//LightEnable[0]	= true;
		//ZEnable			= true;

		// Blend RGB components
		AlphaBlendEnable = true;
		SrcBlend		= SrcAlpha;
		DestBlend		= InvSrcAlpha;
		BlendOp			= Add;

		// Blend the Alpha component
		//SeparateAlphaBlendEnable = true;
		//SrcBlendAlpha	= SrcAlpha;
		//DestBlendAlpha	= InvSrcAlpha;
		//BlendOpAlpha	= Add;
	}
}
