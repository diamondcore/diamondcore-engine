float3 g_sElectricStartPos;
float3 g_sElectricEndPos;
//float3 g_sCameraUp;

float g_sElectricArcScale;
float g_sRandOffset;
float g_sElectricSize;
float g_sElectricArcMidpoint;

float4 g_sElectricColor;
/*
texture ArcColorTex;
sampler2D ArcColorSampler = sampler_state
{
	Texture = <ArcColorTex>;
	MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
};*/

// Input: It uses texture coords as the random number seed.
// Output: Random number: [0,1), that is between 0.0 and 0.999999... inclusive.
// Author: Michael Pohoreski
// Copyright: Copyleft 2012 :-)
float random( float2 p , float offset)
{
  // We need irrationals for pseudo randomness.
  // Most (all?) known transcendental numbers will (generally) work.
 float2 r = float2(
    23.1406926327792690,  // e^pi (Gelfond's constant)
     2.6651441426902251); // 2^sqrt(2) (Gelfond�Schneider constant)
  return frac( cos( fmod( 123456789., 1e-7 + 256. + offset * dot(p,r) ) ) );  
}

float4 DrawBolt_VS(float4 pos0 : POSITION0, float3 lerpData : NORMAL) : POSITION0
{
	/*
	NOTE:  Big thanks to reimers.net, for helping me understand shader-based axial billboarding
	http://www.riemers.net/eng/Tutorials/XNA/Csharp/Series4/Billboarding.php
	*/

	/* lerpData Structure
		x : Value between 0 and 1 determining the lerp position between start and end points.
		y : 1 or -1 Value, determining if this vert is up or down.  Necessary for uniform mesh wavering.
		z : value determining the scale in which this vert is allowed to 'waver' from its calculated position.
	*/

	float3 lerpPos = lerp(g_sElectricStartPos, g_sElectricEndPos, lerpData.x);	
	float lerpScale = (1 - abs(g_sElectricArcMidpoint - lerpData.x)) * 2;
	float3 randPos;
#if 0
	randPos.x = (random(float2(lerpPos.z, lerpPos.y), g_sRandOffset) - 0.5f) *  g_sElectricArcScale * lerpScale  * g_sElectricWaverScale  * lerpData.z;
	randPos.y = (random(float2(lerpPos.z, lerpPos.x), g_sRandOffset) - 0.5f) *  g_sElectricArcScale * lerpScale  * g_sElectricWaverScale * lerpData.z;
	randPos.z = (random(float2(lerpPos.y, lerpPos.x), g_sRandOffset) - 0.5f) *  g_sElectricArcScale * lerpScale  * g_sElectricWaverScale  * lerpData.z;
#else
	randPos.x = (random(float2(lerpPos.z- g_sCameraPos.z, lerpPos.y-g_sCameraPos.y), g_sRandOffset) - 0.5f) *  g_sElectricArcScale * lerpScale  * lerpData.z;
	randPos.y = (random(float2(lerpPos.z-g_sCameraPos.z, lerpPos.x-g_sCameraPos.x), g_sRandOffset) - 0.5f) *  g_sElectricArcScale * lerpScale  * lerpData.z;
	randPos.z = (random(float2(lerpPos.y-g_sCameraPos.y, lerpPos.x-g_sCameraPos.x), g_sRandOffset) - 0.5f) *  g_sElectricArcScale * lerpScale  * lerpData.z;
#endif
	float3 center = float4(lerpPos + randPos, pos0.w);
	float3 eyeVector = center - g_sCameraPos;	
	float3 upVector = g_sElectricEndPos - g_sElectricStartPos;
	normalize(upVector);
	float3 sideVector = cross(eyeVector,upVector);
	sideVector = normalize(sideVector);

	float3 dir = mul(sideVector, 1);
	//dir += mul(upVector, lerpData.y);

	center = mul((pos0 + float4(lerpPos + randPos, pos0.w) + mul(dir, lerpData.y * g_sElectricSize)), g_sWorld).xyz;
	
	return mul(float4(center, 1), g_sViewProj);
}

float4 BoltColor_PS(float2 texC : TEXCOORD0) : COLOR
{
//	return tex2D(ArcColorSampler, texC) * 5; 
	//return float4(1.0, 1.0, 1.0, 1.0);

	return g_sElectricColor;
}

technique ElectricityTech
{
	pass p0
	{
		CullMode		= None;
		Lighting		= false;
		//ZEnable			= false;
		//ZFunc			= LessEqual;
		//ZWriteEnable	= false;
		//AlphaBlendEnable = true;
		//SrcBlend = SrcAlpha;
		//DestBlend = InvSrcAlpha;
		//VertexShader = compile vs_2_0 DrawLine_VS();
		VertexShader = compile vs_3_0 DrawBolt_VS();
		PixelShader = compile ps_3_0 BoltColor_PS();
	}
}
