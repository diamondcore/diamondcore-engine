//uniform extern float4x4	g_sWorld;
//uniform extern float4x4	g_sWVP;
uniform extern float4	g_sColor;

uniform extern texture	g_sFilterTex;	// 16 x 16 texture
sampler FilterS = sampler_state
{
	texture	= <g_sFilterTex>;
	MipFilter = None;
	MinFilter = Linear;
	MagFilter = Linear;
	AddressU = Mirror;
	AddressV = Mirror;
};

struct VS_LINE_OUT
{
	float4 pos	: POSITION;
	float4 texC	: TEXCOORD0;
};

// Anti-Aliased Lines
VS_LINE_OUT	AA_LinesVS(float3 pos0 : POSITION0, 
					   float3 pos1	: POSITION1,
					   float4 weights : TEXCOORD0, 
					   float radius : TEXCOORD1, 
					   float aspect :TEXCOORD2)
{
	VS_LINE_OUT outVS = (VS_LINE_OUT)0;

	float4 p0 = mul(float4(pos0, 1.0f), g_sWVP);
	float4 p1 = mul(float4(pos1, 1.0f), g_sWVP);

	float4 w0 = p0;
	float4 w1 = p1;
	w0.y /= aspect;
	w1.y /= aspect;

	float2 delta2 = w1.xy/w1.z-w0.xy/w0.z;
	float3 deltaP;

	deltaP.xy = delta2;
	deltaP.z = w1.z-w0.z;

	float len = length(delta2);
	float3 U = deltaP/len;

	float3 V;
	V.x = U.y;
	V.y = -U.x;
	V.z = 0;

	outVS.pos = p0*weights.x + p1*weights.y;
	
	float3 offset = U*weights.z + V*weights.w;
	offset.xy *= radius;
	offset.y *= aspect;

	outVS.pos.xy += offset * outVS.pos.z;

	outVS.texC.x = weights.z;
	outVS.texC.y = weights.w;
	outVS.texC.z = 0.0f;
	outVS.texC.w = 1.0f;

	return outVS;
}

float4	AA_LinesPS(VS_LINE_OUT inVS)	:COLOR0
{
	return float4(g_sColor * tex2Dproj(FilterS, inVS.texC));
}

technique AA_LinesTech
{
	pass p0
	{
		VertexShader	= compile vs_3_0 AA_LinesVS();
		PixelShader		= compile ps_3_0 AA_LinesPS();
		AlphaRef		= 1;
		AlphaTestEnable	= false;
		AlphaFunc		= GreaterEqual;
		
		AlphaBlendEnable = true;
		SrcBlend		= SrcAlpha;
		DestBlend		= InvSrcAlpha;
		BlendOp			= Add;
		CullMode		= None;
		Lighting		= false;
		ZEnable			= true;
		ZFunc			= LessEqual;
		ZWriteEnable	= false;
	}
}
