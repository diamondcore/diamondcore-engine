uniform extern float3	g_sCameraPosL;		// Eye Position (camera in world coordinates)
uniform extern int		g_sViewHeight;		// Height of viewport
uniform extern float3	g_sAccel;
uniform extern float	g_sTime;
uniform extern bool		f_sUseSpeed;
uniform extern float	g_sSpeed;
uniform extern float4x4	g_sView;
float g_sNear 	= 0.1f;
float g_sFar	= 1000.0f;

// Useing float4 array allows for Color in the same array as the WorldMat
uniform extern float4	g_sInstanceData[200]; 

// Texture Sample
uniform extern texture	g_sParticleTex;
sampler	ParticleS = sampler_state			
{
	Texture		= <g_sParticleTex>;		
	MinFilter	= Linear;			
	MagFilter	= Linear;
	MipFilter	= Point;
	AddressU	= Clamp;
	AddressV	= Clamp;		
};


/*********************************************************************
						Shader Output Types
*********************************************************************/
struct VS_PARTICLE						// Vertex Shader						
{
	float4 posH		: POSITION0;
	float4 fade		: COLOR0;		// Fade of the particle over time
	float2 tex0		: TEXCOORD0;	// D3D fills in for point sprites
	float  depth	: TEXCOORD1;	// Used for Soft Particle effect
	float  size		: PSIZE;		// Size in pixels
	float4 posC		: TEXCOORD2;
	float  sizeC	: TEXCOORD3;
};


/*********************************************************************
						Vertex Shader Effects
*********************************************************************/	
float4x3	Inverse(float4x3 mat)
{
	// Calculate inverse scale
	float3 invScale;
	invScale.x = dot(mat[0], mat[0]);
	invScale.y = dot(mat[1], mat[1]);
	invScale.z = dot(mat[2], mat[2]);
	invScale = 1.0f/invScale;

	// Invert Scaling
	mat[0] *= invScale.x;
	mat[1] *= invScale.y;
	mat[2] *= invScale.z;

	// Invert Rotation
	(float3x3)mat = transpose((float3x3)mat);

	// Invert Translation
	mat[3] = mul(-mat[3], (float3x3)mat);
	return mat;
}

VS_PARTICLE		SparkVS (float3 posL	 : POSITION0,
						float3 vel 	 : BLENDWEIGHT,
						float  size	 : PSIZE0,
						float  time	 : TEXCOORD0,
						float  life  : TEXCOORD1,
						float  mass	 : TEXCOORD2,
						int4  instID : BLENDINDICES,
						float4 color : COLOR0)
{
	VS_PARTICLE outVS = (VS_PARTICLE)0;	// Zero-out the output

	//--------------------------------------------------------------
	// Get the Vertex Instance Index
	int InstIndex = instID[0]; 	//((int[4])(index))[0];		

#if 0
	// For use when passing in full world matrix
	float4x4 instWorldMat;	// Build the WorldMat for this instance
	instWorldMat[0] = g_sInstanceData[InstIndex];
	instWorldMat[1] = g_sInstanceData[InstIndex + 1];
	instWorldMat[2] = g_sInstanceData[InstIndex + 2];
	instWorldMat[3] = g_sInstanceData[InstIndex + 3];

	// Get Properties of the particle
	float3 properties = g_sInstanceData[InstIndex + 4].xyz;
	bool isAlive = bool(g_sInstanceData[InstIndex + 4].w);
		time = properties.x;
		size = properties.y;
		life = properties.z;
#else
	// Get Properties of the particle
	float3 properties = g_sInstanceData[InstIndex].xyz;
	bool isAlive = bool(g_sInstanceData[InstIndex].w > 0.5f);
		time = properties.x;
		size = properties.y;
		life = properties.z;

	float4x4 instWorldMat = g_sWorld;	// Build the WorldMat for this instance
	//instWorldMat[3] = mul(float4(g_sInstanceData[InstIndex].xyz, 1.0f), instWorldMat[3]);
#endif

	//--------------------------------------------------------------

	// Get Current Particle Age
	float age = g_sTime - time;
    
	// Used to rotate particle around z-axis
	outVS.posC.xyz = normalize(vel);
	
	// Constant acceleration (Equation 19.1).
    posL = posL + vel*age + 0.5f * g_sAccel * age * age;
	
#if 0
	float4 worldPos = mul(float4(posL.xyz, 1.0f), instWorldMat);
#else
	float4 worldPos = float4(posL.xyz, 1.0f);
#endif

	// Transform to homogeneous clip space
	outVS.posH = mul(worldPos, g_sWVP);	

	// Scale up size over time to simulate flare expanding over time
	size += 2.0f*age*age;

	// Scale size with distance from camera
	float dist = distance(worldPos.xyz, g_sCameraPosL);	//camPosL);
	outVS.size = g_sViewHeight*size/(1.0f + 8.0f*dist);
	
	// Fade color through lifetime
	outVS.fade = (1.0f - (age/life));
	if(!isAlive)
		outVS.fade = float4(0,0,0,0);

	// Depth for soft particle effect
	outVS.depth = outVS.posH.z/outVS.posH.w;

	return outVS;
}

VS_PARTICLE		FireVS (float3 posL	 : POSITION0,
						float3 vel 	 : BLENDWEIGHT,
						float  size	 : PSIZE0,
						float  time	 : TEXCOORD0,
						float  life  : TEXCOORD1,
						float  mass	 : TEXCOORD2,
						int4  instID : BLENDINDICES,
						float4 color : COLOR0)
{
	VS_PARTICLE outVS = (VS_PARTICLE)0;	// Zero-out the output

	//--------------------------------------------------------------
	// Get the Vertex Instance Index
	int InstIndex = instID; 	//((int[4])(index))[0];		

#if 0
	// For use when passing in full world matrix
	float4x4 instWorldMat;	// Build the WorldMat for this instance
	instWorldMat[0] = g_sInstanceData[InstIndex];
	instWorldMat[1] = g_sInstanceData[InstIndex + 1];
	instWorldMat[2] = g_sInstanceData[InstIndex + 2];
	instWorldMat[3] = g_sInstanceData[InstIndex + 3];

	// Get Properties of the particle
	float3 properties = g_sInstanceData[InstIndex + 4].xyz;
	bool isAlive = bool(g_sInstanceData[InstIndex + 4].w);
		time = properties.x;
		size = properties.y;
		life = properties.z;
#else
	// Get Properties of the particle
	float3 properties = g_sInstanceData[InstIndex].xyz;
	bool isAlive = bool(g_sInstanceData[InstIndex].w > 0.5f);
		time = properties.x;
		size = properties.y;
		life = properties.z;

	float4x4 instWorldMat = g_sWorld;	// Build the WorldMat for this instance
	//instWorldMat[3] = mul(float4(g_sInstanceData[InstIndex].xyz, 1.0f), instWorldMat[3]);
#endif

	//--------------------------------------------------------------

	// Get Current Particle Age
	float age = g_sTime - time;

	// Rotate particle around z-axis
	float sine, cosine;
	sincos(0.5f*mass*age, sine, cosine);
	float x = posL.x*cosine + posL.y*-sine;
	float y = posL.x*sine + posL.y*cosine;

	// Oscillate particle up and down
	float s = sin(6.0f*age);
	//posL.x = x;
	//posL.y = y + mass*s;
	outVS.tex0.x = x;
	outVS.tex0.y = y + mass*s;
     // Constant acceleration (Equation 19.1).
     //posL = posL + vel*age + 0.5f * g_sAccel * age * age;
	outVS.tex0 = outVS.tex0 + vel*age + 0.5f * g_sAccel * age * age;

#if 0
	float4 worldPos = mul(float4(posL.xyz, 1.0f), instWorldMat);
#else
	float4 worldPos = float4(posL.xyz, 1.0f);
#endif

	// Transform to homogeneous clip space
	outVS.posH = mul(worldPos, g_sWVP);
	
	// Scale up size over time to simulate flare expanding over time
	size += 8.0f*age*age;

	// Scale size with distance from camera
	float dist = distance(worldPos.xyz, g_sCameraPosL);	//camPosL);
	outVS.size = g_sViewHeight*size/(1.0f + 8.0f*dist);

	// Fade color through lifetime
	outVS.fade.rgba = (1.0f - (age/life));
	if(!isAlive)
		outVS.fade = float4(0,0,0,0);

	// Depth for soft particle effect
	outVS.depth = outVS.posH.z/outVS.posH.w;

	return outVS;
}

VS_PARTICLE		SmokeVS(float3 posL	 : POSITION0,
						float3 vel 	 : BLENDWEIGHT,
						float  size	 : PSIZE0,
						float  time	 : TEXCOORD0,
						float  life  : TEXCOORD1,
						float  mass	 : TEXCOORD2,
						int4  instID : BLENDINDICES,
						float4 color : COLOR0)
{
	VS_PARTICLE outVS = (VS_PARTICLE)0;	// Zero-out the output

	//--------------------------------------------------------------
	// Get the Vertex Instance Index
	int InstIndex = instID; 	//((int[4])(index))[0];		

#if 0
	// For use when passing in full world matrix
	float4x4 instWorldMat;	// Build the WorldMat for this instance
	instWorldMat[0] = g_sInstanceData[InstIndex];
	instWorldMat[1] = g_sInstanceData[InstIndex + 1];
	instWorldMat[2] = g_sInstanceData[InstIndex + 2];
	instWorldMat[3] = g_sInstanceData[InstIndex + 3];

	// Get Properties of the particle
	float3 properties = g_sInstanceData[InstIndex + 4].xyz;
	bool isAlive = bool(g_sInstanceData[InstIndex + 4].w);
		time = properties.x;
		size = properties.y;
		life = properties.z;
#else
	// Get Properties of the particle
	float3 properties = g_sInstanceData[InstIndex].xyz;
	bool isAlive = bool(g_sInstanceData[InstIndex].w > 0.5f);
		time = properties.x;
		size = properties.y;
		life = properties.z;

	float4x4 instWorldMat = g_sWorld;	// Build the WorldMat for this instance
	//instWorldMat[3] = mul(float4(g_sInstanceData[InstIndex].xyz, 1.0f), instWorldMat[3]);
#endif

	//--------------------------------------------------------------

	// Get Current Particle Age
	float age = g_sTime - time;

	// Check if speed is a factor
	float scale = age;
	if(f_sUseSpeed)
	{
		float thrust = life - 0.9f;
		modf(thrust, scale);
		thrust -= scale;
		thrust *= 0.5f;
		scale += thrust * (g_sSpeed/100.0f) + 0.9f;
	}

	// Constant acceleration
	posL.xyz = posL.xyz + vel*scale + 0.5f*g_sAccel*scale*scale;
	
#if 0
	float4 worldPos = mul(float4(posL.xyz, 1.0f), instWorldMat);
#else
	float4 worldPos = float4(posL.xyz, 1.0f);
#endif

	// Transform to homogeneous clip space
	outVS.posH = mul(worldPos, g_sWVP);
	
	// Scale up size over time to simulate flare expanding over time
	size += 8.0f*scale*scale;

	// Scale size with distance from camera
	float dist = distance(posL.xyz, g_sCameraPosL);
	outVS.size = g_sViewHeight*size/(1.0f + 8.0f*dist);

	// Fade color through lifetime
	outVS.fade = (1.0f - (scale/life));
	if(!isAlive)
		outVS.fade = float4(0,0,0,0);

	// Depth for soft particle effect
	outVS.depth = outVS.posH.z/outVS.posH.w;	//mul(float4(outVS.posH.z/outVS.posH.w,size,0,1), g_sViewInv);
	
	outVS.posC = mul(worldPos, g_sView);
	outVS.sizeC = outVS.size;

	return outVS;
}

VS_PARTICLE		BulletVS(float3 posL  : POSITION0,
						 float3 vel   : BLENDWEIGHT,
						 float  size  : PSIZE0,
						 float  time  : TEXCOORD0,
						 float  life  : TEXCOORD1,
						 float  mass  : TEXCOORD2,
						 int4  instID : BLENDINDICES,
						 float4 color : COLOR0)
{
	VS_PARTICLE outVS = (VS_PARTICLE)0;	// Zero-out the output

	//--------------------------------------------------------------
	// Get the Vertex Instance Index
	int InstIndex = instID; 	//((int[4])(index))[0];		

#if 0
	// For use when passing in full world matrix
	float4x4 instWorldMat;	// Build the WorldMat for this instance
	instWorldMat[0] = g_sInstanceData[InstIndex];
	instWorldMat[1] = g_sInstanceData[InstIndex + 1];
	instWorldMat[2] = g_sInstanceData[InstIndex + 2];
	instWorldMat[3] = g_sInstanceData[InstIndex + 3];

	// Get Properties of the particle
	float3 properties = g_sInstanceData[InstIndex + 4].xyz;
	bool isAlive = bool(g_sInstanceData[InstIndex + 4].w);
		time = properties.x;
		size = properties.y;
		life = properties.z;
#else
	// Get Properties of the particle
	float3 properties = g_sInstanceData[InstIndex].xyz;
	bool isAlive = bool(g_sInstanceData[InstIndex].w > 0.5f);
		time = properties.x;
		size = properties.y;
		life = properties.z;

	float4x4 instWorldMat = g_sWorld;	// Build the WorldMat for this instance
	//instWorldMat[3] = mul(float4(g_sInstanceData[InstIndex].xyz, 1.0f), instWorldMat[3]);
#endif


	//---------------------------------------------------------------

	// Get Current Particle Age
	float age = g_sTime - time;

	// Constant acceleration
	posL.xyz = posL.xyz + vel*age + 0.5f*g_sAccel*age*age;

#if 0
	float4 worldPos = mul(float4(posL.xyz, 1.0f), instWorldMat);
#else
	float4 worldPos = float4(posL.xyz, 1.0f);
#endif

	// Transform to homogeneous clip space
	outVS.posH = mul(worldPos, g_sWVP);
	
	// Scale up size over time to simulate flare expanding over time
	size += 8.0f*age*age;

	// Scale size with distance from camera
	float dist = distance(worldPos.xyz, g_sCameraPosL);
	outVS.size = g_sViewHeight*size/(1.0f + 8.0f*dist);

	// Fade color through lifetime
	outVS.fade = (1.0f - (age/life));
	if(!isAlive)
		outVS.fade = float4(0,0,0,0);

	// Depth for soft particle effect
	outVS.depth = outVS.posH.z/outVS.posH.w;

	return outVS;
}

VS_PARTICLE		RainVS(float3 posL  : POSITION0,
					   float3 vel   : BLENDWEIGHT,
					   float  size  : PSIZE0,
					   float  time  : TEXCOORD0,
					   float  life  : TEXCOORD1,
					   float  mass  : TEXCOORD2,
					   int4  instID : BLENDINDICES,
					   float4 color : COLOR0)
{
	VS_PARTICLE outVS = (VS_PARTICLE)0;	// Zero-out the output

	//--------------------------------------------------------------
	// Get the Vertex Instance Index
	int InstIndex = instID; 	//((int[4])(index))[0];		

#if 0
	// For use when passing in full world matrix
	float4x4 instWorldMat;	// Build the WorldMat for this instance
	instWorldMat[0] = g_sInstanceData[InstIndex];
	instWorldMat[1] = g_sInstanceData[InstIndex + 1];
	instWorldMat[2] = g_sInstanceData[InstIndex + 2];
	instWorldMat[3] = g_sInstanceData[InstIndex + 3];

	// Get Properties of the particle
	float3 properties = g_sInstanceData[InstIndex + 4].xyz;
	bool isAlive = bool(g_sInstanceData[InstIndex + 4].w);
		time = properties.x;
		size = properties.y;
		life = properties.z;
#else
	// Get Properties of the particle
	float3 properties = g_sInstanceData[InstIndex].xyz;
	bool isAlive = bool(g_sInstanceData[InstIndex].w > 0.5f);
		time = properties.x;
		size = properties.y;
		life = properties.z;

	float4x4 instWorldMat = g_sWorld;	// Build the WorldMat for this instance
	//instWorldMat[3] = mul(float4(g_sInstanceData[InstIndex].xyz, 1.0f), instWorldMat[3]);
#endif

	//---------------------------------------------------------------

	// Get Current Particle Age
	float age = g_sTime - time;

	// Constant acceleration
	posL.xyz = posL.xyz + vel*age + 0.5f*g_sAccel*age*age;

#if 0
	float4 worldPos = mul(float4(posL.xyz, 1.0f), instWorldMat);
#else
	float4 worldPos = float4(posL.xyz, 1.0f);
#endif

	// Transform to homogeneous clip space
	outVS.posH = mul(worldPos, g_sWVP);

	// Scale size with viewport height, and not distance
	outVS.size = 0.0035f*g_sViewHeight*size;

	return outVS;
}


/*********************************************************************
						Pixel Shader Effects
*********************************************************************/
float contrast = 3.0f;
float4	SparkPS (float4 fade: COLOR0,
				float2 tex0: TEXCOORD0,
				float  depth: TEXCOORD1,
				float4 posC : TEXCOORD2)	:COLOR
{
	// Rotate the tex coordinates in the direction of the velocity
	float3 vel = posC.xyz;	// Velocity is passed in here
	float x = tex0.y*cos(vel.y) + tex0.x*-sin(vel.x+vel.z);
	float y = tex0.y*sin(vel.x+vel.z) + tex0.x*cos(vel.y);
	tex0 = float2(x, y);	// Use for orientation of texture

	// Multiply Fade * texture color
	float4 color = tex2D(ParticleS, tex0);
	if(color.a < 0.1f)
		return float4(0.0f, 0.0f, 0.0f, 0.0f);
	
	// Reducing green by 50% of red and blue to 0.0 keeps the color yellow
	fade.g = 0.5f * fade.r;
	fade.b = 0.0f;
	color *= fade;
	return color;//*fade;
}

float4	FirePS (float4 fade: COLOR0,
				float2 tex0: TEXCOORD0,
				float  depth: TEXCOORD1)	:COLOR
{
	/*float depthDif = float(tex2D(DeferredDepth,tex0).r) - depth;
	
	// Soft Particle calculation
	float x;
	if(depthDif > 0.5)
		x = 1-depthDif;
	else
		x = depthDif;
	float alphaDepth = pow(0.5*saturate(2*x),contrast);
	*/
	// Multiply Fade * texture color
	float4 color = tex2D(ParticleS, tex0);
	//color.a *= alphaDepth;
	color *= fade;
	return color;//*fade;
}

float density = -0.5f;
float Opacity(float3 camP, float3 pointCamP, float radius, float2 texC)
{
	float alpha = 0;
	float d = length(camP.xy - pointCamP.xy);
	if(d < radius)
	{
		float w = sqrt(radius*radius - d*d);
		float f = camP.z - w;
		float b = camP.z + w;
		float Zs = tex2D(DeferredDepth, texC).r*1000.0f;
		float ds = min(Zs,b) - max(g_sNear,f);
		alpha = 1-exp(-density*(1-d/radius)*ds);
	}
	return alpha;
}

float4	SmokePS(VS_PARTICLE inVS)	: COLOR
{
/*	float depthDif = float(tex2D(DeferredDepth,inVS.tex0).r)- inVS.depth;
	float x;
	if(depthDif > 0.5)
		x = 1-depthDif;
	else
		x = depthDif;
	float alphaDepth = saturate(depthDif);	//pow(0.5*saturate(2*x),contrast);
	*/
	float4 color = tex2D(ParticleS, inVS.tex0);
	//color.a = alphaDepth;	//Opacity(inVS.posC, float3(inVS.tex0, inVS.depth), inVS.sizeC, inVS.tex0);	//alphaDepth;
	color *= inVS.fade;
	//if(depthDif > 0.5)
	//	color.r = 1.0f;//alphaDepth;
	return color;//*inVS.fade;
}

float4	BulletPS(VS_PARTICLE inVS)	:COLOR
{
	return tex2D(ParticleS, inVS.tex0);
}


/*********************************************************************
						Techniques
*********************************************************************/
technique SparkTech
{
	pass p0
	{
		VertexShader 	= compile vs_3_0	SparkVS();
		PixelShader		= compile ps_3_0	SparkPS();
		
		PointSpriteEnable 	= true;
		AlphaBlendEnable	= true;
		SrcBlend			= One;
		DestBlend			= One;
		ZWriteEnable		= false;
	}
}

technique FireTech
{
	pass p0
	{
		VertexShader 	= compile vs_3_0	FireVS();
		PixelShader		= compile ps_3_0	FirePS();
		
		PointSpriteEnable 	= true;
		AlphaBlendEnable	= true;
		SrcBlend			= One;
		DestBlend			= One;
		ZWriteEnable		= false;
	}
}

technique SmokeTech
{
	pass p0
	{
		VertexShader 	= compile vs_3_0	SmokeVS();
		PixelShader		= compile ps_3_0	SmokePS();
		
		PointSpriteEnable 	= true;
		AlphaBlendEnable	= true;
		SrcBlend			= One;	//SrcAlpha;
		DestBlend			= One;	//InvSrcAlpha;
		ZWriteEnable		= false;
	}
}

technique BulletTech
{
	pass p0
	{
		VertexShader 	= compile vs_3_0	BulletVS();
		PixelShader		= compile ps_3_0	BulletPS();
		
		PointSpriteEnable 	= true;
		AlphaBlendEnable	= true;
		SrcBlend			= One;	
		DestBlend			= One;	
		ZWriteEnable		= false;
	}
}

technique RainTech
{
	pass p0
	{
		VertexShader 	= compile vs_3_0	RainVS();
		PixelShader		= compile ps_3_0	BulletPS();
		
		PointSpriteEnable 	= true;
		AlphaTestEnable		= true;
		AlphaFunc			= GreaterEqual;
		AlphaRef			= 100;
	}
}

