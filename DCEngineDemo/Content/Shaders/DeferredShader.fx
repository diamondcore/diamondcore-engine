uniform extern float4x4 g_sWVP;				// WorldViewProj Matrix
uniform extern float4x4 g_sWorldInvTrans;	// WorldInverseTranspose Matrix
uniform extern float4x4 g_sWorld;			// World Matrix
uniform extern float4x4	g_sViewProj;		// View Projection Matrix
uniform extern float4x3 g_sViewInv;			// View inverse
uniform extern float3	g_sCameraPos;		// Eye Position (camera in world coordinates)
uniform extern float4x4 g_sProj;

// Lighting
uniform	extern float3	g_sLightPosW;		// Light's position (world coordinates)
uniform extern float4	g_sAmbientLight;	// Ambient Light
uniform extern float4	g_sDiffuseLight;	// Diffuse Light
uniform extern float4	g_sSpecLight;		// Specular Light
uniform extern float3	g_sAttenuation;		// Attenuation012

// Materials
uniform extern float4	g_sAmbientMtrl;		// Ambient Material
uniform extern float4	g_sDiffuseMtrl;		// Diffuse Material
uniform extern float4	g_sSpecMtrl;		// Specular Material
uniform extern float	g_sSpecPower;		// Specular Power

uniform extern	float	g_sPhongExp;
uniform extern	float	g_sPhongCoeff;
uniform extern	float	g_sDiffuseCoeff;

uniform extern float2	g_sPixelSize;		// Size of the screen (1/width, 1/height)
uniform extern float3	g_sBrushPos;
uniform extern float	g_sBrushRadius;

// Useing float4 array allows for Color in the same array as the WorldMat
bool f_IsFog = true;
float startFog = 10;
float Shininess = 10.0f; // Higher the number, smoother the light surface 
float4 fogColor = {0.96, 0.96, 0.96, 1};
float focalPlane = 10.0f;
const float MAP_SIZE = 150.0f;	// Map max size is 150.0f 

// Textures
//uniform extern texture		g_sTexture;	
uniform extern texture2D	g_sLightMap;
uniform extern texture2D	g_sSSAOMap;
uniform extern texture2D	g_sNormalTex;	// Used for input of normals by texture
uniform extern texture2D	g_sSpecTex;		// Used for input of specular lighting (shininess) by texture

// Texture Sample
//sampler	TextureS = sampler_state			
//{
	//Texture		= <g_sTexture>;		
	//AddressU	= Wrap;
	//AddressV	= Wrap;		
	//MinFilter	= Linear;			
	//MagFilter	= Linear;
	//MipFilter	= Linear;
//};

/*********************************************************************
						Deferred Shader Data
*********************************************************************/
sampler SSAOMap = sampler_state
{
	Texture		= <g_sSSAOMap>;
	MagFilter	= Point;
	MinFilter	= Point;
	AddressU	= Clamp;
	AddressV	= Clamp;
};
/*
sampler LightMap = sampler_state
{
	Texture		= <g_sLightMap>;
	MagFilter	= Point;
	MinFilter	= Point;
};

// Post Processing
sampler PostProcess = sampler_state
{
	Texture		= <g_sDeferredMtrl>;
	MagFilter	= Linear;
	MinFilter	= Linear;
};

// Linear Depth
sampler LinearDepth = sampler_state
{
	Texture		= <g_sDeferredDepth>;
	MagFilter	= Linear;
	MinFilter	= Linear;
};*/
#include "AmbientOcclusion.fx"
uniform extern texture	g_sTexture;
sampler TextureS = sampler_state
{
	Texture		= <g_sTexture>;		
	AddressU	= Wrap;
	AddressV	= Wrap;		
	MinFilter	= Linear;			
	MagFilter	= Linear;
	MipFilter	= Linear;
};

// Normal Texture Input
sampler NormalMap = sampler_state
{
	Texture		= <g_sNormalTex>;
	//MagFilter	= Point;	//Linear;
	//MinFilter	= Point;	//Linear;
	MinFilter	= Anisotropic;//Linear;
	MagFilter	= Linear;
	//MipFilter	= Linear;//Anisotropic;
	AddressU	= Wrap;
	AddressV	= Wrap;
	MaxAnisotropy = 8;
};

// Specular Texture Input
sampler SpecMap = sampler_state
{
	Texture		= <g_sSpecTex>;
	//MagFilter	= Point;	//Linear;
	//MinFilter	= Point;	//Linear;
	MinFilter	= Anisotropic;//Linear;
	MagFilter	= Linear;
	//MipFilter	= Linear;//Anisotropic;
	AddressU	= Wrap;
	AddressV	= Wrap;
	MaxAnisotropy = 8;
};

/*********************************************************************
						Uber Shader Switches
*********************************************************************/
uniform extern bool	f_Phong;
uniform extern bool	f_BlinnPhong;
uniform extern bool f_IsTex;
uniform extern bool f_UseDOF;
uniform extern bool f_UseSSAO;
uniform extern bool f_UseNormalTex;		// If we should use g_sNormalTex to get the normal
uniform extern bool f_UseNormalTex2;
uniform extern bool f_UseNormalTex3;
uniform extern bool f_UseSpecTex;	// If we should use g_sSpecTex to get the specular color
uniform extern bool f_UseSpecTex2;
uniform extern bool f_UseSpecTex3;
uniform extern bool f_IsTerrain;
uniform extern bool f_UseBrush;
uniform extern bool f_RenderCustomTex;
//uniform extern bool f_IsFog;


//#include "HDR_Light.fx"
#include "Particles.fx"
#include "VertexBlending.fx"
#include "Instancing.fx"
#include "Terrain.fx"
#include "Decals_ProjTexturing.fx"
#include "MotionBlur.fx"
#include "Electricity.fx"
#include "HeatHaze.fx"

/*********************************************************************
						Shader Output Types
*********************************************************************/
/*struct VS_OUTPUT						// Vertex Shader						
{
	float4 posH		: POSITION0;
	float3 normW	: TEXCOORD0;
	float3 posW		: TEXCOORD1;
	float2 texC		: TEXCOORD2;
	float3 toEyeW	: TEXCOORD3;
	float  spec		: TEXCOORD4;
	float3 lightDir	: TEXCOORD5;
	float  depth	: TEXCOORD6;
	float3 posC		: TEXCOORD7;
};*/

struct PS_OUTPUT_DEF
{
	float4 mtrl		: COLOR0;
	float4 normW	: COLOR1;
	float4 posW		: COLOR2;	// (X,Y)
	float4 depth	: COLOR3;	// (Z)
};

/*********************************************************************
						Vertex Shader Effects
*********************************************************************/	
VS_OUTPUT	MainVS (float3 posL	: POSITION0,
					float3 normL : NORMAL0,
					float2 texC	: TEXCOORD0)
{
	VS_OUTPUT outVS = (VS_OUTPUT)0;	// Zero-out the output

	// Transform normal to world space
	float3 normalW = mul(float4(normL, 1.0f), g_sWorldInvTrans).xyz;
	outVS.normW = normalize(normalW);

	// Transform vertex position to world space
	outVS.posW = mul(float4(posL, 1.0f), g_sWorld).xyz;

	// Transform to homogeneous clip space
	outVS.posH = mul(float4(posL, 1.0f), g_sWVP);

	// Vector to eye
	outVS.toEyeW = g_sCameraPos - outVS.posW;

	outVS.texC = texC;	// Pass texture for rasterization

	// Uber Code Here


	return outVS;
}


VS_OUTPUT	DeferredVS (float3 posL		: POSITION0,
						float3 normL	: NORMAL0,
						float2 texC		: TEXCOORD0,
						float3 tang		: TANGENT0,
						float3 bin		: BINORMAL0,
						// Animation Variables
						float4 weights	: BLENDWEIGHT0,
						int4   boneIndex: BLENDINDICES0)
{
	VS_OUTPUT outVS = (VS_OUTPUT)0;

	if(g_sUseVBlend)	// Check if Animated vertex blending is on
	{
		outVS = VBlend(posL, normL, weights, boneIndex);
	}
	else
	{
		outVS.posW = mul(float4(posL, 1.0), g_sWorld);

		outVS.normW = normalize(mul(float4(normL, 0.0), g_sView));//g_sWorld));

		outVS.posH = mul(float4(posL, 1.0), g_sWVP);	

		// Calculate Light Shadow
		//if(f_UseLightShadow)
		//	outVS.projTex = GetLightShadowVS(posL);
	}
	outVS.texC = texC;
	
	if(f_IsTerrain)	
		outVS.tiledC = texC * 16.0f;

	// Move Tangent Coord to World Space
	outVS.tang  = normalize(mul(float4(tang,0.0), g_sView));//g_sWorld));
	//outVS.bin   = normalize(mul(float4(bin,0.0), g_sWorld));
    outVS.tang = normalize( outVS.tang - dot(outVS.tang, outVS.normW)*outVS.normW);
    outVS.bin  = cross(outVS.normW, outVS.tang); 

	// Temporarily store world normal in unused attributes
	outVS.toEyeW.xyz = normalize(mul(float4(normL, 0.0), g_sWorld));
	// Move Tangent Coord to World Space
	outVS.projTex.xyz  = normalize(mul(float4(tang,0.0), g_sWorld));
    outVS.projTex.xyz  = normalize( outVS.projTex.xyz - dot(outVS.projTex.xyz, outVS.toEyeW.xyz)*outVS.toEyeW.xyz);
	// Create binormal
	outVS.posC.xyz  = cross(outVS.toEyeW.xyz, outVS.projTex.xyz); 

/*	float3 tangW = normalize(mul(float4(tang, 0.0), g_sWorld));
	float3 T = normalize(tangW - dot(tangW, outVS.normW)*outVS.normW);
	float3 B = cross(outVS.normW, T);
	outVS.tang	= tangW;//T;
	outVS.bin	= B;//normalize(B);
*/	//outVS.tang	= T;
	//outVS.bin	= normalize(B);

/*	float3x3 TBN;
	TBN[0] = tang;
	TBN[1] = bin;
	TBN[2] = outVS.normW;
	float3x3 tts = transpose(TBN);

	// Calculate Light
	float3 lightDir = g_sLightPosW - outVS.posW;
	lightDir = normalize(mul(float4(lightDir, 0.0f), g_sWorldInvTrans));
	lightDir = mul(lightDir, tts);
	outVS.lightDir = lightDir;
	
	// Eye direction
	//float3 eyeDir = mul(g_sCameraPos, g_sWorldInvTrans);
	//eyeDir = normalize(eyeDir-posL);
	outVS.toEyeW = g_sCameraPos - outVS.posW;//mul(eyeDir, tts);

	// Specularity
	float r = reflect(lightDir, mul(outVS.normW, TBN));
	float s = dot(r, outVS.toEyeW);
	outVS.spec = max(s, 0);
*/
	float4 posC = mul(float4(posL,1.0),g_sView);	//mul(float4(posL,1.0), g_sWVP);
	outVS.depth = posC.zw;
	//float3 camPosW = mul(g_sCameraPos, g_sWorld);
	//float distFog = distance(posL.xy, camPosW.xy);
	//outVS.fog = saturate(exp((startFog-distFog)*0.33));

	return outVS;
}


/*********************************************************************
						Pixel Shader Effects
*********************************************************************/
float dofScale = 0.00025f;//14f;
float numTaps = 25.0f;
float2 taps[25] =	// random offset vectors (2D in screen space)
{
	{-0.946045,	0},
	{0.730225,	0},
	{-1.65112,	0},
	{0.255615,	0},
	{-1.27808,	0},
	{0.390381,	0},
	{-1.9519,	0},
	{1.75952,	0},
	{-0.797607,	0},
	{1.98804,	0},
	{-1.94019,	0},
	{1.70093,	0},
	{0, -0.504639},
	{0, 0.523193},
	{0, -0.615967},
	{0, 1.07983},
	{0, -1.39917},
	{0, 0.99585},
	{0, -0.979248},
	{0, 0.89624},
	{0, -0.481201},
	{0, 0.406006},
	{0,	-0.0300293},
	{0,	0.150146},
	{-0.750732,	0}
};
float4	DepthOfFieldPS (float2 texC	:TEXCOORD0)	:COLOR0
{
	if(!f_UseDOF)
	{
		//////////////Do Not Use DOF//////////////
		float4 cheap = tex2D(DeferredMtrl, texC);
		if(f_UseSSAO)	// Check if SSAO is enabled
			cheap.rgb *= tex2D(SSAOMap, texC).r;
		return cheap;
	}
	
	//////////////Using DOF//////////////
	texC += g_sPixelSize * 0.5f;

	float epsilon = 0.00005f;
	// The focal distance is the average distance in the center of the screen
	float focusDist = tex2D(DeferredDepth, float2(0.5,0.5)).r*1000.0f + epsilon;
	focusDist += tex2D(DeferredDepth, float2(0.5,0.5) + 4 * g_sPixelSize).r*1000.0f + epsilon;
	focusDist += tex2D(DeferredDepth, float2(0.5,0.5) - 4 * g_sPixelSize).r*1000.0f + epsilon;
	focusDist += tex2D(DeferredDepth, float2(0.5,0.5) + 4 * float2(g_sPixelSize.x, -g_sPixelSize.y)).r*1000.0f + epsilon;
	focusDist += tex2D(DeferredDepth, float2(0.5,0.5) + 4 * float2(-g_sPixelSize.x, g_sPixelSize.y)).r*1000.0f + epsilon;
	focusDist /= 5.0f;
	//focusDist = focalPlane;

	float4 color = tex2D(DeferredMtrl, texC);
	if(f_UseSSAO)	// Check if SSAO is enabled
		color.rgb *= tex2D(SSAOMap, texC).r;
	float  depth = tex2D(DeferredDepth, texC).r*1000.0f + epsilon;
	float CoC = abs(depth - focusDist)* dofScale;

	// Mark the focus distance pixel on the screen
	//if(any(texC.xy == float2(float2(0.5,0.5) + 2 * g_sPixelSize).xy) ||
	//	any(texC.xy == float2(float2(0.5,0.5) - 2 * g_sPixelSize).xy) ||
	//	any(texC.xy == float2(float2(0.5,0.5) + 2 * float2(g_sPixelSize.x, -g_sPixelSize.y)).xy) ||
	//	any(texC.xy == float2(float2(0.5,0.5) + 2 * float2(-g_sPixelSize.x, g_sPixelSize.y)).xy))
	//		return float4(depth, 0.0f, 0.0f, 1.0f);

	[unroll]for(int i = 0; i < numTaps; i++)
	{
		float2 tapCoord = texC + taps[i].xy * CoC;
		float4 sample = tex2D(DeferredMtrl, tapCoord);
		if(f_UseSSAO)	// Check if SSAO is enabled
			sample.rgb *= tex2D(SSAOMap, tapCoord).r;
		color += sample;
	}
	return color /(numTaps + 1);
}

float4	Blinn(float3 N, float3 P, float4 texColor, float shininess)			// Blinn Phong
{
	float3 toEye = normalize(g_sCameraPos - P);

	float3 lightDir = normalize(g_sLightPosW - P);
	
	// Blinn Phong calculation
	float3 h = normalize(lightDir + normalize(toEye));
	float t = pow(max(dot(h, N), 0.0f), shininess);
	
	float s = max(dot(lightDir, N), 0.0f);

	float3 spec = t * (g_sSpecMtrl * g_sSpecLight).rgb;
	float3 diffuse = s * (g_sDiffuseMtrl * texColor * g_sDiffuseLight).rgb;
	float3 ambient = (g_sAmbientMtrl * g_sAmbientLight);

	float d = distance(g_sLightPosW, P);
	float a = g_sAttenuation.x + g_sAttenuation.y*d + g_sAttenuation.z*d*d;

	return float4(ambient + (diffuse + spec)/a, g_sDiffuseMtrl.a);
}

// Blinn lighting, using the lit function
half4 blinn2(half3 N, half3 L, half3 V, 
			 half4 diffuse, half4 spec, half shininess)
{
	half3 H = normalize(V+L);
	half4 lighting = lit(dot(L,N), dot(H,N), shininess);
	return diffuse* lighting.y + spec*lighting.z;
}

float4	DeferredLight(float2 texC	: TEXCOORD0)	: COLOR0
{
/*	float3 diffuseColor = tex2D(DeferredMtrl, texC).rgb;
	float  spec	= abs(tex2D(DeferredMtrl, texC).a);
	float3 specColor = g_sSpecPower * g_sSpecLight * max(pow(spec, Shininess), 0);

	float4 normW = tex2D(DeferredNormW, texC);
	half shade = normW.a;

	float2 norm = (normW.xy*2.0f)-1.0f; //-0.5)*2.0f;
	float2 sct;
	sincos(norm.x*PI, sct.x, sct.y);
	float2 scphi = float2(sqrt(1.0 - norm.y*norm.y), norm.y);
	float3 normal = float3(sct.y*scphi.x, sct.x*scphi.x, scphi.y); 

	float3 N = tex2D(DeferredDepth, texC).r*1000.0f;
	float3 T = cross(N, float3(0.0f, 1.0f, 0.0f));
	float3 B = cross(N, T);
	float3x3 TBN = float3x3(T,B,N);
	float3x3 tts = transpose(TBN);

	float3 posW;
	posW.xyz = (tex2D(DeferredPosW, texC).rgb)*MAP_SIZE;	//*2.0f - 1.0f; //.xy;
	//posW.z 	= sqrt(1-dot(posW.xy, posW.xy));

	// Calculate Light
	float3 lightDir = g_sLightPosW - posW;
	//lightDir = normalize(mul(float4(lightDir, 0.0f), g_sWorldInvTrans));
	//lightDir = mul(lightDir, tts);

	//float3 lightDir = normalize(g_sLightPosW - posW);
	//float3 toEye = normalize(g_sViewInv[3].xyz - posW);
	float3 toEye = normalize(g_sCameraPos - posW);
	float3 diffuseIntensity = dot(lightDir, normal);
	float3 specIntensity = pow(max(0, dot(toEye, reflect(-lightDir, normal))), g_sSpecPower);

	//float4 color;
	//float3 ambient = g_sAmbientLight * g_sAmbientMtrl;
	//color.rgb = (diffuseColor + specColor)* shade + ambient;
	//color.a	= 1.0;

	float4 color;
	color.rgb = diffuseIntensity * g_sDiffuseLight.xyz * diffuseColor +
				specIntensity * g_sSpecLight.xyz * specColor;
	color.a	= 1.0;

	//if(f_IsFog)
	//	color.rgb += fogColor.rgb * posW.z;	// Fog
	//else
	//	color.rgb += texC.rgr * 0.5;

	return color; */
	float4 color = tex2D(DeferredMtrl, texC);

	return float4(tex2D(DeferredMtrl, texC));//color;
}


PS_OUTPUT_DEF	DeferredPS (VS_OUTPUT outVS)
{
	PS_OUTPUT_DEF outPS = (PS_OUTPUT_DEF)0;

	// Material
	if(!f_UseLightShadow)
	{
		float4 texColor;
		texColor.a = 0.0f;
		if(f_IsTerrain)	
		{
			if(texColor.a <= 0.5f)	// Check if texColor is already set 
			{
				// Layered textures
				float3 c0 = tex2D(MultiS0, outVS.tiledC).rgb;
				float3 c1 = tex2D(MultiS1, outVS.tiledC).rgb;
				float3 c2 = tex2D(MultiS2, outVS.tiledC).rgb;

				// Blend Map showing distribution between textures
				float3 B = tex2D(BlendMapS, outVS.texC).rgb;	
			
				// Scale the total color to the range [0, 1]
				float totalInverse = 1.0f/(B.r + B.g + B.b);

				c0 *= B.r * totalInverse;	// Red (texture 0)
				c1 *= B.g * totalInverse;	// Green (texture 1)
				c2 *= B.b * totalInverse;	// Blue  (texture 2)
			
				// Sum the colors and modulate with the lighting color
				texColor.rgb = (c0 + c1 + c2);
				texColor.a = 1.0f;
				
				// Add custom texture color
				if(f_RenderCustomTex)
				{
					texColor.rgb *= 0.5f;	// Reduce the main textue by half during this mode
					texColor.rgb += tex2D(CustomMapS, outVS.texC).rgb * 0.5f;
				}
				
				// Terrain Specular Map
				float s0, s1, s2;
				float sa0, sa1, sa2;
				if(f_UseSpecTex)
				{
					float4 sC = tex2D(SpecMap,  outVS.tiledC);
					s0 	= sC.r;
					sa0 = sC.a;
				}
				else
				{
					s0 = g_sPhongCoeff; //Exp/255.0f;
					sa0 = g_sPhongExp/255.0f;
				}
				if(f_UseSpecTex2)
				{
					float4 sC = tex2D(SpecMap2, outVS.tiledC);
					s1 	= sC.r;
					sa1 = sC.a;
				}
				else
				{
					s1 = g_sPhongCoeff; //Exp/255.0f;
					sa1 = g_sPhongExp/255.0f;
				}
				if(f_UseSpecTex3)
				{
					float4 sC = tex2D(SpecMap3, outVS.tiledC);
					s2 	= sC.r;
					sa2 = sC.a;
				}
				else
				{
					s2 = g_sPhongCoeff; //Exp/255.0f;
					sa2 = g_sPhongExp/255.0f;
				}
				s0 *= B.r * totalInverse;	// Red (texture 0)
				s1 *= B.g * totalInverse;	// Green (texture 1)
				s2 *= B.b * totalInverse;	// Blue  (texture 2)
				
				sa0 *= B.r * totalInverse;	// Red (texture 0)
				sa1 *= B.g * totalInverse;	// Green (texture 1)
				sa2 *= B.b * totalInverse;	// Blue  (texture 2)
				outPS.normW.ab	= float2((s0+s1+s2), (sa0 + sa1 + sa2)/*, g_sPhongExp/255.0f*/);

				// Terrain Normal Maps
				float4 n0, n1, n2;
				if(f_UseNormalTex)
				{
					n0 = (2.0f * tex2D(NormalMap,  outVS.tiledC) - 1.0f);
					//n0.z = sqrt(saturate(1 - dot(n0.xy, n0.xy)));  
					//n0 = normalize(n0)* 2;
				}
				else
					n0 = float4(0,0,0,0);
				if(f_UseNormalTex2)
				{
					n1 = (2.0f * tex2D(NormalMap2, outVS.tiledC) - 1.0f);
					//n1.z = sqrt(saturate(1 - dot(n1.xy, n1.xy)));  
					//n1 = normalize(n1)* 2;
				}
				else
					n1 = float4(0,0,0,0);
				if(f_UseNormalTex3)
				{
					n2 = (2.0f * tex2D(NormalMap3, outVS.tiledC) - 1.0f);
					//n2.z = sqrt(saturate(1 - dot(n2.xy, n2.xy)));  
					//n2 = normalize(n2)* 2;
				}
				else
					n2 = float4(0,0,0,0);
				n0 *= B.r * totalInverse;	// Red (texture 0)
				n1 *= B.g * totalInverse;	// Green (texture 1)
				n2 *= B.b * totalInverse;	// Blue  (texture 2)
				float4 bumpMap = (n0 + n1 + n2);
				//bumpMap.z = sqrt(1.0f - (bumpMap.x*bumpMap.x + bumpMap.y*bumpMap.y));
				bumpMap.z = sqrt(saturate(1 - dot(bumpMap.xy, bumpMap.xy)));  
			// //bumpMap.xyz = normalize(bumpMap.xyz)*2;
				// Transform Tangent Normals to World Space
				//outVS.tang = normalize(outVS.tang - dot(outVS.tang, outVS.normW) * outVS.normW);
				float3x3 TBN = (float3x3(outVS.tang, outVS.bin, outVS.normW));
				float3 bumpVec = normalize(mul(bumpMap.xyz, TBN));
				//float3 bumpVec = outVS.normW + bumpMap.x * outVS.tang + bumpMap.y * outVS.bin;
				//bumpVec.z = sqrt(1.0f - (bumpVec.x*bumpVec.x + bumpVec.y*bumpVec.y));
				//bumpVec = normalize(bumpVec);

				// Compress Normal into buffer
				outPS.normW.rg	= (float2(atan2(bumpVec.y, bumpVec.x)/PI, bumpVec.z)+1.0) *0.5f;


				// Save World Normal (Temporary hack, storing in extra spaces)
				TBN = (float3x3(outVS.projTex.xyz, outVS.posC.xyz, outVS.toEyeW.xyz));
				bumpVec = normalize(mul(bumpMap.xyz, TBN));

				// Compress Normal into buffer
				outPS.posW.rg	= (float2(atan2(bumpVec.y, bumpVec.x)/PI, bumpVec.z)+1.0) *0.5f;
			}
			// Check if terrain is inside the brush
			if(f_UseBrush)
			{
				float3 diff = (g_sBrushPos) - outVS.posW;
				float dist = g_sBrushRadius - sqrt(diff.x*diff.x + diff.z*diff.z);
				if(dist > 0.0f && dist < 0.5f)
					texColor = float4(1.0f, 0.0f, 0.0f, 1.0f);
			}
		}
		else
			texColor = tex2D(TextureS, outVS.texC);	// Diffuse

		//texColor.rgb = float3(0.7f, 0.7f, 0.7f);
		outPS.mtrl.rgb	= texColor.rgb*g_sDiffuseMtrl.rgb;	// Will color the roads(temp)
		outPS.mtrl.a	= g_sDiffuseMtrl.a;	//outVS.spec; 
	}
/*	else	// Using the Light Shadow Map
	{
		// Calculate Light Shadow
		float4 color = LightShadowPS(outVS.posW, outVS.normW,
						outVS.toEyeW, outVS.texC, outVS.projTex);
		outPS.mtrl = color;
	}*/

	// Calculate pixel light
	//float dist = distance(outVS.posW, g_sLightPosW);
	//float power = 1.0f - (dist/1000.0f); // (dist/MaxLightRange)
	//float shade = dot(outVS.normW, outVS.lightDir);
	
	if(f_UseSpecTex && !f_IsTerrain)
	{
		// Save Specular (shininess) too the last element in the position buffer
		float4 spec = tex2D(SpecMap, outVS.texC);
		outPS.normW.ab	= float2(spec.r, spec.a/*g_sPhongExp/255.0f*/);//g_sPhongCoeff);
	}
	else if(!f_IsTerrain)
	{
		outPS.normW.ba	= float2(g_sPhongExp/255.0f, g_sPhongCoeff);
	}

	// Calculate Normal
	if(f_UseNormalTex && !f_IsTerrain)
	{
		// Get Normals from texture
		float4 bumpMap = tex2D(NormalMap, outVS.texC)*2.0f - 1.0f;
		//bumpMap.xyz = normalize(bumpMap.xyz);
		//bumpMap.xyz = normalize(float3(2.0f, 2.0f, 1.0f)*bumpMap.xyz - float3(1.0f, 1.0f, 0.0f));
		//bumpMap = (2.0f * bumpMap.xyz - 1.0f);
		//bumpMap.z = sqrt(1.0f - (bumpMap.x*bumpMap.x + bumpMap.y*bumpMap.y));
		//bumpMap.xyz = normalize(bumpMap.xyz);

		bumpMap.z = sqrt(saturate(1 - dot(bumpMap.xy, bumpMap.xy)));
		bumpMap.xyz = normalize(bumpMap.xyz);

		// Transform Tangent Normals to World Space
/*		float3x3 TBN = float3x3(outVS.tang, outVS.bin, outVS.normW);
		bumpVec = mul(bumpVec, TBN);
		bumpVec = normalize(bumpVec);
*/
		float3x3 TBN = (float3x3(outVS.tang, outVS.bin, outVS.normW));
		float3 bumpVec = normalize(mul(bumpMap.xyz, TBN));
		//float3 bumpVec = outVS.normW + bumpMap.x * outVS.tang + bumpMap.y * outVS.bin;
		//bumpVec.z = sqrt(1.0f - (bumpVec.x*bumpVec.x + bumpVec.y*bumpVec.y));
		//bumpVec = normalize(bumpVec);

		// Compress Normal into buffer
		outPS.normW.rg	= (float2(atan2(bumpVec.y, bumpVec.x)/PI, bumpVec.z)+1.0) *0.5f;

		// Save World Normal (Temporary hack, storing in extra spaces)
		TBN = (float3x3(outVS.projTex.xyz, outVS.posC.xyz, outVS.toEyeW.xyz));
		bumpVec = normalize(mul(bumpMap.xyz, TBN));

		// Compress Normal into buffer
		outPS.posW.rg	= (float2(atan2(bumpVec.y, bumpVec.x)/PI, bumpVec.z)+1.0) *0.5f;
	}
	else if(!f_IsTerrain)
	{
		// Use normals from vertices
		//outPS.normW.rb = float2(outVS.normW.xy)*0.5f;
		outPS.normW.rg	= (float2(atan2(outVS.normW.y, outVS.normW.x)/PI, outVS.normW.z)+1.0) *0.5f;

		// Save World Normal (Temporary hack, storing in extra spaces)
		outPS.posW.rg	= (float2(atan2(outVS.toEyeW.y, outVS.toEyeW.x)/PI, outVS.toEyeW.z)+1.0) *0.5f;
	}
	// Store the normal in spherical coordinates
	//outPS.normW.rg	= (float2(atan2(outVS.normW.y, outVS.normW.x)/PI, outVS.normW.z)+1.0) *0.5f;
	
	//outPS.normW.ba	= float2(g_sPhongExp/100.0f, g_sPhongCoeff);
	//outPS.normW.b	= 0.0f;	//half(shade);
	//outPS.normW.a	= 0.0f;


	// Position
	//outPS.posW.xyz = (float3(outVS.posW.xyz/255.0f)+1.0f)*0.5f;// /MAP_SIZE);	//, 0);
	outPS.posW.z = g_sDiffuseCoeff;
	//outPS.posW.a = 1.0f;

	// Depth
	//outPS.depth	= outVS.posC.z/1000.0f;	//normalize(outVS.normW).xy, outVS.depth); // float4(outVS.posW.z*0.5 + 0.5f, 0, 0, 0);
	float d = outVS.depth.x/outVS.depth.y;
	outPS.depth = d;//float4(d, d, d, 1.0f); // z/w
	
	return outPS;
}


/*********************************************************************
						Techniques
*********************************************************************/
technique MainTech
{
	pass p0
	{
		VertexShader	= compile vs_3_0	DeferredVS();
		PixelShader		= compile ps_3_0	DeferredPS();
		CullMode		= ccw;
		//FillMode		= Wireframe;
		FillMode		= Solid;
		ZEnable			= true;
		ZWriteEnable	= true;
		ZFunc			= less;
		StencilEnable	= false;
		AlphaBlendEnable = false;
		AlphaTestEnable	= false;
		//ColorWriteEnable = red | green | blue;
	}
}
technique DefLightTech
{
	pass p0
	{
		VertexShader	= NULL;	
		PixelShader		= compile ps_3_0	DeferredLight();
		//CullMode		= none;
		FillMode		= Solid;
		ZEnable			= false;
		StencilEnable	= false;
		AlphaBlendEnable = true;
		Srcblend		= One;
		Destblend		= One;
		AlphaTestEnable	= false;
		//ColorWriteEnable = red | green | blue;
	}
}
technique DofTech
{
	pass p0
	{
		VertexShader	= NULL;
		PixelShader		= compile ps_3_0	DepthOfFieldPS();
		//CullMode		= none;
		FillMode		= Solid;
		ZEnable			= false;
		StencilEnable	= false;
		//AlphaBlendEnable = true;
		//Srcblend		= One;
		//Destblend		= One;
		//AlphaTestEnable	= false;
		//ColorWriteEnable = red | green | blue;
	}
}
