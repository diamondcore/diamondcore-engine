// Animation
uniform extern float4x4	g_sFinalForms[42];	// Holds the bone transformation matrices
uniform extern bool		g_sUseVBlend;		// Use Vertex Blending?

//#include "ShadowMap.fx"
uniform extern bool f_UseLightShadow;

struct VS_OUTPUT						// Vertex Shader						
{
	float4 posH		: POSITION0;
	float3 normW	: TEXCOORD0;
	float3 posW		: TEXCOORD1;
	float2 texC		: TEXCOORD2;
	float3 toEyeW	: TEXCOORD3;
	float2 tiledC	: TEXCOORD4;
	float3 tang		: TEXCOORD5;
	float3 bin		: TEXCOORD9;
	//float3 lightDir	: TEXCOORD5;
	float2 depth	: TEXCOORD6;
	float4 posC		: TEXCOORD7;
	float4 projTex	: TEXCOORD8;
};

/*********************************************************************
					Vertex Shader Utility Function
*********************************************************************/	
VS_OUTPUT VBlend (float3 posL, float3 normL, float4 weights, int4 boneIndex)
{
	VS_OUTPUT outVS = (VS_OUTPUT)0;	// Zero out the output
	
	float weight0 = weights.x;
	float weight1 = weights.y;
	float weight2 = weights.z;
	float weight3 = 1.0f - (weight0 + weight1 + weight2);

	// Blend the Weight for 4 bone's vertex position
	float4 p = weight0 * mul(float4(posL, 1.0f), g_sFinalForms[boneIndex[0]]);
	p += weight1 * mul(float4(posL, 1.0f), g_sFinalForms[boneIndex[1]]);
	p += weight2 * mul(float4(posL, 1.0f), g_sFinalForms[boneIndex[2]]);
	p += weight3 * mul(float4(posL, 1.0f), g_sFinalForms[boneIndex[3]]);
	p.w = 1.0f;

	// Blend the Weight for 4 bone's normal
	float4 n = weight0 * mul(float4(normL, 0.0f), g_sFinalForms[boneIndex[0]]);
	n += weight1 * mul(float4(normL, 0.0f), g_sFinalForms[boneIndex[1]]);
	n += weight2 * mul(float4(normL, 0.0f), g_sFinalForms[boneIndex[2]]);
	n += weight3 * mul(float4(normL, 0.0f), g_sFinalForms[boneIndex[3]]);
	n.w = 0.0f;

	outVS.normW = mul(n, g_sWorld).xyz;	// Transform normal to World Space

	outVS.posW = mul(p, g_sWorld).xyz;	// Transform vertex position to World Space
	//outVS.toEyeW = g_sCameraPos - posW;

	outVS.posH = mul(p, g_sWVP);	// Transform to homogenous Space

	//outVS.texC = texC;	// Pass texture to PS

	// Calculate Light Shadow
	//if(f_UseLightShadow)
	//	outVS.projTex = GetLightShadowVS(p);

	return outVS;
}
