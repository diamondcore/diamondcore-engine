//uniform extern float4x4 g_sWVP;				// WorldViewProj Matrix

// Textures
uniform extern texture	g_sEnvMap;			// Enviornment texture map

// EnvMap texture Sample
sampler	EnvMapS = sampler_state			
{
	Texture		= <g_sEnvMap>;				
	MinFilter	= Linear;			
	MagFilter	= Linear;
	MipFilter	= Linear;
	AddressU	= MIRROR;
	AddressV	= MIRROR;
	AddressW	= MIRROR;
};


/*********************************************************************
						Shader Output Types
*********************************************************************/
struct VS_SKYBOX												// SkyBox Vertex Shader
{
	float4 posH		: POSITION0;
	float3 envTex	: TEXCOORD0;
};


/*********************************************************************
						Vertex Shader Effects
*********************************************************************/	
VS_SKYBOX	SkyBoxVS (float3 posL	: POSITION0)		// Sky Box
{
	VS_SKYBOX skyVS = (VS_SKYBOX)0;	// Zero out the output

	// Need to set z = w, because z/w = 1
	skyVS.posH = mul(float4(posL, 1.0f), g_sWVP).xyww;

	skyVS.envTex = posL;
	
	return skyVS;
}


/*********************************************************************
						Pixel Shader Effects
*********************************************************************/
float4	SkyBoxPS (float3 envTex	: TEXCOORD0)	: COLOR		// Draw the Sky Box
{
	return texCUBE(EnvMapS, envTex);
}


/*********************************************************************
						Techniques
*********************************************************************/
technique SkyBoxTech							// Display the SkyBox
{
	pass p0
	{
		VertexShader	= compile vs_3_0	SkyBoxVS();
		PixelShader		= compile ps_3_0	SkyBoxPS();

		CullMode		= None;
		ZFunc			= Always;	// Always draw sky
		
		// Clear the Back Buffer, Depth Buffer
		// No longer need m_pD3DDevice->Clear();
		StencilEnable	= true;
		StencilFunc		= Always;	
		StencilPass		= Replace;
		StencilRef		= 0;		// Clear the Stencil Buffer
	}
}
