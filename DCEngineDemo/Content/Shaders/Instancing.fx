uniform extern float3	g_sImposterPos;		// Imposter Position
uniform extern bool		g_sDrawImposter;	// true if texture needs to be drawn

// Useing float4 array allows for Color in the same array as the WorldMat
uniform extern float4	g_sInstancingData[220]; // Max Constant Registers for shaders 3.0 is 224 (float4 is a shader)


/*********************************************************************
						Shader Output Types
*********************************************************************/
struct VS_INST_OUTPUT						// Vertex Shader						
{
	float4 posH		: POSITION0;
	float3 normW	: TEXCOORD1;
	float2 tex0		: TEXCOORD0;
	int   vID		: BLENDINDICES0;
	float4 color	: COLOR0;
};


/*********************************************************************
						Vertex Shader Effects
*********************************************************************/	
VS_INST_OUTPUT	InstancingVS(float3 posL	: POSITION0,
					     float3 normL	: NORMAL0,
					     float2 texC	: TEXCOORD0,
						 int	index	: BLENDINDICES0)
{
	VS_INST_OUTPUT outVS = (VS_INST_OUTPUT)0;	// Zero out the output
	
	// Get the Vertex Instance Index
	int InstIndex = index; 	//((int[4])(index))[0];	
	outVS.vID = InstIndex;	// Store Instance Index ID
	
	float4x4 instWorldMat;	// Build the WorldMat for this instance
	instWorldMat[0] = g_sInstancingData[InstIndex];
	instWorldMat[1] = g_sInstancingData[InstIndex + 1];
	instWorldMat[2] = g_sInstancingData[InstIndex + 2];
	instWorldMat[3] = g_sInstancingData[InstIndex + 3];

	float4 worldPos = mul(float4(posL, 1.0f), instWorldMat);
	float3 worldNor = mul(normL, (float3x3)(instWorldMat)).xyz;	// WorldInvTrans

//------------Imposter Start-------------------
	if(g_sDrawImposter)
	{
		//float3 norm = float3(g_sImposterPos.xy, 10.0f); // was 10.0f
		float3 norm = worldNor; //normalize(worldNor);

		float3 look = normalize(norm - g_sCameraPos);
		float3 right = normalize(cross(float3(0.0f, 1.0f, 0.0f), look));
		float3 up = cross(look, right);

		float4x4 lookAtMat;
		lookAtMat[0] = float4(right, 0.0f);
		lookAtMat[1] = float4(up, 0.0f);
		lookAtMat[2] = float4(look, 0.0f);
		lookAtMat[3] = float4(worldPos.xyz, 1.0f);

		float4 posW = mul(float4(posL, 1.0f), lookAtMat);

		outVS.posH = mul(posW, g_sWVP);
		//float4 posW = mul(worldPos, lookAtMat);
		outVS.normW = mul(worldNor, g_sWVP);
//------------Imposter End---------------------
	}
	else
	{
		outVS.normW = mul(worldNor, g_sWVP);
		outVS.posH = mul(worldPos, g_sWVP);
	}
	outVS.tex0 = texC;	
	outVS.color = g_sInstancingData[InstIndex + 4];	// Color of this instance

	return outVS;
}

/*********************************************************************
						Pixel Shader Effects
*********************************************************************/
float4	TexturePS (float4 color : COLOR0,
				   float2 texC		: TEXCOORD0)	: COLOR
{
	return color;//tex2D(TextureS, texC) *color;
}


/*********************************************************************
						Techniques
*********************************************************************/
technique ImposterTech
{
	pass p0
	{
		VertexShader	= compile vs_3_0	InstancingVS();
		PixelShader		= compile ps_3_0	TexturePS();

		FillMode		= Solid;
		ZEnable			= true;

		AlphaBlendEnable = true;
		DestBlend		= InvSrcAlpha;
	}
}
