uniform extern float4x4 g_sWVP;				// WorldViewProj Matrix
uniform extern float4x4 g_sWorldInvTrans;	// WorldInverseTranspose Matrix
uniform extern float4x4 g_sWorld;			// World Matrix
uniform extern float4x4	g_sViewProj;		// View Projection Matrix
uniform extern float4x3 g_sViewInv;			// View inverse
uniform extern float3	g_sCameraPos;		// Eye Position (camera in world coordinates)
uniform extern float3	g_sViewDir;			// Camera view direction
float g_sNear 	= 0.1f;
float g_sFar	= 1000.0f;

// Texture Sample
//sampler	TextureS = sampler_state			
//{
	//Texture		= <g_sTexture>;		
	//AddressU	= Wrap;
	//AddressV	= Wrap;		
	//MinFilter	= Linear;			
	//MagFilter	= Linear;
	//MipFilter	= Linear;
//};
sampler DeferredDepth;

/*********************************************************************
						Shader Output Types
*********************************************************************/
struct VS_PARTICLE						// Vertex Shader						
{
	float4 posH		: POSITION0;
	float3 posW		: TEXCOORD0;
	float2 texC		: TEXCOORD1;
	float3 toEyeW	: TEXCOORD2;
	float  depth	: TEXCOORD3;
	float  size		: TEXCOORD4;
	float3 normW	: TEXCOORD5;
};


/*********************************************************************
						Vertex Shader Effects
*********************************************************************/	
VS_PARTICLE	ParticleVS (float3 posL	: POSITION0,
						float3 normL : NORMAL0,
						float2 texC	: TEXCOORD0)
{
	VS_PARTICLE outVS = (VS_PARTICLE)0;	// Zero-out the output

	// Transform normal to world space
	float3 normalW = mul(float4(normL, 1.0f), g_sWorldInvTrans).xyz;
	outVS.normW = normalize(normalW);

	// Transform vertex position to world space
	outVS.posW = mul(float4(posL, 1.0f), g_sWorld).xyz;

	// Transform to homogeneous clip space
	outVS.posH = mul(float4(posL, 1.0f), g_sWVP);

	// Vector to eye
	outVS.toEyeW = g_sCameraPos - outVS.posW;

	outVS.texC = texC;	// Pass texture for rasterization

	// Uber Code Here


	return outVS;
}


/*********************************************************************
						Pixel Shader Effects
*********************************************************************/
const float BIAS_DIST = 0.01f;
bool RaySphereIntersect(float3 rO, float3 rD, float3 sO, float sR, 
						inout float near, inout float far)
{
	float3 dt = rO - sO;
	
	float A = dot(rD, rD);
	float B = 2*dot(dt, rD);
	float C = dot(dt, dt) - sR*sR;
	
	float disc = B*B - 4.0*A*C;
	if(disc < BIAS_DIST)
		return false;
	else
	{
		float discSqrt = sqrt(disc);
		near = (-B - discSqrt)/(2*A);
		far = (-B + discSqrt)/(2*A);
		return true;
	}
}

float4	SmokeVolumePS (VS_PARTICLE inVS)	:COLOR0
{
	float2 screenC = 0.5*((inVS.texC) + float2(1,1));
	screenC.y = 1 - screenC.y;
	
	float sceneDepth = tex2D(DeferredDepth, screenC).r*1000.0f;
	
	//float4 viewDepth = mul(float4(inVS.texC, sceneDepth,1),g_sProjInv);
	//float depthScene = viewDepth.z/viewDepth.w;
	
	// Ray sphere intersection
	float pNear = g_sNear;
	float pFar	= g_sFar;
	if(!RaySphereIntersect(g_sCameraPos, g_sViewDir, inVS.posW, inVS.size, pNear, pFar))
		discard;

	float3 nearW = g_sCameraPos + g_sViewDir*pNear;
	float3 farW = g_sCameraPos + g_sViewDir*pFar;
	float4 viewNear = mul(float4(nearW,1), g_sView);
	float4 viewFar = mul(float4(farW,1), g_sView);
	float depth = viewNear.z/viewNear.w;
	float farDepth = viewFar.z/viewFar.w;
	float lifePow = inVS.life; //inVS.texC.z;

	float depthDif = farDepth-sceneDepth;
	if(depthDiff > 0)
	{
		pFar -= depthDif;
		if(pFar < pNear)
			discard;
		farW = g_sCameraPos + g_sViewRay*pFar;
		farDepth = sceneDepth;
	}
	
	float3 unitTex = (nearW - inVS.posW)/inVS.size;
	float3 localNear, localFar;
	float noiseSizeAdj = 1/g_sNoiseSize;
	localNear = nearW * noiseSizeAdj;
	localFar = farW * noiseSizeAdj;

	int iStep = length(localFar-localNear)/g_sStepSize;
	iStep = min(iStep, MAX_STEPS-2)+2;

	return 0;
}

float4	SoftParticlePS(VS_PARTICLE inVS)	: COLOR
{
	float depthDif = float(tex2D(DeferredDepth,inVS.texC).r) - inVS.depth;
	float x;
	if(depthDif > 0.5)
		x = 1-depthDif;
	else
		x = depthDif;
	float alphaDepth = pow(0.5*saturate(2*x),contrast);

	float4 color = tex2D(ParticleS, inVS.texC);
	color.a = alphaDepth;
	return color;
}


/*********************************************************************
						Techniques
*********************************************************************/
technique ParticleTech
{
	pass p0
	{
		VertexShader	= compile vs_3_0	ParticleVS();
		PixelShader		= compile ps_3_0	SmokeVolumePS();
		CullMode		= ccw;
		FillMode		= Solid;
		ZEnable			= true;
		ZWriteEnable	= true;
		ZFunc			= less;
		StencilEnable	= false;
		AlphaBlendEnable = false;
		AlphaTestEnable	= false;
		//ColorWriteEnable = red | green | blue;
	}
}
technique FireTech
{
	pass p0
	{
		VertexShader 	= compile vs_3_0	ParticleVS();
		PixelShadeer	= compile ps_3_0	FirePS();
		
		PointSpriteEnable 	= true;
		AlphaBlendEnable	= true;
		SrcBlend			= One;
		DestBlend			= One;
		ZWriteEnable		= false;
	}
}

technique SmokeTech
{
	pass p0
	{
		VertexShader 	= compile vs_3_0	ParticleVS();
		PixelShadeer	= compile ps_3_0	SmokePS();
		
		PointSpriteEnable 	= true;
		AlphaBlendEnable	= true;
		SrcBlend			= SrcAlpha;
		DestBlend			= InvSrcAlpha;
		ZWriteEnable		= false;
	}
}


