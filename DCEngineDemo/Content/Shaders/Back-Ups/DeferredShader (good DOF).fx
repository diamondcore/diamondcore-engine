uniform extern float4x4 g_sWVP;				// WorldViewProj Matrix
uniform extern float4x4 g_sWorldInvTrans;	// WorldInverseTranspose Matrix
uniform extern float4x4 g_sWorld;			// World Matrix
uniform extern float4x4	g_sViewProj;		// View Projection Matrix
uniform extern float4x3 g_sViewInv;			// View inverse
uniform extern float3	g_sCameraPos;		// Eye Position (camera in world coordinates)

// Lighting
uniform	extern float3	g_sLightPosW;		// Light's position (world coordinates)
uniform extern float4	g_sAmbientLight;	// Ambient Light
uniform extern float4	g_sDiffuseLight;	// Diffuse Light
uniform extern float4	g_sSpecLight;		// Specular Light
uniform extern float3	g_sAttenuation;		// Attenuation012

// Materials
uniform extern float4	g_sAmbientMtrl;		// Ambient Material
uniform extern float4	g_sDiffuseMtrl;		// Diffuse Material
uniform extern float4	g_sSpecMtrl;		// Specular Material
uniform extern float	g_sSpecPower;		// Specular Power

uniform extern float2	g_sPixelSize;		// Size of the screen (1/width, 1/height)

// Useing float4 array allows for Color in the same array as the WorldMat
bool f_IsFog = true;
float startFog = 10;
float Shininess = 10.0f; // Higher the number, smoother the light surface 
float4 fogColor = {0.96, 0.96, 0.96, 1};
float PI = 3.1415926536f;
float focalPlane = 10.0f;

// Textures
uniform extern texture		g_sTexture;	
uniform extern texture2D	g_sDeferredMtrl;
uniform extern texture2D	g_sDeferredNormW;
uniform extern texture2D	g_sDeferredPosW;
uniform extern texture2D	g_sDeferredDepth;

// Texture Sample
sampler	TextureS = sampler_state			
{
	Texture		= <g_sTexture>;		
	AddressU	= Wrap;
	AddressV	= Wrap;		
	MinFilter	= Linear;			
	MagFilter	= Linear;
	MipFilter	= Linear;
};

/*********************************************************************
						Deferred Shader Data
*********************************************************************/
// Material
sampler DeferredMtrl = sampler_state
{
	Texture		= <g_sDeferredMtrl>;
	MagFilter	= Point;
	MinFilter	= Point;
};

// World Normal
sampler DeferredNormW = sampler_state
{
	Texture		= <g_sDeferredNormW>;
	MagFilter	= Point;
	MinFilter	= Point;
};

// World Position (X, Y)
sampler DeferredPosW = sampler_state
{
	Texture		= <g_sDeferredPosW>;
	MagFilter	= Point;
	MinFilter	= Point;
};

// Depth
sampler DeferredDepth = sampler_state
{
	Texture		= <g_sDeferredDepth>;
	MagFilter	= Point;
	MinFilter	= Point;
};

// Post Processing
sampler PostProcess = sampler_state
{
	Texture		= <g_sDeferredMtrl>;
	MagFilter	= Linear;
	MinFilter	= Linear;
};

// Linear Depth
sampler LinearDepth = sampler_state
{
	Texture		= <g_sDeferredDepth>;
	MagFilter	= Linear;
	MinFilter	= Linear;
};


/*********************************************************************
						Uber Shader Switches
*********************************************************************/
uniform extern bool	f_Phong;
uniform extern bool	f_BlinnPhong;
uniform extern bool f_IsTex;
//uniform extern bool f_IsFog;


/*********************************************************************
						Shader Output Types
*********************************************************************/
struct VS_OUTPUT						// Vertex Shader						
{
	float4 posH		: POSITION0;
	float3 normW	: TEXCOORD0;
	float3 posW		: TEXCOORD1;
	float2 texC		: TEXCOORD2;
	float3 toEyeW	: TEXCOORD3;
	//float  fog		: TEXCOORD4;
	float spec		: TEXCOORD4;
	float3 lightDir	: TEXCOORD5;
};

struct PS_OUTPUT_DEF
{
	float4 mtrl		: COLOR0;
	float4 normW	: COLOR1;
	float4 posW		: COLOR2;	// (X,Y)
	float4 depth	: COLOR3;	// (Z)
};

/*********************************************************************
						Vertex Shader Effects
*********************************************************************/	
VS_OUTPUT	MainVS (float3 posL	: POSITION0,
					float3 normL : NORMAL0,
					float2 texC	: TEXCOORD0)
{
	VS_OUTPUT outVS = (VS_OUTPUT)0;	// Zero-out the output

	// Transform normal to world space
	float3 normalW = mul(float4(normL, 1.0f), g_sWorldInvTrans).xyz;
	outVS.normW = normalize(normalW);

	// Transform vertex position to world space
	outVS.posW = mul(float4(posL, 1.0f), g_sWorld).xyz;

	// Transform to homogeneous clip space
	outVS.posH = mul(float4(posL, 1.0f), g_sWVP);

	// Vector to eye
	outVS.toEyeW = g_sCameraPos - outVS.posW;

	outVS.texC = texC;	// Pass texture for rasterization

	// Uber Code Here


	return outVS;
}


VS_OUTPUT	DeferredVS (float3 posL		: POSITION0,
						float3 normL	: NORMAL0,
						float2 texC		: TEXCOORD0,
						float3 tang		: TANGENT0,
						float3 bin		: BINORMAL0)
{
	VS_OUTPUT outVS = (VS_OUTPUT)0;
	
	outVS.posW = mul(float4(posL, 1.0), g_sWorld);

	outVS.texC = texC;
	outVS.normW = normalize(mul(float4(normL, 0.0), g_sWorld));

	outVS.posH = mul(float4(posL, 1.0), g_sWVP);
	float3x3 TBN;
	TBN[0] = tang;
	TBN[1] = bin;
	TBN[2] = outVS.normW;
	float3x3 tts = transpose(TBN);

	// Calculate Light
	float3 lightDir = g_sLightPosW - outVS.posW;
	lightDir = normalize(mul(float4(lightDir, 0.0f), g_sWorldInvTrans));
	lightDir = mul(lightDir, tts);
	outVS.lightDir = lightDir;
	
	// Eye direction
	float3 eyeDir = mul(g_sCameraPos, g_sWorldInvTrans);
	eyeDir = normalize(eyeDir-posL);
	outVS.toEyeW = mul(eyeDir, tts);

	// Specularity
	float r = reflect(lightDir, mul(outVS.normW, TBN));
	float s = dot(r, outVS.toEyeW);
	outVS.spec = max(s, 0);

	//float3 camPosW = mul(g_sCameraPos, g_sWorld);
	//float distFog = distance(posL.xy, camPosW.xy);
	//outVS.fog = saturate(exp((startFog-distFog)*0.33));

	return outVS;
}


/*********************************************************************
						Pixel Shader Effects
*********************************************************************/
float4	BlinnPhongPS(VS_OUTPUT outVS)				// Blinn Phong
{
	// Normalizing in the pixel shader, has a much smoother finish
	outVS.normW = normalize(outVS.normW);
	float3 toEye = normalize(outVS.toEyeW);	

	float3 lightDir = normalize(g_sLightPosW - outVS.posW);
	
	// Blinn Phong calculation
	float3 h = normalize(lightDir + normalize(toEye));
	float t = pow(max(dot(h, outVS.normW), 0.0f), g_sSpecPower);
	
	float s = max(dot(lightDir, outVS.normW), 0.0f);

	float3 spec = t * (g_sSpecMtrl * g_sSpecLight).rgb;
	float3 diffuse = s * (g_sDiffuseMtrl  * g_sDiffuseLight).rgb;
	float3 ambient = (g_sAmbientMtrl * g_sAmbientLight);

	float d = distance(g_sLightPosW, outVS.posW);
	float a = g_sAttenuation.x + g_sAttenuation.y*d + g_sAttenuation.z*d*d;
	
	if(f_IsTex)	// If their is a texture
	{
		// Add lighting and texture color
		float4 texColor = tex2D(TextureS, outVS.texC);
		float3 color = ((diffuse+spec)*(texColor.rgb+ 1/a) + ambient);
		return float4(color, texColor.a * g_sDiffuseMtrl.a);
	}
	// Else just use lighting
	return float4(ambient + (diffuse + spec)/a, g_sDiffuseMtrl.a);
}

float4	PhongPS (VS_OUTPUT outVS)				// Phong
{
	// Normalize per pixel
	outVS.normW = normalize(outVS.normW);
	float3 toEye = normalize(outVS.toEyeW);

	// Phong calculation
	float3 r = reflect(-g_sLightPosW, outVS.normW);
	float t = pow(max(dot(r, toEye), 0.0f), g_sSpecPower);

	float s = max(dot(g_sLightPosW, outVS.normW), 0.0f);

	float3 spec = t*(g_sSpecMtrl*g_sSpecLight).rgb;
	float3 diffuse = s*(g_sDiffuseMtrl*g_sDiffuseLight).rgb;
	float3 ambient = g_sAmbientMtrl*g_sAmbientLight;

	if(f_IsTex)	// If their is a texture
	{
		// Add lighting and texture color
		float4 texColor = tex2D(TextureS, outVS.texC);
		float3 color = (ambient + diffuse)*texColor.rgb + spec;
		return float4(color, texColor.a * g_sDiffuseMtrl.a);
	}
	// Else just use lighting
	return float4(ambient + diffuse + spec, g_sDiffuseMtrl.a);
}

float4	MainPS (VS_OUTPUT outVS)	: COLOR
{
	if(f_Phong)			//(UsePhong)
		return PhongPS(outVS);
	
	if(f_BlinnPhong)	//(UseBlinnPhong)
		return BlinnPhongPS(outVS);

	return float4(1.0f, 1.0f, 1.0f, 0.5f);	// Default Color
}

float dofScale = 0.00025f;//14f;
float numTaps = 25.0f;
float2 taps[25] =	// random offset vectors (2D in screen space)
{
	{-0.946045,	0},
	{0.730225,	0},
	{-1.65112,	0},
	{0.255615,	0},
	{-1.27808,	0},
	{0.390381,	0},
	{-1.9519,	0},
	{1.75952,	0},
	{-0.797607,	0},
	{1.98804,	0},
	{-1.94019,	0},
	{1.70093,	0},
	{0, -0.504639},
	{0, 0.523193},
	{0, -0.615967},
	{0, 1.07983},
	{0, -1.39917},
	{0, 0.99585},
	{0, -0.979248},
	{0, 0.89624},
	{0, -0.481201},
	{0, 0.406006},
	{0,	-0.0300293},
	{0,	0.150146},
	{-0.750732,	0}
};
float4	DepthOfFieldPS (float2 texC	:TEXCOORD0)	:COLOR0
{
	texC += g_sPixelSize * 0.5f;

	// The focal distance is the average distance in the center of the screen
	float focusDist = tex2D(DeferredDepth, float2(0.5,0.5)).r;
	focusDist += tex2D(DeferredDepth, float2(0.5,0.5) + 4 * g_sPixelSize).r;
	focusDist += tex2D(DeferredDepth, float2(0.5,0.5) - 4 * g_sPixelSize).r;
	focusDist += tex2D(DeferredDepth, float2(0.5,0.5) + 4 * float2(g_sPixelSize.x, -g_sPixelSize.y)).r;
	focusDist += tex2D(DeferredDepth, float2(0.5,0.5) + 4 * float2(-g_sPixelSize.x, g_sPixelSize.y)).r;
	focusDist /= 5.0f;
	//focusDist = focalPlane;

	float4 color = tex2D(PostProcess, texC);
	float  depth = tex2D(DeferredDepth, texC).r;
	float CoC = abs(depth - focusDist)* dofScale;

	// Mark the focus distance pixel on the screen
	//if(any(texC.xy == float2(float2(0.5,0.5) + 2 * g_sPixelSize).xy) ||
	//	any(texC.xy == float2(float2(0.5,0.5) - 2 * g_sPixelSize).xy) ||
	//	any(texC.xy == float2(float2(0.5,0.5) + 2 * float2(g_sPixelSize.x, -g_sPixelSize.y)).xy) ||
	//	any(texC.xy == float2(float2(0.5,0.5) + 2 * float2(-g_sPixelSize.x, g_sPixelSize.y)).xy))
	//		return float4(depth, 0.0f, 0.0f, 1.0f);

	[unroll]for(int i = 0; i < numTaps; i++)
	{
		float2 tapCoord = texC + taps[i].xy * CoC;
		color += tex2D(PostProcess, tapCoord);
	}
	return color /(numTaps + 1);
}


float4	DeferredLight(float2 texC	: TEXCOORD0)	: COLOR0
{
	float3 diffuseColor = tex2D(DeferredMtrl, texC).rgb;
	float  spec	= abs(tex2D(DeferredMtrl, texC).a);
	float3 specColor = g_sSpecPower * g_sSpecLight * max(pow(spec, Shininess), 0);

	float4 normW = tex2D(DeferredNormW, texC);
	float shade = normW.a;
	float2 norm = (normW.xy*2.0f)-1.0f; //-0.5)*2.0f;
	float2 sct;
	sincos(norm.x*PI, sct.x, sct.y);
	float2 scphi = float2(sqrt(1.0 - norm.y*norm.y), norm.y);
	float3 normal = float3(sct.y*scphi.x, sct.x*scphi.x, scphi.y); 

	//float3 N = tex2D(DeferredDepth, texC).rgb;
	//float3 T = cross(N, float3(0, 1, 0));
	//float3 B = cross(N, T);
	//float3x3 TBN = float3x3(T,B,N);

	float3 posW;
	posW.xy = (tex2D(DeferredPosW, texC).rg)*2.0f - 1.0f; //.xy;
	//posW.z 	= sqrt(1-dot(posW.xy, posW.xy));
	posW.z	= tex2D(DeferredDepth, texC).r*2.0f - 1.0f; //.x;

	float3 lightDir = normalize(g_sLightPosW - posW);
	float3 toEye = normalize(g_sViewInv[3].xyz - posW);
	float3 diffuseIntensity = dot(lightDir, normal);
	float3 specIntensity = pow(max(0, dot(toEye, reflect(-lightDir, normal))), g_sSpecPower);

	//float4 color;
	//float3 ambient = g_sAmbientLight * g_sAmbientMtrl;
	//color.rgb = (diffuseColor + specColor)* shade + ambient;
	//color.a	= 1.0;

	float4 color;
	color.rgb = diffuseIntensity * g_sDiffuseLight.xyz * diffuseColor +
				specIntensity * g_sSpecLight.xyz * specColor;
	color.a	= 1.0;

	//if(f_IsFog)
	//	color.rgb += fogColor.rgb * posW.z;	// Fog
	//else
	//	color.rgb += texC.rgr * 0.5;

	return color;
}


PS_OUTPUT_DEF	DeferredPS (VS_OUTPUT outVS)
{
	PS_OUTPUT_DEF outPS = (PS_OUTPUT_DEF)0;

	// Material
	float4 texColor = tex2D(TextureS, outVS.texC);	// Diffuse

	outPS.mtrl.rgb	= texColor;
	outPS.mtrl.a	= outVS.spec; //1.0f;

	// Calculate pixel light
	float dist = distance(outVS.posW, g_sLightPosW);
	float power = 1.0f - (dist/1000.0f); // (dist/MaxLightRange)
	float shade = dot(outVS.normW, outVS.lightDir);
	
	// Store the normal in spherical coordinates
	outPS.normW.rg	= (float2(atan2(outVS.normW.y, outVS.normW.x)/PI, outVS.normW.z)+1.0) *0.5f;
	outPS.normW.b	= half(shade);
	//outPS.normW.a	= 0.0f;

	// Position
	outPS.posW = half4(outVS.posW.xy*0.5f + 0.5f, 0, 0);

	// Depth
	outPS.depth	= float4(outVS.posW.z*0.5 + 0.5f, 0, 0, 0);
	
	// Fog
	//if(f_IsFog)
		//outPS.mtrl.rgb = lerp(fogColor, outPS.mtrl.rgb, outVS.fog);
	
	return outPS;
}


/*********************************************************************
						Techniques
*********************************************************************/
technique MainTech
{
	pass p0
	{
		VertexShader	= compile vs_3_0	DeferredVS();
		PixelShader		= compile ps_3_0	DeferredPS();
		CullMode		= ccw;
		FillMode		= Solid;
		ZEnable			= true;
		ZWriteEnable	= true;
		ZFunc			= less;
		StencilEnable	= false;
		AlphaBlendEnable = false;
		AlphaTestEnable	= false;
		//ColorWriteEnable = red | green | blue;
	}
}
technique DefLightTech
{
	pass p0
	{
		VertexShader	= NULL;	//compile vs_3_0	MainVS();
		PixelShader		= compile ps_3_0	DeferredLight();
		//CullMode		= none;
		FillMode		= Solid;
		ZEnable			= false;
		StencilEnable	= false;
		AlphaBlendEnable = true;
		Srcblend		= One;
		Destblend		= One;
		AlphaTestEnable	= false;
		//ColorWriteEnable = red | green | blue;
	}
}
technique DofTech
{
	pass p0
	{
		VertexShader	= NULL;
		PixelShader		= compile ps_3_0	DepthOfFieldPS();
		//CullMode		= none;
		FillMode		= Solid;
		ZEnable			= false;
		StencilEnable	= false;
		//AlphaBlendEnable = true;
		//Srcblend		= One;
		//Destblend		= One;
		//AlphaTestEnable	= false;
		//ColorWriteEnable = red | green | blue;
	}
}
