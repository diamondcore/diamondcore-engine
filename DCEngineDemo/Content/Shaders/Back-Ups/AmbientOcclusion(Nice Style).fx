uniform extern float3	g_sCameraView;		// Eye View vector\
#include "DeferredShader.fx"

// Textures
uniform extern texture	g_sRandomTex;		// Used for random sampling

// Samples points
static const int		NumSamples = 64;//16;	// Number of samples (default = 16)
float 					contrastValue = 1;	// Contrast of the SSAO (default = 1)
float					ScaleMin = 3;		// Min Scale (default = 3)
float					ScaleMax = 1+(2.4/NumSamples);	// Max Scale (default = 30)


// Random texture Sample
sampler	RandomS = sampler_state			
{
	Texture		= <g_sRandomTex>;				
	MinFilter	= POINT;			
	MagFilter	= POINT;
	MipFilter	= POINT;
	AddressU	= MIRROR;
	AddressV	= MIRROR;
};


/*********************************************************************
						Pixel Shader Effects
*********************************************************************/
// Random Unit Vectors in the Unit Sphere
float3 samples[32] = {
	float3(0.355512, 	-0.709318, 	-0.102371),
	float3(0.534186, 	0.71511, 	-0.115167),
	float3(-0.87866, 	0.157139, 	-0.115167),
	float3(0.140679, 	-0.475516, 	-0.0639818),
	float3(-0.0796121, 	0.158842, 	-0.677075),
	float3(-0.0759516, 	-0.101676, 	-0.483625),
	float3(0.12493, 	-0.0223423,	-0.483625),
	float3(-0.0720074, 	0.243395, 	-0.967251),
	float3(-0.207641, 	0.414286, 	0.187755),
	float3(-0.277332, 	-0.371262, 	0.187755),
	float3(0.63864, 	-0.114214, 	0.262857),
	float3(-0.184051, 	0.622119, 	0.262857),
	float3(0.110007, 	-0.219486, 	0.435574),
	float3(0.235085, 	0.314707, 	0.696918),
	float3(-0.290012, 	0.0518654, 	0.522688),
	float3(0.0975089, 	-0.329594, 	0.609803),	// Next 16
	float3(0.53812504, 0.18565957, -0.43192),
	float3(0.13790712, 0.24864247, 0.44301823),
	float3(0.33715037, 0.56794053, -0.005789503),
	float3(-0.6999805, -0.04511441, -0.0019965635),
	float3(0.06896307, -0.15983082, -0.85477847),
	float3(0.056099437, 0.006954967, -0.1843352),
	float3(-0.014653638, 0.14027752, 0.0762037),
	float3(0.010019933, -0.1924225, -0.034443386),
	float3(-0.35775623, -0.5301969, -0.43581226),
	float3(-0.3169221, 0.106360726, 0.015860917),
	float3(0.010350345, -0.58698344, 0.0046293875),
	float3(-0.08972908, -0.49408212, 0.3287904),
	float3(0.7119986, -0.0154690035, -0.09183723),
	float3(-0.053382345, 0.059675813, -0.5411899),
	float3(0.035267662, -0.063188605, 0.54602677),
	float3(-0.47761092, 0.2847911, -0.0271716)
};

float	crossBilateral(float r, float d0, float d1)	// d is linear depth
{
	//const float blurS = ((float)NumSamples + 1.0f)*0.5f;
	const float blurS = ((float)ScaleMax + 1.0f)*0.5f;
	const float blurFO = 1.0f/(2.0f*blurS*blurS);
	
	// d0 and d1 shoud be Pre-scaled linear depth
	float dz = d0 - d1;		// dz is change in depth z
	return exp2(-r*r*blurFO -dz*dz);
}
/*
// Average TexelSize with each pixel, to reduce noise
float	Blur(float2 texC)	
{
	float blur = 0.0f;
	for(int i = 0; i < 4; i++)			// Average pixels in groups of 4
		for(int j = 0; j < 4; j++)
		{
			float2 offset = float2(g_sPixelSize.x * float(j), 
									g_sPixelSize.y * float(i));
			blur += tex2D(DeferredDepth, texC+offset).r;
		}
	return blur*0.0625f;
}*/


float4	ScreenSpacePS (float2 texC	: TEXCOORD0)	: COLOR		// Screen Space Ambient Occlusion
{
	float depth = (tex2D(DeferredDepth, texC).r)*1000.0f;	// Multiply by far clip to convert to meters

	// Place this pixel in a random vector
	float3 randV = tex2D(RandomS, texC * g_sPixelSize/4).rgb;	// 250 is the random multiplyer
	// Create rotation matrix
	float3x3 rotMat;
	float h = 1 / (1 + randV.z);
	rotMat._m00 = h*randV.y*randV.y+randV.z;
	rotMat._m01 = -h*randV.y*randV.x;
	rotMat._m02 = -randV.x;
	rotMat._m10 = -h*randV.y*randV.x;
	rotMat._m11 = h*randV.x*randV.x+randV.z;
	rotMat._m12 = -randV.y;
	rotMat._m20 = randV.x;
	rotMat._m21 = randV.y;
	rotMat._m22 = randV.z;

	// Offset Scales
	float offsetScale = 0.01;
	const float offsetScaleStep = 1 + 2.4/NumSamples;

	float totalO = 0;	// Total Occlusion
	for(int i = 0;  i < (NumSamples/8); i++)
		for(int x = -1; x <= 1; x += 2)
			for(int y = -1; y <= 1; y += 2)
				for(int z = -1; z <= 1; z += 2)
				{		
					// Calculate sample vector with a random reflection plane
					float3 Offset = normalize(float3(x,y,z))*
									(offsetScale *= offsetScaleStep);
					float3 rotOffset = mul(Offset, rotMat);

					// Calculate sample position
					float3 samplePos = float3(texC, depth);
					samplePos += float3(rotOffset.xy, rotOffset.z*depth*2);

					float sampleDepth = tex2D(DeferredDepth, samplePos.xy)*1000.0f; // Multiply by far clip to convert to meters

					float rangeIsInValid = saturate(((depth - sampleDepth)/sampleDepth));	//crossBilateral(offsetScale, depth, sampleDepth);

					// Calculate occlusion
					totalO += lerp(sampleDepth > samplePos.z, 0.5, rangeIsInValid);
				}

	// Average the final occlusion
	totalO = totalO / NumSamples;

	// Use the ContrastValue to adjust the contrast
	float weight = saturate(totalO*totalO+totalO);
	float4 color = tex2D(DeferredMtrl, texC);
	return float4((weight * color.rgb),color.a);//tex2D(TextureS, texC).rgb;		
}

float4	EditedScreenSpacePS (float2 texC	: TEXCOORD0)	: COLOR		// Screen Space Ambient Occlusion
{
	// Sample the Buffers
	float3 posW;
	posW.xy = (tex2D(DeferredPosW, texC).rg)*2.0f - 1.0f;	// LevelOfDetail version	
	posW.z = (tex2D(DeferredDepth, texC).r)*2.0f - 1.0f;

	// Calculate spherical normal
	float4 normW = tex2D(DeferredNormW, texC);
	float2 norm = (normW.xy*2.0f)-1.0f;
	float2 sct;
	sincos(norm.x*PI, sct.x, sct.y);
	float2 scphi = float2(sqrt(1.0 - norm.y*norm.y), norm.y);
	float3 normal = float3(sct.y*scphi.x, sct.x*scphi.x, scphi.y); 
	
	// Place this pixel in a random vector
	float3 randV = tex2D(RandomS, texC * g_sPixelSize/4);	// 250 is the random multiplyer
	// Create rotation matrix
	float3x3 rotMat;
	float h = 1 / (1 + randV.z);
	rotMat._m00 = h*randV.y*randV.y+randV.z;
	rotMat._m01 = -h*randV.y*randV.x;
	rotMat._m02 = -randV.x;
	rotMat._m10 = -h*randV.y*randV.x;
	rotMat._m11 = h*randV.x*randV.x+randV.z;
	rotMat._m12 = -randV.y;
	rotMat._m20 = randV.x;
	rotMat._m21 = randV.y;
	rotMat._m22 = randV.z;	

	float totalW = 0;	// Total Weight
	float totalO = 0;	// Total Occlusion
	[unroll]for(int i = 0;  i < NumSamples && i < 16; i++)
	{		
		// Distribute Samples Scale "Ka"
		float d = (float)i/(float)NumSamples;
		float scaleSample = ScaleMin * d + ScaleMax * (1-d);

		// Calculate sample vector with a random reflection plane
		float3 Offset = scaleSample * reflect(samples[(i)%16].rgb, randV);
	
		// Reflect if self-occluded
		if( dot(Offset, normal) < 0)
			Offset = reflect(Offset, normal);

		// Calculate sample position
		float3 samplesPos = posW.rgb + Offset.rgb;
		float samplesDepth = dot(samplesPos, normal);	// Get the depth

		// Offset in screen space
		float4x4 rMat = {
			rotMat._m00,rotMat._m01, rotMat._m02, 0,
			rotMat._m10,rotMat._m11, rotMat._m12, 0,
			rotMat._m20,rotMat._m21, rotMat._m22, 0,
			0,0,0, 1	};

		float4 screenSpace = mul(float4(Offset,1.0f), rMat);//g_sWVP);
		float2 screenPos = float2(screenSpace.x, -screenSpace.y)/screenSpace.w;	// Depth Map equation

		// Get the pixel position
		float3 pixelPos;
		pixelPos.xy = (tex2D(DeferredPosW, texC + screenPos).rg)*2.0f - 1.0f;
		pixelPos.z = (tex2D(DeferredDepth, texC + screenPos).r)*2.0f - 1.0f;
		float pixelDepth = dot(pixelPos, normal); // Get the pixel depth
		
		// To avoid occluding from the background objects, extend the depth field
		if( pixelPos.z < 0.5)
			pixelDepth = -100000;

		// Calculate the relative depth
		float relativeDepth = (samplesDepth - pixelDepth) / ScaleMax;
	
		// Calculate weight relative to the angle of the surface normal
		float weight = dot(normalize(samplesPos - posW), normalize(normal));
		totalW += weight;

		// Calculate occlusion
		float occlusionValue = weight * 1 / ( 1 + relativeDepth*relativeDepth);
		totalO += step(relativeDepth, -1/(scaleSample*1000)) * occlusionValue;
	}
	
	// Normalize the final occlusion
	totalO = totalO / totalW;

	// Use the ContrastValue to adjust the contrast
	float finalO = pow(abs(totalO), contrastValue);	// Default value is 1
	return 1 - finalO;		
}


float4	OldScreenSpacePS (float2 texC	: TEXCOORD0)	: COLOR		// Screen Space Ambient Occlusion
{
	// Sample the Buffers
	float3 posW;
	posW.xy = (tex2D(DeferredPosW, texC).rg)*2.0f - 1.0f;	// LevelOfDetail version	
	posW.z = (tex2D(DeferredDepth, texC).r)*2.0f - 1.0f;

	// Calculate spherical normal
	float4 normW = tex2D(DeferredNormW, texC);
	float2 norm = (normW.xy*2.0f)-1.0f;
	float2 sct;
	sincos(norm.x*PI, sct.x, sct.y);
	float2 scphi = float2(sqrt(1.0 - norm.y*norm.y), norm.y);
	float3 normal = float3(sct.y*scphi.x, sct.x*scphi.x, scphi.y); 
	
	// Place this pixel in a random vector
	float3 randV = tex2D(RandomS, texC * 250);	// 250 is the random multiplyer
	//randV = mul(randV, TBN);
	faceforward(randV, g_sViewInv[3].xyz, randV);

	float totalW = 0;	// Total Weight
	float totalO = 0;	// Total Occlusion
	[unroll]for(int i = 0;  i < NumSamples && i < 16; i++)
	{		
		// Distribute Samples Scale "Ka"
		float d = (float)i/(float)NumSamples;
		float scaleSample = ScaleMin * d + ScaleMax * (1-d);

		// Calculate sample vector with a random reflection plane
		float3 Offset = scaleSample * reflect(samples[(i)%16].rgb, randV);
	
		// Reflect if self-occluded
		if( dot(Offset, normal) < 0)
			Offset = reflect(Offset, normal);

		// Calculate sample position
		float3 samplesPos = posW.rgb + Offset.rgb;
		float samplesDepth = dot(samplesPos, normal);	// Get the depth

		// Offset in screen space
		float4 screenSpace = mul(float4(Offset,1.0f), g_sWVP);
		float2 screenPos = float2(screenSpace.x, -screenSpace.y)/screenSpace.w;	// Depth Map equation

		// Get the pixel position
		float3 pixelPos;
		pixelPos.xy = (tex2D(DeferredPosW, texC + screenPos).rg)*2.0f - 1.0f;
		pixelPos.z = (tex2D(DeferredDepth, texC + screenPos).r)*2.0f - 1.0f;
		float pixelDepth = dot(pixelPos, normal); // Get the pixel depth
		
		// To avoid occluding from the background objects, extend the depth field
		//if( pixelPos.z < 0.5)
		//	pixelDepth = -100000;

		// Calculate the relative depth
		float relativeDepth = (samplesDepth - pixelDepth) / ScaleMax;
	
		// Calculate weight relative to the angle of the surface normal
		float weight = dot(normalize(samplesPos - posW), normalize(normal));
		totalW += weight;

		// Calculate occlusion
		float occlusionValue = weight * 1 / ( 1 + relativeDepth*relativeDepth);
		totalO += step(relativeDepth, -1/(scaleSample*1000)) * occlusionValue;
	}
	
	// Normalize the final occlusion
	totalO = totalO / totalW;

	// Use the ContrastValue to adjust the contrast
	float finalO = pow(abs(totalO), contrastValue);	// Default value is 1
	return 1 - finalO;		
}


/*********************************************************************
						Techniques
*********************************************************************/
technique SSAOTech
{
	pass p0
	{
		VertexShader	= NULL;	//compile vs_3_0	ScreenSpaceVS();
		PixelShader		= compile ps_3_0	ScreenSpacePS();

		//CullMode		= None;
		FillMode		= Solid;
		ZEnable			= false;
		StencilEnable	= false;
	}
}
