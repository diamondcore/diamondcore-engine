uniform extern float4x4	g_sLightVP;	// Used as light WVP
uniform extern float	g_sLightPower;
uniform extern float3	g_sLightDirW;

static const float SHADOW_EPSILON = 0.00005f;
uniform extern float2 SHADOW_MAP_SIZE;// = 300.0f;//512.0f;	// Shadow Map Size


// Shadow Map Texutre
uniform extern texture	g_sShadowMapTex;
sampler ShadowMapS = sampler_state
{
	texture	= <g_sShadowMapTex>;
	MinFilter	= POINT;
	MagFilter	= POINT;
	MipFilter	= POINT;
	AddressU	= CLAMP;
	AddressV	= CLAMP;
};

/*********************************************************************
						Build the Shadow Map
*********************************************************************/
void	BuildShadowMapVS(float3 posL : POSITION0,
						 out float4 posH : POSITION0,
						 out float2 depth : TEXCOORD0)
{
	//float4x4 lightWVP = mul(g_sWorld, g_sLightVP);
	//posH = mul(float4(posL,1.0f), lightWVP);
	posH = mul(float4(posL, 1.0f), g_sLightVP);
	depth = posH.zw;
}

float4	BuildShadowMapPS(float2 depth : TEXCOORD0)	:COLOR
{
	// Store the pixel's depth in normalized device coordinates
	return depth.x/depth.y;	// z/w (depth in [0,1] range)
}


/*********************************************************************
						Calculate Light Shadow
*********************************************************************/
void	LightShadowVS(float3 posL	: POSITION0,
					  float3 normL	: NORMAL0,
					  float2 texC	: TEXCOORD0,
					out float4 oPosH	: POSITION0,
					out float3 oPosW	: TEXCOORD0,
					out float3 oNormW	: TEXCOORD1,
					out float3 oToEyeW	: TEXCOORD2,
					out float2 oTexC	: TEXCOORD3,
					out float4 oProjTex	: TEXCOORD4)
{
	oPosH = mul(float4(posL,1.0f), g_sWVP);
	oPosW = mul(float4(posL, 1.0f), g_sWorld).xyz;

	oNormW = mul(float4(normL, 0.0f), g_sWorld).xyz;
	
	oToEyeW = g_sCameraPos - oPosW;
	
	oTexC = texC;

	// Generate projective texture coordinates
	//float4x4 lightWVP = mul(g_sWorld, g_sLightVP);
	//oProjTex = mul(float4(posL, 1.0f), lightWVP);
	oProjTex = mul(float4(posL, 1.0f), g_sLightVP);
}
float4	GetLightShadowVS(float3 posL)
{
	// Generate projective texture coordinates
	//float4x4 lightWVP = mul(g_sWorld, g_sLightVP);
	//return mul(float4(posL, 1.0f), lightWVP);
	return mul(float4(posL, 1.0f), g_sLightVP);
}

float4	LightShadowPS(float3 posW,//	: TEXCOORD0,
					  float3 normW,//	: TEXCOORD1,
					  float3 toEyeW,//	: TEXCOORD2,
					  float2 texC,//	: TEXCOORD3,
					  float4 projTex) //: TEXCOORD4)	//: COLOR
{
	normW = normalize(normW);
	toEyeW = normalize(toEyeW);

	float3 lightVecW = normalize(g_sLightPosW - posW);
	
	float3 r = reflect(-lightVecW, normW);
	float t = pow(max(dot(r, toEyeW), 0.0f), g_sSpecPower);
	float s = max(dot(lightVecW, normW), 0.0f);

	#if 0 
		// Use Per-Object Specular & Ambient Mtrl
		float3 spec = t*(g_sSpecMtrl*g_sSpecLight).rgb;
		float3 ambient = g_sAmbientMtrl*g_sAmbientLight;
	#else
		// Use Global Specular & Ambient Mtrl
		float3 spec = t*(float4(1.0f, 1.0f, 1.0f, 1.0f)*g_sSpecLight).rgb;
		float3 ambient = g_sDiffuseMtrl*g_sAmbientLight;
	#endif

	float3 diffuse = s*(g_sDiffuseMtrl*g_sDiffuseLight.rgb);

	float spot = pow(max(dot(-lightVecW, g_sLightDirW), 0.0f), g_sLightPower);

	float4 texColor;// = tex2D(TextureS, texC);
	
	// Get the texture with specified sample
	if(g_sIsGrass)
		texColor = tex2D(GrassS, texC);
	else
		texColor = tex2D(TextureS, texC);	// Diffuse


	// Project texture coordinates and scale/offset to [0,1]
	projTex.xy /= projTex.w;
	projTex.x = 0.5f*projTex.x + 0.5f;
	projTex.y = -0.5f*projTex.y + 0.5f;
	
	// Compute pixel depth
	float depth = projTex.z/projTex.w;	// Depth in [0,1] range

	// Transform to texel space
	float2 texelPos = SHADOW_MAP_SIZE * projTex.xy;

	// Weights for the weighted average
	float2 lerps = frac(texelPos);

	// 2x2 persetage closest filter
	float dx = 1.0f / SHADOW_MAP_SIZE;
	float s0 = (tex2D(ShadowMapS, projTex.xy).r +
				SHADOW_EPSILON < depth) ? 0.0f : 1.0f;
	float s1 = (tex2D(ShadowMapS, projTex.xy + float2(dx, 0.0f)).r +
				SHADOW_EPSILON < depth) ? 0.0f : 1.0f;
	float s2 = (tex2D(ShadowMapS, projTex.xy + float2(0.0f, dx)).r +
				SHADOW_EPSILON < depth) ? 0.0f : 1.0f;
	float s3 = (tex2D(ShadowMapS, projTex.xy + float2(dx, dx)).r +
				SHADOW_EPSILON < depth) ? 0.0f : 1.0f;

	float shadowCoeff = lerp(lerp(s0, s1, lerps.x),
							 lerp(s2, s3, lerps.x),
							 lerps.y);

	// Light/texture pixel
	float3 litColor = spot*ambient*texColor.rgb +
						spot*shadowCoeff*(diffuse*texColor.rgb+spec);
	return float4(litColor, g_sDiffuseMtrl.a*texColor.a);
}


/*********************************************************************
						Techniques
*********************************************************************/
technique BuildShadowMapTech
{
	pass p0
	{
		VertexShader	= compile vs_3_0	BuildShadowMapVS();
		PixelShader		= compile ps_3_0	BuildShadowMapPS();
		CullMode		= none;
	}
}

technique LightShadowTech
{
	pass p0
	{
		//VertexShader	= compile vs_3_0	LightShadowVS();
		//PixelShader		= compile ps_3_0	LightShadowPS();
	}
}
