var ResourceList = [];
var Keys = new KeyList();
var Camera = new Resource();
function Resource() {
    this.rID = CreateSharedResource();
    ResourceList.push(this);
    this.SetPosition = function SetPosition(x, y, z) {
        this.position.x = x; 
        this.position.y = y;
        this.position.z = z;
        SetResourcePosition(this.rID, x, y, z);
    }
    this.SetPositionX = function SetPositionX(x) {
        this.position.x = x;
        SetPosX(this.rID, x);
    }
    this.SetPositionY = function SetPositionY(y) {
        this.position.y = y;
        SetPosX(this.rID, y);
    }
    this.SetPositionZ = function SetPositionZ(z) {
        this.position.z = z;
        SetPosX(this.rID, z);
    }
    this.SetRotationYawPitchRoll = function SetRotationYawPitchRoll(yaw, pitch, roll) {
        this.rotation.x = yaw;
        this.rotation.y = pitch;
        this.rotation.z = roll;
        this.rotation.w = 0;
        SetResourceRotationYawPitchRoll(this.rID, yaw, pitch, roll);
    }
    this.SetRotation = function SetRotation(x, y, z, w) {
        this.rotation.x = x;
        this.rotation.y = y;
        this.rotation.z = z;
        this.rotation.w = w;
        SetResourceRotation(this.rID, x, y, z, w);
    }
    this.SetRotationX = function SetRotationX(x) {
        this.rotation.x = x;
        SetRotationX(this.rID, x);
    }
    this.SetRotationY = function SetRotationY(y) {
        this.rotation.y = y;
        SetRotationX(this.rID, y);
    }
    this.SetRotationZ = function SetRotationZ(z) {
        this.rotation.z = z;
        SetRotationX(this.rID, z);
    }
    this.SetRotationW = function SetRotationW(w) {
        this.rotation.w = w;
        SetRotationW(this.rID, w);
    }
    this.GetPos = function GetPos() {
        this.position.x = GetPosX(this.rID);
        this.position.y = GetPosY(this.rID);
        this.position.z = GetPosZ(this.rID);
        return this.position;
    }
    this.GetRot = function GetRot() {
        this.rotation.x = GetRotationX(this.rID);
        this.rotation.y = GetRotationY(this.rID);
        this.rotation.z = GetRotationZ(this.rID);
		this.rotation.w = GetRotationW(this.rID);
        return this.rotation;
    }
    this.GetRotVec = function GetRotVec() {
        this.rotation.x = GetRotationVecX(this.rID);
        this.rotation.y = GetRotationVecY(this.rID);
        this.rotation.z = GetRotationVecZ(this.rID);
        return this.rotation;
    }
    this.GetForwardVec = function GetForwardVec() {
        this.rotation.x = GetForwardVecX(this.rID);
        this.rotation.y = GetForwardVecY(this.rID);
        this.rotation.z = GetForwardVecZ(this.rID);
        return this.rotation;
    }
    this.GetUpVec = function GetUpVec() {
        this.rotation.x = GetUpVecX(this.rID);
        this.rotation.y = GetUpVecY(this.rID);
        this.rotation.z = GetUpVecZ(this.rID);
        return this.rotation;
    }
    this.GetRightVec = function GetRightVec() {
        this.rotation.x = GetRightVecX(this.rID);
        this.rotation.y = GetRightVecY(this.rID);
        this.rotation.z = GetRightVecZ(this.rID);
        return this.rotation;
    }
    this.Normalize = function Normalize(x, y, z) {
        this.rotation.x = NormalizeX(x, y, z);
        this.rotation.y = NormalizeY(x, y, z);
        this.rotation.z = NormalizeZ(x, y, z);
        return this.rotation;
    }
    this.SetDefaults = function SetDefaults() {
        this.SetPosition(0, 0, 0);
        this.SetRotation(0, 0, 0, 0);
    }
	
    this.position = new Object();
    this.rotation = new Object();
}
function KeyList() {
    this.KEY_ESC = 1;
	this.KEY_1 = 2;
	this.KEY_2 = 3;
	this.KEY_3 = 4;
	this.KEY_4 = 5;
	this.KEY_5 = 6;
	this.KEY_6 = 7;
	this.KEY_7 = 8;
	this.KEY_8 = 9;
	this.KEY_9 = 10;
	this.KEY_0 = 11;
	this.KEY_MINUS = 12;
	this.KEY_PLUS = 13;
	this.KEY_BACKSPACE = 14;
	this.KEY_TAB = 15;
	this.KEY_Q = 16;
	this.KEY_W = 17;
	this.KEY_E = 18;
	this.KEY_R = 19;
	this.KEY_T = 20;
	this.KEY_Y = 21;
	this.KEY_U = 22;
	this.KEY_I = 23;
	this.KEY_O = 24;
	this.KEY_P = 25;
	this.KEY_LEFTBRACKET = 26;
	this.KEY_RIGHTBRACKET = 27;
	this.KEY_ENTER = 28;
	this.KEY_LCTRL = 29;
	this.KEY_A = 30;
	this.KEY_S = 31;
	this.KEY_D = 32;
	this.KEY_F = 33;
	this.KEY_G = 34;
	this.KEY_H = 35;
	this.KEY_J = 36;
	this.KEY_K = 37;
	this.KEY_L = 38;
	this.KEY_SEMICOLON = 39;
	this.KEY_SINGLEQUOTE =40;
	this.KEY_LEFTSHIFT = 42;
	this.KEY_BACKSLASH = 43;
	this.KEY_Z = 44;
	this.KEY_X = 45;
	this.KEY_C = 46;
	this.KEY_V = 47;
	this.KEY_B = 48;
	this.KEY_N = 49;
	this.KEY_M = 50;
	this.KEY_COMMA = 51;
	this.KEY_PERIOD = 52;
	this.KEY_FORWARDSLASH = 53;
	this.KEY_RIGHTSHIFT =54;
	this.KEY_LALT = 56;
	this.KEY_SPACE = 57;
	this.KEY_RALT = 184;
	this.KEY_RCTRL = 157;
}
