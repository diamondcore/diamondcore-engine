// demo globals
// count is used for dbouncing
var count = 0.00;
var bID1;
var zShotTriger;
var zShotAnim;
var sunID;
var ssaoID;
var Building2;
function GraphicsDemo()
{
    var GraphicsDemo = true;
    if (GraphicsDemo == true) {
        //////////////PARTICLES/////////////////////////////////
        var barrel = new Resource();
        CreateMesh(barrel.rID, "barrel1.x", true);
        barrel.SetPosition(20, 3, -10);

       /* var particle = new Resource();
        particle.SetPosition(10, 3.25, -5);
        CreateParticle(particle.rID, "DefaultFire");
        CreateParticle(particle.rID, "DefaultFire");
        //CreateParticle(particle.rID, "DefaultSmoke");
        CreateParticle(particle.rID, "DefaultSmoke");
        CreateParticle(particle.rID, "DefaultSpark");
        */
        /////////////BUILDINGS///////////////////////////////////
        var Building1 = new Resource();
        Building1.SetPosition(-20, 8, 10);
        Building1.SetRotation(0, -0.7071, 0, 0.7071);
        CreateTexMesh(Building1.rID, "MediumBuilding.x", "tile010.jpg", true);

        Building2 = new Resource();
        Building2.SetPosition(0, 10, -25);
        Building2.SetRotation(0, 0.7071, 0, 0.7071);
        CreateTexMesh(Building2.rID, "Box.x", "tile010.jpg", true);
        SetMeshColor(Building2.rID, "Box.x", 1.0, 1.0, 1.0, 1.0);
        //SetMeshColor(Building2.rID, "Box.x", 0.8, 0.8, 0.8, 1.0);
        //SetMeshColor(Building2.rID, "Box.x", 0.5, 0.5, 0.5, 1.0);

        var Building3 = new Resource();
        Building3.SetPosition(-20, 8, -40);
        Building3.SetRotation(0, -0.7071, 0, 0.7071);
        CreateTexMesh(Building3.rID, "MediumBuilding.x", "tile010.jpg", true);

        var Building4 = new Resource();
        Building4.SetPosition(20, 8, -40);
        Building4.SetRotation(0, 0.7071, 0, 0.7071);
        CreateTexMesh(Building4.rID, "MediumBuilding.x", "tile010.jpg", true);

        var Building5 = new Resource();
        Building5.SetPosition(-20, 8, 50);
        Building5.SetRotation(0, 0, 0, 1);
        CreateTexMesh(Building5.rID, "MediumBuilding.x", "tile010.jpg", true);

        var Building6 = new Resource();
        Building6.SetPosition(20, 8, 50);
        Building6.SetRotation(0, 0, 0, 1);
        CreateTexMesh(Building6.rID, "MediumBuilding.x", "tile010.jpg", true);

        ////////////////////////////////////////////////////////////
        // ANIMATION
  /*      var AnimMesh1 = new Resource();
        CreateAnimMeshTex(AnimMesh1.rID, "Zombie.x", "zombie_1.jpg", true);
        AnimMesh1.SetPosition(10, 3, 30);
        AnimMesh1.SetRotation(0, 0.5, 0, 1);
        Anim_Set(AnimMesh1.rID, 0, 0, "shamble", 1.0, 1.0, 0.7);
        Anim_Set(AnimMesh1.rID, 1, 0, "attack1", 1.0, 1.0, 1.0);
        Anim_Play(0, AnimMesh1.rID);

        var AnimMesh2 = new Resource();
        CreateAnimMeshTex(AnimMesh2.rID, "Zombie.x", "zombie_2.jpg", true);
        AnimMesh2.SetPosition(-10, 3, 30);
        AnimMesh2.SetRotation(0, 0, 0, 1);
        Anim_Set(AnimMesh2.rID, 0, 0, "walk", 1.0, 1.0, 0.7);
        Anim_Play(0, AnimMesh2.rID);

        var AnimMesh3 = new Resource();
        CreateAnimMeshTex(AnimMesh3.rID, "Zombie.x", "zombie_3.jpg", true);
        AnimMesh3.SetPosition(10, 3, 40);
        AnimMesh3.SetRotation(0, 0.5, 0, 1);
        Anim_Set(AnimMesh3.rID, 0, 0, "fall", 1.0, 1.0, 0.7);
        Anim_Play(0, AnimMesh3.rID);

        var AnimMesh4 = new Resource();
        CreateAnimMeshTex(AnimMesh4.rID, "Zombie.x", "zombie_4.jpg", true);
        ScaleAnimMesh(AnimMesh4.rID, "Zombie.x", 0.7, 0.7, 0.7);
        AnimMesh4.SetPosition(-18, 3, -10);
        AnimMesh4.SetRotation(0, -0.7071, 0, 0.7071);
        Anim_Set(AnimMesh4.rID, 0, 0, "attack2", 1.0, 1.0, 0.7);
        Anim_Play(0, AnimMesh4.rID);
        zShotAnim = AnimMesh4.rID;  // So we can change animation later
*/
        //////////////////////////////////////////////////////////////////
        //// Create the ground
        var ground = new Resource();
        CreateBillboard(ground.rID, "whitetex.dds", true);
        ground.SetPosition(0, 2.5, 0);
        ground.SetRotation(0.7071, 0, 0, 0.7071);
        ScaleBillboard(ground.rID, "whitetex.dds", 200, 200, 0);
        SetBillboardColor(ground.rID, "whitetex.dds", 0.25, 0.25, 0.25, 1.0);

        //////////////////////////////////////////////////////////////////
        //// Scene Objects
        var m4 = new Resource();
        CreateMesh(m4.rID, "m95.x", true);
        m4.SetPosition(0, 9, -40);
        m4.SetRotation(0, 1, 0, 0);

        ////////////////////////////////////////////////////////////////////
        // Billboard Triggers
        var billboard1 = new Resource();
        CreateBillboard(billboard1.rID, "whitetex.dds", false);
        billboard1.SetPosition(20, 5.9, -10);
        billboard1.SetRotation(0.7071, 0, 0, 0.7071);
        SetBillboardColor(billboard1.rID, "whitetex.dds", 0.0, 0.0, 0.0, 0.0);
        bID1 = billboard1.rID;

        // Triger for zombie shot
        var zShot1 = new Resource();
        CreateBillboard(zShot1.rID, "whitetex.dds", false);
        zShot1.SetPosition(-18, 7, -10);
        zShot1.SetRotation(0, 0, 0, 1);
        SetBillboardColor(zShot1.rID, "whitetex.dds", 0.0, 0.0, 0.0, 0.0);
        zShotTriger = zShot1.rID;

        // Sun Rotation Button
        var vSun = new Resource();
        CreateBillboard(vSun.rID, "RotateSunButton.jpg", false);
        vSun.SetPosition(8, 10, -40);
        vSun.SetRotation(0, 0.7071, 0, 0.7071);
        sunID = vSun.rID;

        // SSAO Button
        var ssao = new Resource();
        CreateBillboard(ssao.rID, "SSAOButton.jpg", false);
        ssao.SetPosition(-6, 10, -40);
        ssao.SetRotation(0, -0.7071, 0, 0.7071);
        ssaoID = ssao.rID;

        ////////////////////////////////////////////////////////////////
        //Camera
        Camera.SetPosition(3, 9, -50);
        SetCameraID(Camera.rID);


        // Set Game Graphics Settings
        UseDOF(false);
        UseShadowMap(false);
        //UseSSAO(true);
        UseSSAO(false);
        UseHDR(true);


        SetChannelVolume(1, 0.5);
        SetChannelVolume(0, 1.0);
        PlayBGM("Content/Sounds/song1.mp3", 1.0, true, false);
        PlayOnce2D("Content/Sounds/cityambiance.wav", 0, true);
    }
}

var zDead = false;
var zExplode = false;
var bStartSun = false;
var bSSAO = true;
function GraphicsCoreUpdate(dt) {

    count += dt;

    //++++++++NEW ADDITIONS+++++++
    //var testMousepos = GetMousePos();
    //Picking Information
   // if (IsPicked(/*id*/))
        //do something

   // if (IsMouseButtonDown(/*0 or 1*/))
        //do something
    //++++++++++++++++++++++++++++

    // If Barrel Shot
    if (IsMouseButtonDown(0) == true && IsPicked(bID1) == true) {
        SetBillboardColor(bID1, "whitetex.dds", 0.0, 0.0, 0.0, 0.0);
        
        // Create Particles
        var particle = new Resource();
        particle.SetPosition(20, 6.5, -10);
        CreateParticle(particle.rID, "DefaultFire");
        CreateParticle(particle.rID, "DefaultSmoke");
        CreateParticle(particle.rID, "DefaultSpark");

        if (!zExplode)
        {
            PlayOnce3D("Content/Sounds/RocketExplosion.wav", 20, 10, -10, 0, false);
            PlayOnce3D("Content/Sounds/firecrackle.mp3", 20, 6.5, -10, 0, true);
        }

        zExplode = true;
    }

    // If Zombie Headshot
    if (IsMouseButtonDown(0) == true && IsPicked(zShotTriger) == true && zDead == false) {
        // Run fall animation
        Anim_Set(zShotAnim, 0, 0, "attack2", 0, 0, 0);
        Anim_Set(zShotAnim, 0, 0, "fall", 1.0, 1.0, 0.7);
        Anim_Play(0, zShotAnim);
        //zDead = true;

        PlayOnce3D("Content/Sounds/zombiemoan.mp3", -18, 3, -10, 0, false);

        // Create headshot spark
        CreateParticle(zShotAnim, "DefaultSpark");
    }

    // Toggle DOF On/OFF
    UseDOF(IsMouseButtonDown(1));
        

    //Tested input it works!
    //List of keys are at bottom of js file
    if (IsKeyDown(Keys.KEY_H) == true && count > .5) {
        ClearScene();
        StopAll(0); // Stop all SFX
        count = 0.00;
    }

    if (IsKeyDown(Keys.KEY_Y) == true && count > .5) {
        
        GraphicsDemo();

        count = 0.00;
    }

    //////////////////////////////////////////////////////////////
    // Set Camera Rotation
    var NewRot = Camera.GetRotVec();
    if (IsKeyDown(Keys.KEY_I) == true) {
        NewRot.y -= 0.174/150;
    }
    if (IsKeyDown(Keys.KEY_K) == true) {
        NewRot.y += 0.174/150;
    }
    if (IsKeyDown(Keys.KEY_J) == true) {
        NewRot.x -= 0.174/150;
    }
    if (IsKeyDown(Keys.KEY_L) == true) {
        NewRot.x += 0.174/150;
    }
    Camera.SetRotationYawPitchRoll(NewRot.x, NewRot.y, NewRot.z);

    // Set Camera Position
    var Dir = Camera.GetRotVec();
    Dir.x = 0;
    Dir.y = 0;
    Dir.z = 0;
    if (IsKeyDown(Keys.KEY_A) == true) {
        Dir.x -= GetRightVecX(Camera.rID);//(10 *dt);
        Dir.y -= GetRightVecY(Camera.rID);//(10 *dt);
        Dir.z -= GetRightVecZ(Camera.rID);//(10 *dt);
    }
    if (IsKeyDown(Keys.KEY_D) == true) {
        Dir.x += GetRightVecX(Camera.rID);//(10 * dt);
        Dir.y += GetRightVecY(Camera.rID);//(10 * dt);
        Dir.z += GetRightVecZ(Camera.rID);//(10 * dt);
    }
    if (IsKeyDown(Keys.KEY_W) == true) {
        Dir.x += GetForwardVecX(Camera.rID);//(10 * dt);
        Dir.y += GetForwardVecY(Camera.rID);//(10 * dt);
        Dir.z += GetForwardVecZ(Camera.rID);//(10 * dt);
    }
    if (IsKeyDown(Keys.KEY_S) == true) {
        Dir.x -= GetForwardVecX(Camera.rID);//(10 * dt);
        Dir.y -= GetForwardVecY(Camera.rID);//(10 * dt);
        Dir.z -= GetForwardVecZ(Camera.rID);//(10 * dt);
    }
    Dir = Camera.Normalize(Dir.x, Dir.y, Dir.z);
    Dir.x *= 10 * dt;
    Dir.y = 0;  //*= 10 * dt;
    Dir.z *= 10 * dt;
    var NewPos = Camera.GetPos();
    Camera.SetPosition(NewPos.x + Dir.x, NewPos.y + Dir.y, NewPos.z + Dir.z);
    //////////////////////////////////////////////////////////////
   

    // Check for picked billboard to turn on sun movement
    if (IsMouseButtonDown(0) == true && IsPicked(sunID) == true) {
        if (bStartSun == false) {
            // Turn Shadow Map On
            SetBillboardColor(sunID, "RotateSunButton.jpg", 0.5, 0.5, 0.5, 1.0);
            bStartSun = true;
            UseShadowMap(true);
        }
        else {
            // Turn Shadow Map Off
            SetBillboardColor(sunID, "RotateSunButton.jpg", 1.0, 1.0, 1.0, 1.0);
            bStartSun = false;
            UseShadowMap(false);
        }
    }

    // Check for picked billboard to turn on/off ssao
    if (IsMouseButtonDown(0) == true && IsPicked(ssaoID) == true) {
        if (bSSAO == false) {
            // Turn Shadow Map On
            SetBillboardColor(ssaoID, "SSAOButton.jpg", 0.5, 0.5, 0.5, 1.0);
            bSSAO = true;
            UseSSAO(true);
        }
        else {
            // Turn SSAO Off
            SetBillboardColor(ssaoID, "SSAOButton.jpg", 1.0, 1.0, 1.0, 1.0);
            bSSAO = false;
            UseSSAO(false);
        }
    }

    // Rotate Light around the map
    if (bStartSun == true) {
        count++;
        SetTimeOfDay(0, 0, count);
    }

    //Test Accessing Resources in list
    if (IsKeyDown(Keys.KEY_SPACE) == true) {
        Anim_Play(1, ResourceList[0].rID);
    }
    if (IsKeyDown(Keys.KEY_B) == true) {
        Anim_Pause(1, ResourceList[0].rID);
    }

    count += dt*0.01;
    if (count > 360)
        count -= 360;
    Building2.SetRotationYawPitchRoll(count, 0, count);
}