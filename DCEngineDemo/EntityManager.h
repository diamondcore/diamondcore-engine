#pragma once

#include <Interfaces.h>
#include "../../DCEngineDemo/Game Objects/BaseGameEntity.h"
#include <map>

// forward declarations
class Terrain;

class EntityManager
{
	MainCore*	m_pMainCore;
	bool		m_bDisplayHiddenObj;

	std::map<int, std::shared_ptr<BaseGameEntity>>	m_EntityList;
	std::map<int, std::shared_ptr<BaseGameEntity>>	m_NonUpdateEntityList;
	std::vector<std::shared_ptr<BaseGameEntity>>	m_SpawnPointList;

	Terrain* m_pTerrain;

	// Set the default camera for a map
	std::shared_ptr<BaseGameEntity>	m_pDefaultCamera;

	std::string m_sLevelsDirectory;
	std::string m_sLevelSelection;

public:
	EntityManager(MainCore* pMainCore);
	~EntityManager();
	void ClearEntityList();
	void Shutdown();

	// Level Loading and Saving
	// Putting it here, because it needs to be accessed by multiple screens
	bool LoadLevel(char* levelName);
	void SaveLevel(char* levelName);
	void InitializeGameObjects(Vector3* playerPos);

	// initialized terrain
	void InitHavokTerrain();
	// gets terrain 
	Terrain* GetTerrain(){return m_pTerrain;}
	// destroys terrain
	void DestroyTerrain(){delete m_pTerrain; m_pTerrain = NULL;}

	void Update(float dt);

	// Accessors to the list of world entities
	void AddEntity(int id, std::shared_ptr<BaseGameEntity> pEntity);
	void RemoveEntity(int id);
	std::shared_ptr<BaseGameEntity>	GetEntity(int id);
	std::shared_ptr<BaseGameEntity>	GetEntityByBodyID(int id);

	// Spawn point funcitons
	void GetNextSpawnPoint(Vector3* pos, Quaternion* orientation);
	void ResetSpawnPoints();

	// Display hidden objects Flag
	bool	DisplayHiddenObjects();
	void	SetDisplayHiddenObject(bool enable);

	// Get default camera
	void	GetDefaultCamera(Vector3& pos, Quaternion& orientation);

	// Level Name Utilities
	std::string GetLevelsDirectory(){return m_sLevelsDirectory;}
	void SetLevelSelection(std::string level){m_sLevelSelection = level;}
	std::string GetLevelSelection();	// Only returns once, then it clears the selection
};