#include "StatTracker.h"

#include <fstream>
#include <sstream>
#include <assert.h>
#include <Windows.h>

#define STATS_MAKEHIDDEN 0

Stats* Stats::s_Instance = 0;
Stats* Stats::Instance()
{
	if(!s_Instance)
		s_Instance = new Stats;
	return s_Instance;
}

Stats::Stats()
{
	setDefaultStats(&m_sStats);
	setDefaultRanks(&m_sStats);
	m_sLastFileName = "";
	m_bLockStats = false;
	m_bLockRanks = false;
}

Stats::~Stats()
{
	//for(int i = 0; i < NUMRANKSLOTS; i++)
	//{
	//	delete &m_sStats.m_Rankings[i].first;
	//}
	//m_sStats.m_fAvgLifeSpan = 0;
}

void Stats::UpdateLifeSpan()
{
	m_sStats.m_fAvgLifeSpan = m_sStats.m_TotalTimePlayed
		/ (float)(max(1, m_sStats.m_iTotalDeaths + m_sStats.m_iNumRacesWon));
}

void Stats::setDefaultStats(GameStats *stats)
{
	stats->m_TotalTimePlayed = 0.0f;
	stats->m_TotalDamageTaken = 0.0f;
	stats->m_fAvgLifeSpan = 0.0f;
	stats->m_iTotalDeaths = 0.0f;
	stats->m_fLastRaceTime = 0.0f;
	stats->m_iTotalRaces = 0;
	stats->m_iMeteorKills = 0;
	stats->m_iTurretKills = 0;
	stats->m_iGeyserKills = 0;
	stats->m_iElectricKills = 0;
	stats->m_iNumCrushKills = 0;
	stats->m_iSuicides = 0;
	stats->m_iNumRageQuits = 0;
	stats->m_iNumRacesWon = 0;
	stats->m_fLeastDamageTaken = FLT_MAX;
	m_fCurrentDamageTaken = 0;
}

void Stats::setDefaultRanks(GameStats *stats, int SkipRank)
{
#if STATS_USERANKINGS
	std::string s = NOSHOWNAME;
	for(int i = SkipRank; i < NUMRANKSLOTS; i++)
	{
		stats->m_Rankings[i] = std::pair<std::string, float>(s, FLT_MAX);
	}
#endif
}

int Stats::InsertRanking(std::string name, float time)
{
	if(m_bLockRanks) return -1;

#if STATS_USERANKINGS
	name.resize(3, ' ');
	int i;
	for(i = 0; i < NUMRANKSLOTS; i++)
	{
		if(time < m_sStats.m_Rankings[i].second)
		{
			std::pair<std::string, float> newRank,
											tempRank;
			newRank.first = name;
			newRank.second = time;
			for(int j = i; j < NUMRANKSLOTS; j++)
			{
				tempRank = m_sStats.m_Rankings[j];
				m_sStats.m_Rankings[j] = newRank;
				newRank = tempRank;
			}
			return i;
		}
	}

#endif
	return -1;
}

void Stats::SetTotalDeaths(int num, bool isRelative)
{
	if(m_bLockStats) return;
	if(isRelative)
		m_sStats.m_iTotalDeaths += num;
	else
		m_sStats.m_iTotalDeaths = num;
	//m_tTotalDeathsCtrl->SetText("# of Deaths : " + std::to_string((_Longlong)m_sStats.m_iTotalDeaths));
	UpdateLifeSpan();
}

void Stats::SetTotalRaces(int num, bool isRelative)
{
	if(m_bLockStats) return;
	if(isRelative)
		m_sStats.m_iTotalRaces += num;
	else
		m_sStats.m_iTotalRaces = num;
	//m_tTotalRacesCtrl->SetText("# of Races : " + std::to_string((_Longlong)m_sStats.m_iTotalRaces));
}

void Stats::SetMeteorDeaths(int num, bool isRelative)
{
	if(m_bLockStats) return;
	if(isRelative)
		m_sStats.m_iMeteorKills += num;
	else
		m_sStats.m_iMeteorKills = num;
	//m_tMeteorCtrl->SetText("# Meteor Deaths : " + std::to_string((_Longlong)m_sStats.m_iMeteorKills));
}

void Stats::SetTurretDeaths(int num, bool isRelative)
{
	if(m_bLockStats) return;
	if(isRelative)
		m_sStats.m_iTurretKills += num;
	else
		m_sStats.m_iTurretKills = num;
	//m_tTurretCtrl->SetText("# Turret Deaths : " + std::to_string((_Longlong)m_sStats.m_iTurretKills));
}

void Stats::SetGeyserDeaths(int num, bool isRelative)
{
	if(m_bLockStats) return;
	if(isRelative)
		m_sStats.m_iGeyserKills += num;
	else
		m_sStats.m_iGeyserKills = num;
	//m_tGeyserCtrl->SetText("# Geyser Deaths : " + std::to_string((_Longlong)m_sStats.m_iGeyserKills));
}

void Stats::SetElectricityDeaths(int num, bool isRelative)
{
	if(m_bLockStats) return;
	if(isRelative)
		m_sStats.m_iElectricKills += num;
	else
		m_sStats.m_iElectricKills = num;
	//m_tElectricCtrl->SetText("# Electricity Deaths" + std::to_string((_Longlong)m_sStats.m_iElectricKills));
}

void Stats::SetCrushDeaths(int num, bool isRelative)
{
	if(m_bLockStats) return;
	if(isRelative)
		m_sStats.m_iNumCrushKills += num;
	else
		m_sStats.m_iNumCrushKills = num;
}

void Stats::SetSuicideDeaths(int num, bool isRelative)
{
	if(m_bLockStats) return;
	if(isRelative)
		m_sStats.m_iSuicides += num;
	else
		m_sStats.m_iSuicides = num;
	//m_tSuicideCtrl->SetText("# Suicide Deaths : " + std::to_string((_Longlong)m_sStats.m_iSuicides));
}

void Stats::SetNumRacesWon(int num, bool isRelative)
{
	if(m_bLockStats) return;
	if(isRelative)
		m_sStats.m_iNumRacesWon += num;
	else
		m_sStats.m_iNumRacesWon = num;
	//m_tNumRacesWonCtrl->SetText("# Races Won : " + std::to_string((_Longlong)m_sStats.m_iNumRacesWon));

	UpdateLifeSpan();
}

void Stats::SetNumRageQuits(int num, bool isRelative)
{
	if(m_bLockStats) return;
	if(isRelative)
		m_sStats.m_iNumRageQuits += num;
	else
		m_sStats.m_iNumRageQuits = num;
	//m_tRageQuitCtrl->SetText("# Rage Quits : " + std::to_string((_Longlong)m_sStats.m_iNumRageQuits));
}

void Stats::SetCurrentDamageTaken(float num, bool isRelative)
{
	if(m_bLockStats) return;
	if(isRelative)
		m_fCurrentDamageTaken += num;
	else
		m_fCurrentDamageTaken = num;
}

void Stats::ResetRace()
{
	if(m_bLockStats) return;
	SetCurrentDamageTaken(0, false);
	SetLastRaceTime(0);
}


void Stats::GenerateStatsFile(std::string filename, bool writeDefault)
{
	if(filename == "")
	{
		if(m_sLastFileName == "")
			return;
		else
			filename = m_sLastFileName;
	}
	m_sLastFileName = filename;

	if(writeDefault)
	{
		GameStats temp;
		setDefaultStats(&temp);
		setDefaultRanks(&temp);

		std::ofstream StatsFile;
		StatsFile.open(filename, std::ios::binary | std::ios::out);
		if(StatsFile.is_open())
		{
			StatsFile.write((char*)&temp, sizeof(temp));
			StatsFile.close();
		}
	}
	else
	{
		std::ofstream StatsFile;
		StatsFile.open(filename, std::ios::binary | std::ios::out);
		if(StatsFile.is_open())
		{
			m_sStats.m_iNumRageQuits++;
			StatsFile.write((char*)&m_sStats, sizeof(m_sStats));
			StatsFile.close();
			m_sStats.m_iNumRageQuits--;
		}
	}	
}

void Stats::ReadStatsFromFile(std::string filename, bool forceCreate)
{
	if(filename == "")
	{
		if(m_sLastFileName == "")
			return;
		else
			filename = m_sLastFileName;
	}

#if STATS_MAKEHIDDEN
	SetFileAttributes((LPCTSTR)m_sLastFileName.c_str(),
								FILE_ATTRIBUTE_NORMAL);
#endif

	std::ifstream StatsFile;
	StatsFile.open(filename, std::ios::binary | std::ios::in);
	if(StatsFile.is_open())
	{
		StatsFile.read((char*)&m_sStats, sizeof(m_sStats));

		StatsFile.close();
		m_sStats.m_iNumRageQuits++;
		std::ofstream outF;
		outF.open(filename, std::ios::binary | std::ios::out);
		outF.write((char*)&m_sStats, sizeof(m_sStats));
		outF.close();
		m_sStats.m_iNumRageQuits--;
		m_sLastFileName = filename;

#if STATS_MAKEHIDDEN
	assert(SetFileAttributes((LPCTSTR)m_sLastFileName.c_str(),
								FILE_ATTRIBUTE_HIDDEN));
#endif
	}
	else
	{
		if(forceCreate)
		{
			GenerateStatsFile(filename);
			StatsFile.open(filename, std::ios::binary | std::ios::in);
			if(StatsFile.is_open())
			{
				StatsFile.read((char*)&m_sStats, sizeof(m_sStats));
				StatsFile.close();
				m_sStats.m_iNumRageQuits++;
				std::ofstream outF;
				outF.open(filename, std::ios::binary);
				if(outF.is_open())
				{
					outF.write((char*)&m_sStats, sizeof(m_sStats));
					outF.close();
				}
				m_sStats.m_iNumRageQuits--;
				m_sLastFileName = filename;

#if STATS_MAKEHIDDEN
				assert(SetFileAttributes((LPCTSTR)m_sLastFileName.c_str(),
								FILE_ATTRIBUTE_HIDDEN));
#endif
			}
		}
		else
		{
			setDefaultStats(&m_sStats);
			setDefaultRanks(&m_sStats);
		}
	}
}

void Stats::WriteStatsToFile(std::string filename, bool forceCreate)
{
	if(filename == "")
	{
		if(m_sLastFileName == "")
			return;
		else
			filename = m_sLastFileName;
	}

#if STATS_MAKEHIDDEN
	SetFileAttributes((LPCTSTR)m_sLastFileName.c_str(),
								FILE_ATTRIBUTE_NORMAL);
#endif
	std::ofstream StatsFile;
	StatsFile.open(filename, std::ios::binary | std::ios::out);
	if(StatsFile.is_open())
	{
		StatsFile.write((char*)&m_sStats, sizeof(m_sStats));
		StatsFile.close();
		m_sLastFileName = filename;
	}
	else if(forceCreate)
	{
		GenerateStatsFile(filename, false);
	}

#if STATS_MAKEHIDDEN
	assert(SetFileAttributes((LPCTSTR)m_sLastFileName.c_str(),
							FILE_ATTRIBUTE_HIDDEN));
#endif
}

std::string Stats::GetRankName(int rankNum)
{
#if STATS_USERANKINGS
	if(rankNum >= 0 && rankNum < NUMRANKSLOTS)
		return m_sStats.m_Rankings[rankNum].first;
#endif
	return std::string("");
}

float Stats::GetRankTime(int rankNum)
{
#if STATS_USERANKINGS
	if(rankNum >= 0 && rankNum < NUMRANKSLOTS)
		return m_sStats.m_Rankings[rankNum].second;
#endif
	return 0;
}

