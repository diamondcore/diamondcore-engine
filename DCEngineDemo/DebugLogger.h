#pragma once

#include <iostream>
#include <fstream>
#include <iomanip>

#define USEDEBUGLOG 0
#define DbgLog DebugLogger::Instance()

class DebugLogger
{
private:
	std::ofstream m_sLogFile;
	DebugLogger(void);
public:
	static DebugLogger *Instance(){static DebugLogger instance; return &instance;}
	~DebugLogger(void);

	void Write(std::string message);
};

