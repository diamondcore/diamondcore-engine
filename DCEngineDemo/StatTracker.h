#ifndef __STATTRACKER_H
#define __STATTRACKER_H

//If the name beside a ranking equals NOSHOWNAME, the ranking will not be displayed
#define NOSHOWNAME "|NONE|"
#define NUMRANKSLOTS 10
#define SStats Stats::Instance()

#define STATS_USERANKINGS 1

#include <iostream>
#include <string>

	struct GameStats
{
	float m_TotalTimePlayed;
	float m_TotalDamageTaken;
	float m_fLeastDamageTaken;
	float m_fAvgLifeSpan;
	float m_fLastRaceTime;
	int m_iMeteorKills;
	int m_iTurretKills;
	int m_iGeyserKills;
	int m_iElectricKills;
	int m_iNumCrushKills;
	int m_iSuicides;
	int m_iTotalDeaths;
	int m_iTotalRaces;
	int m_iNumRacesWon;
	int m_iNumRageQuits;
#if STATS_USERANKINGS
	std::pair<std::string, float> m_Rankings[NUMRANKSLOTS];
#endif
};

class Stats
{
private:
	GameStats m_sStats;
	float m_fCurrentDamageTaken;
	std::string m_sLastFileName;
	static Stats* s_Instance;

	//If set to TRUE, statistics cannot be edited
	bool m_bLockStats;
	//if set to TRUE, ranks cannot be edited
	bool m_bLockRanks;

	Stats();

	void UpdateLifeSpan();

public:
	static Stats *Instance();
	~Stats();

	void GenerateStatsFile(std::string filename = "",bool writeDefault = true);
	void ReadStatsFromFile(std::string filename = "", bool forceCreate = true);
	void WriteStatsToFile(std::string filename = "", bool forceCreate = true);

	void setDefaultStats(GameStats *stats);
	void setDefaultRanks(GameStats *stats, int SkipRank = 0);

	void SetTotalTimePlayed(float time, bool isRelative = true){
		if(m_bLockStats) return;
		if(isRelative) m_sStats.m_TotalTimePlayed += time;
		else m_sStats.m_TotalTimePlayed = time;
		UpdateLifeSpan();}
	void SetTotalDamageTaken(float damage, bool isRelative = true){
		if(m_bLockStats) return;
		if(isRelative) m_sStats.m_TotalDamageTaken += damage;
		else m_sStats.m_TotalDamageTaken = damage;
		SetCurrentDamageTaken(damage, isRelative);}
	void SetLeastDamageTaken(float num){m_sStats.m_fLeastDamageTaken = num;}
	void SetLastRaceTime(float time){m_sStats.m_fLastRaceTime = time;}
	void SetTotalDeaths(int num, bool isRelative = true);
	void SetTotalRaces(int num, bool isRelative = true);
	void SetMeteorDeaths(int num, bool isRelative = true);
	void SetTurretDeaths(int num, bool isRelative = true);
	void SetGeyserDeaths(int num, bool isRelative = true);
	void SetElectricityDeaths(int num, bool isRelative = true);
	void SetCrushDeaths(int num, bool isRelative = true);
	void SetSuicideDeaths(int num, bool isRelative = true);
	void SetNumRacesWon(int num, bool isRelative = true);
	void SetNumRageQuits(int num, bool isRelative = true);
	void SetCurrentDamageTaken(float num, bool isRelative = true);
	int InsertRanking(std::string name, float time);

	void ResetRace();

	void ToggleRankLock(){m_bLockRanks = !m_bLockRanks;}
	void ToggleStatLock(){m_bLockStats = !m_bLockStats;}
	void SetRankLock(bool lock){m_bLockRanks = lock;}
	void SetStatLock(bool lock){m_bLockStats = lock;}

	//Getters
	std::string GetRankName(int rankNum);
	float GetRankTime(int rankNum);
	float GetTotalTimePlayed(){return m_sStats.m_TotalTimePlayed;}
	float GetTotalDamageTaken(){return m_sStats.m_TotalDamageTaken;}
	float GetLastRaceTime(){return m_sStats.m_fLastRaceTime;}
	float GetAverageLifespan(){return m_sStats.m_fAvgLifeSpan;}
	float GetLeastDamageTaken(){return m_sStats.m_fLeastDamageTaken;}
	float GetCurrentDamageTaken(){return m_fCurrentDamageTaken;}
	int GetTotalDeaths(){return m_sStats.m_iTotalDeaths;}
	int GetTotalRaces(){return m_sStats.m_iTotalRaces;}
	int GetNumMeteorDeaths(){return m_sStats.m_iMeteorKills;}
	int GetNumTurretDeaths(){return m_sStats.m_iTurretKills;}
	int GetNumGeyserDeaths(){return m_sStats.m_iGeyserKills;}
	int GetNumElectricityDeaths(){return m_sStats.m_iElectricKills;}
	int GetNumCrushDeaths(){return m_sStats.m_iNumCrushKills;}
	int GetNumSuicideDeaths(){return m_sStats.m_iSuicides;}
	int GetNumRageQuits(){return m_sStats.m_iNumRageQuits;}
	int GetNumRacesWon(){return m_sStats.m_iNumRacesWon;}

	bool GetRankLockState(){return m_bLockRanks;}
	bool GetStatLockState(){return m_bLockStats;}
};

#endif