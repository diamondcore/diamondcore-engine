#include <dinput.h>
#include "RaceState.h"
#include "../WorldManager.h"
#include "../Havok/HavokManager.h"
#include "../UI/ScreenManager.h"
#include "../Game Objects/Turbine.h"
#include "../UI/Screens/StatsScreen.h"
#include "../Game Objects/Engine.h"


// Defines for querying track for damage, 
#define NORMAL D3DXCOLOR(0,0,0,0)
#define DAMAGE D3DXCOLOR(1,0,0,1)
#define SLOW D3DXCOLOR(0,0,1,1)
#define SLIP D3DXCOLOR(0,1,0,1)


RaceState::RaceState()
{
	
}

// public destructor
RaceState* RaceState::Instance()
{
	static RaceState instance;

	return &instance;
}

RaceState::~RaceState()
{

}


//virtual method overrides
void RaceState::Enter(WorldManager* pWorld)
{
	SStats->SetStatLock(false);
	SStats->ResetRace();
}

void RaceState::Execute(WorldManager* pWorld, float dt)
{
	HM->Update(dt);

	// update the turbine
	GPM->GetTurbine()->Update(dt);
	// check for turbine effects


	// Update the world objects
	ScreenMan->GetEntityManager()->Update(dt);

	// checks the terrain for turbine behaviors
	CheckTerrain(dt);

	// updates gameplay needs of turbine
	UpdateTurbineGameplay(dt);

	// if race is over, transition to next state
	//GPM->GetFSM()->ChangeState(ENDRACE);
}
void RaceState::Exit(WorldManager* pWorld)
{
	// Make sure all attributes that may have been turned on are off
	auto GFX = ScreenMan->GetGraphics();
	GFX->UseDistortion(false);
	GFX->UseBlur(false);
}

void RaceState::HandleInput(IInputCore* inputState, float dt)
{
	//need to determine if we're using controller or keyboard
	if (GPM->GetEngine()->GetInputCore()->GetControllerIsConnected() )
	{
		// throttle
		Vector2 triggers = inputState->GetTriggers()*100.0f;
		GPM->GetTurbine()->SetLeftEngineSpeed(triggers.x);
		GPM->GetTurbine()->SetRightEngineSpeed(triggers.y);
	

		//Brake
		if (inputState->IsButtonDown(LEFT_BUMPER ))
		{
			GPM->GetTurbine()->BrakeLeftEngine(100);
		}
		else
		{
			GPM->GetTurbine()->BrakeLeftEngine(0);
		}
		if (inputState->IsButtonDown(RIGHT_BUMPER ))
		{
			GPM->GetTurbine()->BrakeRightEngine(100);
		}
		else
		{
			GPM->GetTurbine()->BrakeRightEngine(0);
		}

		auto GFX = ScreenMan->GetGraphics();
		// repair
		static bool useRepair = false;
		float energyLeft = GPM->GetTurbine()->GetCurrentEnergy() - 50*dt;
		if(inputState->IsButtonDown(X) && energyLeft > 0.0f)
		{
			GPM->GetTurbine()->ModifyCurrentEnergy(-50*dt);			
			GPM->GetTurbine()->Repair(dt);
			energyLeft -= 50*dt;	// Update energy left, for boosts check
			
			if(!useRepair)
			{
				GFX->UseDistortion(true);
				auto left = GPM->GetTurbine()->GetLeftEngine();
				auto right = GPM->GetTurbine()->GetRightEngine();
				GFX->CreateDistortionMesh(left->GetID(), &left->GetMeshName()[0], 0);
				GFX->CreateDistortionMesh(right->GetID(), &right->GetMeshName()[0], 0);
				GFX->SetDistortIntensity(0, 5.0f, 10.0f);
				Vector3 scale = left->GetScale() * 1.1f;
				GFX->SetDistortionMeshScale(left->GetID(), &left->GetMeshName()[0], 0, scale);
				GFX->SetDistortionMeshScale(right->GetID(), &right->GetMeshName()[0], 0, scale);
			}
			useRepair = true;
		}
		else if(useRepair)
		{
			auto left = GPM->GetTurbine()->GetLeftEngine();
			auto right = GPM->GetTurbine()->GetRightEngine();
			GFX->RemoveDistortionMesh(left->GetID(), &left->GetMeshName()[0]);
			GFX->RemoveDistortionMesh(right->GetID(), &right->GetMeshName()[0]);
			GFX->UseDistortion(false);
			useRepair = false;
		}

		// boost
		static bool useBoost = false;
		if (inputState->IsButtonDown(A) && energyLeft > 0.0f)
		{
			GPM->GetTurbine()->ModifyCurrentEnergy(-50*dt);
			GPM->GetTurbine()->SetLeftEngineSpeed(200);
			GPM->GetTurbine()->SetRightEngineSpeed(200);
			// Turn blur on
			if(!useBoost)
				ScreenMan->GetGraphics()->UseBlur(true);
			useBoost = true;
		}
		else if(useBoost)
		{
			// Turn blur off
			ScreenMan->GetGraphics()->UseBlur(false);
			useBoost = false;
		}

		Vector2 LeftStick = inputState->GetLeftStick();
		if (LeftStick.LengthSquared() > 0)
			GPM->GetTurbine()->PushTurbineDown(fabs(LeftStick.y),dt);

		Vector2 RightStick = inputState->GetRightStick();
		if (RightStick.LengthSquared() > 0)
			GPM->GetTurbine()->RotateTurbine(RightStick.x, dt);
		



	}
	else
	{
		// Keyboard movement
		if(inputState->IsKeyDown(DIK_Y))
			GPM->GetTurbine()->SetLeftEngineSpeed(100.0f);
		else
			GPM->GetTurbine()->SetLeftEngineSpeed(0.0f);
		if(inputState->IsKeyDown(DIK_U))
			GPM->GetTurbine()->SetRightEngineSpeed(100.0f);
		else
			GPM->GetTurbine()->SetRightEngineSpeed(0.0f);	

		//Brake
		if (inputState->IsKeyDown(DIK_H) )
		{
			GPM->GetTurbine()->BrakeLeftEngine(100);
		}
		else
		{
			GPM->GetTurbine()->BrakeLeftEngine(0);
		}
		if (inputState->IsKeyDown(DIK_J)	)
		{
			GPM->GetTurbine()->BrakeRightEngine(100);
		}
		else
		{
			GPM->GetTurbine()->BrakeRightEngine(0);
		}
		
		auto GFX = ScreenMan->GetGraphics();
		// repair
		static bool useRepair = false;
		float energyLeft = GPM->GetTurbine()->GetCurrentEnergy() - 50*dt;
		if(inputState->IsKeyDown(DIK_X) && energyLeft > 0.0f)
		{
			GPM->GetTurbine()->ModifyCurrentEnergy(-50*dt);			
			GPM->GetTurbine()->Repair(dt);
			energyLeft -= 50*dt;	// Update energy left, for boosts check
			
			if(!useRepair)
			{
				GFX->UseDistortion(true);
				auto left = GPM->GetTurbine()->GetLeftEngine();
				auto right = GPM->GetTurbine()->GetRightEngine();
				GFX->CreateDistortionMesh(left->GetID(), &left->GetMeshName()[0], 0);
				GFX->CreateDistortionMesh(right->GetID(), &right->GetMeshName()[0], 0);
				GFX->SetDistortIntensity(0, 5.0f, 10.0f);
				Vector3 scale = left->GetScale() * 1.1f;
				GFX->SetDistortionMeshScale(left->GetID(), &left->GetMeshName()[0], 0, scale);
				GFX->SetDistortionMeshScale(right->GetID(), &right->GetMeshName()[0], 0, scale);
			}
			useRepair = true;
		}
		else if(useRepair)
		{
			auto left = GPM->GetTurbine()->GetLeftEngine();
			auto right = GPM->GetTurbine()->GetRightEngine();
			GFX->RemoveDistortionMesh(left->GetID(), &left->GetMeshName()[0]);
			GFX->RemoveDistortionMesh(right->GetID(), &right->GetMeshName()[0]);
			GFX->UseDistortion(false);
			useRepair = false;
		}

		// boost
		static bool useBoost = false;
		if (inputState->IsKeyDown(DIK_SPACE) && energyLeft > 0.0f)
		{
			GPM->GetTurbine()->ModifyCurrentEnergy(-50 * dt);
			GPM->GetTurbine()->SetLeftEngineSpeed(200);
			GPM->GetTurbine()->SetRightEngineSpeed(200);
			// Turn blur on
			if(!useBoost)
				ScreenMan->GetGraphics()->UseBlur(true);
			useBoost = true;
		}
		else if(useBoost)
		{
			// Turn blur off
			ScreenMan->GetGraphics()->UseBlur(false);
			useBoost = false;
		}
	}


	

	// setpos
	/*if (inputState->IsNewKeyRelease(DIK_O))
	{
		GPM->GetTurbine()->SetPosition(0,0,0);
		GPM->GetTurbine()->SetOrientation(0,0,0);
	}*/
	

	

	// Calculate vibrations x is the main thruster vibrations
	// This should just be replaced with sound, vibrations should be used for running into objects or being shot at.
	//Vector2 vibration;
	//vibration.x = (triggers.x + triggers.y)/250.0f;	// Use between 0.0 - 0.5 large vibrater strength
	//inputState->VibrateController(dt, vibration);
}

bool compare(const D3DXCOLOR& lhs, const D3DXCOLOR& rhs)
{
	if(lhs.r == rhs.r && lhs.g == rhs.g &&
		lhs.b == rhs.b && lhs.a == rhs.a)
		return true;
	return false;
}
void RaceState::CheckTerrain(float dt)
{
	D3DXCOLOR result;
	static D3DXCOLOR prevResult = NORMAL;
	GPM->GetEngine()->GetGraphicsCore()->GetColorAt(GPM->GetTurbine()->GetPosition(),result);
	if( !compare(prevResult, result) )
	{
		if ( compare(result, DAMAGE) )
		{
			GPM->GetTurbine()->ApplyDamage( 300 * dt, e_SelfDamage);
			GPM->GetEngine()->GetInputCore()->VibrateController(1,Vector2(1.0f,1.0f) );
		}
		else if ( compare(result, SLOW) )
		{
			GPM->GetTurbine()->SetFriction(-65.0f);
		}
		else if ( compare(result, SLIP) )
		{
			GPM->GetTurbine()->SetFriction(-15.0f);
		}
		else if( compare(result, NORMAL) )
			GPM->GetTurbine()->SetFriction(-40.0f);

		prevResult = result;
	}
}

// Turbine race updates
void RaceState::UpdateTurbineGameplay(float dt)
{
	if (GPM->GetTurbine()->GetHealth() <= 0.0f)
	{
		//GPM->GetTurbine()->SetHealth(200.0f);
		//GPM->RestartTurbine();

		// Increment the proper death counter in StatTracker
		int deathDype = GPM->GetTurbine()->GetLastDamageType();
		switch(deathDype)
		{
		case e_MeteorDamage:
			SStats->SetMeteorDeaths(1);
			SStats->SetTotalDeaths(1);
			break;
		case e_TurretDamage:
			SStats->SetTurretDeaths(1);
			SStats->SetTotalDeaths(1);
			break;
		case e_GeyserDamage:
			SStats->SetGeyserDeaths(1);
			SStats->SetTotalDeaths(1);
			break;
		case e_ElectricDamage:
			SStats->SetElectricityDeaths(1);
			SStats->SetTotalDeaths(1);
			break;
		case e_CrushDamage:
			SStats->SetCrushDeaths(1);
			SStats->SetTotalDeaths(1);
			break;
		case e_SelfDamage:
			SStats->SetSuicideDeaths(1);
			SStats->SetTotalDeaths(1);
			break;
		default:
			SStats->SetTotalDeaths(1);
			break;
		}


		GPM->Death();
	}
}
