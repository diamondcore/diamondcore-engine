#include "EndRaceState.h"
#include "../UI/ScreenManager.h"
#include "../WorldManager.h"
#include "../UI/Screens/EndGameMenuScreen.h"
#include <dinput.h>


EndRaceState::EndRaceState(void)
{
}


EndRaceState::~EndRaceState(void)
{
}

EndRaceState* EndRaceState::Instance()
{
	static EndRaceState instance;

	return &instance;
}

//virtual method overrides
	// Enter's the state, initializes all needed vars
void EndRaceState::Enter(WorldManager* pWorld)
{
	SStats->SetStatLock(true);
	m_sStatScreen = new StatsScreen();
	GPM->SetStatsShowing(true);
	ScreenMan->AddScreen(m_sStatScreen);

	////We need to check to see if the player made it on the leaderboard here
	//if(SStats->GetLastRaceTime() < SStats->GetRankTime(NUMRANKSLOTS-1))
	//{
	//	//Add a method to input the name here.  using "PLR" as a placeholder
	//	m_sStatScreen->AttemptNewRank();
	//}
		
	
	if(SStats->GetLeastDamageTaken() > SStats->GetCurrentDamageTaken())
	{
		SStats->SetLeastDamageTaken(SStats->GetCurrentDamageTaken());
	}
	SStats->WriteStatsToFile();
}

// Called every tick, update loop
void EndRaceState::Execute(WorldManager* pWorld, float dt)
{
	
}

// Leaves the states, destroys all created memory
void EndRaceState::Exit(WorldManager* pWorld)
{
	ScreenMan->RemoveScreen(m_sStatScreen);
	SStats->SetStatLock(false);
}

void EndRaceState::HandleInput(IInputCore* inputState, float dt)
{
	if ( !GPM->IsStatsShowing())
	{

		// if player hits any button, goes to the race
		if (inputState->IsNewButtonRelease(X) ||  
			inputState->IsNewButtonRelease(Y) ||
			inputState->IsNewButtonRelease(A) ||
			inputState->IsNewButtonRelease(B) ||
			inputState->IsNewKeyRelease(DIK_SPACE)
			)
		{
			ScreenMan->AddScreen(new EndGameMenuScreen(GPM->GetScreen()) );
		}

	}

}

