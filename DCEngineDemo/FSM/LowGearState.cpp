#include "LowGearState.h"
#include "../Game Objects/Turbine.h"
#include "IdleGearState.h"
#include "../WorldManager.h"
#include "../UI/ScreenManager.h"

#define SND ScreenMan->GetMainCore()->GetSoundCore()


LowGearState::LowGearState(void)
{

}


LowGearState::~LowGearState(void)
{
}

LowGearState* LowGearState::Instance()
{
	static LowGearState instance;

	return &instance;
}

//virtual method overrides
	// Enter's the state, initializes all needed vars
void LowGearState::Enter(Turbine* pTurbine)
{
	SND->PlaySFX(pTurbine->iShiftSound,false,1);
	SND->PlaySFX(pTurbine->iLowGearSound,true,1);
}

// Called every tick, update loop
void LowGearState::Execute(Turbine* pTurbine, float dt)
{
	if (pTurbine->GetCurrentSpeed() <= 0.0f)
		pTurbine->m_pFSM->ChangeState(IDLEGEAR);
}

// Leaves the states, destroys all created memory
void LowGearState::Exit(Turbine* pTurbine)
{
	SND->PlaySFX(pTurbine->iShiftSound, false, 1);
	SND->StopSFX(pTurbine->iLowGearSound);
}

void LowGearState::HandleInput(IInputCore* inputState, float dt)
{
	
}

