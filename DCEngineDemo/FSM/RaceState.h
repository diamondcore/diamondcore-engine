#include "State.h"
#include "../UI/Screens/StatsScreen.h"

/////////////////////////
// GAMEPLAY STATE
////////////////////////

// forward declarations
class WorldManager;

#define RACE RaceState::Instance()

class RaceState : public State<WorldManager>
{
private:
	// private constructor
	RaceState();

public:

	// public destructor
	static RaceState* Instance();
	~RaceState();

	//////////////////////
	// UTILITIES
	/////////////////////
	//virtual method overrides
	// Enter's the state, initializes all needed vars
	virtual void Enter(WorldManager*);
	// Called every tick, update loop
	virtual void Execute(WorldManager*, float);
	// Leaves the states, destroys all created memory
	virtual void Exit(WorldManager*);
	// handles input
	virtual void HandleInput(IInputCore* inputState, float dt);
	// Checks the terrain for turbine behaviors
	void CheckTerrain(float dt);
	// Turbine race updates
	void UpdateTurbineGameplay(float dt);


	//////////////////////
	// ACCESSORS
	/////////////////////
	

	//////////////////////
	// MUTATORS
	/////////////////////
};