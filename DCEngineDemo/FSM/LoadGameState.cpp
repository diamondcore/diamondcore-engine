#include "LoadGameState.h"
#include "../WorldManager.h"
#include "../UI/ScreenManager.h"
#include "PreRaceState.h"
#include "../Controls/ControlsInclude.h"

// 1 = preload all meshes
// 0 = dynamicly load in meshes as they are created
#define LOAD_ALL 0

LoadGameState::LoadGameState(void)
{
	// Set default selected engines
	m_eSelectedPod		= MKIII_POD;
	m_eSelectedEngine	= MKIII_ENGINE;
	m_bGameLoaded		= false;
	m_bGFXLoaded		= false;
	m_bSoundLoaded		= false;
	m_iLoadProgress		= 0;
#if LOAD_ALL
	m_iTotalToLoad		= 31;
#else
	m_iTotalToLoad		= 7;
#endif
}


LoadGameState::~LoadGameState(void)
{
}

LoadGameState* LoadGameState::Instance()
{
	static LoadGameState instance;

	return &instance;
}

//virtual method overrides
// Enter's the state, initializes all needed vars
void LoadGameState::Enter(WorldManager* pWorld)
{
	DbgLog->Write("Entering LoadGameState...\nDeleting Camera Controls");
	CamMgr->DeleteAllCamControls();
	// init each module
	// Graphics
	if(!m_bGameLoaded)
	{
		DbgLog->Write("Game Not loaded!...");
		m_bGFXLoaded = LoadGFX(pWorld);
	}
	else
	{
		DbgLog->Write("Game already loaded");
	}
	// Sound
	if(!m_bSoundLoaded)
	{
		DbgLog->Write("Sound not loaded!...");
		m_bSoundLoaded = LoadSound(pWorld);
	}
	else
	{
		DbgLog->Write("Sound already loaded");
	}
	
	DbgLog->Write("Assigning LoadGameObjects to m_bGameLoaded");
	// Gameplay
	m_bGameLoaded = LoadGameObjects(pWorld);
}
// Called every tick, update loop
void LoadGameState::Execute(WorldManager* pWorld, float dt)
{
	// if everything loaded, exit this state
	if (m_bGFXLoaded && m_bSoundLoaded && m_bGameLoaded)
	{

		DbgLog->Write("Done Loading, Changing state to PreRace");
		pWorld->GetFSM()->ChangeState(PRERACE);
	}
	else
		Enter(pWorld);
}
// Leaves the states, destroys all created memory
void LoadGameState::Exit(WorldManager* pWorld)
{
	DbgLog->Write("Done!");
}

// Loads Graphics returns a 1 if successful
bool LoadGameState::LoadGFX(WorldManager* pWorld)
{
	DbgLog->Write("Loading GFX...\n");
	auto MEM = ScreenMan->GetMainCore()->m_mMainMemory;
	auto GFX = ScreenMan->GetGraphics();
	static int id = MEM->CreateSharedResource();

	// Load in sections
	switch(m_iLoadProgress)
	{
	case 0:
		{
			GFX->CreateMesh(id, "MKIII_Pod.X", false);
			GFX->RemoveMesh(id, "MKIII_Pod.X");
			m_iLoadProgress++;
			return false;
		}
	case 1:
		{
			GFX->CreateMesh(id, "MKIII_Engine.X", false);
			GFX->RemoveMesh(id, "MKIII_Engine.X");
			m_iLoadProgress++;
			return false;
		}
	case 2:
		{
			GFX->CreateMesh(id, "MKIV_Pod.X", false);
			GFX->RemoveMesh(id, "MKIV_Pod.X");
			m_iLoadProgress++;
			return false;
		}
	case 3:
		{
			GFX->CreateMesh(id, "MKIV_Engine.X", false);
			GFX->RemoveMesh(id, "MKIV_Engine.X");
			m_iLoadProgress++;
			return false;
		}
	case 4:
		{
			GFX->CreateMesh(id, "X95_Pod.X", false);
			GFX->RemoveMesh(id, "X95_Pod.X");
			m_iLoadProgress++;
			return false;
		}
	case 5:
		{
			GFX->CreateMesh(id, "X95_Engine.X", false);
			GFX->RemoveMesh(id, "X95_Engine.X");
			m_iLoadProgress++;
			return false;
		}
	case 6:
		{
			GFX->LoadTexture("SmallLogo.png");
			GFX->LoadTexture("BackgroundImage.png");
			m_iLoadProgress++;
			return false;
		}

#if LOAD_ALL
	case 7:
		{
			GFX->CreateMesh(id, "Turret_GroundBase.X", false);
			GFX->RemoveMesh(id, "Turret_GroundBase.X");
			m_iLoadProgress++;
			return false;
		}
	case 8:
		{
			GFX->CreateMesh(id, "Turret_Mainbody.X", false);
			GFX->RemoveMesh(id, "Turret_Mainbody.X");
			m_iLoadProgress++;
			return false;
		}
	case 9:
		{
			GFX->CreateMesh(id, "Turret_Cannons.X", false);
			GFX->RemoveMesh(id, "Turret_Cannons.X");
			m_iLoadProgress++;
			return false;
		}
	case 10:
		{
			GFX->CreateMesh(id, "Gyser.X", false);
			GFX->RemoveMesh(id, "Gyser.X");
			m_iLoadProgress++;
			return false;
		}
	case 11:
		{
			GFX->CreateMesh(id, "Rock5.X", false);
			GFX->RemoveMesh(id, "Rock5.X");
			m_iLoadProgress++;
			return false;
		}
	case 12:
		{
			GFX->CreateMesh(id, "Electrical Conductor.X", false);
			GFX->RemoveMesh(id, "Electrical Conductor.X");
			m_iLoadProgress++;
			return false;
		}
	case 13:
		{
			GFX->CreateMesh(id, "Camera.X", false);
			GFX->RemoveMesh(id, "Camera.X");
			m_iLoadProgress++;
			return false;
		}
	case 14:
		{
			GFX->CreateMesh(id, "StartPoint.X", false);
			GFX->RemoveMesh(id, "StartPoint.X");
			m_iLoadProgress++;
			return false;
		}
	case 15:
		{
			GFX->CreateMesh(id, "barrel1.x", false);
			GFX->RemoveMesh(id, "barrel1.x");
			m_iLoadProgress++;
			return false;
		}
	case 16:
		{
			GFX->CreateMesh(id, "Box.x", false);
			GFX->RemoveMesh(id, "Box.x");
			m_iLoadProgress++;
			return false;
		}
	case 17:
		{
			GFX->CreateMesh(id, "BoxVolume.X", false);
			GFX->RemoveMesh(id, "BoxVolume.X");
			m_iLoadProgress++;
			return false;
		}
	case 18:
		{
			GFX->CreateMesh(id, "BoxDirVolume.X", false);
			GFX->RemoveMesh(id, "BoxDirVolume.X");
			m_iLoadProgress++;
			return false;
		}
	case 19:
		{
			GFX->CreateMesh(id, "Rock1.X", false);
			GFX->RemoveMesh(id, "Rock1.X");
			m_iLoadProgress++;
			return false;
		}
	case 20:
		{
			GFX->CreateMesh(id, "Rock2.X", false);
			GFX->RemoveMesh(id, "Rock2.X");
			m_iLoadProgress++;
			return false;
		}
	case 21:
		{
			GFX->CreateMesh(id, "Rock3.X", false);
			GFX->RemoveMesh(id, "Rock3.X");
			m_iLoadProgress++;
			return false;
		}
	case 22:
		{
			GFX->CreateMesh(id, "Rock4.X", false);
			GFX->RemoveMesh(id, "Rock4.X");
			m_iLoadProgress++;
			return false;
		}
	case 23:
		{
			GFX->CreateMesh(id, "Rock6.X", false);
			GFX->RemoveMesh(id, "Rock6.X");
			m_iLoadProgress++;
			return false;
		}
	case 24:
		{
			GFX->CreateMesh(id, "Smasher_Base.X", false);
			GFX->RemoveMesh(id, "Smasher_Base.X");
			m_iLoadProgress++;
			return false;
		}
	case 25:
		{
			GFX->CreateMesh(id, "Smasher_Head.X", false);
			GFX->RemoveMesh(id, "Smasher_Head.X");
			m_iLoadProgress++;
			return false;
		}
	case 26:
		{
			GFX->CreateMesh(id, "PalmTreeLarge.X", false);
			GFX->RemoveMesh(id, "PalmTreeLarge.X");
			m_iLoadProgress++;
			return false;
		}
	case 27:
		{
			GFX->CreateMesh(id, "PalmTreeSkinny.X", false);
			GFX->RemoveMesh(id, "PalmTreeSkinny.X");
			m_iLoadProgress++;
			return false;
		}
	case 28:
		{
			GFX->CreateMesh(id, "LargeLeafyPlant.X", false);
			GFX->RemoveMesh(id, "LargeLeafyPlant.X");
			m_iLoadProgress++;
			return false;
		}
	case 29:
		{
			GFX->CreateMesh(id, "SmallLeafyPlant.X", false);
			GFX->RemoveMesh(id, "SmallLeafyPlant.X");
			m_iLoadProgress++;
			return false;
		}
	case 30:
		{
			GFX->CreateMesh(id, "Sphere(33).X", false);
			GFX->RemoveMesh(id, "Sphere(33).X");
			m_iLoadProgress++;
			return false;
		}

#endif

	default:
		break;
	};

	// Done loading
	if(id >= 0)
	{
		DbgLog->Write("Done loading\n");
		MEM->FreeSharedResource(id);
		id = -1;
		m_bGFXLoaded = true;
	}
	return true;
}
// loads sounds, returns a 1 if successful
bool LoadGameState::LoadSound(WorldManager* pWorld)
{

	return true;
}
// loads game objects and places them in the world
bool LoadGameState::LoadGameObjects(WorldManager* pWorld)
{
	DbgLog->Write("Loading Game Objects...\nInitializing Havok");
	// TODO: need a way to get the selected pod and engine
	//	 likely via gameplaymenu or screenmanager
	
	HM->Init();
	DbgLog->Write("Initializing Havok Terrain");
	ScreenMan->GetEntityManager()->InitHavokTerrain();
	
	//pWorld->LoadTurbine(X95_POD,X95_ENGINE);
	//pWorld->LoadTurbine(MKIII_POD,MKIII_ENGINE);
	//pWorld->LoadTurbine(MKIV_POD,MKIV_ENGINE);
	DbgLog->Write("Loading Turbine...");
	pWorld->LoadTurbine((Pod_Type)m_eSelectedPod, (Engine_Type)m_eSelectedEngine);


	/*
	HOW TO LOAD A DECAL:

	ScreenMan->GetGraphics()->AddDecal(int& returnID,
										int SharedID,
										char*  meshName,
										char* textureName,
										bool isDynamic,
										bool lockToObject,
										float maxAge,
										D3DXVECTOR3 pos,
										D3DXVECTOR3 target,
										D3DXVECTOR3 up);

	ARGUMENT DESCRIPTIONS:

		returnID		: returns the ID number of the new decal in this variable.
		sharedID		: Which element in shared memory you want to link the decal up to.
		meshName		: The name of the mesh we want the decal to be based on.
								Needs to be the same mesh of the object we are projecting it onto.
		textureName		: The name of the texture that we want the decal to apply.
		isDynamic		: Determines if the decal is persistent (exists until manually cleared),
								or dynamic (may be deleted if too many dynamic decals are in use).
		lockToObject	: Determines if the decal is locked to the object (meaning it will move with the object),
								or free (the decal moved independently of the object).
		maxAge			: If the decal is dynamic, determines how many seconds the decal will last before disappearing.
								Set to 0.0f to disable aging.
		pos				: Where you want to project the decal from.
								If decal is locked, this must be a position relative to the object.
		target			: Where on the object you are projecting the decal to.
								If decal is locked, this must be a position relative to the object.
		up				:  The up vector of the decal.  Determines rotation.
	*/

	/*
	HOW TO ADD A DISTORTION MESH:

	ScreenMan->GetGraphics()->CreateDistortionMesh(int sharedID,
													char* meshName,
													int groupID);

	ARGUMENT DESCRIPTIONS:

		sharedID		: Which element in shared memory you want to link the distortion mesh up to.
		meshName		: The name of the mesh we want to use.
		groupID			: Which group of distortion meshes (a collection of meshes that share distortion parameters)
								That you want to add the new mesh to.
	*/

	/*
	HOW TO ADD A LIGHTNING BOLT MESH:

	ScreenMan->GetGraphics()->AddBolt(int& boltID, int groupID, int numSegments);

	BEFORE MAKING A LIGHTNING BOLT OBJECT, AT LEAST 1 LIGHTNING BOLT MESH MUST BE CREATED!
	Returns false if the bolt was unable to be created (not enough segments, too high-poly)

	ARGUMENT DESCRIPTIONS:

		boltID		: Returns the ID of the new lightning bolt mesh.
		groupID		: The ID of the group we want to add this bolt to.
		numSegments	: How many arc segments we want the mesh to have.


	HOW TO ADD A LIGHTNING BOLT OBJECT

	ScreenMan->GetGraphics()->AddBoltObject(int &ID,
											D3DXVECTOR3 startPos,
											D3DXVECTOR3 endPos,
											float boltSize,
											float ArcAmount,
											float ArcMidpoint,
											UINT selectionType,
											int selectionID,
											D3DXVECTOR4 color);

	ARGUMENT DESCRIPTIONS:
		
		ID				: Returns the ID of the new lightning bolt object.
		startPos		: Where you want the lightning object to arc from.
		endPos			: Where you want the lightning object to arc to.
		boltSize		: How thick you want the bolt to be.
		ArcAmount		: How much arcing - how much the mesh segments are allowed to move from their initial position -
							you want.
		ArcMidpoint		: Lightning arcs the most in the midpoint, tapering down to 0 at the ends.
								This argument allows us to set that midpoint (0.5 for middle of mesh).
		selectionType	: Whether we want the object to reference a specific lightning mesh, or a specific group of lightning meshes (for randomization).
		selectionID		: Based on Selection type, the ID of the lightning mesh we want to use, or the groupID of meshes we want to use.
		color			: What color we want the lightning object to be.

	*/

	

	

	return true;
}


