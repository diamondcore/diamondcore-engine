#include "State.h"

/////////////////////////
// PRERACE STATE
////////////////////////
/*
	Does any setup or display that isn't done during the race

*/

// forward declarations
class WorldManager;

#define PRERACE PreRaceState::Instance()

class PreRaceState : public State<WorldManager>
{
private:
	// private constructor
	PreRaceState();

	

	
public:

	// public destructor
	static PreRaceState* Instance();
	~PreRaceState();

	//virtual method overrides
	// Enter's the state, initializes all needed vars
	virtual void Enter(WorldManager* pWorld);
	// Called every tick, update loop
	virtual void Execute(WorldManager* pWorld, float dt);
	// Leaves the states, destroys all created memory
	virtual void Exit(WorldManager* pWorld);
	virtual void HandleInput(IInputCore* inputState, float dt);


	
};