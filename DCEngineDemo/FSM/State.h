#pragma once
#include "../../DiamondCoreInclude.h"

// for this fsm, entity_type will pass in maincore for access to 
//	the different parts of the engine
template <typename entity_type>
class State
{
public:

	//game entity methods
	virtual void Enter(entity_type*) = 0;
	virtual void Execute(entity_type*, float) = 0;
	virtual void Exit(entity_type*) = 0;
	virtual void HandleInput(IInputCore* inputState, float dt) = 0;


	

	
	

	


	
};