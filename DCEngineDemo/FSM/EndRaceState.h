#include "State.h"
#include "../UI/Screens/StatsScreen.h"

/////////////////////////
// ENDRACE STATE
////////////////////////
/*
	Final state for gameplay - lets you display 
	stats, do fly-abouts, etc

*/

// forward declarations
class WorldManager;


#define ENDRACE EndRaceState::Instance()

class EndRaceState : public State<WorldManager>
{
private:
	// private constructor
	EndRaceState();

	int m_iSpinCamID;	

	StatsScreen *m_sStatScreen;

public:

	// public destructor
	static EndRaceState* Instance();
	~EndRaceState();

	//virtual method overrides
	// Enter's the state, initializes all needed vars
	virtual void Enter(WorldManager* pWorld);
	// Called every tick, update loop
	virtual void Execute(WorldManager* pWorld, float dt);
	// Leaves the states, destroys all created memory
	virtual void Exit(WorldManager* pWorld);
	virtual void HandleInput(IInputCore* inputState, float dt);


	
};