#include "State.h"

#include "../DebugLogger.h"

/////////////////////////
// LOADGAME STATE
////////////////////////
/*
	Loads all needed assets for the game
*/

// forward declarations
class WorldManager;

#define LOADGAME LoadGameState::Instance()

class LoadGameState : public State<WorldManager>
{
private:
	// private constructor
	LoadGameState();

	// load checks
	bool m_bGFXLoaded, m_bSoundLoaded, m_bGameLoaded;
	int		m_iLoadProgress;
	int		m_iTotalToLoad;

	int		m_eSelectedPod;
	int		m_eSelectedEngine;
	
public:

	static LoadGameState* Instance();
	
	// public destructor
	~LoadGameState();

	//virtual method overrides
	// Enter's the state, initializes all needed vars
	virtual void Enter(WorldManager* pWorld);
	// Called every tick, update loop
	virtual void Execute(WorldManager* pWorld, float dt);
	// Leaves the states, destroys all created memory
	virtual void Exit(WorldManager* pWorld);
	// handle input - doesn't do anything in load but has to be overloaded
	virtual void HandleInput(IInputCore* inputState, float dt){}


	// Loads Graphics returns a 1 if successful
	bool LoadGFX(WorldManager* pWorld);
	// loads sounds, returns a 1 if successful
	bool LoadSound(WorldManager* pWorld);
	// loads game objects and places them in the world
	bool LoadGameObjects(WorldManager* pWorld);

	// set the selected pod and engine to be loaded
	void SetSelectedTurbine(int pod, int engine){m_eSelectedPod = pod; m_eSelectedEngine = engine;}
	// gets the load progress
	float GetLoadProgress(){return (float)m_iLoadProgress/(float)m_iTotalToLoad;}
};