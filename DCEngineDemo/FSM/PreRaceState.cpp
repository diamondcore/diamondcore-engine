#include "PreRaceState.h"
#include "../WorldManager.h"
#include "RaceState.h"


PreRaceState::PreRaceState(void)
{
}


PreRaceState::~PreRaceState(void)
{
}

PreRaceState* PreRaceState::Instance()
{
	static PreRaceState instance;

	return &instance;
}

//virtual method overrides
	// Enter's the state, initializes all needed vars
void PreRaceState::Enter(WorldManager* pWorld)
{

}

// Called every tick, update loop
void PreRaceState::Execute(WorldManager* pWorld, float dt)
{
	pWorld->GetFSM()->ChangeState(RACE);
}

// Leaves the states, destroys all created memory
void PreRaceState::Exit(WorldManager* pWorld)
{

}

void PreRaceState::HandleInput(IInputCore* inputState, float dt)
{
	// if player hits any button, goes to the race
	if (inputState->IsNewButtonRelease(X) ||  
		inputState->IsNewButtonRelease(Y) ||
		inputState->IsNewButtonRelease(A) ||
		inputState->IsNewButtonRelease(B)
		)
		GPM->GetFSM()->ChangeState(RACE);
}

