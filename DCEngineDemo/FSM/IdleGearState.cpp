#include "IdleGearState.h"
#include "../Game Objects/Turbine.h"
#include "LowGearState.h"
#include "../WorldManager.h"
#include "../UI/ScreenManager.h"

#define SND ScreenMan->GetMainCore()->GetSoundCore()

IdleGearState::IdleGearState(void)
{
	
}


IdleGearState::~IdleGearState(void)
{
}

IdleGearState* IdleGearState::Instance()
{
	static IdleGearState instance;

	return &instance;
}

//virtual method overrides
	// Enter's the state, initializes all needed vars
void IdleGearState::Enter(Turbine* pTurbine)
{
	SND->PlaySFX(pTurbine->iIdleEngineSound, true, 1);
}
// Called every tick, update loop
void IdleGearState::Execute(Turbine* pTurbine, float dt)
{
	if (pTurbine->GetCurrentSpeed() > 0)
		pTurbine->m_pFSM->ChangeState(LOWGEAR);
}

// Leaves the states, destroys all created memory
void IdleGearState::Exit(Turbine* pTurbine)
{
	SND->StopSFX(pTurbine->iIdleEngineSound);
}

void IdleGearState::HandleInput(IInputCore* inputState, float dt)
{
	
}

