#include "State.h"

/////////////////////////
// Low Gear STATE
////////////////////////
/*
	sounds playing while in low gear

*/

// forward declarations
class Turbine;

#define IDLEGEAR IdleGearState::Instance()

class IdleGearState : public State<Turbine>
{
private:
	// private constructor
	IdleGearState();

	

	
public:

	// public destructor
	static IdleGearState* Instance();
	~IdleGearState();

	//virtual method overrides
	// Enter's the state, initializes all needed vars
	virtual void Enter(Turbine* pTurbine);
	// Called every tick, update loop
	virtual void Execute(Turbine* pTurbine, float dt);
	// Leaves the states, destroys all created memory
	virtual void Exit(Turbine* pTurbine);
	virtual void HandleInput(IInputCore* inputState, float dt);


	
};