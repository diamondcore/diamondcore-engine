#pragma once

#include "IControl.h"
#include <vector>

enum TransitionState
{
	COMPLETE_IN,
	COMPLETE_OUT,
	TRANSITIONING_IN,
	TRANSITIONING_OUT
};

enum TransitionEffect
{
	SLIDEUP,
	SLIDEDOWN,
	SLIDELEFT,
	SLIDERIGHT,
	ALTERNATEVERTICAL,
	ALTERNATEHORIZONTAL
};

class TransitionController : public IControl
{
public:

	TransitionController(TransitionEffect effect, float transitionMs, bool beginOut = true);

	//IControl Methods
	void Update(float dt);
	void Initialize();

	void DoTransition();
	void BeginTransition();
	void EndTransition();
	inline TransitionState GetTransState() { return mTransState; }

	//TODO On LostDevice?

private:
	static Vector2 sScreenBounds;
	float mElapsedTransitionTime;
	const float mTimeToTransition;
	bool mBeginsOut;
	TransitionState mTransState;
	const TransitionEffect mTransEffect;
	// list of positions for controls to be at when fully transitioned in.
	vector<Vector2> mGoalPosition;
	// list of positions for controls to be at when fully transitioned out.
	vector<Vector2> mTransitionOutPositions;

	float calculateInterpWeight(int ctrlIndex);
};