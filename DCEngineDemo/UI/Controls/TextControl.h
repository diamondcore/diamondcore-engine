#pragma once

#include "IControl.h"

class SpriteFont;
class TextControl : public IControl
{
protected:
	std::stringstream mText;

private:
	std::string mFontName;
	D3DXCOLOR mColor;

	void calculateSize();

public:
	TextControl(const char* fontName, const char* text = "");
	TextControl(const char* fontName, const char* text, Vector2 position);
	void SetColor(float r, float g, float b, float a = 1.0f)
	{
		mColor = D3DXCOLOR(r, g, b, a);
	}

	void Draw(IGraphicsCore* gDevice);
	void Initialize();
	void SetText(std::string text) { mText.str(text); calculateSize(); }

	Vector2 GetSize() { return mSize; }
};