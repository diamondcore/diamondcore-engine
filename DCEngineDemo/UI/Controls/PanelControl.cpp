#include "PanelControl.h"
#include <cfloat>

void PanelControl::LayoutTileArray(D3DXVECTOR3 padding, D3DXVECTOR3 tileSize)
{
	POINT count;
	count.x = GetSize().x / ( tileSize.x + padding.x);
	count.y = GetSize().y / ( tileSize.y + padding.y );

    for ( int row = 0; row < count.y; row ++ )
    {
        for ( int column = 0; column < count.x; column++ )
        {
            int id = column + row * count.x;

            if (id >= mChildren.size())
                break;

			IControl* child;

			int idCounter = 0;
			static list<IControl*>::const_iterator it;
			it = mChildren.begin();
			child = (*it);

			while ( idCounter != id )
			{
				it++;
				child = (*it);
				idCounter++;
			}

            child->SetPosition(Vector2(column * tileSize.x + (column + 1.0f ) * padding.x, row * tileSize.y + (row + 1.0f) * padding.y));
        }
    }
}

void PanelControl::LayoutRow(float xSpacing)
{
	float size = 0;
	int count = 0;

	list<IControl*>::const_iterator it = mChildren.begin();
	for (; it!= mChildren.end(); it++)
	{
		(*it)->SetPosition(Vector2(size + xSpacing, GetPosition().y));

		count++;
		size += (*it)->GetSize().x;
	}
}

void PanelControl::LayoutColumn(float ySpacing)
{
	float size = 0;
	int count = 0;

	list<IControl*>::const_iterator it = mChildren.begin();
	for (; it!= mChildren.end(); it++)
	{
		auto newPos = Vector2(GetPosition().x, size);
		(*it)->SetPosition(newPos);

		count++;
		size += (*it)->GetSize().y + ySpacing;
	}

	SetSize(GetSize().x, max(mSize.y, size));
}

void PanelControl::CenterHorizontal()
{
	// Get the largest X size
	float largestX = FLT_MIN;
	list<IControl*>::const_iterator it = mChildren.begin();
	for (; it!= mChildren.end(); it++)
	{
		auto size = (*it)->GetSize();

		if (size.x > largestX)
			largestX = size.x;
	}

	if (mSize.x == 0)
		SetSize(largestX, mSize.y);

	it = mChildren.begin();
	for (; it!= mChildren.end(); it++)
	{
		auto childSize = (*it)->GetSize();
		auto centeredX = ((mSize - childSize) * 0.5f).x;
		auto childPos = (*it)->GetPosition();
		(*it)->SetPosition(centeredX, childPos.y);
	}
}