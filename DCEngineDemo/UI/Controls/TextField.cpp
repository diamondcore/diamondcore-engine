#include "TextField.h"
#include <Interfaces.h>
#include <dinput.h>

TextField::TextField(Vector2 size, std::string initialText) :
	IControl(),
	mControlDirty(true),
	mCharSize(10),
	mMaxCharacters(-1)
{
	SetSize(size);
	UserData = NULL;
	mBlinkTimer = 0.0f;

	mBackgroundColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mCursorColor = D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);
	mFontColor = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f);

	mCursorSize = Vector2(2, mSize.y - 2);

	mTextSize = 10;

	mCurrentText << initialText;

	mCursorPosition = initialText.size();
}

void TextField::Initialize()
{
	// Calculate initial scale
	// it's from a 1x1 texture

	// ... a 1x1 texture makes the 
	// math here for scaling insanely easy
	SetScale(Vector2(mSize.x, mSize.y));

}

void TextField::Update(float dt)
{
	if (!IsActive())
		return;

	mBlinkTimer += dt;

	if (mBlinkTimer >= 1.0f)
		mBlinkTimer = 0.0f;

	IControl::Update(dt);
}

void TextField::Draw(IGraphicsCore* gDevice)
{
	if (!IsVisible())
	{
		IControl::Draw(gDevice);
		return;
	}

	// make the cursor blink. For great justice.
	if (IsActive() && mBlinkTimer < 0.5f)
	{
		// Calculate Cursor's Render matrix 
		auto cursorMatrix = D3DXMatrixIdentity(&D3DXMATRIX());

		auto parentGlobalPos = GetGlobalPosition();
		parentGlobalPos += Vector2(mCursorPosition * mCharSize, 1);

		D3DXMatrixTransformation2D(&mCursorMatrix, NULL, 0.0, &mCursorSize, 
										&D3DXVECTOR2(0, 0), 
										mRotationAngle,
										&parentGlobalPos);

		gDevice->Render2D("blankTexture.png", mCursorMatrix, mCursorColor);
	}

	auto globalPos = GetGlobalPosition();
	gDevice->RenderString("TimesNewRoman16", mCurrentText.str().c_str(), globalPos, 1.0f-GetDepthPosition(), mFontColor);

	//gDevice->Render2D("blankTexture.png", GetRenderMatrix(), mBackgroundColor);

	IControl::Draw(gDevice);
}

IControl::HandleInputResult TextField::HandleInput(IInputCore* inputState, float dt)
{
	// Until Focus groups are implemented we'll have to hack something in.
	// Only consume input if the mouse cursor is inside of the field while you type.
	auto bounds = GetBounds();
	auto mouseIsInCtrlBounds = inputState->IsMouseInBounds(bounds);

	SetDisabled(!mouseIsInCtrlBounds);

	if (!mouseIsInCtrlBounds)
		return IControl::HandleInput(inputState, dt);

	auto releasedKeys = inputState->GetReleasedKeys();

	if (inputState->IsNewKeyRelease(DIK_RETURN))
	{
		if (OnEnter != NULL)
			OnEnter(this, UserData);

		return IControl::HandleInputResult::CONSUME;
	}

	if (inputState->IsNewKeyRelease(DIK_ESCAPE))
		return IControl::HandleInputResult::CONSUME;

	// A bit costly, but quick and works for now
	if (inputState->IsNewKeyRelease(DIK_BACK))
	{
		if (mCursorPosition > 0)
		{
			string foo(mCurrentText.str());
			foo.pop_back();
			mCurrentText.str(foo);
			mCurrentText.seekp (0, mCurrentText.end);
			mCursorPosition--;
		}

		return IControl::HandleInputResult::CONSUME;
	}

	// Check if we reached the max number of characters allowed
	if (mMaxCharacters >= 0 && mCursorPosition >= mMaxCharacters)
		return IControl::HandleInputResult::CONSUME;

	// TODO: Yuck.
	static HKL layout=GetKeyboardLayout(0);
	static unsigned short* buf = new unsigned short();

	bool shiftDown = inputState->IsKeyDown(DIK_LSHIFT) || inputState->IsKeyDown(DIK_RSHIFT);
	for (auto it = releasedKeys->begin(); it != releasedKeys->end(); it++)
	{
		if ((*it) == 42)
			continue;

		UINT vk = MapVirtualKeyEx((*it),1,layout);
		//TODO Process windows messages so that Shifted keys can be read.
		auto tst = ToAsciiEx(vk,(*it),(const BYTE*)inputState->GetKeyboardState(),buf,0,layout);

		//TODO: I probably have to error protect to make sure fucked up keys don't
		// end up getting shoved into the string buffer.
		if (shiftDown)
			mCurrentText << char(toupper(buf[0]));
		else
			mCurrentText << char(buf[0]);

		mCursorPosition++;
	}

	return IControl::HandleInputResult::CONSUME;
}

void TextField::Destroy()
{

	//delete mBaseFileName;
	IControl::Destroy();
}