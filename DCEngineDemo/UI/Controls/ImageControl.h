#pragma once

#include "IControl.h"

struct D3DXCOLOR;

class ImageControl : public IControl
{

private:
	char* mFilePath;
	Vector2 mOriginalSize;

public:
	ImageControl(char* texturePath);
	ImageControl(char* texturePath, Vector2 position);
	ImageControl(char* texturePath, Vector2 position, Vector2 size);

	void Draw(IGraphicsCore* gDevice);
	void SetTexture(char* newTex) { mFilePath = newTex; }
	void SetColor(float r, float g, float b, float a = 1.0f) { mColor = D3DXCOLOR(r, g, b, a); }

protected:
	D3DXCOLOR mColor;
	void RecalculateRenderMatrix();

};