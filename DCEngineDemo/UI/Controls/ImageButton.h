#pragma once

#include "ImageControl.h"
#include "TextControl.h"
#include <string>
#include <functional>

using namespace std;


class ImageButton : public IControl
{

public:
	void* UserData;
	function<void (ImageButton* sender, void* userData)> OnClicked;
	int XTextPadding;
	int YTextPadding;

	D3DXCOLOR mColor;

	enum ButtonState : int
	{
		IDLE = 0,
		HOVER = 1,
		PRESSED = 2,
		DISABLED = 3,
		RELEASED = 4
	};

	ImageButton(std::string baseFileName, std::string buttonText = "", std::string textFont = "");

	void Update(float dt);
	void Draw(IGraphicsCore* gDevice);
	IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt);
	void Initialize();

	//Note: This must be done before Initialize/AddChild
	void SetClickedSound(string pathToSound) { mButtonClickedSound = pathToSound;}
	inline void SetColor(D3DXCOLOR col) { mColor = col; }
	void SetToggle(bool useToggle);

	void Destroy();

protected:

	Vector2 mOriginalSize;
	void RecalculateRenderMatrix();
	string mButtonClickedSound;
	TextControl* mText;
	const string mBaseFileName;
	std::string mButtonText;
	std::string mFontName;
	std::string mStateTextures[4];
	char* mCurrentTexture;
	bool  mToggle;

	void ChangeState(ButtonState newState);

	ButtonState mCurrentState;
	ButtonState mPreviousState;
};