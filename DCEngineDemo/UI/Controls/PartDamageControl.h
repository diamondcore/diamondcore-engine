#pragma once

#include "ImageControl.h"

class PartDamageControl : public ImageControl
{

private:
	float m_DamagePercentage;

	Vector3 m_HealthyColor;
	Vector3 m_DamagedColor;

public:
	PartDamageControl(char* texturePath);

	void Update(float dt);
	void SetDamagePercentage(float amount) { m_DamagePercentage = amount; }
};