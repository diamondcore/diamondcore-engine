#pragma once

#include "PanelControl.h"

class TextControl;
class ImageControl;
class EnergyBarControl : public PanelControl
{

private:
	Vector3 m_vFullColor;
	Vector3 m_vEmptyColor;

	std::string m_cTitle;

	TextControl* m_tPowerLabel;
	ImageControl* m_iEnergyBar;

	float m_fPowerPercentage;
	Vector2 m_vFullBarSize;

public:
	EnergyBarControl(std::string title = std::string("POWER"));
	EnergyBarControl(Vector2 fullBarSize, std::string title = std::string("POWER"));

	void Update(float dt);

	void Initialize();

	// Should be a float between 0-1
	void SetPower(float amount) { m_fPowerPercentage = amount; }
	void SetColor(Vector3 empty, Vector3 full){m_vEmptyColor = empty; m_vFullColor = full;}
};