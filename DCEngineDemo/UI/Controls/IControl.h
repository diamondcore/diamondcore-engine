#pragma once

#include <assert.h>
#include <DCMathLib.h>
#include <math.h>
#include <functional>
#include <list>

using namespace std;

class IGraphicsCore;
class IInputCore;

class IControl
{
private:

	unsigned int mChildCount;
	
	Vector2 mPosition;
	Vector2 mGlobalPosition;
	float	mPositionDepth;

	IControl* mParent;

	bool mVisible;
	bool mDisabled;
	bool mInitialized;

	bool mRenderMatrixDirty;
	bool mGlobalPositionDirty;

	Vector2 mScale;

	D3DXMATRIX mTransform;

protected:
	float mRotationAngle;
	float mRotationDegrees;

	list<IControl*> mChildren;

	virtual void RecalculateRenderMatrix()
	{
		GetGlobalPosition();
		D3DXMatrixTransformation2D(&mTransform, NULL, 0.0, &mScale, 
									&D3DXVECTOR2(0, 0), 
									mRotationAngle,
									&D3DXVECTOR2(mGlobalPosition.x, mGlobalPosition.y));
		mTransform._43 = 1.0f - GetDepthPosition();	// Allows for 100 max layers
	}

	IControl()
	{
		mParent = NULL;
		mChildCount = 0;
		mRotationAngle = 0;
		D3DXMatrixIdentity(&mTransform);
		SetPosition(Vector2(0, 0));
		mPositionDepth = 0.01f;
		mInitialized = false;
		mDisabled = false;
		mVisible = true;
		mRenderMatrixDirty = true;
		mRotationDegrees = 0;
		mScale = Vector2(1.0f, 1.0f);
		DoTransitionHandler = true;
		mGlobalPositionDirty = true;
	}

	Vector2 mSize;

	Vector2 GetScale() { return mScale; }

	virtual ~IControl() {};

public:
	static Vector2 m_vScreenScale;

	enum HandleInputResult
	{
		NORMAL = 0x0,
		CONSUME = 0x1,
	};

	bool DoTransitionHandler;
	function<void ()> TransitionComplete;

	template<typename T>
	void SetPosition(T xPos, T yPos)
	{
		// TODO: Actually check if dirty and bail out early
		SetPosition(Vector2(xPos, yPos));
	}

	void SetPosition(Vector2 newPos)
	{
		Vector2 offset = newPos - mPosition;

		if (offset == Vector2::Zero())
			return;

		mPosition = mGlobalPosition = newPos;

		mRenderMatrixDirty = true;


		//TODO: This totally shouldn't be necessary anymore.
		// Instead just mark children as dirty so they can recalc
		// their render matrices.
		//Now walk through children and update their positions.
		list<IControl*>::const_iterator it = mChildren.begin();
		for (; it!= mChildren.end(); it++)
			(*it)->SetPosition((*it)->GetPosition());
		
		mRenderMatrixDirty = true;
	}

	Vector2 GetGlobalPosition()
	{
		// return early if we've already recalculated our position
		if (!mGlobalPositionDirty)
			return mGlobalPosition;

		mGlobalPosition = mPosition;

		auto parent = mParent;
		while(parent != NULL)
		{
			mGlobalPosition.x += parent->GetPosition().x;
			mGlobalPosition.y += parent->GetPosition().y;

			parent = parent->GetParent();
		}

		//mGlobalPositionDirty = false;

		return mGlobalPosition;
	}

	float GetDepthPosition()
	{
		if(mParent)
			return mPositionDepth + mParent->GetDepthPosition();
		return mPositionDepth;
	}

	void MoveTowardsFront(int numLayers = 1)
	{
		mPositionDepth += 0.01f * numLayers;	// Move forward by specified number of layers
	}

	void SetScale(Vector2 newScale)
	{
		if (newScale == mScale)
			return;

		mScale = newScale;

		mRenderMatrixDirty = true;
	}

	Vector2 GetPosition() { return mPosition; }

	void SetRotation(float newAngle) 
	{
		mRotationDegrees = newAngle;
		mRotationAngle = newAngle;

		mRenderMatrixDirty = true;
	}

	RECT GetBounds() 
	{
		RECT r;
		auto globalPos = GetGlobalPosition();

		r.left = globalPos.x*m_vScreenScale.x;
		r.top = globalPos.y*m_vScreenScale.y;
		r.right = r.left + mSize.x*m_vScreenScale.x;
		r.bottom = r.top + mSize.y*m_vScreenScale.y;

		return r;
	}

	virtual inline void SetSize(Vector2 newSize) { SetSize(newSize.x, newSize.y); }

	virtual void SetSize(int xSize, int ySize)
	{
		mSize.x = xSize;
		mSize.y = ySize;
		
		mRenderMatrixDirty = true;
	}

	virtual inline Vector2 GetSize() { return mSize; }

	virtual void AddChild(IControl* controlToAdd) 
	{
		mChildren.push_back(controlToAdd);
		mChildCount++;
		controlToAdd->SetParent(this);
		controlToAdd->Initialize();

		// TODO: Temporary hack to recalculate the Global Position when 
		// a child is added.
		controlToAdd->SetPosition(controlToAdd->GetPosition());
	}

	void RemoveChild(IControl* controlToAdd)
	{
		mChildren.remove(controlToAdd);
		mChildCount--;
	}

	void RemoveChildAt(int index)
	{	
		int counter = 0;
		list<IControl*>::const_iterator it = mChildren.begin();
		for (; it!= mChildren.end(); it++)
		{
			if (counter == index)
			{
				RemoveChild(*it);
				//mChildren.erase(it);
				return;
			}
		}
	}

	void SetParent(IControl* parent) { assert(mParent == NULL); mParent = parent; }
	IControl* GetParent() { return mParent; }

	unsigned int GetChildCount() { return mChildCount; }

	bool IsVisible() { return mVisible; }
	bool IsDisabled() { return mDisabled; }

	void SetDisabled(bool disabled) { mDisabled = disabled; }

	bool IsActive() { return !mDisabled && mVisible; }

	bool IsInitialized() { return mInitialized; }

	virtual void Initialize()
	{
		if (mInitialized)
			return;

		mInitialized = true;

		if (mChildCount == 0)
			return;

		list<IControl*>::const_iterator it;
		it = mChildren.begin();

		while ( it != mChildren.end() )
		{
			if (!(*it)->IsInitialized())
				(*it)->Initialize();
			it++;
		}
	}

	void SetVisible( bool visible )
	{
		mVisible = visible;

		if (mChildCount == 0)
			return;

		static list<IControl*>::const_iterator it;
		it = mChildren.begin();

		while ( it != mChildren.end() )
		{
			if (!(*it)->IsInitialized())
				(*it)->SetVisible(visible);
			it++;
		}

	}

	virtual void Draw(IGraphicsCore* gDevice)
	{
		if (mChildCount == 0 || !mVisible)
			return;

		// Draw from first added to last added
		list<IControl*>::const_iterator it = mChildren.begin();
		for (; it!= mChildren.end(); it++)
			(*it)->Draw(gDevice);

	}

	virtual void Update(float dt)
	{
		if (!mVisible)
			return;

		// Update from top to bottom.
		list<IControl*>::reverse_iterator it = mChildren.rbegin();
		for (; it!= mChildren.rend(); it++)
		{
			if (!(*it)->IsVisible())
				continue;

			(*it)->Update(dt);
		}
	}

	virtual HandleInputResult HandleInput(IInputCore* inputState, float dt)
	{
		if (!mVisible)
			return HandleInputResult::NORMAL;

		// Walk through children backwards. Last one drawn
		// should consume input.

		list<IControl*>::reverse_iterator it = mChildren.rbegin();
		for (; it!= mChildren.rend(); it++)
		{
			if ((*it)->HandleInput(inputState, dt) == HandleInputResult::CONSUME)
				return HandleInputResult::CONSUME;
		}

		return HandleInputResult::NORMAL;
	}

	D3DXMATRIX GetRenderMatrix()
	{
		if (mRenderMatrixDirty)
		{
			RecalculateRenderMatrix();
			mRenderMatrixDirty = false;
		}

		return mTransform;
	}

	virtual void Destroy()
	{
		if (mChildCount > 0)
		{
			list<IControl*>::const_iterator it;
			it = mChildren.begin();

			while ( it != mChildren.end() )
			{
				(*it)->Destroy();
				mChildren.erase(it);
				it = mChildren.begin();
			}
		}

		delete this;
	}
};