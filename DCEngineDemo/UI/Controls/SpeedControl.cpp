#include "SpeedControl.h"
#include <Interfaces.h>
#include "../../Graphics Core/Graphics Core/2D/SpriteFont.h"
#include "../ScreenManager.h"

SpeedControl::SpeedControl(float maxSpeed, const char* fontName) :
	TextControl(fontName, "000")
{
	m_fMaxSpeed = maxSpeed * 1.2f;
	m_fCurrentSpeed = 0;
}

void SpeedControl::SetCurrentSpeed(float curSpeed)
{
	if (m_fCurrentSpeed == (int)curSpeed)
		return;

	auto interpWeight = curSpeed / m_fMaxSpeed;
	int adjustedSpeed = MathUtil::Lerp(interpWeight, 0.0f, 999.0f);

	stringstream ss;
	ss << setw(3) << setfill('0') << adjustedSpeed;
	TextControl::SetText(ss.str());

	m_fCurrentSpeed = (int)curSpeed;
}