#pragma once

#include "TextControl.h"

class SpeedControl : public TextControl
{
private:

	float m_fMaxSpeed;
	int m_fCurrentSpeed;

public:

	void SetCurrentSpeed(float curSpeed);
	SpeedControl(float maxSpeed, const char* fontName);
};