#pragma once

#include "PanelControl.h"
#include "PartDamageControl.h"

class HealthControl : public PanelControl
{

private:

	PartDamageControl* m_Pod;
	PartDamageControl* m_LeftEngine;
	PartDamageControl* m_RightEngine;

public:
	HealthControl(char* podTexture, char* leftTexture, char* rightTexture);
	void SetPodDamage(float amount) { m_Pod->SetDamagePercentage(amount); }
	void SetLeftEngineDamage(float amount) { m_LeftEngine->SetDamagePercentage(amount); }
	void SetRightEngineDamage(float amount)  { m_RightEngine->SetDamagePercentage(amount); }
};