#pragma once

#include "IControl.h"
class PanelControl : public IControl
{
public:
	void LayoutTileArray(D3DXVECTOR3 padding, D3DXVECTOR3 tileSize);
	void LayoutRow(float xSpacing);
	void LayoutColumn(float ySpacing);
	void CenterHorizontal();

};