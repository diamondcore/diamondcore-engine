#include "ControllerTextField.h"
#include <Interfaces.h>
#include <dinput.h>

ControllerTextField::ControllerTextField(Vector2 size, std::string initialText) :
	TextField(size, initialText)
{
	 currentChar = 65;
}

void ControllerTextField::Initialize()
{
	TextField::Initialize();
	mCurrentText << static_cast<char>(currentChar);

}

IControl::HandleInputResult ControllerTextField::HandleInput(IInputCore* inputState, float dt)
{
	auto ctrlDisabled = mCursorPosition >= 4;
	SetDisabled(ctrlDisabled);

	if (ctrlDisabled)
		return IControl::HandleInput(inputState, dt);

	// A bit costly, but quick and works for now
	if (inputState->IsNewButtonRelease(B))
	{
		if (mCursorPosition > 0)
		{
			string foo(mCurrentText.str());
			foo.pop_back();
			mCurrentText.str(foo);
			mCurrentText.seekp (0, mCurrentText.end);
			mCursorPosition--;
		}

		return IControl::HandleInputResult::CONSUME;
	}

	// A bit costly, but quick and works for now
	if (inputState->IsNewButtonRelease(A))
	{
		mCursorPosition++;
		mCurrentText << static_cast<char>(currentChar);
		
		if (mCursorPosition >= 3 && OnEnter != NULL)
			OnEnter(this, UserData);

		return IControl::HandleInputResult::CONSUME;
	}

	bool cursorChange = false;

	if (inputState->IsNewButtonRelease(DPAD_UP))
	{
		cursorChange = true;
		currentChar++;
	}

	if (inputState->IsNewButtonRelease(DPAD_DOWN))
	{
		cursorChange = true;
		currentChar--;
	}

	if (currentChar > 122)
		currentChar = 65;

	if (currentChar < 65 )
		currentChar = 122;

	if (!cursorChange)
		return TextField::HandleInput(inputState, dt);

	string foo(mCurrentText.str());
	foo.pop_back();
	mCurrentText.str(foo);
	mCurrentText.seekp (0, mCurrentText.end);

	mCurrentText << static_cast<char>(currentChar);

	return IControl::HandleInputResult::CONSUME;
}