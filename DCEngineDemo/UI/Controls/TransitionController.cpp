#include "TransitionController.h"
#include "../ScreenManager.h"

Vector2 TransitionController::sScreenBounds;

TransitionController::TransitionController(TransitionEffect effect, float transTime, bool beginOut) 
	: IControl(), mTimeToTransition(transTime), mTransEffect(effect)
{
	sScreenBounds = ScreenMan->GetScreenSize();

	mBeginsOut = beginOut;
	mElapsedTransitionTime = 0;

	mTransState = beginOut ? COMPLETE_OUT : COMPLETE_IN;
}

void TransitionController::DoTransition()
{
	// Wait for an in-progress transition first.
	if (mTransState == TRANSITIONING_OUT || 
		mTransState == TRANSITIONING_IN)
		return;

	mTransState = mTransState == COMPLETE_IN ? TRANSITIONING_OUT : TRANSITIONING_IN;
}

void TransitionController::BeginTransition()
{
	assert(mTransState == COMPLETE_IN);

	mTransState = TRANSITIONING_OUT;
}

void TransitionController::EndTransition()
{
	assert(mTransState == COMPLETE_OUT);

	mTransState = TRANSITIONING_IN;
}

float TransitionController::calculateInterpWeight(int ctrlIndex)
{
	//TODO: Come up with own Curve equation, rather
	// Than using a linear one.
	ctrlIndex++;
	auto scale = ((float)ctrlIndex / (float)mChildren.size());
	scale = max(min(scale, 1.0f), 0.15f);

	float weight = min(((scale * mTimeToTransition)  -  mElapsedTransitionTime) / (scale * mTimeToTransition), 1.0f);

	weight = 1.0f - weight;

	//TODO: Move tweening code to a Util Function
	/*float s = weight * 2;
	if (s < 1) 
		weight = pow(s, 2) / 2;
	else
		weight = (-1 / 2.0 * (pow(s - 2, 2) + -1 * 2));*/

	weight = max(min(weight, 1.0f), 0.0f);

	return weight;
}

void TransitionController::Update(float dt)
{
	IControl::Update(dt);

	// We only have work to do if we're not mid transition.
	if (mTransState != TRANSITIONING_OUT && 
		mTransState != TRANSITIONING_IN )
		return;

	// Update our time.
	mElapsedTransitionTime = min(mElapsedTransitionTime + dt, mTimeToTransition);

	// Interpolate all of our children to where they need to go.
	list<IControl*>::const_iterator it = mChildren.begin();
	int index = 0; // Hack to index the list correctly.
	while ( it != mChildren.end() )
	{
		auto childCtrl = (*it);

		Vector2 targetPos;
		Vector2 startingPos;
		if (mTransState == TRANSITIONING_IN)
		{
			targetPos = mGoalPosition[index];
			startingPos = mTransitionOutPositions[index];
		}
		else
		{
			startingPos = mGoalPosition[index];
			targetPos = mTransitionOutPositions[index];
		}

		Vector2 newPos = ((targetPos - startingPos) * calculateInterpWeight(index)) + startingPos;

		childCtrl->SetPosition(newPos);

		it++;
		index++;
	}

	// Return early if we're not complete with the transition.
	if (mTimeToTransition != mElapsedTransitionTime)
		return;

	// We're complete. Mark as so.
	mTransState =  mTransState == TRANSITIONING_IN ? COMPLETE_IN: COMPLETE_OUT;
	mElapsedTransitionTime = 0;

	//TODO: Transition Complete events

}

void TransitionController::Initialize()
{
	// Initialize all child controls
	// This will correctly initialize their positions.
	IControl::Initialize();

	//Shouldn't create a transition control with no children!
	assert(mChildren.size() > 0);

	list<IControl*>::const_iterator it = mChildren.begin();

	int counter = 0;
	while ( it != mChildren.end() )
	{
		auto childCtrl = (*it);

		// Shouldn't need global positions here.
		// Controls in a TransitionController should be relative to the
		// screen, not to a parent.

		// Store the position of our control when it transitions in.
		auto childPosition = childCtrl->GetPosition();
		mGoalPosition.push_back(childPosition);

		//Calculate the position it should be in when it transitions out.
		Vector2 transOutPosition;
		auto childSize = childCtrl->GetSize();

		auto transEffect = mTransEffect;

		if (mTransEffect == ALTERNATEVERTICAL)
			transEffect = counter % 2 == 0 ? SLIDEUP : SLIDEDOWN;
		else if (mTransEffect == ALTERNATEHORIZONTAL)
			transEffect = counter % 2 == 0 ? SLIDELEFT : SLIDERIGHT;

		switch(transEffect)
		{
			case SLIDEUP:
				transOutPosition.x = childPosition.x;
				transOutPosition.y = -childPosition.y - childSize.y;
				break;
			case SLIDEDOWN:
				transOutPosition.x = childPosition.x;
				transOutPosition.y = sScreenBounds.y + childSize.y;
				break;
			case SLIDELEFT:
				transOutPosition.x = sScreenBounds.x + childSize.x;
				transOutPosition.y = childPosition.y;
				break;
			case SLIDERIGHT:
				transOutPosition.x = -childPosition.x - childSize.x;
				transOutPosition.y = childPosition.y;
				break;
		}

		// save our goal position.
		mTransitionOutPositions.push_back(transOutPosition);

		// If we begin out, set our children's positions to where they should be.
		if (mBeginsOut)
			childCtrl->SetPosition(transOutPosition);

		it++;
		counter++;
	}
}

