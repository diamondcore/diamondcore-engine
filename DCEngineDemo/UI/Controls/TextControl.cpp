#include "TextControl.h"
#include <Interfaces.h>
#include "../../Graphics Core/Graphics Core/2D/SpriteFont.h"
#include "../ScreenManager.h"

TextControl::TextControl(const char* fontName, const char* text) :
	IControl(),
	mFontName(fontName)
{
	mText << text;
	mColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

	ScreenMan->GetGraphics()->LoadFont(fontName);

	calculateSize();	
}

TextControl::TextControl(const char* fontName, const char* text, Vector2 pos) :
	IControl(),
	mFontName(fontName)
{
	mText << text;
	mColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	SetPosition(pos);

	ScreenMan->GetGraphics()->LoadFont(fontName);

	calculateSize();
}

void TextControl::calculateSize()
{
	std::string strx = mText.str();
	char * writable = new char[strx.size() + 1];
	std::copy(strx.begin(), strx.end(), writable);
	writable[strx.size()] = '\0';
	
	auto size = ScreenMan->GetGraphics()->MeasureString(mFontName.c_str(), writable);
	delete[] writable;
	SetSize(size.x, size.y);
}

void TextControl::Draw(IGraphicsCore* gDevice)
{
	// TODO: Ew... gross.
	if (IsActive())
		gDevice->RenderString(mFontName.c_str(), mText.str().c_str(), GetGlobalPosition(), 1.0f - GetDepthPosition(), mColor);

	IControl::Draw(gDevice);
}

void TextControl::Initialize()
{
	IControl::Initialize();
}