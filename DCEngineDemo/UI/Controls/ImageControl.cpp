#include "ImageControl.h"
#include <Interfaces.h>
#include "../ScreenManager.h"

ImageControl::ImageControl(char* texturePath) : IControl(),
	mFilePath(texturePath),
	mColor(1.0f, 1.0f, 1.0f, 1.0f)
{
	auto info = D3DXIMAGE_INFO();
	ScreenMan->GetGraphics()->LoadTexture(texturePath);
	ScreenMan->GetGraphics()->GetImageInfo(texturePath, info);

	mOriginalSize = Vector2(info.Width, info.Height);
	SetSize(info.Width, info.Height);
}

void ImageControl::RecalculateRenderMatrix()
{
	SetScale(mSize / mOriginalSize);

	IControl::RecalculateRenderMatrix();
}

ImageControl::ImageControl(char* texturePath, Vector2 position) : IControl(),
	mFilePath(texturePath),
	mColor(1.0f, 1.0f, 1.0f, 1.0f)
	
{
	SetPosition(position);
}

ImageControl::ImageControl(char* texturePath, Vector2 position, Vector2 size) : IControl(),
	mFilePath(texturePath),
	mColor(1.0f, 1.0f, 1.0f, 1.0f)
{
	SetPosition(position);
	SetSize(size);
}

void ImageControl::Draw(IGraphicsCore* gDevice)
{
	if (IsActive())
		gDevice->Render2D(mFilePath, GetRenderMatrix(), mColor);

	IControl::Draw(gDevice);
}