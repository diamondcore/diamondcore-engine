#pragma once

#include "IControl.h"

#include <functional>

class SliderControl : public IControl
{

private:
	static RECT* mSourceRects;
	static float mQuarterWidth;
	//Texture* mSliderTextures[2];
	bool mSliderIsDown;
	float mMouseXPos;
	float mFillWidth;
	int mCurrentValue;
	int mMaxValue;

	void setValue(int newValue);

public:
	inline int GetMaxValue() { return mMaxValue; }
	inline int GetValue() { return mCurrentValue; }
	SliderControl(Vector2 size, int startingValue, int maxValue = 10);
	IControl::HandleInputResult HandleInput();
	function<void (SliderControl* sender, int newValue)> OnValueChanged;
	void Draw(IGraphicsCore* gDevice);
};