#include "EnergyBarControl.h"
#include "../ScreenManager.h"
#include "TextControl.h"
#include "ImageControl.h"

EnergyBarControl::EnergyBarControl(std::string title)
	:PanelControl()
{
	m_cTitle = title;
	
	// Default full bar size
	auto screenSize = ScreenMan->GetScreenSize();
	m_vFullBarSize = Vector2(screenSize.x * 0.45f, screenSize.y / 18.0f);	
}
EnergyBarControl::EnergyBarControl(Vector2 fullBarSize, std::string title)
	:PanelControl()
{
	m_cTitle = title;
	m_vFullBarSize = fullBarSize;
}

void EnergyBarControl::Initialize()
{
	m_fPowerPercentage = 1.0f;
	m_vFullColor = Vector3(0, 1.0f, 0);
	m_vEmptyColor = Vector3(1.0, 0, 0);
	
	auto panelPadding = 20.0f;

	//Set our initial size and position
	auto screenSize = ScreenMan->GetScreenSize();

	//m_vFullBarSize = Vector2(screenSize.x * 0.45f, screenSize.y / 18.0f);	

	m_tPowerLabel = new TextControl("PowerControl", m_cTitle.c_str());
	m_tPowerLabel->SetPosition(Vector2(panelPadding, panelPadding) * 0.5f);

	auto panelSize = m_tPowerLabel->GetSize() + m_vFullBarSize + Vector2(panelPadding, panelPadding);
	SetSize(panelSize);

	SetPosition(Vector2((screenSize.x * 0.5f) - (m_vFullBarSize.x * 0.5f), screenSize.y - panelSize.y));

	m_iEnergyBar = new ImageControl("blankTexture.png");

	m_iEnergyBar->SetSize(m_vFullBarSize);
	m_iEnergyBar->SetPosition(Vector2(panelPadding * 0.5f, panelSize.y - m_vFullBarSize.y - (panelPadding * 0.5f)));

	AddChild(m_iEnergyBar);
	AddChild(m_tPowerLabel);

	PanelControl::Initialize();
}

void EnergyBarControl::Update(float dt)
{
	// TODO: Replace Lerp with Easing function.
	auto newBarCol = Vector3::Lerp(m_vEmptyColor, m_vFullColor, m_fPowerPercentage);
	m_iEnergyBar->SetColor(newBarCol.x, newBarCol.y, newBarCol.z);
	m_iEnergyBar->SetSize(Vector2(m_vFullBarSize.x * m_fPowerPercentage, m_vFullBarSize.y));

	PanelControl::Update(dt);
}