#include "PartDamageControl.h"
#include <Interfaces.h>
#include "../ScreenManager.h"

PartDamageControl::PartDamageControl(char* texturePath) : ImageControl(texturePath)
{
	m_DamagePercentage = 1.0f;
	mColor = D3DXCOLOR(0.0, 1.0f, 0.0f, 1.0f);
	m_HealthyColor = Vector3(0.0f, 1.0f, 0.0f);
	m_DamagedColor = Vector3(1.0f, 0.0f, 0.0f);
}

void PartDamageControl::Update(float dt)
{
	// Recalculate the color we should be rendering at.
	auto newColor = Vector3::Lerp(m_DamagedColor, m_HealthyColor, m_DamagePercentage);
	mColor = D3DXCOLOR(newColor.x, newColor.y, newColor.z, 1.0f);

	ImageControl::Update(dt);
}