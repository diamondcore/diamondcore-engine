#include "SliderControl.h"

#include <math.h>

RECT* SliderControl::mSourceRects = NULL;
float SliderControl::mQuarterWidth = 0;


SliderControl::SliderControl(Vector2 size, int startingValue, int maxValue) : IControl()
{
	//mSliderTextures[0] = ContentMan->Load<Texture>("Textures/SliderBar.png");
	//mSliderTextures[1] = ContentMan->Load<Texture>("Textures/SliderThumbBar.png");
	mSliderIsDown = false;
	mMouseXPos = 0;
	mFillWidth = 0;
	mMaxValue = maxValue;
	SetSize(size);

	//Initialize SoureRects if necessary
	if (mSourceRects == NULL)
	{
		//mQuarterWidth = mSliderTextures[0]->GetWidth() * 0.25f;
		//int height = mSliderTextures[0]->GetHeight();

		mSourceRects = new RECT[4];

		mSourceRects[0].left = 0;
		mSourceRects[0].top = 0;
		mSourceRects[0].right = mQuarterWidth;
		//mSourceRects[0].bottom = height;

		for (int x = 1; x < 4; x++)
		{
			mSourceRects[x].left = mSourceRects[x - 1].right;
			mSourceRects[x].top = 0;
			mSourceRects[x].right = mSourceRects[x].left + mQuarterWidth;
			//mSourceRects[x].bottom = height;
		}
	}

	setValue(startingValue);
}

void SliderControl::setValue(int newValue)
{
	if (newValue == mCurrentValue)
		return;

	mCurrentValue = newValue;

    mFillWidth = (int)(GetSize().x / mMaxValue) * mCurrentValue;

	if (OnValueChanged)
		OnValueChanged(this, newValue);
}

IControl::HandleInputResult SliderControl::HandleInput()
{
	RECT bounds = GetBounds();

	//if (!mSliderIsDown && (Input::IsNewMouseDown() && Input::MouseIsWithin(bounds)))
    {
        mSliderIsDown = true;
		return HandleInputResult::CONSUME;
    }

    // update our position
    if (mSliderIsDown)
    {
		//if (Input::IsMouseDown())
			//mMouseXPos = Input::GetMousePosition().x;
		//else
		//	mSliderIsDown = false;
    }

	// Shouldn't really do this...but a sliderControl shouldn't have any children.
    if (!mSliderIsDown)
		return HandleInputResult::NORMAL;

    auto sizeOfSection = (mSize.x / mMaxValue);
    // Calculate what section of the slider the mouse is in.
    // And adjust it's value.
	
	//TODO: Use Round once it's been brought over from the other codebase.
	auto newValue = (int)(((mMouseXPos - bounds.left) / sizeOfSection) + 0.5f);

	setValue(newValue);

	return HandleInputResult::CONSUME;
}

void SliderControl::Draw(IGraphicsCore* gDevice)
{
	auto ctrlPos = GetGlobalPosition();

	RECT leftEndRect;
	leftEndRect.left = ctrlPos.x;
	leftEndRect.top = ctrlPos.y;
	leftEndRect.right = leftEndRect.left + mQuarterWidth;
	leftEndRect.bottom = leftEndRect.top + mSize.y;

    RECT rightEndRect;
	rightEndRect.left = ctrlPos.x + mSize.x - mQuarterWidth;
	rightEndRect.top = ctrlPos.y;
	rightEndRect.right = rightEndRect.left + mQuarterWidth;
	rightEndRect.bottom = rightEndRect.top + mSize.y;

    Vector2 sliderThumbPos; 
	sliderThumbPos.x = leftEndRect.left + mFillWidth - 5;
	sliderThumbPos.y = leftEndRect.top;

	//TODO: Move clamp to math util.
	sliderThumbPos.x = min(ctrlPos.x + mSize.x - mQuarterWidth - 5, 
						max(ctrlPos.x + mQuarterWidth / 2, sliderThumbPos.x));
            
    RECT fillRect;
	fillRect.left = leftEndRect.right;
	fillRect.top = ctrlPos.y;
	fillRect.right = sliderThumbPos.x + 5;
	fillRect.bottom = fillRect.top + mSize.y;
	
    RECT unfilledRect;
	unfilledRect.left = sliderThumbPos.x;
	unfilledRect.top = ctrlPos.y;
	unfilledRect.right = rightEndRect.left;
	unfilledRect.bottom = unfilledRect.top + mSize.y;
	
	// Draw the left end
	//GraphicsMan->DrawTexture(ctrlPos, Vector2(mQuarterWidth, mSize.y), &mSourceRects[0], mSliderTextures[0]->GetTexture());
	
	// Draw the right end
	//GraphicsMan->DrawTexture(Vector2(rightEndRect.left, rightEndRect.top), Vector2(mQuarterWidth, mSize.y), &mSourceRects[3], mSliderTextures[0]->GetTexture());

	// Draw the unfilled part
	Vector2 unfilledRectSize((unfilledRect.right - unfilledRect.left), (unfilledRect.bottom - unfilledRect.top));
	//GraphicsMan->DrawTexture(Vector2(unfilledRect.left, unfilledRect.top), unfilledRectSize, &mSourceRects[2], mSliderTextures[0]->GetTexture());
	
	// Draw the filled part
	Vector2 filledRectSize((fillRect.right - fillRect.left), (fillRect.bottom - fillRect.top));
	//GraphicsMan->DrawTexture(Vector2(fillRect.left, fillRect.top), filledRectSize, &mSourceRects[1], mSliderTextures[0]->GetTexture());

	// Draw the slider's thumb
    //GraphicsMan->DrawTexture(sliderThumbPos, mSliderTextures[1]->GetTexture());

	IControl::Draw(gDevice);
}