#pragma once

#include "TextField.h"

class ControllerTextField : public TextField
{

public:
	
	ControllerTextField(Vector2 size, std::string initialText = "");
	
	IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt);
	void Initialize();

private:

	int currentChar;
};