#include "HealthControl.h"
#include "../ScreenManager.h"

HealthControl::HealthControl(char* podTexture, char* leftTexture, char* rightTexture) : PanelControl()
{
	m_Pod = new PartDamageControl(podTexture);
	m_LeftEngine = new PartDamageControl(leftTexture);
	m_RightEngine = new PartDamageControl(rightTexture);

	//This is temporary, as we're using blank textures for now.
	m_Pod->SetSize(40, 55);

	m_LeftEngine->SetSize(20, 75);
	m_RightEngine->SetSize(20, 75);

	// This, however, is not.
	auto podSize = m_Pod->GetSize();
	auto engineSpacing = 50.0f;
	
	auto leftSize = m_LeftEngine->GetSize();
	m_LeftEngine->SetPosition(leftSize.x * -0.5f, -(podSize.y + engineSpacing));
	m_Pod->AddChild(m_LeftEngine);

	auto rightSize = m_RightEngine->GetSize();
	m_RightEngine->SetPosition(podSize.x + (leftSize.x * -0.5f), -(podSize.y + engineSpacing));
	m_Pod->AddChild(m_RightEngine);

	AddChild(m_Pod);

	auto averagePodY = (leftSize.y + rightSize.y) * 0.5f;
	auto ctrlSize = Vector2(podSize.x + (leftSize.x * 0.5f) + (rightSize.x * 0.5f), podSize.y + engineSpacing + averagePodY);
	SetSize(ctrlSize);

	auto screenSize = ScreenMan->GetScreenSize();
}