#pragma once

#include "ImageControl.h"
#include "TextControl.h"
#include <string>
#include <functional>

using namespace std;


class TextField : public IControl
{

public:
	void* UserData;
	function<void (TextField* sender, void* userData)> OnEnter;

	TextField(Vector2 size, std::string initialText = "");

	void Update(float dt);
	void Draw(IGraphicsCore* gDevice);
	IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt);
	void Initialize();

	void Destroy();

	std::string GetText() { return mCurrentText.str(); }
	void SetText(std::string text) { mCurrentText.str(text); }
	void SetMaxCharacters(int max) { mMaxCharacters = max; }

private:

	// A total hack variable.
	// Because I can't measure the size of 
	// a character, I'll estimate it for now
	// with the default font so that I can unblock Wes.
	float mTextSize;

	float mBlinkTimer;

	// To be calculated from data
	const int mCharSize;
	
	D3DXCOLOR mBackgroundColor;
	D3DXCOLOR mCursorColor;
	D3DXCOLOR mFontColor;

	Vector2 mCursorSize;
	D3DXMATRIX mCursorMatrix;

	int mMaxCharacters;

	bool mControlDirty;

	Vector2 mCursorScale;

protected:
	stringstream mCurrentText;
	unsigned int mCursorPosition;
};