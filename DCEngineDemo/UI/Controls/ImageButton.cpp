#include "ImageButton.h"
#include <Interfaces.h>
#include <math.h>
#include "../ScreenManager.h"

ImageButton::ImageButton(std::string baseFileName, std::string buttonText, std::string buttonFont) :
	IControl(),
	mBaseFileName(baseFileName),
	mFontName(buttonFont)
{

	mButtonClickedSound = "../Content/Sounds/SFX/Confirm.wav";
	mCurrentState = IDLE;
	mPreviousState = IDLE;
	mButtonText = buttonText;
	UserData = nullptr;
	mText = nullptr;
	mToggle = false;

	XTextPadding = 80;
	YTextPadding = 20;

	// Don't specify an extension for buttons. We'll search for .png's.
	// TODO: Ray - Tell me if we need to support more filetypes
	auto fileName = mBaseFileName;
	for (int i = 0; i < fileName.length(); i++)
	{
		if (fileName[i] == '.')
		{
			fileName = mBaseFileName.substr(0, i);
			break;
		}
	}

	mStateTextures[IDLE] =  fileName + ".png";

	mStateTextures[HOVER] = fileName + "_su.png";

	mStateTextures[PRESSED] = fileName + "_s.png";

	mStateTextures[DISABLED] = fileName + "_u.png";

	mCurrentTexture = strdup(mStateTextures[IDLE].c_str());

	mColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

	auto info = D3DXIMAGE_INFO();
	ScreenMan->GetGraphics()->LoadTexture(mStateTextures[IDLE].c_str());
	ScreenMan->GetGraphics()->GetImageInfo(mStateTextures[IDLE].c_str(), info);

	mOriginalSize = Vector2(info.Width, info.Height);
	SetSize(info.Width, info.Height);
}

void ImageButton::Initialize()
{

	if(mButtonText.empty())
	{
		IControl::Initialize();
		return;
	}

	// If we have text for our button, create the object
	// to be rendered it within.
	mText = new TextControl(mFontName.c_str(),  mButtonText.c_str());
	AddChild(mText);

	// Check that this will fit within our button!
	auto textBounds = mText->GetSize();

	if (textBounds.LengthSquared() >= mSize.LengthSquared())
	{
		//It was too big, time to resize ourselves.
		auto newSize = Vector2(textBounds.x + XTextPadding, textBounds.y + YTextPadding);
		SetSize(newSize);
	}

	// Center the text within the button.
	mText->SetPosition((mSize - textBounds) * 0.5);

	IControl::Initialize();
}

void ImageButton::RecalculateRenderMatrix()
{
	SetScale(mSize / mOriginalSize);

	IControl::RecalculateRenderMatrix();
}

void ImageButton::Update(float dt)
{
	if (mCurrentState != mPreviousState)
		ChangeState(mCurrentState);

	if (!IsActive())
		return;

	IControl::Update(dt);
}

void ImageButton::Draw(IGraphicsCore* gDevice)
{
	if (IsVisible())
	{
		auto globalPos = GetGlobalPosition();
		gDevice->Render2D(mCurrentTexture, GetRenderMatrix(), mColor);
	}

	IControl::Draw(gDevice);
}

IControl::HandleInputResult ImageButton::HandleInput(IInputCore* inputState, float dt)
{
	auto bounds = GetBounds();
	auto mouseIsInCtrlBounds = inputState->IsMouseInBounds(bounds);

	if (!mouseIsInCtrlBounds)
		ChangeState(IDLE);

	if (!IsActive())
		return IControl::HandleInput(inputState, dt);

	if (mouseIsInCtrlBounds)
	{
		if (mCurrentState == PRESSED)
		{
			if (inputState->IsNewMouseClick(0))
			{
				ChangeState(HOVER);

				if (OnClicked)
				{
					ScreenMan->GetMainCore()->GetSoundCore()->PlaySFX(ScreenMan->m_iButtonClickSoundID,false,0);
					OnClicked(this, UserData);
				}

				
			}
			else
				return IControl::CONSUME;
		}
		else if (inputState->IsMouseButtonDown(0))
			ChangeState(PRESSED);
		else
		{
			ChangeState(HOVER);
			return IControl::HandleInput(inputState, dt);
		}

		return IControl::CONSUME;
	}

	ChangeState(IDLE);

	return IControl::HandleInput(inputState, dt);
}

void ImageButton::ChangeState(ButtonState newState)
{
	if (mCurrentState == newState)
		return;

	mPreviousState = mCurrentState;
	mCurrentState = newState;

	// Check if we have a texture for this state. Set it if so.
	if (mStateTextures[mCurrentState].empty())
		return;

	// Make this more efficient when Render2D can take 
	// a const char*
	if(mToggle)
		mCurrentTexture = strdup(mStateTextures[PRESSED].c_str());
	else
		mCurrentTexture = strdup(mStateTextures[mCurrentState].c_str());
}

void ImageButton::Destroy()
{
	//delete mBaseFileName;
	IControl::Destroy();
}

void ImageButton::SetToggle(bool useToggle)
{
	mToggle = useToggle;
	ChangeState(HOVER);
}