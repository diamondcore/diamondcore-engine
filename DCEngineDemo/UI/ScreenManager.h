#pragma once

class IGameScreen;

#include <list>
#include <assert.h>
#include <Interfaces.h>
#include "../EntityManager.h"

using namespace std;


#define ScreenMan ScreenManager::Instance()

void ResizeCallback();

class TextControl;

class ScreenManager
{
private:
	ScreenManager()
	{
		mChildCount = 0;
		m_pEntityManager = 0;	
	}

	list<IGameScreen*> mChildren;
	unsigned int mChildCount;

	MainCore* m_pMainCore;

	EntityManager* m_pEntityManager;

	TextControl* m_tFPSCounter;

	Vector2		m_vOriginalScreenSize;



public:

	////////////////
	// SOUND STUFF
	////////////////
	int	m_iButtonClickSoundID;

	static ScreenManager* ScreenManager::Instance()
	{
		static ScreenManager temp;
		return &temp;
	}

	MainCore* GetMainCore() { return m_pMainCore; }
	ISoundCore* GetSound() { return m_pMainCore->GetSoundCore(); }
	IGraphicsCore* GetGraphics() { return m_pMainCore->GetGraphicsCore(); }
	EntityManager* GetEntityManager() { return m_pEntityManager; }
	Vector2 GetScreenSize() { return m_pMainCore->GetGraphicsCore()->GetScreenSize(); }

	void Initialize(MainCore* main);

	void Draw();

	void AddScreen(IGameScreen* screenToAdd);

	void RemoveScreen(IGameScreen* screenToRemove);

	// Removes all screens from the stack. If supplied, the screen passed in will be added afterwards
	void ExitScreens(IGameScreen* screenToAddToTopofStack = NULL);

	IGameScreen* GetCurrentScreen();

	void Update(float dt);

	void ShutDown();

	void AdjustSize();
	
	void GetWindowPos(int *x, int *y)
	{
		HWND hWndParent = GetParent(GetGraphics()->GetWindow());
		POINT p = {0};

		MapWindowPoints(GetGraphics()->GetWindow(), hWndParent, &p, 1);

		(*x) = p.x;
		(*y) = p.y;
	}
};
