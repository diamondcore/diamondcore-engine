#include "MouseSynthesiser.h"
#include "ScreenManager.h"

MouseSynthesiser::MouseSynthesiser()
{
	mButtonIter = 0;
	m_iNumCols	= 0;
}
MouseSynthesiser::~MouseSynthesiser()
{
}
void MouseSynthesiser::ShutDown()
{
	mButtonPositions.clear();
}

void MouseSynthesiser::Initialize()
{
	if(mButtonPositions.empty())
		return;

	// Set the buttton iterator to the begining of the list
	mButtonIter = -1;
	/*int x, y;
	ScreenMan->GetWindowPos(&x, &y);
	Vector2 buttonPos = mButtonPositions[mButtonIter]->GetGlobalPosition() + mButtonPositions[mButtonIter]->GetSize()*0.6f;
	SetCursorPos(buttonPos.x + x, buttonPos.y + y); */
}

void MouseSynthesiser::AddButton(ImageButton* button)
{
	mButtonPositions.push_back(button);	// Add button position to list
}
void MouseSynthesiser::SetupTable(int cols)	// Setup number of rows and columns
{
	m_iNumCols = cols - 1;
}

void MouseSynthesiser::HandleInput(IInputCore* inputState, float dt)
{
	// Synthesise mouse movement up and down
	if(!mButtonPositions.empty())
	{
		if(inputState->IsNewButtonRelease(DPAD_DOWN))
		{
			mButtonIter++;
			mButtonIter += m_iNumCols;	// Go down a column
			if(mButtonIter >= mButtonPositions.size())
				mButtonIter %= mButtonPositions.size();
		
			if(mButtonPositions[mButtonIter]->IsVisible())
			{
				int x, y;
				ScreenMan->GetWindowPos(&x, &y);
				Vector2 buttonPos = mButtonPositions[mButtonIter]->GetGlobalPosition() + mButtonPositions[mButtonIter]->GetSize()*0.6f;
				buttonPos.x *= IControl::m_vScreenScale.x;
				buttonPos.y *= IControl::m_vScreenScale.y;
				SetCursorPos(buttonPos.x + x, buttonPos.y + y); 
			}
		}
		else if(inputState->IsNewButtonRelease(DPAD_UP))
		{
			mButtonIter--;
			mButtonIter -= m_iNumCols;	// Go up a column
			if(mButtonIter < 0)
				mButtonIter = mButtonPositions.size()-1;

			if(mButtonPositions[mButtonIter]->IsVisible())
			{
				int x, y;
				ScreenMan->GetWindowPos(&x, &y);
				Vector2 buttonPos = mButtonPositions[mButtonIter]->GetGlobalPosition() + mButtonPositions[mButtonIter]->GetSize()*0.6f;
				buttonPos.x *= IControl::m_vScreenScale.x;
				buttonPos.y *= IControl::m_vScreenScale.y;
				SetCursorPos(buttonPos.x + x, buttonPos.y + y); 
			}
		}
		else if(m_iNumCols != 0)
		{
			if(inputState->IsNewButtonRelease(DPAD_RIGHT))
			{
				mButtonIter++;
				if(mButtonIter >= mButtonPositions.size())
					mButtonIter %= mButtonPositions.size();
		
				if(mButtonPositions[mButtonIter]->IsVisible())
				{
					int x, y;
					ScreenMan->GetWindowPos(&x, &y);
					Vector2 buttonPos = mButtonPositions[mButtonIter]->GetGlobalPosition() + mButtonPositions[mButtonIter]->GetSize()*0.6f;
					buttonPos.x *= IControl::m_vScreenScale.x;
					buttonPos.y *= IControl::m_vScreenScale.y;
					SetCursorPos(buttonPos.x + x, buttonPos.y + y); 
				}
			}
			else if(inputState->IsNewButtonRelease(DPAD_LEFT))
			{
				mButtonIter--;
				if(mButtonIter < 0)
					mButtonIter = mButtonPositions.size()-1;

				if(mButtonPositions[mButtonIter]->IsVisible())
				{
					int x, y;
					ScreenMan->GetWindowPos(&x, &y);
					Vector2 buttonPos = mButtonPositions[mButtonIter]->GetGlobalPosition() + mButtonPositions[mButtonIter]->GetSize()*0.6f;
					buttonPos.x *= IControl::m_vScreenScale.x;
					buttonPos.y *= IControl::m_vScreenScale.y;
					SetCursorPos(buttonPos.x + x, buttonPos.y + y); 
				}
			}
		}
	}
	
	// Check stick movement
	Vector2 stickPos = inputState->GetLeftStick();
	if(stickPos.LengthSquared() > 0.0f)
	{
		// Use left thumb stick to move the mouse
		POINT p;
		GetCursorPos(&p);
		stickPos.x = stickPos.x*10.0f + p.x;
		stickPos.y = stickPos.y*-10.0f + p.y;
		SetCursorPos(stickPos.x, stickPos.y); 
	}
	
	// Synthesise a mouse click for the 'A' Button
	if(inputState->IsNewButtonRelease(A))
	{
		INPUT    Input={0};	
		ZeroMemory(&Input,sizeof(INPUT));									// Fills a block of memory with zeros.
		Input.type        = INPUT_MOUSE;									// Let input know we are using the mouse.
		Input.mi.dwFlags  = MOUSEEVENTF_LEFTUP;								// We are setting left mouse button up.
		SendInput( 1, &Input, sizeof(INPUT) );	
	}
	else if(inputState->IsButtonDown(A))
	{
		INPUT    Input={0};													// Create our input.
		Input.type        = INPUT_MOUSE;									// Let input know we are using the mouse.
		Input.mi.dwFlags  = MOUSEEVENTF_LEFTDOWN;							// We are setting left mouse button down.
		SendInput( 1, &Input, sizeof(INPUT) );								// Send the input.
	}
}
