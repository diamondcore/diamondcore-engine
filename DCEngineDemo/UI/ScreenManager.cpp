#include "ScreenManager.h"
#include "Screens\IGameScreen.h"
#include "../../Graphics Core/Graphics Core/MacroTools.h"
#include "../Game Objects/Terrain.h"
#include "Controls\TextControl.h"
#include "Screens\PauseScreen.h"

void ScreenManager::Initialize(MainCore* main)
{
	static bool initialized = false;

	if (initialized)
		return;

	m_pMainCore = main;
		
	// Set the screen resize callback
	GetGraphics()->SetResizeCallBack( ResizeCallback );
	m_vOriginalScreenSize = GetGraphics()->GetScreenSize();
	AdjustSize();

	// Create entity manager
	m_pEntityManager = new EntityManager(m_pMainCore);

#if defined(DEBUG) | defined(_DEBUG)
		
	m_tFPSCounter = new TextControl("TimesNewRoman16", "Frames Per Second: ");

#endif
	// set sound volume to .5
	ScreenMan->GetMainCore()->GetSoundCore()->SetMasterVolume(0.5f);
	ScreenMan->GetMainCore()->GetSoundCore()->SetChannelVolume(0,0.5f);
	ScreenMan->GetMainCore()->GetSoundCore()->SetChannelVolume(1,0.5f);

	// init UI sounds
	m_iButtonClickSoundID = ScreenMan->GetMainCore()->GetSoundCore()->CreateInstance2D("Content/Sounds/FX/22K/sfx_button_yes.wav",true,0,false);
}

void ScreenManager::Update(float dt)
{
	// Begin updating our input and gathering the state
	static auto inputCore = m_pMainCore->GetInputCore();
	
	inputCore->Update(dt);

	static list<IGameScreen*>::reverse_iterator it;
	it = mChildren.rbegin();

	// Walk through lists backwards. Controls on top consume shit first.
	for (; it != mChildren.rend(); it++)
	{
		if ((*it)->HandleInput(inputCore, dt) == IControl::HandleInputResult::CONSUME)
			break;
	}

	it = mChildren.rbegin();
	while ( it != mChildren.rend())
	{
		(*it)->Update(dt);

		// Opaque screens shouldn't bother updating
		// anything underneath them.
		if ((*it)->IsOpaque())
			break;

		it++;
	}

	GetGraphics()->Update(dt);

	GetSound()->Update(dt);

#if defined(DEBUG) | defined(_DEBUG)
	
	std::stringstream ss;
	ss << "Frames Per Second: " << GetGraphics()->GetFPS();
	std::string s = ss.str();
	m_tFPSCounter->SetText(ss.str().c_str());

#endif
}

void ScreenManager::Draw()
{
	static list<IGameScreen*>::reverse_iterator rit;
	rit = mChildren.rbegin();

	// A single screen, or an opaque on top only
	// requires us to draw the top.
	GetGraphics()->BeginRender();
	if (mChildren.size() == 1 || (*rit)->IsOpaque())
	{
		(*rit)->Draw3D(GetGraphics());
		(*rit)->Draw2D(GetGraphics());
#if defined(DEBUG) | defined(_DEBUG)
		
		m_tFPSCounter->Draw(GetGraphics());

#endif

		if ((*rit)->IsOpaque())
		{
			GetGraphics()->EndRender();
			return;
		}
	}
	else
	{
		static list<IGameScreen*>::const_iterator it;
		it = mChildren.begin();
		for (; it != mChildren.end(); it++)
		{
			(*it)->Draw3D(GetGraphics());
		}

		it = mChildren.begin();
		for (; it != mChildren.end(); it++)
		{
			(*it)->Draw2D(GetGraphics());
		}

#if defined(DEBUG) | defined(_DEBUG)
		
		m_tFPSCounter->Draw(GetGraphics());

#endif
	}
	GetGraphics()->EndRender();
}

void ScreenManager::ShutDown()
{
	// Walk through it backwards to make sure we handle dependencies correctly.
	list<IGameScreen*>::reverse_iterator it = mChildren.rbegin();
	for (; it!= mChildren.rend(); it++)
		(*it)->ShutDown();

	// Clear world entities
	if(m_pEntityManager)
	{
		m_pEntityManager->Shutdown();
		delete m_pEntityManager;
		m_pEntityManager = 0;
	}
}

void ScreenManager::AddScreen(IGameScreen* screenToAdd)
{
	//TODO: Double check that we aren't adding the same screen twice.

	mChildren.push_back(screenToAdd);
	mChildCount++;

	screenToAdd->Initialize();
}

IGameScreen* ScreenManager::GetCurrentScreen()
{
	if(mChildren.empty())
		return 0;
	return mChildren.front();
}

void ScreenManager::RemoveScreen(IGameScreen* screenToRemove)
{
	mChildren.remove(screenToRemove);

	mChildCount--;

	assert(mChildCount >= 0); // Tried to remove too many screens

	screenToRemove->ShutDown();
}

// Removes all screens from the stack. If supplied, the screen passed in will be added afterwards
void ScreenManager::ExitScreens(IGameScreen* screenToAddToTopofStack)
{
	while (mChildCount > 0)
		RemoveScreen(mChildren.front());

	if (screenToAddToTopofStack)
		AddScreen(screenToAddToTopofStack);
}

Vector2 IControl::m_vScreenScale = Vector2();
void ResizeCallback()
{
	ScreenMan->AdjustSize();
}
void ScreenManager::AdjustSize()
{
	// Update the scale of the UI with the screen size
	RECT clientRect;
	GetClientRect(GetGraphics()->GetWindow(), &clientRect);
	Vector2 screenSize(clientRect.right, clientRect.bottom);

	IControl::m_vScreenScale = screenSize/m_vOriginalScreenSize;
	
	// If in gameplay mode, pause the game
	auto em = GetEntityManager();
	if(em && !em->DisplayHiddenObjects() && !PauseScreen::AlreadyExists())
		AddScreen( new PauseScreen( GetCurrentScreen() ) );
}
