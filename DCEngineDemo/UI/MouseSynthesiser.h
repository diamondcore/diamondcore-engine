#pragma once

#include "Screens\IGameScreen.h"
#include "Controls\ImageButton.h"

class MouseSynthesiser
{
public:
	MouseSynthesiser();
	~MouseSynthesiser();
	void ShutDown();

	void Initialize();
	void AddButton(ImageButton* button);	// Add button to menu selection
	void SetupTable(int cols);				// Setup number of columns


	void HandleInput(IInputCore* inputState, float dt);

private:
	int	m_iNumCols;

	// Button Positions
	std::vector<ImageButton*>  mButtonPositions;
	int						   mButtonIter;

};