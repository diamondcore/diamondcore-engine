#include "StatsScreen.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <Windows.h>

#include "../../UI/Controls/PanelControl.h"
#include "../../UI/Controls/TextField.h"
#include "../../UI/Controls/ControllerTextField.h"
#include "../../UI/Controls/ImageButton.h"
#include "../ScreenManager.h"
#include "../../WorldManager.h"




StatsScreen::StatsScreen()
	:IGameScreen()
{
	m_bAttemptLeaderBoardOnInit = true;
}

void StatsScreen::Initialize()
{
	//AddControl(new PanelControl());
	m_pStatsPanel = new PanelControl();
	AddControl(m_pStatsPanel);

	m_iHighlightedRankNum = -1;

	if(m_bAttemptLeaderBoardOnInit)
		AttemptNewRank();
	else
	{
		InitRanks();
		//m_pStatsPanel->SetPosition(Vector2(-ScreenMan->GetScreenSize().x * 0.5f, 50));
	}
}

void StatsScreen::InitRanks()
{

	//auto m_pStatsPanel = new PanelControl();
	//AddControl(m_pStatsPanel);
	m_pStatsPanel->SetPosition(ScreenMan->GetScreenSize() * 0.5f);
		//m_pStatsPanel->SetSize(256, 128);
	auto m_tTopText = new TextControl("Trebuchet32", "Rank     Name     Time");
	auto m_tBlankText = new TextControl("TimesNewRoman16", "");

	m_pStatsPanel->AddChild(m_tTopText);
	m_pStatsPanel->AddChild(m_tBlankText);

	m_tTopText->SetColor(1, 1, 0);

	string temp;
	for(int i = 0; i < NUMRANKSLOTS; i++)
	{
		temp = std::to_string((_Longlong)i+1);
		temp.resize(2, ' ');
		temp = temp + ".  ";
		m_tRankings[i]  = new TextControl("Trebuchet25", temp.c_str());
		m_pStatsPanel->AddChild(m_tRankings[i]);
	}

	SetRankColor(m_iHighlightedRankNum, 1, 0, 1);

	auto Btn_ShowStats = new ImageButton("UI/generic.png", "Statistics", "TimesNewRoman16");
	Btn_ShowStats->SetSize(256, 64);
	m_pStatsPanel->AddChild(Btn_ShowStats);

	m_pStatsPanel->LayoutColumn(0.0f);
	Vector2 size = m_tRankings[0]->GetSize();
	size.x *= NUMRANKSLOTS;
	m_pStatsPanel->SetSize(size);
	//m_pStatsPanel->SetPosition(Vector2(0.0f, 20.0f));
	//m_pStatsPanel->SetPosition(Vector2(0, 50));

	UpdateRankings();

	Vector2 scrPos = ScreenMan->GetScreenSize();
	m_pStatsPanel->SetPosition(Vector2(-scrPos.x * 0.25f, 50));
	
	//scrPos.x *= 0.75f;
	scrPos.y *= 0.75f;
	Btn_ShowStats->SetPosition(scrPos);

	Btn_ShowStats->OnClicked = [=] (ImageButton* sender, void* userData)
	{
		m_pStatsPanel->RemoveChild(m_tTopText);
		m_pStatsPanel->RemoveChild(m_tBlankText);
		for(int i = 0; i < NUMRANKSLOTS; i++)
		{
			m_pStatsPanel->RemoveChild(m_tRankings[i]);
		}
		m_pStatsPanel->RemoveChild(Btn_ShowStats);
		//NullifyRanks();
		//RemoveControl(m_pStatsPanel);
		NullifyRanks();
		InitStats();
	};

	
}

void StatsScreen::InitStats()
{
	//auto m_pStatsPanel = new PanelControl();
	//AddControl(m_pStatsPanel);
	m_pStatsPanel->SetPosition(ScreenMan->GetScreenSize() * 0.5f);
		//m_pStatsPanel->SetSize(256, 128);
	//m_pStatsPanel = new PanelControl();
	m_tTotalTimeCtrl = new TextControl("Trebuchet25", "Total Time : ");
	m_tTotalDamageCtrl = new TextControl("Trebuchet25", "Total Damage : ");
	m_tTotalDeathsCtrl = new TextControl("Trebuchet25", "# of Deaths : ");
	m_tNumRacesWonCtrl = new TextControl("Trebuchet25", "# Races Won : ");
	m_tTotalRacesCtrl = new TextControl("Trebuchet25", "# of Races : ");
	m_tAvgLifeCtrl = new TextControl("Trebuchet25", "Avg. Lifespan : ");
	m_tLastRaceCtrl = new TextControl("Trebuchet25", "Last Race Time : ");
	m_tMeteorCtrl = new TextControl("Trebuchet25", "# Meteor Deaths : ");
	m_tTurretCtrl = new TextControl("Trebuchet25", "# Turret Deaths : ");
	m_tGeyserCtrl = new TextControl("Trebuchet25", "# Geyser Deaths : ");
	m_tElectricCtrl = new TextControl("Trebuchet25", "# Electricity Deaths : ");
	m_tCrushCtrl = new TextControl("Trebuchet25", "# Crush Deaths : ");
	m_tSuicideCtrl = new TextControl("Trebuchet25", "# Suicide Deaths : ");
	m_tRageQuitCtrl = new TextControl("Trebuchet25", "# Rage Quits : ");
	m_tLeastDamageTakenCtrl = new TextControl("Trebuchet25", "Lowest Damage Taken : ");
	m_tCurrentDamageTakenCtrl = new TextControl("Trebuchet25", "Damage Taken : ");

	
	m_pStatsPanel->AddChild(m_tTotalTimeCtrl);
	m_pStatsPanel->AddChild(m_tTotalDamageCtrl);
	m_pStatsPanel->AddChild(m_tLeastDamageTakenCtrl);
	m_pStatsPanel->AddChild(m_tCurrentDamageTakenCtrl);
	m_pStatsPanel->AddChild(m_tAvgLifeCtrl);
	m_pStatsPanel->AddChild(m_tLastRaceCtrl);
	m_pStatsPanel->AddChild(m_tTotalDeathsCtrl);
	m_pStatsPanel->AddChild(m_tTotalRacesCtrl);
	m_pStatsPanel->AddChild(m_tNumRacesWonCtrl);	
	m_pStatsPanel->AddChild(m_tMeteorCtrl);
	m_pStatsPanel->AddChild(m_tTurretCtrl);
	m_pStatsPanel->AddChild(m_tGeyserCtrl);
	m_pStatsPanel->AddChild(m_tElectricCtrl);
	m_pStatsPanel->AddChild(m_tCrushCtrl);
	m_pStatsPanel->AddChild(m_tSuicideCtrl);
	m_pStatsPanel->AddChild(m_tRageQuitCtrl);

	auto Btn_ShowRanks = new ImageButton("UI/generic.png", "Leaderboard", "TimesNewRoman16");
	Btn_ShowRanks->SetSize(256, 64);
	m_pStatsPanel->AddChild(Btn_ShowRanks);

	//AddControl(m_pStatsPanel);
	Vector2 size = m_tLeastDamageTakenCtrl->GetSize();
	size.x *= 15;
	m_pStatsPanel->SetSize(size);

	m_pStatsPanel->LayoutColumn(0.0f);
	//m_pStatsPanel->SetPosition(Vector2(0.0f, 20.0f));
	//m_pStatsPanel->SetPosition(Vector2(0, 50));

	UpdateStats();

	Vector2 scrPos = ScreenMan->GetScreenSize();
	m_pStatsPanel->SetPosition(Vector2(-scrPos.x * 0.25f, 50));
	//scrPos.x *= 0.75f;
	scrPos.y *= 0.75f;
	Btn_ShowRanks->SetPosition(scrPos);

	Btn_ShowRanks->OnClicked = [=] (ImageButton* sender, void* userData)
	{
		m_pStatsPanel->RemoveChild(m_tTotalTimeCtrl);
		m_pStatsPanel->RemoveChild(m_tTotalDamageCtrl);
		m_pStatsPanel->RemoveChild(m_tLeastDamageTakenCtrl);
		m_pStatsPanel->RemoveChild(m_tCurrentDamageTakenCtrl);
		m_pStatsPanel->RemoveChild(m_tTotalDeathsCtrl);
		m_pStatsPanel->RemoveChild(m_tTotalRacesCtrl);
		m_pStatsPanel->RemoveChild(m_tNumRacesWonCtrl);
		m_pStatsPanel->RemoveChild(m_tAvgLifeCtrl);
		m_pStatsPanel->RemoveChild(m_tLastRaceCtrl);
		m_pStatsPanel->RemoveChild(m_tMeteorCtrl);
		m_pStatsPanel->RemoveChild(m_tTurretCtrl);
		m_pStatsPanel->RemoveChild(m_tGeyserCtrl);
		m_pStatsPanel->RemoveChild(m_tElectricCtrl);
		m_pStatsPanel->RemoveChild(m_tCrushCtrl);
		m_pStatsPanel->RemoveChild(m_tSuicideCtrl);
		m_pStatsPanel->RemoveChild(m_tRageQuitCtrl);
		m_pStatsPanel->RemoveChild(Btn_ShowRanks);
		//m_pStatsPanel->RemoveChild(Btn_ShowRanks);
		//NullifyStats();
		//m_pStatsPanel->RemoveChild(Btn_ShowRanks);
		//RemoveControl(m_pStatsPanel);
		NullifyStats();
		InitRanks();
		//m_pStatsPanel->SetPosition(Vector2(-ScreenMan->GetScreenSize().x * 0.5f, 50));
	};

	
}

void StatsScreen::NullifyRanks()
{
	for(int i = 0; i < NUMRANKSLOTS; i++)
		m_tRankings[i] = NULL;
}

void StatsScreen::NullifyStats()
{
	m_tTotalTimeCtrl = NULL;
	m_tTotalDamageCtrl = NULL;
	m_tTotalDeathsCtrl = NULL;
	m_tTotalRacesCtrl = NULL;
	m_tAvgLifeCtrl = NULL;
	m_tLastRaceCtrl = NULL;
	m_tMeteorCtrl = NULL;
	m_tTurretCtrl = NULL;
	m_tGeyserCtrl = NULL;
	m_tElectricCtrl = NULL;
	m_tCrushCtrl = NULL;
	m_tSuicideCtrl = NULL;
	m_tRageQuitCtrl = NULL;
	m_tLeastDamageTakenCtrl = NULL;
	m_tCurrentDamageTakenCtrl = NULL;
}

void StatsScreen::UpdateRankings()
{
	//string temp;
	//std::string name;
	//float t;
	//for(int i = 0; i < NUMRANKSLOTS; i++)
	//{
	//	name = SStats->GetRankName(i);
	//	temp = std::to_string((_Longlong)(i+1)) + "  :";
	//	if(name != NOSHOWNAME)
	//	{
	//		stringstream time;
	//		t = SStats->GetRankTime(i);
	//		time >> t;
	//		temp += name + "     " + time.str();	
	//	}
	//	m_tRankings[i]->SetText(temp.c_str());
	//}
	
	std::string temp;
	std::string name;
	std::string spacing;
	float time;
	int numMinutes;
	float numSeconds;
	int numMillisecs;

	for(int i = 0; i < NUMRANKSLOTS; i++)
	{
		name = SStats->GetRankName(i);
		if(name != NOSHOWNAME)
		{
			time = SStats->GetRankTime(i);
			numMinutes = (int)time / 60;
			numSeconds = fmodf(time, 60);
			numMillisecs = (int)(fmodf(numSeconds, 1) * 1000);
			temp = std::to_string((_Longlong) i + 1);		
			spacing.resize(9 - temp.size(), ' ');
			temp.resize(2, ' ');
			temp = spacing + temp;
			temp = temp + "            "
						+ name + "       "
						+ std::to_string((_Longlong)numMinutes) + " : "
						+ std::to_string((_Longlong)numSeconds) + "."
						+ std::to_string((_Longlong)numMillisecs);
			m_tRankings[i]->SetText(temp);
		}
		else
		{
			temp = " ";
			m_tRankings[i]->SetText(temp);
		}
	}

}

void StatsScreen::UpdateTimePlayed()
{
	if(!m_tTotalTimeCtrl)
		return;
	float time = SStats->GetTotalTimePlayed();
	int numMinutes = (int)time / 60;
	float numSeconds = fmodf(time, 60);
	int numMillisecs = (int)(fmodf(numSeconds, 1) * 1000);
	m_tTotalTimeCtrl->SetText("Total Time Played :     " +
								std::to_string((_Longlong)numMinutes) + " : " +
								std::to_string((_Longlong)numSeconds) + "." +
								std::to_string((_Longlong)numMillisecs));
	//m_tTotalTimeCtrl->SetText("Total Time Played : " + std::to_string((_Longlong)SStats->GetTotalTimePlayed()));
}

void StatsScreen::UpdateLastRaceTime()
{
	if(!m_tLastRaceCtrl)
		return;
	float time = SStats->GetLastRaceTime();
	int numMinutes = (int)time / 60;
	float numSeconds = fmodf(time, 60);
	int numMillisecs = (int)(fmodf(numSeconds, 1) * 1000);
	m_tLastRaceCtrl->SetText("Last Race Time :     " +
								std::to_string((_Longlong)numMinutes) + " : " +
								std::to_string((_Longlong)numSeconds) + "." +
								std::to_string((_Longlong)numMillisecs));
	//m_tLastRaceCtrl->SetText("Last Race Time : " + std::to_string((_Longlong)SStats->GetLastRaceTime()));
}

void StatsScreen::UpdateDamageTaken()
{
	if(!m_tTotalDamageCtrl)
		return;
	int decPlace = (int)(fmodf(SStats->GetTotalDamageTaken(), 1) * 1000);
	m_tTotalDamageCtrl->SetText("Total Damage Taken :     " + std::to_string((_Longlong)SStats->GetTotalDamageTaken())
		+ "." + std::to_string((_Longlong)decPlace));
}

void StatsScreen::UpdateMeteorCount()
{
	if(!m_tMeteorCtrl)
		return;
	m_tMeteorCtrl->SetText("# Meteor Deaths :     " + std::to_string((_Longlong)SStats->GetNumMeteorDeaths()));
}

void StatsScreen::UpdateTurretCount()
{
	if(!m_tTurretCtrl)
		return;
	m_tTurretCtrl->SetText("# Turret Deaths :     " + std::to_string((_Longlong)SStats->GetNumTurretDeaths()));
}

void StatsScreen::UpdateGeyserCount()
{
	if(!m_tGeyserCtrl)
		return;
	m_tGeyserCtrl->SetText("# Geyser Deaths :     " + std::to_string((_Longlong)SStats->GetNumGeyserDeaths()));
}

void StatsScreen::UpdateElectricCount()
{
	if(!m_tElectricCtrl)
		return;
	m_tElectricCtrl->SetText("# Electricity Deaths :     " + std::to_string((_Longlong)SStats->GetNumElectricityDeaths()));
}

void StatsScreen::UpdateCrushCount()
{
	if(!m_tCrushCtrl)
		return;
	m_tCrushCtrl->SetText("# Crush Deaths :     " + std::to_string((_Longlong)SStats->GetNumCrushDeaths()));
}

void StatsScreen::UpdateSuicideCount()
{
	if(!m_tSuicideCtrl)
		return;
	m_tSuicideCtrl->SetText("# Suicide Deaths :     " + std::to_string((_Longlong)SStats->GetNumSuicideDeaths()));
}

void StatsScreen::UpdateDeathCount()
{
	if(!m_tTotalDeathsCtrl)
		return;
	m_tTotalDeathsCtrl->SetText("# of Deaths :     " + std::to_string((_Longlong)SStats->GetTotalDeaths()));
}

void StatsScreen::UpdateRaceCount()
{
	if(!m_tTotalRacesCtrl)
		return;
	m_tTotalRacesCtrl->SetText("# of Races :     " + std::to_string((_Longlong)SStats->GetTotalRaces()));
}

void StatsScreen::UpdateWinCount()
{
	if(!m_tNumRacesWonCtrl)
		return;
	m_tNumRacesWonCtrl->SetText("# Races Won :     " + std::to_string((_Longlong)SStats->GetNumRacesWon()));
}

void StatsScreen::UpdateRageQuitCount()
{
	if(!m_tRageQuitCtrl)
		return;
	m_tRageQuitCtrl->SetText("# Rage Quits :     " + std::to_string((_Longlong)SStats->GetNumRageQuits()));
}

void StatsScreen::UpdateAverageLifespan()
{
	if(!m_tAvgLifeCtrl)
		return;
	float time = SStats->GetAverageLifespan();
	int numMinutes = (int)time / 60;
	float numSeconds = fmodf(time, 60);
	int numMillisecs = (int)(fmodf(numSeconds, 1) * 1000);
	m_tAvgLifeCtrl->SetText("Average Lifespan :     " +
								std::to_string((_Longlong)numMinutes) + " : " +
								std::to_string((_Longlong)numSeconds) + "." +
								std::to_string((_Longlong)numMillisecs));
}

void StatsScreen::UpdateLeastDamageTaken()
{
	if(!m_tLeastDamageTakenCtrl)
		return;
	int decPlace = (int)(fmodf(SStats->GetLeastDamageTaken(), 1) * 1000);
	m_tLeastDamageTakenCtrl->SetText("Lowest Race Damage :     " + std::to_string((_Longlong)SStats->GetLeastDamageTaken()) +
		"." + std::to_string((_Longlong)decPlace));
}

void StatsScreen::UpdateCurrentDamageTaken()
{
	if(!m_tCurrentDamageTakenCtrl)
		return;
	int decPlace = (int)(fmodf(SStats->GetCurrentDamageTaken(), 1) * 1000);
	m_tCurrentDamageTakenCtrl->SetText("Damage Taken in Last Race :     " + std::to_string((_Longlong)SStats->GetCurrentDamageTaken()) +
		"." + std::to_string((_Longlong)decPlace));
	if(SStats->GetCurrentDamageTaken() > SStats->GetLeastDamageTaken())
		m_tCurrentDamageTakenCtrl->SetColor(1, 0, 0);
	else if(SStats->GetCurrentDamageTaken() == SStats->GetLeastDamageTaken())
		m_tCurrentDamageTakenCtrl->SetColor(1, 1, 0);
	else
		m_tCurrentDamageTakenCtrl->SetColor(0, 1, 0);
}

void StatsScreen::UpdateStats()
{
	UpdateTimePlayed();
	UpdateDamageTaken();
	UpdateLastRaceTime();
	UpdateMeteorCount();
	UpdateTurretCount();
	UpdateElectricCount();
	UpdateCrushCount();
	UpdateGeyserCount();
	UpdateRageQuitCount();
	UpdateAverageLifespan();
	UpdateSuicideCount();
	UpdateDeathCount();
	UpdateRaceCount();
	UpdateWinCount();
	UpdateLeastDamageTaken();
	UpdateCurrentDamageTaken();
}

void StatsScreen::UpdateAll()
{
	UpdateRankings();
	UpdateStats();
}

void StatsScreen::SetRankColor(int i, float r, float g, float b)
{
	if(i >= 0 && i < NUMRANKSLOTS)
	{
		m_tRankings[i]->SetColor(r, g, b);
	}
}

void StatsScreen::AttemptNewRank()
{
	//We need to check to see if the player made it on the leaderboard here
	if(SStats->GetLastRaceTime() < SStats->GetRankTime(NUMRANKSLOTS-1))
	{
		//auto m_pStatsPanel = new PanelControl();
		//AddControl(m_pStatsPanel);

		static float lastTime;
		lastTime = SStats->GetLastRaceTime();
		static bool clicked = false;
		static TextField* tf = 0;
		static ImageButton* Btn_TextBox = 0;
		//static PanelControl* control = 0;
		// Pre declare the text field so that the textbox can delete it
		if(!clicked)
		{
			if(ScreenMan->GetMainCore()->GetInputCore()->GetControllerIsConnected())
			{
				// Use controller
				tf = new ControllerTextField(Vector2(500, 20), "");
				tf->MoveTowardsFront(3);
			}
			else
			{
				// Use keyboard
				tf = new TextField(Vector2(500, 20), "");
				tf->SetMaxCharacters(3);
				tf->MoveTowardsFront(3);
			}
			Btn_TextBox = new ImageButton("UI/generic.png");
			Btn_TextBox->MoveTowardsFront(2);
		}
		else
		{
			// Cancel load
			clicked = false;
			m_pStatsPanel->RemoveChild(tf);
			m_pStatsPanel->RemoveChild(Btn_TextBox);
			RemoveControl(m_pStatsPanel);
			//RemoveControl(control);
		}

		// Setup the TextBox
		Btn_TextBox->SetSize(256, 86);
		Btn_TextBox->SetPosition(Vector2(-128.0f, -57.0f));
		
		
		auto tc_TextBox = new TextControl("TimesNewRoman16", "You're on the Leaderboard!\n");
		
		tc_TextBox->SetPosition(Vector2(16, 10));
		tf->SetPosition(Vector2(Btn_TextBox->GetSize().x*0.4,36)); //Vector2(16, 36));
		Btn_TextBox->AddChild(tc_TextBox);
		m_pStatsPanel->AddChild(Btn_TextBox);

		m_pStatsPanel->SetPosition(ScreenMan->GetScreenSize() * 0.5f);
		//m_pStatsPanel->SetSize(256, 128);

		// Set up the text field
		tf->OnEnter = [=] (TextField* sender, void* userData)
		{ 		
			// Save the level
			std::string name =  sender->GetText();
			if(!name.empty())
			{
				m_iHighlightedRankNum = SStats->InsertRanking(name, lastTime);
				SStats->SetLastRaceTime(lastTime);
				
				SStats->WriteStatsToFile();
				m_pStatsPanel->RemoveChild(Btn_TextBox);
				InitRanks();
				//m_pStatsPanel->SetPosition(Vector2(-ScreenMan->GetScreenSize().x * 0.5f, 50));
			}
			clicked = false;
			GPM->SetStatsShowing(false);
			
		};		
		Btn_TextBox->AddChild(tf);
		//tf->SetPosition(Vector2(-(ScreenMan->GetScreenSize().x * 0.5f), 0));
		//tc_TextBox->SetPosition(Vector2(ScreenMan->GetScreenSize().x * 0.5f, 0));
	}
	else
	{
		InitRanks();
		//m_pStatsPanel->SetPosition(Vector2(-ScreenMan->GetScreenSize().x * 0.5f, 50));
	}
}

void StatsScreen::Update(float dt)
{
}

void StatsScreen::Draw2D(IGraphicsCore* gDevice)
{
	// Do 2D Draw
	IGameScreen::Draw2D(gDevice);
}

void StatsScreen::ShutDown()
{
	//RemoveControl(m_pStatsPanel);
	IGameScreen::ShutDown();
}