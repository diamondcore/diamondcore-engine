#pragma once


#include "IGameScreen.h"
#include "../Controls/ImageButton.h"
#include "../../UI/Controls/PanelControl.h"
#include "../Controls/TransitionController.h"

class Turbine;
class Terrain;

class TogglePhysics		: public IGameScreen
{
	IGameScreen*	m_pParent;

public:
	TogglePhysics(IGameScreen* pParent);

	void Initialize();
	void Update(float dt);
	void Draw2D(IGraphicsCore* gDevice);
	void ShutDown();
	
	void ButtonClicked(ImageButton* sender, int buttonInded);

	IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt);

private:
	Terrain*	m_pTerrain;
	Turbine*	m_pTurbine;
	int			m_iCount;

	TransitionController*	mTogglePhysicsPanel;
};