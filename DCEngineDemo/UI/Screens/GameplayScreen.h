#pragma once
#include "../../WorldManager.h"
#include "IGameScreen.h"
#include "../Controls/ImageButton.h"

class HealthControl;
class EnergyBarControl;
class TextControl;
class PanelControl;
class SpeedControl;

class GameplayScreen : public IGameScreen
{
public:

	GameplayScreen();
	
	void Initialize();
	void Update(float dt);
	void Draw2D(IGraphicsCore* gDevice);
	void ShutDown();

	IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt);
	// base callback function
    void TriggerVolumeCallback(TriggerType eType, int triggerID, int iBodyID, float dt);

	float m_fRaceTimer;

private:
	bool mGamePaused;

	


	//Remove these variables once the controls are updating with correct
	// data
	float TEMPPODDAMAGETIMER;
	float TEMPLENGINEDAMAGETIMER;
	float TEMPRENGINEDAMAGETIMER;
	float TEMPENERGYTIMER;;

	HealthControl* m_hHealthCtrl;
	EnergyBarControl* m_eEnergyCtrl;
	TextControl* m_tRaceMilSecondCtrl;
	TextControl* m_tRaceSecondCtrl;
	TextControl* m_tRaceMinuteCtrl;
	PanelControl* m_pTimerPanel;
	SpeedControl* m_sSpeedControl;
};