#include "LevelDesignerScreen.h"
#include "../ScreenManager.h"
#include "../../UI/Screens/GameplayMenu.h"
#include "../../DCEngineDemo/Controls/Camera/CameraManager.h"
#include "../../UI/Controls/SliderControl.h"
#include "../../UI/Controls/TextField.h"
#include "TogglePhysics.h"
#include "PauseScreen.h"
#include "../../Game Objects/LightEntity.h"
#include "../../Game Objects/PhysicsEntity.h"
#include "../../Game Objects/CameraEntity.h"
#include "../../Game Objects/Turret.h"
#include "../../Game Objects/TriggerEntity.h"
#include "../../Game Objects/SmokeTrap.h"
#include "../../Game Objects/DestructableObjects.h"
#include "../../Game Objects/LightningTrap.h"
#include "../../Game Objects/Smasher.h"
#include "MessageBox.h"
#include <dinput.h>
#include <fstream>
#include <strstream>

LevelDesignerScreen::LevelDesignerScreen()
	:IGameScreen()
{
	m_fBrushRadius			= 5.0f;
	m_iSelectedID			= -1;
	m_iCameraID				= -1;
	m_eCurrentBrushState	= state_None;
	m_ePreviousBrushState	= state_None;
	mTerrainControls		= 0;
	mObjectControls			= 0;
	mBoundaryControls		= 0;
	mObjectLists			= 0;
	m_pStartPointCounter	= 0;
	m_pFinishLineCounter	= 0;
	m_bEnableAxisX			= 1;
	m_bEnableAxisY			= 1;
	m_bEnableAxisZ			= 1;
	m_CurrentMenu			= "Menu";
	mCurrentSelection		= 0;
	m_iSelectionResponse	= 0;
}

void LevelDesignerScreen::Update(float dt)
{
	if(mRootControl)
		mRootControl->Update(dt);

	CamMgr->Update(dt);
}

void LevelDesignerScreen::Initialize()
{
	ScreenMan->GetEntityManager()->SetDisplayHiddenObject(true);	// Unhide all objects

	auto mainCore = ScreenMan->GetMainCore();

	// Set Graphics World Properties
	auto GFX = mainCore->GetGraphicsCore();

	// Set the camera glare type
	GFX->SetCameraGlareType(10);
	GFX->UseSSAO(true);				// Helps in seeing the difference in the terrain

	/////////////////
	// CAM MANAGER
	////////////////
	m_iCameraID = CamMgr->CreateCamera(FREECAM);
	CamMgr->SetActiveCamControl(m_iCameraID);
	CamMgr->SetPosition(0,8,-20);
	m_vPreviousCamPos  = CamMgr->GetCameraControlByID(m_iCameraID)->GetPosition();
	m_vPreviousCamLook = CamMgr->GetCameraControlByID(m_iCameraID)->GetLookAt();
	m_vPreviousCamUp   = CamMgr->GetCameraControlByID(m_iCameraID)->GetUp();

	//////////////////////////////////////////////////
	//		Initialize the terrain editor
	//////////////////////////////////////////////////
	//GFX->RemoveTerrain();
	//GFX->InitTerrain(100, 100, 255, 255, 1);	// Medium Terrain
	//GFX->InitTerrain(180, 180, 510, 510, 1);	// Large Terrain
	GFX->SetBrush(0);	//&m_fBrushRadius);

	m_TerrainTex[0] = "Grass/Grass1D.png"; //"seafloor.bmp";
	m_TerrainTex[1] = "Dirt/Dirt2D.png";   //"GrassBright.jpg";
	m_TerrainTex[2] = "Rock/Rock2D.png";   //"ground2.bmp";

	// Load Terrain Textures
	GFX->LoadTexture(&m_TerrainTex[0][0]);
	GFX->LoadTexture(&m_TerrainTex[1][0]);
	GFX->LoadTexture(&m_TerrainTex[2][0]);

	// Set Terrain Textures
	GFX->SetTerrainTex1(&m_TerrainTex[0][0]);
	GFX->SetTerrainTex2(&m_TerrainTex[1][0]);
	GFX->SetTerrainTex3(&m_TerrainTex[2][0]);
	
	//////////////////////////////////////////////////
	//			Initialize UI Elements
	//////////////////////////////////////////////////
	// Add the first control to the screen
	auto pc = new PanelControl();
	AddControl(pc);

	CreateTerrainGUI();
	CreateObjectControlGUI();
	CreateBoundaryControlGUI();
	pc->MoveTowardsFront(50);
	
	// Create the LevelDesigner Sub-Menu screen
	//mLevelDesignerPanel = new TransitionController(SLIDEUP, 1.0f, true);
	mLevelDesignerMenu = new PanelControl();
	AddControl(mLevelDesignerMenu);
	//mLevelDesignerPanel->AddChild(mLevelDesignerMenu);

	// Create New Medium Level Button
	auto Btn_MedLevel = new ImageButton("UI/generic.png", "New Medium Level", "TimesNewRoman16");
	Btn_MedLevel->SetSize(111*2.0f, 57);
	Btn_MedLevel->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		GFX->RemoveTerrain();
		//GFX->InitTerrain(100, 100, 255, 255, 1);	// Medium Terrain
		GFX->InitTerrain(170, 170, 510, 510, 1);	// Large Terrain
		//GFX->InitTerrain(300, 300, 1000, 1000, 1);	// Large Terrain
		GFX->SetBrush(0);	//&m_fBrushRadius);
		
		m_TerrainTex[0] = "Grass/Grass1D.png"; //"seafloor.bmp";
		m_TerrainTex[1] = "Dirt/Dirt2D.png";   //"GrassBright.jpg";
		m_TerrainTex[2] = "Rock/Rock2D.png";   //"ground2.bmp";

		// Load Terrain Textures
		GFX->LoadTexture(&m_TerrainTex[0][0]);
		GFX->LoadTexture(&m_TerrainTex[1][0]);
		GFX->LoadTexture(&m_TerrainTex[2][0]);

		// Set Terrain Textures
		GFX->SetTerrainTex1(&m_TerrainTex[0][0]);
		GFX->SetTerrainTex2(&m_TerrainTex[1][0]);
		GFX->SetTerrainTex3(&m_TerrainTex[2][0]);
		
		// Add a light
		auto light = std::shared_ptr<LightEntity>(new LightEntity);
		light->SetPosition(Vector3(-58.1764f, 132.938f, 60.3592));
		light->SetScale(Vector3(16.36f, 16.36f, 16.36f));
		light->Init();
		light->Update(0.0f);	// Apply position changes
		ScreenMan->GetEntityManager()->AddEntity(light->GetID(), light);

		//  Hide this menu screen
		mLevelDesignerMenu->SetVisible(false);
		mTerrainControls->SetVisible(true);
		mMouse.ShutDown();	// Remove old D-Pad menu
	};
	mLevelDesignerMenu->AddChild(Btn_MedLevel);
	
	// Create New Large Level Button
	auto Btn_LargeLevel = new ImageButton("UI/generic.png", "New Large Level", "TimesNewRoman16");
	Btn_LargeLevel->SetSize(111*2.0f, 57);
	Btn_LargeLevel->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		GFX->RemoveTerrain();
		//GFX->InitTerrain(180, 180, 510, 510, 1);	// Large Terrain
		//GFX->InitTerrain(170, 170, 510, 510, 1);	// Large Terrain
		GFX->InitTerrain(600, 600, 2000, 2000, 1);	// Large Terrain
		GFX->SetBrush(0);	//&m_fBrushRadius);
		
		m_TerrainTex[0] = "Grass/Grass1D.png"; //"seafloor.bmp";
		m_TerrainTex[1] = "Dirt/Dirt2D.png";   //"GrassBright.jpg";
		m_TerrainTex[2] = "Rock/Rock2D.png";   //"ground2.bmp";

		// Load Terrain Textures
		GFX->LoadTexture(&m_TerrainTex[0][0]);
		GFX->LoadTexture(&m_TerrainTex[1][0]);
		GFX->LoadTexture(&m_TerrainTex[2][0]);

		// Set Terrain Textures
		GFX->SetTerrainTex1(&m_TerrainTex[0][0]);
		GFX->SetTerrainTex2(&m_TerrainTex[1][0]);
		GFX->SetTerrainTex3(&m_TerrainTex[2][0]);
			
		// Add a light
		auto light = std::shared_ptr<LightEntity>(new LightEntity);
		light->SetPosition(Vector3(-58.1764f, 132.938f, 60.3592));
		light->SetScale(Vector3(16.36f, 16.36f, 16.36f));
		light->Init();
		light->Update(0.0f);	// Apply position changes
		ScreenMan->GetEntityManager()->AddEntity(light->GetID(), light);

		//  Hide this menu screen
		mLevelDesignerMenu->SetVisible(false);
		mTerrainControls->SetVisible(true);
		mMouse.ShutDown();	// Remove old D-Pad menu
	};
	mLevelDesignerMenu->AddChild(Btn_LargeLevel);

	// Load Level Button
	auto Btn_LoadLevel = new ImageButton("UI/generic.png", "Load Level", "TimesNewRoman16");
	Btn_LoadLevel->SetSize(111*2.0f, 57);
	Btn_LoadLevel->OnClicked = [=] (ImageButton* sender, void* userData)
	{
#if 0
		// Use TextBox
		static bool clicked = false;
		static TextField* tf = 0;
		static ImageButton* Btn_TextBox = 0;
		// Pre declare the text field so that the textbox can delete it
		if(!clicked)
		{
			tf = new TextField(Vector2(500, 20), "");
			Btn_TextBox = new ImageButton("UI/generic.png");
			tf->MoveTowardsFront(3);
			Btn_TextBox->MoveTowardsFront(2);
		}
		else
		{
			// Cancel load (Doesn't work)
			clicked = false;
			mLevelDesignerMenu->RemoveChild(tf);
			mLevelDesignerMenu->RemoveChild(Btn_TextBox);
		}

		// Setup the TextBox
		Btn_TextBox->SetSize(222, 114);
		Btn_TextBox->SetPosition(Vector2((-222.0f + Btn_LoadLevel->GetSize().x)*0.5f, -57.0f));
		Btn_TextBox->OnClicked = [=] (ImageButton* sender, void* userData)
		{ 	
			// Load the Level
			/*GPM->GetEngine()->GetGraphicsCore()->LoadTerrain(&tf->GetText()[0]);
			clicked = false;
			mOptionsMenu->RemoveChild(tf);
			mOptionsMenu->RemoveChild(Btn_TextBox);*/
		};
		mLevelDesignerMenu->AddChild(Btn_TextBox);
		auto tc_TextBox = new TextControl("TimesNewRoman16", "Enter Level Name:\n");
		tc_TextBox->SetPosition(30, 30);
		Btn_TextBox->AddChild(tc_TextBox);

		// Set up the text field
		tf->OnEnter = [=] (TextField* sender, void* userData)
		{ 		
			// Load the level
			bool result = false;
			std::string name =  sender->GetText();
			if(!name.empty())
			{
				result = ScreenMan->GetEntityManager()->LoadLevel(&name[0]);
				if(result)
				{
					ScreenMan->GetEntityManager()->InitializeGameObjects(0);
					// Setup the initial camera
					Vector3 pos;
					Quaternion rot;
					ScreenMan->GetEntityManager()->GetDefaultCamera(pos, rot);
					CamMgr->GetCameraControlByID(m_iCameraID)->SetPosition(pos.x, pos.y, pos.z);
					((GameplayCam*)(CamMgr->GetCameraControlByID(m_iCameraID)))->SetLookAt(rot);
				}
			}
			clicked = false;
			
			//  Hide this menu screen
			mLevelDesignerMenu->RemoveChild(Btn_TextBox);

			if(result)
			{
				mLevelDesignerMenu->SetVisible(false);
				mTerrainControls->SetVisible(true);
				mMouse.ShutDown();	// Remove old D-Pad menu
			}
		};
		tf->SetPosition(28, 70);
		Btn_TextBox->AddChild(tf);
#else
		// Use Level Selection Menu
		m_iSelectionResponse = 1;	// Set selection response to load the level
		ScreenMan->AddScreen(new GameplayMenu());
		mLevelDesignerMenu->SetVisible(false);
		mTerrainControls->SetVisible(true);
		mMouse.ShutDown();	// Remove old D-Pad menu
		SwitchBrushState(state_None);
#endif
	};
	mLevelDesignerMenu->AddChild(Btn_LoadLevel);

	// Delete Level Button
	auto Btn_DeleteLevel = new ImageButton("UI/generic.png", "Delete Level", "TimesNewRoman16");
	Btn_DeleteLevel->SetSize(111*2.0f, 57);
	Btn_DeleteLevel->OnClicked = [=] (ImageButton* sender, void* userData)
	{
		// Use Level Selection Menu
		m_iSelectionResponse = -1;	// Set selection response to delete the level
		ScreenMan->AddScreen(new GameplayMenu());
	};
	mLevelDesignerMenu->AddChild(Btn_DeleteLevel);

	// Set the screen Menu layout
	mLevelDesignerMenu->LayoutColumn(50.0f);
	
	auto screenSize = ScreenMan->GetGraphics()->GetScreenSize();
	Vector2 menuSize = Btn_MedLevel->GetSize();
	menuSize.y *= (pc->GetChildCount() + 2);
	mLevelDesignerMenu->SetPosition((screenSize - menuSize) * 0.5f);

	// Create D-Pad menu selection
	mMouse.AddButton(Btn_MedLevel);
	mMouse.AddButton(Btn_LargeLevel);
	mMouse.AddButton(Btn_LoadLevel);
	mMouse.AddButton(Btn_DeleteLevel);
	mMouse.Initialize();
	
	//////////////////////////////////////////////////
	// Add Catagories starting with "Menu"
	//////////////////////////////////////////////////
	AddToCategory("Menu", "Gameplay");
	AddToCategory("Menu", "Scenery");
	AddToCategory("Menu", "Obstacles");

	// Sub-Menus
	AddToCategory("Gameplay", "Trigger Volumes");
	AddToCategory("Scenery", "Rocks");
	AddToCategory("Scenery", "Foliage");

	//////////////////////////////////////////////////
	// Add Object Choices
	//////////////////////////////////////////////////
	// Barrel
	auto object = std::shared_ptr<PhysicsEntity>(new PhysicsEntity(ET_PHYSICSOBJECT));
	object->SetMeshName("barrel1.x");
	object->SetRenderable(true);
	AddToCategory("Obstacles", "Barrel", object);
	// Set Default Mesh attributes
	object->Init();
	GFX->SetDefaultMeshLightAttributes("barrel1.x", 0.25f, 60.0f, 50.0f);
	object->Shutdown();	// Remove this instance of world object
	
	// Box
	object = std::shared_ptr<PhysicsEntity>(new PhysicsEntity(ET_PHYSICSOBJECT));
	object->SetMeshName("Box.x");
	object->SetRenderable(true);
	AddToCategory("Obstacles", "Box", object);
	// Set Default Mesh attributes
	object->Init();
	GFX->SetDefaultMeshTexture("Box.x", "tile010.jpg");
	GFX->SetDefaultMeshLightAttributes("Box.x", 0.5f, 60.0f, 100.0f);
	object->Shutdown();	// Remove this instance of world object
	
	// Bolder
	auto bolder = std::shared_ptr<PhysicsEntity>(new PhysicsEntity(ET_PHYSICSOBJECT));
	bolder->SetMeshName("Rock5.X");	
	bolder->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	bolder->SetRenderable(true);
	bolder->SetIsSphere(true);
	bolder->SetMass(10000.0f);
	bolder->SetAngDamining(1.0f);
	AddToCategory("Obstacles", "Boulder", bolder);
	
	// Start Point
	auto object2 = std::shared_ptr<BaseGameEntity>(new BaseGameEntity());
	object2->SetMeshName("StartPoint.X");
	object2->SetRenderable(false);
	object2->SetEntityType(ET_SPAWNPOINT);
	object2->SetColor(D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.0f));
	object2->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	object2->SetMaxNumCopies(1);
	m_pStartPointCounter = &object2->GetNumberCopies();
	AddToCategory("Gameplay", "Start Point",  object2);

	// Camera
	auto camera = std::shared_ptr<CameraEntity>(new CameraEntity());
	camera->SetMeshName("Camera.X");
	camera->SetRenderable(false);
	camera->SetColor(D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.0f));
	camera->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	camera->SetMaxNumCopies(1);	// Should use this as default level view
	AddToCategory("Gameplay", "Camera", camera);
	
	// Light
	auto light = std::shared_ptr<LightEntity>(new LightEntity());
	light->SetMaxNumCopies(20);
	AddToCategory("Scenery", "Light", light);
	
	// Turret
	auto turret = std::shared_ptr<Turret>(new Turret("Turret_GroundBase.X", "Turret_Mainbody.X", "Turret_Cannons.X"));
	turret->SetMaxNumCopies(20);
	turret->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	turret->SetColor(D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.0f));
	AddToCategory("Obstacles", "Turret", turret);
	
	// Smoke Trap
	auto smoke = std::shared_ptr<SmokeTrap>(new SmokeTrap("Gyser.X"));
	smoke->SetMaxNumCopies(20);
	smoke->SetScale(Vector3(0.01f, 0.01f, 0.01f));
	AddToCategory("Obstacles", "Geyser", smoke);
	
	// Meteor
	auto meteor = std::shared_ptr<DestructableObjects>(new DestructableObjects(_Explosion));
	meteor->SetMeshName("Rock5.X");
	meteor->SetScale( Vector3(0.1f, 0.1f, 0.1f) );
	AddToCategory("Obstacles", "Meteor", meteor);
	
	// Lightning trap
	auto lightningTrap = std::shared_ptr<LightningTrap>(new LightningTrap("Electrical Conductor.X"));
	lightningTrap->SetScale(Vector3(0.1f, 0.1f,0.1f));
	lightningTrap->SetMaxNumCopies(20);
	AddToCategory("Obstacles", "Volt Trap", lightningTrap);	

	// Create Tiled Object List (For testing)
	SetupAssets();
	CreateTriggerVolumeGUI();	// Create Tiled Object List (For testing)
	PopulateCatagories();

	// Hide unused UI
	mTerrainControls->SetVisible(false);
	mObjectControls->SetVisible(false);
	mBoundaryControls->SetVisible(false);
	mObjectLists->SetVisible(false);
	
	// Reset the current level name
	m_cLevelName = "";
}

void LevelDesignerScreen::CreateTriggerVolumeGUI()
{
 /*   // Race Volume
    auto object3 = std::shared_ptr<TriggerEntity>(new TriggerEntity(ET_TRIGGERVOLUME,_Race));
	object3->SetMeshName("BoxVolume.x");
	object3->SetRenderable(false);
	AddToCategory("Trigger Volumes", "Race", object3);

    //Lap Volume
    object3 = std::shared_ptr<TriggerEntity>(new TriggerEntity(ET_TRIGGERVOLUME,_Lap));
	object3->SetMeshName("BoxVolume.x");
	object3->SetRenderable(false);
	object3->SetColor(D3DXCOLOR(0.0f, 0.0f, 0.8f, 1.0f));	// Make trigger green
	AddToCategory("Trigger Volumes", "Lap", object3);
*/	
	//Damage Volume
    auto object3 = std::shared_ptr<TriggerEntity>(new TriggerEntity(ET_TRIGGERVOLUME,_Damage));
	object3->SetMeshName("BoxVolume.x");
	object3->SetRenderable(false);
	object3->SetColor(D3DXCOLOR(0.2f, 0.0f, 0.0f, 1.0f));	// Make trigger light red
	AddToCategory("Trigger Volumes", "Damage", object3);
	
/*	//Start Volume
    object3 = std::shared_ptr<TriggerEntity>(new TriggerEntity(ET_TRIGGERVOLUME,_Start));
	object3->SetMeshName("BoxVolume.x");
	object3->SetRenderable(false);
	AddToCategory("Trigger Volumes", "Start", object3);
*/	
	//Finish Volume
    object3 = std::shared_ptr<TriggerEntity>(new TriggerEntity(ET_TRIGGERVOLUME,_Finish));
	object3->SetMeshName("BoxVolume.x");
	object3->SetRenderable(false);
	object3->SetMaxNumCopies(1);
	m_pFinishLineCounter = &object3->GetNumberCopies();
	AddToCategory("Trigger Volumes", "Finish", object3);
	
	//Force Volume
    object3 = std::shared_ptr<TriggerEntity>(new TriggerEntity(ET_TRIGGERVOLUME,_Force));
	object3->SetMeshName("BoxDirVolume.x");
	object3->SetRenderable(false);
	object3->SetMaxNumCopies(20);
	object3->SetColor(D3DXCOLOR(0.0f, 0.8f, 0.0f, 1.0f));	// Make trigger green
	AddToCategory("Trigger Volumes", "Force", object3);
}

IControl::HandleInputResult LevelDesignerScreen::HandleInput(IInputCore* inputState, float dt)
{
	mMouse.HandleInput(inputState, dt);

	auto inputResult = IGameScreen::HandleInput(inputState, dt);

	// Consume input for button
	if(inputResult == IControl::HandleInputResult::CONSUME)
		return IControl::HandleInputResult::CONSUME;
	
	// Test going into pause screen
	if( !PauseScreen::AlreadyExists() &&
		(inputState->IsNewKeyRelease(DIK_ESCAPE) ||
		inputState->IsNewButtonRelease(START)) )
		ScreenMan->AddScreen( new PauseScreen(this, // Check if we are inside the level designer
				(m_iSelectionResponse == 0 && !mLevelDesignerMenu->IsVisible())) );

	//////////////////////////////////////////////////
	//			Terrain Editor input
	//////////////////////////////////////////////////
	// Update the brush position periodically (kind of expensive)
	static float count = 0.0f;
	count += dt;
	if( count > 0.1f && m_eCurrentBrushState < state_Num_Terrain_Tools && 
		m_eCurrentBrushState != 0)//0.3f)
	{
		auto GFX = ScreenMan->GetGraphics();
		GFX->UpdateBrush();
		count = 0.0f;
	}	

	// Scale brush size with mouse wheel
	if(m_eCurrentBrushState < state_Num_Terrain_Tools && 
		m_eCurrentBrushState != 0)
	{
		if(inputState->IsButtonDown(DPAD_LEFT))
			m_fBrushRadius -= 0.1f;
		else if(inputState->IsButtonDown(DPAD_RIGHT))
			m_fBrushRadius += 0.1f;
		else
			m_fBrushRadius += inputState->GetMouseDZ()*0.01f;
		if(m_fBrushRadius < 0.0f)	// Clip to 0.0f min
			m_fBrushRadius = 0.0f;
	}

	// Use Brush if Left Mouse Button is clicked
	if(inputState->IsMouseButtonDown(0))
		UseBrush(inputState, dt);

	// Toggle between terrain and object mode with the back button
	if(inputState->IsNewButtonRelease(BACK))
	{
		if(mTerrainControls->IsVisible())		// Switch to Object Mode
		{
			mObjectControls->SetVisible(true);
			mTerrainControls->SetVisible(false);
			mBoundaryControls->SetVisible(false);
			SwitchBrushState(state_None);
		}
		else if(mObjectControls->IsVisible())	// Switch to Boundary Mode
		{
			mTerrainControls->SetVisible(false);
			mObjectControls->SetVisible(false);
			mBoundaryControls->SetVisible(true);
			SwitchBrushState(state_Start_Custom_Paint);
		}
		else if(mBoundaryControls->IsVisible())	// Switch to Terrain Mode
		{
			mObjectControls->SetVisible(false);
			mTerrainControls->SetVisible(true);
			mBoundaryControls->SetVisible(false);
			SwitchBrushState(state_None);
		}
	}

#if defined(DEBUG) | defined(_DEBUG)
	// Recompile the shader without stoping the game
	if(inputState->IsNewKeyRelease(DIK_9))
		ScreenMan->GetGraphics()->ReloadShader();
	if(inputState->IsNewKeyRelease(DIK_1))
		ScreenMan->GetGraphics()->PrintBuffer("Depth");
	if(inputState->IsNewKeyRelease(DIK_2))
		ScreenMan->GetGraphics()->PrintBuffer("Pos");
	if(inputState->IsNewKeyRelease(DIK_3))
		ScreenMan->GetGraphics()->PrintBuffer("Norm");
	if(inputState->IsNewKeyRelease(DIK_4))
		ScreenMan->GetGraphics()->PrintBuffer("Mtrl");
	if(inputState->IsNewKeyRelease(DIK_5))
		ScreenMan->GetGraphics()->PrintBuffer("SSAO");
#endif

	//////////////////////////////////////////////////
	//			Do normal camera logic
	//////////////////////////////////////////////////
	CamMgr->HandleInput(inputState, dt);	
	return IControl::HandleInputResult::CONSUME;
}

void LevelDesignerScreen::UseBrush(IInputCore* inputState, float dt)
{
	// Depending on current seleted state. Use brush action.
	switch(m_eCurrentBrushState)
	{
	case state_None:
		{
			break;
		}
	// Terrain Editor
	case state_Morph_Up:
		{
			auto GFX = ScreenMan->GetGraphics();
			GFX->MorphUp(25.0f*dt);
			break;
		}
	case state_Morph_Down:
		{
			auto GFX = ScreenMan->GetGraphics();
			GFX->MorphDown(25.0f*dt);
			break;
		}
	case state_Smooth:
		{
			auto GFX = ScreenMan->GetGraphics();
			GFX->Smooth(dt);
			break;
		}
	case state_Extrude:
		{
			auto GFX = ScreenMan->GetGraphics();
			GFX->Smooth(-0.25f*(3.0f*dt));
			break;
		}
	case state_Paint_Tex_1:
		{
			auto GFX = ScreenMan->GetGraphics();
			GFX->PaintTex1(20.0f*dt);
			break;
		}
	case state_Paint_Tex_2:
		{
			auto GFX = ScreenMan->GetGraphics();
			GFX->PaintTex2(20.0f*dt);
			break;
		}
	case state_Paint_Tex_3:
		{
			auto GFX = ScreenMan->GetGraphics();
			GFX->PaintTex3(20.0f*dt);
			break;
		}
	// Custom texture painting
	case state_Paint_Custom_Tex_1:
		{
			auto GFX = ScreenMan->GetGraphics();
			GFX->PaintCustomTex1(20.0f*dt);
			break;
		}
	case state_Paint_Custom_Tex_2:
		{
			auto GFX = ScreenMan->GetGraphics();
			GFX->PaintCustomTex2(20.0f*dt);
			break;
		}
	case state_Paint_Custom_Tex_3:
		{
			auto GFX = ScreenMan->GetGraphics();
			GFX->PaintCustomTex3(20.0f*dt);
			break;
		}
	case state_Paint_Custom_Tex_4:
		{
			// Paint as black
			auto GFX = ScreenMan->GetGraphics();
			GFX->PaintCustomTex1(-20.0f*dt);
			GFX->PaintCustomTex2(-20.0f*dt);
			GFX->PaintCustomTex3(-20.0f*dt);
			break;
		}
	// Object Placement
	case state_Select:
		{
			auto GFX = ScreenMan->GetGraphics();
			
			// Un Highlight previous selection
			if(m_iSelectedID >= 0)
			{
				auto entity = ScreenMan->GetEntityManager()->GetEntity(m_iSelectedID);
				if(entity)
					GFX->SetMeshColor(m_iSelectedID, &m_cSelectedName[0], entity->GetColor());
				//GFX->SetMeshColor(m_iSelectedID, &m_cSelectedName[0], D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
			}
			// Check pick ray for new selection
			char selectedName[256];
			GFX->GetSelectedObj(selectedName, m_iSelectedID);
			m_cSelectedName = selectedName;
			
			// Highlight selected object
			if(m_iSelectedID >= 0)
			{
				if( ScreenMan->GetEntityManager()->GetEntity(m_iSelectedID) )
					GFX->SetMeshColor(m_iSelectedID, selectedName, D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));
				else
					m_iSelectedID = -1;
			}
			break;
		}
	case state_Move:
		{
			if(m_iSelectedID < 0)	// Make sure a valid ID is selected
				break;
			// Convert screen space change in mouse position to world space
			auto MEM = ScreenMan->GetMainCore()->m_mMainMemory;
			Vector3 pos = MEM->GetSharedResource(m_iSelectedID)->GetPosition();
			D3DXMATRIX view = CamMgr->m_pCurrentCamControl->GetView();
			D3DXMatrixInverse(&view, 0, &view);
			view._41 = pos.x;
			view._42 = pos.y;
			view._43 = pos.z;
			float scale = 0.1f; //0.08f;
			Vector3 diff;
			
			// Only apply to the axis' enabled
			if(m_bEnableAxisX)
				diff.x = inputState->GetMouseDX()*scale;
			if(m_bEnableAxisY)
				diff.y = inputState->GetMouseDY()*-1.0f*scale;
			if(m_bEnableAxisZ)
				diff.z = inputState->GetMouseDZ()*0.25f*scale;
			
			pos = diff.TransformCoord(view);

			// Do it this way until world entity manager is created
			MEM->GetSharedResource(m_iSelectedID)->SetPosition(pos);
			ScreenMan->GetEntityManager()->GetEntity(m_iSelectedID)->SetPosition(pos);
			ScreenMan->GetEntityManager()->GetEntity(m_iSelectedID)->Update(0.0f);
			break;
		}
	case state_Rotate:
		{
			if(m_iSelectedID < 0)
				break;
			auto MEM = ScreenMan->GetMainCore()->m_mMainMemory;
			Quaternion quat = MEM->GetSharedResource(m_iSelectedID)->GetOrientation();
			Quaternion mouseRot;
			// Convert screen space change in mouse rotation to world space
			D3DXMATRIX view = CamMgr->m_pCurrentCamControl->GetView();
			D3DXMatrixInverse(&view, 0, &view);
			float scale = 0.01f;
			Vector3 movement;

			// Only apply to the axis' enabled
			if(m_bEnableAxisX)
				movement.x = inputState->GetMouseDY()*-1.0f*scale;
			if(m_bEnableAxisY)
				movement.y = inputState->GetMouseDX()*-1.0f*scale;
			if(m_bEnableAxisZ)
				movement.z = inputState->GetMouseDZ()*-0.03f*scale;

			if(m_bEnableAxisX && m_bEnableAxisY && m_bEnableAxisZ)
				D3DXVec3TransformNormal(&movement, &movement, & view);
			mouseRot.SetFromYawPitchRoll(movement.y, movement.z, movement.x);
			quat *= mouseRot;

			// Do it this way until world entity manager is created
			MEM->GetSharedResource(m_iSelectedID)->SetOrientation(quat);
			ScreenMan->GetEntityManager()->GetEntity(m_iSelectedID)->SetOrientation(quat);
			ScreenMan->GetEntityManager()->GetEntity(m_iSelectedID)->Update(0.0f);
			break;
		}
	case state_Scale:
		{
			if(m_iSelectedID < 0)
				break;
			Vector3 scaleV; 
			auto GFX = ScreenMan->GetGraphics();
			GFX->GetMeshScale(m_iSelectedID, &m_cSelectedName[0], scaleV);
			
			// Convert screen space change in mouse position to world space
		/*	D3DXMATRIX view = CamMgr->m_pCurrentCamControl->GetView();
			D3DXMatrixInverse(&view, 0, &view);
			view._41 = scaleV.x;
			view._42 = scaleV.y;
			view._43 = scaleV.z;	*/
			float scale = 0.01f; //0.08f;
			Vector3 diff;
			float movement = inputState->GetMouseDX()*scale;
						
			// Only apply to the axis' enabled
			if(m_bEnableAxisX)
				diff.x = movement; //inputState->GetMouseDX()*scale;
			if(m_bEnableAxisY)
				diff.y = movement; //inputState->GetMouseDY()*-1.0f*scale;
			if(m_bEnableAxisZ)
				diff.z = movement; //inputState->GetMouseDZ()*0.25f*scale;
			
			//scaleV = diff.TransformCoord(view);
			scaleV += diff;

			// Set the scale in the mesh and in the entity
			GFX->SetMeshScale(m_iSelectedID, &m_cSelectedName[0], scaleV);
			ScreenMan->GetEntityManager()->GetEntity(m_iSelectedID)->SetScale(scaleV);
			break;
		}
	default:
		return;
	};
}

void LevelDesignerScreen::SwitchBrushState(BrushState toState)
{
	m_ePreviousBrushState = m_eCurrentBrushState;
	m_eCurrentBrushState = toState;

	// Switch cursor type
	if(m_eCurrentBrushState == 0 &&
		m_ePreviousBrushState != 0)
	{
		// When in "None" state, remove all cursors
		// Unselect the object
		if(m_iSelectedID >= 0)
		{
			// Set color back to what's stored in the entity
			ScreenMan->GetGraphics()->SetMeshColor(m_iSelectedID, &m_cSelectedName[0], ScreenMan->GetEntityManager()->GetEntity(m_iSelectedID)->GetColor());
			m_iSelectedID = -1;
		}
		// Convert to object placement cursor
		ScreenMan->GetGraphics()->SetBrush(0);	// Hides brush
	}
	else if(m_ePreviousBrushState < state_Num_Terrain_Tools &&
		m_eCurrentBrushState > state_Num_Terrain_Tools)
	{
		// Convert to object placement cursor
		ScreenMan->GetGraphics()->SetBrush(0);	// Hides brush
	}
	else if( (m_ePreviousBrushState > state_Num_Terrain_Tools ||
			 m_ePreviousBrushState == 0) &&
			 m_eCurrentBrushState != 0 &&
		m_eCurrentBrushState < state_Num_Terrain_Tools)
	{
		// Unselect the object
		if(m_iSelectedID >= 0)
		{
			// Set color back to what's stored in the entity
			ScreenMan->GetGraphics()->SetMeshColor(m_iSelectedID, &m_cSelectedName[0], ScreenMan->GetEntityManager()->GetEntity(m_iSelectedID)->GetColor());
			m_iSelectedID = -1;
		}

		// Convert to terrain editor cursor(brush)
		ScreenMan->GetGraphics()->SetBrush(&m_fBrushRadius);	// Unhides brush
	}
	// Toggle custom texture display
	if(m_eCurrentBrushState >= state_Start_Custom_Paint &&
		m_eCurrentBrushState < state_Num_Terrain_Tools)
		ScreenMan->GetGraphics()->RenderCustomTextue(true);
	else
		ScreenMan->GetGraphics()->RenderCustomTextue(false);
}

void LevelDesignerScreen::Draw2D(IGraphicsCore* gDevice)
{
	// Draw 2D UI
	IGameScreen::Draw2D(gDevice);
}

void LevelDesignerScreen::ShutDown()
{
	if(m_iCameraID >= 0)
	{
		// Remove the camera
		m_vPreviousCamPos = CamMgr->GetPosition();
		CamMgr->DeleteCameraControl(m_iCameraID);
		m_iCameraID = -1;
	}
	mMouse.ShutDown();
	m_ObjectList.clear();
	IGameScreen::ShutDown();
}

void LevelDesignerScreen::SelectLevel(std::string level)
{
	if(m_iSelectionResponse == 1)
	{
		// Load the level
		// Begin the level designer
		if( ScreenMan->GetEntityManager()->LoadLevel( &level[0] ) )
		{
			m_cLevelName = level;
			ScreenMan->GetEntityManager()->InitializeGameObjects(0);
	
			// Setup the initial camera
			Vector3 pos;
			Quaternion rot;
			ScreenMan->GetEntityManager()->GetDefaultCamera(pos, rot);
			CamMgr->SetPosition(pos.x, pos.y, pos.z);
			((GameplayCam*)(CamMgr->m_pCurrentCamControl))->SetLookAt(rot);
		}
	}
	else if(m_iSelectionResponse == -1)
	{
		// Delete the level
		std::string msg = "Are you sure you want to delete \"";
		msg += level;
		msg += "\"?";
		MessageBoxScreen::ShowYesNo("Alert", msg.c_str(), [=] (ImageButton* sender, void* userData)
		{
			// Delete the level
			// Remove level to the Gameplay list of levels
			std::fstream file;
			std::string storage;
			file.open("Content/Levels/LevelList.list", fstream::in);	// Append to the end of the file
			if(file.is_open())
			{
				bool found = false;
				std::string line;
				// Check if level is already in the list
				while(!file.eof())
				{
					getline(file, line);
					if(line.find(level, 0) != string::npos)
						found = true;		// Skip saving the line that we are removing
					else if(!line.empty())
						storage.append( line + "\n" );	// Save all other lines
				}
				file.close();

				if(found)
				{
					// Rewrite the level list, without the removed level
					file.open("Content/Levels/LevelList.list", fstream::out | fstream::trunc);	// Overwrite the file
					file.write( storage.c_str(), storage.size() );	// Add back in the modified level list file
					file.close();

					// Delete all of the files for this level
					std::string dir = "Content/Levels/" + level;
					dir.append(1, '\0');

					SHFILEOPSTRUCT fileop = {0};
					fileop.hwnd   = NULL;    // no status display
					fileop.wFunc  = FO_DELETE;  // delete operation
					fileop.pFrom  = dir.c_str();  // source file name as double null terminated string
					fileop.pTo    = NULL;    // no destination needed
					fileop.fFlags = FOF_NOCONFIRMATION|FOF_SILENT;  // do not prompt the user
					fileop.fAnyOperationsAborted = FALSE;
					fileop.lpszProgressTitle     = NULL;
					fileop.hNameMappings         = NULL;
					int ret = SHFileOperation(&fileop); 
				}
			}
		});
	}

    //Put here to fix bug not being able to edit levels
    m_iSelectionResponse = 0;
}
void LevelDesignerScreen::UpdateLoadedTextures()
{
	// Update the texture names in the GUI
	char buff[256];
	for(int i = 0; i < 3; i++)
	{
		memset(buff, 0, 256);
		ScreenMan->GetGraphics()->GetTexture(i, buff);
		m_TerrainTex[i] = buff;
		mTfPaintTex[i]->SetText( m_TerrainTex[i] );
	}
}

void LevelDesignerScreen::EnterScreen()
{
	ScreenMan->GetEntityManager()->SetDisplayHiddenObject(true);	// Unhide all objects
	SwitchBrushState(m_ePreviousBrushState);
	
	// Set the camera back to free roam
	m_iCameraID = CamMgr->CreateCamera(FREECAM);
	CamMgr->SetActiveCamControl(m_iCameraID);
	CamMgr->GetCameraControlByID(m_iCameraID)->SetPosition(m_vPreviousCamPos.x, m_vPreviousCamPos.y, m_vPreviousCamPos.z);
	CamMgr->GetCameraControlByID(m_iCameraID)->SetLookAt(m_vPreviousCamLook, m_vPreviousCamUp);
}
void LevelDesignerScreen::ExitScreen()
{
	// Remove the camera
	m_vPreviousCamPos  = CamMgr->GetCameraControlByID(m_iCameraID)->GetPosition();
	m_vPreviousCamLook = CamMgr->GetCameraControlByID(m_iCameraID)->GetLookAt();
	m_vPreviousCamUp   = CamMgr->GetCameraControlByID(m_iCameraID)->GetUp();
	m_vPreviousCamLook = m_vPreviousCamPos + m_vPreviousCamLook.NormalizeRet();
	m_vPreviousCamUp.Normalize();
	CamMgr->DeleteCameraControl(m_iCameraID);
	m_iCameraID = -1;

	SwitchBrushState(state_None);
	ScreenMan->GetEntityManager()->SetDisplayHiddenObject(false);	// Hide non-gameplay objects
}
Vector3 LevelDesignerScreen::GetPrevCamPos()
{
	return m_vPreviousCamPos;
}

// Create UI Screens
void LevelDesignerScreen::CreateTerrainGUI()
{
	// Set Graphics World Properties
	auto GFX = ScreenMan->GetGraphics();

	// Add Terrain Controls Panel
	mTerrainControls = new PanelControl();
	AddControl(mTerrainControls);

	// Add buttons
	// Morph Up 
	auto Btn_MorphUp = new ImageButton("UI/generic.png", "Pile Up", "TimesNewRoman16");
	Btn_MorphUp->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Morph_Up);
	};
	mTerrainControls->AddChild(Btn_MorphUp);
	
	// Morph Down
	auto Btn_MorphDown = new ImageButton("UI/generic.png", "Dig", "TimesNewRoman16");
	Btn_MorphDown->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Morph_Down);
	};
	mTerrainControls->AddChild(Btn_MorphDown);
	
	// Smooth
	auto Btn_Smooth = new ImageButton("UI/generic.png", "Smooth", "TimesNewRoman16");
	Btn_Smooth->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Smooth);
	};
	mTerrainControls->AddChild(Btn_Smooth);
	
	// Extrude
	auto Btn_Extrude = new ImageButton("UI/generic.png", "Extrude", "TimesNewRoman16");
	Btn_Extrude->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Extrude);
	};
	mTerrainControls->AddChild(Btn_Extrude);

	// Set the layout as columns
	mTerrainControls->LayoutColumn(50.0f);
	
	// Add new panel control for terrain texturing 
	auto pc2 = new PanelControl();
	mTerrainControls->AddChild(pc2);

	auto painBtnSize = Vector2(111.0f * 2.0f,  57.0f);
	auto paintFieldSize = Vector2(111.0f * 1.5f,  20.0f);

	// Paint Texture 1
	auto Btn_Paint_Tex_1 = new ImageButton("UI/generic.png");
	Btn_Paint_Tex_1->SetSize(painBtnSize);
	Btn_Paint_Tex_1->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Paint_Tex_1);
	};
	pc2->AddChild(Btn_Paint_Tex_1);
	// Set up the text field
	mTfPaintTex[0] = new TextField(paintFieldSize, m_TerrainTex[0].c_str());
	mTfPaintTex[0]->SetPosition(22, 18);
	mTfPaintTex[0]->OnEnter = [=] (TextField* sender, void* userData)
	{ 		
		// Load the texture and set as terrain texture
		std::string name =  sender->GetText();
		if(!name.empty())
		{
			m_TerrainTex[0] = name;
			GFX->LoadTexture(&name[0]);		// Make sure texture is loaded.
			GFX->SetTerrainTex1(&name[0]);	// Will stay as default if invalid texture is entered
		}
	};
	Btn_Paint_Tex_1->AddChild(mTfPaintTex[0]);
	
	// Paint Texture 2
	auto Btn_Paint_Tex_2 = new ImageButton("UI/generic.png");
	Btn_Paint_Tex_2->SetSize(painBtnSize);
	Btn_Paint_Tex_2->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Paint_Tex_2);
	};
	pc2->AddChild(Btn_Paint_Tex_2);

	// Set up the text field
	mTfPaintTex[1] = new TextField(paintFieldSize, m_TerrainTex[1].c_str());
	mTfPaintTex[1]->SetPosition(22, 18);
	mTfPaintTex[1]->OnEnter = [=] (TextField* sender, void* userData)
	{ 		
		// Load the texture and set as terrain texture
		std::string name =  sender->GetText();
		if(!name.empty())
		{
			m_TerrainTex[1] = name;
			GFX->LoadTexture(&name[0]);		// Make sure texture is loaded.
			GFX->SetTerrainTex2(&name[0]);	// Will stay as default if invalid texture is entered
		}
	};
	Btn_Paint_Tex_2->AddChild(mTfPaintTex[1]);
	
	// Paint Texture 3
	auto Btn_Paint_Tex_3 = new ImageButton("UI/generic.png");
	Btn_Paint_Tex_3->SetSize(painBtnSize);
	Btn_Paint_Tex_3->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Paint_Tex_3);
	};
	pc2->AddChild(Btn_Paint_Tex_3);

	// Set up the text field
	mTfPaintTex[2] = new TextField(paintFieldSize, m_TerrainTex[2].c_str());
	mTfPaintTex[2]->SetPosition(22, 18);
	mTfPaintTex[2]->OnEnter = [=] (TextField* sender, void* userData)
	{ 		
		// Load the texture and set as terrain texture
		std::string name =  sender->GetText();
		if(!name.empty())
		{
			m_TerrainTex[2] = name;
			GFX->LoadTexture(&name[0]);		// Make sure texture is loaded.
			GFX->SetTerrainTex3(&name[0]);	// Will stay as default if invalid texture is entered
		}
	};
	Btn_Paint_Tex_3->AddChild(mTfPaintTex[2]);

	// Switch to Object Controls
	auto Btn_ObjectMode = new ImageButton("UI/generic.png", "Objects", "TimesNewRoman16");
	Btn_ObjectMode->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = 0;

		mTerrainControls->SetVisible(false);
		this->SwitchBrushState(state_None);
		mObjectControls->SetVisible(true);
	};
	pc2->AddChild(Btn_ObjectMode);
	
	// Switch to Boundary Controls
	auto Btn_BoundaryMode = new ImageButton("UI/generic.png", "Zones", "TimesNewRoman16");
	Btn_BoundaryMode->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = 0;

		mTerrainControls->SetVisible(false);
		this->SwitchBrushState(state_Start_Custom_Paint);
		mBoundaryControls->SetVisible(true);
	};
	pc2->AddChild(Btn_BoundaryMode);
	
	// Layout color control panel to the right
	pc2->LayoutRow(160.0f);
}
void LevelDesignerScreen::CreateObjectControlGUI()
{
	// Set Graphics World Properties
	auto GFX = ScreenMan->GetGraphics();

	// Add Terrain Controls Panel
	mObjectControls = new PanelControl();
	AddControl(mObjectControls);

	// Add buttons
	// Select
	auto Btn_Select = new ImageButton("UI/generic.png", "Select", "TimesNewRoman16");
	Btn_Select->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Select);
	};
	mObjectControls->AddChild(Btn_Select);
	
	// Move
	auto Btn_Move = new ImageButton("UI/generic.png", "Move", "TimesNewRoman16");
	Btn_Move->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Move);
	};
	mObjectControls->AddChild(Btn_Move);
	
	// Rotate
	auto Btn_Rotate = new ImageButton("UI/generic.png", "Rotate", "TimesNewRoman16");
	Btn_Rotate->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Rotate);
	};
	mObjectControls->AddChild(Btn_Rotate);
	
	// Scale
	auto Btn_Scale = new ImageButton("UI/generic.png", "Scale", "TimesNewRoman16");
	Btn_Scale->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Scale);
	};
	mObjectControls->AddChild(Btn_Scale);
	
		// Axis selection
		auto axis = new PanelControl();
		mObjectControls->AddChild(axis);

		// Enable X-axis
		auto Btn_X =  new ImageButton("UI/generic.png", "X", "TimesNewRoman16");
		Btn_X->XTextPadding = 14.0f;
		Btn_X->YTextPadding = 13.0f;
		Btn_X->SetSize(1.0f, 1.0f);
		Btn_X->OnClicked = [=] (ImageButton* sender, void* userData)
		{ 
			this->m_bEnableAxisX = (!m_bEnableAxisX);
			// Toggle the button image
			sender->SetToggle(m_bEnableAxisX);
		};
		Btn_X->SetToggle(m_bEnableAxisX);
		axis->AddChild(Btn_X);
	
		// Enable Y-axis
		auto Btn_Y =  new ImageButton("UI/generic.png", "Y", "TimesNewRoman16");
		Btn_Y->XTextPadding = 14.0f;
		Btn_Y->YTextPadding = 13.0f;
		Btn_Y->SetSize(1.0f, 1.0f);
		Btn_Y->OnClicked = [=] (ImageButton* sender, void* userData)
		{ 
			this->m_bEnableAxisY = (!m_bEnableAxisY);
			// Toggle the button image
			sender->SetToggle(m_bEnableAxisY);
		};
		Btn_Y->SetToggle(m_bEnableAxisY);
		axis->AddChild(Btn_Y);

		// Enable Z-axis
		auto Btn_Z =  new ImageButton("UI/generic.png", "Z", "TimesNewRoman16");
		Btn_Z->XTextPadding = 14.0f;
		Btn_Z->YTextPadding = 13.0f;
		Btn_Z->SetSize(1.0f, 1.0f);
		Btn_Z->OnClicked = [=] (ImageButton* sender, void* userData)
		{ 
			this->m_bEnableAxisZ = (!m_bEnableAxisZ);
			// Toggle the button image
			sender->SetToggle(m_bEnableAxisZ);
		};
		Btn_Z->SetToggle(m_bEnableAxisZ);
		axis->AddChild(Btn_Z);

		// Set the layout as columns
		axis->LayoutRow(10.0f);

	// Set the layout as columns
	mObjectControls->LayoutColumn(50.0f);
	
	// Add new panel control for top bar
	auto pc2 = new PanelControl();
	mObjectControls->AddChild(pc2);

	// Create Object
	auto Btn_Create = new ImageButton("UI/generic.png", "Create", "TimesNewRoman16");
	Btn_Create->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Open the Create object table
		if(mObjectLists->IsVisible())
		{
			m_CategoryMenu[m_CurrentMenu]->SetVisible(false);
			m_CurrentMenu = "Menu";				// Reset to the front menu
			mObjectLists->SetVisible(false);
		}
		else
		{
			mObjectLists->SetVisible(true);
			m_CategoryMenu[m_CurrentMenu]->SetVisible(true);
		}
	};
	pc2->AddChild(Btn_Create);
	
	// Delete Selected Object
	auto Btn_Delete = new ImageButton("UI/generic.png", "Delete", "TimesNewRoman16");
	Btn_Delete->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Delete the selected object
		if(!m_cSelectedName.empty())
		{
			ScreenMan->GetEntityManager()->RemoveEntity(m_iSelectedID);
			m_iSelectedID = -1;
			m_cSelectedName.clear();
		}
	};
	pc2->AddChild(Btn_Delete);
	
	// Switch to Terrain Controls
	auto Btn_TerrainMode = new ImageButton("UI/generic.png", "Terrain", "TimesNewRoman16");
	Btn_TerrainMode->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = 0;

		m_CategoryMenu[m_CurrentMenu]->SetVisible(false);
		m_CurrentMenu = "Menu";				// Reset to the front menu
		mObjectLists->SetVisible(false);
		mObjectControls->SetVisible(false);
		this->SwitchBrushState(state_None);
		mTerrainControls->SetVisible(true);
	};
	pc2->AddChild(Btn_TerrainMode);

	// Switch to Boundary Controls
	auto Btn_BoundaryMode = new ImageButton("UI/generic.png", "Zones", "TimesNewRoman16");
	Btn_BoundaryMode->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = 0;

		m_CategoryMenu[m_CurrentMenu]->SetVisible(false);
		m_CurrentMenu = "Menu";				// Reset to the front menu
		mObjectLists->SetVisible(false);
		mObjectControls->SetVisible(false);
		this->SwitchBrushState(state_Start_Custom_Paint);
		mBoundaryControls->SetVisible(true);
	};
	pc2->AddChild(Btn_BoundaryMode);
	
	// Layout color control panel to the right
	pc2->LayoutRow(160.0f);
}
void LevelDesignerScreen::CreateBoundaryControlGUI()
{
	auto GFX = ScreenMan->GetGraphics();
	
	// Add Boundary Controls Panel
	mBoundaryControls = new PanelControl();
	AddControl(mBoundaryControls);

	// Set the display name for what the colors represent
	std::string redBoundary		= "Damage Zone";
	std::string greenBoundary	= "Slide Zone";
	std::string blueBoundary	= "Slow Zone";
	std::string blackBoundary	= "Normal Zone";

	auto painBtnSize = Vector2(111.0f * 2.0f,  57.0f);
	auto paintFieldSize = Vector2(111.0f * 1.5f,  20.0f);
		
	// Switch to Object Controls
	auto Btn_ObjectMode = new ImageButton("UI/generic.png", "Objects", "TimesNewRoman16");
	Btn_ObjectMode->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = 0;

		mBoundaryControls->SetVisible(false);
		this->SwitchBrushState(state_None);
		mObjectControls->SetVisible(true);
	};
	mBoundaryControls->AddChild(Btn_ObjectMode);

	// Switch to Terrain Controls
	auto Btn_TerrainMode = new ImageButton("UI/generic.png", "Terrain", "TimesNewRoman16");
	Btn_TerrainMode->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = 0;

		mBoundaryControls->SetVisible(false);
		this->SwitchBrushState(state_None);
		mTerrainControls->SetVisible(true);
	};
	mBoundaryControls->AddChild(Btn_TerrainMode);

	// Set the layout as columns
	mBoundaryControls->LayoutColumn(50.0f);

	// Add new panel control for terrain texturing 
	auto pc2 = new PanelControl();
	mBoundaryControls->AddChild(pc2);

	// Paint Custom Texture 1
	auto Btn_Paint_Tex_1 = new ImageButton("UI/generic.png", redBoundary, "TimesNewRoman16");
	Btn_Paint_Tex_1->SetSize(painBtnSize);
	Btn_Paint_Tex_1->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Paint_Custom_Tex_1);
	};
	pc2->AddChild(Btn_Paint_Tex_1);
	
	// Paint Custom Texture 2
	auto Btn_Paint_Tex_2 = new ImageButton("UI/generic.png", greenBoundary, "TimesNewRoman16");
	Btn_Paint_Tex_2->SetSize(painBtnSize);
	Btn_Paint_Tex_2->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Paint_Custom_Tex_2);
	};
	pc2->AddChild(Btn_Paint_Tex_2);
	
	// Paint Custom Texture 3
	auto Btn_Paint_Tex_3 = new ImageButton("UI/generic.png", blueBoundary, "TimesNewRoman16");
	Btn_Paint_Tex_3->SetSize(painBtnSize);
	Btn_Paint_Tex_3->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Paint_Custom_Tex_3);
	};
	pc2->AddChild(Btn_Paint_Tex_3);

	// Paint Custom Texture 4
	auto Btn_Paint_Tex_4 = new ImageButton("UI/generic.png", blackBoundary, "TimesNewRoman16");
	Btn_Paint_Tex_4->SetSize(painBtnSize);
	Btn_Paint_Tex_4->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Toggle the button image
		if(mCurrentSelection)
			mCurrentSelection->SetToggle(false);
		mCurrentSelection = sender;
		mCurrentSelection->SetToggle(true);
		this->SwitchBrushState(state_Paint_Custom_Tex_4);
	};
	pc2->AddChild(Btn_Paint_Tex_4);

	// Layout color control panel to the right
	pc2->LayoutRow(160.0f);
}

void LevelDesignerScreen::CreateTiledObjectList(std::string category)
{
	auto GFX = ScreenMan->GetGraphics();

	// Add Category Menu
	m_CategoryMenu[category] = new PanelControl();
	m_CategoryMenu[category]->SetSize(450, 400);
	mObjectLists->AddChild( m_CategoryMenu[category] );
	

	// Load tile array of objects
	auto list =  &m_CategoryList[category];
	Vector2 largestSize(111.0f, 57.0f);
	for(int title = 0; title < list->size(); title++)
	{
		ImageButton* Btn_Tile;
		// Check if the string is for an object
		auto i = m_ObjectList.find( (*list)[title] );
		if(i == m_ObjectList.end())
		{
			// Add Sub-Menu Button
			Btn_Tile = new ImageButton("UI/generic.png", (*list)[title], "TimesNewRoman16");
			Btn_Tile->OnClicked = [=] (ImageButton* sender, void* userData)
			{
				m_CategoryMenu[category]->SetVisible(false);
				m_CurrentMenu	= (*list)[title] ;
				m_CategoryMenu[ (*list)[title] ]->SetVisible(true);
			};
			m_CategoryMenu[category]->AddChild(Btn_Tile);

			// Generate the Sub-Menu
			CreateTiledObjectList( (*list)[title] );
			m_CategoryMenu[ (*list)[title] ]->SetVisible(false);
		}
		else
		{
			// Add button for each object
			Btn_Tile = new ImageButton("UI/generic.png", (*i).first.c_str(), "TimesNewRoman16");
			Btn_Tile->OnClicked = [=] (ImageButton* sender, void* userData)
			{ 
				// Create a clone of that object and place it into the world
				auto clone = (*i).second->Clone();

				if(clone)
				{
					// Set the object right infront of the camera
					Vector3 pos = CamMgr->GetPosition();
					pos += CamMgr->m_pCurrentCamControl->GetLookAt() * 10.0f;
					clone->SetPosition(pos);
					clone->Update(0.0f);			// Apply the position changes
					ScreenMan->GetEntityManager()->AddEntity( clone->GetID(), clone);
				}
				else
				{
					// We have reached the max number of copies available for this object
					std::string msg = "You have reached the maximum number of objects for ";
					msg += (*i).first.c_str();
					msg += ". \n You can not create more then ";
					msg += std::to_string( (long double)(*i).second->GetMaxNumberCopies() );
					msg += " ";
					msg += (*i).first.c_str();
					msg += ".";
					MessageBoxScreen::Show("Alert", msg.c_str());
				}
				m_CategoryMenu[category]->SetVisible(false);
				m_CurrentMenu = "Menu";				// Reset to the front menu
				mObjectLists->SetVisible(false);
			};
			m_CategoryMenu[category]->AddChild(Btn_Tile);
		}

		// Store the largest possible button size
		if(largestSize.x < Btn_Tile->GetSize().x)
			largestSize.x = Btn_Tile->GetSize().x;
		if(largestSize.y < Btn_Tile->GetSize().y)
			largestSize.y = Btn_Tile->GetSize().y;
	}
	
	// Layout panel in the center of the screen
	m_CategoryMenu[category]->LayoutTileArray(Vector3(10, 10, 0), Vector3(largestSize.x, largestSize.y, 0));

	auto screenSize = GFX->GetScreenSize();
	m_CategoryMenu[category]->SetPosition((screenSize - m_CategoryMenu[category]->GetSize()) * 0.5f);
}
void LevelDesignerScreen::PopulateCatagories()
{
	auto GFX = ScreenMan->GetGraphics();

	// Create Object List Panel Control
	mObjectLists = new PanelControl();
	AddControl(mObjectLists);

	// Generate the categories
	CreateTiledObjectList( "Menu" );
}
void LevelDesignerScreen::AddToCategory(std::string category, std::string name, std::shared_ptr<BaseGameEntity> object)
{
	// Add name to the category
	m_CategoryList[category].push_back( name );

	// Link the object and the name
	if(object)
		m_ObjectList.insert(std::pair<std::string, std::shared_ptr<BaseGameEntity>>(name, object));	
}

bool LevelDesignerScreen::MeetsSaveRequirements()
{
	bool result = true;
	std::string msg = "Can't save the level:\n";
	if( (*m_pFinishLineCounter) != 1 )
	{
		// Make sure they have a finish line
		msg += "- Need a finish line\n";
		result = false;
	}
	if( (*m_pStartPointCounter) != 1 )
	{
		// Make sure they have a start point
		msg += "- Need a starting point\n";
		result = false;
	}

	if(!result)
		MessageBoxScreen::Show("Alert", msg.c_str());
	return result;
}
void LevelDesignerScreen::IncrementObjectList(std::shared_ptr<BaseGameEntity> entity)
{
	for(auto i = m_ObjectList.begin();
		i != m_ObjectList.end(); i++)
		if( entity->GetEntityType() == (*i).second->GetEntityType() )
		{
			// Compare the entities loaded to the original entity list
			// May require some upcasting to specific objects
			if( entity->GetEntityType() == ET_TRIGGERVOLUME )
			{
				if( *((TriggerEntity*)(entity.get())) == *(*i).second )
				{
					// Set the object's parent and increment local level designer counter
					entity->SetNumCopies( &(*i).second->GetNumberCopies() );
					(*i).second->GetNumberCopies()++;
					return;
				}
			}
			else if( entity->GetEntityType() == ET_PHYSICSOBJECT )
			{
				if( *((PhysicsEntity*)(entity.get())) == *(*i).second )
				{
					// Set the object's parent and increment local level designer counter
					entity->SetNumCopies( &(*i).second->GetNumberCopies() );
					(*i).second->GetNumberCopies()++;
					return;
				}
			}
            else if( entity->GetEntityType() == ET_Smasher )
			{
               if( *((Smasher*)(entity.get())) == *(*i).second )
				{
					// Set the object's parent and increment local level designer counter
					entity->SetNumCopies( &(*i).second->GetNumberCopies() );
					(*i).second->GetNumberCopies()++;
					return;
				}
			}
			else
			{
				if( *entity == *(*i).second )
				{
					// Set the object's parent and increment local level designer counter
					entity->SetNumCopies( &(*i).second->GetNumberCopies() );
					(*i).second->GetNumberCopies()++;
					return;
				}
			}
		}
}


void LevelDesignerScreen::SetupAssets()
{
	// Rock 1
	auto rock = std::shared_ptr<PhysicsEntity>(new PhysicsEntity(ET_PHYSICSOBJECT));
	rock->SetMeshName("Rock1.X");	
	rock->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	rock->SetRenderable(true);
	rock->SetGravity(false);
	rock->SetMass(10000.0f);
	rock->SetAngDamining(1.0f);
	AddToCategory("Rocks", "Rock Wall Large", rock);
	
	// Rock 2
	rock = std::shared_ptr<PhysicsEntity>(new PhysicsEntity(ET_PHYSICSOBJECT));
	rock->SetMeshName("Rock2.X");	
	rock->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	rock->SetRenderable(true);
	rock->SetGravity(false);
	rock->SetMass(10000.0f);
	rock->SetAngDamining(1.0f);
	AddToCategory("Rocks", "Rock Wall Small", rock);
	
	// Rock 3
	rock = std::shared_ptr<PhysicsEntity>(new PhysicsEntity(ET_PHYSICSOBJECT));
	rock->SetMeshName("Rock3.X");	
	rock->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	rock->SetRenderable(true);
	rock->SetGravity(false);
	rock->SetMass(10000.0f);
	rock->SetAngDamining(1.0f);
	AddToCategory("Rocks", "Rock Pillar", rock);
	
	// Rock 4
	rock = std::shared_ptr<PhysicsEntity>(new PhysicsEntity(ET_PHYSICSOBJECT));
	rock->SetMeshName("Rock4.X");	
	rock->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	rock->SetRenderable(true);
	rock->SetGravity(false);
	rock->SetMass(10000.0f);
	rock->SetAngDamining(1.0f);
	AddToCategory("Rocks", "Stalagmite", rock);
	
	// Rock 6
	/*rock = std::shared_ptr<PhysicsEntity>(new PhysicsEntity(ET_PHYSICSOBJECT));
	rock->SetMeshName("Rock6.X");	
	rock->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	rock->SetRenderable(true);
	rock->SetGravity(false);
	rock->SetMass(10000.0f);
	rock->SetAngDamining(1.0f);
	AddToCategory("Rocks", "Stalagmite2", rock);*/

	// Foliage
	// Small Plant
	auto smallPlant = std::shared_ptr<BaseGameEntity>(new BaseGameEntity());
	smallPlant->SetMeshName("SmallLeafyPlant.X");
	smallPlant->SetEntityType(ET_LEVELOBJECT);
	smallPlant->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	smallPlant->SetRenderable(true);
	AddToCategory("Foliage", "Small Plant", smallPlant);
	
	// Large Plant
	auto largePlant = std::shared_ptr<BaseGameEntity>(new BaseGameEntity());
	largePlant->SetMeshName("LargeLeafyPlant.X");
	largePlant->SetEntityType(ET_LEVELOBJECT);
	largePlant->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	largePlant->SetRenderable(true);
	AddToCategory("Foliage", "Large Plant", largePlant);
	
	// Skinny Palm Tree
	largePlant = std::shared_ptr<BaseGameEntity>(new BaseGameEntity());
	largePlant->SetMeshName("PalmTreeSkinny.X");
	largePlant->SetEntityType(ET_LEVELOBJECT);
	largePlant->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	largePlant->SetRenderable(true);
	AddToCategory("Foliage", "Skinny Palm Tree", largePlant);
	
	// Large Palm Tree
	largePlant = std::shared_ptr<BaseGameEntity>(new BaseGameEntity());
	largePlant->SetMeshName("PalmTreeLarge.X");
	largePlant->SetEntityType(ET_LEVELOBJECT);
	largePlant->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	largePlant->SetRenderable(true);
	AddToCategory("Foliage", "Large Palm Tree", largePlant);

	// Smasher
	auto smasher = std::shared_ptr<Smasher>(new Smasher());
	smasher->SetMeshName("Smasher_Head.X");
    smasher->SetEntityType(ET_Smasher);
	smasher->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	smasher->SetRenderable(true);
	AddToCategory("Obstacles", "Smasher", smasher);
}
