#include "TurbineSelectionScreen.h"
#include "../ScreenManager.h"
#include "../../DCEngineDemo/Controls/Camera/CameraManager.h"
#include "../../UI/Controls/SliderControl.h"
#include "../../UI/Controls/PanelControl.h"
#include "../../UI/Controls/EnergyBarControl.h"
#include "GameplayMenu.h"
#include "MainMenuScreen.h"
#include "PauseScreen.h"
#include "../../FSM/LoadGameState.h"
#include "../../WorldManager.h"
#include "../../Game Objects/Engine.h"
#include "../../Game Objects/Pod.h"
#include <dinput.h>

TurbineSelectionScreen::TurbineSelectionScreen()
	:IGameScreen()
{
	m_iEngineID[0]		= -1;
	m_iEngineID[1]		= -1;
	m_iEngineID[2]		= -1;
	m_iPodID[0]			= -1;
	m_iPodID[1]			= -1;
	m_iPodID[2]			= -1;
	m_iBackground		= -1;
	m_iSmallLogo		= -1;
	m_iCurrentEngine	= -1;
	m_iCurrentPod		= -1;
	m_iCameraID			= -1;
	m_iLightID			= -1;
	m_cPodName[0]		= "MKIII_Pod.X";
	m_cPodName[1]		= "MKIV_Pod.X";
	m_cPodName[2]		= "X95_Pod.X";
	m_cEngineName[0]	= "MKIII_Engine.X";
	m_cEngineName[1]	= "MKIV_Engine.X";
	m_cEngineName[2]	= "X95_Engine.X";


	m_fPodTransitionTimer = m_EngineTransitionTimer = m_cfTransitionDuration = 1.5f;
	// create sounds
	m_iBGMSounds1 =	ScreenMan->GetMainCore()->GetSoundCore()->CreateInstance2D("Content/Sounds/FX/22K/sfx_amb_watoshop_te_loop.wav",true,1,true);

	m_iBGMSounds2 = ScreenMan->GetMainCore()->GetSoundCore()->CreateInstance2D("Content/Sounds/FX/22K/sfx_amb_crowd_bazaar_loop.wav",true,1,true);

	m_iBGMSounds3 = ScreenMan->GetMainCore()->GetSoundCore()->CreateInstance2D("Content/Sounds/FX/22K/sfx_amb_crowd_distant_loop.wav",true,1,true);
}

void TurbineSelectionScreen::Update(float dt)
{
	if (mRootControl)
		mRootControl->Update(dt);

	updateTransitions(dt);

	// Add the goto
	
}

void TurbineSelectionScreen::Initialize()
{
	ScreenMan->GetEntityManager()->SetDisplayHiddenObject(false);	// Hide non-gameplay objects

	mTurbineSelection = new PanelControl();
	AddControl( mTurbineSelection );

	// Create the Background
	auto GFX = ScreenMan->GetGraphics();
	auto MEM = ScreenMan->GetMainCore()->m_mMainMemory;

	if(m_iBackground < 0)
	{
		m_iBackground = MEM->CreateSharedResource();
		GFX->CreateTexture(m_iBackground, "BackgroundImage.png");
	}

	// Create Small Logo
	if(m_iSmallLogo < 0)
	{
		m_iSmallLogo = MEM->CreateSharedResource();
		GFX->CreateTexture(m_iSmallLogo, "SmallLogo.png");
	}

	// Scale and Position the background
	auto screenSize = ScreenMan->GetGraphics()->GetScreenSize();
	Vector2 backgroundSize(1920, 1080);
	backgroundSize += (screenSize - backgroundSize);
	Vector3 scale = Vector3((backgroundSize.x/1920.0f), (backgroundSize.y/1080.0f), 1.0f);
	MEM->GetSharedResource(m_iBackground)->SetPosition(Vector3(0.0f, 0.0f, 1.0f));
	GFX->SetTextureScale(m_iBackground, "BackgroundImage.png", scale);
	GFX->SetTextureScale(m_iSmallLogo, "SmallLogo.png", scale);
	MEM->GetSharedResource(m_iSmallLogo)->SetPosition(Vector3(10.0f, 10.0f, 0.9f));

	// Add MainMenu Button
	auto Btn_MainMenu = new ImageButton("UI/generic.png", "Main Menu", "TimesNewRoman16");
	Btn_MainMenu->SetSize(222.0f, 57.0f);
	Btn_MainMenu->SetPosition(Vector2(10.0f, screenSize.y*0.9f));
	Btn_MainMenu->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Add MainMenu Screen and remove this screen
		//ScreenMan->AddScreen(new MainMenuScreen());
		//ScreenMan->RemoveScreen(this);
		ScreenMan->ExitScreens(new MainMenuScreen());

		// stop sounds, because main screen has different ones
		ScreenMan->GetMainCore()->GetSoundCore()->StopAll(1);
	};
	mTurbineSelection->AddChild(Btn_MainMenu);
	mMouse.AddButton(Btn_MainMenu);

	// Add Ready Button
	auto Btn_Ready = new ImageButton("UI/generic.png", "Ready", "TimesNewRoman16");
	Btn_Ready->SetSize(222.0f, 57.0f);
	Btn_Ready->SetPosition(Vector2(screenSize.x-232.0f, screenSize.y*0.9f));
	Btn_Ready->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Set the selected pod and engine to be loaded
		if(m_iCurrentEngine >= 0 &&	m_iCurrentPod >= 0)
			LOADGAME->SetSelectedTurbine(m_iCurrentPod, m_iCurrentEngine);

		
		// Add Gameplay Screen and remove this screen
		//ScreenMan->AddScreen(new GameplayMenu());
		//ScreenMan->RemoveScreen(this);
		ScreenMan->ExitScreens(new GameplayMenu());
	};
	mTurbineSelection->AddChild(Btn_Ready);
	mMouse.AddButton(Btn_Ready);

	////////////////////////////////////////////////
	// Add Selection UI
	////////////////////////////////////////////////
	// Initialize the Graphics
	m_iCameraID = CamMgr->CreateCamera(FREECAM);
	CamMgr->SetActiveCamControl(m_iCameraID);
	Vector3 pos(0, 10, -30);
	CamMgr->SetPosition(pos.x, pos.y, pos.z);
	Vector3 lookAt(0, 0, 1);
	CamMgr->m_pCurrentCamControl->SetLookAt(Vector3(0, 10, -29), Vector3(0.0f, 1.0f, 0.0f));
	
	auto view = CamMgr->m_pCurrentCamControl->GetView();
	auto rightVec = Vector3(view._11, view._21, view._31);
	rightVec.Normalize();
	
	m_vOnscreen_Pod		= pos + lookAt * 10.0f + Vector3(-4.0f, 0.0f, 0.0f);
	m_vOffscreen_Pod	= m_vOnscreen_Pod + (rightVec * -10.0f);
	m_vOnscreen_Engine	= pos + lookAt * 10.0f + Vector3(3.0f, 0.0f, 0.0f);
	m_vOffscreen_Engine	= m_vOnscreen_Engine + (rightVec * 10.0f);
	
	for(int i = 0; i < 3; i++)
	{
		// Create the shared memory
		m_iEngineID[i] = MEM->CreateSharedResource();
		m_iPodID[i] = MEM->CreateSharedResource();

		// Load in the Graphics
		GFX->CreateMesh(m_iEngineID[i], &m_cEngineName[i][0], false);
		GFX->CreateMesh(m_iPodID[i], &m_cPodName[i][0], false);

		// Set the scale
		if (i == 1)
			GFX->SetMeshScale(m_iEngineID[i], &m_cEngineName[i][0], Vector3(0.002f, 0.002f, 0.002f));
		else
			GFX->SetMeshScale(m_iEngineID[i], &m_cEngineName[i][0], Vector3(0.01f, 0.01f, 0.01f));
		GFX->SetMeshScale(m_iPodID[i], &m_cPodName[i][0], Vector3(0.005f, 0.005f, 0.005f));
		
		// Set the default position off screen
		MEM->GetSharedResource(m_iEngineID[i])->SetPosition(m_vOffscreen_Pod);
		MEM->GetSharedResource(m_iPodID[i])->SetPosition(m_vOffscreen_Engine);

		// Add a slight tilt so we can see the interesting parts better.
		auto quat = Quaternion();
		quat.SetFromYawPitchRoll(1.57f, -1, 0.0f);

		MEM->GetSharedResource(m_iEngineID[i])->SetOrientation(quat);
		MEM->GetSharedResource(m_iPodID[i])->SetOrientation(quat);
	}
	// Move the current pod onscreen
	//m_iCurrentPod = 0;
	//MEM->GetSharedResource(m_iPodID[m_iCurrentPod])->SetPosition(m_vOnscreen_Pod);

	// Move the current engine onscreen
	//m_iCurrentEngine = 0;
	//MEM->GetSharedResource(m_iEngineID[m_iCurrentEngine])->SetPosition(m_vOnscreen_Engine);

	// Add Previous Pod button
	auto Btn_Prev_pod = new ImageButton("UI/generic.png", "<", "PowerControl");
	//Btn_Prev_pod->SetSize(222.0f, 57.0f);
	Btn_Prev_pod->SetPosition(Vector2(screenSize.x*0.25f, screenSize.y*0.75f));
	Btn_Prev_pod->OnClicked = [=] (ImageButton* sender, void* userData)
	{
		if (m_fPodTransitionTimer != m_cfTransitionDuration)
			return;

		int podID = (m_iCurrentPod + 1) % 3;
		ChangeSelection(podID, m_iCurrentEngine);
	};
	mTurbineSelection->AddChild(Btn_Prev_pod);
	mMouse.AddButton(Btn_Prev_pod);

	// Add Next Pod button
	auto Btn_Next_pod = new ImageButton("UI/generic.png", ">", "PowerControl");
	//Btn_Next_pod->SetSize(222.0f, 57.0f);
	Btn_Next_pod->SetPosition(Vector2(screenSize.x*0.25f + Btn_Prev_pod->GetSize().x + 10.0f, screenSize.y*0.75f));
	Btn_Next_pod->OnClicked = [=] (ImageButton* sender, void* userData)
	{
		if (m_fPodTransitionTimer != m_cfTransitionDuration)
			return;

		int podID = (m_iCurrentPod - 1);
		if(podID < 0)
			podID = 2;
		ChangeSelection(podID, m_iCurrentEngine);
	};
	mTurbineSelection->AddChild(Btn_Next_pod);
	mMouse.AddButton(Btn_Next_pod);
	
	// Add Previous Engine button
	auto Btn_Prev_engine = new ImageButton("UI/generic.png", "<", "PowerControl");
	//Btn_Prev_engine->SetSize(222.0f, 57.0f);
	Btn_Prev_engine->SetPosition(Vector2(screenSize.x*0.5f, screenSize.y*0.75f));
	Btn_Prev_engine->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		if (m_EngineTransitionTimer != m_cfTransitionDuration)
			return;

		int engineID = (m_iCurrentEngine + 1) % 3;
		ChangeSelection(m_iCurrentPod, engineID);
	};
	mTurbineSelection->AddChild(Btn_Prev_engine);
	mMouse.AddButton(Btn_Prev_engine);

	// Add Next Engine button
	auto Btn_Next_Engine = new ImageButton("UI/generic.png", ">", "PowerControl");
	//Btn_Next_Engine->SetSize(222.0f, 57.0f);
	Btn_Next_Engine->SetPosition(Vector2(screenSize.x*0.5f + Btn_Prev_engine->GetSize().x + 10.0f, screenSize.y*0.75f));
	Btn_Next_Engine->OnClicked = [=] (ImageButton* sender, void* userData)
	{

		if (m_EngineTransitionTimer != m_cfTransitionDuration)
			return;
		int engineID = (m_iCurrentEngine - 1);
		if(engineID < 0)
			engineID = 2;
		ChangeSelection(m_iCurrentPod, engineID);
	};
	mTurbineSelection->AddChild(Btn_Next_Engine);
	mMouse.AddButton(Btn_Next_Engine);

	// Add Display Turbine Selection Stats
	Vector2 size(screenSize.x*0.25f, screenSize.y/20.0f);
	m_pHealthBar = new EnergyBarControl(size, "Health");
	m_pEnergyBar = new EnergyBarControl(size, "Energy");
	mTurbineSelection->AddChild(m_pHealthBar);
	mTurbineSelection->AddChild(m_pEnergyBar);
	m_pHealthBar->SetPosition(Vector2(screenSize.x*0.5f, 20.0f));
	m_pEnergyBar->SetPosition(Vector2(screenSize.x*0.5f, size.y + 60.0f));
	m_pHealthBar->SetColor(Vector3(.5,0,0), Vector3(.5,0,0));
	m_pEnergyBar->SetColor(Vector3(0,.5,0), Vector3(0,.5,0));

	// Initialize the stats
	ChangeSelection(0, 0);
	mMouse.SetupTable(2);
	mMouse.Initialize();

	//mTurbineSelection->LayoutColumn(50.0f);

	GFX->UseSSAO(true);
	GFX->SetCameraGlareType(10);

	if(m_iLightID < 0)
		GFX->AddLight(m_iLightID, Vector4(m_vOnscreen_Pod.x + 5.0f, m_vOnscreen_Pod.y + 5.0f, m_vOnscreen_Pod.z, 0.0), Vector4(1.0f, 0.0f, 0.0f, 0.0f), 44);

	// play sounds
	ScreenMan->GetMainCore()->GetSoundCore()->PlaySFX(m_iBGMSounds1,true,1);
	ScreenMan->GetMainCore()->GetSoundCore()->PlaySFX(m_iBGMSounds2,true,1);
	ScreenMan->GetMainCore()->GetSoundCore()->PlaySFX(m_iBGMSounds3,true,1);
}

void TurbineSelectionScreen::ShutDown()
{
	auto MEM = ScreenMan->GetMainCore()->m_mMainMemory;
	auto GFX = ScreenMan->GetGraphics();
	for(int i = 0; i < 3; i++)
	{
		GFX->RemoveMesh(m_iEngineID[i], &m_cEngineName[i][0]);
		GFX->RemoveMesh(m_iPodID[i], &m_cPodName[i][0]);
		MEM->FreeSharedResource(m_iEngineID[i]);
		MEM->FreeSharedResource(m_iPodID[i]);
		m_iEngineID[i] = m_iPodID[i] = -1;
	}
	
	// Remove Background and small logo
	GFX->RemoveTextrue(m_iBackground, "BackgroundImage.png");
	MEM->FreeSharedResource(m_iBackground);
	GFX->RemoveTextrue(m_iSmallLogo, "SmallLogo.png");
	MEM->FreeSharedResource(m_iSmallLogo);
	m_iBackground = m_iSmallLogo = -1;

	GFX->RemoveLight(m_iLightID);

	mMouse.ShutDown();
	CamMgr->DeleteCameraControl(m_iCameraID);
	m_iCameraID = -1;
	IGameScreen::ShutDown();

	
}

IControl::HandleInputResult TurbineSelectionScreen::HandleInput(IInputCore* inputState, float dt)
{
	mMouse.HandleInput(inputState, dt);

	auto inputResult = IGameScreen::HandleInput(inputState, dt);

	// When UI consumes input, for the frame, there's no need to pass
	// it along to the game manager. Think of how clicking on a button
	// shouldn't make the character fire their weapon.
	if (inputResult == IControl::HandleInputResult::CONSUME)
		return IControl::HandleInputResult::CONSUME;


	// experiment... gameplay screen should always consume
	// input?
	return IControl::HandleInputResult::CONSUME;
}

void TurbineSelectionScreen::Draw2D(IGraphicsCore* gDevice)
{
	// Do 2D Draw
	IGameScreen::Draw2D(gDevice);
}

void TurbineSelectionScreen::updateTransitions(float dt)
{	
	if (m_fPodTransitionTimer != m_cfTransitionDuration)
	{
		m_fPodTransitionTimer = min(m_fPodTransitionTimer + dt, m_cfTransitionDuration);

		auto lerpAmount = MathUtil::EaseInOut(m_fPodTransitionTimer / m_cfTransitionDuration, 4);

		auto lerpOnscreen = Vector3::Lerp(m_vOffscreen_Pod, m_vOnscreen_Pod, lerpAmount);
		auto lerpoffscreen = Vector3::Lerp(m_vOnscreen_Pod, m_vOffscreen_Pod, lerpAmount);

		auto MEM = ScreenMan->GetMainCore()->m_mMainMemory;

		MEM->GetSharedResource(m_iPodID[m_iCurrentPod])->SetPosition(lerpOnscreen);
		MEM->GetSharedResource(m_iPodID[m_iPrevPod])->SetPosition(lerpoffscreen);
	}

	if (m_EngineTransitionTimer != m_cfTransitionDuration)
	{
		m_EngineTransitionTimer = min(m_EngineTransitionTimer + dt, m_cfTransitionDuration);

		auto lerpAmount = MathUtil::EaseInOut(m_EngineTransitionTimer / m_cfTransitionDuration, 4);

		auto lerpOnscreen = Vector3::Lerp(m_vOffscreen_Engine, m_vOnscreen_Engine, lerpAmount);
		auto lerpoffscreen = Vector3::Lerp(m_vOnscreen_Engine, m_vOffscreen_Engine, lerpAmount);

		auto MEM = ScreenMan->GetMainCore()->m_mMainMemory;

		MEM->GetSharedResource(m_iEngineID[m_iCurrentEngine])->SetPosition(lerpOnscreen);
		MEM->GetSharedResource(m_iEngineID[m_iPrevEngine])->SetPosition(lerpoffscreen);
	}
	
}

void TurbineSelectionScreen::ChangeSelection(int podID, int engineID)
{
	auto MEM = ScreenMan->GetMainCore()->m_mMainMemory;

	if(m_iCurrentPod != podID && podID >= 0 && podID < 3)
	{
		m_iPrevPod = m_iCurrentPod;
		m_iCurrentPod = podID;

		m_fPodTransitionTimer = 0.0f;

		if (strcmp(&m_cPodName[m_iCurrentPod][0], "MKIII_Pod.X") == 0 )
		{
			m_fPodHealth = MKIII_POD_HEALTH;
			m_fPodEnergy = MKIII_POD_ENERGY;
		}
		else if (strcmp(&m_cPodName[m_iCurrentPod][0], "MKIV_Pod.X") == 0 )
		{
			m_fPodHealth = MKIV_POD_HEALTH;
			m_fPodEnergy = MKIV_POD_ENERGY;
		}
		else if (strcmp(&m_cPodName[m_iCurrentPod][0], "X95_Pod.X") == 0 )
		{
			m_fPodHealth = X95_POD_HEALTH;
			m_fPodEnergy = X95_POD_ENERGY;
		}
		// Update stats display
		m_pHealthBar->SetPower((m_fPodHealth + m_fEngineHealth)/(MAX_POD_HEALTH + MAX_ENGINE_HEALTH));
		m_pEnergyBar->SetPower((m_fPodEnergy + m_fEngineEnergy)/(MAX_POD_ENERGY + MAX_ENGINE_ENERGY));
	}

	if(m_iCurrentEngine != engineID && engineID >= 0 && engineID < 3)
	{
		m_iPrevEngine = m_iCurrentEngine;
		m_iCurrentEngine = engineID;
		m_EngineTransitionTimer = 0.0f;

		if (strcmp(&m_cEngineName[m_iCurrentEngine][0], "MKIII_Engine.X") == 0 )
		{
			m_fEngineHealth = MKIIIHEALTH;
			m_fEngineEnergy = MKIIIENERGY;
		}
		else if (strcmp(&m_cEngineName[m_iCurrentEngine][0], "MKIV_Engine.X") == 0 )
		{
			m_fEngineHealth = MKIVHEALTH;
			m_fEngineEnergy = MKIVENERGY;
		}
		else if (strcmp(&m_cEngineName[m_iCurrentEngine][0], "X95_Engine.X") == 0 )
		{
			m_fEngineHealth = X95HEALTH;
			m_fEngineEnergy = X95ENERGY; 
		}
		// Update stats display
		m_pHealthBar->SetPower((m_fPodHealth + m_fEngineHealth)/(MAX_POD_HEALTH + MAX_ENGINE_HEALTH));
		m_pEnergyBar->SetPower((m_fPodEnergy + m_fEngineEnergy)/(MAX_POD_ENERGY + MAX_ENGINE_ENERGY));
	}
}
