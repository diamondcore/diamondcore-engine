#include "GameplayScreen.h"
#include "../ScreenManager.h"
#include "../../DCEngineDemo/Controls/Camera/CameraManager.h"
#include "../Controls/SliderControl.h"
#include "../Controls/PanelControl.h"
#include "../Controls/TextField.h"
#include "../../DCEngineDemo/Havok/TriggerVolumes.h"
#include <dinput.h>
#include "LevelDesignerScreen.h"
#include "PauseScreen.h"
#include "../Controls/EnergyBarControl.h"
#include "../Controls/HealthControl.h"
#include "../../Game Objects/Turbine.h"
#include "../../Game Objects/Pod.h"
#include "../../Game Objects/Engine.h"
#include "../Controls/SpeedControl.h"
#include "DeathScreen.h"
#include <string>

GameplayScreen::GameplayScreen() : IGameScreen()
{
	mGamePaused = false;

	m_fRaceTimer = 0;

	/*TEMPPODDAMAGETIMER = 0;
	TEMPLENGINEDAMAGETIMER = 0;
	TEMPRENGINEDAMAGETIMER = 0;*/
	//TEMPENERGYTIMER = 0;


}

void GameplayScreen::Update(float dt)
{
	IGameScreen::Update(dt);

	if (mGamePaused)
		return;

	GPM->Update(dt);
	   //Process all Trigger Volumes
    int scratchInt = HM->m_vTriggerVolumes.size();

    for (int i = 0; i < scratchInt; i++)
		{
          int scratchInt2 = HM->m_vTriggerVolumes[i]->m_Colliders.size();
          for (int k = 0; k < scratchInt2; k++)
              {
				  TriggerVolumeCallback(HM->m_vTriggerVolumes[i]->m_type, HM->m_vTriggerVolumes[i]->getTriggerBody()->getUid(), HM->m_vTriggerVolumes[i]->m_Colliders[k], dt);
              }
		  // Check if collisions should be cleared
		  if(HM->m_vTriggerVolumes[i]->m_bShouldClear && HM->m_vTriggerVolumes[i]->m_Colliders.size() > 0)	
	         HM->m_vTriggerVolumes[i]->m_Colliders.clear();
		}


	// @Tim - Remove code until the next comment
	// and replace it with stuff that pulls state in from
	// the Turbine.
	//TEMPPODDAMAGETIMER += dt;
	//TEMPLENGINEDAMAGETIMER += dt * 2;
	//TEMPRENGINEDAMAGETIMER += dt * -0.5f;
	//TEMPENERGYTIMER +=  dt * 0.5f;
	
	// These all expect a float value from 0-1.0f
	auto pod = GPM->GetTurbine();
	if(pod)
	{
		m_hHealthCtrl->SetLeftEngineDamage(pod->GetLeftEngine()->GetHealth() / pod->GetLeftEngine()->GetMaxHealth());
		m_hHealthCtrl->SetRightEngineDamage(pod->GetRightEngine()->GetHealth() / pod->GetRightEngine()->GetMaxHealth());
		m_hHealthCtrl->SetPodDamage(pod->GetPod()->GetHealth() / pod->GetPod()->GetMaxHealth());
	
		m_eEnergyCtrl->SetPower(pod->GetCurrentEnergy() / pod->GetMaxEnergy());

		m_sSpeedControl->SetCurrentSpeed(pod->GetCurrentVelocitySpeed());//GPM->GetTurbine()->GetCurrentSpeed());
	}
	// @Tim - Remove this variable and replace it with the DT 
	// coming in from... World Manager?
	m_fRaceTimer+= dt;

	auto time = m_fRaceTimer;

	int numMinutes = ((int)time / 60);
	m_tRaceMinuteCtrl->SetText(std::to_string((_Longlong)numMinutes));
	int numSeconds = ((int)time % 60);
	m_tRaceSecondCtrl->SetText(std::to_string((_Longlong)numSeconds));
	time -= (int)time;

	int numMilSeconds = time * 100;

	m_tRaceMilSecondCtrl->SetText(std::to_string((_Longlong)numMilSeconds));
	m_pTimerPanel->LayoutRow(0.0f);	

	SStats->SetLastRaceTime(m_fRaceTimer);
	SStats->SetTotalTimePlayed(dt);
}

void GameplayScreen::Initialize()
{
	ScreenMan->GetEntityManager()->SetDisplayHiddenObject(false);	// Hide non-gameplay objects

	ScreenMan->GetEntityManager()->InitializeGameObjects(GPM->GetPlayerPos());

	auto screenSize = ScreenMan->GetScreenSize();

	AddControl(new PanelControl());

	m_eEnergyCtrl = new EnergyBarControl();
	AddControl(m_eEnergyCtrl);

	m_hHealthCtrl = new HealthControl("blankTexture.png", "blankTexture.png", "blankTexture.png");

	auto healthbarSize = m_hHealthCtrl->GetSize();
	auto energyBarPos = m_eEnergyCtrl->GetPosition();

	auto centeredHealthBar = Vector2((energyBarPos.x - healthbarSize.x) * 0.5f, energyBarPos.y);
	m_hHealthCtrl->SetPosition(centeredHealthBar);
	AddControl(m_hHealthCtrl);

	m_pTimerPanel = new PanelControl();
	m_tRaceMinuteCtrl = new TextControl("RaceTimer", "00");
	auto div1 = new TextControl("RaceTimer", " : ");
	m_tRaceSecondCtrl = new TextControl("RaceTimer", "00");
	auto div2 = new TextControl("RaceTimer", " : ");
	m_tRaceMilSecondCtrl = new TextControl("RaceTimer", "00");

	m_pTimerPanel->AddChild(m_tRaceMinuteCtrl);
	m_pTimerPanel->AddChild(div1);
	m_pTimerPanel->AddChild(m_tRaceSecondCtrl);
	m_pTimerPanel->AddChild(div2);
	m_pTimerPanel->AddChild(m_tRaceMilSecondCtrl);

	AddControl(m_pTimerPanel);
	
	m_pTimerPanel->LayoutRow(0.0f);

	m_pTimerPanel->SetSize((m_tRaceMinuteCtrl->GetSize() * 3) + (div2->GetSize() * 2));
	auto panelSize = m_pTimerPanel->GetSize(); 

	m_pTimerPanel->SetPosition((screenSize.x - panelSize.x) * 0.5f, 0.0f);

	m_sSpeedControl = new SpeedControl(50.0f, "SpeedControl");
	auto speedCtrlSize = m_sSpeedControl->GetSize();
	m_sSpeedControl->SetPosition(screenSize.x - speedCtrlSize.x - 15, energyBarPos.y);
	AddControl(m_sSpeedControl);

	// Set static Graphics settings for box and barrel
	auto GFX = ScreenMan->GetGraphics();
	GFX->SetDefaultMeshLightAttributes("barrel1.x", 0.25f, 60.0f, 50.0f);
	GFX->LoadTexture("tile010.jpg");
	GFX->SetDefaultMeshTexture("Box.x", "tile010.jpg");
	GFX->SetDefaultMeshLightAttributes("Box.x", 0.5f, 60.0f, 100.0f);

	mRootControl->MoveTowardsFront(50);
	GPM->SetScreen(this);
}

void GameplayScreen::ShutDown()
{
    GPM->Shutdown();
	IGameScreen::ShutDown();
}

IControl::HandleInputResult GameplayScreen::HandleInput(IInputCore* inputState, float dt)
{
	// Test going into pause screen
	if( !PauseScreen::AlreadyExists() &&
		(inputState->IsNewKeyRelease(DIK_ESCAPE) || 
		inputState->IsNewButtonRelease(START)   || 
		!ScreenMan->GetMainCore()->GetMemoryManager()->GetIsActiveWindow()) )
		ScreenMan->AddScreen( new PauseScreen(this) );

	if (mGamePaused || GPM->GetTurbineIsDead())
		return IGameScreen::HandleInput(inputState, dt);

	auto inputResult = IGameScreen::HandleInput(inputState, dt);

	// When UI consumes input, for the frame, there's no need to pass
	// it along to the game manager. Think of how clicking on a button
	// shouldn't make the character fire their weapon.
	if (inputResult == IControl::HandleInputResult::CONSUME)
		return IControl::HandleInputResult::CONSUME;

	// Otherwise, do normal game logic.
	
	//TODO: Implement
	GPM->HandleInput(inputState, dt);

	// experiment... gameplay screen should always consume
	// input?
	return IControl::HandleInputResult::CONSUME;
}

void GameplayScreen::Draw2D(IGraphicsCore* gDevice)
{
	// Do 2D Draw
	IGameScreen::Draw2D(gDevice);

	// It's possible that more logic may need to go here later?
}

void GameplayScreen::TriggerVolumeCallback(TriggerType eType, int triggerID, int iBodyID, float dt)
{
   switch (eType)
       {
       
       case _Race:
           {
           break;
           }
       case _Lap:
           {
           break;
           }
       case _Damage:
           {
			   // Apply damage if we are inside the trigger volume
			   auto turbine = GPM->GetTurbine();
			   if(!turbine)
				   break;
			   float damage = 15.0f;	// This can be tweaked
			   if(turbine->GetLeftEngine()->GetBody()->getUid() == iBodyID)
				   turbine->ApplyDamage(damage, 1, e_SelfDamage);
			   if(turbine->GetRightEngine()->GetBody()->getUid() == iBodyID)
				   turbine->ApplyDamage(damage, 2, e_SelfDamage);
			   if(turbine->GetPod()->GetBody()->getUid() == iBodyID)
				   turbine->ApplyDamage(damage, 0, e_SelfDamage);
           break;
           }
       case _Start:
           {
           break;
           }
       case _Finish:
           {
			   GPM->FinishRace();
			   m_fRaceTimer = 0.0f;

           break;
           }
       case _Force:
           {
			   if( !((ForceVolume*)(HM->m_TriggerVolumes[triggerID]))->m_bActive )
				   break;
			   // Make sure we don't apply force to the turbine
			   bool isTurbine = false;
			   /*auto turbine = GPM->GetTurbine();
			   if(turbine)
				   if(turbine->GetLeftEngine()->GetBodyID() == iBodyID ||
					   turbine->GetRightEngine()->GetBodyID() == iBodyID ||
					   turbine->GetPod()->GetBodyID() == iBodyID)
					   isTurbine = true;*/
					   
			   // Apply the force at the entity's center
			   //auto pos = HM->m_EntityList[iBodyID]->getPosition();	
			   auto pos = HM->m_TriggerVolumes[triggerID]->getTriggerBody()->getPosition();

			   // Apply force in the direction of the trigger volume, up is default
			   auto rot = HM->m_TriggerVolumes[triggerID]->getTriggerBody()->getRotation();
			   Quaternion Orientation;
			   Orientation.x = rot.getComponent<0>();
			   Orientation.y = rot.getComponent<1>();
			   Orientation.z = rot.getComponent<2>();
			   Orientation.w = rot.getComponent<3>();
			   Orientation.Normalize();

			   // Calculate the force Vector3 direction using the quaternion orientation
			   D3DXMATRIX O;
			   Vector3 dir;
			   D3DXMatrixRotationQuaternion(&O, &Orientation);
			   D3DXVec3TransformNormal(&dir, &Vector3(0.0f, 1.0f, 0.0f), &O);
			   dir.Normalize();

			   // Scale the direction by the force applied
			   if( isTurbine )
				   dir *= 1000.0f;	//HM->m_EntityList[iBodyID]->getMass() * 20.0f;
			   else
				   dir *= (1000.0f + HM->m_EntityList[iBodyID]->getMass() * (*(float*)(HM->m_TriggerVolumes[triggerID]->m_pParam)) ); //1000.0f;	//HM->m_EntityList[iBodyID]->getMass() * 20.0f;

			   // Apply force at the trigger volumes position
			   HM->ApplyForce(iBodyID, dt, dir.x, dir.y, dir.z, pos(0), pos(1), pos(2));
	           break;
           }
	   case _Explosion:
           {
			   auto entityList = HM->m_EntityList;
			   if(entityList.find(iBodyID) == entityList.end())	// Make sure entity is in the list
				   return;
			   // Get distance to the object
			   hkVector4 bodyPos = entityList[iBodyID]->getPosition();
			   hkVector4 triggerPos = HM->m_TriggerVolumes[triggerID]->getTriggerBody()->getPosition();
			   Vector3 bPos(bodyPos.getComponent<0>(), bodyPos.getComponent<1>(), bodyPos.getComponent<2>());
			   Vector3 tPos(triggerPos.getComponent<0>(), triggerPos.getComponent<1>(), triggerPos.getComponent<2>());
			   tPos = bPos - tPos;
			   float dist = tPos.Magnitude();
			   
			   // Apply force 
			   tPos.Normalize();
			   tPos *= HM->m_EntityList[iBodyID]->getMass() * 30.0f;
			   HM->ApplyForce(iBodyID, dt, tPos.x, tPos.y, tPos.z, bPos.x, bPos.y, bPos.z);

			   // Check if the collided object is part of the turbine
			   auto turbine = GPM->GetTurbine();
			   if(!turbine)
				   break;
			   if(turbine->GetPod()->GetBody()->getUid() == iBodyID)
			   {
				   // Apply damage to the pod
				   turbine->ApplyDamage(50.0f,1, e_SelfDamage);
				   return;
			   }
			   else if(turbine->GetLeftEngine()->GetBody()->getUid() == iBodyID)
			   {
				   // Apply damage to the left engine
				   turbine->ApplyDamage(50.0f,1, e_SelfDamage);
				   return;
			   }
			   else if(turbine->GetRightEngine()->GetBody()->getUid() == iBodyID)
			   {
				   // Apply damage to the right engine
				   turbine->ApplyDamage(50.0f,2, e_SelfDamage);
				   return;
			   }
			   else
			   {
				   // Check for collisions against other objects

			   }
			   
	           break;
           }
       case _Crush:
           {
			   // Apply damage if we are touching the spikes of the crusher
			   auto turbine = GPM->GetTurbine();
			   if(!turbine)
				   break;
			   float damage = 25.0f;	// This can be tweaked
			   if(turbine->GetLeftEngine()->GetBody()->getUid() == iBodyID)
				   turbine->ApplyDamage(damage, 1, e_CrushDamage);
			   if(turbine->GetRightEngine()->GetBody()->getUid() == iBodyID)
				   turbine->ApplyDamage(damage, 2, e_CrushDamage);
			   if(turbine->GetPod()->GetBody()->getUid() == iBodyID)
				   turbine->ApplyDamage(damage, 0, e_CrushDamage);
			  break;
           }
	   default:
		   break;
       }

}