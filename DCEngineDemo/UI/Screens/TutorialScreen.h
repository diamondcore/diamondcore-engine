#pragma once

#include "IGameScreen.h"

class GameplayScreen;

class TutorialScreen	: public IGameScreen
{
public:
	TutorialScreen();
	
	void Initialize();
	void Update(float dt);
	void ShutDown();
	void Draw2D(IGraphicsCore* gDevice);
	void Draw3D(IGraphicsCore* gDevice);
	
	IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt);

private:
	int		m_iCountDown;
	float	m_fTransDelay;
	bool	m_bLevelLoaded;
	bool	mNeedMessage;
	bool	mUseController;
	void	InitializeLevel();

	// GamePlay Attributes
	enum GamePlayTutorialStage
	{
		GP_INTRO,
		GP_FORWARD,
		GP_REVERSE,
		GP_CAMERA_CONTROLS,
		GP_CAMERA_VIEW,
		GP_BOOST,
		GP_HEAL,
		GP_FINISH
	} mGamePlayTutStage;
	float			mOrigHealth;
	IControl*		mGamePlayPanel;
	GameplayScreen*	mGameplayScreen;
	void	UpdateGamePlayTutorial(float dt);

	// LevelDesigner Controls
	IControl* mLevelDesignerPanel;
};