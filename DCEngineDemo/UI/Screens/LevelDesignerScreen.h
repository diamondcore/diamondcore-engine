#pragma once

#include "IGameScreen.h"
#include "../Controls/ImageButton.h"
#include "../../UI/Controls/PanelControl.h"
#include "../Controls/TransitionController.h"
#include "../MouseSynthesiser.h"
#include "../Controls/TextField.h"
#include <map>

class LevelDesignerScreen	: public IGameScreen
{
	// Brush States: 
	// Used to specify the action of the brush
	// when the left mouse button is clicked.
	enum BrushState {
		state_None,

		// Terrain Editor
		state_Morph_Up,
		state_Morph_Down,
		state_Smooth,
		state_Extrude,
		state_Paint_Tex_1,
		state_Paint_Tex_2,
		state_Paint_Tex_3,

		state_Start_Custom_Paint,

		// Custom texture painting
		state_Paint_Custom_Tex_1,	// Red
		state_Paint_Custom_Tex_2,	// Green
		state_Paint_Custom_Tex_3,	// Blue
		state_Paint_Custom_Tex_4,	// Black 

		state_Num_Terrain_Tools,	// This is used for determining a switch between a brush and is not an actuall state to be set to

		// Object Placement
		state_Select,
		state_Move,
		state_Rotate,
		state_Scale
	} m_eCurrentBrushState,
	  m_ePreviousBrushState;

	void SwitchBrushState(BrushState toState);

public:
	LevelDesignerScreen();

	void Initialize();
	void Update(float dt);
	void Draw2D(IGraphicsCore* gDevice);
	void ShutDown();

	void SelectLevel(std::string level);
	void UpdateLoadedTextures();

	void		SetLevelName(std::string name){m_cLevelName = name;}
	std::string GetLevelName() {return m_cLevelName;}
	
	void EnterScreen();
	void ExitScreen();

	Vector3 GetPrevCamPos();
	bool	MeetsSaveRequirements();

	void ButtonClicked(ImageButton* sender, int buttonInded);

	void IncrementObjectList(std::shared_ptr<BaseGameEntity> entity);

	IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt);

private:
	std::string m_cLevelName;

	float	m_fBrushRadius;
	Vector3	m_vPreviousCamPos;
	Vector3 m_vPreviousCamLook;
	Vector3 m_vPreviousCamUp;
	int		m_iCameraID;
	void UseBrush(IInputCore* inputState, float dt);

	// Terrain Texture Names (For local reference)
	std::string	m_TerrainTex[3];

	// Required object Counters
	int*	m_pStartPointCounter;
	int*	m_pFinishLineCounter;

	// Enable axis
	bool	m_bEnableAxisX;
	bool	m_bEnableAxisY;
	bool	m_bEnableAxisZ;

	// Selected Object
	std::string	m_cSelectedName;
	int			m_iSelectedID;

	// Level Selection Response
	int			m_iSelectionResponse;

	// Object Type List with referrence name
	std::map<std::string, std::shared_ptr<BaseGameEntity>>	m_ObjectList;
	std::map<std::string, PanelControl*>					m_CategoryMenu;
	std::map<std::string, std::vector<std::string>>			m_CategoryList;
	std::string												m_CurrentMenu;

	// Create UI Screens
	void CreateTerrainGUI();
	void CreateObjectControlGUI();
	void CreateBoundaryControlGUI();

	// Create Object Creation Menu
    void CreateTriggerVolumeGUI();
	void SetupAssets();
	void CreateTiledObjectList(std::string category);
	void PopulateCatagories();
	void AddToCategory(std::string category, std::string name, 
					std::shared_ptr<BaseGameEntity> object = 0);

	MouseSynthesiser		mMouse;
	
	TextField*				mTfPaintTex[3];
	ImageButton*			mCurrentSelection;
	PanelControl*			mTerrainControls;
	PanelControl*			mObjectControls;
	PanelControl*			mBoundaryControls;
	PanelControl*			mObjectLists;
	PanelControl*			mLevelDesignerMenu;
	TransitionController*	mLevelDesignerPanel;
};