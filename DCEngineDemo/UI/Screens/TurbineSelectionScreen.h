#pragma once

#include "IGameScreen.h"
#include "../Controls/ImageButton.h"
#include "../../UI/Controls/PanelControl.h"
#include "../MouseSynthesiser.h"

class EnergyBarControl;

class TurbineSelectionScreen	: public IGameScreen
{
public:
	TurbineSelectionScreen();
	
	void Initialize();
	void Update(float dt);
	void Draw2D(IGraphicsCore* gDevice);
	void ShutDown();

	IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt);

private:
	MouseSynthesiser	mMouse;
	int					m_iCameraID;
	int					m_iLightID;

	Vector3				m_vOnscreen_Engine;
	Vector3				m_vOnscreen_Pod;
	Vector3				m_vOffscreen_Pod;
	Vector3				m_vOffscreen_Engine;
	
	// Engine variables
	int					m_iEngineID[3];
	std::string			m_cEngineName[3];
	int					m_iCurrentEngine;
	int					m_iPrevEngine;

	// Pod Variables
	int					m_iPodID[3];
	std::string			m_cPodName[3];
	int					m_iCurrentPod;
	int					m_iPrevPod;

	void ChangeSelection(int podID, int engineID);
	void updateTransitions(float dt);

	// Turbine Stats Display
	float				m_fPodHealth;
	float				m_fPodEnergy;
	float				m_fEngineHealth;
	float				m_fEngineEnergy;
	EnergyBarControl*	m_pHealthBar;
	EnergyBarControl*	m_pEnergyBar;

	PanelControl*	mTurbineSelection;

	float				m_fPodTransitionTimer;
	float				m_EngineTransitionTimer;
	float				m_cfTransitionDuration;

	// Background images
	int					m_iBackground;
	int					m_iSmallLogo;

	////////////
	// SOUND
	///////////
	int m_iBGMSounds1,m_iBGMSounds2, m_iBGMSounds3;
};