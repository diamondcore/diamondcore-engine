#ifndef __STATSSCREEN_H
#define __STATSSCREEN_H

#include <string>

#include "IGameScreen.h"
#include "../../StatTracker.h"

class TextControl;
class PanelControl;


class StatsScreen : public IGameScreen
{
private:

	TextControl *m_tRankings[NUMRANKSLOTS];
	TextControl *m_tTotalTimeCtrl;
	TextControl *m_tTotalDamageCtrl;
	TextControl *m_tTotalDeathsCtrl;
	TextControl *m_tTotalRacesCtrl;
	TextControl *m_tAvgLifeCtrl;
	TextControl *m_tLastRaceCtrl;
	TextControl *m_tMeteorCtrl;
	TextControl *m_tTurretCtrl;
	TextControl *m_tGeyserCtrl;
	TextControl *m_tElectricCtrl;
	TextControl *m_tCrushCtrl;
	TextControl *m_tSuicideCtrl;
	TextControl *m_tRageQuitCtrl;
	TextControl *m_tNumRacesWonCtrl;
	TextControl *m_tLeastDamageTakenCtrl;
	TextControl *m_tCurrentDamageTakenCtrl;
	PanelControl *m_pStatsPanel;

	bool m_bAttemptLeaderBoardOnInit;
	int m_iHighlightedRankNum;

protected:

	void NullifyRanks();
	void NullifyStats();
	

public:
	StatsScreen();
	//~StatsScreen();

	void Initialize();
	void Update(float dt);
	void Draw2D(IGraphicsCore* gDevice);
	void ShutDown();

	void InitRanks();
	void InitStats();

	//Setters

	void UpdateTimePlayed();
	void UpdateDamageTaken();
	void UpdateLastRaceTime();
	
	void UpdateRankings();
	void UpdateMeteorCount();
	void UpdateTurretCount();
	void UpdateGeyserCount();
	void UpdateElectricCount();
	void UpdateCrushCount();
	void UpdateSuicideCount();
	void UpdateDeathCount();
	void UpdateRaceCount();
	void UpdateWinCount();
	void UpdateRageQuitCount();
	void UpdateAverageLifespan();
	void UpdateLeastDamageTaken();
	void UpdateCurrentDamageTaken();

	void UpdateStats();
	void UpdateAll();

	void SetRankColor(int rankNum, float r, float g, float b);

	void AttemptNewRank();

};

#endif