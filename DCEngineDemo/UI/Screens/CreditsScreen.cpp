#include "../ScreenManager.h"
#include "../../UI/Controls/PanelControl.h"
#include "../../UI/Controls/EnergyBarControl.h"
#include "GameplayMenu.h"
#include "MainMenuScreen.h"
#include "CreditsScreen.h"
#include <dinput.h>

CreditsScreen::CreditsScreen()
	:IGameScreen()
{
	mIsOpaque = true;
}

void CreditsScreen::Update(float dt)
{
	auto pos = mCreditsPanel->GetPosition();
	pos.y -= 60.0f * dt;

	if (abs(pos.y) >= mCreditsPanel->GetSize().y * 1.3f)
		pos.y = ScreenMan->GetScreenSize().y;

	mCreditsPanel->SetPosition(pos);

	IGameScreen::Update(dt);
}

IControl::HandleInputResult CreditsScreen::HandleInput(IInputCore* inputState, float dt)
{
	if (inputState->IsNewButtonRelease(X) ||  
		inputState->IsNewButtonRelease(Y) ||
		inputState->IsNewButtonRelease(A) ||
		inputState->IsNewButtonRelease(B) ||
		inputState->IsNewKeyRelease(DIK_SPACE)
		)
	{
		ScreenMan->RemoveScreen(this);
		return IControl::CONSUME;
	}

	return IGameScreen::HandleInput(inputState, dt);
}

void CreditsScreen::Initialize()
{
	auto screenSize = ScreenMan->GetScreenSize();
	AddControl(new PanelControl());

	auto backBtn = new ImageButton("UI/generic.png", "Back", "TimesNewRoman16");
	backBtn->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 	
		ScreenMan->RemoveScreen(this);
	};
	auto btnSize = backBtn->GetSize();
	backBtn->SetPosition(Vector2(screenSize.x - btnSize.x, screenSize.y - btnSize.y));
	AddControl(backBtn);

	auto mainCtrl = new PanelControl();
	mainCtrl->SetSize(screenSize);
	AddControl(mainCtrl);

	auto titleFont = "RaceTimer";
	auto nameFont = "TimesNewRoman16";
	auto nameSpacing = 10.0f;

	mainCtrl->AddChild(new TextControl(titleFont, "TURBINE"));

	auto gameplayPanel = new PanelControl();
	gameplayPanel->AddChild(new TextControl(titleFont, "Gameplay Programmers"));
	gameplayPanel->AddChild(new TextControl(nameFont, "Tim Kennedy (Lead)"));
	gameplayPanel->AddChild(new TextControl(nameFont, "Andrew Batista"));
	gameplayPanel->AddChild(new TextControl(nameFont, "Ray Batts"));
	gameplayPanel->AddChild(new TextControl(nameFont, "Jonathan James"));
	gameplayPanel->AddChild(new TextControl(nameFont, "Westley Wood"));

	gameplayPanel->LayoutColumn(nameSpacing);
	gameplayPanel->CenterHorizontal();
	mainCtrl->AddChild(gameplayPanel);

	auto enginePanel = new PanelControl();
	enginePanel->AddChild(new TextControl(titleFont, "Engine Programmers"));
	enginePanel->AddChild(new TextControl(nameFont, "Ray Batts (Lead)"));
	enginePanel->AddChild(new TextControl(nameFont, "Tim Kennedy"));
	
	enginePanel->LayoutColumn(nameSpacing);
	enginePanel->CenterHorizontal();
	mainCtrl->AddChild(enginePanel);

	auto graphicsPanel = new PanelControl();
	graphicsPanel->AddChild(new TextControl(titleFont, "Graphics Programmers"));
	graphicsPanel->AddChild(new TextControl(nameFont, "Westley Wood (Lead)"));
	graphicsPanel->AddChild(new TextControl(nameFont, "Andrew Batista"));
	graphicsPanel->AddChild(new TextControl(nameFont, "Jonathan James"));

	graphicsPanel->LayoutColumn(nameSpacing);
	graphicsPanel->CenterHorizontal();
	mainCtrl->AddChild(graphicsPanel);	
	
	auto HavokPanel = new PanelControl();
	HavokPanel->AddChild(new TextControl(titleFont, "Havok Physics"));
	HavokPanel->AddChild(new TextControl(nameFont, "Jonathan James (Lead)"));
	HavokPanel->AddChild(new TextControl(nameFont, "Westley Wood"));

	HavokPanel->LayoutColumn(nameSpacing);
	HavokPanel->CenterHorizontal();
	mainCtrl->AddChild(HavokPanel);	

	auto soundPanel = new PanelControl();
	soundPanel->AddChild(new TextControl(titleFont, "Sound"));
	soundPanel->AddChild(new TextControl(nameFont, "Ray Batts (Lead)"));
	soundPanel->AddChild(new TextControl(nameFont, "Tim Kennedy"));

	soundPanel->LayoutColumn(nameSpacing);
	soundPanel->CenterHorizontal();
	mainCtrl->AddChild(soundPanel);		
	
	auto editorPanel = new PanelControl();
	editorPanel->AddChild(new TextControl(titleFont, "Level Editor"));
	editorPanel->AddChild(new TextControl(nameFont, "Westley Wood"));

	editorPanel->LayoutColumn(nameSpacing);
	editorPanel->CenterHorizontal();
	mainCtrl->AddChild(editorPanel);
	
	auto uiPanel = new PanelControl();
	uiPanel->AddChild(new TextControl(titleFont, "UI Programmer"));
	uiPanel->AddChild(new TextControl(nameFont, "Ray Batts"));

	uiPanel->LayoutColumn(nameSpacing);
	uiPanel->CenterHorizontal();
	mainCtrl->AddChild(uiPanel);


	auto artPanel = new PanelControl();
	artPanel->AddChild(new TextControl(titleFont, "Artists"));
	artPanel->AddChild(new TextControl(nameFont, "Justin Monaco"));
	artPanel->AddChild(new TextControl(nameFont, "Antonio Smith"));

	artPanel->LayoutColumn(nameSpacing);
	artPanel->CenterHorizontal();
	mainCtrl->AddChild(artPanel);
	
	auto advisorPanel = new PanelControl();
	advisorPanel->AddChild(new TextControl(titleFont, "Faculty Advisor"));
	advisorPanel->AddChild(new TextControl(nameFont, "Ed Magnin"));

	advisorPanel->LayoutColumn(nameSpacing);
	advisorPanel->CenterHorizontal();
	mainCtrl->AddChild(advisorPanel);

	auto deanPanel = new PanelControl();
	deanPanel->AddChild(new TextControl(titleFont, "DeVry GSP Dean"));
	deanPanel->AddChild(new TextControl(nameFont, "Bill McClure"));

	deanPanel->LayoutColumn(nameSpacing);
	deanPanel->CenterHorizontal();
	mainCtrl->AddChild(deanPanel);

	auto facultyPanel = new PanelControl();
	facultyPanel->AddChild(new TextControl(titleFont, "GSP Professors"));
	facultyPanel->AddChild(new TextControl(nameFont, "Corey Clark"));
	facultyPanel->AddChild(new TextControl(nameFont, "Ed Magnin"));
	facultyPanel->AddChild(new TextControl(nameFont, "Derek Manns"));
	facultyPanel->AddChild(new TextControl(nameFont, "Charles Nicholson"));

	facultyPanel->LayoutColumn(nameSpacing);
	facultyPanel->CenterHorizontal();
	mainCtrl->AddChild(facultyPanel);

	auto specialThanks = new PanelControl();
	specialThanks->AddChild(new TextControl(titleFont, "Special Thanks"));
	specialThanks->AddChild(new TextControl(nameFont, "Tim: To Melanie, Liam and Lilliana - We were only a family of two with a wish"));
	specialThanks->AddChild(new TextControl(nameFont, "when this whole journey started, but now we are a family of four"));
	specialThanks->AddChild(new TextControl(nameFont, "and off to new adventures with dreams in hand. Thank you for all your"));
	specialThanks->AddChild(new TextControl(nameFont, "love and support! I could not have done this without you all."));
	specialThanks->LayoutColumn(nameSpacing);
	specialThanks->CenterHorizontal();
	mainCtrl->AddChild(specialThanks);

	mainCtrl->LayoutColumn(75);
	mainCtrl->CenterHorizontal();

	mainCtrl->SetPosition(mainCtrl->GetPosition().x, screenSize.y);

	mCreditsPanel = mainCtrl;
}



