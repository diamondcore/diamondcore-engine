#include "MainMenuScreen.h"
#include "../Controls/ImageButton.h"
#include "../ScreenManager.h"
#include "GameplayScreen.h"
#include "LevelDesignerScreen.h"
#include "StatsScreen.h"
#include "GameplayMenu.h"
#include "TutorialScreen.h"
#include "TurbineSelectionScreen.h"
#include "../../FSM/LoadGameState.h"
#include "../../UI/Controls/SliderControl.h"
#include "../../UI/Controls/PanelControl.h"
#include "../../UI/Controls/TextField.h"
#include "../../UI/Controls/EnergyBarControl.h"
#include "../../DCEngineDemo/Controls/Camera/CameraManager.h"
#include "CreditsScreen.h"

#define SND ScreenMan->GetMainCore()->GetSoundCore()

MainMenuScreen::MainMenuScreen() : IGameScreen()
{
	// This an unused variable for now.
	mTransitionMs = 2.5f;
	m_iCameraID	  = -1;

	m_iMainMenuLoopID = SND->CreateInstance2D("Content/Sounds/Music/ConflictLoop.wav",true,1,true);
}

void MainMenuScreen::Update(float dt)
{
	if (mRootControl)
		mRootControl->Update(dt);

	static bool loaded = false;
	static int update = 0;
	if(update < 2)
		update++;
	else if(!loaded)
	{
		loaded = LOADGAME->LoadGFX(0);
		mEnergyCtrl->SetPower(LOADGAME->GetLoadProgress());
	}
	else if(!mMainMenuPanel->IsVisible())
	{
		mMainMenuPanel->SetVisible(true);
		mEnergyCtrl->SetVisible(false);
	}
}

void MainMenuScreen::Initialize()
{
	SND->PlaySFX(m_iMainMenuLoopID,true,1);

	auto screenSize = ScreenMan->GetGraphics()->GetScreenSize();
	auto pc = new PanelControl();
	pc->SetSize(screenSize);
	AddControl(pc);

	mMainMenuPanel = new PanelControl();
	AddControl(mMainMenuPanel);

	// Add the energy control
	mEnergyCtrl = new EnergyBarControl("Loading...");
	AddControl(mEnergyCtrl);
	mEnergyCtrl->SetColor(Vector3(0.5f, 0.5f, 0.5f), Vector3(0.5f, 0.5f,0.5f));//Vector3(1.0f, 1.0f, 1.0f));
	mEnergyCtrl->SetPower(0.025f);

	// Create Logo 
	auto logoCtrl = new ImageControl("LogoFinal.png");
	mMainMenuPanel->AddChild(logoCtrl);

	// Scale and Position the Logo
	auto xSize = screenSize.x * 0.75f;
	auto finalSize = Vector2(xSize, 181 * (xSize/ 1046));
	logoCtrl->SetSize(finalSize);

	// Setup Camera for Main Menu
	m_iCameraID = CamMgr->CreateCamera(FREECAM);
	CamMgr->SetActiveCamControl(m_iCameraID);
	CamMgr->SetPosition(0.0f, 10.0f, -30.0f);
	CamMgr->m_pCurrentCamControl->SetLookAt(Vector3(0.0f, 10.0f, -29.0f), Vector3(0.0f, 1.0f, 0.0f));
	
	auto testBtn = new ImageButton("UI/generic.png", "Game Play", "TimesNewRoman16");
	testBtn->SetSize(111*1.5f, 57);
	// Using a lambda function in order to 
	// set our callback delegate. I'll soon work on
	// allowing a function pointer to be used as well.
	testBtn->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 	
		ScreenMan->ExitScreens(new TurbineSelectionScreen());
	};
	mMainMenuPanel->AddChild(testBtn);
	
	// Level Designer button
	auto Btn_LevelDesigner = new ImageButton("UI/generic.png", "Level Designer", "TimesNewRoman16");
	Btn_LevelDesigner->SetSize(111*1.5f, 57);
	Btn_LevelDesigner->OnClicked = [=] (ImageButton* sender, void* userData)
	{
		ScreenMan->ExitScreens(new LevelDesignerScreen());
	};
	mMainMenuPanel->AddChild(Btn_LevelDesigner);
	
	// Tutorial button
	auto Btn_Tutorial = new ImageButton("UI/generic.png", "Tutorial", "TimesNewRoman16");
	Btn_Tutorial->SetSize(111*1.5f, 57);
	Btn_Tutorial->OnClicked = [=] (ImageButton* sender, void* userData)
	{
		ScreenMan->ExitScreens(new TutorialScreen());
	};
	mMainMenuPanel->AddChild(Btn_Tutorial);

	// Credits button
	auto creditsBtn = new ImageButton("UI/generic.png", "Credits", "TimesNewRoman16");
	creditsBtn->SetSize(111*1.5f, 57);
	creditsBtn->OnClicked = [=] (ImageButton* sender, void* userData)
	{
		ScreenMan->AddScreen(new CreditsScreen());
	};
	mMainMenuPanel->AddChild(creditsBtn);
	mMainMenuPanel->LayoutColumn(25.0f);
	
	Vector2 menuSize = mMainMenuPanel->GetSize();
	mMainMenuPanel->SetPosition((screenSize - menuSize) * 0.5f);
	mMainMenuPanel->CenterHorizontal();
	mMainMenuPanel->SetVisible(false);

	// Save button positions
	mMouse.AddButton(testBtn);	
	mMouse.AddButton(Btn_LevelDesigner);
	mMouse.AddButton(creditsBtn);
	mMouse.Initialize();

	pc->CenterHorizontal();
}

void MainMenuScreen::Draw2D(IGraphicsCore* gDevice)
{
	// Do 2D Draw
	IGameScreen::Draw2D(gDevice);
}

void MainMenuScreen::ShutDown()
{
	SND->StopSFX(m_iMainMenuLoopID);
	CamMgr->DeleteCameraControl(m_iCameraID);
	m_iCameraID = -1;
	mMouse.ShutDown();
	IGameScreen::ShutDown();
}

IControl::HandleInputResult MainMenuScreen::HandleInput(IInputCore* inputState, float dt)
{
	mMouse.HandleInput(inputState, dt);
	return IGameScreen::HandleInput(inputState, dt);
}