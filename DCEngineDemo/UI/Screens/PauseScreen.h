#pragma once

#include "IGameScreen.h"
#include "../Controls/ImageButton.h"
#include "../../UI/Controls/PanelControl.h"
#include "../Controls/TransitionController.h"
#include "../MouseSynthesiser.h"

class PauseScreen	: public IGameScreen
{
	IGameScreen*	m_pParent;
	static bool		s_bAlreadyExists;

public:
	PauseScreen(IGameScreen* pParent, bool useEditorTools = false);

	void Initialize();
	void Update(float dt);
	void Draw2D(IGraphicsCore* gDevice);
	void ShutDown();
	
	void ButtonClicked(ImageButton* sender, int buttonInded);
	static bool AlreadyExists() { return s_bAlreadyExists; }

	IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt);

private:
	bool	m_bUseEditTools;
	bool	m_bExit;

	MouseSynthesiser		mMouse;

	void CreateTerrainOptionsMenu();

	PanelControl*			mOptionsMenu;
	TransitionController*	mOptionsPanel;
};