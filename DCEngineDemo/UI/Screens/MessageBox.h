#pragma once

#include "IGameScreen.h"

class ImageButton;

class MessageBoxScreen : public IGameScreen
{
private:

	enum MBType
	{
		OK,
		YESNOCANCEL
	};

	const char* mHeaderText;
	const char* mMessageText;
	const char* mFontName;
	MBType mType;
	function<void (ImageButton* sender, void* userData)> mOnAccept;

	MessageBoxScreen(Vector2 size, const char* header, const char* message, const char* mFont, function<void (ImageButton* sender, void* userData)> mOnAccept = nullptr);

public:
	static void Show(const char* header, const char* message, const char* mFont = "TimesNewRoman16");
	static void ShowYesNo(const char* header, const char* message, function<void (ImageButton* sender, void* userData)> onAccept, const char* mFont = "TimesNewRoman16");

	void Initialize();
};