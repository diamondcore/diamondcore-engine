#pragma once

#include "IGameScreen.h"
#include "../Controls/ImageButton.h"
#include "../../UI/Controls/PanelControl.h"
#include "../Controls/TransitionController.h"
#include "../MouseSynthesiser.h"

class EndGameMenuScreen	: public IGameScreen
{
	IGameScreen*	m_pParent;

public:
	EndGameMenuScreen(IGameScreen* pParent);

	void Initialize();
	void Update(float dt);
	void Draw2D(IGraphicsCore* gDevice);
	void ShutDown();
	
	void ButtonClicked(ImageButton* sender, int buttonInded);

	IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt);

private:
	
	MouseSynthesiser		mMouse;

	void CreateDeathOptionsMenu();

	PanelControl*			mOptionsMenu;
	TransitionController*	mOptionsPanel;
};