#pragma once

#include "IGameScreen.h"

class CreditsScreen	: public IGameScreen
{
public:
	CreditsScreen();
	
	void Initialize();

	void Update(float dt);
	
	IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt);


private:

	IControl* mCreditsPanel;
	void CenterElements(PanelControl* panel);
};