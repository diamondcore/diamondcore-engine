#pragma once

#include "IGameScreen.h"
#include "../Controls/ImageButton.h"
#include "../../UI/Controls/PanelControl.h"
#include "../MouseSynthesiser.h"

class GameplayMenu	: public IGameScreen
{
public:
	GameplayMenu();
	
	void Initialize();
	void Update(float dt);
	void Draw2D(IGraphicsCore* gDevice);
	void ShutDown();

	IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt);

private:
	std::vector<std::string>	m_LevelList;
	int							m_iCountDown;
	int							m_iCurrentPage;
	
	MouseSynthesiser			mMouse;

	void CreateLevelSelection();

	std::vector<PanelControl*>	mLevelSelection;
};