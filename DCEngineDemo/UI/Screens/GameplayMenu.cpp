#include "GameplayMenu.h"
#include "../ScreenManager.h"
#include "../../DCEngineDemo/Controls/Camera/CameraManager.h"
#include "../../UI/Controls/SliderControl.h"
#include "../../UI/Controls/PanelControl.h"
#include "../../UI/Screens/LevelDesignerScreen.h"
#include "GameplayScreen.h"
#include "PauseScreen.h"
#include "../../WorldManager.h"
#include <dinput.h>
#include <fstream>


GameplayMenu::GameplayMenu()
	:IGameScreen()
{
	m_iCountDown	= -1;
	m_iCurrentPage	= 0;

}

void GameplayMenu::Update(float dt)
{
	if (mRootControl)
		mRootControl->Update(dt);

	if(m_iCountDown == 0)
	{
		// Begin Gameplay Screen
		if(!ScreenMan->GetEntityManager()->DisplayHiddenObjects())
			ScreenMan->AddScreen(new GameplayScreen());
		ScreenMan->RemoveScreen(this);
	}
	else if(m_iCountDown > 0)
	{
		if(m_iCountDown == 3)	// Load the physics terrain
		{	
			//HM->Init();
			//
			////Demo Getting verts
			//Vector3* vertList = new Vector3[ScreenMan->GetGraphics()->GetNumVerts()];
			//ScreenMan->GetGraphics()->GetTerrainVerts(vertList);

			//// Demo getting indices
			//WORD* indexList = new WORD[ScreenMan->GetGraphics()->GetNumIndices()];
			//ScreenMan->GetGraphics()->GetTerrainIndices(indexList);
	
			//Vector3 size;
			//ScreenMan->GetGraphics()->GetTerrainSize(size);
			//int rows, cols;
			//ScreenMan->GetGraphics()->GetTerrainNumRowsCols(rows, cols);

			//GPM->InitHavokTerrain(vertList,indexList,rows, cols, size.z, size.x);

			//delete [] vertList;	// Clean up demo
			//delete [] indexList;	// Clean up demo
		}
		else if(m_iCountDown == 1 && !ScreenMan->GetEntityManager()->DisplayHiddenObjects())
		{
			// Initialize Gameplay
			auto mainCore = ScreenMan->GetMainCore();
			GPM->Init(mainCore);
		
		}
		if(m_iCountDown <= 3)
		{
			//HM->Update(dt);
		}
		m_iCountDown--;	// Count down until Gameplay Screen Starts
	}
}

void GameplayMenu::Initialize()
{
	// Create the UI
	CreateLevelSelection();

	mMouse.Initialize();
	mMouse.SetupTable(4);
}

void GameplayMenu::ShutDown()
{
	m_LevelList.clear();
	mLevelSelection.clear();
	mMouse.ShutDown();
	IGameScreen::ShutDown();

	/////////////////
	// Stop sounds
	/////////////////
	ScreenMan->GetMainCore()->GetSoundCore()->StopAll(1);
}

IControl::HandleInputResult GameplayMenu::HandleInput(IInputCore* inputState, float dt)
{
	mMouse.HandleInput(inputState, dt);

	auto inputResult = IGameScreen::HandleInput(inputState, dt);

	// When UI consumes input, for the frame, there's no need to pass
	// it along to the game manager. Think of how clicking on a button
	// shouldn't make the character fire their weapon.
	if (inputResult == IControl::HandleInputResult::CONSUME)
		return IControl::HandleInputResult::CONSUME;

	// Otherwise, do normal game logic.	
	// Test going into pause screen
	if( !PauseScreen::AlreadyExists() &&
		(inputState->IsNewKeyRelease(DIK_ESCAPE) ||
		 inputState->IsNewButtonRelease(START)) )
		ScreenMan->AddScreen( new PauseScreen(this) );


	// experiment... gameplay screen should always consume
	// input?
	return IControl::HandleInputResult::CONSUME;
}

void GameplayMenu::Draw2D(IGraphicsCore* gDevice)
{
	// Do 2D Draw
	IGameScreen::Draw2D(gDevice);
}

void GameplayMenu::CreateLevelSelection()
{
	auto GFX = ScreenMan->GetGraphics();
	auto screenSize = GFX->GetScreenSize();
	
	// Add level selection Controls Panel
	auto pc = new PanelControl();
	AddControl(pc);

	// Set the Title of the screen
	auto title = new TextControl("Impact48", "Level Selection");
	pc->AddChild(title);
	Vector2 titlePos = title->GetSize() * -1.0f;
	titlePos.x += (screenSize.x - 111.0f) * 0.5f;
	titlePos.y += screenSize.y * 0.25f;
	title->SetPosition( titlePos );
	
	// Setup the menu navigation controls
	auto pc2 = new PanelControl();
	pc->AddChild(pc2);

	auto Btn_Next = new ImageButton("UI/generic.png", "Next", "TimesNewRoman16");
	auto Btn_Prev = new ImageButton("UI/generic.png", "Prev", "TimesNewRoman16");
	// Add Previous page button
	Btn_Prev->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		if(m_iCurrentPage - 1 >= 0)
		{
			// Switch to the previous page
			mLevelSelection[m_iCurrentPage]->SetVisible(false);
			m_iCurrentPage--;
			mLevelSelection[m_iCurrentPage]->SetVisible(true);
			// Enable the next button if it's disabled
			if(!Btn_Next->IsVisible())
				Btn_Next->SetVisible(true);
		}
		if(m_iCurrentPage - 1 < 0)
			Btn_Prev->SetVisible(false);
	};
	pc2->AddChild(Btn_Prev);

	// Add Next page button to previous page
	Btn_Next->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		if(m_iCurrentPage + 1 < mLevelSelection.size())
		{
			// Switch to the next page
			mLevelSelection[m_iCurrentPage]->SetVisible(false);
			m_iCurrentPage++;
			mLevelSelection[m_iCurrentPage]->SetVisible(true);
			// Enable the previous button if it's disabled
			if(!Btn_Prev->IsVisible())
				Btn_Prev->SetVisible(true);
		}
		if(m_iCurrentPage + 1 >= mLevelSelection.size())
			Btn_Next->SetVisible(false);
	};
	pc2->AddChild(Btn_Next);
	
	Btn_Prev->SetVisible(false);	// The first page doesn't have a previous page
	pc2->LayoutColumn(50.0f);
	

	// Get a list of all the levels
	std::string file_path = ScreenMan->GetEntityManager()->GetLevelsDirectory();
	file_path += "LevelList.list";
	std::ifstream file( file_path.c_str() );
	if(!file.is_open())
		return;		// Return if file doesn't exist
	
	// Read the file
	std::string temp;
	while(!file.eof())
	{
		getline(file, temp);
		if(!temp.empty())
			m_LevelList.push_back(temp);
		temp.clear();
	}

	// Figure out how many buttons can be on the screen
	Vector2 space(222.0f, 57.0f);
	Vector2 pSize = screenSize * 0.9f;
	space.x = pSize.x / space.x;
	space.y = pSize.y / space.y;

	// Load tiles for choices of levels
	int count = 0;
	int max = space.x + space.y;
	int page = -1;
	for(auto i = m_LevelList.begin(); 
		i != m_LevelList.end(); i++)
	{
		// Check if we need to create a new page
		if(count % max == 0)
		{
			// Create a new page
			mLevelSelection.push_back( new PanelControl() );
			page++;
			mLevelSelection[page]->SetSize(pSize);	//screenSize*0.75f);
			AddControl(mLevelSelection[page]);
		}

		// Add button for each level
		auto Btn_Tile = new ImageButton("UI/generic.png", (*i).c_str(), "TimesNewRoman16");
		Btn_Tile->SetSize(222.0f, 57.0f);
		Btn_Tile->OnClicked = [=] (ImageButton* sender, void* userData)
		{ 
			if(m_iCountDown < 0)
			{
				// Set the selected level
				if(ScreenMan->GetEntityManager()->DisplayHiddenObjects())
					((LevelDesignerScreen*)(ScreenMan->GetCurrentScreen()))->SelectLevel( &(*i)[0] );
				else	// Load the level
					ScreenMan->GetEntityManager()->LoadLevel( &(*i)[0] );

				m_iCountDown = 4;	// Count down 4 Updates until starting the level
			}
		};
		mLevelSelection[page]->AddChild(Btn_Tile);
		mMouse.AddButton(Btn_Tile);
		count++;
	}

	// If we only have one page then hide the next button
	if(page <= 0)
		Btn_Next->SetVisible(false);
	if(page < 0)
		return;
	
	// Layout panels in the center of the screen
	Vector2 pos;
	pos = (screenSize - pSize) * 0.5f;
	pos += Vector2(111.0f, 160.0f);		// Adjustments to center the panels
	for(auto p = mLevelSelection.begin();
		p != mLevelSelection.end(); p++)
	{
		(*p)->LayoutTileArray(Vector3(10, 10, 0), Vector3(222, 57, 0));
		(*p)->SetPosition( pos );
		(*p)->SetVisible(false);
	}
	
	if(page > 0)
	{
		mMouse.AddButton(Btn_Prev);
		mMouse.AddButton(Btn_Next);
	}

	// Unhide the first page
	mLevelSelection[0]->SetVisible(true);
}
