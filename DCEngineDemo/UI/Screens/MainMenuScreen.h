#pragma once

#include "IGameScreen.h"
#include "../Controls/ImageButton.h"
#include "../Controls/TransitionController.h"
#include "../MouseSynthesiser.h"

class PanelControl;
class EnergyBarControl;

class MainMenuScreen : public IGameScreen
{
public:

	MainMenuScreen();
	
	void Update(float dt);

	void Initialize();

	void ShutDown();

	void ButtonClicked(ImageButton* sender, int buttonInded);
	void Draw2D(IGraphicsCore* gDevice);

	IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt);

	// sound id
	int m_iMainMenuLoopID;

private:

	float mTransitionMs;

	int					m_iCameraID;
	MouseSynthesiser	mMouse;
	
	EnergyBarControl*	  mEnergyCtrl;
	PanelControl*         mMainMenuPanel;
	TransitionController* mOptionsPanel;
	TransitionController* mCreditsPanel;

	TransitionController* mMainMenuTitle;
	TransitionController* mOptionsTitle;
	TransitionController* mCreditsTitle;
	TransitionController* mExtrasTitle;

	TransitionController* mOptionsBackCtrl;
	TransitionController* mCreditsBackBtn;
	
};