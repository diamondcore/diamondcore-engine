#include "PauseScreen.h"
#include "../ScreenManager.h"
#include "../../WorldManager.h"
#include "../../DCEngineDemo/Controls/Camera/CameraManager.h"
#include "../../UI/Controls/SliderControl.h"
#include "../../UI/Controls/TextField.h"
#include "MainMenuScreen.h"
#include "TogglePhysics.h"
#include "MessageBox.h"
#include "LevelDesignerScreen.h"
#include <dinput.h>
bool PauseScreen::s_bAlreadyExists = false;

PauseScreen::PauseScreen(IGameScreen* pParent, bool useEditorTools)
	:IGameScreen(),
	m_pParent(pParent)
{
	s_bAlreadyExists	= true;
	m_bUseEditTools		= useEditorTools;
	mOptionsPanel		= 0;
	mOptionsMenu		= 0;
}

void PauseScreen::Update(float dt)
{
	if(mRootControl)
		mRootControl->Update(dt);
			
	if(m_bExit)
	{
		// Clear the world and remove the parent screen
		ScreenMan->GetGraphics()->SetBrush(0);
		ScreenMan->GetGraphics()->RemoveTerrain();
		ScreenMan->GetGraphics()->ClearScene();
		ScreenMan->GetEntityManager()->ClearEntityList();

		// Add MainMenu Screen and remove this screen
		ScreenMan->ExitScreens(new MainMenuScreen());
	}
}

void PauseScreen::Initialize()
{
	// Create Pause Screen for terrain
	//mOptionsPanel = new TransitionController(SLIDEUP, 1.0f, true);
	CreateTerrainOptionsMenu();
	//AddControl(mOptionsPanel);
	//mOptionsPanel->DoTransition();
}

IControl::HandleInputResult PauseScreen::HandleInput(IInputCore* inputState, float dt)
{
	mMouse.HandleInput(inputState, dt);

	auto inputResult = IGameScreen::HandleInput(inputState, dt);

	// Consume input for button
	if(inputResult == IControl::HandleInputResult::CONSUME)
		return IControl::HandleInputResult::CONSUME;
	
	/////////////////////////////////////////////////////////
	// Toggle pause menu
	/////////////////////////////////////////////////////////
	if(inputState->IsNewKeyRelease(DIK_ESCAPE) ||
		inputState->IsNewButtonRelease(START))
		ScreenMan->RemoveScreen(this);
	
	return IControl::HandleInputResult::CONSUME;
}

void PauseScreen::Draw2D(IGraphicsCore* gDevice)
{
	// Draw 2D UI
	IGameScreen::Draw2D(gDevice);
}

void PauseScreen::ShutDown()
{
	mMouse.ShutDown();
	IGameScreen::ShutDown();
	s_bAlreadyExists	= false;
}

void PauseScreen::CreateTerrainOptionsMenu()
{	
	auto GFX = ScreenMan->GetGraphics();
	
	// Add Options Menu
	mOptionsMenu = new PanelControl();
	mOptionsMenu->SetSize(400, 400);
	AddControl(mOptionsMenu);
	//mOptionsPanel->AddChild(mOptionsMenu);
	mOptionsMenu->MoveTowardsFront(3);	// Move this up 3 layer

	// Add Options Menu Buttons
	// Add button to resume game
	auto Btn_ResumeGame = new ImageButton("UI/generic.png", "Resume", "TimesNewRoman16");
	Btn_ResumeGame->SetSize(138.75f, 71.25f);
	Btn_ResumeGame->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		ScreenMan->RemoveScreen(this);
	};
	mOptionsMenu->AddChild(Btn_ResumeGame);
	mMouse.AddButton(Btn_ResumeGame);

	// Exit function
	m_bExit = false;

	// Add button to return to the MainMenu
	auto Btn_MainMenu = new ImageButton("UI/generic.png", "Main Menu", "TimesNewRoman16");
	Btn_MainMenu->SetSize(138.75f, 71.25f);
	Btn_MainMenu->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		if(m_bUseEditTools)
		{
			std::string msg = "Are you sure you want to leave the game?";
			MessageBoxScreen::ShowYesNo("Alert", msg.c_str(), [=] (ImageButton* sender, void* userData)
			{
				m_bExit = true;
			});
		}
		else
			m_bExit = true;
	};
	mOptionsMenu->AddChild(Btn_MainMenu);
	mMouse.AddButton(Btn_MainMenu);

	// Add Level Designer buttons
	if(m_bUseEditTools)
	{
		// Add button to jump into the game with physics
		auto Btn_TestLevel = new ImageButton("UI/generic.png", "Test Level", "TimesNewRoman16");
		Btn_TestLevel->SetSize(138.75f, 71.25f);
		Btn_TestLevel->OnClicked = [=] (ImageButton* sender, void* userData)
		{ 
			ScreenMan->AddScreen( new TogglePhysics(m_pParent) );
			ScreenMan->RemoveScreen(this);
		};
		mOptionsMenu->AddChild(Btn_TestLevel);
		mMouse.AddButton(Btn_TestLevel);

		// Save Level Button
		auto Btn_SaveLevel = new ImageButton("UI/generic.png", "Save Level", "TimesNewRoman16");
		Btn_SaveLevel->SetSize(138.75f, 71.25f);
		Btn_SaveLevel->OnClicked = [=] (ImageButton* sender, void* userData)
		{ 
			// Check if we are ready to save
			if( ((LevelDesignerScreen*)(m_pParent))->MeetsSaveRequirements() )
			{
				static bool clicked = false;
				static TextField* tf = 0;
				static ImageButton* Btn_TextBox = 0;
				// Pre declare the text field so that the textbox can delete it
				if(!clicked)
				{
					// Get the current level name stored in the level designer
					tf = new TextField(Vector2(500, 20), ((LevelDesignerScreen*)(m_pParent))->GetLevelName());
					Btn_TextBox = new ImageButton("UI/generic.png");
					tf->MoveTowardsFront(3);
					Btn_TextBox->MoveTowardsFront(2);
				}
				else
				{
					// Cancel load
					clicked = false;
					mOptionsMenu->RemoveChild(tf);
					mOptionsMenu->RemoveChild(Btn_TextBox);
				}

				// Setup the TextBox
				Btn_TextBox->SetSize(222, 114);
				Btn_TextBox->SetPosition(Vector2((-222.0f + Btn_SaveLevel->GetSize().x)*0.5f, -57.0f));
				Btn_TextBox->OnClicked = [=] (ImageButton* sender, void* userData)
				{ 	
					// Load the Level
					/*GPM->GetEngine()->GetGraphicsCore()->LoadTerrain(&tf->GetText()[0]);
					clicked = false;
					mOptionsMenu->RemoveChild(tf);
					mOptionsMenu->RemoveChild(Btn_TextBox);*/
				};
				mOptionsMenu->AddChild(Btn_TextBox);
				auto tc_TextBox = new TextControl("TimesNewRoman16", "Enter Level Name:\n");
				tc_TextBox->SetPosition(30, 30);
				Btn_TextBox->AddChild(tc_TextBox);

				// Set up the text field
				tf->OnEnter = [=] (TextField* sender, void* userData)
				{ 		
					// Save the level
					std::string name =  sender->GetText();
					if(!name.empty())
					{
						ScreenMan->GetEntityManager()->SaveLevel(&name[0]);
						((LevelDesignerScreen*)(m_pParent))->SetLevelName(name);
					}
					clicked = false;
					mOptionsMenu->RemoveChild(Btn_TextBox);
				};
				tf->SetPosition(28, 70);
				Btn_TextBox->AddChild(tf);
			}
		};
		mOptionsMenu->AddChild(Btn_SaveLevel);
		mMouse.AddButton(Btn_SaveLevel);
	}

	// Add button to Exit the game
	auto Btn_ExitGame = new ImageButton("UI/generic.png", "Exit Game", "TimesNewRoman16");
	Btn_ExitGame->SetSize(138.75f, 71.25f);
	Btn_ExitGame->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		std::string msg = "Are you sure you want to exit the game?";
		MessageBoxScreen::ShowYesNo("Alert", msg.c_str(), [=] (ImageButton* sender, void* userData)
		{
			// Exit the game
			PostQuitMessage(0);
		});
	};
	mOptionsMenu->AddChild(Btn_ExitGame);
	mMouse.AddButton(Btn_ExitGame);
	
	// Layout panel in the center of the screen
	mOptionsMenu->LayoutColumn(15);

	// Adjust menu position to be centered
	auto screenSize = GFX->GetScreenSize();
	Vector2 menuSize = Btn_MainMenu->GetSize();
	menuSize.y *= (mOptionsMenu->GetChildCount() + 1);
	mOptionsMenu->SetPosition((screenSize - menuSize) * 0.5f);
	mOptionsMenu->MoveTowardsFront(50);

	// initialize mouse synthesiser
	mMouse.Initialize();
}