#include "DeathScreen.h"
#include "../ScreenManager.h"
#include "../../WorldManager.h"
#include "../../DCEngineDemo/Controls/Camera/CameraManager.h"
#include "../../UI/Controls/SliderControl.h"
#include "../../UI/Controls/TextField.h"
#include "MainMenuScreen.h"
#include "TogglePhysics.h"
#include "../../Game Objects/Turbine.h"
#include "LevelDesignerScreen.h"
#include "MessageBox.h"
#include <dinput.h>

DeathScreen::DeathScreen(IGameScreen* pParent)
	:IGameScreen(),
	m_pParent(pParent)
{
	mOptionsPanel	= 0;
	mOptionsMenu	= 0;
}

void DeathScreen::Update(float dt)
{
	if(mRootControl)
		mRootControl->Update(dt);

	// Continue to update gameplay
	CamMgr->Update(dt);
	HM->Update(dt);
	auto turbine = GPM->GetTurbine();
	if(turbine)
		turbine->Update(dt);
}

void DeathScreen::Initialize()
{
	// Create Pause Screen for terrain
	//mOptionsPanel = new TransitionController(SLIDEUP, 1.0f, true);
	CreateDeathOptionsMenu();
	//AddControl(mOptionsPanel);
	//mOptionsPanel->DoTransition();
}

IControl::HandleInputResult DeathScreen::HandleInput(IInputCore* inputState, float dt)
{
	mMouse.HandleInput(inputState, dt);

	auto inputResult = IGameScreen::HandleInput(inputState, dt);

	// Consume input for button
	if(inputResult == IControl::HandleInputResult::CONSUME)
		return IControl::HandleInputResult::CONSUME;
	
	///////////////////////////////////////////////////////////
	//// Toggle pause menu
	///////////////////////////////////////////////////////////
	//if(inputState->IsNewKeyRelease(DIK_ESCAPE) ||
	//	inputState->IsNewButtonRelease(START))
	//	ScreenMan->RemoveScreen(this);
	
	return IControl::HandleInputResult::CONSUME;
}

void DeathScreen::Draw2D(IGraphicsCore* gDevice)
{
	// Draw 2D UI
	IGameScreen::Draw2D(gDevice);
}

void DeathScreen::ShutDown()
{
	mMouse.ShutDown();
	IGameScreen::ShutDown();
}

void DeathScreen::CreateDeathOptionsMenu()
{	
	auto GFX = ScreenMan->GetGraphics();
	auto screenSize = GFX->GetScreenSize();
	
	// Add Options Menu
	mOptionsMenu = new PanelControl();
	mOptionsMenu->SetSize(400, 400);
	AddControl(mOptionsMenu);

	// Set the Title of the screen
	auto title = new TextControl("Impact48", "You Died!");
	mOptionsMenu->AddChild(title);
	Vector2 titlePos = title->GetSize() * -1.0f;
	//titlePos.x += (screenSize.x ) * 0.5f;
	//titlePos.y += screenSize.y * 0.25f;
	//title->SetPosition( titlePos );


	//mOptionsPanel->AddChild(mOptionsMenu);
	mOptionsMenu->MoveTowardsFront(3);	// Move this up 3 layer

	// Add Options Menu Buttons
	// Add button to resume game
	auto Btn_RetryGame = new ImageButton("UI/generic.png", "Retry", "TimesNewRoman16");
	Btn_RetryGame->SetSize(138.75f, 71.25f);
	Btn_RetryGame->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		GPM->RestartTurbine();
		//TODO: IF OBSTACLES NEED RESET, PUT CODE HERE
		//TODO: Reset timer!!!

		// revive turbine
		GPM->SetTurbineIsDead(false);
		ScreenMan->RemoveScreen(this);
	};
	mOptionsMenu->AddChild(Btn_RetryGame);
	mMouse.AddButton(Btn_RetryGame);

	// Add button to return to the MainMenu
	auto Btn_MainMenu = new ImageButton("UI/generic.png", "Main Menu", "TimesNewRoman16");
	Btn_MainMenu->SetSize(138.75f, 71.25f);
	Btn_MainMenu->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Clear the world and remove the parent screen
		ScreenMan->GetGraphics()->SetBrush(0);
		ScreenMan->GetGraphics()->RemoveTerrain();
		ScreenMan->GetGraphics()->ClearScene();
		ScreenMan->GetEntityManager()->ClearEntityList();
		
		// Add MainMenu Screen and remove this screen
		//ScreenMan->AddScreen(new MainMenuScreen());
		//ScreenMan->RemoveScreen(this);
		ScreenMan->ExitScreens(new MainMenuScreen());

		//if(!ScreenMan->GetEntityManager()->DisplayHiddenObjects())	// Shutdown World Manager
		//	m_pParent->ShutDown();

		//ScreenMan->RemoveScreen(m_pParent);
		
	};
	mOptionsMenu->AddChild(Btn_MainMenu);
	mMouse.AddButton(Btn_MainMenu);

	// Add button to return to Exit the game
	auto Btn_ExitGame = new ImageButton("UI/generic.png", "Exit Game", "TimesNewRoman16");
	Btn_ExitGame->SetSize(138.75f, 71.25f);
	Btn_ExitGame->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		std::string msg = "Are you sure you want to exit the game?";
		MessageBoxScreen::ShowYesNo("Alert", msg.c_str(), [=] (ImageButton* sender, void* userData)
		{
			// Exit the game
			PostQuitMessage(0);
		});
	};
	mOptionsMenu->AddChild(Btn_ExitGame);
	mMouse.AddButton(Btn_ExitGame);
	
	// Layout panel in the center of the screen
	mOptionsMenu->LayoutColumn(15);

	// Adjust menu position to be centered
	//auto screenSize = GFX->GetScreenSize();
	Vector2 menuSize = Btn_MainMenu->GetSize();
	menuSize.y *= (mOptionsMenu->GetChildCount() + 1);
	mOptionsMenu->SetPosition((screenSize - menuSize) * 0.5f);

	// initialize mouse synthesiser
	mMouse.Initialize();
}