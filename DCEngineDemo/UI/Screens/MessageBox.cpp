#include "MessageBox.h"
#include <Interfaces.h>
#include "../ScreenManager.h"
#include "../Controls/ImageControl.h"
#include "../Controls/ImageButton.h"
#include "../Controls/PanelControl.h"

/*ImageControl::ImageControl(char* texturePath, Vector2 position, Vector2 size) : IControl(),
	mFilePath(texturePath),
	mColor(1.0f, 1.0f, 1.0f, 1.0f)
{
	// Create our controls

	auto bkGrnd = new ImageControl();
}*/

MessageBoxScreen::MessageBoxScreen(Vector2 size, const char* header, const char* message, const char* mFont, function<void (ImageButton* sender, void* userData)> onAccept) :
	mHeaderText(header),
	mMessageText(message),
	mFontName(mFont)
{
	mOnAccept = onAccept;
}

void MessageBoxScreen::Initialize()
{

	IControl* buttons;

	if(mOnAccept == nullptr)
	{
		auto okBtn = new ImageButton("UI/generic.png", "OK", "TimesNewRoman16");
		okBtn->OnClicked = [=] (ImageButton* sender, void* userData)
		{ 
			ScreenMan->RemoveScreen(this);
		};

		buttons = okBtn;
	}
	else
	{
		auto pc = new PanelControl;
		buttons = new PanelControl();

		auto noBtn = new ImageButton("UI/generic.png", "NO", "TimesNewRoman16");
		noBtn->OnClicked = [=] (ImageButton* sender, void* userData)
		{ 
			ScreenMan->RemoveScreen(this);
		};
		pc->AddChild(noBtn);

		auto okBtn = new ImageButton("UI/generic.png", "YES", "TimesNewRoman16");
		okBtn->OnClicked = [=] (ImageButton* sender, void* userData)
		{
			mOnAccept(sender, userData);
			ScreenMan->RemoveScreen(this);
		};
		pc->AddChild(okBtn);

		pc->LayoutRow(20.0f);

		auto okSize = okBtn->GetSize();
		auto noSize = noBtn->GetSize();

		pc->SetSize(okSize.x + noSize.x, max(okSize.y, noSize.y));

		buttons = pc;
	}

	auto ic = new ImageControl("UI/TextBox.png");

	auto tc = new TextControl(mFontName, mMessageText);
	auto hc = new TextControl("SpeedControl", mHeaderText);
	tc->SetColor(0.0f, 0.0f, 0.0f);
	hc->SetColor(0.4f, 0.8f, 1.0f);

	// Size of the overall box must be: Size of Text + size of button + size of Header + padding.
	auto txtSize = tc->GetSize();
	auto headSize = hc->GetSize();
	auto buttonSize = buttons->GetSize();
	auto padding = Vector2(35, 60);

	ic->SetSize(Vector2(txtSize.x * 1.8f + (padding.x * 2), (headSize.y + txtSize.y) * 2.5f + (padding.y * 2) + buttonSize.y));
	auto icSize = ic->GetSize();

	auto btnSize = buttons->GetSize();
	buttons->SetPosition((icSize.x * 0.5f) - btnSize.x * 0.5f, icSize.y - padding.y - buttonSize.y);
	ic->AddChild(buttons);

	tc->SetPosition((icSize - txtSize) * 0.5f);
	
	ic->AddChild(tc);

	hc->SetPosition((ic->GetSize().ScaleRet(Vector2(0.07f, 0.11f))));
	ic->AddChild(hc);

	auto screenSize = ScreenMan->GetGraphics()->GetScreenSize();

	ic->SetPosition((screenSize.x - icSize.x) * 0.5f, (screenSize.y - icSize.y) * 0.5f);
	ic->MoveTowardsFront(50);
	AddControl(ic);
}

void MessageBoxScreen::Show(const char* header, const char* message, const char* mFont)
{
	ScreenMan->AddScreen(new MessageBoxScreen(Vector2(500, 500), header, message, mFont));
}

void MessageBoxScreen::ShowYesNo(const char* header, const char* message, function<void (ImageButton* sender, void* userData)> onAccept, const char* mFont)
{
	ScreenMan->AddScreen(new MessageBoxScreen(Vector2(500, 500), header, message, mFont, onAccept));
}