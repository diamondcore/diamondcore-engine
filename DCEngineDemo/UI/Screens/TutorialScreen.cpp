#include "TutorialScreen.h"
#include "../../WorldManager.h"
#include "../../Havok/HavokManager.h"
#include "../../Game Objects/Turbine.h"
#include "../../Game Objects/Engine.h"
#include "../../Game Objects/Pod.h"
#include "GameplayScreen.h"
#include "MainMenuScreen.h"
#include "../ScreenManager.h"
#include "../Controls/PanelControl.h"
#include "../Controls/ImageButton.h"
#include "../../Game Objects/LightningTrap.h"
#include "../../Game Objects/LightEntity.h"
#include "../../DCEngineDemo/Controls/Camera/CameraManager.h"
#include "MessageBox.h"
#include <dinput.h>

#define GFX ScreenMan->GetGraphics()

// The transition delay(in seconds) between completing a 
// objective and displaying the message for the next objective.
#define TRANS_DELAY 2.5f

TutorialScreen::TutorialScreen()
	:IGameScreen()
{
	m_bLevelLoaded		= false;
	mUseController		= false;
	m_iCountDown		= -1;
	m_fTransDelay		= 0.0f;
	mOrigHealth			= 0.0f;
	mGameplayScreen		= 0;
	mGamePlayTutStage	= GP_INTRO;
}

void TutorialScreen::Update(float dt)
{
	if (mRootControl)
		mRootControl->Update(dt);

	if(!m_bLevelLoaded)
	{
		// Loading the Level
		if(m_iCountDown == 0 && !mGameplayScreen)
		{
			mGameplayScreen = new GameplayScreen();
			mGameplayScreen->Initialize();
			m_bLevelLoaded = true;
			m_fTransDelay = TRANS_DELAY;	// Delay while spawn camera is adjusting
		}
		else if(m_iCountDown > 0)
		{
			if(m_iCountDown == 1 && !ScreenMan->GetEntityManager()->DisplayHiddenObjects())
			{
				// Initialize Gameplay
				auto mainCore = ScreenMan->GetMainCore();
				GPM->Init(mainCore);
			}
			m_iCountDown--;	// Count down until Gameplay Screen Starts
		}
	}
	else if(mGameplayScreen)
	{
		mGameplayScreen->Update(dt);
		UpdateGamePlayTutorial(dt);
	}
	//TODO: Add something like this later for the level designer.
	//else if(mLevelDesignerScreen)
	//	UpdateLevelDesignerTutorial();
}

void TutorialScreen::UpdateGamePlayTutorial(float dt)
{
	if(m_fTransDelay > 0.0f)
	{
		m_fTransDelay -= dt;
		return;
	}
	
	//TODO: Probably should rewrite this dialog.
	switch(mGamePlayTutStage)
	{
	case GP_INTRO:
		{
			std::string msg = 
				"Welcome to Turbine the game!\n"
				"This is a game about (BLAH)...";
			MessageBoxScreen::Show("Introduction", msg.c_str());
			mGamePlayTutStage = GP_FORWARD;	// Move to the next stage
			break;
		}
	case GP_FORWARD:
		{
			if(mNeedMessage)
			{
				std::string msg = 
					"You can move the Left Engine forward by\n"
					"pressing [Y] and the Right Engine forward\n"
					"by pressing [U].\n"
					"Try it now.";
				MessageBoxScreen::Show("Forward Movement", msg.c_str());
				mNeedMessage = false;
			}
			else
			{
				// Check if the turbine is moving forward
				auto turbine = GPM->GetTurbine();
				if( turbine->GetLeftEngine()->GetSpeed() > 50.0f ||
					turbine->GetRightEngine()->GetSpeed() > 50.0f )
				{
					// Move to the next stage
					mGamePlayTutStage = GP_REVERSE;	
					mNeedMessage = true;
					m_fTransDelay = TRANS_DELAY;	// Delay for a few seconds
				}
			}
			break;
		}
	case GP_REVERSE:
		{
			if(mNeedMessage)
			{
				std::string msg = 
					"You can move the Left Engine in reverse by\n"
					"pressing [H] and the Right Engine in\n"
					"reverse by pressing [J].\n"
					"Try it now.";
				MessageBoxScreen::Show("Reverse Movement", msg.c_str());
				mNeedMessage = false;
			}
			else
			{
				// Check if the turbine is moving in reverse
				auto turbine = GPM->GetTurbine();
				if( turbine->GetLeftEngine()->GetBrake() > 0.0f ||
					turbine->GetRightEngine()->GetBrake() > 0.0f )
				{
					// Move to the next stage
					mGamePlayTutStage = GP_CAMERA_CONTROLS;	
					mNeedMessage = true;
					m_fTransDelay = TRANS_DELAY;	// Delay for a few seconds
				}
			}
			break;
		}
	case GP_CAMERA_CONTROLS:
		{
			if(mNeedMessage)
			{
				std::string msg = 
					//"Camera control instruction stuff...\n"
					"[W] - Zoom In     [A] - Zoom Out\n"
					"[S] - Rotate Left [D] - Rotate Right\n"
					"[E] - Rotate Up   [Q] - Rotate Down";
				MessageBoxScreen::Show("Camera Control", msg.c_str());
				mNeedMessage = false;
			}
			else
			{
				// Check if the camera has been moved
				//TODO: Check if camera has been moved below.
				if( true )
				{
					// Move to the next stage	
					mGamePlayTutStage = GP_CAMERA_VIEW;
					mNeedMessage = true;
					m_fTransDelay = TRANS_DELAY;	// Delay for a few seconds
				}
			}
			break;
		}
	case GP_CAMERA_VIEW:
		{
			if(mNeedMessage)
			{
				std::string msg = 
					"Camera toggle view instruction stuff...\n"
					"[N] - Toggle Camera View";
				MessageBoxScreen::Show("Camera View", msg.c_str());
				mNeedMessage = false;
			}
			else
			{
				// Check if the camera has been toggled
				//TODO: Check if camera toggle is toggled below.
				if( true )
				{
					// Move to the next stage	
					mGamePlayTutStage = GP_BOOST;
					mNeedMessage = true;
					m_fTransDelay = TRANS_DELAY;	// Delay for a few seconds
				}
			}
			break;
		}
	case GP_BOOST:
		{
			if(mNeedMessage)
			{
				std::string msg = 
					"Boost description & instruction stuff...\n"
					"[Spacebar] - Boost";
				MessageBoxScreen::Show("Boosting", msg.c_str());
				mNeedMessage = false;
			}
			else
			{
				// Check if the turbine has been boosted
				auto turbine = GPM->GetTurbine();
				if( turbine->GetLeftEngine()->GetSpeed()  >= 200.0f &&
					turbine->GetRightEngine()->GetSpeed() >= 200.0f )
				{
					// Move to the next stage	
					mGamePlayTutStage = GP_HEAL;
					mNeedMessage = true;
					m_fTransDelay = TRANS_DELAY;	// Delay for a few seconds
				}
			}
			break;
		}
	case GP_HEAL:
		{
			if(mNeedMessage)
			{
				std::string msg = 
					"Healing description & instruction stuff...\n"
					"[X] - Heal";
				MessageBoxScreen::Show("Healing", msg.c_str());
				mNeedMessage = false;

				// Damage the turbine
				auto turbine = GPM->GetTurbine();
				mOrigHealth = turbine->GetHealth()*0.9f;
				turbine->ApplyDamage(mOrigHealth*0.5f, e_SelfDamage);
			}
			else
			{
				// Check if the turbine has been healed
				if( GPM->GetTurbine()->GetHealth() >= mOrigHealth )
				{
					// Move to the next stage	
					mGamePlayTutStage = GP_FINISH;
					mNeedMessage = true;
					m_fTransDelay = TRANS_DELAY;	// Delay for a few seconds
				}
			}
			break;
		}
	case GP_FINISH:
		{
			if(mNeedMessage)
			{
				std::string msg = 
					"Now your ready to play for Real YO... \n"
					"Blah Blah...Blah.......(Something poetic).\n"
					"Get out there and have fun.";
				MessageBoxScreen::Show("Tutorial Finished", msg.c_str());
				mNeedMessage = false;
			}
			else
			{
				// Remove the tutorial level
				// Clear the world and remove the parent screen
				ScreenMan->GetGraphics()->SetBrush(0);
				ScreenMan->GetGraphics()->RemoveTerrain();
				ScreenMan->GetGraphics()->ClearScene();
				ScreenMan->GetEntityManager()->ClearEntityList();

				// Add MainMenu Screen and remove this screen
				ScreenMan->ExitScreens(new MainMenuScreen());
			}
			break;
		}
	default:
		break;
	};
}

IControl::HandleInputResult TutorialScreen::HandleInput(IInputCore* inputState, float dt)
{
	if(mGameplayScreen)
		mGameplayScreen->HandleInput(inputState, dt);

	return IGameScreen::HandleInput(inputState, dt);
}

void TutorialScreen::Initialize()
{
	// Setup for Gameplay Tutorial
	ScreenMan->GetEntityManager()->SetDisplayHiddenObject(false);

	InitializeLevel();
}

void TutorialScreen::InitializeLevel()
{
#if 1
	// Create a new level for tutorial
	GFX->RemoveTerrain();
	GFX->InitTerrain(170, 170, 510, 510, 1);	// Large Terrain
	
	std::string m_TerrainTex[3];
	m_TerrainTex[0] = "Grass/Grass1D.png";
	m_TerrainTex[1] = "Dirt/Dirt2D.png";
	m_TerrainTex[2] = "Rock/Rock2D.png";

	// Load Terrain Textures
	GFX->LoadTexture(&m_TerrainTex[0][0]);
	GFX->LoadTexture(&m_TerrainTex[1][0]);
	GFX->LoadTexture(&m_TerrainTex[2][0]);

	// Set Terrain Textures
	GFX->SetTerrainTex1(&m_TerrainTex[0][0]);
	GFX->SetTerrainTex2(&m_TerrainTex[1][0]);
	GFX->SetTerrainTex3(&m_TerrainTex[2][0]);

	// Add a light
	auto light = std::shared_ptr<LightEntity>(new LightEntity);
	light->SetPosition(Vector3(-58.1764f, 132.938f, 60.3592));
	light->SetScale(Vector3(16.36f, 16.36f, 16.36f));
	light->Init();
	light->Update(0.0f);	// Apply position changes
	ScreenMan->GetEntityManager()->AddEntity(light->GetID(), light);
#else
	// Load a level
	ScreenMan->GetEntityManager()->LoadLevel( "TutorialLevel" );
#endif
	m_iCountDown = 4;	// Count down 4 Updates until starting the level
}

void TutorialScreen::ShutDown()
{
	if(mGameplayScreen)
	{
		mGameplayScreen->ShutDown();
		mGameplayScreen = 0;
	}
	
	IGameScreen::ShutDown();
}

void TutorialScreen::Draw2D(IGraphicsCore* gDevice) 
{
	if(mGameplayScreen)
		mGameplayScreen->Draw2D(gDevice);
	IGameScreen::Draw2D(gDevice);
}
void TutorialScreen::Draw3D(IGraphicsCore* gDevice)
{
	if(mGameplayScreen)
		mGameplayScreen->Draw3D(gDevice);
	IGameScreen::Draw3D(gDevice);
}