#pragma once

#include "../Controls/IControl.h"
#include <Interfaces.h>

class IInputCore;

class IGameScreen
{
protected:
	bool mIsOpaque;
	IControl* mRootControl;

	IGameScreen() 
	{
		mIsOpaque = true;
		mRootControl = NULL;
	}

public:
	
	bool IsOpaque() { return mIsOpaque; }

	virtual void Draw2D(IGraphicsCore* gDevice) 
	{
		if (mRootControl)
			mRootControl->Draw(gDevice);
	}

	virtual void Draw3D(IGraphicsCore* gDevice) { }

	virtual IControl::HandleInputResult HandleInput(IInputCore* inputState, float dt) 
	{
		if (mRootControl)
			return mRootControl->HandleInput(inputState, dt);

		return IControl::NORMAL;
	}

	const IControl* GetRootControl() { return mRootControl; }

	void AddControl(IControl* control)
	{
		if (!mRootControl)
		{
			mRootControl = control;
			mRootControl->Initialize();
		}
		else
			mRootControl->AddChild(control);
	}

	void RemoveControl(IControl* control)
	{
		control->Destroy();
	}

	virtual void Initialize() = 0;

	virtual void Update(float dt)
	{ 
		if (mRootControl)
			mRootControl->Update(dt);
	}
	virtual void ShutDown()
	{
		if (mRootControl != NULL)
			RemoveControl(mRootControl);

		delete this;
	}
};