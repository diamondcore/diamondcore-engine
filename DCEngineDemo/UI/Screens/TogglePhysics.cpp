#include "TogglePhysics.h"
#include "../ScreenManager.h"
#include "../../DCEngineDemo/Controls/ControlsInclude.h"
//#include "../../DCEngineDemo/Controls/Camera/CameraManager.h"
#include "../../UI/Controls/SliderControl.h"
#include "../../UI/Controls/TextField.h"
#include "PauseScreen.h"
#include "LevelDesignerScreen.h"
#include "../DCEngineDemo/Havok/HavokManager.h"
#include "../../Game Objects/Engine.h"
#include "../../Game Objects/Turbine.h"
#include "../../Game Objects/Terrain.h"
#include <dinput.h>
#include "MessageBox.h"

TogglePhysics::TogglePhysics(IGameScreen* pParent)
	:IGameScreen(),
	m_pParent(pParent)
{
	m_pTurbine	= 0;
	m_pTerrain	= NULL;
	m_iCount	= 0;
}

void TogglePhysics::Update(float dt)
{
	if(mRootControl)
		mRootControl->Update(dt);

	CamMgr->Update(dt);
		
	HM->Update(dt);
	if(m_iCount > 3)
	{
		m_pTurbine->Update(dt);
	}
	else
		m_iCount++;
}

void TogglePhysics::Initialize()
{
	((LevelDesignerScreen*)(m_pParent))->ExitScreen();

	auto mainCore = ScreenMan->GetMainCore();

	// Set Graphics World Properties
	auto GFX = mainCore->GetGraphicsCore();
	
	// Initialize physics
	HM->Init();

	ScreenMan->GetEntityManager()->InitHavokTerrain();

	// Spawn turbine at camera position
	m_pTurbine = new Turbine(((LevelDesignerScreen*)(m_pParent))->GetPrevCamPos(),Vector3(-2,0,6),Vector3(2,0,6),Vector3(0,0,-1),Vector3(.5,.5,3),
		Vector3(.5,.5,3),Vector3(1,.25,1),"MKIII_Pod.X","MKIII_Engine.X");

	Quaternion rot;
	Vector3 look = CamMgr->m_pCurrentCamControl->GetLookAt();
	rot.QuatFromVectors(Vector3(0,0,1), look);
	rot.x = rot.z = 0.0f;
	rot.Normalize();
	m_pTurbine->SetOrientation(rot);
	CamMgr->SetActiveCamControl(m_pTurbine->GetCamID());


	/////////////////////////////////////////////////
	// Create UI
	/////////////////////////////////////////////////
	auto pc = new PanelControl();
	AddControl(pc);

	// Add the exit physics buttons
	auto Btn_Exit = new ImageButton("UI/generic.png", "Exit", "TimesNewRoman16");
	Btn_Exit->OnClicked = [=] (ImageButton* sender, void* userData)
	{ 
		// Go back to the LevelDesignerScreen
		((LevelDesignerScreen*)(m_pParent))->EnterScreen();

		ScreenMan->GetMainCore()->GetSoundCore()->StopAll(0);
		ScreenMan->GetMainCore()->GetSoundCore()->StopAll(1);
		
		// Remove the Turbine and Physics
		ScreenMan->RemoveScreen(this);
	};
	pc->AddChild(Btn_Exit);	
	
	// Set the layout as columns
	pc->LayoutColumn(5.0f);
	pc->MoveTowardsFront(50);
}

IControl::HandleInputResult TogglePhysics::HandleInput(IInputCore* inputState, float dt)
{
	// Test going into pause screen
	if( !PauseScreen::AlreadyExists() &&
		(inputState->IsNewKeyRelease(DIK_ESCAPE) ||
		inputState->IsNewButtonRelease(START)) )
		ScreenMan->AddScreen( new PauseScreen(this) );

	auto inputResult = IGameScreen::HandleInput(inputState, dt);

	// Consume input for button
	if(inputResult == IControl::HandleInputResult::CONSUME)
		return IControl::HandleInputResult::CONSUME;

	//need to determine if we're using controller or keyboard
	if (ScreenMan->GetMainCore()->GetInputCore()->GetControllerIsConnected() )
	{
		// throttle
		Vector2 triggers = inputState->GetTriggers()*100.0f;
		m_pTurbine->SetLeftEngineSpeed(triggers.x);
		m_pTurbine->SetRightEngineSpeed(triggers.y);
	

		//Brake
		if (inputState->IsButtonDown(LEFT_BUMPER ))
		{
			m_pTurbine->BrakeLeftEngine(100);
		}
		else
		{
			m_pTurbine->BrakeLeftEngine(0);
		}
		if (inputState->IsButtonDown(RIGHT_BUMPER ))
		{
			m_pTurbine->BrakeRightEngine(100);
		}
		else
		{
			m_pTurbine->BrakeRightEngine(0);
		}

		auto GFX = ScreenMan->GetGraphics();
		// repair
		static bool useRepair = false;
		float energyLeft = m_pTurbine->GetCurrentEnergy() - 50*dt;
		if(inputState->IsButtonDown(X) && energyLeft > 0.0f)
		{
			m_pTurbine->ModifyCurrentEnergy(-50*dt);			
			m_pTurbine->Repair(dt);
			energyLeft -= 50*dt;	// Update energy left, for boosts check
			
			if(!useRepair)
			{
				GFX->UseDistortion(true);
				auto left = m_pTurbine->GetLeftEngine();
				auto right = m_pTurbine->GetRightEngine();
				GFX->CreateDistortionMesh(left->GetID(), &left->GetMeshName()[0], 0);
				GFX->CreateDistortionMesh(right->GetID(), &right->GetMeshName()[0], 0);
				GFX->SetDistortIntensity(0, 5.0f, 10.0f);
				Vector3 scale = left->GetScale() * 1.1f;
				GFX->SetDistortionMeshScale(left->GetID(), &left->GetMeshName()[0], 0, scale);
				GFX->SetDistortionMeshScale(right->GetID(), &right->GetMeshName()[0], 0, scale);
			}
			useRepair = true;
		}
		else if(useRepair)
		{
			auto left = m_pTurbine->GetLeftEngine();
			auto right = m_pTurbine->GetRightEngine();
			GFX->RemoveDistortionMesh(left->GetID(), &left->GetMeshName()[0]);
			GFX->RemoveDistortionMesh(right->GetID(), &right->GetMeshName()[0]);
			GFX->UseDistortion(false);
			useRepair = false;
		}

		// boost
		static bool useBoost = false;
		if (inputState->IsButtonDown(A) && energyLeft > 0.0f)
		{
			m_pTurbine->ModifyCurrentEnergy(-50*dt);
			m_pTurbine->SetLeftEngineSpeed(200);
			m_pTurbine->SetRightEngineSpeed(200);
			// Turn blur on
			if(!useBoost)
				ScreenMan->GetGraphics()->UseBlur(true);
			useBoost = true;
		}
		else if(useBoost)
		{
			// Turn blur off
			ScreenMan->GetGraphics()->UseBlur(false);
			useBoost = false;
		}

		Vector2 LeftStick = inputState->GetLeftStick();
		if (LeftStick.LengthSquared() > 0)
			m_pTurbine->PushTurbineDown(fabs(LeftStick.y),dt);

		Vector2 RightStick = inputState->GetRightStick();
		if (RightStick.LengthSquared() > 0)
			m_pTurbine->RotateTurbine(RightStick.x, dt);
		



	}
	else
	{
		// Keyboard movement
		if(inputState->IsKeyDown(DIK_Y))
			m_pTurbine->SetLeftEngineSpeed(100.0f);
		else
			m_pTurbine->SetLeftEngineSpeed(0.0f);
		if(inputState->IsKeyDown(DIK_U))
			m_pTurbine->SetRightEngineSpeed(100.0f);
		else
			m_pTurbine->SetRightEngineSpeed(0.0f);	

		//Brake
		if (inputState->IsKeyDown(DIK_H) )
		{
			m_pTurbine->BrakeLeftEngine(100);
		}
		else
		{
			m_pTurbine->BrakeLeftEngine(0);
		}
		if (inputState->IsKeyDown(DIK_J)	)
		{
			m_pTurbine->BrakeRightEngine(100);
		}
		else
		{
			m_pTurbine->BrakeRightEngine(0);
		}
		
		auto GFX = ScreenMan->GetGraphics();
		// repair
		static bool useRepair = false;
		float energyLeft = m_pTurbine->GetCurrentEnergy() - 50*dt;
		if(inputState->IsKeyDown(DIK_X) && energyLeft > 0.0f)
		{
			m_pTurbine->ModifyCurrentEnergy(-50*dt);			
			m_pTurbine->Repair(dt);
			energyLeft -= 50*dt;	// Update energy left, for boosts check
			
			if(!useRepair)
			{
				GFX->UseDistortion(true);
				auto left = m_pTurbine->GetLeftEngine();
				auto right = m_pTurbine->GetRightEngine();
				GFX->CreateDistortionMesh(left->GetID(), &left->GetMeshName()[0], 0);
				GFX->CreateDistortionMesh(right->GetID(), &right->GetMeshName()[0], 0);
				GFX->SetDistortIntensity(0, 5.0f, 10.0f);
				Vector3 scale = left->GetScale() * 1.1f;
				GFX->SetDistortionMeshScale(left->GetID(), &left->GetMeshName()[0], 0, scale);
				GFX->SetDistortionMeshScale(right->GetID(), &right->GetMeshName()[0], 0, scale);
			}
			useRepair = true;
		}
		else if(useRepair)
		{
			auto left = m_pTurbine->GetLeftEngine();
			auto right = m_pTurbine->GetRightEngine();
			GFX->RemoveDistortionMesh(left->GetID(), &left->GetMeshName()[0]);
			GFX->RemoveDistortionMesh(right->GetID(), &right->GetMeshName()[0]);
			GFX->UseDistortion(false);
			useRepair = false;
		}

		// boost
		static bool useBoost = false;
		if (inputState->IsKeyDown(DIK_SPACE) && energyLeft > 0.0f)
		{
			m_pTurbine->ModifyCurrentEnergy(-50 * dt);
			m_pTurbine->SetLeftEngineSpeed(200);
			m_pTurbine->SetRightEngineSpeed(200);
			// Turn blur on
			if(!useBoost)
				ScreenMan->GetGraphics()->UseBlur(true);
			useBoost = true;
		}
		else if(useBoost)
		{
			// Turn blur off
			ScreenMan->GetGraphics()->UseBlur(false);
			useBoost = false;
		}
	}

	// setpos
	//if (inputState->IsNewKeyRelease(DIK_O))
	//{
	//	m_pTurbine->SetPosition(0,0,0);
	//	m_pTurbine->SetOrientation(0,0,0);
	//}

	// Test getting zone information
	static float zoneDB = 0.0f;
	if(inputState->IsKeyDown(DIK_1) && zoneDB < 0.0f)
	{
		D3DXCOLOR color;
		ScreenMan->GetGraphics()->GetColorAt(m_pTurbine->GetPosition(), color);
		// Display Color info
		char buf[128];
		sprintf(buf, "%f %f %f %f", color.r, color.g, color.b, color.a);
		MessageBoxScreen::Show("Current Color", buf);
		zoneDB = 2.0f;
	}
	else
		zoneDB -= dt;
	
#if defined(DEBUG) | defined(_DEBUG)
	// Test damage particles
	if(inputState->IsKeyDown(DIK_2))
	{
		for(int i = 0; i < 3; i++)
			m_pTurbine->ApplyDamage(2.0f, i, 0);
	}
#endif

	//////////////////////////////////////////////////
	//			Do normal camera logic
	//////////////////////////////////////////////////
	// Handle camera input
	//CamMgr->HandleInput(inputState, dt);	
	return IControl::HandleInputResult::CONSUME;
}

void TogglePhysics::Draw2D(IGraphicsCore* gDevice)
{
	// Draw 2D UI
	IGameScreen::Draw2D(gDevice);
}

void TogglePhysics::ShutDown()
{
	if(m_pTurbine)
	{
		m_pTurbine->Shutdown();
		delete m_pTurbine;
		m_pTurbine	= 0;
	}

	// Make sure all attributes that may have been turned on are off
	auto GFX = ScreenMan->GetGraphics();
	GFX->UseDistortion(false);
	GFX->UseBlur(false);

	HM->Shutdown();
	IGameScreen::ShutDown();
}

