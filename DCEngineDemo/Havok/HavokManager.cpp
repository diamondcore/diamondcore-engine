#include "HavokManager.h"
#include "TriggerVolumes.h"
#include <Physics/Dynamics/Constraint/Bilateral/LimitedHinge/hkpLimitedHingeConstraintData.h>
#include <DCMathLib.h>

#define VDB 0
std::shared_ptr<HavokManager> HavokManager::s_Instance = 0;
HavokManager* HavokManager::Instance()
{
	if(!s_Instance)
		s_Instance = std::shared_ptr<HavokManager>(new HavokManager);
	return (HavokManager*)s_Instance.get();
}
HavokManager::HavokManager()
{
	m_GroupFilter = 0;
}
HavokManager::~HavokManager()
{
}

void HavokManager::Init()
{
	// Create the Physics World
	m_HavokWorld = new HavokUtility(false);	// Enable Visual Debugger

	
	// Collision group 2 will not collide with group 3
	m_HavokWorld->getWorld()->lock();
	m_GroupFilter = new hkpGroupFilter();
	DisableCosllisionsBetween(2, 3);
	m_HavokWorld->getWorld()->setCollisionFilter(m_GroupFilter);
	m_HavokWorld->getWorld()->unlock();

	HM->DisableCosllisionsBetween(2, 4);	// Disable collision with smasher
	HM->DisableCosllisionsBetween(3, 4);	// Disable collision with terrain	
}
void HavokManager::Shutdown()
{
	m_HavokWorld->getWorld()->lock();
	// Remove all constraint references
	for(auto i = m_ConstraintList.begin();
		i != m_ConstraintList.end(); i++)
	{
		for(auto j = (*i).second.begin();
			j != (*i).second.end(); j++)
			m_HavokWorld->getWorld()->removeConstraint((*j).second);
	}
	m_ConstraintList.clear();

	// Remove all of the entity references
	for(auto i = m_EntityList.begin();
		i != m_EntityList.end(); i++)
		if(m_TriggerVolumes.find((*i).first) == m_TriggerVolumes.end())	// Make sure to not delete entities of trigger volumes
			(*i).second->removeReference();
	m_HavokWorld->getWorld()->unlock();
	m_EntityList.clear();

	//m_HavokWorld->shutdownHavok();
	// Delete the Havok World
	delete m_HavokWorld;
	s_Instance = 0;
}

void HavokManager::RemoveEntity(int id)
{
	// Check if the ID is in the map
	auto iter = m_EntityList.find(id);
	if(iter == m_EntityList.end())
		return;

	// Remove entity from the world
	m_HavokWorld->getWorld()->lock();
	m_HavokWorld->getWorld()->removeEntity(m_EntityList[id]);
	m_EntityList[id]->removeReference();	// Remove our local reference
	m_HavokWorld->getWorld()->unlock();

	// Clear from the map
	m_EntityList.erase( iter );
}

void HavokManager::RemoveTriggerVolume(int id)
{
	// Check if the ID is in the map
	auto iter = m_TriggerVolumes.find(id);
	if(iter == m_TriggerVolumes.end())
		return;

	// Remove from vector
	for(int i = 0; i < m_vTriggerVolumes.size(); i++)
		if(m_vTriggerVolumes[i]->getTriggerBody()->getUid() == id)
		{
			m_vTriggerVolumes.erase( m_vTriggerVolumes.begin() + i );
			break;
		}

	// Remove from entity list if it exists
	auto entity = m_EntityList.find(id);
	if(entity != m_EntityList.end())
		m_EntityList.erase( entity );

	// Remove entity from the world
	m_HavokWorld->getWorld()->lock();
	m_HavokWorld->getWorld()->removeEntity(m_TriggerVolumes[id]->getTriggerBody());
	//m_TriggerVolumes[id]->removeReference();	// Remove our local reference
	m_HavokWorld->getWorld()->unlock();

	// Clear from the map
	m_TriggerVolumes.erase( iter );
}

void HavokManager::RemoveConstraint(int bodyID, int targetID)
{
	// Check if the body ID is valid
	auto iter = m_ConstraintList.find( bodyID );
	if(iter == m_ConstraintList.end())
		return;
	// Check if the target ID is valid
	auto list = (*iter).second.find(targetID);
	if(list == (*iter).second.end())
		return;
	
	// Remove entity from the world
	m_HavokWorld->getWorld()->lock();
	int ret = m_HavokWorld->getWorld()->removeConstraint( list->second );
	//list->second->removeReference();	// Remove the local reference
	m_HavokWorld->getWorld()->unlock();

	// Clear from maps
	iter->second.erase( list );	// Remove from target map
	if(iter->second.empty())
		m_ConstraintList.erase( iter );	// Remove from body map
}

hkpRigidBody* HavokManager::FindEntity(int id)
{
	// Check if the ID is in the map
	auto iter = m_EntityList.find(id);
	if(iter == m_EntityList.end())
		return NULL;
	else
		return iter->second;
}

void HavokManager::SetBodyPosition(hkpRigidBody* body,float x, float y, float z)
{
	m_HavokWorld->getWorld()->lock();
	body->setPosition(hkVector4(x, y, z));
	m_HavokWorld->getWorld()->unlock();
}
void HavokManager::SetBodyOrientation(hkpRigidBody* body,float x, float y, float z)
{
	m_HavokWorld->getWorld()->lock();
	hkQuaternion scratch;
	//scratch.setFromEulerAngles(z,x,y);
	scratch.setFromEulerAngles(y,x,z);
	body->setRotation(scratch);
	m_HavokWorld->getWorld()->unlock();
}
void HavokManager::RotateBody(hkpRigidBody* body,float x, float y, float z)
{
	m_HavokWorld->getWorld()->lock();
	hkQuaternion scratch;
	//scratch.setFromEulerAngles(z,x,y);
	scratch.setFromEulerAngles(y,x,z);
	scratch.mul(body->getRotation());
	body->setRotation(scratch);
	m_HavokWorld->getWorld()->unlock();
}
void HavokManager::SetBodyFriction(hkpRigidBody* body, float friction)
{
	m_HavokWorld->getWorld()->lock();
	body->setFriction(friction);
	m_HavokWorld->getWorld()->unlock();
}
void HavokManager::SetAngularDampining(hkpRigidBody* body, float dampining)
{
	m_HavokWorld->getWorld()->lock();
	body->setAngularDamping(dampining);
	m_HavokWorld->getWorld()->unlock();
}
void HavokManager::AddToCollisionGroup(hkpRigidBody* body, int group)
{
	m_HavokWorld->getWorld()->lock();
	body->setCollisionFilterInfo( hkpGroupFilter::calcFilterInfo(group));
	m_HavokWorld->getWorld()->unlock();
}
void HavokManager::DisableCosllisionsBetween(int groupA, int groupB)
{
	m_HavokWorld->getWorld()->lock();
	m_GroupFilter->disableCollisionsBetween(groupA, groupB);
	m_HavokWorld->getWorld()->unlock();
}
// Create Box
hkpRigidBody* HavokManager::AddMovingBox(int id, float posX, float posY, float posZ, 
								float sizeX, float sizeY, float sizeZ)
{
	return AddMovingBox(id, posX, posY, posZ, sizeX, sizeY, sizeZ, 5.0f);	// Use default mass
}
hkpRigidBody* HavokManager::AddMovingBox(int id, float posX, float posY, float posZ, 
								float sizeX, float sizeY, float sizeZ, float massKg)
{
	// Create Box shape with the dimensions
	hkReal hkConvexShapeRadius = 0.05;
	hkpShape* movingBodyShape = new hkpBoxShape(hkVector4(sizeX, sizeY, sizeZ));//, hkConvexShapeRadius);

	// Compute the inertia tensor from the shape
	hkMassProperties massProperties;

	hkpInertiaTensorComputer::computeShapeVolumeMassProperties(movingBodyShape, massKg, massProperties);

	// Create the Rigid Body information
	hkpRigidBodyCinfo rigidBodyInfo;

	// Set the rigid body properties
	rigidBodyInfo.m_position		= hkVector4(posX, posY, posZ);
	rigidBodyInfo.m_mass			= massProperties.m_mass;
	rigidBodyInfo.m_centerOfMass	= massProperties.m_centerOfMass;
	rigidBodyInfo.m_inertiaTensor	= massProperties.m_inertiaTensor;
	rigidBodyInfo.m_shape			= movingBodyShape;
	rigidBodyInfo.m_motionType		= hkpMotion::MOTION_BOX_INERTIA;
	rigidBodyInfo.m_friction = 0.5f;
	//SEVERE HACK-SHOULDNT HAVE TO SET GRAVITY THIS HIGH
	//DONE TO KEEP TURBINE FROM GOING CRAZY
	//rigidBodyInfo.m_gravityFactor = 0.001f;
	//rigidBodyInfo.m_friction = 1;

	// Create new rigid body with this info
	hkpRigidBody* newRigidBody = new hkpRigidBody(rigidBodyInfo);

	// Add rigid body to Physics World
	m_HavokWorld->getWorld()->lock();
	m_HavokWorld->getWorld()->addEntity(newRigidBody);

	m_EntityList[newRigidBody->getUid()] = newRigidBody;		// Add the rigid body to our list
	// The shape is stored in rigid body, so don't need this reference anymore
	movingBodyShape->removeReference();
	m_HavokWorld->getWorld()->unlock();
	return newRigidBody;
}
hkpRigidBody* HavokManager::AddMovingBox(int id, float posX, float posY, float posZ, 
								float sizeX, float sizeY, float sizeZ, float massKg, int groupID)
{
	// Create Box shape with the dimensions
	hkReal hkConvexShapeRadius = 0.05;
	hkpShape* movingBodyShape = new hkpBoxShape(hkVector4(sizeX, sizeY, sizeZ));//, hkConvexShapeRadius);

	// Compute the inertia tensor from the shape
	hkMassProperties massProperties;

	hkpInertiaTensorComputer::computeShapeVolumeMassProperties(movingBodyShape, massKg, massProperties);

	// Create the Rigid Body information
	hkpRigidBodyCinfo rigidBodyInfo;

	// Set the rigid body properties
	rigidBodyInfo.m_position		= hkVector4(posX, posY, posZ);
	rigidBodyInfo.m_mass			= massProperties.m_mass;
	rigidBodyInfo.m_centerOfMass	= massProperties.m_centerOfMass;
	rigidBodyInfo.m_inertiaTensor	= massProperties.m_inertiaTensor;
	rigidBodyInfo.m_shape			= movingBodyShape;
	rigidBodyInfo.m_motionType		= hkpMotion::MOTION_BOX_INERTIA;
	rigidBodyInfo.m_friction = 0.5f;
	rigidBodyInfo.m_collisionFilterInfo = hkpGroupFilter::calcFilterInfo(groupID);
	//SEVERE HACK-SHOULDNT HAVE TO SET GRAVITY THIS HIGH
	//DONE TO KEEP TURBINE FROM GOING CRAZY
	//rigidBodyInfo.m_gravityFactor = 0.001f;
	//rigidBodyInfo.m_friction = 1;

	// Create new rigid body with this info
	hkpRigidBody* newRigidBody = new hkpRigidBody(rigidBodyInfo);

	// Add rigid body to Physics World
	m_HavokWorld->getWorld()->lock();
	m_HavokWorld->getWorld()->addEntity(newRigidBody);

	m_EntityList[newRigidBody->getUid()] = newRigidBody;		// Add the rigid body to our list
	// The shape is stored in rigid body, so don't need this reference anymore
	movingBodyShape->removeReference();
	m_HavokWorld->getWorld()->unlock();
	return newRigidBody;
}

// Create Sphere
hkpRigidBody* HavokManager::AddMovingSphere(int id, float posX, float posY, float posZ, float radius)
{
	return AddMovingSphere(id, posX, posY, posZ, radius, 1.0f);	// Add with default mass
}
hkpRigidBody* HavokManager::AddMovingSphere(int id, float posX, float posY, float posZ, float radius, float massKg)
{
	hkpSphereShape* sphere = new hkpSphereShape(radius);

	// Create the Rigid Body information
	hkpRigidBodyCinfo rigidBodyInfo;

	//hkpExtendedMeshShape* mesh = new hkpExtendedMeshShape();
	// Set the rigid body properties

	rigidBodyInfo.m_shape		= sphere;
	rigidBodyInfo.m_motionType	= hkpMotion::MOTION_DYNAMIC;
	hkpInertiaTensorComputer::setShapeVolumeMassProperties(sphere, massKg, rigidBodyInfo);
	rigidBodyInfo.m_position	= hkVector4(posX, posY, posZ);
	rigidBodyInfo.m_friction	= 1.0f;
	rigidBodyInfo.m_restitution	= 0.2f;
	
	// Create new rigid body with this info
	hkpRigidBody* newRigidBody = new hkpRigidBody(rigidBodyInfo);

	// Add rigid body to Physics World
	m_HavokWorld->getWorld()->lock();
	m_HavokWorld->getWorld()->addEntity(newRigidBody);
	m_EntityList[newRigidBody->getUid()] = newRigidBody;
	
	// The shape is stored in rigid body, so don't need this reference anymore
	sphere->removeReference();
	m_HavokWorld->getWorld()->unlock();

	return newRigidBody;
}
hkpRigidBody* HavokManager::AddCapsule()
{
		hkVector4 A( 0.f, 1.2f, 0.f);
		hkVector4 B( 0.f,-1.2f, 0.f);

		// Radius for the capsule
		hkReal radius = 1.0f;

		hkpCapsuleShape* shape = new hkpCapsuleShape(A, B, radius);

		// Set up construction info for this rigid body
		hkpRigidBodyCinfo rigidBodyInfo;

		rigidBodyInfo.m_motionType = hkpMotion::MOTION_SPHERE_INERTIA;
		hkMatrix3Util::_setDiagonal( 1.f, 1.f, 1.f, rigidBodyInfo.m_inertiaTensor );

		rigidBodyInfo.m_gravityFactor = 0.2f;
		rigidBodyInfo.m_mass = 1.0f;
		rigidBodyInfo.m_shape = shape;
		rigidBodyInfo.m_position.set(-3,0,3);

		hkpRigidBody* newRigidBody = new hkpRigidBody(rigidBodyInfo);

		// Add rigid body to Physics World
		m_HavokWorld->getWorld()->lock();
		m_HavokWorld->getWorld()->addEntity(newRigidBody);
		m_EntityList[newRigidBody->getUid()] = newRigidBody;
	
	// The shape is stored in rigid body, so don't need this reference anymore
	shape->removeReference();
	m_HavokWorld->getWorld()->unlock();

	return newRigidBody;

}
// Add fixed surface
hkpRigidBody* HavokManager::AddFixedSurface(int id, float posX, float posY, float posZ, float sizeX, float sizeY, float sizeZ)
{
	// Create default rotation
	hkReal angle = 0.0;
	hkVector4 axis = hkVector4(0.0, 0.0, 1.0);
	hkQuaternion rot(axis, angle);
	return AddFixedSurface(id, posX, posY, posZ, sizeX, sizeY, sizeZ, rot.getImag().getComponent(0), rot.getImag().getComponent(1), rot.getImag().getComponent(2), rot.getReal());
}
hkpRigidBody* HavokManager::AddFixedSurface(int id, float posX, float posY, float posZ, float sizeX, float sizeY, float sizeZ,
						float rotX, float rotY, float rotZ, float rotW)
{
	// Create Box shape with dimensions
	hkReal hkConvexShapeRadius = 0.05;
	hkpShape* fixedSurfaceShape = new hkpBoxShape(hkVector4(sizeX, sizeY, sizeZ), hkConvexShapeRadius);

	// Create rigid body information
	hkpRigidBodyCinfo	rigidBodyInfo;

	// Attach the static motion to the game scene
	rigidBodyInfo.m_mass		= 0.0;
	rigidBodyInfo.m_shape		= fixedSurfaceShape;
	rigidBodyInfo.m_motionType	= hkpMotion::MOTION_FIXED;
	rigidBodyInfo.m_position	= hkVector4(posX, posY, posZ);
	rigidBodyInfo.m_rotation	= hkQuaternion(rotX, rotY, rotZ, rotW);

	// Create new rigid body with this info
	hkpRigidBody* newRigidBody = new hkpRigidBody(rigidBodyInfo);

	// Add the rigid body to the Physics World
	m_HavokWorld->getWorld()->lock();
	m_HavokWorld->getWorld()->addEntity(newRigidBody);
	m_EntityList[newRigidBody->getUid()] = newRigidBody;

	// The shape is stored in rigid body, so don't need this reference anymore
	fixedSurfaceShape->removeReference();
	m_HavokWorld->getWorld()->unlock();
	return newRigidBody;
}
void HavokManager::AddFixedConstraint(int BodyID,int TargetID,hkVector4* bodyConnectPt, hkVector4* targetConnectPt, bool stablize)
{
	HK_DISPLAY_LINE( m_EntityList[BodyID]->getPosition(), m_EntityList[TargetID]->getPosition(), hkColor::GREEN );

	//Connect point one
		hkVector4 pt;
		pt.setAdd(m_EntityList[BodyID]->getPosition(), *bodyConnectPt);
		
		// Move pivot to corner of cube
		hkTransform pivot1;
		pivot1.setIdentity();
		pivot1.setTranslation(pt);
		//Debug to show where the connection point actually is on the body
		AddConstraintViewer(&pivot1);

	//Connect point two
		hkVector4 pt2;
		pt2.setAdd(m_EntityList[TargetID]->getPosition(), *targetConnectPt);

		hkTransform pivot2;
		pivot2.setIdentity();
		pivot2.setTranslation(pt2);
		AddConstraintViewer(&pivot2);



		hkpFixedConstraintData* data = new hkpFixedConstraintData(); 
		//data->setInBodySpace(pivot1,pivot2);
		data->setInWorldSpace(m_EntityList[BodyID]->getTransform(),m_EntityList[TargetID]->getTransform(), pivot2);
		// Limit the force
		const hkReal maxImpulse = 50.0f;
		data->setMaximumLinearImpulse(maxImpulse);
		data->setMaximumAngularImpulse(maxImpulse);
		data->setSolvingMethod(hkpConstraintAtom::METHOD_STABILIZED);

		hkpConstraintInstance* constraint = new hkpConstraintInstance(m_EntityList[BodyID], m_EntityList[TargetID], data);
		m_HavokWorld->getWorld()->lock();	
		m_HavokWorld->getWorld()->addConstraint( constraint); 
		m_ConstraintList[BodyID][TargetID] = constraint;

		data->removeReference();
		constraint->removeReference();
		m_HavokWorld->getWorld()->unlock();

	// Enable stabilized solver and pre-stabilize inertias
	if(stablize)
	{
		hkpConstraintStabilizationUtil::setConstraintsSolvingMethod(m_HavokWorld->getWorld(), hkpConstraintAtom::METHOD_STABILIZED);
		hkpConstraintStabilizationUtil::stabilizePhysicsWorldInertias(m_HavokWorld->getWorld());
	}
}
void HavokManager::AddConstraintViewer(hkTransform* transform)
{
  //Removed Due to using Trigger Volumes to display contraints
  //This was conflicting with ID's and trigger call backs
}
void HavokManager::AddConstraintViewer(hkVector4* position)
{
  //Removed Due to using Trigger Volumes to display contraints
  //This was conflicting with ID's and trigger call backs
}
void HavokManager::AddBallSocketConstraint(int BodyID, int TargetID,hkVector4* bodyConnectPt, hkVector4* targetConnectPt)
{
    //Connection point one
    hkVector4 pt;
    pt.setAdd(m_EntityList[BodyID]->getPosition(), *bodyConnectPt);		
    AddConstraintViewer(&pt);

    //Connect point two
    hkVector4 pt2;
    pt2.setAdd(m_EntityList[TargetID]->getPosition(), *targetConnectPt);
    AddConstraintViewer( &pt2 );


	hkpBallAndSocketConstraintData* data2 = new hkpBallAndSocketConstraintData(); 
	data2->setInBodySpace(pt,pt2);
    data2->setMaximumAngularImpulse(hkSimdReal_PiOver2);
    data2->setMaximumLinearImpulse(hkSimdReal_PiOver2);
	data2->setSolvingMethod(hkpConstraintAtom::METHOD_STABILIZED);

	hkpConstraintInstance* constraint2 = new hkpConstraintInstance(m_EntityList[BodyID],m_EntityList[TargetID], data2);
	m_HavokWorld->getWorld()->lock();	
	m_HavokWorld->getWorld()->addConstraint( constraint2); 	
	m_ConstraintList[BodyID][TargetID] = constraint2;
	data2->removeReference();
	constraint2->removeReference();
	m_HavokWorld->getWorld()->unlock();

	// Enable stabilized solver and pre-stabilize inertias
	//hkpConstraintStabilizationUtil::setConstraintsSolvingMethod(m_HavokWorld->getWorld(), hkpConstraintAtom::METHOD_STABILIZED);
	//hkpConstraintStabilizationUtil::stabilizePhysicsWorldInertias(m_HavokWorld->getWorld());
}
void HavokManager::AddSpringConstraint(int BodyID,int TargetID,hkVector4* bodyConnectPt, hkVector4* targetConnectPt)
{
		hkpPrismaticConstraintData* pris = new hkpPrismaticConstraintData(); 

		// Create constraint
		hkVector4 axis(0.0f, 1.0f, 0.0f);
		pris->setInWorldSpace(m_EntityList[BodyID]->getTransform(),m_EntityList[TargetID]->getTransform(), m_EntityList[BodyID]->getPosition(),axis);
		pris->setMaxLinearLimit(3.0f);
		pris->setMinLinearLimit(-3.0f);
				
		// create

		hkpSpringDamperConstraintMotor* spring = new hkpSpringDamperConstraintMotor( 100.0f,5.0f );
		pris->setMotor( spring );
		spring->removeReference();

	
		hkpConstraintInstance* constraint = new hkpConstraintInstance( m_EntityList[BodyID],m_EntityList[TargetID], pris );
		m_HavokWorld->getWorld()->lock();
		m_HavokWorld->getWorld()->addConstraint(constraint);
		m_ConstraintList[BodyID][TargetID] = constraint;
		pris->setMotorActive(constraint, true);
		constraint->removeReference();
		pris->removeReference();
		m_HavokWorld->getWorld()->unlock();

}
void HavokManager::AddLimitedHingeConstraint(int BodyID, int TargetID, hkVector4* axis, hkReal minAngle, hkReal maxAngle)
{
	hkpLimitedHingeConstraintData* hinge = new hkpLimitedHingeConstraintData();
	hinge->setInWorldSpace(m_EntityList[BodyID]->getTransform(), m_EntityList[TargetID]->getTransform(), m_EntityList[BodyID]->getPosition(), *axis);
	hinge->setSolvingMethod( hkpConstraintAtom::METHOD_STABILIZED );
	hinge->setMinAngularLimit(minAngle);	//-HK_REAL_PI/2.f);
	hinge->setMaxAngularLimit(maxAngle);	//HK_REAL_PI/2.f);

	hkpConstraintInstance* constraint = new hkpConstraintInstance(m_EntityList[BodyID], m_EntityList[TargetID], hinge);
	m_HavokWorld->getWorld()->lock();
	m_HavokWorld->getWorld()->addConstraint(constraint);
	m_ConstraintList[BodyID][TargetID] = constraint;
	constraint->removeReference();
	hinge->removeReference();
	m_HavokWorld->getWorld()->unlock();
}

// Add Force
void HavokManager::ApplyForce(int id, float dt, float forceX, float forceY, float forceZ)
{
	m_HavokWorld->getWorld()->lock();

	m_EntityList[id]->applyForce(dt, hkVector4(forceX, forceY, forceZ));
	m_HavokWorld->getWorld()->unlock();
}
// Set Gravity Factor
void HavokManager::SetGravityFactor(int id)
{
	m_HavokWorld->getWorld()->lock();
    m_EntityList[id]->setGravityFactor(0);
	m_HavokWorld->getWorld()->unlock();
}
void HavokManager::GoForward(int id, float dt,float speed)
{	
	m_HavokWorld->getWorld()->lock();		
	scratchVec = FindEntity(id)->getTransform().getRotation().getColumn(2);
	hkSimdRealParameter param = speed;
	scratchVec.mul(param);
	m_HavokWorld->getWorld()->unlock();
	ApplyForce(id,dt,scratchVec.getComponent(0),scratchVec.getComponent(1),scratchVec.getComponent(2));
}
void HavokManager::GoForward(int id, float dt,float speed, float pointX, float pointY, float pointZ)
{	
	m_HavokWorld->getWorld()->lock();		
	scratchVec = FindEntity(id)->getTransform().getRotation().getColumn(2);
	hkSimdRealParameter param = speed;
	scratchVec.mul(param);
	m_HavokWorld->getWorld()->unlock();
	ApplyForce(id,dt,0,0,scratchVec.getComponent(2),pointX,pointY,pointZ);
}
void HavokManager::ApplyForce(int id, float dt, float forceX, float forceY, float forceZ, float pointX, float pointY, float pointZ)
{
	hkVector4 pt;
	pt.setAdd(m_EntityList[id]->getPosition(), hkVector4(pointX, pointY, pointZ));
	m_HavokWorld->getWorld()->lock();
	m_EntityList[id]->applyForce(dt, hkVector4(forceX, forceY, forceZ), pt);
	m_HavokWorld->getWorld()->unlock();
}
hkpRigidBody* HavokManager::CreateTriggerVolume(int id,TriggerType type, float posX, float posY, float posZ,
									  float sizeX, float sizeY, float sizeZ)
{
	hkVector4 start(0.0f, 0.0f, 0.0f);
	hkVector4 end(0.0f, 100.0f, 0.0f);
	// Create default rotation
	hkReal angle = 0.0;
	hkVector4 axis = hkVector4(0.0, 0.0, 1.0);
	hkQuaternion rot(axis, angle);

	return CreateTriggerVolume(id,type, posX, posY,  posZ,sizeX,  sizeY,  sizeZ,
							rot.getImag().getComponent(0), rot.getImag().getComponent(1), rot.getImag().getComponent(2), rot.getReal());
}
hkpRigidBody* HavokManager::CreateTriggerVolume(int id,TriggerType type, float posX, float posY, float posZ,
						float sizeX, float sizeY, float sizeZ,
						float rotX, float rotY, float rotZ, float rotW)
{



	
		// Create the trigger volume.
		hkpRigidBodyCinfo boxInfo;
		hkVector4 boxSize( sizeX, sizeY , sizeZ );
		hkpShape* boxShape = hkpShapeGenerator::createConvexVerticesBox( boxSize );
		boxInfo.m_motionType = hkpMotion::MOTION_FIXED;

		boxInfo.m_shape = boxShape;
		boxInfo.m_position.set(posX, posY, posZ);
		// Create fixed box
        m_HavokWorld->getWorld()->lock();
		hkpRigidBody* box = new hkpRigidBody( boxInfo );
		m_HavokWorld->getWorld()->addEntity( box );
		boxShape->removeReference();

		// Make the box semi-transparent.
		HK_SET_OBJECT_COLOR((hkUlong) box->getCollidable(), HK_TRIGGER_VOLUME_DEBUG_COLOR );
		// Convert the box into a trigger volume.

        BaseTriggerVolume* temp;
        switch (type)

            {
            case _Race:
                {
                		temp = new RaceProgress( box ) ;
                        break;
                }              
            case _Lap:
                {
                		temp = new LapVolume( box ) ;
                        break;
                }
            case _Damage:
                {
                		temp = new DamageVolume( box ) ;
                        break;
                }
            case _Start:
                {
                		temp = new StartVolume( box ) ;
                        break;
                }
            case _Finish:
                {
                		temp = new FinishVolume( box ) ;
                        break;
                }
            case _Force:
                {
                		temp = new ForceVolume( box ) ;
                        break;
                }
            case _Explosion:
                {
                		temp = new ExplosionVolume( box ) ;
                        break;
                }
            case _Crush:
                {
                		temp = new CrushVolume( box ) ;
                        break;
                }
            }

		( temp )->removeReference();
        m_vTriggerVolumes.push_back(temp);
		m_TriggerVolumes[box->getUid()] = temp;		// Add the rigid body to our list

		box->removeReference();
		m_HavokWorld->getWorld()->unlock();

	return box;
}
hkpRigidBody* HavokManager::CreateMovingTriggerVolume(int id,TriggerType type, float posX, float posY, float posZ, float sizeX, float sizeY, float sizeZ, float massKg)
{
		// Create the trigger volume.
		hkpRigidBodyCinfo boxInfo;
		hkVector4 boxSize( sizeX, sizeY , sizeZ );
		hkpShape* boxShape = hkpShapeGenerator::createConvexVerticesBox( boxSize );
		boxInfo.m_motionType = hkpMotion::MOTION_DYNAMIC;

		boxInfo.m_shape = boxShape;
		boxInfo.m_position.set(posX, posY, posZ);
		
		// Compute the inertia tensor from the shape
		hkMassProperties massProperties;

		hkpInertiaTensorComputer::computeShapeVolumeMassProperties(boxShape, massKg, massProperties);

		// Set the rigid body properties
		boxInfo.m_position		= hkVector4(posX, posY, posZ);
		boxInfo.m_mass			= massProperties.m_mass;
		boxInfo.m_centerOfMass	= massProperties.m_centerOfMass;
		boxInfo.m_inertiaTensor	= massProperties.m_inertiaTensor;
		boxInfo.m_shape			= boxShape;
		boxInfo.m_motionType	= hkpMotion::MOTION_BOX_INERTIA;
		boxInfo.m_friction = 0.5f;
		//SEVERE HACK-SHOULDNT HAVE TO SET GRAVITY THIS HIGH
		//DONE TO KEEP TURBINE FROM GOING CRAZY
		//boxInfo.m_gravityFactor = 0.001f;
		//boxInfo.m_friction = 1;

		// Create fixed box
        m_HavokWorld->getWorld()->lock();
		hkpRigidBody* box = new hkpRigidBody( boxInfo );
		m_HavokWorld->getWorld()->addEntity( box );
		boxShape->removeReference();

		// Make the box semi-transparent.
		HK_SET_OBJECT_COLOR((hkUlong) box->getCollidable(), HK_TRIGGER_VOLUME_DEBUG_COLOR );
		// Convert the box into a trigger volume.

        BaseTriggerVolume* temp;
        switch (type)

            {
            case _Race:
                {
                		temp = new RaceProgress( box ) ;
                        break;
                }              
            case _Lap:
                {
                		temp = new LapVolume( box ) ;
                        break;
                }
            case _Damage:
                {
                		temp = new DamageVolume( box ) ;
                        break;
                }
            case _Start:
                {
                		temp = new StartVolume( box ) ;
                        break;
                }
            case _Finish:
                {
                		temp = new FinishVolume( box ) ;
                        break;
                }
            case _Force:
                {
                		temp = new ForceVolume( box ) ;
                        break;
                }
            case _Explosion:
                {
                		temp = new ExplosionVolume( box ) ;
                        break;
                }
            case _Crush:
                {
                		temp = new CrushVolume( box ) ;
                        break;
                }
            }

		( temp )->removeReference();
        m_vTriggerVolumes.push_back(temp);
		m_TriggerVolumes[box->getUid()] = temp;		// Add the rigid body to our list
		m_EntityList[box->getUid()] = box;
		box->removeReference();
		m_HavokWorld->getWorld()->unlock();

	return box;
}
void HavokManager::Update(float dt)
{
	m_HavokWorld->stepSimulation(dt);
}

void HavokManager::CreateTerrain(Vector3 vertList[], WORD indexList[], Vector2 dimentions, Vector2 size, MySampledHeightFieldShape** outShape, hkpRigidBody** outBody)
{
	//  //// ------------Setup terrain Physics--------------
	HM->m_HavokWorld->getWorld()->lock();

	// Here we create an array of shorts that our heightfield will lookup
	// This array could easily have been loaded from a texture.
	const int xRes = dimentions.x-1;
	const int zRes = dimentions.y-1;
	hkReal* m_heightData = hkAllocate<hkReal>(xRes * zRes, HK_MEMORY_CLASS_DEMO);
	{
			for (int j = 0; j < xRes; j++)
			{
				int i = 0;
				 for(int k = zRes-1; k >= 0; k--)
				 {
					 int index = j*zRes + k;
					 float height = vertList[indexList[index*6]].y;
					 
					 m_heightData[j*zRes + i] = static_cast<hkReal>  ( hkUint16(-1) * (height) );
					 i++;
				 }
			}

	}

	// Finally, we want our new heightfield. It will be a fixed body with 
	// the shape that we have defined at the start. Our heightfield above 
	// can cope with any resolution, but we will say that it is a 64x64
	// grid.
	
	hkpSampledHeightFieldBaseCinfo ci;
	ci.m_xRes = xRes;
	ci.m_zRes = zRes;
	ci.m_scale.mul(hkVector4((float)size.x/((float)xRes-0.5f), 1.0f, (float)size.y/((float)zRes-0.5f)));
	MySampledHeightFieldShape* heightField = new MySampledHeightFieldShape( ci , m_heightData);
		
	// Now that we have a shape all we need is the fixed body to represent it in the 
	// the simulation. Standard rigid body setup.
	
	hkpRigidBodyCinfo rci;
	rci.m_motionType = hkpMotion::MOTION_FIXED;
	rci.m_rotation.setAxisAngle(hkVector4(0.0f, 1.0f, 0.0f), -1.57);
	hkVector4 toCenter;
	toCenter.setRotatedDir(rci.m_rotation, heightField->m_extents);
	rci.m_position.setMul4( -0.5f, toCenter ); // center the heightfield
	rci.m_position.add(hkVector4(hkReal(-0.5)*ci.m_scale.getComponent(0), 0.0f, hkReal(0.5)*ci.m_scale.getComponent(2)));
	rci.m_shape = heightField;
	rci.m_collisionFilterInfo = hkpGroupFilter::calcFilterInfo(3);	// Set Terrain to collision group 3
	//rci.m_centerOfMass = (hkVector4(sizeX/ 2, 0, sizeZ/ 2));
	rci.m_friction = 0.2f;
	hkpRigidBody* body = new hkpRigidBody( rci );
	
	HM->m_HavokWorld->getWorld()->addEntity(body);
	//// Just need to remove the reference we hold to the shape, and that is it.
	//mHeightField->removeReference();

	HM->m_HavokWorld->getWorld()->unlock();

	m_EntityList[body->getUid()] = body;		// Add the rigid body to our list

	(*outShape) = heightField;
	(*outBody) = body;
}
