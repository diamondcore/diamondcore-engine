#pragma once
#include "Vector3.h"
// Math and Base includes
#include <Common/Base/hkBase.h>
#include <Common/Base/System/hkBaseSystem.h>
#include <Common/Base/System/Error/hkDefaultError.h>
#include <Common/Base/Memory/System/Util/hkMemoryInitUtil.h>
#include <Common/Base/Monitor/hkMonitorStream.h>
#include <Common/Base/Memory/System/hkMemorySystem.h>
#include <Common/Base/Memory/Allocator/Malloc/hkMallocAllocator.h>

/////////////////////////////////////////////////////////////////////////////////////////////// Dynamics includes
#include <Physics/Collide/hkpCollide.h>	
#include <Physics/Collide/Agent/ConvexAgent/SphereBox/hkpSphereBoxAgent.h>	
#include <Physics/Collide/Agent/ConvexAgent/BoxBox/hkpBoxBoxAgent.h>
#include <Physics/Collide/Shape/Convex/Capsule/hkpCapsuleShape.h>
#include <Physics/Collide/Shape/Convex/Box/hkpBoxShape.h>					
#include <Physics/Collide/Shape/Convex/Sphere/hkpSphereShape.h>				
#include <Physics/Collide/Dispatch/hkpAgentRegisterUtil.h>					
#include <Physics/Collide/Agent/hkpProcessCollisionOutput.h>
#include <Common/Base/Math/Matrix/hkMatrix3Util.h>
#include <Physics/Collide/Query/CastUtil/hkpWorldRayCastInput.h>			
#include <Physics/Collide/Query/CastUtil/hkpWorldRayCastOutput.h>			
#include <Common/Base/Algorithm/PseudoRandom/hkPseudoRandomGenerator.h>
#include <Physics/Dynamics/Constraint/Bilateral/BallAndSocket/hkpBallAndSocketConstraintData.h>
#include <Physics/Dynamics/Constraint/Bilateral/Fixed/hkpFixedConstraintData.h>
#include <Physics/Dynamics/Constraint/hkpConstraintInstance.h>
#include <Physics/Dynamics/Constraint/Util/hkpConstraintStabilizationUtil.h>
#include <Physics/Dynamics/Constraint/Bilateral/Prismatic/hkpPrismaticConstraintData.h>
#include <Physics/Dynamics/Constraint/Motor/SpringDamper/hkpSpringDamperConstraintMotor.h>
#include <Physics/Dynamics/Constraint/Motor/Callback/hkpCallbackConstraintMotor.h>
#include <Physics/Utilities/Actions/Spring/hkpSpringAction.h>
#include <Physics/ConstraintSolver/Constraint/hkpConstraintQueryIn.h>
#include <Physics/ConstraintSolver/Constraint/Motor/hkpMotorConstraintInfo.h>

// Trigger Volumes Includes
#include <Physics/Collide/Shape/Misc/PhantomCallback/hkpPhantomCallbackShape.h>
#include <Physics/Collide/Shape/Misc/Bv/hkpBvShape.h>
#include <Physics/Utilities/Collide/TriggerVolume/hkpTriggerVolume.h>
#include <Physics/Utilities/Collide/hkpShapeGenerator.h>
#include <Physics/Collide/Shape/Compound/Collection/ExtendedMeshShape/hkpExtendedMeshShape.h>
#include <Physics/Collide/Shape/Compound/Tree/Mopp/hkpMoppBvTreeShape.h>
#include <Common/Visualize/hkDebugDisplay.h>
#include <Physics/Collide/Shape/HeightField/SampledHeightField/hkpSampledHeightFieldShape.h>
#include <Physics/Collide/Shape/HeightField/SampledHeightField/hkpSampledHeightFieldBaseCinfo.h>



#include <Physics/Dynamics/World/hkpWorld.h>								
#include <Physics/Dynamics/Entity/hkpRigidBody.h>							
#include <Physics/Utilities/Dynamics/Inertia/hkpInertiaTensorComputer.h>	

#include <Common/Base/Thread/Job/ThreadPool/Cpu/hkCpuJobThreadPool.h>
#include <Common/Base/Thread/Job/ThreadPool/Spu/hkSpuJobThreadPool.h>
#include <Common/Base/Thread/JobQueue/hkJobQueue.h>

/////////////////////////////////////////////////////////////////////////////////////////////// Visual Debugger includes
#include <Common/Visualize/hkVisualDebugger.h>
#include <Physics/Utilities/VisualDebugger/hkpPhysicsContext.h>		
#include <Physics/Utilities/VisualDebugger/Viewer/Dynamics/hkpConstraintViewer.h>

class hkpMoppBvTreeShape;
#include <Physics/Collide/Shape/Compound/Collection/Mesh/hkpMeshMaterial.h>
#include <Physics/Collide/Shape/Compound/Tree/Mopp/hkpMoppUtility.h>
#include <Physics/Internal/Collide/Mopp/Code/hkpMoppCode.h>
#include <Physics/Collide/Util/hkpTriangleUtil.h>
class hkpExtendedMeshShape;
class hkpMoppCode;
class hkpMoppBvTreeShape;
class DestroyAllMopps;

#include <stdio.h>
static void HK_CALL errorReport(const char* msg, void*)
{
	printf("%s", msg);
}


/***********************************************************
					How To Use:
// Create instance ov the HavokUtility
HavokUtility* havokUtil = new HavokUtility();

// Register the Havok Visual Debugger (optional)
havokUtil->registerVisualDebugger();

// Create the physical world with getWorld() to create objects

// Step through the simulation
havokUtil->stepSimulation(timestep);
havokUtil->stepVisualDebugger(timestep);

// Delete HavokUtility and release all Havok objects
delete havokUtil;
************************************************************/
class HavokUtility
{
public:
	HavokUtility(bool visualDebuggerActive);
	~HavokUtility();

	void	registerVisualDebugger();

	void	stepSimulation(float dt);

	hkpWorld*			getWorld();
	hkVisualDebugger*	getVisualDebugger();

public:
	void	initHavok();
	void	shutdownHavok();
	
	// Havok Memory Variables
	hkMemoryRouter*			m_pMemoryRouter;
	hkHardwareInfo			m_HardwareInfo;
	int						m_iTotalNumThreadsUsed;
	hkCpuJobThreadPoolCinfo	m_threadPoolCinfo;
	hkJobThreadPool*		m_pThreadPool;
	hkJobQueueCinfo			m_jobQueInfo;
	hkJobQueue*				m_pJobQueue;

	// Havok World controllers
	hkpWorldCinfo	m_WorldInfo;
	hkpWorld*		m_pPhysicsWorld;
	// Havok Visual Debugger
	bool						m_bVisualDebuggerActive;
	hkVisualDebugger*			m_pVisualDebugger;
	hkpPhysicsContext*			m_pPhysicsContext;
	void	stepVisualDebugger(float dt);

public:
	// Example functions
	void	addFixedSurface(const hkVector4& position, const hkVector4& dimentions);
	void	addMovingBoxes(const hkVector4& position, const hkVector4& dimentions);
	void	addMovingSphere(const hkVector4& position, const hkReal& radius);
};