#pragma once
#include "HavokManager.h"
#include <map>



// base trigger volume class - pure virtual
class BaseTriggerVolume : public hkpTriggerVolume
{
	public:
		HK_DECLARE_CLASS_ALLOCATOR( HK_MEMORY_CLASS_DEMO );

		BaseTriggerVolume( hkpRigidBody* triggerBody ) : hkpTriggerVolume( triggerBody ){}

		//Call back we do not want to Override
		using hkpTriggerVolume::triggerEventCallback;
		//Call back we are overriding and adding our own information
		virtual void triggerEventCallback( hkpRigidBody* body, EventType type )= 0;
		//List of bodies that have collided with this trigger volume to process later
		std::vector<int> m_Colliders;

        //Type in Base Just in case
        TriggerType m_type;
		//Defines whether to clear after an update
		bool	m_bShouldClear;
		void*   m_pParam;       // Optional paramater   
};
class RaceProgress : public BaseTriggerVolume
{
public:
	RaceProgress(hkpRigidBody* triggerBody);

	virtual void triggerEventCallback( hkpRigidBody* body, EventType type );
};
class LapVolume : public BaseTriggerVolume
{
public:
	LapVolume(hkpRigidBody* triggerBody);

	virtual void triggerEventCallback( hkpRigidBody* body, EventType type );
};
class DamageVolume : public BaseTriggerVolume
{
public:
	DamageVolume(hkpRigidBody* triggerBody);

	virtual void triggerEventCallback( hkpRigidBody* body, EventType type );
};
class StartVolume : public BaseTriggerVolume
{
public:
	StartVolume(hkpRigidBody* triggerBody);

	virtual void triggerEventCallback( hkpRigidBody* body, EventType type );
};
class FinishVolume : public BaseTriggerVolume
{
public:
	FinishVolume(hkpRigidBody* triggerBody);

	virtual void triggerEventCallback( hkpRigidBody* body, EventType type );
};
class ForceVolume : public BaseTriggerVolume
{
public:
	ForceVolume(hkpRigidBody* triggerBody);

	float	m_fForce;
	bool    m_bActive;

	virtual void triggerEventCallback( hkpRigidBody* body, EventType type );
};
class ExplosionVolume : public BaseTriggerVolume
{
public:
	ExplosionVolume(hkpRigidBody* triggerBody);

	virtual void triggerEventCallback( hkpRigidBody* body, EventType type );
};
class CrushVolume : public BaseTriggerVolume
{
public:
	CrushVolume(hkpRigidBody* triggerBody);

	virtual void triggerEventCallback( hkpRigidBody* body, EventType type );
};


