#include "TriggerVolumes.h"

RaceProgress::RaceProgress(hkpRigidBody* triggerBody)
	:BaseTriggerVolume(triggerBody)
{
    m_type = _Race;
	m_bShouldClear = true;
}
void RaceProgress::triggerEventCallback( hkpRigidBody* body, EventType type )
{
	if ( type & ENTERED_EVENT )
	{
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(255, 0, 0));	

	}
    if ( type & LEFT_EVENT )
	{
      //Add to list of bodies that have hit this trigger volume so we can do call back in gameplay
    HM->scratchInt = m_Colliders.size();
        if (HM->scratchInt > 0)
            {
              for (int i = 0; i < HM->scratchInt; i++ )
                  {
                      if ( body->getUid() == m_Colliders[i] )
                          {
                            //Already in list;
                            return;
                          }
                  }
            }

        m_Colliders.push_back(body->getUid());
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(0, 255, 0));
	}
}
LapVolume::LapVolume(hkpRigidBody* triggerBody)
	:BaseTriggerVolume(triggerBody)
{
	m_type = _Lap;
	m_bShouldClear = true;
}
void LapVolume::triggerEventCallback( hkpRigidBody* body, EventType type )
{
	if ( type & ENTERED_EVENT )
	{
		//Add to list of bodies that have hit this trigger volume so we can do call back in gameplsy
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(255, 0, 0));	

	}
	if ( type & LEFT_EVENT )
	{
      //Add to list of bodies that have hit this trigger volume so we can do call back in gameplay
     HM->scratchInt = m_Colliders.size();
        if (HM->scratchInt > 0)
            {
              for (int i = 0; i < HM->scratchInt; i++ )
                  {
                      if ( body->getUid() == m_Colliders[i] )
                          {
                            //Already in list;
                            return;
                          }
                  }
            }

        m_Colliders.push_back(body->getUid());
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(0, 255, 0));
	}
}
DamageVolume::DamageVolume(hkpRigidBody* triggerBody)
	:BaseTriggerVolume(triggerBody)
{
    m_type = _Damage;
	m_bShouldClear = true;
}
void DamageVolume::triggerEventCallback( hkpRigidBody* body, EventType type )
{
	if ( type & LEFT_EVENT )
	{
		//Add to list of bodies that have hit this trigger volume so we can do call back in gameplsy
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(255, 0, 0));	

	}
	if ( type & ENTERED_EVENT )
	{
      //Add to list of bodies that have hit this trigger volume so we can do call back in gameplay
     HM->scratchInt = m_Colliders.size();
        if (HM->scratchInt > 0)
            {
              for (int i = 0; i < HM->scratchInt; i++ )
                  {
                      if ( body->getUid() == m_Colliders[i] )
                          {
                            //Already in list;
                            return;
                          }
                  }
            }

        m_Colliders.push_back(body->getUid());
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(0, 255, 0));
	}
}
StartVolume::StartVolume(hkpRigidBody* triggerBody)
	:BaseTriggerVolume(triggerBody)
{
    m_type = _Start;
	m_bShouldClear = true;
}
void StartVolume::triggerEventCallback( hkpRigidBody* body, EventType type )
{
	if ( type & ENTERED_EVENT )
	{
		//Add to list of bodies that have hit this trigger volume so we can do call back in gameplsy
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(255, 0, 0));	

	}
	if ( type & LEFT_EVENT )
	{
      //Add to list of bodies that have hit this trigger volume so we can do call back in gameplay
     HM->scratchInt = m_Colliders.size();
        if (HM->scratchInt > 0)
            {
              for (int i = 0; i < HM->scratchInt; i++ )
                  {
                      if ( body->getUid() == m_Colliders[i] )
                          {
                            //Already in list;
                            return;
                          }
                  }
            }

        m_Colliders.push_back(body->getUid());
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(0, 255, 0));
	}
}
FinishVolume::FinishVolume(hkpRigidBody* triggerBody)
	:BaseTriggerVolume(triggerBody)
{
    m_type = _Finish;
	m_bShouldClear = true;
}
void FinishVolume::triggerEventCallback( hkpRigidBody* body, EventType type )
{
	if ( type & ENTERED_EVENT )
	{
		//Add to list of bodies that have hit this trigger volume so we can do call back in gameplsy
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(255, 0, 0));	

	}
	if ( type & LEFT_EVENT )
	{
      //Add to list of bodies that have hit this trigger volume so we can do call back in gameplay
     HM->scratchInt = m_Colliders.size();
        if (HM->scratchInt > 0)
            {
              for (int i = 0; i < HM->scratchInt; i++ )
                  {
                      if ( body->getUid() == m_Colliders[i] )
                          {
                            //Already in list;
                            return;
                          }
                  }
            }

        m_Colliders.push_back(body->getUid());
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(0, 255, 0));
	}
}
ForceVolume::ForceVolume(hkpRigidBody* triggerBody)
	:BaseTriggerVolume(triggerBody)
{
    m_type = _Force;
	m_bShouldClear = false;
	m_fForce = 0.0f;
	m_pParam = &m_fForce;
	m_bActive = true;
}
void ForceVolume::triggerEventCallback( hkpRigidBody* body, EventType type )
{
	if ( type & LEFT_EVENT )
	{
		// Remove entity as they leave the trigger volume
		 HM->scratchInt = m_Colliders.size();
        if (HM->scratchInt > 0)
            {
              for (int i = 0; i < HM->scratchInt; i++ )
                  {
                      if ( body->getUid() == m_Colliders[i] )
                          {
                            //Remove from list;
							m_Colliders.erase( m_Colliders.begin() + i );
                            
							//Reset color
							HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(127, 127, 127));	
							return;
                          }
                  }
            }

		//Add to list of bodies that have hit this trigger volume so we can do call back in gameplay
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(0, 255, 0));	

	}
	if ( type & ENTERED_EVENT )
	{
		// Add entity as they enter the trigger volume
		HM->scratchInt = m_Colliders.size();
        if (HM->scratchInt > 0)
            {
              for (int i = 0; i < HM->scratchInt; i++ )
                  {
                      if ( body->getUid() == m_Colliders[i] )
                          {
                            //Already in list;
                            return;
                          }
                  }
            }

        m_Colliders.push_back(body->getUid());

      //Add to list of bodies that have hit this trigger volume so we can do call back in gameplay
	  HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(255, 0, 0));
	}

}
ExplosionVolume::ExplosionVolume(hkpRigidBody* triggerBody)
	:BaseTriggerVolume(triggerBody)
{
    m_type = _Explosion;
	m_bShouldClear = true;
}
void ExplosionVolume::triggerEventCallback( hkpRigidBody* body, EventType type )
{
	if ( type & LEFT_EVENT )
	{
		//Add to list of bodies that have hit this trigger volume so we can do call back in gameplay
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(255, 0, 0));	

	}
	if ( type & ENTERED_EVENT )
	{
      //Add to list of bodies that have hit this trigger volume so we can do call back in gameplay
     HM->scratchInt = m_Colliders.size();
        if (HM->scratchInt > 0)
            {
              for (int i = 0; i < HM->scratchInt; i++ )
                  {
                      if ( body->getUid() == m_Colliders[i] )
                          {
                            //Already in list;
                            return;
                          }
                  }
            }

        m_Colliders.push_back(body->getUid());
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(0, 255, 0));
	}
}
CrushVolume::CrushVolume(hkpRigidBody* triggerBody)
	:BaseTriggerVolume(triggerBody)
{
    m_type = _Crush;
	m_bShouldClear = true;
}
void CrushVolume::triggerEventCallback( hkpRigidBody* body, EventType type )
{
	if ( type & LEFT_EVENT )
	{
		//Add to list of bodies that have hit this trigger volume so we can do call back in gameplsy
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(255, 0, 0));	

	}
	if ( type & ENTERED_EVENT )
	{
      //Add to list of bodies that have hit this trigger volume so we can do call back in gameplay
     HM->scratchInt = m_Colliders.size();
        if (HM->scratchInt > 0)
            {
              for (int i = 0; i < HM->scratchInt; i++ )
                  {
                      if ( body->getUid() == m_Colliders[i] )
                          {
                            //Already in list;
                            return;
                          }
                  }
            }

        m_Colliders.push_back(body->getUid());
		HK_SET_OBJECT_COLOR((hkUlong)body->getCollidable(), hkColor::rgbFromChars(0, 255, 0));
	}
}
