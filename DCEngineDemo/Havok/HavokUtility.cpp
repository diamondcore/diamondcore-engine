// Creates characters that Havok Engine uses
#include <Common/Base/KeyCode.cxx>

// This disables animation headers (will cause errors if uncommented)
#ifdef HK_FEATURE_PRODUCT_ANIMATION
#undef HK_FEATURE_PRODUCT_ANIMATION
#endif

// This is used for some memory linkings at compile time
#ifndef HK_EXCLUDE_LIBRARY_hkgpConvexDecomposition
#define HK_EXCLUDE_LIBRARY_hkgpConvexDecomposition
#endif

// This is for linking hkBase.lib
#include <Common/Base/Config/hkProductFeatures.cxx>

// These lines are used to help get rid of some linker problems for hkpCollide.lib
#include <Physics/Collide/hkpCollide.h>
struct hkTestEntry* hkUnitTestDatabase = HK_NULL;
hkBool HK_CALL hkTestReport(hkBool32 cond, const char* desc, const char* file, int line){	return false;	}
/******************************************************************************/
#include "HavokUtility.h"

HavokUtility::HavokUtility(bool visualDebuggerActive)
{
	// Set the Visual Debugger to on/off
	m_bVisualDebuggerActive = visualDebuggerActive;
	if(visualDebuggerActive)
	{
		#define HAVOK_VISUAL_DEBUGGER_ENABLED 1
	}
	// Initialize Havok
	initHavok();
}
HavokUtility::~HavokUtility()
{
	// Release Havok
	shutdownHavok();
}

void HavokUtility::initHavok()
{
	// Initialize Havok Memory
	m_pMemoryRouter = hkMemoryInitUtil::initDefault(hkMallocAllocator::m_defaultMallocAllocator, hkMemorySystem::FrameInfo(500*1024));
	hkBaseSystem::init( m_pMemoryRouter, errorReport );

	
	hkpConstraintViewer::m_scale = 1.0f;
	/******************************************************************************/
	// Initialize the multi-threading classes
	hkGetHardwareInfo(m_HardwareInfo);
	m_iTotalNumThreadsUsed = m_HardwareInfo.m_numThreads;

	// Use one less that number of threads, so we can use it in the simulation
	m_threadPoolCinfo.m_numThreads = m_iTotalNumThreadsUsed - 1;

	// Enable timer collection
	m_threadPoolCinfo.m_timerBufferPerThreadAllocation = 200000;	// Allocates 200 kb per thread
	m_pThreadPool = new hkCpuJobThreadPool( m_threadPoolCinfo );

	// Create the jobqueue for the physic's multi-threads
	m_jobQueInfo.m_jobQueueHwSetup.m_numCpuThreads = m_iTotalNumThreadsUsed;
	m_pJobQueue = new hkJobQueue(m_jobQueInfo);

	// Enable monitors for this thread
	hkMonitorStream::getInstance().resize(200000);

	/******************************************************************************/
	// Create the physics world

	// Set the simulation type of the world to muti-threaded
	m_WorldInfo.m_simulationType = hkpWorldCinfo::SIMULATION_TYPE_MULTITHREADED;

	// Enable automatically removing objects that go outside the world 
	m_WorldInfo.m_broadPhaseBorderBehaviour = hkpWorldCinfo::BROADPHASE_BORDER_REMOVE_ENTITY;

	// Set normal gravity, collision tollerance, and the world size
	m_WorldInfo.m_gravity.set(0, -9.8, 0);
	//m_WorldInfo.m_gravity.set(0, 0, 0);
	//m_WorldInfo.setupSolverInfo(hkpWorldCinfo::SOLVER_TYPE_8ITERS_MEDIUM);
	m_WorldInfo.m_collisionTolerance = 0.1f;
	m_WorldInfo.setBroadPhaseWorldSize(5000.0f);

	// Initialize the physics world with the set World Info
	m_pPhysicsWorld = new hkpWorld(m_WorldInfo);

	// Disable deactivation, so that you can view timers in the VDB.  Should not be used in full game.
	m_pPhysicsWorld->m_wantDeactivation = false;

	// You have to use this to locks the thread, So that we can write to the world
	m_pPhysicsWorld->markForWrite();

	// Register all collisiton agents that will be used in the world (you must register them before creating entities)
	hkpAgentRegisterUtil::registerAllAgents( m_pPhysicsWorld->getCollisionDispatcher() );

	// Register all modules that will be running multi-threaded with the job queue
	m_pPhysicsWorld->registerWithJobQueue( m_pJobQueue );

	// Once done editing the world, you must unlock the thread
	m_pPhysicsWorld->unmarkForWrite();


	/******************************************************************************/
	// Register the Visual Debugger
	#ifdef HAVOK_VISUAL_DEBUGGER_ENABLED
		registerVisualDebugger();
	#endif
}

void HavokUtility::shutdownHavok()
{
	// Clean up the Physics World
	m_pPhysicsWorld->markForWrite();
	m_pPhysicsWorld->removeReference();

	// Delete the Job Queue, thread pool, deallocate memory
	delete m_pJobQueue;
	m_pThreadPool->removeReference();

	// Clean up the Visual Debugger(if used)
	#ifdef HAVOK_VISUAL_DEBUGGER_ENABLED
		m_pVisualDebugger->removeReference();
		m_pPhysicsContext->removeReference();
	#endif

	// Deallocate the buffer used for Havok
	hkBaseSystem::quit();
	hkMemoryInitUtil::quit();
}

void HavokUtility::registerVisualDebugger()
{


	hkArray<hkProcessContext*>	m_arrayPhysicsContext;

	// Create the Context for the visual debugger
	m_pPhysicsContext = new hkpPhysicsContext();
	hkpPhysicsContext::registerAllPhysicsProcesses();	// Register all physics viewers

	// Add the Physics World so the viewers can see it
	m_pPhysicsWorld->markForWrite();
	m_pPhysicsContext->addWorld(m_pPhysicsWorld);
	m_arrayPhysicsContext.pushBack(m_pPhysicsContext);
	m_pPhysicsWorld->unmarkForWrite();

	// Create the Visual Debugger instance
	m_pVisualDebugger = new hkVisualDebugger(m_arrayPhysicsContext);
	m_pVisualDebugger->serve();

}

void HavokUtility::stepSimulation(float dt)
{
	// Step multithreaded simulation using this thread and all threads in the thread pool
	m_pPhysicsWorld->stepMultithreaded(m_pJobQueue, m_pThreadPool, dt);
	stepVisualDebugger(dt);
	hkMonitorStream::getInstance().reset();
	m_pThreadPool->clearTimerData();
}

void HavokUtility::stepVisualDebugger(float dt)
{
	#ifdef HAVOK_VISUAL_DEBUGGER_ENABLED
		// Sync the timer data and step Visual Debugger
		m_pPhysicsContext->syncTimers( m_pThreadPool );
		m_pVisualDebugger->step();
	#endif
}

hkpWorld* HavokUtility::getWorld()
{
	return m_pPhysicsWorld;
}

hkVisualDebugger* HavokUtility::getVisualDebugger()
{
	return m_pVisualDebugger;
}

// Some Havok Physics Wrapper functions
void HavokUtility::addFixedSurface(const hkVector4& position, const hkVector4& dimensions)
{
	// Create Box shape with dimensions
	hkReal hkConvexShapeRadius = 0.05;
	hkpShape* fixedSurfaceShape = new hkpBoxShape(dimensions, hkConvexShapeRadius);

	// Create rigid body information
	hkpRigidBodyCinfo	rigidBodyInfo;

	// Attach the static motion to the game scene
	rigidBodyInfo.m_mass = 0.0;
	rigidBodyInfo.m_shape = fixedSurfaceShape;
	rigidBodyInfo.m_motionType = hkpMotion::MOTION_FIXED;
	rigidBodyInfo.m_position = position;
	hkVector4 axis = hkVector4(0.0, 0.0, 1.0);
	hkReal angle = -0.1;
	rigidBodyInfo.m_rotation = hkQuaternion(axis, angle);

	// Create new rigid body with this info
	hkpRigidBody* newRigidBody = new hkpRigidBody(rigidBodyInfo);

	// Add the rigid body to the Physics World
	m_pPhysicsWorld->lock();
	m_pPhysicsWorld->addEntity(newRigidBody);

	// Decrease reference counter for the rigid body and shape
	newRigidBody->removeReference();
	fixedSurfaceShape->removeReference();
	m_pPhysicsWorld->unlock();
}

void HavokUtility::addMovingBoxes(const hkVector4& position, const hkVector4& dimensions)
{
	// Create Box shape with the dimensions
	hkReal hkConvexShapeRadius = 0.05;
	hkpShape* movingBodyShape = new hkpBoxShape(dimensions, hkConvexShapeRadius);

	// Compute the inertia tensor from the shape
	hkMassProperties massProperties;
	hkReal massOfBox = 5.0;
	hkpInertiaTensorComputer::computeShapeVolumeMassProperties(movingBodyShape, massOfBox, massProperties);

	// Create the Rigid Body information
	hkpRigidBodyCinfo rigidBodyInfo;

	// Set the rigid body properties
	rigidBodyInfo.m_position		= position;
	rigidBodyInfo.m_mass			= massProperties.m_mass;
	rigidBodyInfo.m_centerOfMass	= massProperties.m_centerOfMass;
	rigidBodyInfo.m_inertiaTensor	= massProperties.m_inertiaTensor;
	rigidBodyInfo.m_shape			= movingBodyShape;
	rigidBodyInfo.m_motionType		= hkpMotion::MOTION_BOX_INERTIA;

	// Create new rigid body with this info
	hkpRigidBody* newRigidBody = new hkpRigidBody(rigidBodyInfo);

	// Add rigid body to Physics World
	m_pPhysicsWorld->lock();
	m_pPhysicsWorld->addEntity(newRigidBody);

	// Decrease reference counter for rigid body and shape
	newRigidBody->removeReference();
	movingBodyShape->removeReference();
	m_pPhysicsWorld->unlock();
}

void HavokUtility::addMovingSphere(const hkVector4& position, const hkReal& radius)
{
	hkpSphereShape* sphere = new hkpSphereShape(radius);

	// Create the Rigid Body information
	hkpRigidBodyCinfo rigidBodyInfo;

	// Set the rigid body properties
	rigidBodyInfo.m_shape		= sphere;
	rigidBodyInfo.m_motionType	= hkpMotion::MOTION_DYNAMIC;
	hkpInertiaTensorComputer::setShapeVolumeMassProperties(sphere, 1.0f, rigidBodyInfo);
	rigidBodyInfo.m_position	= position;
	rigidBodyInfo.m_friction	= 1.0f;
	rigidBodyInfo.m_restitution	= 0.2f;
	
	// Create new rigid body with this info
	hkpRigidBody* newRigidBody = new hkpRigidBody(rigidBodyInfo);

	// Add rigid body to Physics World
	m_pPhysicsWorld->lock();
	m_pPhysicsWorld->addEntity(newRigidBody);

	// Decrease reference counter for rigid body and shape
	newRigidBody->removeReference();
	sphere->removeReference();
	m_pPhysicsWorld->unlock();
}