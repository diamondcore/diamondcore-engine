#pragma once
#include "HavokUtility.h"
#include <Physics/Collide/Filter/Group/hkpGroupFilter.h>
#include <map>
#include <vector>

enum TriggerType{_Race,_Lap,_Damage,_Start,_Finish,_Force,_Explosion,_Crush};

class MySampledHeightFieldShape: public hkpSampledHeightFieldShape
{
	public:
		

		MySampledHeightFieldShape( const hkpSampledHeightFieldBaseCinfo& ci, hkReal* data )
			:	hkpSampledHeightFieldShape(ci),
				m_data(data)
		{
		}

		// Generate a rough terrain
		HK_FORCE_INLINE hkReal getHeightAtImpl( int x, int z ) const
		{
			// Lookup data and return a float
			//

			auto v = m_data[x * m_zRes + z];
			auto v1 = hkReal(v);
			auto v2 = hkReal( hkUint16(-1) );
			auto v3 =  v1 / v2;
			return v3;
		}

		// Assuming each heightfield quad is defined as four points { 00, 01, 11, 10 },
		// this should return true if the two triangles share the edge p00-p11.
		// Otherwise it should return false if the triangles share the edge p01-p10
		HK_FORCE_INLINE hkBool getTriangleFlipImpl() const
		{	
			return false;
		}

		virtual void collideSpheres( const CollideSpheresInput& input, SphereCollisionOutput* outputArray) const
		{
			hkSampledHeightFieldShape_collideSpheres(*this, input, outputArray);
		}

	private:

		hkReal* m_data;
};
// forward declaration
class Terrain;
struct Vector2;

class BaseTriggerVolume;
class Terrain;
struct Vector2;

#define HM HavokManager::Instance()
class HavokManager
{
private:
	static std::shared_ptr<HavokManager> s_Instance;
public:
	static HavokManager* Instance();
	HavokUtility*	m_HavokWorld;
	std::map<int, hkpRigidBody*>	m_EntityList;
	// holds the constraints between two bodies
	std::map<int, std::map<int,hkpConstraintInstance*>>	m_ConstraintList;
	//Used for look up
	std::map<int, BaseTriggerVolume*> m_TriggerVolumes;
	//Will be used to process all the bodies that have been hit
	std::vector<BaseTriggerVolume*> m_vTriggerVolumes;
	// group filter
	hkpGroupFilter*	m_GroupFilter;

	HavokManager();
	~HavokManager();

	void	Init();
	void	Shutdown();
	void	Update(float dt);
	void	RemoveEntity(int id);
	void	RemoveTriggerVolume(int id);
	void	RemoveConstraint(int bodyID, int targetID);	
	hkpRigidBody* 	FindEntity(int id);

	int tempVar;
    int scratchInt;
	hkVector4 scratchVec;

	// Create Moving Box
	hkpRigidBody*		AddMovingBox(int id, float posX, float posY, float posZ, 
						float sizeX, float sizeY, float sizeZ);
	hkpRigidBody*		AddMovingBox(int id, float posX, float posY, float posZ, 
						float sizeX, float sizeY, float sizeZ, float massKg);
	hkpRigidBody*		AddMovingBox(int id, float posX, float posY, float posZ, 
						float sizeX, float sizeY, float sizeZ, float massKg, int groupID);

	// Create Moving Sphere
	hkpRigidBody*		AddMovingSphere(int id, float posX, float posY, float posZ, float radius);
	hkpRigidBody*		AddMovingSphere(int id, float posX, float posY, float posZ,
						float radius, float massKg);
	
	hkpRigidBody*		AddCapsule();

	// Create Fixed Surface
	hkpRigidBody*		AddFixedSurface(int id, float posX, float posY, float posZ,
						float sizeX, float sizeY, float sizeZ);
	hkpRigidBody*		AddFixedSurface(int id, float posX, float posY, float posZ,
						float sizeX, float sizeY, float sizeZ,
						float rotX, float rotY, float rotZ, float rotW);
	// Apply Forces
    void    SetGravityFactor(int id);
	void	ApplyForce(int id, float dt, float forceX, float forceY, float forceZ);
	void	ApplyForce(int id, float dt, float forceX, float forceY, float forceZ,
						float pointX, float pointY, float pointZ);

	void	GoForward(int id, float dt,float speed);
    void    GoForward(int id, float dt,float speed, float pointX, float pointY, float pointZ);
	// Apply Constraints
	void	AddFixedConstraint(int BodyID,int TargetID,hkVector4* bodyConnectPt, hkVector4* targetConnectPt, bool stablize = true);
	void	AddBallSocketConstraint(int BodyID, int TargetID,hkVector4* bodyConnectPt, hkVector4* targetConnectPt);
	void	AddSpringConstraint(int BodyID,int TargetID,hkVector4* bodyConnectPt, hkVector4* targetConnectPt);
	void	AddLimitedHingeConstraint(int BodyID, int TargetID, hkVector4* axis, hkReal minAngle, hkReal maxAngle);

	//Create Trigger Volumes
	hkpRigidBody*		CreateTriggerVolume(int id,TriggerType type, float posX, float posY, float posZ,
						float sizeX, float sizeY, float sizeZ);
	hkpRigidBody*		CreateTriggerVolume(int id,TriggerType type, float posX, float posY, float posZ,
						float sizeX, float sizeY, float sizeZ,
						float rotX, float rotY, float rotZ, float rotW);
	hkpRigidBody*		CreateMovingTriggerVolume(int id,TriggerType type, float posX, float posY, float posZ,
						float sizeX, float sizeY, float sizeZ, float massKg);
	// Mutator Methods

	void SetBodyPosition(hkpRigidBody* body,float x, float y, float z);
	void SetBodyOrientation(hkpRigidBody* body,float x, float y, float z);
	void RotateBody(hkpRigidBody* body,float x, float y, float z);
	void SetBodyFriction(hkpRigidBody* body, float friction);
	void SetAngularDampining(hkpRigidBody* body, float dampining);

	// Collision Groups (Should be using groups > 1)
	void AddToCollisionGroup(hkpRigidBody* body, int group);
	void DisableCosllisionsBetween(int groupA, int groupB);

	//Debug functions
	void AddConstraintViewer(hkTransform* transform);
    void AddConstraintViewer(hkVector4* position);
	//Mesh Functions
	void CreateTerrain(Vector3 vertList[], WORD indexList[], Vector2 dimentions, Vector2 size, MySampledHeightFieldShape** outShape, hkpRigidBody** outBody);
};