#include "AICoreDependencies.h"

AgentProperties::AgentProperties(int iSharedID, int iMovementID, bool bDoPathing, bool bDoPotential)
{
	m_pGraph = NULL;
	m_pSteering = NULL;
	m_iSharedID= iSharedID;
	m_bDoPathing = bDoPathing;
	m_iMovementID = iMovementID;
	m_bDoPotential = bDoPotential;
	m_pStateMachine = std::make_shared<StateMachine<AgentProperties>>(this);
	m_pStateMachine->setGlobal(AIGlobal);
	m_pStateMachine->ChangeState(AIIdle);
}

void AgentProperties::Update(float dt)
{
	m_pStateMachine->Update(dt);
}