#ifndef __INFLUENCEMAP_H
#define __INFLUENCEMAP_H

#include <map>
#include <vector>
#include <math.h>
#include <string>
#include <fstream>
#include <memory>

#include <DCMathLib.h>

#include "InfluenceMapDependants.h"


class InfluenceMap
{
protected:

	/*================VARIABLES================*/

	//map of a vector that contains the nodes for a room
	//accesed by room ID
	std::map<int, std::vector<std::shared_ptr<infNode>>> m_mMapNodeSets;
	std::map<int, std::vector<std::shared_ptr<infNode>>>::iterator m_MapSetIterator;

	std::map<UINT, std::pair<std::pair<float, std::shared_ptr<infNode>>, std::pair<float, std::shared_ptr<infNode>>>> mp_mMinMax;

	//map that contains all doors in a room & where they lead
	//reference by room ID, then door ID
	std::map<int, std::vector<std::shared_ptr<door>>> m_mDoorMap;

	//2d map system that holds the minimal travel distance to each door
	//referenced by 1st door ID, then 2nd door ID
	std::map<int, std::map<int, float>> m_mDistToDoors;

	//vector that holds our doors
	std::vector<std::shared_ptr<door>> m_vDoors;

	//map that contains our hotspots
	//reference by agentID
	std::map<int, std::shared_ptr<HotSpot>> m_mHotSpots;

	//vector that holds all our ids for our hotspots
	std::vector<int> m_vHotSpotAgentIDs;
	
	//2d map that we can use to reference an "alliance reputation" that influences how a team views the influence
	//from another team.   A positive reputation is friendly, and a negative reputation is hostile
	std::map<UINT, std::map<UINT, infReputation>> m_mmTeamReputation;

	//vector that contains the textures for influence falloff control
	std::vector<std::shared_ptr<falloffValues>> m_vFalloffMaps;

	//this variable tells us how many layers/subsets each dataset is using
	UINT m_iInfLayers;

	//variable involved with putting doors in their own rooms
	int m_iDoorRoomIDs;

	//door counter (when a door is made, its ID# is set to this variable's current value
	int m_iDoorID;

	//vector that contains the IDs of each dataset.  Used to bypass having to increment through
	//the map that contains the data
	std::vector<UINT> m_vTeamIDs;

	/*================METHODS================*/


	//adds/edits an agent's hotspot in the hotspot map
	std::shared_ptr<HotSpot> AddHotSpot(int iAgentID, UINT iTeamID, std::shared_ptr<infNode> pNode, bool &bIsNewHotSpot,
		std::vector<float> &vAmounts, std::vector<float> &vSpreads, std::vector<std::string>& vFalloffMaps);

	//checks to see if an element in a map<UINT, float> exists, without declaring a new value
	//this help keep memory usage down as the number of teams increase
	bool DoesReputationExist(std::map<UINT, infReputation> mMap, const UINT iKey);

public:
	InfluenceMap();
	//static InfluenceMap* Instance(){static InfluenceMap instance; return &instance;}
	~InfluenceMap(){Clean();}

	//sets the number of layers we are going to use
	void SetNumOfLayers(UINT iCount){m_iInfLayers = iCount;}

	//returns the number of layers we are using
	UINT GetNumOfLayers(){return m_iInfLayers;}

	//adds a node in the given room at the given position and returns a pointer to it
	std::shared_ptr<infNode> AddNode(int iRoomNum, Vector3 vPosition);

	std::vector<int> GetHotSpotIDs(){return m_vHotSpotAgentIDs;}

	//Adds a door.  Elements in vRooms tells us how many rooms it connects to.
	std::shared_ptr<infNode> AddDoor(Vector3 vPosition, const std::vector<int> &vRooms);
	int GetNumOfDoors(){return m_vDoors.size();}

	//clears current door paths and bakes new ones
	//this method MUST be called in order for multi-room calculations to work
	void BakeDoorPaths();

	//recursive algorithm that does depth-first search to find the minimum
	//required distance to the target door
	void FindMinDistToDoor(std::shared_ptr<door> pStartDoor, std::shared_ptr<door> pCurrentDoor, std::shared_ptr<door> pEndDoor, float fCurrentDistance);

	std::vector<std::shared_ptr<infNode>> FindMaxNodes(UINT iTeamNum, UINT iMapNum);
	std::vector<std::shared_ptr<infNode>> FindMinNodes(UINT iTeamNum, UINT iMapNum);

	//adds a falloff map to the given layer
	std::shared_ptr<falloffValues> GetFalloffMap(std::string sFilename, bool bDoCreate);
	void RemoveFalloffMap(std::string sFilename);

	//gets the color of a pixel in the falloff map.  Returns true if
	//the falloff texture, false if it doesnt
	//this method returns a pixel value using a range between 0 and 1 for x and y
	bool GetFalloffPointByRange(std::shared_ptr<falloffValues> pFalloff, float fX, float fY, D3DXCOLOR &color, bool bTile);

	//gets the color of a pixel in the falloff map.  Returns true if
	//the falloff texture, false if it doesnt
	//this method returns a pixel value by using x and y as a raw pixel coordinate
	bool GetFalloffPointByPixel(std::shared_ptr<falloffValues> pFalloff, float fX, float fY, D3DXCOLOR &color, bool bTile);

	//adds/edits an agent's hotspot in the hotspot map
	std::shared_ptr<HotSpot> AddHotSpot(int iAgentID, UINT iTeamID, int iRoomID, Vector3 vPosition, bool bUseNearestNode, bool &bIsNewHotSpot,
		const std::vector<float> &vAmounts, const std::vector<float> &vSpreads, const std::vector<std::string>& vFalloffMaps);
	std::shared_ptr<HotSpot> AddHotSpot(int iAgentID, UINT iTeamID, int iRoomID, Vector3 vPosition, bool bUseNearestNode, bool &bIsNewHotSpot,
		float vAmounts, float vSpreads, std::string vFalloffMaps);
	std::shared_ptr<HotSpot> AddHotSpot(int iAgentId, UINT iTeamID, int iRoomID, Vector3 vPosition, bool bUseNearestNode, bool &bIsNewHotSpot);
	//gets a specific hotspot's team ID
	UINT GetHotSpotTeamID(int iAgentID){return m_mHotSpots[iAgentID]->sTeamID;}
	//adds an influence layer to a specific hotspot
	void AddHotSpotLayer(int iAgentID, float fInfluenceStrength, float fInfluenceRange, std::string sFalloffMap);
	//adds an influence layer to a specific hotspot
	void SetHotSpotLayer(int iAgentID, UINT iLayerNum, float fInfluenceStrength, float fInfluenceRange, std::string sFalloffMap);
	//edits a hotspot's strength on a particular layer
	void SetHotSpotLayerStrength(int iAgentID, UINT iLayerNum, float fInfluenceStrength);
	//edits a hotspot's range on a particular layer
	void SetHotSpotLayerRange(int iAgentID, UINT iLayerNum, float fInfluenceRange);
	//edits a hotspot's falloff texture on a particular layer
	void SetHotSpotLayerFalloffMap(int iAgentID, UINT iLayerNum, std::string sFalloffMap);
	//returns the number of influence layers for a specific hotspot
	UINT GetNumOfHotSpotLayers(int iAgentID){return m_mHotSpots[iAgentID]->vInfluenceRange.size();}
	UINT GetNumOfHotSpots(){return m_mHotSpots.size();}

	//sets a specific hotspot's position and ID to that of a node in the map
	void SetHotSpotNode(int iAgentID, std::shared_ptr<infNode> pNode){m_mHotSpots[iAgentID]->m_vPosition = pNode->m_vPosition; m_mHotSpots[iAgentID]->m_iRoomID = pNode->m_iRoomID;}
	//sets a new position for a specific hotspot
	void SetHotSpotPosition(int iAgentID, Vector3 vPosition){m_mHotSpots[iAgentID]->m_vPosition = vPosition;}
	//sets a new room number for a specific hotspot
	void SetHotSpotRoomNum(int iAgentID, int iRoomNum){m_mHotSpots[iAgentID]->m_iRoomID = iRoomNum;}
	//sets a hotspot's team
	void SetHotSpotTeam(int iAgentID, UINT iTeamID){m_mHotSpots[iAgentID]->sTeamID = iTeamID;}

	//returns a pointer to a hotspot
	std::shared_ptr<HotSpot> GetHotSpot(int iAgentID){return m_mHotSpots[iAgentID];}

	//removes an agent's hotspot
	void RemoveHotSpot(int iAgentID);

	//sets alliance reputations for the given team. default enemy teams should not be done here, as the program
	//assumes that all unlisted reputations are enemies
	void SetTeamReputation(UINT iTeam1, UINT iTeam2, UINT iTeamLayer, float fValue){m_mmTeamReputation[iTeam1][iTeam2].SetValue(iTeamLayer, fValue);}

	//clears all custom reputation scores for a specific team
	//NOTE: if includeSelf is set to true, the team will become hostile to itself
	void ClearTeamReputations(UINT iTeam, bool bIncludeSelf = false);
	//removes a specific reputation score for a specific team
	void ClearReputation(UINT iTeam1, UINT iTeam2){m_mmTeamReputation[iTeam1].erase(iTeam2);}
	//removes all custom team reputation scores
	void ClearAllReputations(bool bIncludeSelf = false);

	/*calculates the influence of a given node
	bUseInterpolation	- use the best door distance only, or consider all doors?
	bAddInterpolation	- when interpolating, use only the highest result, or combine them?
	bUncapRange			- disregard range limits for fancier texture falloff results?
	bTileTexture		- when range is uncapped, will we tile the falloff texture?*/
	void CalculateNode(std::shared_ptr<infNode> pNode,  UINT iTeamID,
					bool bUseInterpolation, bool bAddInterpolation, bool bUncapRange, bool bTileTexture);

	/*calculates the influence of a given node
	bUseInterpolation	- use the best door distance only, or consider all doors?
	bAddInterpolation	- when interpolating, use only the highest result, or combine them?
	bUncapRange			- disregard range limits for fancier texture falloff results?
	bTileTexture		- when range is uncapped, will we tile the falloff texture?*/
	bool CalculateNode(UINT iNodeNum, int iRoomNum, UINT iTeamID,
					bool bUseInterpolation, bool bAddInterpolation, bool bUncapRange, bool bTileTexture);

	//calculates the influence of a given node for a specific hotspot
	//void calculateInfluence_HotSpot(std::shared_ptr<infNode>, int agentID, UINT teamID, bool interpolate, bool additive);

	/*calculates all nodes in the given node's room
	bUseInterpolation	- use the best door distance only, or consider all doors?
	bAddInterpolation	- when interpolating, use only the best result, or combine them?
	bUncapRange			- disregard range limits for fancier texture falloff results?
	bTileTexture		- when range is uncapped, will we tile the falloff texture?*/
	bool CalculateRoom(int iRoomID, UINT iTeamID,
					bool bUseInterpolation, bool bAddInterpolation, bool bUncapRange, bool bTileTexture);

	/*calculates all in the map
	bUseInterpolation	- use the best door distance only, or consider all doors?
	bAddInterpolation	- when interpolating, use only the best result, or combine them?
	bUncapRange			- disregard range limits for fancier texture falloff results?
	bTileTexture		- when range is uncapped, will we tile the falloff texture?*/
	void CalculateMap(UINT iTeamID,
					bool bUseInterpolation, bool bAddInterpolation, bool bUncapRange, bool bTileTexture);

	//void calculateHotSpot(int iAgentID, bool bInterpolate, bool relative);

	void ClearAllNodes();

	//clears all influence data out of a node
	void ClearNode(std::shared_ptr<infNode> pNode);

	//resets the influences on all nodes to 0
	void ResetInfluences();

	//resets the values of a node's specific team data
	void ResetValues(std::shared_ptr<infNode> pNode, UINT iTeamID);

	void RemoveAllNodes();

	//returns the nearest possible node to the given position
	std::shared_ptr<infNode> GetNearestNode(Vector3 vPosition);

	//returns the nearest node to the given position in the given room
	std::shared_ptr<infNode> GetNearestNodeInRoom(UINT iRoomID, Vector3 vPosition);

	//combines specific team influences, for a specific team, on a specific node
	void CombineInfluences_Node(eInfluenceCalculationTypes eCalculationMethod, std::shared_ptr<infNode> pNode, UINT iTargetTeam, UINT iResultMap, const std::vector<UINT> &vMaps);
	//combines a team influences, for a specific team, on a specific node
	void CombineAllInfluences_Node(eInfluenceCalculationTypes eCalculationMethod, std::shared_ptr<infNode> pNode, UINT iTargetTeam, UINT iResultMap);
	//combines specific team influences, for a specific team, in a specific room
	void CombineInfluences_Room(eInfluenceCalculationTypes eCalculationMethod, UINT iRoomID, UINT iTargetTeam, UINT iResultMap, const std::vector<UINT> &vMaps);
	//combines all team influences, for a specific team, in a specific room
	void CombineAllInfluences_Room(eInfluenceCalculationTypes eCalculationMethod, UINT iRoomID, UINT iTargetTeam, UINT iResultMap);
	//combines specific team influences, for a specific team, for all nodes
	void CombineInfluences_Map(eInfluenceCalculationTypes eCalculationMethod, UINT iTargetTeam, UINT iResultMap, const std::vector<UINT> &vMaps);
	//combines a team influences, for a specific team, for all nodes
	void CombineAllInfluences_Map(eInfluenceCalculationTypes eCalculationMethod, UINT iTargetTeam, UINT iResultMap);

	//gets the influence for a specific team for the given position and room, returned in nodeValues format
	nodeValues ComputeRawNode(eInfluenceCalculationTypes eCalculationMethod,  const Vector3 &vPosition, int iRoomNum, UINT iTeamNum,
					bool bUseInterpolation, bool bAddInterpolation, bool bUncapRange, bool bTileTexture);
	//gets the influence for a specific team for the given position and room, returned in nodeValues format
	nodeValues ComputeRawNode(const Vector3 &vPosition, int iRoomNum, UINT iTeamNum,
					bool bUseInterpolation, bool bAddInterpolation, bool bUncapRange, bool bTileTexture);

	float GetCombinedInfluence(std::shared_ptr<infNode> node, UINT iTeamID, UINT iLayerNum){return node->mValues[iTeamID].m_vCombinedInfluence[iLayerNum];}
	float GetTeamInfluence(std::shared_ptr<infNode> node, UINT iTeamID, UINT iLayerNum){return node->mValues[iTeamID].m_vTeamInfluence[iLayerNum];}
	nodeValues GetTeamInfluenceData(std::shared_ptr<infNode> node, UINT iTeamID){return node->mValues[iTeamID];}
	void GetAllInfluenceData(std::shared_ptr<infNode> node, std::vector<nodeValues> *resultValues);


	void Clean();
};

#endif