#ifndef __AICOREDEPENDENCIES_H
#define __AICOREDEPENDENCIES_H

#include <memory>
#include <DCMathLib.h>
#include <vector>

#include "Path.h"
#include "Graph.h"
#include "PotentialManager.h"
#include "SteeringForceManager.h"

#include "FSM\AI_States.h"

class AICoreDLL;

class AgentProperties
{
private:
	Vector3 m_vTarget;
	
	int m_iMovementID,
		m_iSharedID;
	bool m_bDoPathing,
		m_bDoPotential;
public:
	AgentProperties(int iSharedID, int iMovementID, bool bDoPathing = false, bool bDoPotential = false);

	std::shared_ptr<StateMachine<AgentProperties>> m_pStateMachine;
	std::shared_ptr<Path> m_pPath;
	PotentialManager* m_pPotential;
	Graph *m_pGraph;
	SteeringForceManager2* m_pSteering;

	void SetTarget(Vector3 target){m_vTarget = target;}

	void SetSteering(SteeringForceManager2 *pSteering)
	{
		m_pSteering = pSteering;
	}
	void SetGraph(Graph *pGraph)
	{
		m_pGraph = pGraph;
		m_pPath = std::make_shared<Path>(m_pGraph);
	}
	//void SetTarget(Vector3 target){m_vTarget = target;}

	void Update(float dt);

	int GetMovementID(){return m_iMovementID;}
	int GetSharedID(){return m_iSharedID;}
	bool GetDoPathing(){return m_bDoPathing;}
	bool GetDoPotential(){return m_bDoPotential;}
	Vector3 GetTarget(){return m_vTarget;}


	friend AICoreDLL;
};

struct sDebugInfo
{
	Vector3 vPosition;
	Quaternion qOrientation;
	float fPotentialValue;
	bool bIsOnPath;
};

#endif