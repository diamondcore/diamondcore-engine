#include "Node.h"

namespace WTW
{
	Node::Node(int id, Vector3 pos)
		:m_ID(id), m_vPosition(pos)
	{
		m_fRadius = 0.5f;
		m_F = 0.0f; //this is set depending on path processing

		Activate();
	}
	Node::~Node()
	{
		ClearEdges();
	}

	void Node::Activate()
	{
		m_vColor =  Vector3(0,0,0);	//(199,100,1);
		m_bActivated = true;
	}	
	void Node::Deactivate()
	{
		m_vColor = Vector3(255,0,0); //same color as background
		m_bActivated = false;
	}
	bool Node::IsActivated()
	{
		return m_bActivated;
	}

	float Node::getEuclideanCostTo(Node* end) 
	{
		double tempZ = abs(m_vPosition.z - end->getPos().z);
		double tempX = abs(m_vPosition.x - end->getPos().x);

		return sqrt(tempZ*tempZ + tempX*tempX); 
		//return end->getPos().euclideanDist(m_vPosition); // node to endNode
	}
	float Node::getEuclideanCostTo(Vector3& entity) 
	{
		double tempZ = abs(m_vPosition.z - entity.z);
		double tempX = abs(m_vPosition.x - entity.x);

		return sqrt(tempZ*tempZ + tempX*tempX); 
		//return entity.euclideanDist(m_vPosition); // NPC to node
	}

	void Node::UpdateEdges()
	{
		for(auto E = m_pEdges.begin();
			E != m_pEdges.end();
			E++)
				(*E)->Update(); //update edges
	}

	bool Node::operator== (const Node &rhs) const
	{
		if(m_ID == rhs.getID())
			return true;
		else
			return false;
	}
	bool Node::operator!= (const Node &rhs) const
	{
		return !(m_ID == rhs.getID());
	}
	void Node::AddEdge(Edge* edge)
	{
		m_pEdges.push_back(edge);
	}
	void Node::ClearEdges()
	{
		while(!m_pEdges.empty())
		{
			delete m_pEdges.front();
			m_pEdges.front() = 0;
			m_pEdges.erase(m_pEdges.begin());
		}
		m_pEdges.clear();
	}


	/***************************Edge*********************/
	Edge::Edge(Node* A, Node* B)
		:nodeA(A), nodeB(B)
	{
		//G(n) distance of Edge
		double tempZ = abs(A->getPos().z - B->getPos().z);
		double tempX = abs(A->getPos().x - B->getPos().x);
		weightG = sqrt(tempZ*tempZ + tempX*tempX); 
		//weightG = nodeB->getPos().euclideanDist(nodeA->getPos()); 
		Update(); //set if activated or not
	}

	bool Edge::IsActivated()
	{
		return m_bActivated;
	}

	bool Edge::operator== (const Edge &rhs) const
	{
		if(m_ID == rhs.getID())
			return true;
		else
			return false;
	}

	void Edge::Update()	// Update node/edge accessability
	{
		if( !nodeA || !nodeB )	// Make sure the nodes both exist
		{
			Deactivate();
			return;
		}
		if(nodeA->IsActivated() && nodeB->IsActivated())
			Activate();	
		else
			Deactivate();	
	}

	void Edge::Activate()
	{
		m_bActivated = true;
	}
	void Edge::Deactivate()
	{
		m_bActivated = false;
	}
}