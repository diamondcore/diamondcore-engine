#include "Pathfinding.h"

PathfindingManager::PathfindingManager()
{
	m_pPath = std::make_shared<Path>(&m_Graph);
}

bool PathfindingManager::FindPath(int iEntityID, Vector3 *vStartPosition, Vector3 vEndPosition, float m_fMaxDistance)
{
	m_pPath->SetPath(vStartPosition, vEndPosition);
	m_mvEntityPaths[iEntityID] = m_pPath->GetPath();
	if(m_mvEntityPaths[iEntityID].size() == 0)
		return false;
	return true;
}

bool PathfindingManager::UpdatePath(int iEntityID, Vector3 vEntityPosition, float fNodeRange, UINT iCheckNextNodes)
{
	Vector3 vLength;
	float dist;
	UINT size = m_mvEntityPaths[iEntityID].size();
	

	for(UINT i = 0; i <= iCheckNextNodes; i++)
	{
		if(i >= size) break;

		vLength = vEntityPosition - m_mvEntityPaths[iEntityID].at(size-(i+1));
		dist = vLength.Magnitude();
		if(dist <= fNodeRange)
		{
			m_mvEntityPaths[iEntityID].pop_back();
			return true;
		}
	}

	return false;
}

bool PathfindingManager::GetPointInPath(Vector3 *result, int iEntityID, UINT iPointNum)
{
	if(m_mvEntityPaths[iEntityID].size() <= iPointNum)
		return false;

	*result = m_mvEntityPaths[iEntityID][iPointNum];

	return true;
}

void PathfindingManager::RemovePath(int iEntityID)
{
	m_mvEntityPaths.erase(iEntityID);
}