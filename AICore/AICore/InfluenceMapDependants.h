#ifndef __INFLUENCEMAPDEPENDANTS_H
#define __INFLUENCEMAPDEPENDANTS_H

//enumeration for how we are going to combine team maps
enum eInfluenceCalculationTypes
{
	eInfluence = 0,		//combine as a normal influence map
	eTension,		//combine as a tension map
	eVulnerability,	//combine as a vulnerability map
};

//a vector of floats for influence calculations.
//each element is the influence of a different layer
class nodeValues
{
public:
	//stores individual team influences
	std::vector<float> m_vTeamInfluence;

	//stores combined influences for a team
	//used to prevent overwriting individual team data
	std::vector<float> m_vCombinedInfluence;
};

//struct of data that is contained within each node
class infNode
{
private:

	//tells us what room this node is in
	int m_iRoomID;
	//per-team node values
	std::map<UINT, nodeValues> mValues;

	friend class InfluenceMap;
public:
	infNode(Vector3 position, int roomID){m_vPosition = position;m_iRoomID = roomID;}

	//node's position in the world
	Vector3 m_vPosition;
	//float m_fPotentialResult;
};

//contains necessary door data
class door
{
private:
	UINT ID;

	std::vector<int> vConnectedRooms;
	Vector3 m_vPosition;

	friend class InfluenceMap;

public:
	door(UINT id){ID = id;}
};

//data collection that stores our falloff textures
class falloffValues
{
private:
	std::string m_sFilename;

	std::vector<D3DXCOLOR> m_vPixels;

	int iWidth;
	int iHeight;

	friend class InfluenceMap;

public:
	falloffValues(std::string filename){m_sFilename = filename;}
};

//data contained within each hotspot, or influence source
class HotSpot
{
private:
	int m_iID;
	int m_iRoomID;
	std::vector<float> vInfluenceAmount;
	std::vector<float> vInfluenceRange;
	std::vector<std::shared_ptr<falloffValues>> vFalloffLayers;

	UINT sTeamID;

	friend class InfluenceMap;

public:
	HotSpot(Vector3 vPosition, int iRoomID){vPosition = vPosition; m_iRoomID = iRoomID;}
	int GetID(){return m_iID;}
	Vector3 m_vPosition;
};

class infReputation
{
private:
	std::vector<float> m_vPerMapRep;

public:
	infReputation(){}
	infReputation(UINT iSize, float fValue){SetSize(iSize, fValue);}	

	void SetSize(UINT iSize, float fValue)
	{
		m_vPerMapRep.resize(iSize, fValue);
	}

	void SetValue(UINT iterator, float fValue)
	{
		if(m_vPerMapRep.size() == 0)
		{
			m_vPerMapRep.push_back(fValue);
		}
		else if(iterator+1 > m_vPerMapRep.size())
		{
			m_vPerMapRep.resize(iterator+1, fValue);
		}
		m_vPerMapRep[iterator] = fValue;
	}

	float GetValue(UINT iterator)
	{
		if(iterator > m_vPerMapRep.size()) return 0.0f;
		return m_vPerMapRep[iterator];
	}
};

#endif