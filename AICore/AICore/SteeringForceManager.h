#ifndef __STEERINGFORCEMANAGER_H
#define __STEERINGFORCEMANAGER_H

#include <map>
#include <vector>
#include <memory>

#include <DCMathLib.h>

#include "SteeringForces.h"

////data class containing information we will need for registered forces
//class ForceRegistryData
//{
//public:
//	//force data
//	std::shared_ptr<SteeringForceBase> force;
//	//force priority
//	float priority;
//	//registry number
//	int registryID;
//	//is this force registered?
//	bool isRegistered;
//
//	ForceRegistryData(){isRegistered = false;}
//	ForceRegistryData(std::shared_ptr<SteeringForceBase> forceval, float priorityVal, bool registered, int registry){
//		force = forceval;
//		priority = priority;
//		isRegistered = registered;
//		registryID = registry;
//	}
//
//	//overload for sorting algorithm
//	bool operator < (const ForceRegistryData& data) const
//	{
//		return (priority < data.priority);
//	}
//};
//
//class SteeringForceManager
//{
//protected:
//	//map that contains a pointer all forces for each entity
//	std::map<int, std::vector<std::shared_ptr<ForceRegistryData>>> m_vcEntityForces;
//	
//	////map that contains a pointer to all forces we may wish to reference later
//	//std::map<int, std::weak_ptr<ForceRegistryData>> m_pRegisteredForces;
//
//public:
//	SteeringForceManager(){}
//	~SteeringForceManager(){clean();}
//
//	//adds a non-registered seek force
//	void AddSeekForce(int iEntityID,			//ID of the owner
//					Vector3 *target,		//target that the force will use
//					float fForceMagnitude,		//strength of the force
//					float fPriority,			//where in the force vector the new force will go
//					bool bTargetIsDynamic);		//reference the target or use a copy of its current position?
//
//	////adds a registered seek force
//	//void AddSeekForce(int iEntityID,			//ID of the owner
//	//				Vector3 *target,		//target that the force will use
//	//				float fForceMagnitude,		//strength of the force
//	//				float fPriority,			//where in the force vector the new force will go
//	//				bool bTargetIsDynamic,		//reference the target or use a copy of its current position?
//	//				int iRegisterNum);			//ID we want to register the force under, for later reference
//
//	//adds a non-registered flee force
//	void AddFleeForce(int iEntityID,			//ID of the owner
//					Vector3 *target,		//target that the force will use
//					float fForceMagnitude,		//strength of the force
//					float fPriority,			//where in the force vector the new force will go
//					bool bTargetIsDynamic);		//reference the target or use a copy of its current position?
//
//	////adds a registered flee force
//	//void AddFleeForce(int iEntityID,			//ID of the owner
//	//				Vector3 *target,		//target that the force will use
//	//				float fForceMagnitude,		//strength of the force
//	//				float fPriority,			//where in the force vector the new force will go
//	//				bool bTargetIsDynamic,		//reference the target or use a copy of its current position?
//	//				int iRegisterNum);			//ID we want to register the force under, for later reference
//
//	//adds a non-registered arrive force
//	void AddArriveForce(int iEntityID,			//ID of the owner
//					Vector3 *target,		//target that the force will use
//					float fInnerRange,			//distance from the target that the entity should stop by
//					float fOuterRange,			//distance from the target that affects distance/deceleration factor
//					float fForceMagnitude,		//strength of the force
//					float fPriority,			//where in the force vector the new force will go
//					bool bTargetIsDynamic);		//reference the target or use a copy of its current position?
//
//	////adds a registered arrive force
//	//void AddArriveForce(int iEntityID,			//ID of the owner
//	//				Vector3 *target,		//target that the force will use
//	//				float fInnerRange,			//distance from the target that the entity should stop by
//	//				float fOuterRange,			//distance from the target that affects distance/deceleration factor
//	//				float fForceMagnitude,		//strength of the force
//	//				float fPriority,			//where in the force vector the new force will go
//	//				bool bTargetIsDynamic,		//reference the target or use a copy of its current position?
//	//				int iRegisterNum);			//ID we want to register the force under, for later reference
//
//	////removes a particular force
//	//bool RemoveForce(int iEntityID, int iRegistryID);
//	//removes all forces of a particular type from an entity's force list
//	void RemoveAllForcesOfType(int iEntityID, int iSteeringForceType, UINT iStartRange, UINT iEndRange);
//	//removes all forces for a particular entity
//	void ClearForces(int iEntityID);
//
//	//returns the number of all forces in existence
//	UINT GetNumOfActiveForces(){return m_vcEntityForces.size();}
//	//returns the number of all active forces on a particular entity
//	UINT GetNumOfEntityForces(int iEntityID){return m_vcEntityForces[iEntityID].size();}
//	////returns the number of forces that are registered for lookup
//	//UINT GetNumOfRegisteredForces(){return  m_pRegisteredForces.size();}	
//
//	//updates forces
//	Vector3 UpdateForces(int iEntityID, Vector3 *vEntityPosition, Vector3 *vEntityVelocity, 
//						bool bUseConstantVelocity, UINT iStartRange, UINT iEndRange);
//
//	//Vector3 UpdateForces(int iEntityID, Vector3 *vEntityPosition, Vector3 *vEntityVelocity, 
//	//					bool bUseVelocitySnapshot, float fMaxPriority, float fMinPriority);
//
//	void clean();
//};


/*============================================
Remaking a more versatile steering force system
============================================*/
class SteeringForceData
{
private:
	
	Vector3 m_vLastResult,		//last result calculated
		m_vTarget,				//data for non-referenced target
		*m_pTarget;				//pointer to the target

	float m_fPriority;
	float m_fInnerRange, m_fOuterRange;
	float m_fForceMagnitude;
	unsigned int m_iRegistryName;
	UINT m_iForceType;

	friend class SteeringForceManager2;
public:
	SteeringForceData(){}
	//overload for sorting algorithm
	bool operator < (const SteeringForceData& data) const
	{
		return (m_fPriority < data.m_fPriority);
	}
};

class SteeringForceManager2
{
protected:

	std::map<int, std::vector<std::shared_ptr<SteeringForceData>>> m_mSteeringForces;
	std::vector<std::shared_ptr<SteeringForceData>>::iterator it;
	//std::map<int, std::map<unsigned int, std::weak_ptr<SteeringForceData>>> m_mRegisteredForces;

	Vector3 m_vResult;

	void DoSeek(Vector3 *vEntityPosition, Vector3 *vTarget, Vector3 *vEntityVelocity);
	void DoFlee(Vector3 *vEntityPosition, Vector3 *vTarget, Vector3 *vEntityVelocity);
	void DoArrive(Vector3 *vEntityPosition, Vector3 *vTarget, Vector3 *vEntityVelocity, float fInnerRange, float fOuterRange);

public:
	SteeringForceManager2(){}
	~SteeringForceManager2(){};
	//adds a non-registered seek force
	void AddSeekForce(int iEntityID,			//ID of the owner
					Vector3 *target,		//target that the force will use
					float fForceMagnitude,		//strength of the force
					float fPriority,			//where in the force vector the new force will go
					bool bTargetIsDynamic,		//reference the target or use a copy of its current position?
					unsigned int iRegisterID = 0);

	//adds a non-registered flee force
	void AddFleeForce(int iEntityID,			//ID of the owner
					Vector3 *target,		//target that the force will use
					float fForceMagnitude,		//strength of the force
					float fPriority,			//where in the force vector the new force will go
					bool bTargetIsDynamic,		//reference the target or use a copy of its current position?
					unsigned int iRegisterID = 0);

	//adds a non-registered arrive force
	void AddArriveForce(int iEntityID,			//ID of the owner
					Vector3 *target,		//target that the force will use
					float fInnerRange,			//distance from the target that the entity should stop by
					float fOuterRange,			//distance from the target that affects distance/deceleration factor
					float fForceMagnitude,		//strength of the force
					float fPriority,			//where in the force vector the new force will go
					bool bTargetIsDynamic,		//reference the target or use a copy of its current position?
					unsigned int iRegisterID = 0);

	bool SetRegisteredForceTarget(int iEntityID, unsigned int iRegisteredID, Vector3 *vTarget, bool bIsDynamic = false);
	bool SetRegisteredForceStrength(int iEntityID, unsigned int iRegisteredID, float fMagnitude = 1);

	bool RemoveRegisteredForce(int iEntityID, unsigned int iRegisteredID, bool bRemoveAll = true);
	//removes all forces of a particular type from an entity's force list
	void RemoveAllForcesOfType(int iEntityID, int iSteeringForceType);
	//removes all forces for a particular entity
	void ClearForces(int iEntityID){m_mSteeringForces[iEntityID].clear();}

	//returns the number of all forces in existence
	UINT GetNumOfForces(int iEntityID){return m_mSteeringForces[iEntityID].size();}
	UINT GetTotalNumOfForces();

	//updates forces
	Vector3 UpdateForces(int iEntityID, Vector3 *vEntityPosition, Vector3 *vEntityVelocity, 
						bool bUseConstantVelocity);
};


#endif