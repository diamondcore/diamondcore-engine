#include "PotentialManager.h"

PotentialManager::PotentialManager()
{
	m_iCurrentElement = 0;
}

void PotentialManager::BuildNeighbors(float fNeighborRange)
{
	UINT i, j;
	UINT k = m_vNodes.size();

	Vector3 dist;
	std::shared_ptr<nodeNeighbors> temp;

	for(i = 0; i < k; i++)
	{
		temp = m_vNodes[i];
		temp->m_vNeighbors.clear();
		for(j = 0; j < k; j++)
		{
			if(i != j)
			{
				dist = m_vNodes[j]->m_pNode->m_vPosition - m_vNodes[i]->m_pNode->m_vPosition;
				if(dist.Magnitude() < fNeighborRange)
				{
					temp->m_vNeighbors.push_back(m_vNodes[j]);
				}
			}
		}
	}
}

int PotentialManager::AddElement(unsigned int dTeamName, Vector3 vPosition, bool bUseNearestNode, bool bForceCheck)
{
	EditElement(m_iCurrentElement, 0, vPosition, bUseNearestNode, bForceCheck);
	m_iCurrentElement++;
	return m_iCurrentElement - 1;
}

bool PotentialManager::AddElement(int iElementID, unsigned int dTeamName,  Vector3 vPosition, bool bUseNearestNode, bool bForceCheck)
{
	bool b = true;
	if(bForceCheck)
	{
		for(std::map<unsigned int, UINT>::iterator it = m_mTeamNames.begin(); it != m_mTeamNames.end(); it++)
		{
			if(it->first == dTeamName)
			{
				m_cPotential.AddHotSpot(iElementID, it->second, 0, vPosition, bUseNearestNode, b);
				return true;
			}
		}
		b = false;
		m_mTeamNames[dTeamName] = m_iCurrentTeam;
		m_iCurrentTeam++;
	}
	hotspots[iElementID] = std::shared_ptr<HotSpot>(m_cPotential.AddHotSpot(iElementID, m_mTeamNames[dTeamName], 0, vPosition, bUseNearestNode, b));
	return b;
}

bool PotentialManager::EditElement(int iEntityID, unsigned int dTeamName,  Vector3 vPosition, bool bUseNearestNode, bool bForceCheck)
{
	bool b = true;
	if(bForceCheck)
	{	
		for(std::map<unsigned int, UINT>::iterator it = m_mTeamNames.begin(); it != m_mTeamNames.end(); it++)
		{
			if(it->first == dTeamName)
			{
				m_cPotential.AddHotSpot(iEntityID, it->second, 0, vPosition, bUseNearestNode, b);
				return true;
			}
		}
		b = false;
		m_mTeamNames[dTeamName] = m_iCurrentTeam;
		m_iCurrentTeam++;
	}
	hotspots[iEntityID] = std::shared_ptr<HotSpot>(m_cPotential.GetHotSpot(iEntityID));
	m_cPotential.AddHotSpot(iEntityID, m_mTeamNames[dTeamName], 0, vPosition, bUseNearestNode, b);
	return b;
}

void PotentialManager::UpdateNodes(UINT iNumOfNodes)
{
	if(iNumOfNodes == 0)
	{
		std::map<unsigned int, UINT>::iterator it;
		for(it = m_mTeamNames.begin(); it != m_mTeamNames.end(); it++)
		{
			m_cPotential.CalculateMap(0, false, false, false, false);
		}
	}
	static UINT currentPosition = 0;
	UINT size = m_vNodes.size();
	std::map<unsigned int, UINT>::iterator it;
	for(UINT currentCount = 0; currentCount < iNumOfNodes; currentCount++)
	{
		for(it = m_mTeamNames.begin(); it != m_mTeamNames.end(); it++)
		{
			m_cPotential.CalculateNode(m_vNodes[currentPosition]->m_pNode, it->second, false, false, false, false);
		}
		currentPosition++;
		if(currentPosition >= size)
		{
			currentPosition = 0;
		}
	}
}

void PotentialManager::UpdateNodes(std::vector<D3DXMATRIX> &nodePos, std::vector<float> nodeValues, UINT iNumOfNodes)
{
	UINT size = m_vNodes.size();
	D3DXMATRIX m;
	Vector3 pos;
	float val;
	if(iNumOfNodes == 0)
	{
		std::map<unsigned int, UINT>::iterator it;
		for(it = m_mTeamNames.begin(); it != m_mTeamNames.end(); it++)
		{
			m_cPotential.CalculateMap(0, false, false, false, false);
		}
		
		for(UINT i = 0; i < size; i++)
		{
			GetNodeInfo(i, pos, val);
			nodeValues[i] = val;
			D3DXMatrixIdentity(&m);
			D3DXMatrixTranslation(&m, pos.x, pos.y, pos.z);
			nodePos[i] = m;
		}
	}
	static UINT currentPosition = 0;
	std::map<unsigned int, UINT>::iterator it;
	for(UINT currentCount = 0; currentCount < iNumOfNodes; currentCount++)
	{
		for(it = m_mTeamNames.begin(); it != m_mTeamNames.end(); it++)
		{
			m_cPotential.CalculateNode(m_vNodes[currentPosition]->m_pNode, it->second, false, false, false, false);
		}
		GetNodeInfo(currentPosition, pos, val);
		nodeValues[currentPosition] = val;
		D3DXMatrixIdentity(&m);
		D3DXMatrixTranslation(&m, pos.x, pos.y, pos.z);
		nodePos[currentPosition] = m;
		currentPosition++;
		if(currentPosition >= size)
		{
			currentPosition = 0;
		}
	}
}

bool PotentialManager::GetBestTarget(Vector3 *vResult, UINT iAgentID, const Vector3 &vPosition, float fApproachRange, bool bDestroyOnreach)
{
	Vector3 vDist,
		vBest(vPosition);

	std::vector<int> ids = m_cPotential.GetHotSpotIDs();

	UINT i, k, teamID;
	int currentID;

	Vector3 pos;
	float bestVal = FLT_MAX * -1;
	float nodeResult, dist;
	nodeValues values;
	std::map<unsigned int, UINT>::iterator it;
	std::map<UINT, nodeValues> tempVals;

	bool reachedTarget = false;

	m_vProbableTargets.clear();
	std::shared_ptr<infNode> rawNode = std::make_shared<infNode>(vPosition, 0);

	for(i = 0; i < ids.size(); i++)
	{
		currentID = ids[i];

		pos = m_cPotential.GetHotSpot(currentID)->m_vPosition;
		rawNode->m_vPosition = pos;
		//values = m_cPotential.ComputeRawNode(pos, 0, 0, false, false, false, false);
		nodeResult = 0.0;
		for(it = m_mTeamNames.begin(); it != m_mTeamNames.end(); it++)
		{
			teamID = it->first;
			m_cPotential.CalculateNode(rawNode, teamID, false, false, false, false);
			tempVals[teamID] = m_cPotential.GetTeamInfluenceData(rawNode, teamID);
			for(k = 0; k < tempVals[teamID].m_vTeamInfluence.size(); k++)
			{
				nodeResult += tempVals[teamID].m_vTeamInfluence[k] *
					m_mAgentPriorities[iAgentID][teamID].GetValue(k);
			}
		}
		m_vProbableTargets.push_back(std::pair<float, Vector3>(nodeResult, pos));
		vDist = pos - vPosition;
		dist = vDist.MagnitudeSquared();
		if(nodeResult/dist > bestVal)
		{
			bestVal = nodeResult/dist;
			vBest = pos;
		}
		
		if(vDist.MagnitudeSquared() < fApproachRange)
		{
			reachedTarget = true;
			if(bDestroyOnreach)
			{
				m_cPotential.RemoveHotSpot(currentID);
				UpdateNodes();
			}
		}
	}
	*vResult = vBest;
	return reachedTarget;
}
Vector3 PotentialManager::Navigate(UINT iAgentID, const Vector3 &vPosition)
{
	UINT i, k;
	//UINT j;
	UINT size = m_vNodes.size();
	//UINT neighborSize;

	std::map<UINT, nodeValues> nodeVals, tempVals;

	nodeNeighbors *pNode;

	std::map<unsigned int, UINT>::iterator it;

	float nodeResult,
		relativeResult;
	float val,
		distSq;

	float best = FLT_MAX * -1;

	UINT currentID;
	//bool isBest;

	Vector3 vDist,
			vBest(vPosition);

	std::shared_ptr<infNode> rawNode = std::make_shared<infNode>(vPosition, 0);

	nodeResult = 0.0;
	for(it = m_mTeamNames.begin(); it != m_mTeamNames.end(); it++)
	{
		currentID = it->second;
		m_cPotential.CalculateNode(rawNode, currentID, false, false, false, false);
		tempVals[currentID] = m_cPotential.GetTeamInfluenceData(rawNode, currentID);
		for(k = 0; k < tempVals[currentID].m_vTeamInfluence.size(); k++)
		{
			nodeResult += tempVals[currentID].m_vTeamInfluence[k] *
				m_mAgentPriorities[iAgentID][currentID].GetValue(k);
		}
		//relativeResult = nodeResult;
	}
	relativeResult = nodeResult;

	for(i = 0; i < size; i++)
	{
		pNode = m_vNodes[i].get();
		nodeResult = 0.0;
		vDist = pNode->m_pNode->m_vPosition - vPosition;
		distSq = vDist.MagnitudeSquared();
		for(it = m_mTeamNames.begin(); it != m_mTeamNames.end(); it++)
		{
			currentID = it->second;
			nodeVals[currentID] = m_cPotential.GetTeamInfluenceData(m_vNodes[i]->m_pNode, currentID);
			for(k = 0; k < m_cPotential.GetNumOfLayers() && k < nodeVals[currentID].m_vTeamInfluence.size(); k++)
			{
				nodeResult += nodeVals[currentID].m_vTeamInfluence[k] *
					m_mAgentPriorities[iAgentID][currentID].GetValue(k);
			}
			//pNode->m_pNode->m_fPotentialResult = nodeResult;
		}
		pNode->m_fPotentialResult = nodeResult;
	}

	//for(i = 0; i < size; i++)
	//{
	//	pNode = &m_vNodes[i];
	//	neighborSize =  pNode->m_vNeighbors.size();
	//	nodeResult = pNode->m_pNode->m_fPotentialResult;
	//	for(it = m_mTeamNames.begin(); it != m_mTeamNames.end(); it++)
	//	{
	//		currentID = it->second;
	//		isBest = true;
	//		if(size > 0)
	//		{
	//			for(j = 0; j < neighborSize; j++)
	//			{
	//				/*===================
	//				Calculate nodeResult and neighborResult;
	//					Using m_cPotential.GetAllCombinedInfluences
	//					then compress to a single nodeValues and run through vectorMath
	//				if for all neighbors, currentValue is greater than all neighborValue,
	//				list this node as a target
	//				===================*/
	//				if(pNode->m_pNode->m_fPotentialResult <= pNode->m_vNeighbors[j]->m_fPotentialResult)
	//				{
	//					isBest = false;
	//					break;
	//				}
	//				else if(best < pNode->m_pNode->m_fPotentialResult)
	//				{
	//					best = pNode->m_pNode->m_fPotentialResult;
	//					vBest = pNode->m_pNode->m_vPosition;
	//				}
	//			}
	//		}
	//		if(isBest)
	//		{
	//			m_vProbableTargets.push_back(std::pair<float, Vector3>(nodeResult, pNode->m_pNode->m_vPosition));
	//		}
	//	}
	//	if(m_vProbableTargets.size() == 0)
	//		m_vProbableTargets.push_back(std::pair<float, Vector3>(best, vBest));
	//}

	best = FLT_MAX * -1;

	//return GetBestProbableTarget();
	for(i = 0; i < size; i++)
	{
		pNode = m_vNodes[i].get();
		val = pNode->m_fPotentialResult;

		vDist = pNode->m_pNode->m_vPosition - vPosition;
		distSq = vDist.MagnitudeSquared();

		val = (val - relativeResult) / distSq;

		if(val > best)
		{
			best = val;
			vBest = pNode->m_pNode->m_vPosition;
		}
		//if(bRelativeColor)
		//	pNode->m_pNode->m_fPotentialResult = val;
	}
	return vBest;
}

void PotentialManager::GetNodeInfo(UINT nodeNum, Vector3 &pos, float &result)
{
	if(nodeNum >= m_vNodes.size())
		return;
	pos = m_vNodes[nodeNum].get()->m_pNode->m_vPosition;
	result = m_vNodes[nodeNum].get()->m_fPotentialResult;
}

void PotentialManager::GetProbableTargets(std::vector<Vector3> *results)
{
	results->clear();
	UINT size = m_vProbableTargets.size();

	for(UINT i = 0; i < size; i++)
	{
		results->push_back(m_vProbableTargets[i].second);
	}
}