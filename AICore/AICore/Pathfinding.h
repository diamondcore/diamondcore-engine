#ifndef __PATHFINDING_H
#define __PATHFINDING_H

#include <map>
#include <vector>
#include <memory>

#include "Path.h"

#include <DCMathLib.h>

//test

class PathfindingManager
{
protected:
	Graph m_Graph;
	std::shared_ptr<Path> m_pPath;

	struct MapNode
	{
		std::shared_ptr<Vector3> vPosition;
		bool bIsTraversable;
	};

	std::vector<MapNode> m_vsMapNodes;
	std::map<int, std::vector<Vector3>> m_mvEntityPaths;

public:
	PathfindingManager();

	void ManualCreateEdge(Vector3 posA, Vector3 posB);
	void GenerateGraph(int size, int maxNumNodes){m_Graph.generateNodes(size, maxNumNodes);}
	void CoverNodes(std::vector<Vector3> boundingBox, std::vector<D3DXMATRIX> transforms);
	std::vector<Vector3> GetPath(Vector3 start, Vector3 end);
	std::vector<D3DXMATRIX> GetPathMat(Vector3 start, Vector3 end);

	//bool LoadFromFile(std::string directory);
	bool FindPath(int iEntityID, Vector3 *vStartPosition, Vector3 vEndPosition, float fMaxDistance);
	bool UpdatePath(int iEntityID, Vector3 vEntityPosition, float fNodeRange, UINT iCheckNextNodes = 1);
	bool GetPointInPath(Vector3 *result, int iEntityID, UINT iPointNum = 0);
	void RemovePath(int iEntityID);
	Graph *GetGraph(){return &m_Graph;}
};

#endif