#include "InfluenceMap.h"

#define DEFAULT_ENEMY_REPUTATION -1.0f
#define DEFAULT_ALLY_REPUTATION 1.0f

InfluenceMap::InfluenceMap()
{
	//setting intitial values for tracking variables
	m_iDoorID = 0;
	m_iDoorRoomIDs = -1;
	m_iInfLayers = 1;
}

std::shared_ptr<infNode> InfluenceMap::AddNode(int roomNum, Vector3 position)
{
	//creating a node at the given position
	m_mMapNodeSets[roomNum].push_back(std::make_shared<infNode>(position, roomNum));
	//setting the node's room association
	//m_mMapNodeSets[roomNum].back()->m_iRoomID = roomNum;

	//storing the node into the room map
	return m_mMapNodeSets[roomNum].back();
}

std::shared_ptr<infNode> InfluenceMap::AddDoor(Vector3 position, const std::vector<int> &rooms)
{
	//creating the door with a new ID
	std::shared_ptr<door> tempDoor = make_shared<door>(m_iDoorID);

	//tempDoor->ID = m_iDoorID;

	//incrementing the door counter
	m_iDoorID++;

	//creating a node for the door
	std::shared_ptr<infNode> placeholderNode = std::make_shared<infNode>(position, m_iDoorRoomIDs);

	//placeholderNode->m_vPosition = position;
	//placeholderNode->m_iRoomID = m_iDoorRoomIDs;

	//setting all the rooms this node will connect to
	tempDoor->vConnectedRooms = rooms;

	//connecting up the door with the room that was created for it
	tempDoor->vConnectedRooms.push_back(m_iDoorRoomIDs);

	//adding the door to the door vector
	m_vDoors.push_back(tempDoor);

	//adding the door node to the door's room
	m_mMapNodeSets[m_iDoorRoomIDs].push_back(placeholderNode);

	//setting the door room counter for a new door
	m_iDoorRoomIDs--;

	//returning a pointer to the new node
	return placeholderNode;
}

void InfluenceMap::BakeDoorPaths()
{
	UINT i, j;

	//empties the door map
	m_mDoorMap.clear();
	//empties door distances
	m_mDistToDoors.clear();

	//nested for loop assigns the doors to their appropriate rooms.
	//this is required for finding influences between rooms
	for(i = 0; i < m_vDoors.size(); i++)
	{
		for(j = 0; j < m_vDoors[i]->vConnectedRooms.size(); j++)
		{
			m_mDoorMap[m_vDoors[i]->vConnectedRooms[j]].push_back(m_vDoors[i]);
		}
	}

	//calculates the minimum distance between all door combinations
	for(i = 0; i < m_vDoors.size(); i++)
	{
		//for(j = 0; j < (int)m_vDoors[i].vConnectedRooms.size(); j++)
		for(j = 0; j < m_vDoors.size(); j++)
		{
			//setting all distances to default FLT_MAX
			m_mDistToDoors[m_vDoors[i]->ID][m_vDoors[j]->ID] = FLT_MAX;
			FindMinDistToDoor(m_vDoors[i], m_vDoors[i], m_vDoors[j], 0.0f);
		}
	}
}

//recursive function does a depth-first search for finding the minimum distance required to travel to each door
void InfluenceMap::FindMinDistToDoor(std::shared_ptr<door> startDoor, std::shared_ptr<door> currentDoor, std::shared_ptr<door> endDoor, float currentDistance)
{
	//sets the minum distance to the current distance, if current distance is lower
	if(m_mDistToDoors[startDoor->ID][currentDoor->ID] > currentDistance)
	{		
		m_mDistToDoors[startDoor->ID][currentDoor->ID] = currentDistance;
	}

	//breaks out of the method if the appropriate door has been found, or
	//a better path to the current node has already been found
	if(currentDoor->ID == endDoor->ID ||
		m_mDistToDoors[startDoor->ID][currentDoor->ID] < currentDistance)
	{
		return;
	}

	UINT i, j;

	//uses brute force checking (for now)
	for(i = 0; i < currentDoor->vConnectedRooms.size(); i++)
	{
		//this check might not be needed
		if(currentDoor->vConnectedRooms[i] != m_iDoorRoomIDs)
		{
			for(j = 0; j < m_mDoorMap[currentDoor->vConnectedRooms[i]].size(); j++)
			{
				//set the door's distance to itself to 0
				if(currentDoor->ID == m_mDoorMap[currentDoor->vConnectedRooms[i]][j]->ID)
				{
					m_mDistToDoors[currentDoor->ID][m_mDoorMap[currentDoor->vConnectedRooms[i]][j]->ID] = 0.0f;
				}
				else
				{
					//Vector3 vDistance = currentDoor->m_vPosition - endDoor->m_vPosition;
					Vector3 vDistance = currentDoor->m_vPosition - m_mDoorMap[currentDoor->vConnectedRooms[i]][j]->m_vPosition;

					//find the minimum distance to the next door in the list
					FindMinDistToDoor(startDoor, m_mDoorMap[currentDoor->vConnectedRooms[i]][j], endDoor,
									currentDistance + sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z)));
				}
			}
		}
	}
}

std::vector<std::shared_ptr<infNode>> InfluenceMap::FindMaxNodes(UINT teamNum, UINT mapNum)
{
	std::shared_ptr<infNode> currentNode;

	float max = FLT_MIN;

	std::vector<std::shared_ptr<infNode>> vec;

	UINT i, size;

	for(m_MapSetIterator = m_mMapNodeSets.begin(); m_MapSetIterator != m_mMapNodeSets.end(); m_MapSetIterator++)
	{
		size = m_MapSetIterator->second.size();
		for(i = 0; i < size; i++)
		{
			//looking at the next node
			currentNode = m_MapSetIterator->second[i];
			//go through all the node's influence data
			if((int)currentNode->mValues[teamNum].m_vTeamInfluence.size() > mapNum)
			{
				//value higher than the previous highest value found - start list over again with the current node
				if(currentNode->mValues[teamNum].m_vTeamInfluence[mapNum] > max)
				{
					vec.clear();
					vec.push_back(currentNode);
					max = currentNode->mValues[teamNum].m_vTeamInfluence[mapNum];
				}
				//value found equal to the max value - add current node to vector
				else if(currentNode->mValues[teamNum].m_vTeamInfluence[mapNum] == max)
				{
					vec.push_back(currentNode);
				}
			}
		}
	}
	return vec;
}

std::vector<std::shared_ptr<infNode>> InfluenceMap::FindMinNodes(UINT teamNum, UINT mapNum)
{
	std::shared_ptr<infNode> currentNode;

	float min = FLT_MAX;

	std::vector<std::shared_ptr<infNode>> vec;

	UINT i, size;

	for(m_MapSetIterator = m_mMapNodeSets.begin(); m_MapSetIterator != m_mMapNodeSets.end(); m_MapSetIterator++)
	{
		size = m_MapSetIterator->second.size();
		for(i = 0; i < size; i++)
		{
			//looking at the next node
			currentNode = m_MapSetIterator->second[i];
			//go through all the node's influence data
			if((int)currentNode->mValues[teamNum].m_vTeamInfluence.size() > mapNum)
			{
				//value higher than the previous lowest value found - start list over again with the current node
				if(currentNode->mValues[teamNum].m_vTeamInfluence[mapNum] < min)
				{
					vec.clear();
					vec.push_back(currentNode);
					min = currentNode->mValues[teamNum].m_vTeamInfluence[mapNum];
				}
				//value found equal to the min value - add current node to vector
				else if(currentNode->mValues[teamNum].m_vTeamInfluence[mapNum] == min)
				{
					vec.push_back(currentNode);
				}
			}
		}
	}
	return vec;
}

std::shared_ptr<falloffValues> InfluenceMap::GetFalloffMap(std::string filename, bool bDoCreate)
{
	LPCSTR name = filename.c_str();

	UINT s;

	//check to see if the desired map has already been loaded
	for(s = 0; s < m_vFalloffMaps.size(); s++)
	{
		//if the map has alread been created, return the one already made
		if(m_vFalloffMaps[s]->m_sFilename == name) return m_vFalloffMaps[s];
	}

	if(!bDoCreate) return nullptr;

	HRESULT hResult;
	// Get the width and height of the image
	D3DXIMAGE_INFO imageInfo;

	
	//actual checking to see if the file exists is done here.
	//If the file does not exist, returns a NULL pointer
	hResult = D3DXGetImageInfoFromFileA(name, &imageInfo);
	if FAILED (hResult){
		return nullptr;
	}
	std::ifstream f_File;

	UINT offset;
	UINT dummy;

	f_File.open(filename, std::ios::binary);

	//getting the point in the texture where actual texture data begins
	UINT i, x, y;
	for(i = 0; i < 10; i++)
	{
		dummy = f_File.get();
	}

	offset = f_File.get();
	offset += f_File.get()*256;
	offset += f_File.get()*256*256;
	offset += f_File.get()*256*256*256;

	//go back to the beginning of the file
	f_File.clear();
	f_File.seekg(0, std::ios::beg);

	m_vFalloffMaps.push_back(std::make_shared<falloffValues>(filename));
	m_vFalloffMaps.back()->iHeight = imageInfo.Height;
	m_vFalloffMaps.back()->iWidth = imageInfo.Width;

	//skip over all undesired data
	for(i = 0; i < offset; i++)
	{
		dummy = f_File.get();
	}

	//getting the pixel data
	for(x = 0; x < (int)imageInfo.Width; x++)
	{
		for(y = 0; y < (int)imageInfo.Height; y++)
		{
			D3DXCOLOR color;			
			
			color.g = (FLOAT)f_File.get();
			color.b = (FLOAT)f_File.get();
			color.r = (FLOAT)f_File.get();
			color.a = 1;
			m_vFalloffMaps.back()->m_vPixels.push_back(color);
		}
	}

	f_File.close();
	
	return m_vFalloffMaps.back();
};

void InfluenceMap::RemoveFalloffMap(std::string filename)
{
	std::vector<std::shared_ptr<falloffValues>>::iterator it;

	for(it = m_vFalloffMaps.begin(); it != m_vFalloffMaps.end(); it++)
	{
		if(it->get()->m_sFilename == filename)
		{
			m_vFalloffMaps.erase(it);
			return;
		}
	}
}

bool InfluenceMap::GetFalloffPointByRange(std::shared_ptr<falloffValues> falloff, float x, float y, D3DXCOLOR &color, bool tile)
{
	//if the given map doesnt exist
	if(falloff == nullptr)
	{
		return false;
	}
	//multiply by with and height to get a pixel coordinate
	x *= falloff->iWidth;
	y *= falloff->iHeight;

	if(tile)
	{
		//modulus function puts the coordinates back into scope of the texture
		x = (float)(((int)x/* + falloff->iWidth*/) % falloff->iWidth);
		y = (float)(((int)y/* + falloff->iHeight*/) % falloff->iHeight);
	}
	else
	{
		//cap the coordinates to the texture dimensions
		if(x >= (float)falloff->iWidth)
			x = (float)falloff->iWidth - 1;
		else if(x < 0)
			x = 0.0f;
		if(y >= (float)falloff->iHeight)
			y = (float)falloff->iHeight - 1;
		else if(y < 0)
			y = 0.0f;
	}

	color = falloff->m_vPixels[(int)x + ((int)y * (int)x)];
	return true;
}

bool InfluenceMap::GetFalloffPointByPixel(std::shared_ptr<falloffValues> falloff, float x, float y, D3DXCOLOR &color, bool tile)
{
	//if the map data doesnt exist
	if(falloff == nullptr)
	{
		return false;
	}
	if(tile)
	{
		//modulus function puts the coordinates back into scope of the texture
		x = (float)((int)x % falloff->iWidth);
		y = (float)((int)y & falloff->iWidth);
	}
	else
	{
		//cap the coordinates to the texture dimensions
		if(x > (float)falloff->iWidth)
			x = (float)falloff->iWidth;
		else if(x < 0)
			x = 0.0f;
		if(y > (float)falloff->iHeight)
			y = (float)falloff->iHeight;
		else if(y <= 0)
			y = 0.0f;
	}

	color = falloff->m_vPixels[(int)x + ((int)y * (int)x)];
	return true;
}

std::shared_ptr<HotSpot> InfluenceMap::AddHotSpot(int agentID, UINT teamID, std::shared_ptr<infNode> node,  bool &bIsNewHotSpot,
												  std::vector<float> &amounts, std::vector<float> &spreads, std::vector<std::string>& falloffMaps)
{
	//temporary vectors to place our values in (WHY does editing the vectors directly result in performace loss?!)
	std::vector<float> t_influence;
	std::vector<float> t_spread;
	std::vector<std::shared_ptr<falloffValues>> t_maps;

	UINT i = 0;

	//if the given vecs are different sizes, get the size of the smallest one
	UINT max = min(spreads.size(), amounts.size());
	max = min(max, falloffMaps.size());

	//copy all data into the vectors
	while(i < max)
	{
		t_influence.push_back(amounts[i]);
		t_spread.push_back(spreads[i]);
		t_maps.push_back(GetFalloffMap(falloffMaps[i], true));
		i++;
	}

	//create the hotspot at the given position in the given room number
	std::shared_ptr<HotSpot> hotspot = std::make_shared<HotSpot>(node->m_vPosition, node->m_iRoomID);
	hotspot->m_iID = agentID;
	hotspot->vInfluenceAmount = t_influence;
	hotspot->vInfluenceRange = t_spread;
	hotspot->vFalloffLayers = t_maps;
	//m_mHotSpots[agentID].sourceNode = node;
	hotspot->sTeamID = teamID;

	//add the new hotspot to our data collection
	m_mHotSpots[agentID] = hotspot;

	//find out if the specified team exists yet
	bool exists = false;
	for(i = 0; i < m_vTeamIDs.size(); i++)
	{
		if(m_vTeamIDs[i] == teamID)
		{
			exists = true;
			break;
		}
	}
	//add the team if it doesnt exist
	if(!exists)
	{
		m_vTeamIDs.push_back(teamID);
		//set the new team to have an alliance of 1.0 to itself
		m_mmTeamReputation[teamID][teamID].SetSize(max, 1.0);
	}

	//find out if our agent exists yet
	exists = false;
	for(i = 0; i < m_vHotSpotAgentIDs.size(); i++)
	{
		if(m_vHotSpotAgentIDs[i] == agentID)
		{
			exists = true;
			break;
		}
	}
	//add the new agent
	if(!exists) m_vHotSpotAgentIDs.push_back(agentID);

	//may not be necessary
	////set the new team to have an alliance of 1.0 to itself
	//if(!DoesExist(m_mmTeamReputation[teamID], teamID))
	//{
	//	m_mmTeamReputation[teamID][teamID] = 1.0f;
	//}

	bIsNewHotSpot = exists;

	return hotspot;
}

std::shared_ptr<HotSpot> InfluenceMap::AddHotSpot(int agentID, UINT teamID, int iRoomID, Vector3 position, bool bUseNearestNode, bool &bIsNewHotSpot,
								  const std::vector<float> &amounts, const std::vector<float> &spreads, const std::vector<std::string>& falloffMaps)
{
	if(bUseNearestNode)
	{
		//finds the nearest node in the given room
		std::shared_ptr<infNode> tempNode = GetNearestNodeInRoom(iRoomID, position);
		//sets a new position if a node was found
		if(tempNode != nullptr)
		{
			position = tempNode->m_vPosition;
		}
		else
		{
			position.x = position.y = position.z = 0.0f;
		}
	}

	//temporary vectors to place our values in (WHY does editing the vectors directly result in performace loss?!)
	std::vector<float> t_influence;
	std::vector<float> t_spread;
	std::vector<std::shared_ptr<falloffValues>> t_maps;

	UINT i = 0;

	//if the given vecs are different sizes, get the size of the smallest one
	UINT max = min(spreads.size(), amounts.size());
	max = min(max, falloffMaps.size());

	//copy all data into the vectors
	while(i < max)
	{
		t_influence.push_back(amounts[i]);
		t_spread.push_back(spreads[i]);
		t_maps.push_back(GetFalloffMap(falloffMaps[i], false));
		i++;
	}

	//create the new hotspot
	std::shared_ptr<HotSpot> hotspot = std::make_shared<HotSpot>(position, iRoomID);
	
	hotspot->vInfluenceAmount = t_influence;
	hotspot->vInfluenceRange = t_spread;
	hotspot->vFalloffLayers = t_maps;
	//m_mHotSpots[agentID].sourceNode = node;
	hotspot->sTeamID = teamID;

	//add the new hotspot to our data collection
	m_mHotSpots[agentID] = hotspot;

	bool exists = false;
	for(i = 0; i < m_vTeamIDs.size(); i++)
	{
		if(m_vTeamIDs[i] == teamID)
		{
			exists = true;
			break;
		}
	}
	//add the team if it doesnt exist
	if(!exists)
	{
		m_vTeamIDs.push_back(teamID);
		//set the new team to have an alliance of 1.0 to itself
		m_mmTeamReputation[teamID][teamID].SetSize(max, 1.0);
	}

	//find out if our agent exists yet
	exists = false;
	for(i = 0; i < m_vHotSpotAgentIDs.size(); i++)
	{
		if(m_vHotSpotAgentIDs[i] == agentID)
		{
			exists = true;
			break;
		}
	}
	//add the new agent
	if(!exists) m_vHotSpotAgentIDs.push_back(agentID);

	//may not be neccessary
	//if(!DoesExist(m_mmTeamReputation[teamID], teamID))
	//{
	//	m_mmTeamReputation[teamID][teamID] = 1.0f;
	//}

	bIsNewHotSpot = exists;

	return hotspot;
}

std::shared_ptr<HotSpot> InfluenceMap::AddHotSpot(int agentID, UINT teamID, int iRoomID, Vector3 position, bool bUseNearestNode, bool &bIsNewHotSpot,
									float amounts, float spreads, std::string falloffMaps)
{
	if(bUseNearestNode)
	{
		//finds the nearest node in the given room
		std::shared_ptr<infNode> tempNode = GetNearestNodeInRoom(iRoomID, position);
		//sets a new position if a node was found
		if(tempNode != nullptr)
		{
			position = tempNode->m_vPosition;
		}
		else
		{
			position.x = position.y = position.z = 0.0f;
		}
	}

	//temporary vectors to place our values in (WHY does editing the vectors directly result in performace loss?!)
	std::vector<float> t_influence;
	std::vector<float> t_spread;
	std::vector<std::shared_ptr<falloffValues>> t_maps;

	UINT i = 0;

	//copy all data into the vectors
	t_influence.push_back(amounts);
	t_spread.push_back(spreads);
	t_maps.push_back(GetFalloffMap(falloffMaps, false));

	//create the new hotspot
	std::shared_ptr<HotSpot> hotspot = std::make_shared<HotSpot>(position, iRoomID);
	
	hotspot->vInfluenceAmount = t_influence;
	hotspot->vInfluenceRange = t_spread;
	hotspot->vFalloffLayers = t_maps;
	//m_mHotSpots[agentID].sourceNode = node;
	hotspot->sTeamID = teamID;
	hotspot->m_vPosition = position;

	//add the new hotspot to our data collection
	m_mHotSpots[agentID] = hotspot;

	bool exists = false;
	for(i = 0; i < m_vTeamIDs.size(); i++)
	{
		if(m_vTeamIDs[i] == teamID)
		{
			exists = true;
			break;
		}
	}
	//add the team if it doesnt exist
	if(!exists)
	{
		m_vTeamIDs.push_back(teamID);
		//set the new team to have an alliance of 1.0 to itself
		m_mmTeamReputation[teamID][teamID].SetSize(1, 1.0);
	}

	//find out if our agent exists yet
	exists = false;
	for(i = 0; i < m_vHotSpotAgentIDs.size(); i++)
	{
		if(m_vHotSpotAgentIDs[i] == agentID)
		{
			exists = true;
			break;
		}
	}
	//add the new agent
	if(!exists) m_vHotSpotAgentIDs.push_back(agentID);

	//may not be neccessary
	//if(!DoesExist(m_mmTeamReputation[teamID], teamID))
	//{
	//	m_mmTeamReputation[teamID][teamID] = 1.0f;
	//}
	bIsNewHotSpot = exists;

	return hotspot;
}

std::shared_ptr<HotSpot> InfluenceMap::AddHotSpot(int agentID, UINT teamID, int iRoomID, Vector3 position, bool bUseNearestNode, bool &bIsNewHotSpot)
{
	if(bUseNearestNode)
	{
		//finds the nearest node in the given room
		std::shared_ptr<infNode> tempNode = GetNearestNodeInRoom(iRoomID, position);
		//sets a new position if a node was found
		if(tempNode != nullptr)
		{
			position = tempNode->m_vPosition;
		}
		//else
		//{
		//	position.x = position.y = position.z = 0.0f;
		//}
	}

	UINT i = 0;

	//create the new hotspot
	std::shared_ptr<HotSpot> hotspot = std::make_shared<HotSpot>(position, iRoomID);

	//m_mHotSpots[agentID].sourceNode = node;
	hotspot->sTeamID = teamID;
	hotspot->m_vPosition = position;
	hotspot->m_iID = agentID;
	//add the new hotspot to our data collection
	m_mHotSpots[agentID] = hotspot;

	bool exists = false;
	for(i = 0; i < m_vTeamIDs.size(); i++)
	{
		if(m_vTeamIDs[i] == teamID)
		{
			exists = true;
			break;
		}
	}
	//add the team if it doesnt exist
	if(!exists)
	{
		m_vTeamIDs.push_back(teamID);
		//set the new team to have an alliance of 1.0 to itself
		m_mmTeamReputation[teamID][teamID].SetSize(1, 1.0);
	}

	//find out if our agent exists yet
	exists = false;
	for(i = 0; i < m_vHotSpotAgentIDs.size(); i++)
	{
		if(m_vHotSpotAgentIDs[i] == agentID)
		{
			exists = true;
			break;
		}
	}
	//add the new agent
	if(!exists) m_vHotSpotAgentIDs.push_back(agentID);

	//may not be neccessary
	//if(!DoesExist(m_mmTeamReputation[teamID], teamID))
	//{
	//	m_mmTeamReputation[teamID][teamID] = 1.0f;
	//}
	bIsNewHotSpot = exists;

	return hotspot;
}

void InfluenceMap::AddHotSpotLayer(int agentID, float influenceStrength, float vInfluenceRange, std::string falloffMap)
{
	//adding an additional layer to a hotspot's influence layers
	m_mHotSpots[agentID]->vInfluenceAmount.push_back(influenceStrength);
	m_mHotSpots[agentID]->vInfluenceRange.push_back(vInfluenceRange);
	m_mHotSpots[agentID]->vFalloffLayers.push_back(GetFalloffMap(falloffMap, false));
}

void InfluenceMap::SetHotSpotLayer(int agentID, UINT layerNum, float influenceStrength, float vInfluenceRange, std::string falloffMap)
{
	//editing the data for a specific layer of a hotspot's influence data
	if(layerNum >= 0 && layerNum < (int)m_mHotSpots[agentID]->vInfluenceAmount.size())
	{
		m_mHotSpots[agentID]->vInfluenceAmount.push_back(influenceStrength);
		m_mHotSpots[agentID]->vInfluenceRange.push_back(vInfluenceRange);
		m_mHotSpots[agentID]->vFalloffLayers.push_back(GetFalloffMap(falloffMap, false));
	}
}

void InfluenceMap::SetHotSpotLayerStrength(int agentID, UINT layerNum, float influenceStrength)
{
	//editing the strength data for a specific layer of a hotspot's influence data
	if(layerNum >= 0 && layerNum < (int)m_mHotSpots[agentID]->vInfluenceAmount.size())
	{
		m_mHotSpots[agentID]->vInfluenceAmount.push_back(influenceStrength);
	}
}

void InfluenceMap::SetHotSpotLayerRange(int agentID, UINT layerNum, float vInfluenceRange)
{
	//editing the range data for a specific layer of a hotspot's influence data
	if(layerNum >= 0 && layerNum < (int)m_mHotSpots[agentID]->vInfluenceRange.size())
	{
		m_mHotSpots[agentID]->vInfluenceRange.push_back(vInfluenceRange);
	}
}

void InfluenceMap::SetHotSpotLayerFalloffMap(int agentID, UINT layerNum, std::string falloffMap)
{
	//editing the falloff map a specific layer of a hotspot's influence data
	if(layerNum >= 0 && layerNum < (int)m_mHotSpots[agentID]->vFalloffLayers.size())
	{
		m_mHotSpots[agentID]->vFalloffLayers.push_back(GetFalloffMap(falloffMap, false));
	}
}

void InfluenceMap::RemoveHotSpot(int agentID)
{
	m_mHotSpots.erase(agentID);

	std::vector<int>::iterator it = m_vHotSpotAgentIDs.begin();

	//removing the hotspot's agent data
	for(UINT i = 0; i < m_vHotSpotAgentIDs.size(); i++)
	{
		if(agentID == m_vHotSpotAgentIDs[i])
		{
			m_vHotSpotAgentIDs.erase(it);
			break;
		}
		it++;
	}
}


void InfluenceMap::ClearTeamReputations(UINT team, bool includeSelf)
{
	if(includeSelf)
	{
		m_mmTeamReputation[team].clear();
		return;
	}

	infReputation self = m_mmTeamReputation[team][team];
	m_mmTeamReputation.clear();
	m_mmTeamReputation[team][team] = self;
}

void InfluenceMap::ClearAllReputations(bool bIncludeSelf)
{
	if(bIncludeSelf)
	{
		m_mmTeamReputation.clear();
		return;
	}
	std::map<UINT, std::map<UINT, infReputation>>::iterator repIT;
	infReputation self;
	for(repIT = m_mmTeamReputation.begin(); repIT != m_mmTeamReputation.end(); repIT++)
	{
		self = m_mmTeamReputation[repIT->first][repIT->first];
		m_mmTeamReputation[repIT->first].clear();
		m_mmTeamReputation[repIT->first][repIT->first] = self;
	}
}

void InfluenceMap::CalculateNode(std::shared_ptr<infNode> node, UINT teamID,
									  bool bUseInterpolation, bool bAddInterpolation, bool bUncapRange, bool bTileTexture)
{
	//clearing out the existing data
	ResetValues(node, teamID);

	Vector3 vDistance;
	float sqDistance;

	//local influence data for current data
	nodeValues values;

	//counters
	UINT i, j, k, l, m, it, size;


	float imagePosX;

	D3DXCOLOR falloffColor;

	//resizing the data to the current layer size we want
	values.m_vTeamInfluence.resize(m_iInfLayers, 0);

	//pointer to the hotspot we are currently looking at
	std::shared_ptr<HotSpot> hotspot(nullptr);

	size = m_vHotSpotAgentIDs.size();
	//runs through each hotspot
	for(it = 0; it < size; it++)
	{
		//setting the current hotspot
		hotspot = m_mHotSpots[m_vHotSpotAgentIDs[it]];

		//if the hotspot's team id is equal to our desired team, then calculate it
		if(hotspot->sTeamID == teamID)
		{
			//if the target node is in the same room as the hotspot
			if(node->m_iRoomID == hotspot->m_iRoomID)
			{
				//find the distance between the node and hotspot
				vDistance = node->m_vPosition - hotspot->m_vPosition;
				sqDistance = sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));

				//find the lower value so we dont go out of bounds
				m = min(m_iInfLayers, hotspot->vInfluenceAmount.size());

				//cycle through each influence layer
				for(i = 0; i < m; i++)
				{
					//find the range to use in the falloff
					imagePosX = sqDistance / hotspot->vInfluenceRange[i];

					//checks to see if the falloff texture exists
					if(GetFalloffPointByRange(hotspot->vFalloffLayers[i], imagePosX, 0, falloffColor, bTileTexture))
					{
						//if we remove the range cap for the texture or if the distance falls within the influence range
						if(bUncapRange || sqDistance < hotspot->vInfluenceRange[i])
						{
							values.m_vTeamInfluence[i] = (((float)falloffColor.r / 255.0f) * hotspot->vInfluenceAmount[i]) -
													(((float)falloffColor.b / 255.0f) * hotspot->vInfluenceAmount[i]);
						}
						//if the distance falls outside the influence range
						else
						{
							values.m_vTeamInfluence[i] = 0;
						}
					}
					//if no texture and the distance falls within the influence range
					else if(sqDistance < hotspot->vInfluenceRange[i])
					{
						values.m_vTeamInfluence[i] = hotspot->vInfluenceAmount[i] * (1 - imagePosX);	
					}
					//if no texture and the distance falls outside the influence range
					else
					{
						values.m_vTeamInfluence[i] = 0;
					}
				}
				//for any nonexisting data, use 0
				for(; i < m_iInfLayers; i++)
				{
					values.m_vTeamInfluence[i] = 0.0f;
				}
			}
			//if the node and hotspot are in different rooms
			else
			{
				//get the rooms
				int room1 = hotspot->m_iRoomID;
				int room2 = node->m_iRoomID;			

				//find the lower value so we dont go out of bounds
				m = min(m_iInfLayers, hotspot->vInfluenceAmount.size());

				//if we don't want to consider all paths, just the shortest one
				if(!bUseInterpolation)
				{
					float minDistance, currentDistance;

					minDistance = FLT_MAX;

					//find the minimum distance between the target node and the hotspot
					for(j = 0; j < m_mDoorMap[room1].size(); j++)
					{
						float val;
						//do distance checks between the node and hotspot and the doors in their associated rooms
						//to find the absolute shortest distance
						for(k = 0; k < m_mDoorMap[room2].size(); k++)
						{
							val = m_mDistToDoors[m_mDoorMap[room1][j]->ID][m_mDoorMap[room2][k]->ID];

							vDistance = node->m_vPosition - m_mDoorMap[room1][j]->m_vPosition;

							currentDistance = sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));

							vDistance = hotspot->m_vPosition - m_mDoorMap[room2][k]->m_vPosition;

							currentDistance += sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));
						
							currentDistance += m_mDistToDoors[m_mDoorMap[room1][j]->ID][m_mDoorMap[room2][k]->ID];

							if(currentDistance < minDistance)
							{
								minDistance = currentDistance;
							}
						}
					}
					//calculate the influences for all layers using the found distance
					//and the hotspot's falloff textures
					for(l = 0; l < m; l++)
					{
						//find the range to use in the falloff
						imagePosX = minDistance / hotspot->vInfluenceRange[l];

						//checks to see if the falloff texture exists
						if(GetFalloffPointByRange(hotspot->vFalloffLayers[l], imagePosX, 0, falloffColor, bTileTexture))
						{
							//if we remove the range cap for the texture or if the distance falls within the influence range
							if(bUncapRange || minDistance < hotspot->vInfluenceRange[l])
							{
								values.m_vTeamInfluence[l] = (((float)falloffColor.r / 255.0f) * hotspot->vInfluenceAmount[l]) -
														(((float)falloffColor.b / 255.0f) * hotspot->vInfluenceAmount[l]);
							}
							//if the distance falls outside the influence range
							else
							{
								values.m_vTeamInfluence[l] = 0;
							}
						}
						//if the distance falls within the influence range
						else if(minDistance < hotspot->vInfluenceRange[l])
						{
							values.m_vTeamInfluence[l] = hotspot->vInfluenceAmount[l] * (1 - imagePosX);	
						}
						//if the distance falls outside the influence range
						else
						{
							values.m_vTeamInfluence[l] = 0;
						}
					}
					//substitute data for any missing layers with 0
					for(; l < m_iInfLayers; l++)
					{
						values.m_vTeamInfluence[l] = 0.0f;
					}
				}
				//if we want to check the results from all possible paths between the node and hotspot
				else
				{
					float value, bestValue,
						distance;

					if(bAddInterpolation)
						bestValue = 0.0f;
					else
						bestValue = FLT_MIN;

					for(l = 0; l < i; l++)
					{
						//from each door in the hotspot's room...
						for(j = 0; j < m_mDoorMap[room1].size(); j++)
						{
							//...to each door in the node's room...
							for(k = 0; k < m_mDoorMap[room2].size(); k++)
							{
								vDistance = node->m_vPosition - m_mDoorMap[room1][j]->m_vPosition;

								distance = sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));

								vDistance = hotspot->m_vPosition - m_mDoorMap[room2][k]->m_vPosition;

								distance += sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));

								distance += m_mDistToDoors[m_mDoorMap[room1][j]->ID][m_mDoorMap[room2][k]->ID];
						
								//find the range to use in the falloff
								imagePosX = distance / hotspot->vInfluenceRange[l];

								//checks to see if the falloff texture exists
								if(GetFalloffPointByRange(hotspot->vFalloffLayers[l], imagePosX, 0, falloffColor, bTileTexture))
								{
									//if we remove the range cap for the texture or if the distance falls within the influence range
									if(bUncapRange || distance < hotspot->vInfluenceRange[l])
									{
										value = (((float)falloffColor.r / 255.0f) * hotspot->vInfluenceAmount[l]) -
																(((float)falloffColor.b / 255.0f) * hotspot->vInfluenceAmount[l]);
									}
									//if the distance falls outside the influence range
									else
									{
										value = 0;
									}
								}
								//if the distance falls within the influence range
								else if(distance < hotspot->vInfluenceRange[l])
								{
									value = hotspot->vInfluenceAmount[l] * (1 - imagePosX);	
								}
								//if the distance falls outside the influence range
								else
								{
									value = 0;
								}

								if(bAddInterpolation)
								{
									bestValue += value;
								}
								//use the best value found
								else if(value > bestValue)
								{
									bestValue = value;
								}
							}
						}
						values.m_vTeamInfluence[l] = bestValue;
					}
					//substitute data for any missing layers with 0
					for(; l < m_iInfLayers; l++)
					{
						values.m_vTeamInfluence[l] = 0.0f;
					}
				}
			}
			//add the results to what we have calculated so far
			for(m = 0; m < m_iInfLayers; m++)
			{
				node->mValues[teamID].m_vTeamInfluence[m] += values.m_vTeamInfluence[m];
			}
		}
	}
}

bool InfluenceMap::CalculateNode(UINT iNodeNum, int iRoomNum, UINT teamID,
									  bool bUseInterpolation, bool bAddInterpolation, bool bUncapRange, bool bTileTexture)
{
	iNodeNum = min(iNodeNum, m_mMapNodeSets[iRoomNum].size());
	if(iNodeNum == 0)
	{
		m_mMapNodeSets.erase(iRoomNum);
		return false;
	}

	std::shared_ptr<infNode> node = m_mMapNodeSets[iRoomNum][iNodeNum];

	CalculateNode(node, teamID, bUseInterpolation, bAddInterpolation, bUncapRange, bTileTexture);

	return true;
}

//void InfluenceMap::calculateInfluence_HotSpot(std::shared_ptr<infNode> node, int agentID, UINT teamID, bool interpolate, bool additive)
//{
//	//clearing out the existing data
//	resetValues(node, teamID);
//
//	Vector3 vDistance;
//	float sqDistance;
//
//	//local influence data for current data
//	nodeValues values;
//
//	//counters
//	UINT i, j, k, l, m;
//
//	float imagePosX;
//
//	float distRatioX;
//
//	D3DXCOLOR falloffColor;
//
//	//resizing the data to the current layer size we want
//	values.m_vTeamInfluence.resize(m_iInfLayers, 0);
//
//	//pointer to the desired hotspot
//	std::shared_ptr<HotSpot> hotspot = m_mHotSpots.at(agentID);
//
//
//	if(hotspot->sTeamID == teamID)
//	{
//		//if the target node is in the same room as the hotspot
//		if(node->m_iRoomID == hotspot->m_iRoomID)
//		{
//			vDistance = node->m_vPosition - hotspot->m_vPosition;
//			sqDistance = sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));
//			
//			//find the lower value so we dont go out of bounds
//			l = min(m_iInfLayers, hotspot->vInfluenceAmount.size());
//			for(i = 0; i < l; i++)
//			{
//				if(sqDistance < hotspot->vInfluenceRange[i])
//				{
//					distRatioX = sqDistance / hotspot->vInfluenceRange[i];
//
//					imagePosX = distRatioX;
//
//					if(GetFalloffPointByRange(hotspot->vFalloffLayers[i], imagePosX, 0, falloffColor, true))
//					{
//						values.m_vTeamInfluence[i] = (((float)falloffColor.r / 255.0f) * hotspot->vInfluenceAmount[i]) -
//													(((float)falloffColor.b / 255.0f) * hotspot->vInfluenceAmount[i]);
//					}
//					else
//					{
//						values.m_vTeamInfluence[i] = hotspot->vInfluenceAmount[i] * (1 - distRatioX);
//					}
//				}
//				else
//				{
//					values.m_vTeamInfluence[i] = 0;
//				}
//			}
//			for(; i < m_iInfLayers; i++)
//			{
//				values.m_vTeamInfluence[i] = 0.0f;
//			}
//		}
//		else
//		{
//			int room1 = node->m_iRoomID;
//			int room2 = hotspot->m_iRoomID;
//
//			//find the lower value so we dont go out of bounds
//			i = min(m_iInfLayers, hotspot->vInfluenceAmount.size());
//
//			//if we don't want to consider all paths, just the UINTest one
//			if(!interpolate)
//			{
//				float minDistance, currentDistance;
//
//				minDistance = FLT_MAX;
//
//				//find the minimum distance between the target node and the hotspot
//				for(j = 0; j < m_mDoorMap[room1].size(); j++)
//				{
//					for(k = 0; k < m_mDoorMap[room2].size(); k++)
//					{
//						float val = m_mDistToDoors[m_mDoorMap[room1][j]->ID][m_mDoorMap[room2][k]->ID];
//
//						vDistance = node->m_vPosition - m_mDoorMap[room1][j]->m_vPosition;
//
//						currentDistance = sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));
//
//						vDistance = hotspot->m_vPosition - m_mDoorMap[room2][k]->m_vPosition;
//
//						currentDistance += sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));
//						
//						currentDistance += m_mDistToDoors[m_mDoorMap[room1][j]->ID][m_mDoorMap[room2][k]->ID];
//
//						if(currentDistance < minDistance)
//						{
//							minDistance = currentDistance;
//						}
//					}
//				}
//				for(l = 0; l < i; l++)
//				{
//					if(l >= i)
//						break;
//					if(minDistance < hotspot->vInfluenceRange[l])
//					{
//						distRatioX = minDistance / hotspot->vInfluenceRange[l];
//
//						imagePosX = distRatioX;
//
//						if(GetFalloffPointByRange(hotspot->vFalloffLayers[l], imagePosX, 0, falloffColor, true))
//						{
//							values.m_vTeamInfluence[l] = (((float)falloffColor.r / 255.0f) * hotspot->vInfluenceAmount[l]) -
//													(((float)falloffColor.b / 255.0f) * hotspot->vInfluenceAmount[l]);
//						}
//						else
//						{
//							values.m_vTeamInfluence[l] = hotspot->vInfluenceAmount[l] * (1 - distRatioX);
//						}
//					
//					}
//					else
//					{
//						values.m_vTeamInfluence[l] = 0;
//					}
//				}
//				for(; l < m_iInfLayers; l++)
//				{
//					values.m_vTeamInfluence[l] = 0.0f;
//				}
//			}
//			else
//			{
//				float value, bestValue,
//					distance;
//
//				bestValue = 0;
//
//				for(l = 0; l < i; l++)
//				{
//					for(j = 0; j < (int)m_mDoorMap[room1].size(); j++)
//					{
//						for(k = 0; k < (int)m_mDoorMap[room2].size(); k++)
//						{
//							vDistance = node->m_vPosition - m_mDoorMap[room1][j]->m_vPosition;
//
//							distance = sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));
//
//							vDistance = hotspot->m_vPosition - m_mDoorMap[room2][k]->m_vPosition;
//
//							distance += sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));
//
//							distance += m_mDistToDoors[m_mDoorMap[room1][j]->ID][m_mDoorMap[room2][k]->ID];
//						
//							if(distance < hotspot->vInfluenceRange[l])
//							{
//								distRatioX = distance / (hotspot->vInfluenceRange[l]);
//
//								if(GetFalloffPointByRange(hotspot->vFalloffLayers[l], distRatioX, 0, falloffColor, true))
//								{
//									value = (((float)falloffColor.r / 255.0f) * hotspot->vInfluenceAmount[l]) -
//													(((float)falloffColor.b / 255.0f) * hotspot->vInfluenceAmount[l]);
//								}
//								else
//								{
//									value = (((float)falloffColor.r / 255.0f) * hotspot->vInfluenceAmount[l]) -
//													(((float)falloffColor.b / 255.0f) * hotspot->vInfluenceAmount[l]);
//								}
//								if((hotspot->vInfluenceAmount[l] >= 0 && value > bestValue) ||
//									(hotspot->vInfluenceAmount[l] < 0 && value < bestValue))
//								{
//									bestValue = value;
//								}
//							}
//							else
//							{
//								value = 0;
//
//								if((hotspot->vInfluenceAmount[l] >= 0 && value > bestValue) ||
//									(hotspot->vInfluenceAmount[l] < 0 && value < bestValue))
//								{
//									bestValue = value;
//								}
//							}
//						}
//					}
//					values.m_vTeamInfluence[l] = bestValue;
//				}
//				for(; l < m_iInfLayers; l++)
//				{
//					values.m_vTeamInfluence[l] = 0.0f;
//				}
//			}
//		}
//		if(additive)
//		{
//			for(m = 0; m < m_iInfLayers; m++)
//			{
//				node->mValues[teamID].m_vTeamInfluence[m] += values.m_vTeamInfluence[m];
//			}
//		}
//		else
//		{
//			node->mValues[teamID].m_vTeamInfluence = values.m_vTeamInfluence;
//		}
//	}
//}

bool InfluenceMap::CalculateRoom(int iRoomID, UINT teamID,
								 bool bUseInterpolation, bool bAddInterpolation, bool bUncapRange, bool bTileTexture)
{
	//checking to see how many nodes are in the given room
	UINT size = m_mMapNodeSets[iRoomID].size();
	if(size == 0)
	{
		//program would have created the unused room during checking - get rid of it
		m_mMapNodeSets.erase(iRoomID);
		return false;
	}
	for(UINT i = 0; i < size; i++)
	{
		//run through each node in the room
		CalculateNode(m_mMapNodeSets[iRoomID][i], teamID,
							bUseInterpolation, bAddInterpolation, bUncapRange, bTileTexture);
	}
	return true;
}

void InfluenceMap::CalculateMap(UINT iTeamID, bool bUseInterpolation, bool bAddInterpolation, bool bUncapRange, bool bTileTexture)
{
	UINT size;
	int roomID;

	//go through each room, call the CalculateRoom method
	for(m_MapSetIterator = m_mMapNodeSets.begin(); m_MapSetIterator != m_mMapNodeSets.end(); m_MapSetIterator++)
	{
		roomID = m_MapSetIterator->first;
		size = m_mMapNodeSets[roomID].size();
		for(UINT i = 0; i < size; i++)
		{
			//calculate each node in the room
			CalculateNode(m_mMapNodeSets[roomID][i], iTeamID,
								bUseInterpolation, bAddInterpolation, bUncapRange, bTileTexture);
		}
	}
}

////void InfluenceMap::calculateHotSpot(int agentID, bool interpolate, bool relative)
////{
//	//Vector3 vDistance;
//	//float sqDistance;
//
//	//nodeValues values;
//
//	//int i, j, k, l;
//
//	//float imagePosX;
//
//	//float distRatioX;
//
//	//D3DXCOLOR falloffColor;
//
//	//values.vInfluence.resize(m_iInfLayers);
//
//	//infNode *node;
//
//	//for(m_MapSetIterator = m_mMapNodeSets.begin(); m_MapSetIterator != m_mMapNodeSets.end(); m_MapSetIterator++)
//	//{
//	//	for(i = 0; i < (int)m_MapSetIterator->second.size(); i++)
//	//	{
//	//		node = m_MapSetIterator->second[i];
//	//		resetValues(node, m_mHotSpots[agentID].teamID);
//
//	//		//if the target node is in the same room as the hotspot
//	//		if(node->m_iRoomID == m_mHotSpots[agentID].iRoomID)
//	//		{
//	//			vDistance = node->m_vPosition - m_itHotSpotIterator->second.position;
//	//			sqDistance = sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));
//
//	//			for(i = 0; i < m_iInfLayers || i < (int)m_mHotSpots[agentID].vInfluenceAmount.size(); i++)
//	//			{
//	//				if(sqDistance < m_mHotSpots[agentID].vInfluenceRange[i])
//	//				{
//	//					distRatioX = sqDistance / m_mHotSpots[agentID].vInfluenceRange[i];
//
//	//					imagePosX = distRatioX;
//
//	//					if(GetFalloffPointByRange(m_mHotSpots[agentID].vFalloffLayers[i], imagePosX, 0, falloffColor))
//	//					{
//	//						values.vInfluence[i] = ((float)falloffColor.r / (float)255) * m_mHotSpots[agentID].vInfluenceAmount[i];
//	//					}
//	//					else
//	//					{
//	//						values.vInfluence[i] = m_mHotSpots[agentID].vInfluenceAmount[i] * (1 - distRatioX);
//	//					}
//	//				}
//	//				else
//	//				{
//	//					values.vInfluence[i] = 0;
//	//				}
//	//			}
//	//		}
//	//		else
//	//		{
//	//			int room1 = node->m_iRoomID;
//	//			int room2 = m_mHotSpots[agentID].iRoomID;
//
//	//			//if we don't want to consider all paths
//	//			if(!interpolate)
//	//			{
//	//				float minDistance, currentDistance;
//
//	//				minDistance = FLT_MAX;
//
//	//				//find the minimum distance between the target node and the hotspot
//	//				for(j = 0; j < (int)m_mDoorMap[room1].size(); j++)
//	//				{
//	//					for(k = 0; k < (int)m_mDoorMap[room2].size(); k++)
//	//					{
//	//						float val = m_mDistToDoors[m_mDoorMap[room1][j]->ID][m_mDoorMap[room2][k]->ID];
//
//	//						vDistance = node->m_vPosition - m_mDoorMap[room1][j]->m_vPosition;
//
//	//						currentDistance = sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));
//
//	//						vDistance = m_mHotSpots[agentID].position - m_mDoorMap[room2][k]->m_vPosition;
//
//	//						currentDistance += sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));
//	//					
//	//						currentDistance += m_mDistToDoors[m_mDoorMap[room1][j]->ID][m_mDoorMap[room2][k]->ID];
//
//	//						if(currentDistance < minDistance)
//	//						{
//	//							minDistance = currentDistance;
//	//						}
//	//					}
//	//				}
//	//				for(l = 0; l < m_iInfLayers; l++)
//	//				{
//	//					if(minDistance < m_mHotSpots[agentID].vInfluenceRange[l])
//	//					{
//	//						distRatioX = minDistance / m_itHotSpotIterator->second.vInfluenceRange[l];
//
//	//						imagePosX = distRatioX;
//
//	//						if(GetFalloffPointByRange(m_mHotSpots[agentID].vFalloffLayers[l], imagePosX, 0, falloffColor))
//	//						{
//	//							values.vInfluence[l] = ((float)falloffColor.r / (float)255) * m_itHotSpotIterator->second.vInfluenceAmount[l];
//	//						}
//	//						else
//	//						{
//	//							values.vInfluence[l] = m_mHotSpots[agentID].vInfluenceAmount[l] * (1 - distRatioX);
//	//						}
//	//				
//	//					}
//	//					else
//	//					{
//	//						values.vInfluence[l] = 0;
//	//					}
//	//				}
//	//			}
//	//			else
//	//			{
//	//				float value, bestValue,
//	//					distance;
//
//	//				bestValue = 0;
//
//	//				for(l = 0; l < m_iInfLayers; l++)
//	//				{
//	//					for(j = 0; j < (int)m_mDoorMap[room1].size(); j++)
//	//					{
//	//						for(k = 0; k < (int)m_mDoorMap[room2].size(); k++)
//	//						{
//	//							vDistance = node->m_vPosition - m_mDoorMap[room1][j]->m_vPosition;
//
//	//							distance = sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));
//
//	//							vDistance = m_mHotSpots[agentID].position - m_mDoorMap[room2][k]->m_vPosition;
//
//	//							distance += sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));
//
//	//							distance += m_mDistToDoors[m_mDoorMap[room1][j]->ID][m_mDoorMap[room2][k]->ID];
//	//					
//	//							if(distance < m_mHotSpots[agentID].vInfluenceRange[l])
//	//							{
//	//								distRatioX = distance / (m_mHotSpots[agentID].vInfluenceRange[l]);
//
//	//								if(GetFalloffPointByRange(m_mHotSpots[agentID].vFalloffLayers[l], distRatioX, 0, falloffColor))
//	//								{
//	//									value = ((float)falloffColor.r / (float)255) * m_mHotSpots[agentID].vInfluenceAmount[l];
//	//								}
//	//								else
//	//								{
//	//									values.vInfluence[l] = m_itHotSpotIterator->second.vInfluenceAmount[l] * (1 - distRatioX);
//	//								}
//	//								if((m_mHotSpots[agentID].vInfluenceAmount[l] >= 0 && value > bestValue) ||
//	//									(m_mHotSpots[agentID].vInfluenceAmount[l] < 0 && value < bestValue))
//	//								{
//	//									bestValue = value;
//	//								}
//	//							}
//	//							else
//	//							{
//	//								value = 0;
//
//	//								if((m_mHotSpots[agentID].vInfluenceAmount[l] >= 0 && value > bestValue) ||
//	//									(m_mHotSpots[agentID].vInfluenceAmount[l] < 0 && value < bestValue))
//	//								{
//	//									bestValue = value;
//	//								}
//	//							}
//	//						}
//	//					}
//	//					values.vInfluence[l] = bestValue;
//	//				}
//	//			}
//	//		}
//
//	//		if(!relative)
//	//			node->mValues[m_mHotSpots[agentID].teamID] = values;
//	//		else
//	//		{
//	//			if(node->mValues[m_mHotSpots[agentID].teamID].vInfluence.size() < values.vInfluence.size())
//	//				node->mValues[m_mHotSpots[agentID].teamID].vInfluence.resize(values.vInfluence.size());
//
//	//			for(UINT i = 0; i < values.vInfluence.size(); i++)
//	//			{
//	//				node->mValues[m_mHotSpots[agentID].teamID].vInfluence[i] += values.vInfluence[i];
//	//			}
//	//		}
//	//	}
//	//}
////}

void InfluenceMap::ClearAllNodes()
{
	UINT i, j, k, roomSize, teamSize;
	//for each room
	for(m_MapSetIterator = m_mMapNodeSets.begin(); m_MapSetIterator != m_mMapNodeSets.end(); m_MapSetIterator++)
	{
		roomSize = m_MapSetIterator->second.size();
		//for each node in the room
		for(i = 0; i < roomSize; i++)
		{
			teamSize = m_vTeamIDs.size();
			//for each team
			for(j = 0; j < teamSize; j++)
			{
				//for each map layer
				for(k = 0; k < m_iInfLayers; k++)
				{
					ClearNode(m_MapSetIterator->second[i]);
				}
			}
		}
	}
}

void InfluenceMap::ClearNode(std::shared_ptr<infNode>node)
{
	//remove all influence data
	node->mValues.clear();
}

void InfluenceMap::ResetInfluences()
{
	UINT i, j, k, roomSize, teamSize;
	//for each room
	for(m_MapSetIterator = m_mMapNodeSets.begin(); m_MapSetIterator != m_mMapNodeSets.end(); m_MapSetIterator++)
	{
		roomSize = m_MapSetIterator->second.size();
		//for each node in the room
		for(i = 0; i < roomSize; i++)
		{
			teamSize = m_vTeamIDs.size();
			//for each team
			for(j = 0; j < teamSize; j++)
			{
				//for each map layer
				for(k = 0; k < m_iInfLayers; k++)
				{
					ResetValues(m_MapSetIterator->second[i], m_vTeamIDs[j]);
				}
			}
		}
	}
}

void InfluenceMap::ResetValues(std::shared_ptr<infNode> node, UINT teamID)
{
	//empties the vector
	node->mValues[teamID].m_vTeamInfluence.clear();
	node->mValues[teamID].m_vTeamInfluence.resize(m_iInfLayers, 0);
}

void InfluenceMap::RemoveAllNodes()
{
	mp_mMinMax.clear();
	m_mMapNodeSets.clear();
}

std::shared_ptr<infNode> InfluenceMap::GetNearestNode(Vector3 position)
{
	std::shared_ptr<infNode> nearestNode(nullptr);

	float minDistance = FLT_MAX;
	float distance;
	Vector3 vDistance;
	UINT size;

	//for each room
	for(m_MapSetIterator = m_mMapNodeSets.begin(); m_MapSetIterator != m_mMapNodeSets.end(); m_MapSetIterator++)
	{
		//for each node in the room
		size = m_MapSetIterator->second.size();
		for(UINT i = 0; i < size; i++)
		{
			vDistance = position - m_MapSetIterator->second[i]->m_vPosition;

			distance = (vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z);
			//current distance is the closest so far
			if(distance < minDistance)
			{
				minDistance = distance;
				nearestNode = m_MapSetIterator->second[i];
			}
		}
	}
	return nearestNode;
}

std::shared_ptr<infNode> InfluenceMap::GetNearestNodeInRoom(UINT iRoomID, Vector3 position)
{
	std::shared_ptr<infNode> nearestNode(nullptr);

	float minDistance = FLT_MAX;
	float distance;
	Vector3 vDistance;
	UINT size = m_mMapNodeSets[iRoomID].size();

	//for each node in the room
	for(UINT i = 0; i < size; i++)
	{
		vDistance = position - m_mMapNodeSets[iRoomID][i]->m_vPosition;

		distance = sqrt((vDistance.x * vDistance.x) + (vDistance.y * vDistance.y) + (vDistance.z * vDistance.z));
		//current distance is the closest so far
		if(distance < minDistance)
		{
			minDistance = distance;
			nearestNode = m_mMapNodeSets[iRoomID][i];
		}
	}
	return nearestNode;
}

void InfluenceMap::CombineInfluences_Node(eInfluenceCalculationTypes calculationMethod, std::shared_ptr<infNode> node, UINT targetTeam, UINT resultMap, const std::vector<UINT> &maps)
{
	//stop if an invalid node has been given
	if(node == NULL) return;

	//vectors for holding combination data
	std::vector<float> influence;
	std::vector<float> tension;
	std::vector<float> teamFactor;

	//counters
	UINT i, j, l;
	int currentMap;

	influence.resize(m_iInfLayers, 0);
	tension.resize(m_iInfLayers, 0);
	teamFactor.resize(m_iInfLayers, 0);

	//combine using influence method or the 1st half of vulnerability method
	if(calculationMethod == eInfluence || calculationMethod == eVulnerability)
	{			
		for(l = 0; l < maps.size(); l++)
		{
			//get the current desired map
			currentMap = maps[l];

			//resize the team value vector if it is too small
			if(node->mValues[currentMap].m_vTeamInfluence.size() < m_iInfLayers)
			{
				node->mValues[currentMap].m_vTeamInfluence.resize(m_iInfLayers, 0);
			}
			//checking for the alliance reputation for the target team
			if(DoesReputationExist(m_mmTeamReputation[targetTeam], currentMap))
			{
				//get the reputation for the current map layer
				for(j = 0; j < m_iInfLayers; j++)
				{
					teamFactor[j] = m_mmTeamReputation[targetTeam][currentMap].GetValue(j);
				}
			}
			else
			{
				//if reputation does not exist, substitute a default
				for(j = 0; j < m_iInfLayers; j++)
				{
					teamFactor[j] = DEFAULT_ENEMY_REPUTATION -1.0f;
				}
			}
			//adjust team influences by their associated layer reputations
			for(j = 0; j < m_iInfLayers; j++)
			{					
				influence[j] += node->mValues[currentMap].m_vTeamInfluence[j] * teamFactor[j];
			}
		}
	}
	//combine with tension method or 2nd half of vulnerability method
	if(calculationMethod == eTension || calculationMethod == eVulnerability)
	{
		for(j = 0; j < m_iInfLayers; j++)
		{
			for(l = 0; l < maps.size(); l++)
			{
				currentMap = maps[l];

				//resize the team value vector if it is too small
				if(node->mValues[currentMap].m_vTeamInfluence.size() <= m_iInfLayers)
				{
					node->mValues[currentMap].m_vTeamInfluence.resize(m_iInfLayers, 0);
				}
				tension[j] += node->mValues[currentMap].m_vTeamInfluence[j];
			}
		}
	}
	if(calculationMethod == eInfluence)
	{
		node->mValues[resultMap].m_vCombinedInfluence = influence;
	}
	else if(calculationMethod == eTension)
	{
		node->mValues[resultMap].m_vCombinedInfluence = tension;
	}
	else if(calculationMethod == eVulnerability)
	{
		for(i = 0; i < m_iInfLayers; i++)
		{			
				influence[i] = tension[i] - abs(influence[i]);
		}
		node->mValues[resultMap].m_vCombinedInfluence = influence;
	}
}

void InfluenceMap::CombineAllInfluences_Node(eInfluenceCalculationTypes calculationMethod, std::shared_ptr<infNode> node, UINT targetTeam, UINT resultMap)
{
	if(node == NULL) return;

	std::vector<float> influence;
	std::vector<float> tension;
	std::vector<float> teamFactor;

	UINT i, j, l;
	int currentMap;

	influence.resize(m_iInfLayers, 0);
	tension.resize(m_iInfLayers, 0);
	teamFactor.resize(m_iInfLayers, 0);

	//combine using influence method or the 1st half of vulnerability method
	if(calculationMethod == eInfluence || calculationMethod == eVulnerability)
	{			
		for(l = 0; l < m_vTeamIDs.size(); l++)
		{
			currentMap = m_vTeamIDs[l];

			//resize if the vector is too small
			if(node->mValues[currentMap].m_vTeamInfluence.size() <= m_iInfLayers)
			{
				node->mValues[currentMap].m_vTeamInfluence.resize(m_iInfLayers, 0);
			}
			//checking for the alliance reputation for the target team
			if(DoesReputationExist(m_mmTeamReputation[targetTeam], currentMap))
			{
				for(j = 0; j < m_iInfLayers; j++)
				{
					teamFactor[j] = m_mmTeamReputation[targetTeam][currentMap].GetValue(j);
				}
			}
			else
			{
				for(j = 0; j < m_iInfLayers; j++)
				{
					teamFactor[j] = DEFAULT_ENEMY_REPUTATION;
				}
			}
			for(j = 0; j < m_iInfLayers; j++)
			{					
				influence[j] += node->mValues[currentMap].m_vTeamInfluence[j] * teamFactor[j];
			}
		}
	}
	//combine using tension method or the 2nd half of vulnerability method
	if(calculationMethod == eTension || calculationMethod == eVulnerability)
	{
		for(j = 0; j < m_iInfLayers; j++)
		{
			for(l = 0; l < m_vTeamIDs.size(); l++)
			{
				currentMap = m_vTeamIDs[l];
				//resize if the vector is too small
				if(node->mValues[currentMap].m_vTeamInfluence.size() < m_iInfLayers)
				{
					node->mValues[currentMap].m_vTeamInfluence.resize(m_iInfLayers, 0);
				}
				tension[j] += node->mValues[currentMap].m_vTeamInfluence[j];
			}
		}
	}

	if(calculationMethod == eInfluence)
	{
		node->mValues[resultMap].m_vCombinedInfluence = influence;
	}
	else if(calculationMethod == eTension)
	{
		node->mValues[resultMap].m_vCombinedInfluence = tension;
	}
	else if(calculationMethod == eVulnerability)
	{
		for(i = 0; i < m_iInfLayers; i++)
		{			
			influence[i] = tension[i] - abs(influence[i]);
		}
		node->mValues[resultMap].m_vCombinedInfluence = influence;
	}
}

void InfluenceMap::CombineInfluences_Room(eInfluenceCalculationTypes calculationMethod, UINT iRoomID, UINT targetTeam, UINT resultMap, const std::vector<UINT> &maps)
{
	m_MapSetIterator = m_mMapNodeSets.find(iRoomID);

	if(m_MapSetIterator == m_mMapNodeSets.end()) return;

	std::vector<float> influence;
	std::vector<float> tension;
	std::vector<float> teamFactor;

	UINT i, j, k, l, size;
	int currentMap;

	std::shared_ptr<infNode> node;

	size = m_mMapNodeSets[iRoomID].size();
	//run through all the nodes in the room
	for(k = 0; k < size; k++)
	{
		influence.assign(m_iInfLayers, 0);
		tension.assign(m_iInfLayers, 0);
		teamFactor.assign(m_iInfLayers, 0);

		node = m_mMapNodeSets[iRoomID][k];

		//use the influence method or the 1st part of the vulnerability method
		if(calculationMethod == eInfluence || calculationMethod == eVulnerability)
		{			
			for(l = 0; l < maps.size(); l++)
			{
				currentMap = maps[l];

				if(node->mValues[currentMap].m_vTeamInfluence.size() < m_iInfLayers)
				{
					node->mValues[currentMap].m_vTeamInfluence.resize(m_iInfLayers, 0);;
				}

				//checking for the alliance reputation for the target team
				if(DoesReputationExist(m_mmTeamReputation[targetTeam], currentMap))
				{
					//getting the reputation value for this specific layer
					for(j = 0; j < m_iInfLayers; j++)
					{
						teamFactor[j] = m_mmTeamReputation[targetTeam][currentMap].GetValue(j);
					}
				}
				else
				{
					//substitute a default value
					for(j = 0; j < m_iInfLayers; j++)
					{
						teamFactor[j] = DEFAULT_ENEMY_REPUTATION -1.0f;
					}
				}

				//adjust the value by the reputation score
				for(j = 0; j < m_iInfLayers; j++)
				{					
					influence[j] += node->mValues[currentMap].m_vTeamInfluence[j] * teamFactor[j];
				}
			}
		}
		//use the tension method or the 2nd part of the vulnerability method
		if(calculationMethod == eTension || calculationMethod == eVulnerability)
		{
			for(j = 0; j < m_iInfLayers; j++)
			{
				for(l = 0; l < maps.size(); l++)
				{				
					currentMap = maps[l];
					if(node->mValues[currentMap].m_vTeamInfluence.size() < m_iInfLayers)
					{
						node->mValues[currentMap].m_vTeamInfluence.resize(m_iInfLayers, 0);
					}
					tension[j] += node->mValues[currentMap].m_vTeamInfluence[j];
				}
			}
		}

		if(calculationMethod == eInfluence)
		{
			node->mValues[resultMap].m_vCombinedInfluence = influence;
		}
		else if(calculationMethod == eTension)
		{
			node->mValues[resultMap].m_vCombinedInfluence = tension;
		}
		else if(calculationMethod == eVulnerability)
		{
			for(i = 0; i < m_iInfLayers; i++)
			{			
				influence[i] = tension[i] - abs(influence[i]);
			}
			node->mValues[resultMap].m_vCombinedInfluence = influence;
		}
	}
}

void InfluenceMap::CombineAllInfluences_Room(eInfluenceCalculationTypes calculationMethod, UINT iRoomID, UINT targetTeam, UINT resultMap)
{
	m_MapSetIterator = m_mMapNodeSets.find(iRoomID);

	if(m_MapSetIterator == m_mMapNodeSets.end()) return;

	std::vector<float> influence;
	std::vector<float> tension;
	std::vector<float> teamFactor;

	UINT i, j, k, l;
	int currentMap;

	std::shared_ptr<infNode> node;

	for(k = 0; k < m_mMapNodeSets[iRoomID].size(); k++)
	{
		influence.assign(m_iInfLayers, 0);
		tension.assign(m_iInfLayers, 0);
		teamFactor.assign(m_iInfLayers, 0);

		node = m_mMapNodeSets[iRoomID][k];

		if(calculationMethod == eInfluence || calculationMethod == eVulnerability)
		{			
			for(l = 0; l < m_vTeamIDs.size(); l++)
			{
				currentMap = m_vTeamIDs[l];

				if(node->mValues[currentMap].m_vTeamInfluence.size() < m_iInfLayers)
				{
					node->mValues[currentMap].m_vTeamInfluence.resize(m_iInfLayers, 0);
				}

				//checking for the alliance reputation for the target team
				if(DoesReputationExist(m_mmTeamReputation[targetTeam], currentMap))
				{
					for(j = 0; j < m_iInfLayers; j++)
					{
						teamFactor[j] = m_mmTeamReputation[targetTeam][currentMap].GetValue(j);
					}
				}
				else
				{
					for(j = 0; j < m_iInfLayers; j++)
					{
						teamFactor[j] = DEFAULT_ENEMY_REPUTATION -1.0f;
					}
				}
				for(j = 0; j < m_iInfLayers; j++)
				{					
					influence[j] += node->mValues[currentMap].m_vTeamInfluence[j] * teamFactor[j];
				}
			}
		}
		if(calculationMethod == eTension || calculationMethod == eVulnerability)
		{
			for(j = 0; j < m_iInfLayers; j++)
			{
				for(l = 0; l < m_vTeamIDs.size(); l++)
				{

					currentMap = m_vTeamIDs[l];
					if(node->mValues[currentMap].m_vTeamInfluence.size() < m_iInfLayers)
					{
						node->mValues[currentMap].m_vTeamInfluence.resize(m_iInfLayers, 0);
					}
					tension[j] += node->mValues[currentMap].m_vTeamInfluence[j];
				}
			}
		}

		if(calculationMethod == eInfluence)
		{
			node->mValues[resultMap].m_vCombinedInfluence = influence;
		}
		else if(calculationMethod == eTension)
		{
			node->mValues[resultMap].m_vCombinedInfluence = tension;
		}
		else if(calculationMethod == eVulnerability)
		{
			for(i = 0; i < m_iInfLayers; i++)
			{
				influence[i] = tension[i] - abs(influence[i]);
			}
			node->mValues[resultMap].m_vCombinedInfluence = influence;
		}
	}
}

void InfluenceMap::CombineInfluences_Map(eInfluenceCalculationTypes calculationMethod, UINT targetTeam, UINT resultMap, const std::vector<UINT> &maps)
{
	std::vector<float> influence;
	std::vector<float> tension;
	std::vector<float> teamFactor;

	UINT i, j, k, l;
	int currentMap;

	std::shared_ptr<infNode> node;

	for(m_MapSetIterator = m_mMapNodeSets.begin(); m_MapSetIterator != m_mMapNodeSets.end(); m_MapSetIterator++)
	{
		for(k = 0; k < m_mMapNodeSets[m_MapSetIterator->first].size(); k++)
		{
			influence.assign(m_iInfLayers, 0);
			tension.assign(m_iInfLayers, 0);
			teamFactor.assign(m_iInfLayers, 0);

			node = m_MapSetIterator->second[k];

			if(calculationMethod == eInfluence || calculationMethod == eVulnerability)
			{			
				for(l = 0; l < maps.size(); l++)
				{
					currentMap = maps[l];

					if(node->mValues[currentMap].m_vTeamInfluence.size() < m_iInfLayers)
					{
						node->mValues[currentMap].m_vTeamInfluence.resize(m_iInfLayers, 0);
					}

					//checking for the alliance reputation for the target team
					if(DoesReputationExist(m_mmTeamReputation[targetTeam], currentMap))
					{
						for(j = 0; j < m_iInfLayers; j++)
						{
							teamFactor[j] = m_mmTeamReputation[targetTeam][currentMap].GetValue(j);
						}
					}
					else
					{
						for(j = 0; j < m_iInfLayers; j++)
						{
							teamFactor[j] = DEFAULT_ENEMY_REPUTATION -1.0f;
						}
					}

					for(j = 0; j < m_iInfLayers; j++)
					{					
						influence[j] += node->mValues[currentMap].m_vTeamInfluence[j] * teamFactor[j];
					}
				}
			}
			if(calculationMethod == eTension || calculationMethod == eVulnerability)
			{
				for(j = 0; j < m_iInfLayers; j++)
				{
					for(l = 0; l < maps.size(); l++)
					{
						currentMap = maps[l];
						if(node->mValues[currentMap].m_vTeamInfluence.size() < m_iInfLayers)
						{
							node->mValues[currentMap].m_vTeamInfluence.resize(m_iInfLayers, 0);
						}
						tension[j] += node->mValues[currentMap].m_vTeamInfluence[j];
					}
				}
			}

			if(calculationMethod == eInfluence)
			{
				node->mValues[resultMap].m_vCombinedInfluence = influence;
			}
			else if(calculationMethod == eTension)
			{
				node->mValues[resultMap].m_vCombinedInfluence = tension;
			}
			else if(calculationMethod == eVulnerability)
			{
				for(i = 0; i < m_iInfLayers; i++)
				{
					influence[i] = tension[i] - abs(influence[i]);
				}
				node->mValues[resultMap].m_vCombinedInfluence = influence;
			}
		}
	}
}

void InfluenceMap::CombineAllInfluences_Map(eInfluenceCalculationTypes calculationMethod, UINT targetTeam, UINT resultMap)
{
	std::vector<float> influence;
	std::vector<float> tension;
	std::vector<float> teamFactor;

	UINT i, j, k, l;
	int currentMap;

	std::shared_ptr<infNode> node;

	for(m_MapSetIterator = m_mMapNodeSets.begin(); m_MapSetIterator != m_mMapNodeSets.end(); m_MapSetIterator++)
	{
		for(k = 0; k < m_mMapNodeSets[m_MapSetIterator->first].size(); k++)
		{
			influence.assign(m_iInfLayers, 0);
			tension.assign(m_iInfLayers, 0);
			teamFactor.assign(m_iInfLayers, 0);

			node = m_MapSetIterator->second[k];

			if(calculationMethod == eInfluence || calculationMethod == eVulnerability)
			{			
				for(l = 0; l < m_vTeamIDs.size(); l++)
				{
					currentMap = m_vTeamIDs[l];

					if(node->mValues[currentMap].m_vTeamInfluence.size() < m_iInfLayers)
					{
						node->mValues[currentMap].m_vTeamInfluence.resize(m_iInfLayers, 0);
					}

					//checking for the alliance reputation for the target team
					if(DoesReputationExist(m_mmTeamReputation[targetTeam], currentMap))
					{
						for(j = 0; j < m_iInfLayers; j++)
						{
							teamFactor[j] = m_mmTeamReputation[targetTeam][currentMap].GetValue(j);
						}
					}
					else
					{
						for(j = 0; j < m_iInfLayers; j++)
						{
							teamFactor[j] = DEFAULT_ENEMY_REPUTATION -1.0f;
						}
					}

					for(j = 0; j < m_iInfLayers; j++)
					{					
						influence[j] += node->mValues[currentMap].m_vTeamInfluence[j] * teamFactor[j];
					}
				}
			}
			if(calculationMethod == eTension || calculationMethod == eVulnerability)
			{
				for(j = 0; j < m_iInfLayers; j++)
				{
					for(l = 0; l < m_vTeamIDs.size(); l++)
					{
						currentMap = m_vTeamIDs[l];
						if(node->mValues[currentMap].m_vTeamInfluence.size() < m_iInfLayers)
						{
							node->mValues[currentMap].m_vTeamInfluence.resize(m_iInfLayers, 0);
						}
						tension[j] += node->mValues[currentMap].m_vTeamInfluence[j];
					}
				}
			}

			if(calculationMethod == eInfluence)
			{
				node->mValues[resultMap].m_vCombinedInfluence = influence;
			}
			else if(calculationMethod == eTension)
			{
				node->mValues[resultMap].m_vCombinedInfluence = tension;
			}
			else if(calculationMethod == eVulnerability)
			{
				for(i = 0; i < m_iInfLayers; i++)
				{
					influence[i] = tension[i] - abs(influence[i]);
				}
				node->mValues[resultMap].m_vCombinedInfluence = influence;
			}
		}
	}
}

nodeValues InfluenceMap::ComputeRawNode(eInfluenceCalculationTypes eCalculationMethod,  const Vector3 &vPosition, int iRoomNum, UINT iTeamNum,
					bool bUseInterpolation, bool bAddInterpolation, bool bUncapRange, bool bTileTexture)
{
	std::shared_ptr<infNode> node = std::make_shared<infNode>(vPosition, iRoomNum);
	node->m_iRoomID = iRoomNum;

	UINT size = m_vTeamIDs.size();
	UINT i;

	for(i = 0; i < size; i++)
	{
		CalculateNode(node, m_vTeamIDs[i], bUseInterpolation, bAddInterpolation, bUncapRange, bTileTexture);
	}
	
	CombineAllInfluences_Node(eCalculationMethod, node, iTeamNum, iTeamNum);

	return node->mValues[iTeamNum];
}

nodeValues InfluenceMap::ComputeRawNode(const Vector3 &vPosition, int iRoomNum, UINT iTeamNum,
					bool bUseInterpolation, bool bAddInterpolation, bool bUncapRange, bool bTileTexture)
{
	std::shared_ptr<infNode> node = std::make_shared<infNode>(vPosition, iRoomNum);
	//node->m_iRoomID = iRoomNum;

	UINT size = m_vTeamIDs.size();
	UINT i;

	for(i = 0; i < size; i++)
	{
		CalculateNode(node, m_vTeamIDs[i], bUseInterpolation, bAddInterpolation, bUncapRange, bTileTexture);
	}

	return node->mValues[iTeamNum];
}

void InfluenceMap::GetAllInfluenceData(std::shared_ptr<infNode> node, std::vector<nodeValues> *resultValues)
{
	resultValues->clear();

	for(UINT i = 0; i < m_vTeamIDs.size(); i++)
	{
		resultValues->push_back(node->mValues[m_vTeamIDs[i]]);
	}
}

void InfluenceMap::Clean()
{
	/*==========================
	no longer necessary because of conversion to shared pointers

	//for each room
	for(m_MapSetIterator = m_mMapNodeSets.begin(); m_MapSetIterator != m_mMapNodeSets.end(); m_MapSetIterator++)
	{
		//delete each node in the room
		for(int i = 0; i < (int)m_MapSetIterator->second.size(); i++)
		{
			delete m_MapSetIterator->second[i];
		}
	}
	

	//delete all falloff maps
	for(int j = 0; j < (int)m_vFalloffMaps.size(); j++)
	{
		delete m_vFalloffMaps[j];
	}

	==========================*/
}

bool InfluenceMap::DoesReputationExist(std::map<UINT, infReputation> map, const UINT key)
{
	static std::map<UINT, infReputation>::iterator low;

	for(low = map.begin(); low != map.end(); low++)
	{
		if(low->first == key)
			return true;
	}
	return false;
}