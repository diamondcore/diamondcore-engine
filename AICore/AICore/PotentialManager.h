#ifndef __POTENTIALMANAGER_H
#define __POTENTIALMANAGER_H

#include <vector>
#include <map>
#include <math.h>
#include <memory>
#include <DCMathLib.h>
#include <string>

#include "InfluenceMap.h"

class nodeNeighbors
{
public:
	std::shared_ptr<infNode> m_pNode;
	std::vector<std::shared_ptr<nodeNeighbors>> m_vNeighbors;
	nodeNeighbors(std::shared_ptr<infNode> thisNode){m_pNode = thisNode;}
	float m_fPotentialResult;
};

class PotentialManager
{
protected:
	InfluenceMap m_cPotential;

	std::map<unsigned int, UINT> m_mTeamNames;
	std::map<int, std::map<int, infReputation>> m_mAgentPriorities;
	std::map<UINT, Vector3> m_mBestTargets;
	Vector3 vLastBestTarget;
	std::map<UINT,std::shared_ptr<HotSpot>> hotspots;

	std::vector<std::shared_ptr<nodeNeighbors>> m_vNodes;

	std::vector<std::pair<float, Vector3>> m_vProbableTargets;

	UINT m_iMapSizeX,
		m_iMapSizeY,
		m_iMapSizeZ,
		m_iCurrentTeam,
		m_iSize;

	int m_iCurrentElement;

	bool bRelativeColor;

public:
	PotentialManager();
	~PotentialManager(){};

	void SetNumOfSubsets(UINT iNum){m_cPotential.SetNumOfLayers(iNum);}
	void BuildNeighbors(float fNeighborRange);
	void AddNode(Vector3 vPosition){
		std::shared_ptr<nodeNeighbors> temp = std::make_shared<nodeNeighbors>((m_cPotential.AddNode(0, vPosition))); m_vNodes.push_back(temp); m_iSize++;}
	float GetNodeResult(UINT iNodeNum){return m_vNodes[iNodeNum]->m_fPotentialResult;}
	void CreateTeam(unsigned int dTeamName){m_mTeamNames[dTeamName] = m_iCurrentTeam; m_iCurrentTeam++;}
	int AddElement(unsigned int dTeamName, Vector3 vPosition, bool bUseNearestNode, bool bForceCheck);
	bool AddElement(int iElementID, unsigned int dTeamName,  Vector3 vPosition, bool bUseNearestNode, bool bForceCheck);
	bool EditElement(int iEntityID, unsigned int dTeamName,  Vector3 vPosition, bool bUseNearestNode, bool bForceCheck);
	void AddElementData(int iEntityID, float fStrength, float fRange = 0, std::string sFalloff = ""){
		m_cPotential.AddHotSpotLayer(iEntityID, fStrength, fRange, sFalloff);
		hotspots[iEntityID] = m_cPotential.GetHotSpot(iEntityID);
	}
	void EditElementData(int iEntityID, UINT iNum, float fStrength, float fRange, std::string sFalloff){
		m_cPotential.SetHotSpotLayer(iEntityID, iNum, fStrength, fRange, sFalloff);
	}
	void RemoveElementData(int iEntityID){m_cPotential.RemoveHotSpot(iEntityID);}
	void AddFalloffData(std::string sFalloff){m_cPotential.GetFalloffMap(sFalloff, true);}
	void SetElementPriority(UINT iElement1, UINT iElement2, UINT iDataLayerNum, float fPriority){
		if(m_cPotential.GetNumOfLayers() > iDataLayerNum)
			m_cPotential.SetTeamReputation(iElement2, iElement2, iDataLayerNum, fPriority);
	}
	UINT GetNumOfElements(int iEntityID){return m_cPotential.GetNumOfHotSpotLayers(iEntityID);}
	UINT GetNumOfHotSpots(){return m_cPotential.GetNumOfHotSpots();}
	void SetAgentPriority(int iAgentID, unsigned int dTeamName, UINT iDataLayer, float fValue){
		m_mAgentPriorities[iAgentID][dTeamName].SetValue(iDataLayer, fValue);
	}

	void UpdateNodes(UINT iNumOfNodes = 0);
	void UpdateNodes(std::vector<D3DXMATRIX> &nodePos, std::vector<float> nodeValues, UINT iNumOfNodes = 0);
	bool GetBestTarget(Vector3 *vResult, UINT iAgentID, const Vector3 &vPosition, float fApproachRange, bool bDestroyOnReach);
	Vector3 Navigate(UINT iAgentID, const Vector3 &vPosition);

	UINT GetNumOfNodes(){return m_iSize;}
	void GetNodeInfo(UINT nodeNum, Vector3 &pos, float &result);

	void clearMap(){m_cPotential.RemoveAllNodes(); m_vNodes.clear();}
	void GetProbableTargets(std::vector<Vector3> *results);
};

#endif