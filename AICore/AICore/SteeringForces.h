#ifndef __STEERINGFORCES_H
#define __STEERINGFORCES_H

#include <DCMathLib.h>

enum e_ForceTypes
{
	SteeringForce_Seek = 0,
	SteeringForce_Flee,
	SteeringForce_Arrive
};

//class from which steering forces will be derived from
class SteeringForceBase
{
protected:
	Vector3 m_vLastResult,		//last result calculated
		m_vTarget,				//data for non-referenced target
		*m_pTarget;				//pointer to the target

	//strength of the force
	float m_fForceMagnitude;
	//type of force
	short m_sSteeringForceType;

public:	
	void SetMagnitude(float magnitude){m_fForceMagnitude = magnitude;}

	void UseStaticTarget(Vector3 target){m_vTarget = target; m_pTarget = &m_vTarget;}
	void UseExternalTarget(Vector3 *target){m_pTarget = target;}

	virtual int GetSteeringForceType() = 0;
	float GetMagnitude(){return m_fForceMagnitude;}
	Vector3 GetLastResult(){return m_vLastResult;}
	Vector3 GetTarget(){return *m_pTarget;}
	
	virtual void UpdateForce(Vector3 *result, Vector3 *vEntityPosition, Vector3 *vEntityVelocity) = 0;	
};

class SteeringForceSeek : public SteeringForceBase
{
protected:

public:
	//Set up force with a non-referenced target
	SteeringForceSeek(Vector3 target, float forceMagnitude);
	//Set up force with a referenced target
	SteeringForceSeek(Vector3 *target, float forceMagnitude);

	int GetSteeringForceType(){return SteeringForce_Seek;}

	void UpdateForce(Vector3 *result, Vector3 *vEntityPosition, Vector3 *vEntityVelocity);
};

class SteeringForceFlee : public SteeringForceBase
{
protected:

public:
	//Set up force with a non-referenced target
	SteeringForceFlee(Vector3 target, float forceMagnitude);
	//Set up force with a referenced target
	SteeringForceFlee(Vector3 *target, float forceMagnitude);

	int GetSteeringForceType(){return SteeringForce_Flee;}

	void UpdateForce(Vector3 *result, Vector3 *vEntityPosition, Vector3 *vEntityVelocity);
};

class SteeringForceArrive : public SteeringForceBase
{
protected:
	//distance from the target that the entity must stop by
	float m_fInnerRange;
	//distance value that controls the rate of deceleration
	float m_fOuterRange;

public:
	//Set up force with a non-referenced target
	SteeringForceArrive(Vector3 target, float innerRange, float outerRange, float forceMagnitude);
	//Set up force with a referenced target
	SteeringForceArrive(Vector3 *target, float innerRange, float outerRange, float forceMagnitude);

	void SetDecelerationRanges(float innerRange, float outerRange){m_fInnerRange = innerRange; m_fOuterRange = outerRange;}
	float GetInnerRange(){return m_fInnerRange;}
	float GetOuterRange(){return m_fOuterRange;}

	int GetSteeringForceType(){return SteeringForce_Arrive;}

	void UpdateForce(Vector3 *result, Vector3 *vEntityPosition, Vector3 *vEntityVelocity);
};


#endif