#ifndef __AICORE_H
#define __AICORE_H

#include "PotentialManager.h"
#include "SteeringForceManager.h"
#include "Pathfinding.h"

#include <string>
#include <map>
#include <vector>
#include <memory>

#include <DCMathLib.h>
#include <DCPhysicsLib.h>

class AgentProperties
{
public:
	AgentProperties(bool bDoPathing = false, bool bDoPotential = false);
	int m_iMovementID,
		m_iSharedID;
	bool m_bDoPathing,
		m_bDoPotential;
};

class AI_Core
{
private:

protected:
	PotentialManager m_cPotential;
	//SteeringForceManager m_cSteering;
	SteeringForceManager2 m_cSteering;
	PathfindingManager m_cPathfinding;

	//std::vector<std::shared_ptr<AIEntityData>> m_vAIAgents;
	//std::map<int, std::shared_ptr<AIEntityData>> m_mPotentialAgents;
	std::vector<std::shared_ptr<AgentProperties>> m_vAIAgents;
	std::map<int, std::shared_ptr<AgentProperties>> m_vAgentMap;
	//std::map<int, std::shared_ptr<SharedResource>> m_mPotentialAgents;

public:
	AI_Core();
	//static AI_Core *Instance(){static AI_Core instance; return &instance;}
	~AI_Core();

	/*=============General=============*/
	int AddEntity(int iSharedID, Vector3 vPosition, Vector3 vVelocity);
	void SetEntity(int iEntityID, Vector3 *vPosition = 0, Vector3 *vVelocity = 0);
	bool RemoveEntity(int iEntityID);
	//Vector3 GetEntityPosition(int iEntityID){return MMI->GetPosition(iEntityID);}
	//Quaternion
	void Update(float fTimeElapsed);

	/*=============Steering Forces=============*/
	void AddSeekForce(int iEntityID, Vector3 *vTarget, float fMagnitude,
		float fPriority, bool bUseDynamicTarget, unsigned int iRegistrationID){
			m_cSteering.AddSeekForce(iEntityID, vTarget, fMagnitude, fPriority, bUseDynamicTarget, iRegistrationID);
	}
	void AddFleeForce(int iEntityID, Vector3 *vTarget, float fMagnitude,
		float fPriority, bool bUseDynamicTarget, unsigned int iRegistrationID){
			m_cSteering.AddFleeForce(iEntityID, vTarget, fMagnitude, fPriority, bUseDynamicTarget, iRegistrationID);
	}
	void AddArriveForce(int iEntityID, Vector3 *vTarget, float fInnerRange, float fOuterRange, float fMagnitude,
		float fPriority, bool bUseDynamicTarget, unsigned int iRegistrationID){
			m_cSteering.AddArriveForce(iEntityID, vTarget, fInnerRange, fOuterRange, fMagnitude, fPriority, bUseDynamicTarget, iRegistrationID);
	}
	/*
	Adds a new force to the force registry/
	iEntityID is the entity that the force will be assigned to
	iforceType is the type of force that will be created
	vTarget is the target for the force
	fForceMagnitude is the strength of the force
	fInnerRange and fOuterRange are variables used in an Arrive force, if one is chosen
	fPriority is how important the new force is - determines where in the force list for the entity it is inserted
	bUseDynamicTarget determines if we want to follow the target as it changes, or use a snapshot of its current position
	iRegistrationID - if this is set to anything other than 0, the new force will get a unique ID that can be used to single it out later
	*/
	void AddForceToEntity(int iEntityID, UINT iForceType, Vector3 *vTarget, float fForceMagnitude,
		float fInnerRange, float fOuterRange,
		float fPriority, bool bUseDynamicTarget, unsigned int iRegistrationID);
	/*
	Removes a registered force
	iEntityID is the ID of the entity whose forces we want to look at.
	iRegistrationID is the ID of the force we want to remove.
	This function returns true if the force was found, false if not
	*/
	bool RemoveRegisteredForce(int iEntityID, unsigned int iRegistrationID){
		return m_cSteering.RemoveRegisteredForce(iEntityID, iRegistrationID);}
	/*
	Removes all forces of a given type from an entity's accumulated forces
	*/
	void RemoveForcesOfType(int iEntityID, UINT iForceType);
	/*
	Removes all forces being applied to an entity
	*/
	void RemoveAllForces(int iEntityID){m_cSteering.ClearForces(iEntityID);}
	/*
	Returns the accumulated forces for all forces of a specified entity
	iEntityId is the ID of the entity we want to use
	vPosition is the entity's current position
	vVelocity is the entity's current velocity
	bUseConstantVelocity determines if we want to use the current velocity, or adjust it for each additional force
	*/
	Vector3 GetEntityForces(int iEntityID, Vector3 &vPosition, Vector3 &vVelocity, bool bUseConstantVelocity);

	/*=============Potential Map : Decision-Making=============*/

	/*
	Adds a potential element to the potential map
	Returns the ID of the newly created element
	vPosition is the position of the element
	dMapTeamName is the team that the new element is associated with
		Due to some issues that still need to be worked out, teams are unavaliable - defaults to 0
	*/
	int AddPotentialElement(Vector3 vPosition, Vector3 vVelocity, unsigned int dMapTeamName);
	/*
	Edits the general properties of an existing potential element
	iElementID is the ID of the element
	dTeamName is the team the element is associated with
	vPosition is the position of the element
	bCheckIfExists forces the system to make sure the element exists before editing it
	*/
	void SetPotentialElement(int iElementID, unsigned int dTeamName, Vector3 vPosition, bool bCheckIfExists = true);
	/*
	Removes the potential element with the corresponding ID
	*/
	void RemovePotentialElement(int iElementID);
	/*
	Adds new map data to a potential element
	Returns the map # that the new data corresponds to
	fStrength = the strength of the element
	fRange = the distance the potential influence will travel
	sFalloff is the falloff texture data we want to use
	*/
	UINT AddPotentialElementData(int iElementID, float fStrength, float fRange, std::string sFalloff = "");
	/*
	Sets map data for an potential element
	iSetNum is the map whose associated data we want to edit
	*/
	void SetPotentialElementData(int iElementID, UINT iSetNum, float fStrength, float fRange, std::string sFalloff = "");
	/*
	Adds falloff data for the potential system.
	the data must be loaded through here before it can be used by the potential elements.
	*/
	void AddPotentialFalloffMap(std::string sDirectory){m_cPotential.AddFalloffData(sDirectory);}
	/*
	Sets the per-map priorities for an AI entity
	iEntityID is the ID of the AI entity
	dTeamName currently does nothing
	iDataSet is the map that we want to set the priority for
	fValue is the priority value we will use
	*/
	void SetAIEntityPriority(int iEntityID, UINT iDataSet, float fValue, unsigned int dTeamName = 0){
		m_cPotential.SetAgentPriority(iEntityID, dTeamName, iDataSet, fValue);}
	/*
	Gets the best potential target for an entity
	returns true if one was found, false otherwise
	result stores the decided best position to head towards
	*/
	bool GetBestPotentialTarget(Vector3 *result, UINT iAIEntityID, const Vector3 &vPosition, float fContactRange, bool bDestroyElementOnContact = false);
	void SetNumOfPotentialMaps(UINT iNumOfMaps){m_cPotential.SetNumOfSubsets(iNumOfMaps);}
	void SetPotentialUse(int iEntityID, bool bDoUse = true);

	/*=============Pathfinding=============*/

	void CreateDefaultMap(int iSize, int iMaxNumNodes){m_cPathfinding.GenerateGraph(iSize, iMaxNumNodes);}
	bool FindPath(int iEntityID, Vector3 *vPosition, Vector3 vDestination, float fMaxDistance = FLT_MAX);
	bool CheckPath(Vector3 *result, int iEntityID, Vector3 vPosition, float fMaxContactDistance, UINT iNumOfNodesToCheck){
		if(m_cPathfinding.UpdatePath(iEntityID, vPosition, fMaxContactDistance, iNumOfNodesToCheck))
		{
			return m_cPathfinding.GetPointInPath(result, iEntityID, 0);
		}
		return false;
	}
	void CoverNodes(Vector3 vSize, Vector3 vPosition);

	void SetPathingUse(int iEntityID, bool bDoUse = true);
};

#endif