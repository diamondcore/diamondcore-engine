#include "SteeringForces.h"

#include <math.h>


SteeringForceSeek::SteeringForceSeek(Vector3 target, float forceMagnitude)
{
	//sets a non-referenced target
	UseStaticTarget(target);
	m_fForceMagnitude = forceMagnitude;
}

SteeringForceSeek::SteeringForceSeek(Vector3 *target, float forceMagnitude)
{
	//sets a referenced target
	UseExternalTarget(target);
	m_fForceMagnitude = forceMagnitude;
}

void SteeringForceSeek::UpdateForce(Vector3 *result, Vector3 *vEntityPosition, Vector3 *vEntityVelocity)
{
	//getting relative position
	m_vLastResult = *m_pTarget - *vEntityPosition;

	//normalizing relative position
	D3DXVec3Normalize(&m_vLastResult, &m_vLastResult);
	//scaling relative position
	D3DXVec3Scale(&m_vLastResult, &m_vLastResult, m_fForceMagnitude);

	//storing result into inout
	*result = m_vLastResult;
}

SteeringForceFlee::SteeringForceFlee(Vector3 target, float forceMagnitude)
{
	//sets a non-referenced target
	UseStaticTarget(target);
	m_fForceMagnitude = forceMagnitude;
}

SteeringForceFlee::SteeringForceFlee(Vector3 *target, float forceMagnitude)
{
	//sets a referenced target
	UseExternalTarget(target);
	m_fForceMagnitude = forceMagnitude;
}

void SteeringForceFlee::UpdateForce(Vector3 *result, Vector3 *vEntityPosition, Vector3 *vEntityVelocity)
{
	//getting relative position
	m_vLastResult = *vEntityPosition - *m_pTarget;

	//normalizing relative position
	D3DXVec3Normalize(&m_vLastResult, &m_vLastResult);
	//scaling relative position
	D3DXVec3Scale(&m_vLastResult, &m_vLastResult, m_fForceMagnitude);

	//storing result into inout
	*result = m_vLastResult;
}

SteeringForceArrive::SteeringForceArrive(Vector3 target, float innerRange, float outerRange, float forceMagnitude)
{
	//setting non-referenced target
	UseStaticTarget(target);
	m_fInnerRange = innerRange;
	m_fOuterRange = outerRange;
	m_fForceMagnitude = forceMagnitude;
}

SteeringForceArrive::SteeringForceArrive(Vector3 *target, float innerRange, float outerRange, float forceMagnitude)
{
	//setting non-referenced target
	UseExternalTarget(target);
	m_fInnerRange = innerRange;
	m_fOuterRange = outerRange;
	m_fForceMagnitude = forceMagnitude;
}

void SteeringForceArrive::UpdateForce(Vector3 *result, Vector3 *vEntityPosition, Vector3 *vEntityVelocity)
{

	Vector3 directionNormal;

	//getting relative position
	m_vLastResult = *m_pTarget = *vEntityPosition;
	//normalizing relative direction
	D3DXVec3Normalize(&directionNormal, &m_vLastResult);
	//scaling relative direction by inner range
	D3DXVec3Scale(&directionNormal, &directionNormal, m_fInnerRange);

	//shortening relative position by minimum threshhold
	m_vLastResult -= directionNormal;

	//calculating the distance
	float distance = sqrt(m_vLastResult.x * m_vLastResult.x + m_vLastResult.y * m_vLastResult.y + m_vLastResult.z * m_vLastResult.z);

	//getting the deceleration force by getting inverse of entity velocity
	m_vLastResult = *vEntityVelocity * -1;

	//ajusting deceleration force
	if(distance != 0.0f)
		m_vLastResult *= (m_fOuterRange / distance) * m_fForceMagnitude;

	//returning result
	*result = m_vLastResult;
}