#include "Graph.h"

//returns a random double between zero and max node size
inline double RandFloat()      {return rand();}

Graph::Graph()
{
}
Graph::~Graph()
{
	Shutdown();
}
void Graph::Shutdown()
{
	while(!m_pNodes.empty())
	{
		delete m_pNodes[0];
		m_pNodes[0] = 0;
		m_pNodes.erase(m_pNodes.begin());
	}
	m_pNodes.clear();
}
void Graph::Reset()
{
	for (auto it = m_pNodes.begin(); it != m_pNodes.end(); it++)
		(*it)->Activate();
}

Vector3	Graph::getRandomSpawn() // Holds random spawn points
{
	spawnNode = RandFloat();
	spawnNode %= (m_pNodes.size()); //set Spawn Node
	
	if(!m_pNodes[spawnNode]->IsActivated())	// Make sure node is valid
		return getRandomSpawn();
	else
		return m_pNodes[spawnNode]->getPos();
}
void Graph::setRandomDestination()
{	
	Destination = RandFloat();
	Destination %= (m_pNodes.size()); //set new Destination
	if(!m_pNodes[Destination]->IsActivated())	
		setRandomDestination();
}
WTW::Node* Graph::getRandomDestination() const
{
	return m_pNodes[Destination];
}
void Graph::getActiveNodes(std::vector<D3DXMATRIX>& transforms) //D3DXMATRIX transforms, int& count)	// Gets an array of all active nodes
{
	// Temporarily store all active nodes
	std::vector<WTW::Node*> activeNodes;
	for(auto n = m_pNodes.begin(); n != m_pNodes.end(); n++)
		if( (*n)->IsActivated() )
			activeNodes.push_back( (*n) );
	
	// Prepare array
	//count = activeNodes.size();
	//transforms = new D3DXMATRIX[count];
	for(int i = 0; i < activeNodes.size(); i++)
	{
		D3DXMATRIX temp;
		Vector3 pos = activeNodes[i]->getPos();
		D3DXMatrixTranslation(&temp, pos.x, pos.y, pos.z);
		transforms.push_back(temp);
	}
}
void Graph::getDeactiveNodes(std::vector<D3DXMATRIX>& transforms) //D3DXMATRIX transforms[], int& count)	// Gets an array of all deactive nodes
{
	// Temporarily store all deactive nodes
	std::vector<WTW::Node*> deactiveNodes;
	for(auto n = m_pNodes.begin(); n != m_pNodes.end(); n++)
		if( !(*n)->IsActivated() )
			deactiveNodes.push_back( (*n) );
	
	// Prepare array
	//count = deactiveNodes.size();
	//transforms = new D3DXMATRIX[count];
	for(int i = 0; i < deactiveNodes.size(); i++)
	{
		D3DXMATRIX temp;
		Vector3 pos = deactiveNodes[i]->getPos();
		D3DXMatrixTranslation(&temp, pos.x, pos.y, pos.z);
		transforms.push_back(temp);
	}
}

float Graph::getDiff()
{	return diff;	}

// Auto assign Nodes to parameters
void Graph::generateNodes(int boundary, int maxNodes)
{
	diff = (boundary*boundary)/maxNodes; // Get the distance in space per nodes
	int id = 0;
	//boundary -=diff;		// (Uncomment to) not include nodes that are on the wall
	for(int y = 0; y <= boundary; y+=diff)
	{
		for(int x = 0; x <= boundary; x+=diff)
		{
			if(id == maxNodes) break;
			m_pNodes.push_back(new WTW::Node( id, Vector3(x+1.5f, 0.01f, y+1.5f) )); // Create node on graph
			id++;
			//x+=diff;
		}	
		if(id == maxNodes)
			break;
		//y+=diff;
	}
	numRows = ( (boundary*2) /diff ) +1; // Set the number of nodes
	diagCross = sqrt( (diff*diff) + (diff*diff) ); // Set the diagonal distance between nodes

	// Create the edges for the Node graph
	for(auto n = m_pNodes.begin();
		n != m_pNodes.end();
		n++)
			generateEdges( (*n) );
}
void Graph::generateEdges(WTW::Node* node) // Link nodes across and diagnal from this node
{
	node->ClearEdges();	// Reset Edge list

	for( auto B = m_pNodes.begin();
		B != m_pNodes.end();
		B++ )
		if( *node != *(*B) && 
				(node->getPos() - (*B)->getPos()).Magnitude() <=
				diagCross ) 
				node->AddEdge(	new WTW::Edge( node, (*B) ) ); // Store this Edge
}
const	std::vector<WTW::Node*>& Graph::getNodes() // Returns a vector of the Nodes on the graph
{
	return m_pNodes; // Returns a vector of the graphed Nodes
}
void Graph::CoverNodes(Vector3 size, Vector3 pos)	// Deactivate nodes covered by this object
{
	for(auto n = m_pNodes.begin();
		n != m_pNodes.end();
		n++)
	{
		D3DXVECTOR3 dist = (*n)->getPos();
		dist -= pos;	// Calculate distance

		// Calculate the squared dimentions of the object
		float boundingX = size.x*0.5f;	
		float boundingZ = size.z*0.5f;

		// Check if rhs is inside the area of the object
		if (fabsf(dist.x) > boundingX)
			continue;
		else if(fabsf(dist.z) > boundingZ)
			continue;
		(*n)->Deactivate();	// Position is within cell radius
	}
}
void Graph::ExposeNodes(Vector3 size, Vector3 pos)	// Deactivate nodes covered by this object
{
	for(auto n = m_pNodes.begin();
		n != m_pNodes.end();
		n++)
	{
		D3DXVECTOR3 dist = (*n)->getPos();
		dist -= pos;	// Calculate distance

		// Calculate the squared dimentions of the object
		float boundingX = size.x*0.5f;	
		float boundingZ = size.z*0.5f;

		// Check if rhs is inside the area of the object
		if (fabsf(dist.x) > boundingX)
			continue;
		else if(fabsf(dist.z) > boundingZ)
			continue;
		(*n)->Activate();	// Position is within cell radius
	}
}

// Manually add/remove nodes
void Graph::createNode(Vector3 pos)
{
	// Check if node already exist
	int availID = -1;
	for(auto n = m_pNodes.begin(); n != m_pNodes.end(); n++)
		if( !(*n) )							// Check if node is available
			availID = (*n)->getID();
		else if( (*n)->getPos() == pos )	// Check if node matches requested position
			return;
	
	// Create the new node and add to the list
	if( availID >= 0 )										// Replace a previously deleted node
		m_pNodes[availID] = new WTW::Node(availID, pos);
	else													// Add new node to end of vector
		m_pNodes.push_back( new WTW::Node( m_pNodes.size(), pos ) );
}
void Graph::createEdge(Vector3 posA, Vector3 posB)
{
	WTW::Node* tempA = NULL;
	WTW::Node* tempB = NULL;
	// Check if both nodes already exist
	for(auto n = m_pNodes.begin(); n != m_pNodes.end(); n++)
		if( (*n)->getPos() == posA )
		{
			tempA = (*n);
			if(tempB)		// If both have been found then stop searching
				break;
		}
		else if( (*n)->getPos() == posB )
		{
			tempB = (*n);
			if(tempA)		// If both have been found then stop searching
				break;
		}
	
	// If nodes weren't found then add them
	if( !tempA )
	{
		tempA = new WTW::Node( m_pNodes.size(), posA );
		m_pNodes.push_back( tempA );
	}
	if( !tempB )
	{
		tempB = new WTW::Node( m_pNodes.size(), posB );
		m_pNodes.push_back( tempB );
	}
	// Add Edges
	tempA->AddEdge( new WTW::Edge(tempA, tempB) );
	tempB->AddEdge( new WTW::Edge(tempB, tempA) );
}
void Graph::removeNode(Vector3 pos)
{
	// Find the node at this position
	for(int n = 0; n < m_pNodes.size(); n++)
		if( m_pNodes[n]->getPos() == pos )
		{
			delete m_pNodes[n];	// Delete the node
			m_pNodes[n] = 0;
			break;
		}
}