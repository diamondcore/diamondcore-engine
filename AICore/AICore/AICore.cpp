#include "AICore.h"
#include <iostream>

#define POTENTIALSTEERING 1
#define PATHFINDINGSTEERING 2

AgentProperties::AgentProperties(bool bDoPathing, bool bDoPotential)
{
	m_bDoPathing = bDoPathing;
	m_bDoPotential = bDoPotential;
}

AI_Core::AI_Core()
{
}

AI_Core::~AI_Core()
{
}

/*=============General=============*/
int AI_Core::AddEntity(int iSharedID, Vector3 vPosition, Vector3 vVelocity)
{
	int id = MMI->CreateBall(vPosition, 1.0f);
	MMI->SetVelocity(id, vVelocity);

	//AgentProperties a;
	//a.m_iMovementID = id;
	//a.m_bDoPathing = false;
	//a.m_bDoPotential = false;
	//m_vAIAgents.push_back(a);

	std::shared_ptr<AgentProperties> data = std::make_shared<AgentProperties>(true, true);
	data->m_iMovementID = id;
	data->m_iSharedID= iSharedID;
	m_vAIAgents.push_back(data);
	m_vAgentMap[id] = data;

	//m_vAIAgents[id].m_pAgent = data;

	//std::shared_ptr<AIEntityData> data = std::make_shared<AIEntityData>(id);
	//data->SetPosition(vPosition);
	//data->SetQuaternion(MMI->GetOrientation(id));

	//m_vAIAgents.push_back(data);
	return id;
}

void AI_Core::SetEntity(int iEntityID, Vector3 *vPosition, Vector3 *vVelocity)
{
	if(vPosition)
		MMI->SetPosition(m_vAgentMap[iEntityID]->m_iMovementID, *vPosition);
	if(vVelocity)
		MMI->SetVelocity(m_vAgentMap[iEntityID]->m_iMovementID, *vVelocity);
}

bool AI_Core::RemoveEntity(int iEntityID)
{
	m_vAgentMap.erase(iEntityID);

	std::vector<std::shared_ptr<AgentProperties>>::iterator it = m_vAIAgents.begin();
	UINT size = m_vAIAgents.size();

	for(UINT i = 0; i < size; i++)
	{
		if(it->get()->m_iMovementID == iEntityID)
		{
			m_vAIAgents.erase(it);
			return true;
		}
	}
	return false;
}

void AI_Core::Update(float fTimeElapsed)
{
	MMI->Update(fTimeElapsed);

	std::vector<std::shared_ptr<AgentProperties>>::iterator it;
	Vector3 result;

	for(it = m_vAIAgents.begin(); it != m_vAIAgents.end(); it++)
	{
		//why was I hardcoding this?

		//temp = it->m_pAgent;
		//temp->m_vPosition = MMI->GetPosition(temp->m_iEntityID);
		//temp->m_qOrientation = MMI->GetOrientation(temp->m_iEntityID);
		//if(it->m_bDoPathing)
		//{
		//	m_cPathfinding.UpdatePath(temp->m_iEntityID, temp->m_vPosition, 2, 1);
		//	if(m_cPathfinding.GetPointInPath(&result, temp->m_iEntityID, 0))
		//		m_cSteering.SetRegisteredForce(temp->m_iEntityID, PATHFINDINGSTEERING, &result);
		//	else
		//		m_cSteering.SetRegisteredForce(temp->m_iEntityID, PATHFINDINGSTEERING, &result, 0.0f);
		//}
		//if(it->m_bDoPotential)
		//{
		//	m_cPotential.GetBestTarget(&result, temp->m_iEntityID, temp->m_vPosition, 1, false);
		//	m_cSteering.SetRegisteredForce(temp->m_iEntityID, POTENTIALSTEERING, &result);
		//}
		//m_cSteering.UpdateForces(temp->m_iEntityID, &temp->m_vPosition, &MMI->GetVelocity(temp->m_iEntityID), true);
	}
}

/*=============Steering Forces=============*/
void AI_Core::AddForceToEntity(int iEntityID, UINT iForceType, Vector3 *vTarget, float fForceMagnitude,
		float fInnerRange, float fOuterRange,
		float fPriority, bool bUseDynamicTarget, unsigned int iRegistrationID)
{
	if(iForceType == SteeringForce_Seek)
	{
		m_cSteering.AddSeekForce(iEntityID, vTarget, fForceMagnitude, fPriority, bUseDynamicTarget, iRegistrationID);
		return;
	}
	if(iForceType == SteeringForce_Flee)
	{
		m_cSteering.AddFleeForce(iEntityID, vTarget, fForceMagnitude, fPriority, bUseDynamicTarget, iRegistrationID);
		return;
	}
	if(iForceType == SteeringForce_Arrive)
	{
		m_cSteering.AddArriveForce(iEntityID, vTarget, fInnerRange, fOuterRange, fForceMagnitude, fPriority, bUseDynamicTarget, iRegistrationID);
		return;
	}
}

void AI_Core::RemoveForcesOfType(int iEntityID, UINT iForceType)
{
	//m_cSteering.RemoveAllForcesOfType(iEntityID, iForceType, iStartElement, iEndElement);
	m_cSteering.RemoveAllForcesOfType(iEntityID, iForceType);
}

Vector3 AI_Core::GetEntityForces(int iEntityID, Vector3 &vPosition, Vector3 &vVelocity, bool bUseConstantVelocity)
{
	//return m_cSteering.UpdateForces(iEntityID, &vPosition, &vVelocity, bUseConstantVelocity, 0, 0);
	return m_cSteering.UpdateForces(iEntityID, &vPosition, &vVelocity, bUseConstantVelocity);
}

/*=============Potential Map=============*/
int AI_Core::AddPotentialElement(Vector3 vPosition, Vector3 vVelocity, unsigned int dMapTeamName)
{
	//int idNum = MMI->CreateBall(vPosition, 1.0f);
	//MMI->SetVelocity(idNum, vVelocity);
	//int id = m_cPotential.AddElement(0, vPosition, false, true);
	//m_mPotentialAgents[idNum] = std::make_shared<AIEntityData>(id);
	//m_mPotentialAgents[idNum]->SetPosition(vPosition);
	//m_mPotentialAgents[idNum]->SetQuaternion(MMI->GetOrientation(idNum));
	//return idNum;
	int id = m_cPotential.AddElement(0, vPosition, false, true);
	return id;
}

void AI_Core::SetPotentialElement(int iEntityID, unsigned int dTeamName, Vector3 vPosition, bool bCheckIfExists)
{
	m_cPotential.EditTeamElement(iEntityID, 0, vPosition, false, bCheckIfExists);
}

void AI_Core::RemovePotentialElement(int iElementID)
{
	m_cPotential.RemoveElementData(iElementID);
}

UINT AI_Core::AddPotentialElementData(int iEntityID, float fStrength, float fRange, std::string sFalloff)
{
	m_cPotential.AddElementData(iEntityID, fStrength, fRange, sFalloff);
	return m_cPotential.GetNumOfElements(iEntityID) - 1;
}

void AI_Core::SetPotentialElementData(int iElementID, UINT iSetNum, float fStrength, float fRange, std::string sFalloff)
{
	m_cPotential.EditElementData(iElementID, iSetNum, fStrength, fRange, sFalloff);
}

bool AI_Core::GetBestPotentialTarget(Vector3 *result, UINT iAIEntityID, const Vector3 &vPosition, float fContactRange, bool bDestroyElementOnContact)
{
	return m_cPotential.GetBestTarget(result, iAIEntityID, vPosition, fContactRange, bDestroyElementOnContact);
}

void AI_Core::SetPotentialUse(int iEntityID, bool bDoUse)
{
	m_vAgentMap[iEntityID].get()->m_bDoPotential = bDoUse;

	//why was I hardcoding this?

	//if(bDoUse)
	//{
	//	Vector3 v;
	//	m_cPotential.GetBestTarget(&v, iEntityID, m_vAIAgents[iEntityID].m_pAgent->m_vPosition, 2, false);
	//	m_cSteering.AddSeekForce(iEntityID, &v, 1, 1, false, POTENTIALSTEERING);
	//	return;
	//}
	//m_cSteering.RemoveRegisteredForce(iEntityID, POTENTIALSTEERING);
}

/*=============Pathfinding=============*/

bool AI_Core::FindPath(int iEntityID, Vector3 *vPosition, Vector3 vDestination, float fMaxDistance)
{
	return m_cPathfinding.FindPath(iEntityID, vPosition, vDestination, fMaxDistance);	
}

void AI_Core::CoverNodes(Vector3 vSize, Vector3 vPosition)
{
	m_cPathfinding.GetGraph()->CoverNodes(vSize, vPosition);
}

void AI_Core::SetPathingUse(int iEntityID, bool bDoUse)
{
	m_vAgentMap[iEntityID].get()->m_bDoPathing = bDoUse;

	//why was I hardcoding this?

	//if(bDoUse)
	//{
	//	Vector3 v;
	//	m_cSteering.AddSeekForce(iEntityID, &v, 1, 0, false, PATHFINDINGSTEERING);
	//	return;
	//}
	//m_cSteering.RemoveRegisteredForce(iEntityID, PATHFINDINGSTEERING);
}