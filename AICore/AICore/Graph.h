#pragma once

#include "Node.h"
#include <vector>

class Graph
{
	int		Destination, spawnNode; // Astar Random goal Node ID
	float	diff, diagCross;		// Space between nodes
	int		numRows;
	std::vector<WTW::Node*> m_pNodes;

public:
	Graph();
	~Graph();

	void	Shutdown();
	void	Reset();

	// Accessor Methods
	Vector3		getRandomSpawn();				// Holds random spawn points
	void		setRandomDestination();
	WTW::Node*	getRandomDestination() const;
	const std::vector<WTW::Node*>&	getNodes(); // Returns a vector of the Nodes on the graph

	float	getDiff();
	float	getDiag()const		{return diagCross;}
	int		getNumRows()const	{return numRows;}
	int		getNumNodes()const	{return m_pNodes.size();}
	void	getActiveNodes(std::vector<D3DXMATRIX>& transforms); //D3DXMATRIX transforms[], int& count);	// Gets an array of all active nodes
	void	getDeactiveNodes(std::vector<D3DXMATRIX>& transforms); //D3DXMATRIX transforms[], int& count);	// Gets an array of all deactive nodes

public:
	// Manually Create Nodes
	void	createNode(Vector3 pos);
	void	createEdge(Vector3 posA, Vector3 posB);
	void	removeNode(Vector3 pos);

public:
	// Auto Create Nodes to parameters
	void	generateNodes(int boundary, int maxNodes); 
	void	generateEdges(WTW::Node* node);				// Auto assign Edges
	void	CoverNodes(Vector3 size, Vector3 pos);	// Deactivate nodes covered by this object
	void	ExposeNodes(Vector3 size, Vector3 pos);	// Activate nodes covered by this object
};