#include "Path.h"

inline double EuclideanDist(Vector3 from, Vector3 to)
{
	double tempZ = abs(to.z - from.z);
	double tempX = abs(to.x - from.x);

	return sqrt(tempZ*tempZ + tempX*tempX); 
}
Path::Path(Graph* graph)
	:m_bIsTraveling(true),
	 m_fMaxSpeed(20.0f),
	 m_pGraph(graph)
{
	//if(m_bIsTraveling)
	//	searchPathNode();
}
Path::~Path()
{

}
void Path::SetPath(Vector3* owner, Vector3 end)
{
	m_pOwnerPos = owner;
	SetOwner(owner);
	SetDestination(end);
	if(m_bIsTraveling)
		searchPathNode();
}

WTW::Node* Path::GetNextNode()
{
	return m_NodeList.front();
}
Vector3 Path::UpdateForce(int numNodesToCheck)
{
	float radius = 3.0f;

	//if on a node further along the path, store the iterator here
	int largest = -1;
	//nodeList iterator
	int count;
	//make sure we don't go out of bounds on our checking
	numNodesToCheck = min(numNodesToCheck, m_NodeList.size());
	//check ahead the defined amount of nodes
	for(count = 0; count < numNodesToCheck; count++)
	{
		if(EuclideanDist(m_NodeList[count]->getPos(), m_pNodeOwner->getPos()) <= radius)
		{
			largest = count;
		}
	}
	//if we have found a node that is within our radius, delete it and all previous nodes
	if(largest != -1)
	{
		for(count = 0; count <= largest; count++)
		{
			m_NodeList.erase(m_NodeList.begin());
		}
	}
	//if(!m_NodeList.empty() && 
	//	EuclideanDist(m_NodeList[0]->getPos(), m_pNodeOwner->getPos()) <=
	//	radius)	// Owner reaches node, then deletes node, and goes to the next node
	//	{
	//		if(m_NodeList.size() > 1)
	//			m_NodeList.erase(m_NodeList.begin());
	//		else
	//		{
	//			//MDI->TransferMessage(0, m_pOwner->getID(), m_pOwner->getID(),
	//			//			_lostTarget, _StateMachine, (bool*)false);
	//			m_bIsTraveling = 0;
	//			return Vector3();
	//		}
	//	}
	if(m_NodeList.empty() &&		// If node is empty and still not at destination
		EuclideanDist(m_pNodeEnd->getPos(), m_pNodeOwner->getPos()) > radius)
			searchPathNode();		// Research for best nodes path
	else if(m_NodeList.empty())
	{
		//MDI->TransferMessage(0, m_pOwner->getID(), m_pOwner->getID(),
		//					_lostTarget, _StateMachine, (bool*)false);
		m_bIsTraveling = 0;
		return Vector3();
	}

	Vector3 toTarget = m_NodeList[0]->getPos() - m_pNodeOwner->getPos();
	double dist = toTarget.MagnitudeSquared();
	return toTarget;	
	if (dist > 0)	// using Arrival type of algorithm
	{
		// Calculate the speed required to reach the target given the desired
		// Deceleration
		double speed =  dist / ((double)2 * 0.3f);	//DecelerationTweaker);     

		// Make sure the velocity does not exceed the max
		speed = min(speed, m_fMaxSpeed);	//((speed < MAX_SPEED) ? (speed) : (MAX_SPEED)); // Min(speed, MAX_SPEED)

		Vector3 DesiredVelocity =  toTarget * speed / dist;
		return DesiredVelocity;
	}
	return Vector3();
}

void Path::SetOwner(Vector3* owner)
{
	m_bIsTraveling = 1;

	m_pNodeOwner = NULL;
	m_pOwnerPos = owner;

	nodeID = -1;
	for(auto i = m_pGraph->getNodes().begin(); 
		i != m_pGraph->getNodes().end();
		i++) 
	{
		if(EuclideanDist((*i)->getPos(), *owner) <= m_pGraph->getDiff())
		{
			if(!(*i)->IsActivated())
				continue;
			// Create temp nodes to represent position, bc that's what the function takes in for paramaters
			if( nodeID != -1 &&
				(*i)->getEuclideanCostTo(*owner) <=	
				m_pGraph->getNodes()[nodeID]->getEuclideanCostTo(*owner)) 
					nodeID = (*i)->getID(); // If node is closer then other node
			else if( nodeID == -1)
				nodeID = (*i)->getID(); // Initialize node
		}
	}
	if(nodeID >= 0 && nodeID < m_pGraph->getNodes().size())
		m_pNodeOwner = m_pGraph->getNodes()[nodeID];
	else
		m_bIsTraveling = 0;
}

void Path::SetDestination(Vector3 destination)
{
	m_bIsTraveling = 1;

	m_pNodeEnd = NULL;

	int destID = -1;
	for(auto i = m_pGraph->getNodes().begin(); 
		i != m_pGraph->getNodes().end();
		i++) 
	{
		if(EuclideanDist((*i)->getPos(), destination) <= m_pGraph->getDiff())
		{
			if(!(*i)->IsActivated())
				continue;
			// Create temp nodes to represent position, bc that's what the function takes in for paramaters
			if( destID != -1 &&
				(*i)->getEuclideanCostTo(destination) <=	
				m_pGraph->getNodes()[destID]->getEuclideanCostTo(destination)) 
					destID = (*i)->getID(); // If node is closer then other node
			else if( destID == -1)
				destID = (*i)->getID(); // Initialize node
		}
	}
	if(destID >= 0 && destID < m_pGraph->getNodes().size())
		m_pNodeEnd = m_pGraph->getNodes()[destID];
	else
		m_bIsTraveling = 0;
}

std::vector<Vector3> Path::GetPath()
{
	// Get all the path's positions
	std::vector<Vector3> pathPositions;
	for(auto i = m_NodeList.begin(); i != m_NodeList.end(); i++)
		pathPositions.push_back( (*i)->getPos() );
	return pathPositions;
}
void Path::GetPathNodes(std::vector<D3DXMATRIX>& transforms)	// Used for drawing the path
{
	// Prepare array
	for(int i = 0; i < m_NodeList.size(); i++)
	{
		D3DXMATRIX temp;
		Vector3 pos = m_NodeList[i]->getPos();
		D3DXMatrixTranslation(&temp, pos.x, pos.y, pos.z);
		transforms.push_back(temp);
	}
}
bool Path::IsTraveling()
{	return m_bIsTraveling;	}
void Path::SetMaxSpeed(float maxSpeed)
{	m_fMaxSpeed = maxSpeed;	}

void Path::setNode()
{
	nodeID = -1;
	for(auto i = m_pGraph->getNodes().begin(); 
		i != m_pGraph->getNodes().end();
		i++)
		if(EuclideanDist((*i)->getPos(), *m_pOwnerPos) <= m_pGraph->getDiff())
		{
			if(!(*i)->IsActivated())
				continue;
			// Create temp nodes to represent position, bc that's what the function takes in for paramaters
			if( nodeID != -1 &&
				(*i)->getEuclideanCostTo(*m_pOwnerPos) <=	
				m_pGraph->getNodes()[nodeID]->getEuclideanCostTo(*m_pOwnerPos)) 
					nodeID = (*i)->getID(); // If node is closer then other node
			else if( nodeID == -1)
				nodeID = (*i)->getID(); // Initialize node
		}
	if(nodeID != -1)
		m_pNodeOwner = m_pGraph->getNodes()[nodeID];
}
void Path::searchPathNode()
{
	setNode(); // Set closest node as starting node
	m_bVisited.resize( m_pGraph->getNumNodes() );

	if(nodeID < 0)
		m_bIsTraveling = 0;
		//MDI->TransferMessage(0, m_pOwner->getID(), m_pOwner->getID(), 
		//					_lostTarget, _StateMachine, (bool*)false);
	else
	{
		m_NodeList.clear();
		m_NodeList.push_back(m_pGraph->getNodes()[nodeID]);	// Set First Node
		currentG = 0.0f;
		if(maxG < 0.0)
			Search(m_pGraph->getNodes()[nodeID], m_pNodeEnd);//ASI->getRandomDestination());
		else
			SearchWithMax(m_pGraph->getNodes()[nodeID], m_pNodeEnd);
	}
}
void Path::Search(WTW::Node* start, WTW::Node* end)
{	
	if(start == end)
	{
		m_bVisited.clear();
		return;
	}

	if( m_bVisited[start->getID()] ) return;

	m_bVisited[start->getID()] = true;
	WTW::Edge* traveledPath = NULL;

	for(auto E = start->getEdges().begin();
		E != start->getEdges().end();
		E++)
	{
		// Check if Edge is still active (update status)
		(*E)->Update();

		// Check if Edge is accessible
		if( !(*E)->IsActivated() ) continue;
		if(m_NodeList.size() > 1 && (*E)->getNodeB() == m_NodeList[m_NodeList.size()-2])	
			continue;			// Don't back travel
		if(traveledPath == NULL) traveledPath = (*E);		// Set first edge as test

		// Set Edge's F value at current location
		(*E)->getNodeB()->setF( (currentG + (*E)->getG()) + 
								(*E)->getNodeB()->getEuclideanCostTo(end) );
		
		if( (*E)->getNodeB()->getF() < traveledPath->getNodeB()->getF() ) // Set first Edge
			traveledPath = (*E); // Store best path
	}
	if(traveledPath != NULL)
	{
		m_NodeList.push_back(traveledPath->getNodeB()); // Set path
		currentG += traveledPath->getG(); // Update distance traveled
	
		Search(m_NodeList[m_NodeList.size()-1], end);
		return;
	}
}
void Path::SearchWithMax(WTW::Node* start, WTW::Node* end)
{	
	if(start == end)
	{
		m_bVisited.clear();
		return;
	}

	if( m_bVisited[start->getID()] ) return;

	m_bVisited[start->getID()] = true;
	WTW::Edge* traveledPath = NULL;

	for(auto E = start->getEdges().begin();
		E != start->getEdges().end();
		E++)
	{
		// Check if Edge is still active (update status)
		(*E)->Update();

		// Check if Edge is accessible
		if( !(*E)->IsActivated() ) continue;
		if(m_NodeList.size() > 1 && (*E)->getNodeB() == m_NodeList[m_NodeList.size()-2])	
			continue;			// Don't back travel
		if(traveledPath == NULL) traveledPath = (*E);		// Set first edge as test

		// Set Edge's F value at current location
		(*E)->getNodeB()->setF( (currentG + (*E)->getG()) + 
								(*E)->getNodeB()->getEuclideanCostTo(end) );
		
		if( (*E)->getNodeB()->getF() < traveledPath->getNodeB()->getF() ) // Set first Edge
			traveledPath = (*E); // Store best path
	}
	if(traveledPath != NULL)
	{
		if(currentG + traveledPath->getG() <= maxG)
		{
			m_NodeList.push_back(traveledPath->getNodeB()); // Set path
			currentG += traveledPath->getG(); // Update distance traveled
	
			SearchWithMax(m_NodeList[m_NodeList.size()-1], end);
		}
		return;
	}
}