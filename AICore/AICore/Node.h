#pragma once

#include <DCMathLib.h>
#include <list>

namespace WTW
{
class Edge;
	class Node
	{
	public:
		Node(int id, Vector3 pos);
		~Node();

		void Activate();
		void Deactivate();
		bool IsActivated();
		void	SetGraphicsID(int id){m_GraphicsID = id;}

		Vector3 getPos()			{return m_vPosition;}
		int		getID()const		{return m_ID;}
		int		getGraphicsID()		{return m_GraphicsID;}
		float	getRadius()const	{return m_fRadius;}

		//set euclidean value from this Node to Destination
		float	getEuclideanCostTo(Node* end);
		float	getEuclideanCostTo(Vector3& entity);
		void	setF(float tempF)	{m_F = tempF;} //store temp F for current path
		float	getF()				{return m_F;}

		std::list<Edge*>& getEdges() {return m_pEdges;}
		void AddEdge(Edge* edge);
		void ClearEdges();
		void UpdateEdges(); //update accessibility status

		bool operator== (const Node &rhs) const;
		bool operator!= (const Node &rhs) const;

	private:
		Vector3 m_vPosition;
		Vector3 m_vColor;
		int		m_ID,
				m_GraphicsID;
		float	m_fRadius, 
				m_F;
		bool	m_bActivated; // Displays if the node is accessible (on/off)
	
		// List of edges connect to Node
		std::list<Edge*> m_pEdges; 
	};


	/****************Edge*****************/
	class Edge
	{
	protected:
		void	Activate();
		void	Deactivate();

	public:
		Edge(Node* A, Node* B);
		~Edge(){}
		bool	operator== (const Edge &rhs) const;

		void	Update();		// Updates Activity status
		bool	IsActivated();
		float	getG()			{return weightG;}

		Node*	getNodeA()		{return nodeA;}	// The Owner is Node A
		Node*	getNodeB()		{return nodeB;}
		int		getID()const	{return m_ID;}

	private:
		Node*	nodeA,	*nodeB;
		float	weightG;
		int		m_ID; 
		bool	m_bActivated;
	};
}