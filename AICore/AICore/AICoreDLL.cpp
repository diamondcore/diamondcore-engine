#include "AICoreDLL.h"
#include <Windows.h>
#include <GraphicsDLLInterface.h>
#include "FSM\AI_FSM.h"

#define AITICKINTERVAL 2.0
#define AIDEBUG_POTENTIAL_POSX 0
#define AIDEBUG_POTENTIAL_POSY 0
#define AIDEBUG_POTENTIAL_POSZ 0
#define AIDEBUG_POTENTIAL_SIZEX 100
#define AIDEBUG_POTENTIAL_SIZEY 0
#define AIDEBUG_POTENTIAL_SIZEZ 100
#define AIDEBUG_POTENTIAL_NODEDENSITY 5

#define AIFORCE_POTENTIAL 1

AICoreDLL::AICoreDLL()
{
	//m_pAICore = NULL;
	m_bIsPaused = false;
	m_fTickInterval = AITICKINTERVAL;
	m_fCurrentTime = 0.0f;

	m_bUseSharedIDs = true;

#if AICOREDEBUG
	
	//std::vector<WTW::Node*> nodes = m_Graph.getNodes();
	//UINT size = nodes.size();
	////sDebugInfo info;	
	//int sharedID;

	//for(UINT i = 0; i < size; i++)
	//{
	//	sharedID = m_pSharedMemory->CreateSharedResource();
	//	m_pSharedMemory->GetSharedResource(sharedID)->SetPosition(nodes[i]->getPos());
	//	auto data = MessageData();
	//	data.m_mMeshData.m_bUseShadowMap = false;
	//	strcpy_s(data.m_mMeshData.m_cMeshname, 5, "Box.x");
	//	m_pMessageMgr->DispatchMsg(sharedID, CT_GRAPHICS, msg_CreateMesh, &data);
	//}

	//info.qOrientation = Quaternion();
	//info.bIsOnPath = false;
	//info.fPotentialValue = 0.0f;

	//for(UINT i = 0; i < size; i++)
	//{
	//	sharedID = m_pSharedMemory->CreateSharedResource();
	//	info.vPosition = nodes[i]->getPos();
	//}
	
	////create potential nodes
	//UINT x, z;
	//for(x = 0; x < AIDEBUG_POTENTIAL_SIZEX; x += AIDEBUG_POTENTIAL_SIZEX / AIDEBUG_POTENTIAL_NODEDENSITY)
	//{
	//	for(z = 0; z < AIDEBUG_POTENTIAL_SIZEZ; z += AIDEBUG_POTENTIAL_SIZEZ / AIDEBUG_POTENTIAL_NODEDENSITY)
	//	{
	//	}
	//}
#endif
}

AICoreDLL::~AICoreDLL()
{
	m_Graph.Shutdown();
}

int AICoreDLL::AddEntity(int &iSharedID, float fPositionX, float fPositionY, float fPositionZ,
		float fVelocityX, float fVelocityY, float fVelocityZ)
{
	int id = MMI->CreateBall(Vector3(fPositionX, fPositionY, fPositionZ), 1.0f);
	MMI->SetVelocity(id, Vector3(fVelocityX, fVelocityY,fVelocityZ));
	MMI->SetPosition(id, Vector3(fPositionX, fPositionY, fPositionZ));
	Quaternion q;
	MMI->SetOrientation(id, q);

	if(iSharedID == -1)
	{
		iSharedID = m_pSharedMemory->CreateSharedResource();
		m_pSharedMemory->GetSharedResource(iSharedID)->SetPosition(Vector3(fPositionX, fPositionY, fPositionZ));
		m_pSharedMemory->GetSharedResource(iSharedID)->SetOrientation(Quaternion(0, 0, 0, 0));
	}
	std::shared_ptr<AgentProperties> data = std::make_shared<AgentProperties>(iSharedID, id,  true, true);
	data->SetGraph(&m_Graph);
	data->SetSteering(&m_cSteering);
	data->m_pPath->SetDestination(Vector3(0, 0, 0));
	data->m_pPath->SetOwner(&MMI->GetPosition(id));
	data->m_pPotential = &m_cPotential;
	//data->m_pStateMachine->ChangeState(AIWander);
	m_vAIAgents.push_back(data);
	if(m_bUseSharedIDs)
	{
		m_AgentMap[iSharedID] = data;
		return iSharedID;
	}

	m_AgentMap[id] = data;

	return id;
}

void AICoreDLL::SetEntity(int iEntityID, Vector3 *vPosition, Vector3 *vVelocity)
{
	if(vPosition)
		MMI->SetPosition(m_AgentMap[iEntityID]->m_iMovementID, *vPosition);
	if(vVelocity)
		MMI->SetVelocity(m_AgentMap[iEntityID]->m_iMovementID, *vVelocity);
}

bool AICoreDLL::RemoveEntity(int iEntityID)
{
	m_AgentMap.erase(iEntityID);
	std::vector<std::shared_ptr<AgentProperties>>::iterator it = m_vAIAgents.begin();
	UINT size = m_vAIAgents.size();
	if(m_bUseSharedIDs)
	{
		for(UINT i = 0; i < size; i++)
		{
			if(it->get()->m_iSharedID == iEntityID)
			{
				m_vAIAgents.erase(it);
				return true;
			}
		}
		return false;
	}
	for(UINT i = 0; i < size; i++)
	{
		if(it->get()->m_iMovementID == iEntityID)
		{
			m_vAIAgents.erase(it);
			return true;
		}
	}
	return false;
}


void AICoreDLL::AddSeekForce(int iEntityID, float *fTarget, float fMagnitude,
			float fPriority, bool bUseDynamicTarget, unsigned int iRegistrationID)
{
	m_cSteering.AddSeekForce(m_AgentMap[iEntityID]->m_iMovementID, (Vector3*)fTarget, fMagnitude, fPriority, bUseDynamicTarget, iRegistrationID);
}

void AICoreDLL::AddArriveForce(int iEntityID, float *fTarget, float fInnerRange, float fOuterRange, float fMagnitude,
			float fPriority, bool bUseDynamicTarget, unsigned int iRegistrationID)
{
	m_cSteering.AddArriveForce(m_AgentMap[iEntityID]->m_iMovementID, (Vector3*)fTarget, fInnerRange, fOuterRange, fMagnitude, fPriority, bUseDynamicTarget, iRegistrationID);
}

void AICoreDLL::AddFleeForce(int iEntityID, float *fTarget, float fMagnitude,
			float fPriority, bool bUseDynamicTarget, unsigned int iRegistrationID)
{
	m_cSteering.AddFleeForce(m_AgentMap[iEntityID]->m_iMovementID, (Vector3*)fTarget, fMagnitude, fPriority, bUseDynamicTarget, iRegistrationID);
}

bool AICoreDLL::RemoveRegisteredForce(int iEntityID, unsigned int iRegistrationID, bool bRemoveAllWithID)
{
	return m_cSteering.RemoveRegisteredForce(m_AgentMap[iEntityID]->m_iMovementID, iRegistrationID, bRemoveAllWithID);
}

void AICoreDLL::RemoveAllForces(int iEntityID)
{
	if(m_bUseSharedIDs)
	{
		m_cSteering.ClearForces(m_AgentMap[iEntityID]->m_iMovementID);
	}
}

void AICoreDLL::SetTarget(int iEntityID, Vector3 vTarget, bool bForceCheck)
{
	if(bForceCheck)
	{
		UINT size = m_vAIAgents.size();
		if(m_bUseSharedIDs)
		{
			for(UINT i = 0; i < size; i++)
			{
				if(m_vAIAgents[i]->GetSharedID() == iEntityID)
				{
					m_vAIAgents[i]->SetTarget(vTarget);
					return;
				}
			}
		}
		for(UINT i = 0; i < size; i++)
		{
			if(m_vAIAgents[i]->GetMovementID() == iEntityID)
			{
				m_vAIAgents[i]->SetTarget(vTarget);
				return;
			}
		}
		return;
	}
	m_AgentMap[iEntityID]->SetTarget(vTarget);
}

void AICoreDLL::SetState(int iEntityID, int iState)
{
	if(iState == State_Idle)
	{
		m_AgentMap[iEntityID]->m_pStateMachine->ChangeState(AIIdle);
		return;
	}
	if(iState == State_Pathing)
	{
		m_AgentMap[iEntityID]->m_pStateMachine->ChangeState(AIIdle);
		return;
	}
	if(iState == State_Potential)
	{
		m_AgentMap[iEntityID]->m_pStateMachine->ChangeState(AIPotential);
		return;
	}
	if(iState == State_Flee)
	{
		m_AgentMap[iEntityID]->m_pStateMachine->ChangeState(AIFlee);
		return;
	}
	if(iState == State_Wander)
	{
		m_AgentMap[iEntityID]->m_pStateMachine->ChangeState(AIWander);
		return;
	}
}

int AICoreDLL::AddPotentialElement(float fPositionX, float fPositionY, float fPositionZ,
		float fVelocityX, float fVelocityY, float fVelocityZ, unsigned int dTeamName)
{
	int id = m_cPotential.AddElement(dTeamName, Vector3(fPositionX, fPositionY,fPositionZ), false, true);
	


	int iSharedID = id;

#if AICOREDEBUG
	iSharedID = m_pSharedMemory->CreateSharedResource();
	m_pSharedMemory->GetSharedResource(iSharedID)->SetPosition(Vector3(fPositionX, fPositionY, fPositionZ));
	//m_pSharedMemory->GetSharedResource(iSharedID)->SetOrientation(Quaternion(0, 0, 0, 0));

	auto data = MessageData();
	data.m_mMeshData.m_bUseShadowMap = false;
	strcpy_s(data.m_mMeshData.m_cMeshname, 5, "Box.x");
	m_pMessageMgr->DispatchMsg(iSharedID, CT_GRAPHICS, msg_CreateMesh, &data);

#endif

	m_vPotSources.push_back(std::pair<int, int>(iSharedID, id));

	return iSharedID;
}

void AICoreDLL::SetPotentialElement(int iElementID, float fPosX, float fPosY, float fPosZ,  unsigned int dTeamName, bool bCheckExists)
{
	std::vector<std::pair<int, int>>::iterator p_it;

	for(p_it = m_vPotSources.begin(); p_it != m_vPotSources.end(); p_it++)
	{
		if(p_it->first == iElementID)
		{
			m_cPotential.EditElement(p_it->second, dTeamName, Vector3(fPosX, fPosY, fPosZ), false, bCheckExists);
			return;
		}
	}
	//m_cPotential.EditElement(iElementID, dTeamName, Vector3(fPosX, fPosY, fPosZ), false, bCheckExists);
}

void AICoreDLL::RemovePotentialElement(int iElementID)
{
	std::vector<std::pair<int, int>>::iterator p_it;

	for(p_it = m_vPotSources.begin(); p_it != m_vPotSources.end(); p_it++)
	{
		if(p_it->first == iElementID)
		{
			m_cPotential.RemoveElementData(p_it->second);
#if AICOREDEBUG
			auto data = MessageData();
			data.m_mMeshData.m_bUseShadowMap = false;
			strcpy_s(data.m_mMeshData.m_cMeshname, 5, "Box.x");
			m_pMessageMgr->DispatchMsg(p_it->second, CT_GRAPHICS, msg_RemoveMesh, &data);
#endif
			return;
		}
	}
	//m_cPotential.RemoveElementData(iElementID);
}

UINT AICoreDLL::AddPotentialElementData(int iElementID, float fStrength, float fRange, std::string sFalloff)
{
	m_cPotential.AddElementData(iElementID, fStrength, fRange, sFalloff);
	return m_cPotential.GetNumOfElements(iElementID) - 1;
}

void AICoreDLL::SetPotentialElementData(int iElementID, UINT iSetNum, float fStrength, float fRange, std::string sFalloff)
{
	m_cPotential.EditElementData(iElementID, iSetNum, fStrength, fRange, sFalloff);
}

void AICoreDLL::AddPotentialFalloffMap(std::string sDirectory)
{
	m_cPotential.AddFalloffData(sDirectory);
}

void AICoreDLL::SetAIEntityPriority(int iEntityID, UINT iDataSet, float fValue, unsigned int dTeamName)
{
	m_cPotential.SetAgentPriority(iEntityID, dTeamName, iDataSet, fValue);
}

void AICoreDLL::SetAIPotentialUse(int iEntityID, bool bUsePotentialSystem)
{
	m_AgentMap[iEntityID].get()->m_bDoPotential = bUsePotentialSystem;
}

void AICoreDLL::CreateMap(int iSize, int iMaxNumNodes)
{
	m_Graph.Shutdown();
	m_Graph.generateNodes(iSize, iMaxNumNodes);

#if AICOREDEBUG
	std::vector<WTW::Node*> nodes = m_Graph.getNodes();
	UINT size = nodes.size();
	//sDebugInfo info;	
	int sharedID;

	for(UINT i = 0; i < size; i++)
	{
		sharedID = m_pSharedMemory->CreateSharedResource();
		m_pSharedMemory->GetSharedResource(sharedID)->SetPosition(nodes[i]->getPos());
		nodes[i]->SetGraphicsID(sharedID);
		auto data = MessageData();
		data.m_mMeshData.m_bUseShadowMap = false;
		strcpy_s(data.m_mMeshData.m_cMeshname, 6, "Box.x");
		m_pMessageMgr->DispatchMsg(sharedID, CT_GRAPHICS, msg_CreateMesh, &data);
	}
#endif
}

bool AICoreDLL::MakePath(int iEntityID, float fDestX, float fDestY, float fDestZ, float fMaxDist)
{
	m_AgentMap[iEntityID]->m_pPath.get()->SetMaxG(fMaxDist);
	m_AgentMap[iEntityID]->m_pPath.get()->SetPath(&m_pSharedMemory->GetSharedResource(m_AgentMap[iEntityID]->m_iSharedID)->GetPosition(),
		Vector3(fDestX, fDestY, fDestZ));
	return true;
	//return m_cPathfinding.FindPath(iEntityID, &MMI->GetPosition(iEntityID), Vector3(fDestX, fDestY, fDestZ), fMaxDist);
}

void AICoreDLL::CoverNodes(float fSizeX, float fSizeY, float fSizeZ,
		float fPosX, float fPosY, float fPosZ)
{
	m_Graph.CoverNodes(Vector3(fSizeX, fSizeY, fSizeZ), Vector3(fPosX, fPosY, fPosZ));
	//m_cPathfinding.GetGraph()->CoverNodes(Vector3(fSizeX, fSizeY, fSizeZ), Vector3(fPosX, fPosY, fPosZ));
}

void AICoreDLL::ExposeNodes(float fSizeX, float fSizeY, float fSizeZ,
		float fPosX, float fPosY, float fPosZ)
{
	m_Graph.ExposeNodes(Vector3(fSizeX, fSizeY, fSizeZ), Vector3(fPosX, fPosY, fPosZ));
	//m_cPathfinding.GetGraph()->CoverNodes(Vector3(fSizeX, fSizeY, fSizeZ), Vector3(fPosX, fPosY, fPosZ));
}

void AICoreDLL::SetAIPathingUse(int iEntityID, bool bUsePathingSystem)
{
	m_AgentMap[iEntityID].get()->m_bDoPathing= bUsePathingSystem;
}

void AICoreDLL::SetSharedIDs(bool set)
{
	m_bUseSharedIDs = set;

	std::map<int, std::shared_ptr<AgentProperties>> temp;
	std::map<int, std::shared_ptr<AgentProperties>>::iterator it;
	if(m_bUseSharedIDs)
	{
		for(it = m_AgentMap.begin(); it != m_AgentMap.end(); it++)
		{
			temp[it->second->m_iSharedID] = it->second;
		}
		return;
	}
	else
	{
		for(it = m_AgentMap.begin(); it != m_AgentMap.end(); it++)
		{
			temp[it->second->m_iMovementID] = it->second;
		}
	}
	m_AgentMap = temp;
}

void AICoreDLL::Steering()
{
	//std::cout<<"Updating AI Core Steering\n";
	std::vector<std::shared_ptr<AgentProperties>>::iterator it;
	AgentProperties* data;
	Vector3 result;
	int mID;

	for(it = m_vAIAgents.begin(); it != m_vAIAgents.end(); it++)
	{
		data = it->get();
		mID = data->m_iMovementID;
		result = m_cSteering.UpdateForces(mID,
									//&m_pSharedMemory->GetSharedResource(data->m_iSharedID)->GetPosition(),
									&MMI->GetPosition(mID),
									&MMI->GetVelocity(mID),
									true);
		MMI->AddForce(mID, result);
	}
}

void AICoreDLL::Pathing()
{
	//std::cout<<"Updating AI Core Pathfinding (Andrew)\n";
	std::vector<std::shared_ptr<AgentProperties>>::iterator it;
	AgentProperties* data;
	Vector3 result;
	int mID;

	for(it = m_vAIAgents.begin(); it != m_vAIAgents.end(); it++)
	{
		data = it->get();
		mID = data->m_iMovementID;
		if(data->m_bDoPathing && data->m_pPath->IsTraveling())
		{
			//it->get()->m_pPath->SetPath(&m_pSharedMemory->GetSharedResource(it->get()->m_iSharedID)->GetPosition(), it->get()->m_vTarget);
			result = it->get()->m_pPath->UpdateForce();
		}
		else
			result.x = result.y = result.z = 0.0;

		result += m_cSteering.UpdateForces(mID,
									&MMI->GetPosition(mID),
									&MMI->GetVelocity(mID),
									true);
		MMI->AddForce(mID, result);
	}
}

void AICoreDLL::Potential()
{
	//std::cout<<"Updating AI Core Potential System\n";
	std::vector<std::shared_ptr<AgentProperties>>::iterator it;
	AgentProperties* data;
	Vector3 result;
	int mID;

	for(it = m_vAIAgents.begin(); it != m_vAIAgents.end(); it++)
	{
		data = it->get();
		mID = data->m_iMovementID;
		if(data->m_bDoPotential)
		{
			if(m_cPotential.GetBestTarget(&result, mID, MMI->GetPosition(mID), 1, true))
			//if(m_cPotential.GetBestTarget(&result, mID, m_pSharedMemory->GetSharedResource(it->get()->m_iSharedID)->GetPosition(), 1, true))
			{
				data->m_pPath->SetPath(&MMI->GetPosition(mID), result);
				//it->get()->m_pPath->SetPath(&m_pSharedMemory->GetSharedResource(it->get()->m_iSharedID)->GetPosition(), result);
			}
		}
	}
}

void AICoreDLL::Update(float dt)
{
	ProcessMessages();
	std::vector<std::shared_ptr<AgentProperties>>::iterator it;
	AgentProperties* data;	

	if(m_bIsPaused)
	{
		for(it = m_vAIAgents.begin(); it != m_vAIAgents.end(); it++)
		{
			data = it->get();
			MMI->SetPosition(data->m_iMovementID, m_pSharedMemory->GetSharedResource(data->m_iSharedID)->GetPosition());
		}
	return;
	}

	int mID;
	m_fCurrentTime += dt;
	//Vector3 result(0, 0, 0);
	
	
	//If over the tick time, update the potential target and path to our destination
	if(m_fCurrentTime >= m_fTickInterval)
	{
		m_fCurrentTime = 0.0f;
		UINT size;
		std::vector<WTW::Node*> nodes;	
#if AICOREDEBUG
		nodes = m_Graph.getNodes();
		size = nodes.size();

		for(UINT i = 0; i < size; i++)
		{
			if(nodes[i]->IsActivated())
			{
				auto data = MessageData();
				data.m_mColorData.m_fColor[0] = 0;
				data.m_mColorData.m_fColor[1] = 0;
				data.m_mColorData.m_fColor[2] = 0;
				data.m_mColorData.m_fColor[3] = 1;
				strcpy_s(data.m_mColorData.m_cResourceName, 6, "Box.x");
				m_pMessageMgr->DispatchMsg(nodes[i]->getGraphicsID(), CT_GRAPHICS, msg_SetMeshColor, &data);
			}
			else
			{
				auto data = MessageData();
				data.m_mColorData.m_fColor[0] = 255;
				data.m_mColorData.m_fColor[1] = 0;
				data.m_mColorData.m_fColor[2] = 0;
				data.m_mColorData.m_fColor[3] = 1;
				strcpy_s(data.m_mColorData.m_cResourceName, 6, "Box.x");
				m_pMessageMgr->DispatchMsg(nodes[i]->getGraphicsID(), CT_GRAPHICS, msg_SetMeshColor, &data);
			}
		}
#endif
		for(it = m_vAIAgents.begin(); it != m_vAIAgents.end(); it++)
		{
			it->get()->m_pStateMachine->Update(dt);

#if AICOREDEBUG
			nodes = it->get()->m_pPath->GetPathList();
			size = nodes.size();
			for(UINT i = 0; i < size; i++)
			{
				auto data = MessageData();
				data.m_mColorData.m_fColor[0] = 255;
				data.m_mColorData.m_fColor[1] = 255;
				data.m_mColorData.m_fColor[2] = 0;
				data.m_mColorData.m_fColor[3] = 1;
				strcpy_s(data.m_mColorData.m_cResourceName, 6, "Box.x");
				m_pMessageMgr->DispatchMsg(nodes[i]->getGraphicsID(), CT_GRAPHICS, msg_SetMeshColor, &data);
			}
#endif
		}	
	}

	Steering();
	MMI->Update(dt);	

	Vector3 world(1, 0, 0);
	Vector3 normVel;
	Quaternion q;
	float dot;
	

	for(it = m_vAIAgents.begin(); it != m_vAIAgents.end(); it++)
	{	
		data = it->get();
		//data->m_pStateMachine->Update(dt);
		mID = data->m_iMovementID;

		auto newPos = MMI->GetPosition(mID);
		m_pSharedMemory->GetSharedResource(data->m_iSharedID)->SetPosition(newPos);

		normVel = MMI->GetVelocity(mID);
		normVel.Normalize();
		if(normVel.MagnitudeSquared() < 1)
			normVel.x = 1;

		//Setting the entity orientation to its velocity
		dot = normVel * world;
		//if vectors are paralell, skip this
		if(dot < 0.9999 && dot > -0.9999)
		{
			normVel.CrossProduct(normVel, world);
			q.x = normVel.x;
			q.y = normVel.y;
			q.z = normVel.z;		
			q.w = sqrt(normVel.MagnitudeSquared() * world.MagnitudeSquared()) + dot;
		}
		m_pSharedMemory->GetSharedResource(data->m_iSharedID)->SetOrientation(MMI->GetOrientation(mID));
	}

#if AICOREDEBUG
	//setting debug stuff
#endif
}

void AICoreDLL::Initialize()
{
	//std::cout<<"Initializing AI Core\n";
	//m_pAICore = new AI_Core();
	MMI->InitPhysics();
	m_fCurrentTime = 0.0f;
}

void AICoreDLL::Shutdown()
{
	//std::cout<<"Shutting down AI Core\n";
	m_Graph.Shutdown();
	//if(m_pAICore != NULL)
	//{
	//	std::cout<<"Deleting AI Core\n";
	//	delete m_pAICore;
	//	m_pAICore = NULL;
	//}
	//else
	//{
	//	std::cout<<"Unable to Delete AI Core - Core does not exist\n";
	//}
}

void AICoreDLL::Pause()
{
	m_bIsPaused = !m_bIsPaused;
}

void AICoreDLL::ProcessMessages()
{
	int imsg;
	// process all messages in queue
	Mail* message = NULL;
	while (m_sMyMessages.size() > 0)
	{
		// get message
		m_sMyMessages.GetNextMessage( &message );

		do{
			imsg = message->m_iMsg;
			//create an AI with a defined position, assume no velocity
			if(imsg == MSG_CreateEntity_Pos)
			{
				int id = AddEntity(message->m_mMessageData->m_mAIEntityData.m_iSharedID,
					message->m_mMessageData->m_mAIEntityData.m_fPosition[0],
					message->m_mMessageData->m_mAIEntityData.m_fPosition[1],
					message->m_mMessageData->m_mAIEntityData.m_fPosition[2]);
				SetAIPathingUse(id, message->m_mMessageData->m_mAIEntityData.m_bUsePathfinding);
				SetAIPotentialUse(id, message->m_mMessageData->m_mAIEntityData.m_bUsePotential);
				break;
			}
			//create an AI with a defined position and velocity
			if(imsg == MSG_CreateEntity_PosVel)
			{
				AddEntity(message->m_mMessageData->m_mAIEntityData.m_iSharedID,
					message->m_mMessageData->m_mAIEntityData.m_fPosition[0],
					message->m_mMessageData->m_mAIEntityData.m_fPosition[1],
					message->m_mMessageData->m_mAIEntityData.m_fPosition[2],
					message->m_mMessageData->m_mAIEntityData.m_fVelocity[0],
					message->m_mMessageData->m_mAIEntityData.m_fVelocity[1],
					message->m_mMessageData->m_mAIEntityData.m_fVelocity[2]);
				break;
			}
			//remove an entity
			if(imsg == MSG_RemoveEntity)
			{
				bool success = RemoveEntity(message->m_mMessageData->m_mAIEntityData.m_iSharedID);
				break;
			}
			if(imsg == MSG_AddForce)
			{
				int type = message->m_mMessageData->m_mAISteeringData.m_iForceType;
				if(type == Force_Seek)
				{
					AddSeekForce(message->m_mMessageData->m_mAISteeringData.m_iEntityID,
									message->m_mMessageData->m_mAISteeringData.m_fTarget,
									message->m_mMessageData->m_mAISteeringData.m_fMagnitude,
									message->m_mMessageData->m_mAISteeringData.m_fPriority,
									false,
									message->m_mMessageData->m_mAISteeringData.m_iRegistrationID);
					break;
				}
				if(type == Force_Flee)
				{
					AddFleeForce(message->m_mMessageData->m_mAISteeringData.m_iEntityID,
									message->m_mMessageData->m_mAISteeringData.m_fTarget,
									message->m_mMessageData->m_mAISteeringData.m_fMagnitude,
									message->m_mMessageData->m_mAISteeringData.m_fPriority,
									false,
									message->m_mMessageData->m_mAISteeringData.m_iRegistrationID);
					break;
				}
				if(type == Force_Arrive)
				{
					AddArriveForce(message->m_mMessageData->m_mAISteeringData.m_iEntityID,
									message->m_mMessageData->m_mAISteeringData.m_fTarget,
									message->m_mMessageData->m_mAISteeringData.m_fData[0],
									message->m_mMessageData->m_mAISteeringData.m_fData[1],
									message->m_mMessageData->m_mAISteeringData.m_fMagnitude,
									message->m_mMessageData->m_mAISteeringData.m_fPriority,
									false,
									message->m_mMessageData->m_mAISteeringData.m_iRegistrationID);
					break;
				}
				break;
			}
			//remove a force
			if(imsg == MSG_RemoveForce)
			{
				bool success = RemoveRegisteredForce(message->m_mMessageData->m_mAIEntityData.m_iSharedID,
															message->m_mMessageData->m_mAISteeringData.m_iRegistrationID);
				break;
			}
			if(imsg == MSG_ClearForces)
			{
				RemoveAllForces(message->m_mMessageData->m_mAIEntityData.m_iSharedID);
				break;
			}
			//turn pathfinding on/off
			if(imsg == MSG_SetPathingUse)
			{
				//if(message->m_mMessageData->m_mAIEntityData.m_bUsePathfinding)
				//{
				//	SetAIPathingUse(message->m_mMessageData->m_mAIEntityData.m_iSharedID);
				//	break;
				//}
				SetAIPathingUse(message->m_mMessageData->m_mAIEntityData.m_iSharedID, message->m_mMessageData->m_mAIEntityData.m_bUsePathfinding);
				break;
			}
			//set entity position
			if(imsg == MSG_SetEntity_Pos)
			{
				SetEntity(message->m_mMessageData->m_mAIEntityData.m_iSharedID,
							&Vector3(message->m_mMessageData->m_mAIEntityData.m_fPosition[0],
							message->m_mMessageData->m_mAIEntityData.m_fPosition[1],
							message->m_mMessageData->m_mAIEntityData.m_fPosition[2]),
							0);
				break;
			}
			//set entity velocity
			if(imsg == MSG_SetEntity_Vel)
			{
				SetEntity(message->m_mMessageData->m_mAIEntityData.m_iSharedID,
							0,
							&Vector3(message->m_mMessageData->m_mAIEntityData.m_fVelocity[0],
							message->m_mMessageData->m_mAIEntityData.m_fVelocity[1],
							message->m_mMessageData->m_mAIEntityData.m_fVelocity[2]));
				break;
			}
			if(imsg == MSG_SetState)
			{
				SetState(message->m_mMessageData->m_mAIEntityData.m_iSharedID,
							message->m_mMessageData->m_mAIEntityData.m_iState);
				break;
			}
			if(imsg = MSG_SetTarget)
			{
				SetTarget(message->m_mMessageData->m_mAIEntityData.m_iSharedID,
					Vector3(message->m_mMessageData->m_mAIEntityData.m_fTarget[0],
							message->m_mMessageData->m_mAIEntityData.m_fTarget[1],
							message->m_mMessageData->m_mAIEntityData.m_fTarget[2]));
				break;
			}
			//set entity potential priority
			if(imsg == MSG_SetPotentialUse)
			{
				//if(message->m_mMessageData->m_mAIEntityData.m_bUsePotential)
				//{
				//	SetAIPotentialUse(message->m_mMessageData->m_mAIEntityData.m_iSharedID);
				//	break;
				//}
				SetAIPotentialUse(message->m_mMessageData->m_mAIEntityData.m_iSharedID, message->m_mMessageData->m_mAIEntityData.m_bUsePotential);
				break;
			}
			//create a potential element
			if(imsg == MSG_CreatePotential)
			{
				int id = m_cPotential.AddElement(message->m_mMessageData->m_mAIPotentialData.m_iTeamID,
													Vector3(message->m_mMessageData->m_mAIPotentialData.m_fPosition[0],
													message->m_mMessageData->m_mAIPotentialData.m_fPosition[1],
													message->m_mMessageData->m_mAIPotentialData.m_fPosition[2]),
													false, true);
				break;
			}
			if(imsg == MSG_AddPotentialData)
			{
				m_cPotential.AddElementData(message->m_mMessageData->m_mAIPotentialData.m_iElementID,
											message->m_mMessageData->m_mAIPotentialData.m_fStrength,
											message->m_mMessageData->m_mAIPotentialData.m_fRange);
				break;
			}
			//set a potential element
			if(imsg == MSG_SetPotential)
			{
				m_cPotential.EditElement(message->m_mMessageData->m_mAIPotentialData.m_iElementID,
											message->m_mMessageData->m_mAIPotentialData.m_iTeamID,
											Vector3(message->m_mMessageData->m_mAIPotentialData.m_fPosition[0],
													message->m_mMessageData->m_mAIPotentialData.m_fPosition[1],
													message->m_mMessageData->m_mAIPotentialData.m_fPosition[2]),
													false, false);
				break;
			}
			//remove a potential element
			if(imsg == MSG_RemovePotential)
			{
				RemovePotentialElement(message->m_mMessageData->m_mAIPotentialData.m_iElementID);
				break;
			}
			//set an entity's priority value
			if(imsg == MSG_SetAIPriority)
			{
				SetAIEntityPriority(message->m_mMessageData->m_mAIPotentialPriority.m_iAgentID,
									message->m_mMessageData->m_mAIPotentialPriority.m_iMapNum,
									message->m_mMessageData->m_mAIPotentialPriority.m_fPriorityValue);
				break;
			}
			//find a path for an entity
			//m_iFloatData[0], [1], & [2] hold the position
			//m_iFloatData[3] should hold the ID of the entity
			if(imsg == MSG_FindPath)
			{
				bool success = MakePath(message->m_mMessageData->m_mAIPathingData.m_iAgentID,
										message->m_mMessageData->m_mAIPathingData.m_fDestination[0],
										message->m_mMessageData->m_mAIPathingData.m_fDestination[1],
										message->m_mMessageData->m_mAIPathingData.m_fDestination[2],
										message->m_mMessageData->m_mAIPathingData.m_fMaxDistance);
				break;
			}
			//create a nav graph
			if(imsg == MSG_CreateNavGraph)
			{
				CreateMap(message->m_mMessageData->m_mAIPathMapData.m_iMapSize,
							message->m_mMessageData->m_mAIPathMapData.m_iMaxNumOfNodes);
				break;
			}
			if(imsg == MSG_SetNavNodes)
			{
				if(message->m_mMessageData->m_mAIPathMapData.m_bNodeToggle)
				{
					m_Graph.ExposeNodes(Vector3(message->m_mMessageData->m_mAIPathMapData.m_fTogglePosition[0],
												message->m_mMessageData->m_mAIPathMapData.m_fTogglePosition[1],
												message->m_mMessageData->m_mAIPathMapData.m_fTogglePosition[2]),
										Vector3(message->m_mMessageData->m_mAIPathMapData.m_fToggleSize[0],
												message->m_mMessageData->m_mAIPathMapData.m_fToggleSize[1],
												message->m_mMessageData->m_mAIPathMapData.m_fToggleSize[2]));
					break;
				}
				m_Graph.CoverNodes(Vector3(message->m_mMessageData->m_mAIPathMapData.m_fTogglePosition[0],
											message->m_mMessageData->m_mAIPathMapData.m_fTogglePosition[1],
											message->m_mMessageData->m_mAIPathMapData.m_fTogglePosition[2]),
									Vector3(message->m_mMessageData->m_mAIPathMapData.m_fToggleSize[0],
											message->m_mMessageData->m_mAIPathMapData.m_fToggleSize[1],
											message->m_mMessageData->m_mAIPathMapData.m_fToggleSize[2]));
				break;
			}
			//pause the core
			//values '0' and '1' turns pause on or off
			//value '-1' toggles pause
			if(imsg == MSG_Pause)
			{
				if(message->m_mMessageData->m_iIntData[0] == -1)
				{
					m_bIsPaused = !m_bIsPaused;
					break;
				}
				if(message->m_mMessageData->m_iIntData[0] == 0)
				{
					m_bIsPaused = false;
					break;
				}
				m_bIsPaused = true;
				break;
			}
			if(imsg == MSG_Clear)
			{
				break;
			}
			//this is where a switch default code would be
#if AICOREDEBUG
			OutputDebugString("AI Core Error : Invalid Message Type\n");
#endif
		}while(false);

	}
	delete message;
}

HRESULT CreateAIObject(HINSTANCE hDLL, IAICore **pInterface)
{
	if(!*pInterface)
	{
		*pInterface = new AICoreDLL();
		return 1;
	}

	return 0;
}

HRESULT ReleaseAIObject(IAICore **pInterface)
{
	if(!*pInterface)
	{

		return 0;
	}

	delete *pInterface;
	*pInterface = NULL;
	return 1;
}