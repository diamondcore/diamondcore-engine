#pragma once

#include "Graph.h"

class Path
{
	Graph*					m_pGraph;		// Graph to search for path on
	Vector3*				m_pOwnerPos;
	WTW::Node*				m_pNodeOwner;
	WTW::Node*				m_pNodeEnd;
	std::vector<WTW::Node*>	m_NodeList; // Best path of nodes
	std::vector<bool>		m_bVisited;

	float	m_fMaxSpeed;
	bool	m_bIsTraveling;
	int		nodeID;					// Node closest to point
	float	currentG;				// Distance Traveled
	float	maxG;					//Max Distance allowed to travel

	void	setNode();
	void	searchPathNode();		// Assign closest Node as location
	void	Search(WTW::Node* start, WTW::Node* end);
	void	SearchWithMax(WTW::Node* start, WTW::Node* end);

public:
	Path(Graph* graph);
	~Path();
	void	SetPath(Vector3* owner, Vector3 end);
	
	WTW::Node*	GetNextNode();
	Vector3	UpdateForce(int numNodesToCheck = 1);	// Velocity in the direction of the node
	std::vector<Vector3> GetPath();
	std::vector<WTW::Node*>& GetPathList(){return m_NodeList;}
	void	GetPathNodes(std::vector<D3DXMATRIX>& transforms);	// Used for drawing the path

	bool	IsTraveling();
	void	SetMaxSpeed(float maxSpeed);
	void	SetOwner(Vector3* owner);
	void	SetDestination(Vector3 destination);
	void	SetMaxG(float g){maxG = g;}
};