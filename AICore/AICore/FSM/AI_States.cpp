#include "AI_States.h"

#include "../AICoreDependencies.h"
#include "../AICoreDLL.h"

#include <DCPhysicsLib.h>

#define PATHCHECKNUM 1

void Agent_Idle::Enter(AgentProperties* ent)
{
	m_WaitTimes[ent->GetSharedID()] = 0.0;
}

void Agent_Idle::Execute(AgentProperties* ent, float dt)
{
	m_WaitTimes[ent->GetSharedID()] += dt;
	if(m_WaitTimes[ent->GetSharedID()] >= m_fWaitTime)
	{
		if(ent->m_pStateMachine->GetPrevious())
			ent->m_pStateMachine->RevertState();
		else
			ent->m_pStateMachine->ChangeState(AIWander);
	}
}

void Agent_Idle::Exit(AgentProperties* ent)
{
}

void Agent_Wander::Enter(AgentProperties* ent)
{
	if(ent->m_pSteering && ent->m_pGraph)
	{		
		ent->m_pPath->SetPath(&MMI->GetPosition(ent->GetMovementID()), ent->m_pGraph->getRandomSpawn());
		if(ent->m_pPath->GetPath().size() > 0)
		{
			ent->m_pSteering->ClearForces(ent->GetMovementID());
			ent->m_pSteering->AddSeekForce(ent->GetMovementID(), &ent->m_pPath->GetNextNode()->getPos(), 100, 0, false, 2);
		}
	}
}

void Agent_Wander::Execute(AgentProperties* ent, float dt)
{
	if(ent->m_pSteering)
	{
		
		if(ent->m_pGraph)
		{
			if(ent->m_pPath->GetPath().size() == 0)
			{
				ent->m_pStateMachine->ChangeState(AIIdle);
				return;
			}		
			else
			{
				ent->m_pSteering->ClearForces(ent->GetMovementID());
				ent->m_pPath->SetOwner(&MMI->GetPosition(ent->GetMovementID()));
				ent->m_pPath->UpdateForce(PATHCHECKNUM);
				if(ent->m_pPath->GetPath().size() >= PATHCHECKNUM)
				{
					ent->m_pSteering->AddSeekForce(ent->GetMovementID(), &ent->m_pPath->GetNextNode()->getPos(), 100, 0, false, 2);
					ent->m_pSteering->AddArriveForce(ent->GetMovementID(), &ent->m_pPath->GetNextNode()->getPos(), 0, 4, 100, 0, false, 2);
				}
			}	
		}
	}
}

void Agent_Wander::Exit(AgentProperties* ent)
{
	//ent->m_pSteering->ClearForces(ent->GetMovementID());
	//ent->m_pSteering->AddArriveForce(ent->GetMovementID(), &MMI->GetPosition(ent->GetMovementID()), 3, 15, 100, 0, false, 2);
}

void Agent_Potential::Enter(AgentProperties* ent)
{	
}

void Agent_Potential::Execute(AgentProperties* ent, float dt)
{
	Vector3 v;
	bool b;
	b = ent->m_pPotential->GetBestTarget(&v, ent->GetSharedID(), MMI->GetPosition(ent->GetMovementID()), 1, true);
	v.Normalize();
	MMI->AddForce(ent->GetMovementID(), v * 10);
	if(b)
	{
		if(ent->m_pPotential->GetNumOfHotSpots() == 0)
		{
		}
	}
}

void Agent_Potential::Exit(AgentProperties* ent)
{
}

void Agent_Flee::Enter(AgentProperties* ent)
{
}

void Agent_Flee::Execute(AgentProperties* ent, float dt)
{
}

void Agent_Flee::Exit(AgentProperties* ent)
{
}

void Agent_Path::Enter(AgentProperties* ent)
{
	if(ent->m_pSteering && ent->m_pGraph)
	{
		ent->SetTarget(ent->m_pGraph->getRandomSpawn());
		ent->m_pPath->SetPath(&MMI->GetPosition(ent->GetMovementID()), ent->GetTarget());
		if(ent->m_pPath->GetPath().size() > 0)
			ent->m_pSteering->AddSeekForce(ent->GetMovementID(), &ent->m_pPath->GetNextNode()->getPos(), 100, 0, false, 2);
	}
}

void Agent_Path::Execute(AgentProperties* ent, float dt)
{
	if(ent->m_pSteering)
	{
		
		if(ent->m_pGraph)
		{
			if(ent->m_pPath->GetPath().size() == 0)
			{
				ent->m_pStateMachine->ChangeState(AIIdle);
				return;
			}		
			else
			{
				ent->m_pSteering->ClearForces(ent->GetMovementID());
				ent->m_pPath->SetOwner(&MMI->GetPosition(ent->GetMovementID()));
				ent->m_pPath->UpdateForce(PATHCHECKNUM);
				if(ent->m_pPath->GetPath().size() >= PATHCHECKNUM)
				{
					ent->m_pSteering->AddSeekForce(ent->GetMovementID(), &ent->m_pPath->GetNextNode()->getPos(), 100, 0, false, 2);
					ent->m_pSteering->AddArriveForce(ent->GetMovementID(), &ent->m_pPath->GetNextNode()->getPos(), 0, 4, 100, 0, false, 2);
				}
			}	
		}
	}
}

void Agent_Path::Exit(AgentProperties* ent)
{
	if(ent->m_pSteering)
	{
		ent->m_pSteering->ClearForces(ent->GetMovementID());
		ent->m_pSteering->AddArriveForce(ent->GetMovementID(), &MMI->GetPosition(ent->GetMovementID()), 3, 15, 100, 0, false, 2);
	}
}

void Agent_Global::Enter(AgentProperties* ent)
{
}

void Agent_Global::Execute(AgentProperties* ent, float dt)
{
}

void Agent_Global::Exit(AgentProperties* ent)
{
	//if(MMI->GetVelocity(ent->GetMovementID()).MagnitudeSquared() > ent->m_pPath
}