//AI FSM
#ifndef __AI_FSM_H
#define __AI_FSM_H

//pure virtual interface for individual states

template <class ent_type> class State
{
protected:

public:
	virtual void Enter(ent_type*) = 0;
	virtual void Execute(ent_type*, float dt) = 0;
	virtual void Exit(ent_type*) = 0;
};

template <class ent_type> class StateMachine
{
private:
	ent_type* m_pOwner;		//owner of the state machine
	State<ent_type>* m_pCurrentState;
	State<ent_type>* m_pPreviousState;
	State<ent_type>* m_pGlobalState;

public:
	//Constructor (force an owner)
	StateMachine(ent_type* p_owner):m_pOwner(p_owner),m_pCurrentState(0), m_pPreviousState(0), m_pGlobalState(0){}

	//setters
	void setCurrent(State<ent_type>* p_cur)
	{
		m_pCurrentState->Enter(m_pOwner);
	}
	void setPrevious(State<ent_type>* p_prev)
	{
		m_pPreviousState = p_prev;
	}
	void setGlobal(State<ent_type>* p_glob)
	{
		m_pGlobalState = p_glob;
	}

	//change state
	void ChangeState(State<ent_type>* p_new)
	{
		m_pPreviousState = m_pCurrentState;
		if(m_pPreviousState)
		{
			m_pCurrentState->Exit(m_pOwner);
		}
		m_pCurrentState = p_new;
		if(m_pCurrentState)
			m_pCurrentState->Enter(m_pOwner);
	}
	void RevertState()
	{
		ChangeState(m_pPreviousState);
	}

	//getters
	State<ent_type>* GetCurrent() const
	{
		return m_pCurrentState;
	}
	State<ent_type>* GetGlobal() const
	{
		return m_pGlobalState;
	}
	State<ent_type>* GetPrevious() const
	{
		return m_pPreviousState;
	}

	void Update(float dt) const
	{
		if(m_pGlobalState)
			m_pGlobalState->Execute(m_pOwner, dt);	
		if(m_pCurrentState)
			m_pCurrentState->Execute(m_pOwner, dt);
	}
};



#endif