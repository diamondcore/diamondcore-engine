#ifndef __AI_STATES_H
#define __AI_STATES_H

#include <map>

#include "AI_FSM.h"

class AgentProperties;

#define AIIdle Agent_Idle::Instance()
#define AIWander Agent_Wander::Instance()
#define AIFlee Agent_Flee::Instance()
#define AIPath Agent_Path::Instance()
#define AIGlobal Agent_Global::Instance()
#define AIPotential Agent_Potential::Instance()

class Agent_Idle : public State<AgentProperties>
{
private:
	Agent_Idle(){m_fWaitTime = 0.0;};

	std::map<int, float> m_WaitTimes;
	float m_fWaitTime;

public:
	static Agent_Idle* Instance(){static Agent_Idle instance; return &instance;}
	~Agent_Idle(){};

	virtual void Enter(AgentProperties*);
	virtual void Execute(AgentProperties*, float dt);
	virtual void Exit(AgentProperties*);
};

class Agent_Wander : public State<AgentProperties>
{
private:
	Agent_Wander(){}

public:
	static Agent_Wander* Instance(){static Agent_Wander instance; return &instance;}
	~Agent_Wander(){}

	virtual void Enter(AgentProperties*);
	virtual void Execute(AgentProperties*, float dt);
	virtual void Exit(AgentProperties*);
};

class Agent_Potential : public State<AgentProperties>
{
private:
	Agent_Potential(){}

public:
	static Agent_Potential* Instance(){static Agent_Potential instance; return &instance;}
	~Agent_Potential(){}

	virtual void Enter(AgentProperties*);
	virtual void Execute(AgentProperties*, float dt);
	virtual void Exit(AgentProperties*);
};

class Agent_Flee : public State<AgentProperties>
{
private:
	Agent_Flee(){};

public:
	static Agent_Flee* Instance(){static Agent_Flee instance; return &instance;}
	~Agent_Flee(){};

	virtual void Enter(AgentProperties*);
	virtual void Execute(AgentProperties*, float dt);
	virtual void Exit(AgentProperties*);
};

class Agent_Path : public State<AgentProperties>
{
private:
	Agent_Path(){};

public:
	static Agent_Path* Instance(){static Agent_Path instance; return &instance;}
	~Agent_Path(){};

	virtual void Enter(AgentProperties*);
	virtual void Execute(AgentProperties*, float dt);
	virtual void Exit(AgentProperties*);
};

class Agent_Global : public State<AgentProperties>
{
private:
	Agent_Global(){}

public:
	static Agent_Global* Instance(){static Agent_Global instance; return &instance;}
	~Agent_Global(){}

	virtual void Enter(AgentProperties*);
	virtual void Execute(AgentProperties*, float dt);
	virtual void Exit(AgentProperties*);
};

#endif