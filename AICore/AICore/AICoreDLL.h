#pragma once

#include <AIDLLInterface.h>
#include <DCMathLib.h>
#include <DCPhysicsLib.h>
#include <string>
#include <memory>

#include "PotentialManager.h"
#include "SteeringForceManager.h"
#include "AICoreDependencies.h"

#define AICOREDEBUG 1

// Enable Memory Leak Detection (in Debug mode)
#if defined(DEBUG) | defined(_DEBUG)
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif

/**********************************
Outputs the File and Line when a
memory leak occurs.
***********************************/
#if defined(DEBUG) | defined(_DEBUG)
#define DEBUG_NEW new(_NORMAL_BLOCK,__FILE__, __LINE__)
#else
#define DEBUG_NEW new
#endif
//#define new DEBUG_NEW

class AICoreDLL : public IAICore
{
private:
	PotentialManager m_cPotential;
	SteeringForceManager2 m_cSteering;
	//PathfindingManager m_cPathfinding;
	Graph m_Graph;

	std::vector<std::shared_ptr<AgentProperties>> m_vAIAgents;
	std::map<int, std::shared_ptr<AgentProperties>> m_AgentMap;

	float m_fCurrentTime,				//The current time between ticks
		m_fTickInterval;				//the duration required before each update

	bool m_bIsPaused;					//is the core paused?
	bool m_bUseSharedIDs;

	std::vector<std::pair<int, int>> m_vPotSources;
#if AICOREDEBUG
	//std::vector<std::pair<int, sDebugInfo>> m_vNodes;
	
	std::vector<std::pair<int, Vector3>> m_vAIPotTargets;
#endif

public:
	AICoreDLL();
	~AICoreDLL();

	//Creates an AI entity, returns the ID.
	//Passing in a negative number for the shared ID will create a new shared resource.
	int AddEntity(int &iSharedID,
		float fPositionX, float fPositionY, float fPositionZ,
		float fVelocityX = 0, float fVelocityY = 0, float fVelocityZ = 0);
	void SetEntity(int iEntityID, Vector3 *vPosition = 0, Vector3 *vVelocity = 0);
	bool RemoveEntity(int iEntityID);

	//FORCES

	//Adds a Seek force to the specified entity
	//Setting iRegistrationID to a value other than 0 will allow the force to be independantly manipulated later on
	void AddSeekForce(int iEntityID, float *fTarget, float fMagnitude,
			float fPriority = 0, bool bUseDynamicTarget = false, unsigned int iRegistrationID = 0);
	//Adds a Flee force to the specified entity
	//Setting iRegistrationID to a value other than 0 will allow the force to be independantly manipulated later on
	void AddFleeForce(int iEntityID, float *fTarget, float fMagnitude,
			float fPriority = 0, bool bUseDynamicTarget = false, unsigned int iRegistrationID = 0);
	//Adds an Arrive Force to the specified entity
	//Setting iRegistrationID to a value other than 0 will allow the force to be independantly manipulated later on
	void AddArriveForce(int iEntityID, float *fTarget, float fInnerRange, float fOuterRange, float fMagnitude,
			float fPriority = 0, bool bUseDynamicTarget = false, unsigned int iRegistrationID = 0);
	//removes a registered force from a specific entity
	bool RemoveRegisteredForce(int iEntityID, unsigned int iRegistrationID = 0, bool bRemoveAllWithID = false);
	//removes all forces from a specific entity
	void RemoveAllForces(int iEntityID);
	void SetTarget(int iEntityID, Vector3 vTarget, bool bForceCheck = true);

	virtual void SetState(int iEntityID, int iState);

	//Potential System

	//adds a new element to the potential system
	
	//edits basic properties of an element in the potential system
	void SetPotentialElement(int iElementID, float fPosX, float fPosY, float fPosZ, unsigned int dTeamName = 0, bool bCheckExists = false);
	void RemovePotentialElement(int iElementID);
	//adds new map data to  an element in the potential system
	UINT AddPotentialElementData(int iElementID,
		float fStrength, float fRange = 0, std::string sFalloff = "");
	//edits/adds map data to the potential system
	void SetPotentialElementData(int iElementID, UINT iSetNum,
		float fStrength, float fRange = 0, std::string sFalloff ="");
	//adds a new falloff texture for the potential system to use
	void AddPotentialFalloffMap(std::string sDirectory);
	//sets a priority value for an entity in relation to a map element 
	void SetAIEntityPriority(int iEntityID, UINT iDataSet, float fValue, unsigned int dTeamName = 0);

	

	void SetAIPotentialUse(int iEntityID, bool bUsePotentialSystem = true);

	//PATHFINDING

	//creates a nav graph
	void CreateMap(int iSize, int iMaxNumNodes);
	//makes a path for an entity to follow
	bool MakePath(int iEntityID, float fDestX, float fDestY, float fDestZ, float fMaxDist = -1);
	//covers nodes in the map
	void CoverNodes(float fSizeX, float fSizeY, float fSizeZ,
		float fPosX, float fPosY, float fPosZ);
	void ExposeNodes(float fSizeX, float fSizeY, float fSizeZ,
		float fPosX, float fPosY, float fPosZ);

	void SetAIPathingUse(int iEntityID, bool bUsePathingSystem = true);
	void ToggleSharedIDs(){SetSharedIDs(!m_bUseSharedIDs);}
	void SetSharedIDs(bool set);

	/*=====Non-Virtual methods - The only methods accessible from outside this class=====*/

	int AddPotentialElement(float fPositionX, float fPositionY, float fPositionZ,
		float fVelocityX = 0, float fVelocityY = 0, float fVelocityZ = 0, unsigned int dTeamName = 0);

	//updates all pathfinding
	virtual void Pathing();
	//Updates all steering forces
	virtual void Steering();
	virtual void Potential();

	//updates the core
	virtual void Update(float dt);
	//initializes the core
	virtual void Initialize();
	//shuts down the core
	virtual void Shutdown();
	//pauses the core
	virtual void Pause();
	//processes all core messages
	virtual void ProcessMessages();

	int GetCorePriority() { return 2; }
};

extern "C"
{
	HRESULT CreateAIObject(HINSTANCE hDLL, IAICore **pInterface);
	HRESULT ReleaseAIObject(IAICore **pInterface);
}

typedef HRESULT (*CREATEAIOBJECT)(HINSTANCE hDLL, IAICore **pInterface);
typedef HRESULT (*RELEASEAIOBJECT)(IAICore **pInterface);