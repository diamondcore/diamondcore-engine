#include "SteeringForceManager.h"

#include <algorithm>


//void SteeringForceManager::AddSeekForce(int iEntityID, Vector3 *target, float fForceMagnitude,
//										float fPriority, bool bTargetIsDynamic)
//{
//	std::shared_ptr<ForceRegistryData> data;
//
//	if(bTargetIsDynamic)
//	{
//		//create force with dynamic target
//		data = std::make_shared<ForceRegistryData>(std::make_shared<SteeringForceSeek>(target, fForceMagnitude), fPriority, false, 0);
//	}
//	else
//	{
//		//create force with static target
//		data = std::make_shared<ForceRegistryData>(std::make_shared<SteeringForceSeek>(*target, fForceMagnitude), fPriority, false, 0);
//	}
//	//adding force to the entity's force list
//	m_vcEntityForces[iEntityID].push_back(data);
//	//sorting the force list
//	std::sort(m_vcEntityForces[iEntityID].begin(), m_vcEntityForces[iEntityID].end());
//}
//
////void SteeringForceManager::AddSeekForce(int iEntityID, Vector3 *target, float fForceMagnitude,
////										float fPriority, bool bTargetIsDynamic, int iRegisterNum)
////{
////	std::shared_ptr<ForceRegistryData> data;
////
////	if(bTargetIsDynamic)
////	{
////		//create force with dynamic target
////		data = std::make_shared<ForceRegistryData>(std::make_shared<SteeringForceSeek>(target, fForceMagnitude), fPriority, true, iRegisterNum);
////	}
////	else
////	{
////		//create force with static target
////		data = std::make_shared<ForceRegistryData>(std::make_shared<SteeringForceSeek>(*target, fForceMagnitude), fPriority, true, iRegisterNum);
////	}
////
////	//adding force to the entity's force list
////	m_vcEntityForces[iEntityID].push_back(data);
////	//sorting the force list
////	std::sort(m_vcEntityForces[iEntityID].begin(), m_vcEntityForces[iEntityID].end());
////
////	//storing force into the registry
////	m_pRegisteredForces[iRegisterNum] = data;
////}
//
//void SteeringForceManager::AddFleeForce(int iEntityID, Vector3 *target, float fForceMagnitude,
//										float fPriority, bool bTargetIsDynamic)
//{
//	std::shared_ptr<ForceRegistryData> data;
//
//	if(bTargetIsDynamic)
//	{
//		//create force with dynamic target
//		data = std::make_shared<ForceRegistryData>(std::make_shared<SteeringForceFlee>(target, fForceMagnitude), fPriority, false, 0);
//	}
//	else
//	{
//		//create force with static target
//		data = std::make_shared<ForceRegistryData>(std::make_shared<SteeringForceFlee>(*target, fForceMagnitude), fPriority, false, 0);
//	}
//
//	//adding force to the entity's force list
//	m_vcEntityForces[iEntityID].push_back(data);
//	//sorting the force list
//	std::sort(m_vcEntityForces[iEntityID].begin(), m_vcEntityForces[iEntityID].end());
//}
//
////void SteeringForceManager::AddFleeForce(int iEntityID, Vector3 *target, float fForceMagnitude,
////										float fPriority, bool bTargetIsDynamic, int iRegisterNum)
////{
////	std::shared_ptr<ForceRegistryData> data;
////
////	if(bTargetIsDynamic)
////	{
////		//create force with dynamic target
////		data = std::make_shared<ForceRegistryData>(std::make_shared<SteeringForceFlee>(target, fForceMagnitude), fPriority, true, iRegisterNum);
////	}
////	else
////	{
////		//create force with static target
////		data = std::make_shared<ForceRegistryData>(std::make_shared<SteeringForceFlee>(*target, fForceMagnitude), fPriority, true, iRegisterNum);
////	}
////
////	//adding force to the entity's force list
////	m_vcEntityForces[iEntityID].push_back(data);
////	//sorting the force list
////	std::sort(m_vcEntityForces[iEntityID].begin(), m_vcEntityForces[iEntityID].end());
////
////	//storing force into the registry
////	m_pRegisteredForces[iRegisterNum] = data;
////}
//
//void SteeringForceManager::AddArriveForce(int iEntityID, Vector3 *target, float fInnerRange, float fOuterRange, float fForceMagnitude,
//										  float fPriority, bool bTargetIsDynamic)
//{
//	std::shared_ptr<ForceRegistryData> data;
//
//	if(bTargetIsDynamic)
//	{
//		//create force with dynamic target
//		data = std::make_shared<ForceRegistryData>(std::make_shared<SteeringForceArrive>(target, fInnerRange, fOuterRange, fForceMagnitude), fPriority, false, 0);
//	}
//	else
//	{
//		//create force with static target
//		data = std::make_shared<ForceRegistryData>(std::make_shared<SteeringForceArrive>(*target, fInnerRange, fOuterRange, fForceMagnitude), fPriority, false, 0);
//	}
//
//	//adding force to the entity's force list
//	m_vcEntityForces[iEntityID].push_back(data);
//	//sorting the force list
//	std::sort(m_vcEntityForces[iEntityID].begin(), m_vcEntityForces[iEntityID].end());
//}
//
////void SteeringForceManager::AddArriveForce(int iEntityID, Vector3 *target, float fInnerRange, float fOuterRange, float fForceMagnitude,
////										  float fPriority, bool bTargetIsDynamic, int iRegisterNum)
////{
////	std::shared_ptr<ForceRegistryData> data;
////
////	if(bTargetIsDynamic)
////	{
////		//create force with dynamic target
////		data = std::make_shared<ForceRegistryData>(std::make_shared<SteeringForceArrive>(target, fInnerRange, fOuterRange, fForceMagnitude), fPriority, true, iRegisterNum);
////	}
////	else
////	{
////		//create force with static target
////		data = std::make_shared<ForceRegistryData>(std::make_shared<SteeringForceArrive>(*target, fInnerRange, fOuterRange, fForceMagnitude), fPriority, true, iRegisterNum);
////	}
////
////	//adding force to the entity's force list
////	m_vcEntityForces[iEntityID].push_back(data);
////	//sorting the force list
////	std::sort(m_vcEntityForces[iEntityID].begin(), m_vcEntityForces[iEntityID].end());
////
////	//storing force into the registry
////	m_pRegisteredForces[iRegisterNum] = data;
////}
//
////bool SteeringForceManager::RemoveForce(int iEntityID, int iRegistryID)
////{
////	//UINT forceVecSize = m_vcEntityForces[iEntityID].size();
////
////	//if(iSteeringForceNum < 0 || iSteeringForceNum >= forceVecSize)
////	//	return false;
////
////	////std::vector<std::shared_ptr<SteeringForceBase>> steeringForces;
////
////	////steeringForces = m_vcEntityForces[iEntityID];
////
////	//forceVecSize--;
////
////	//for(UINT i = iSteeringForceNum; i < forceVecSize; i++)
////	//{
////	//	m_vcEntityForces[iEntityID][i] = m_vcEntityForces[iEntityID][i+1];
////	//}
////
////	//m_vcEntityForces[iEntityID].pop_back();
////	return true;
////}
//
//void SteeringForceManager::RemoveAllForcesOfType(int iEntityID, int iSteeringForceType, UINT iStartRange, UINT iEndRange)
//{
//	UINT forceVecSize = m_vcEntityForces[iEntityID].size();
//
//	if(iStartRange > forceVecSize)
//		return;
//
//	if(iEndRange == 0)
//		iEndRange = forceVecSize;
//	
//	iEndRange = min(iEndRange, forceVecSize - 1);
//
//	UINT i, j;
//
//	/*
//	Alternate method - not yet tested
//
//	std::vector<std::shared_ptr<ForceRegistryData>>::iterator it;
//
//	it = m_vcEntityForces[iEntityID].begin;
//	for(j = 0; j < iStartRange; j++)
//	{
//		it++;
//	}
//
//	for(i = iStartRange; i < iEndRange; i++)
//	{
//		if(it->get()->force->GetSteeringForceType() == iSteeringForceType)
//		{
//			if(it->get()->isRegistered)
//			{
//				m_pRegisteredForces.erase(m_vcEntityForces[iEntityID].at(i)->registryID);
//			}
//
//			forceVecSize--;
//
//			m_vcEntityForces[iEntityID].erase(it);
//
//			i--;
//			it--;
//
//			iEndRange = min(iEndRange, forceVecSize);
//		}
//		it++;
//	}
//	*/
//
//	for(i = iStartRange; i < iEndRange; i++)
//	{
//		if(m_vcEntityForces[iEntityID].at(i)->force->GetSteeringForceType() == iSteeringForceType)
//		{
//			//if(m_vcEntityForces[iEntityID].at(i)->isRegistered)
//			//{
//			//	m_pRegisteredForces.erase(m_vcEntityForces[iEntityID].at(i)->registryID);
//			//}
//
//			forceVecSize--;
//
//			for(j = i; j < forceVecSize; j++)
//			{
//				m_vcEntityForces[iEntityID][j] = m_vcEntityForces[iEntityID][j+1];
//			}
//			m_vcEntityForces[iEntityID].pop_back();
//			i--;
//			iEndRange--;
//		}
//	}
//}
//
//void SteeringForceManager::ClearForces(int iEntityID)
//{
//	//remove all forces for the given ID
//	m_vcEntityForces[iEntityID].clear();
//}
//
//Vector3 SteeringForceManager::UpdateForces(int iEntityID, Vector3 *vEntityPosition, Vector3 *vEntityVelocity,
//										   bool bUseConstantVelocity, UINT iStartRange, UINT iEndRange)
//{
//	Vector3 total(0, 0, 0);
//	UINT forceVecSize = m_vcEntityForces[iEntityID].size();
//
//	if(iEndRange == 0 || iEndRange > forceVecSize)
//		iEndRange = forceVecSize;
//
//	if(iStartRange >= forceVecSize)
//		return total;
//	iEndRange = min(iEndRange, forceVecSize);
//	
//	Vector3 result;
//
//	if(bUseConstantVelocity)
//	{
//		for(UINT i = iStartRange; i < iEndRange; i++)
//		{
//			m_vcEntityForces[iEntityID].at(i)->force->UpdateForce(&result, vEntityPosition, vEntityVelocity);
//			D3DXVec3Add(&total, &total, &result);
//		}
//		return total;
//	}
//	Vector3 currentVelocity(vEntityVelocity->x, vEntityVelocity->y, vEntityVelocity->z);
//	for(UINT i = iStartRange; i < iEndRange; i++)
//	{
//		m_vcEntityForces[iEntityID].at(i)->force->UpdateForce(&result, vEntityPosition, &currentVelocity);
//		D3DXVec3Add(&total, &total, &result);
//		D3DXVec3Add(&currentVelocity, &currentVelocity, &result);
//	}
//
//	return total;
//}
//
////Vector3 SteeringForceManager::UpdateForces(int iEntityID, Vector3 *vEntityPosition, Vector3 *vEntityVelocity, 
////										   bool bUseSnapshot, float fMaxPriority, float fMinPriority)
////{
////	Vector3 total(0, 0, 0);
////	Vector3 result;
////	UINT i = 0;
////	UINT size = m_vcEntityForces[iEntityID].size();
////
////	while(m_vcEntityForces[iEntityID].at(i)->priority > fMaxPriority)
////	{
////		i++;
////		if(i >= size)
////			return total;
////	};
////	if(bUseSnapshot)
////	{
////		while(m_vcEntityForces[iEntityID].at(i)->priority >= fMinPriority)
////		{
////			m_vcEntityForces[iEntityID].at(i)->force->UpdateForce(&result, vEntityPosition, vEntityVelocity);
////			D3DXVec3Add(&total, &total, &result);
////			i++;
////			if(i >= size)
////				return total;	
////		}
////		return total;
////	}
////	Vector3 currentVelocity(vEntityVelocity->x, vEntityVelocity->y, vEntityVelocity->z);
////	while(m_vcEntityForces[iEntityID].at(i)->priority >= fMinPriority)
////	{
////		m_vcEntityForces[iEntityID].at(i)->force->UpdateForce(&result, vEntityPosition, &currentVelocity);
////		D3DXVec3Add(&total, &total, &result);
////		i++;
////		if(i >= size)
////			return total;
////		D3DXVec3Add(&currentVelocity, &currentVelocity, &result);	
////	}
////	return total;
////}
//
//void SteeringForceManager::clean()
//{
//	m_vcEntityForces.clear();
//}

void SteeringForceManager2::AddSeekForce(int iEntityID,			//ID of the owner
					Vector3 *target,		//target that the force will use
					float fForceMagnitude,		//strength of the force
					float fPriority,			//where in the force vector the new force will go
					bool bTargetIsDynamic,		//reference the target or use a copy of its current position?
					unsigned int iRegisterID)
{
	std::shared_ptr<SteeringForceData> data = std::make_shared<SteeringForceData>();
	data->m_fPriority = fPriority;
	data->m_iForceType = SteeringForce_Seek;
	data->m_vTarget = *target;
	data->m_fForceMagnitude = fForceMagnitude;
	if(bTargetIsDynamic)
	{
		data->m_pTarget = target;
	}
	else
	{
		data->m_pTarget = &data->m_vTarget;
	}
	if(iRegisterID != 0)
	{
		data->m_iRegistryName = iRegisterID;
	}

	m_mSteeringForces[iEntityID].push_back(data);
	std::sort(m_mSteeringForces[iEntityID].begin(), m_mSteeringForces[iEntityID].end());
}

void SteeringForceManager2::AddFleeForce(int iEntityID,			//ID of the owner
					Vector3 *target,		//target that the force will use
					float fForceMagnitude,		//strength of the force
					float fPriority,			//where in the force vector the new force will go
					bool bTargetIsDynamic,		//reference the target or use a copy of its current position?
					unsigned int iRegisterID)
{
	std::shared_ptr<SteeringForceData> data = std::make_shared<SteeringForceData>();
	data->m_fPriority = fPriority;
	data->m_iForceType = SteeringForce_Flee;
	data->m_vTarget = *target;
	data->m_fForceMagnitude = fForceMagnitude;
	if(bTargetIsDynamic)
	{
		data->m_pTarget = target;
	}
	else
	{
		data->m_pTarget = &data->m_vTarget;
	}
	if(iRegisterID != 0)
	{
		data->m_iRegistryName = iRegisterID;
	}
	m_mSteeringForces[iEntityID].push_back(data);
	std::sort(m_mSteeringForces[iEntityID].begin(), m_mSteeringForces[iEntityID].end());
}

void SteeringForceManager2::AddArriveForce(int iEntityID,			//ID of the owner
					Vector3 *target,		//target that the force will use
					float fInnerRange,			//distance from the target that the entity should stop by
					float fOuterRange,			//distance from the target that affects distance/deceleration factor
					float fForceMagnitude,		//strength of the force
					float fPriority,			//where in the force vector the new force will go
					bool bTargetIsDynamic,		//reference the target or use a copy of its current position?
					unsigned int iRegisterID)
{
	std::shared_ptr<SteeringForceData> data = std::make_shared<SteeringForceData>();
	data->m_fPriority = fPriority;
	data->m_iForceType = SteeringForce_Arrive;
	data->m_fInnerRange = fInnerRange;
	data->m_fOuterRange = fOuterRange;
	data->m_vTarget = *target;
	data->m_fForceMagnitude = fForceMagnitude;
	if(bTargetIsDynamic)
	{
		data->m_pTarget = target;
	}
	else
	{
		data->m_pTarget = &data->m_vTarget;
	}
	if(iRegisterID != 0)
	{
		data->m_iRegistryName = iRegisterID;
	}
	m_mSteeringForces[iEntityID].push_back(data);
	std::sort(m_mSteeringForces[iEntityID].begin(), m_mSteeringForces[iEntityID].end());
}

bool SteeringForceManager2::SetRegisteredForceTarget(int iEntityID, unsigned int iRegisteredID, Vector3 *vTarget, bool bIsDynamic)
{
	std::shared_ptr<SteeringForceData> data;
	for(it = m_mSteeringForces[iEntityID].begin(); it != m_mSteeringForces[iEntityID].end(); it++)
	{
		data = *it;
		if(data->m_iRegistryName == iRegisteredID)
		{
			data->m_vTarget = *vTarget;
			if(bIsDynamic)
			{
				data->m_pTarget = &data->m_vTarget;			
			}
			return true;
		}
	}
	return false;
}

bool SteeringForceManager2::SetRegisteredForceStrength(int iEntityID, unsigned int iRegisteredID, float fMagnitude)
{
	std::shared_ptr<SteeringForceData> data;
	for(it = m_mSteeringForces[iEntityID].begin(); it != m_mSteeringForces[iEntityID].end(); it++)
	{
		data = *it;
		if(data->m_iRegistryName == iRegisteredID)
		{
			data->m_fForceMagnitude = fMagnitude;
			return true;
		}
	}
	return false;
}

bool SteeringForceManager2::RemoveRegisteredForce(int iEntityID, unsigned int iRegisteredID, bool bRemoveAll)
{
	UINT size = m_mSteeringForces[iEntityID].size();
	UINT count = 0;
	if(bRemoveAll)
	{
		bool found = false;
		for(it = m_mSteeringForces[iEntityID].begin(); count < size; count++)
		{
			if(it->get()->m_iRegistryName == iRegisteredID)
			{
				m_mSteeringForces[iEntityID].erase(it);
				found = true;
				size = m_mSteeringForces[iEntityID].size();
			}
			if(size <= count)
				break;
			it++;
		}
		return found;
	}
	for(it = m_mSteeringForces[iEntityID].begin(); count < size; count++)
	{
		if(it->get()->m_iRegistryName == iRegisteredID)
		{
			m_mSteeringForces[iEntityID].erase(it);
			return true;
		}
		if(size <= count)
				break;
		it++;
	}
	return false;
}

void SteeringForceManager2::RemoveAllForcesOfType(int iEntityID, int iSteeringForceType)
{
	for(it = m_mSteeringForces[iEntityID].begin(); it != m_mSteeringForces[iEntityID].end(); it++)
	{
		if(it->get()->m_iForceType == iSteeringForceType)
		{
			m_mSteeringForces[iEntityID].erase(it);
		}
	}
}

void SteeringForceManager2::DoSeek(Vector3 *vEntityPosition, Vector3 *vTarget, Vector3 *vEntityVelocity)
{
	//getting relative position
	m_vResult = *vTarget - *vEntityPosition;

	//normalizing relative position
	D3DXVec3Normalize(&m_vResult, &m_vResult);
}

void SteeringForceManager2::DoFlee(Vector3 *vEntityPosition, Vector3 *vTarget, Vector3 *vEntityVelocity)
{
	//getting relative position
	m_vResult = *vEntityPosition - *vTarget;

	//normalizing relative position
	D3DXVec3Normalize(&m_vResult, &m_vResult);
}

void SteeringForceManager2::DoArrive(Vector3 *vEntityPosition, Vector3 *vTarget, Vector3 *vEntityVelocity, float fInnerRange, float fOuterRange)
{
	//Vector3 directionNormal;

	//getting relative position
	m_vResult = *vTarget = *vEntityPosition;

	//getting the current distance
	float distance = m_vResult.Magnitude();

	//full stopping force if the object is inside the inner range
	if(distance <= fInnerRange)
	{
		m_vResult = *vEntityVelocity * -1;
		return;
	}

	////normalizing relative direction
	//D3DXVec3Normalize(&directionNormal, &m_vResult);
	////scaling relative direction by inner range
	//D3DXVec3Scale(&directionNormal, &directionNormal, fInnerRange);

	////shortening relative position by minimum threshhold
	//m_vResult -= directionNormal;

	//calculating the distance
	distance -= fInnerRange;

	//getting the deceleration force by getting inverse of entity velocity
	m_vResult = *vEntityVelocity * -1;

	//ajusting deceleration force
	if(distance != 0.0f)
		m_vResult *= (fOuterRange / distance);
}

UINT SteeringForceManager2::GetTotalNumOfForces()
{
	std::map<int, std::vector<std::shared_ptr<SteeringForceData>>>::iterator i;
	UINT total = 0;

	for(i = m_mSteeringForces.begin(); i != m_mSteeringForces.end(); i++)
	{
		total += i->second.size();
	}
	return total;
}

Vector3 SteeringForceManager2::UpdateForces(int iEntityID, Vector3 *vEntityPosition, Vector3 *vEntityVelocity, 
						bool bUseConstantVelocity)
{
	Vector3 total(0, 0, 0);
	std::shared_ptr<SteeringForceData> data;
	UINT count = 0;
	if(bUseConstantVelocity)
	{
		for(it = m_mSteeringForces[iEntityID].begin(); it != m_mSteeringForces[iEntityID].end(); it++)
		{
			data = *it;
			switch(data->m_iForceType)
			{
			case SteeringForce_Seek:
				DoSeek(vEntityPosition, data->m_pTarget, vEntityVelocity);
				D3DXVec3Scale(&m_vResult, &m_vResult, data->m_fForceMagnitude);
				break;
			case SteeringForce_Flee:
				DoFlee(vEntityPosition, data->m_pTarget, vEntityVelocity);
				D3DXVec3Scale(&m_vResult, &m_vResult, data->m_fForceMagnitude);
				break;
			case SteeringForce_Arrive:
				DoArrive(vEntityPosition, data->m_pTarget, vEntityVelocity, data->m_fInnerRange, data->m_fOuterRange);
				D3DXVec3Scale(&m_vResult, &m_vResult, data->m_fForceMagnitude);
				break;
			}
			D3DXVec3Add(&total, &total, &m_vResult);
			count++;
		}
		return total;
	}
	Vector3 AdjustedVelocity = *vEntityVelocity;
	for(it = m_mSteeringForces[iEntityID].begin(); it != m_mSteeringForces[iEntityID].end(); it++)
	{
		data = *it;
		switch(data->m_iForceType)
		{
		case SteeringForce_Seek:
			DoSeek(vEntityPosition, data->m_pTarget, vEntityVelocity);
			D3DXVec3Scale(&m_vResult, &m_vResult, data->m_fForceMagnitude);
			break;
		case SteeringForce_Flee:
			DoFlee(vEntityPosition, data->m_pTarget, vEntityVelocity);
			D3DXVec3Scale(&m_vResult, &m_vResult, data->m_fForceMagnitude);
			break;
		case SteeringForce_Arrive:
			DoArrive(vEntityPosition, data->m_pTarget, vEntityVelocity, data->m_fInnerRange, data->m_fOuterRange);
			D3DXVec3Scale(&m_vResult, &m_vResult, data->m_fForceMagnitude);
			break;
		}
		D3DXVec3Add(&total, &total, &m_vResult);
		D3DXVec3Add(&AdjustedVelocity, &AdjustedVelocity, &m_vResult);
		count++;
	}
	return total;
}