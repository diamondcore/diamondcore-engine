#pragma once
#include <vector>
#include <map>
#include <DCMathLib.h>
#include <list>


/*
	Movement Manager - handles the movement of entities that need to do so. Gives needed entities
	direct access to update positions, add forces, rotation, etc.

	Singleton to allow large scale protected access

*/


//forward declarations
class RigidBody;
class ForceRegistry;


#define MMI MovementManager::Instance()

class MovementManager
{
private:
	MovementManager();

	// map of all rigid bodies, used for random access
	std::map<int, RigidBody*> m_mRigidBodies;

	//list of all moving rigid bodies - used for iterating through 
	std::vector<RigidBody *> m_vRigidBodies;
	
	// force registry
	ForceRegistry *m_pForces;
	
	// rigid body id system
	int m_iBodyID;
	

public:

	static MovementManager* Instance();

	~MovementManager();
	
	// single call init physics call
	void InitPhysics();
	// single call physics run
	void RunPhysics(float dt);
	// single call physics shutdown/cleanup
	void ShutdownPhysics();
	
	
	
	//calls update method for all moving entities
	void Update(float dt);



	
	
	///////////////////
	// REGISTRATION
	///////////////////
	//registers new rigid body
	void AddBody(RigidBody* pNewBody);
	// creates and registers bodies
	// creates a formless rigid body for use
	int CreateBody(Vector3 vPosition, void* associatedObject = NULL);
	// creates a ball rigid body with a sphere graphics object and circle bv
	int CreateBall(Vector3 vPosition, float fRadius);
	// creates a box rigid body with a box graphics object and AABB bv
	int CreateBoxA(Vector3 vPosition, Vector3 vHalfExtents);
	// creates a box rigid body with a box graphics object and OBB bv
	int CreateBoxO(Vector3 vPosition, Vector3 vHalfExtents);
	// creates a obb w/ rigid body, but no graphical representation
	int CreateBoxNoGFX(Vector3 vPosition, Vector3 vHalfExtents);
	// creates a half-space with a normal and an offset
	int CreateHalfSpace(Vector3 vNormal, float fOffset, bool display);
	// Creates a box, but with a sphere equal to the smallest half-extent
	int CreateBoxS(Vector3 vPosition, Vector3 vHalfExtents);

	///////////////////
	// DELETION
	///////////////////
	void DeleteBody(int iBodyID);
	void DeleteAllBodies();
	
	//////////////////
	// ACCESS
	//////////////////
	// returns rigid body ptr for access
	RigidBody* GetBody(int iBodyID);

	
	
	////////////////
	// ACCESSORS
	////////////////
	// finds all neighbors within a radius of a point
	std::vector<RigidBody*> GetNeighbors(Vector3 vCenter, float radius);
	// gets object's position
	Vector3 GetPosition(int iBodyID);

	// Gets object's mass
	float GetMass(int iBodyID) ;	
	// Returns object's inverse mass (if 0, means object is immobile)
	float GetInverseMass(int iBodyID) ;
	// Returns a bool telling if an object can move or not
	bool HasFiniteMass(int iBodyID) ;
	
	// Returns the value of the object's inertial tensor in body space
	Matrix3 GetInertiaTensor(int iBodyID) ;
	
	// Returns the value of the object's inertial tensor in world space 
    Matrix3 GetInertiaTensorWorld(int iBodyID) ;
	
	// Returns value of object's inverse inertial tensor in body space
	Matrix3 GetInverseInertiaTensor(int iBodyID) ;
	
	// Returns value of object's inverse inertial tensor
	Matrix3 GetInverseInertiaTensorWorld(int iBodyID) ;
	// Returns Linear Damping Value
	float GetLinearDamping(int iBodyID) ;
	// REturns Angular Damping Value
	float GetAngularDamping(int iBodyID) ;
	
	// Returns the values of the object's orientation
    Quaternion GetOrientation(int iBodyID ) ;
	
	// Returns value of transform matrix
    Matrix4 GetTransform(int iBodyID) ;
	
    // Returns value of object's velocity
	Vector3 GetVelocity(int iBodyID) ;
	
	// returns rotation value item
    Vector3 GetRotation(int iBodyID) ;
	
	// returns last fram accel
    Vector3 GetLastFrameAcceleration(int iBodyID) ;
	
	// returns acceleration value
    Vector3 GetAcceleration(int iBodyID) ;

	std::list<void*> GetCollidingObjects(int bodyID);
	
	

	////////////////////
	//  MUTATORS
	///////////////////
	// sets position of body
	void SetPosition(int iBodyID, Vector3 vPosition);
	//adds force to COM to the specified body id
	void AddForce(int iBodyID, const Vector3 &vForce);
	// sets orientation   
	void SetOrientation(int iBodyID, const Quaternion &orientation);
    // sets orientation by passing individual values
	void SetOrientation(int iBodyID, const float w, const float x,const float y, const float z);
    // sets velocity 
	void SetVelocity(int iBodyID, const Vector3 &velocity);
    // sets velocity by passing individual values
	void SetVelocity(int iBodyID, const float x, const float y, const float z);
    // affects a change in velocity using the current velocity in the calculation
    void AddVelocity(int iBodyID, const Vector3 &deltaVelocity);
    // sets the rotation value   
	void SetRotation(int iBodyID, const Vector3 &rotation);
    // sets rotation values individually
	void SetRotation(int iBodyID, const float x, const float y, const float z);
	// add force to point on object in world space
    void AddForceAtPoint(int iBodyID, const Vector3 &force, const Vector3 &point);
	// manuall rotates by the given quaternion
	void RotateBody(int iBodyID, Quaternion &qRotation);
	// add force to point on object (in body space)
    void AddForceAtBodyPoint(int iBodyID, const Vector3 &force, const Vector3 &point);
	// adds torque (angular accel) to object
    void AddTorque(int iBodyID, const Vector3 &torque);  
	// adds rotation (angular vel) to object
	void AddRotation(int iBodyID, const Vector3 &deltaRotation);
	// scalar add rotation for a particular axis
	void AddRotationX(int iBodyID, const float fRotationAmt);
	// scalar add rotation for a particular axis
	void AddRotationY(int iBodyID, const float fRotationAmt);
	// scalar add rotation for a particular axis
	void AddRotationZ(int iBodyID, const float fRotationAmt);
	// sets acceleration for object
    void SetAcceleration(int iBodyID, const Vector3 &acceleration);
    // sets acceleration by individual axis
	void SetAcceleration(int iBodyID, const float x, const float y, const float z);
	// sets mass of object
	void SetMass(int iBodyID, const float fMass);
	// sets mass of object
	void SetInverseMass(int iBodyID, const float invMass);
	// sets the transform of the rigidbody
	void SetTransform(int iBodyID, Matrix4 mTransform);
	// sets the bounding offset for the rigid body, so bv updates correctly
	void SetBoundingOffset(int iBodyID, float x , float y, float z);

	void SetObjectRef(int bodyID, void* objRef);
	

	//////////////
	// FORCES
	/////////////
	void AddSpringForce(int iObjectID1, Vector3 vLocalAttachPt, 
		int iObjectID2, Vector3 vLocalAttachPt2, 
		float fSpringConstant, float fRestlength);

	void AddGravityForce(int iObjectID, Vector3 vGravVector);
	void AddGravityForce(int iObjectID, float fGravityY);



	////////////////
	// COLLISIONS
	///////////////
	bool AABBTest(float minX1, float minY1, float minZ1, float maxX1, float maxY1, float maxZ1,
		float minX2, float minY2, float minZ2, float maxX2, float maxY2, float maxZ2);

	void AddOBBToBody(int id, Vector3 vHalfExtents);


};

