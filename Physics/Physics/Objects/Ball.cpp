#include "Ball.h"
//#include "../Collisions/Bounding Volumes/Circle.h"
//#include "../Collisions/Detection/AdvCollisions.h"
//#include "../../DiamondCore/IGraphicsHandle.h"


Ball::Ball()
	:RigidBody(-1, Vector3(0.0f,0.0f,0.0f)),m_fRadius(1.0f)
{
	//give it a graphical object
	//m_iGraphicsID = IGH->CreateSphere();

	//IGH->SetSphereScale(m_iGraphicsID, m_fRadius);
	
	m_qOrientation = Quaternion();

}

Ball::Ball(int iBodyID, Vector3 vPosition, float fRadius, int iBVid)
	:RigidBody(iBodyID, vPosition, iBVid), m_fRadius(fRadius)
{
	//m_iGraphicsID = IGH->CreateSphere();
	
	//IGH->SetSphereScale(m_iGraphicsID, m_fRadius);

	m_qOrientation = Quaternion();

	// inertial tensor for solid cube
	
	Matrix3 InertiaTensor;

	m_fInverseMass = 1.0f / 20.0f;

	// I = 2/5 MR^2
	float x = 0.4f * (1.0f/m_fInverseMass) * (fRadius * fRadius);
	float y = 0.4f * (1.0f/m_fInverseMass) * (fRadius * fRadius);
	float z = 0.4f * (1.0f/m_fInverseMass) * (fRadius * fRadius);

	InertiaTensor.data[0] = x;
	InertiaTensor.data[4] = y;
	InertiaTensor.data[8] = z;

	SetInertiaTensor(InertiaTensor);

	


	
}

Ball::~Ball()
{
	//IGH->RemoveSphere(m_iGraphicsID);
}

void Ball::VUpdate(float dt)
{
	//update physics
	RigidBody::VUpdate(dt);

	// update bv
	//CMI->GetBV(m_iBoundingVolumeID)->VUpdate(dt);
	//ACMI->GetBV(m_iBoundingVolumeID)->VUpdate(dt);
	//	
	////update graphics position
	//IGH->SetSpherePosition(m_iGraphicsID, m_vPosition.x, m_vPosition.y, m_vPosition.z );
	//// update graphics orientation
	//IGH->SetSphereRotation(m_iGraphicsID, m_qOrientation.x, m_qOrientation.y, m_qOrientation.z, m_qOrientation.w);
}