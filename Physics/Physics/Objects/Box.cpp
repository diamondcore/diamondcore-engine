#include "Box.h"
//#include "../Collisions/Detection/AdvCollisions.h"
//#include "../../DiamondCore/IGraphicsHandle.h"
//#include "../Collisions/Bounding Volumes/OBB.h"



Box::Box()
	:RigidBody(-1, Vector3(0,0,0))
{
	//m_iGraphicsID = IGH->CreateSquare();
}

Box::Box(int id, Vector3 vPosition, Vector3 vHalfExtents,int iBVid)
	:RigidBody(id, vPosition, iBVid), m_vHalfExtents(vHalfExtents)
{
	//m_iGraphicsID = IGH->CreateSquare();

	//IGH->SetSquareScale(m_iGraphicsID, vHalfExtents.x*2, vHalfExtents.y*2, vHalfExtents.z*2);

	m_qOrientation = Quaternion();

	m_fInverseMass = 1.0f / 10.0f;

	// inertial tensor for solid cube
	
	Matrix3 InertiaTensor;

	InertiaTensor.SetBlockInertiaTensor(vHalfExtents, GetMass());

	/*float x = 0.08333f * (1.0f/m_fInverseMass) * (powf(vHalfExtents.y,2) + powf(m_vHalfExtents.z,2));
	float y = 0.08333f * (1.0f/m_fInverseMass) * (powf(vHalfExtents.x,2) + powf(m_vHalfExtents.z,2));
	float z = 0.08333f * (1.0f/m_fInverseMass) * (powf(vHalfExtents.x,2) + powf(m_vHalfExtents.y,2));

	InertiaTensor.data[0] = x;
	InertiaTensor.data[4] = y;
	InertiaTensor.data[8] = z;*/

	SetInertiaTensor(InertiaTensor);

	CalculateDerivedData();
			
}

Box::Box(int id, Vector3 vPosition, Vector3 vHalfExtents, bool iGraphical, int iBVid )
	:RigidBody(id, vPosition, iBVid), m_vHalfExtents(vHalfExtents)
{
	//if (iGraphical == true)
	//{
	//	m_iGraphicsID = IGH->CreateSquare();
	//	IGH->SetSquareScale(m_iGraphicsID, vHalfExtents.x*2, vHalfExtents.y*2, vHalfExtents.z*2);
	//}
	//else
		m_iGraphicsID = -1;

	

	m_qOrientation = Quaternion();

	m_fInverseMass = 1.0f / 10.0f;

	// inertial tensor for solid cube
	
	Matrix3 InertiaTensor;

	InertiaTensor.SetBlockInertiaTensor(vHalfExtents, GetMass());

	SetInertiaTensor(InertiaTensor);

	CalculateDerivedData();
			
}

Box::~Box(void)
{
}

void Box::VUpdate(float dt)
{
	//update physics
	RigidBody::VUpdate(dt);

	// update bv loc
	//ACMI->GetBV(m_iBoundingVolumeID)->VUpdate(dt);

	//if (m_iGraphicsID != -1)
	//{
	//	//update graphics position
	//	IGH->SetSquarePosition(m_iGraphicsID, m_vPosition.x, m_vPosition.y, m_vPosition.z);

	//	//update graphics orientation
	//	IGH->SetSquareRotation(m_iGraphicsID, m_qOrientation.x, m_qOrientation.y, m_qOrientation.z, m_qOrientation.w);
	//}
	
}