#pragma once

#pragma warning(disable : 4995)

// Safety include - add to add needed
#include "MovementManager.h"
//#include "Collisions/Detection/CollisionDetector.h"
//#include "Collisions/Detection/AdvCollisions.h"
#include "Objects/RigidBody.h"
#include "Objects/Ball.h"
//#include "Collisions/Bounding Volumes/Circle.h"
#include "Objects/Box.h"
#include "Constraints/ForceGenerator.h"
//#include "Constraints/ContactGenerator.h"