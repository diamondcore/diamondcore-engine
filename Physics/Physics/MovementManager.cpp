#include "MovementManager.h"
//#include "Collisions/Detection/CollisionDetector.h"
//#include "Collisions/Detection/AdvCollisions.h"
#include "Objects\RigidBody.h"
#include "Objects\Ball.h"
#include "Objects\Box.h"
#include "Constraints\ForceGenerator.h"
//#include "Constraints\ContactGenerator.h"
//#include "Collisions\Bounding Volumes\AABB.h"
//#include "../../DiamondCore/IGraphicsHandle.h"


MovementManager::MovementManager()
{
	m_iBodyID = 0;
}

MovementManager* MovementManager::Instance()
{
	static MovementManager instance;

	return &instance;
}


MovementManager::~MovementManager(void)
{
	
}

// single call init physics call
void MovementManager::InitPhysics()
{
	//create force registry
	m_pForces = new ForceRegistry();

	// reset body counter
	m_iBodyID = 0;

}

// single call physics run
void MovementManager::RunPhysics(float dt)
{
	
	//ACMI->Update(dt);
	Update(dt);
	
}

void MovementManager::ShutdownPhysics()
{
	//delete all boies
	DeleteAllBodies();

	//delete force registry
	delete m_pForces;
	m_pForces = NULL;

	
}

//calls update method for all moving entities
void MovementManager::Update(float dt)
{
	// Update forces
	m_pForces->updateForces(dt);

	// Clear out previous collision data
	// NOTE: doing it here because otherwise we'd have to do a lame "grab the list of bodies.

	for (auto it = m_vRigidBodies.begin(); it != m_vRigidBodies.end(); it++)
		(*it)->ClearCollisionList();

	//check for collisions
	//ACMI->Update(dt);

	// update movement
	for (auto it = m_vRigidBodies.begin(); it != m_vRigidBodies.end(); it++)
		(*it)->VUpdate(dt);	
}


///////////////////
// REGISTRATION
///////////////////
//registers new rigid body
void MovementManager::AddBody(RigidBody* pNewBody)
{
	
}

// creates a formless rigid body for use
int MovementManager::CreateBody(Vector3 vPosition, void* associatedObject)
{
	// create rigid body, no bv
	RigidBody* pTemp = new RigidBody(m_iBodyID, vPosition);

	pTemp->SetObjectRef(associatedObject);

	// push body onto vector
	m_vRigidBodies.push_back(pTemp);

	// push body into map
	m_mRigidBodies.insert(pair<int, RigidBody*> (m_iBodyID, pTemp));

	// increment ID system
	return m_iBodyID++;
}

// creates and registers bodies
int MovementManager::CreateBall(Vector3 vPosition, float fRadius)
{
	//create the rigid body and circle bv
	Ball* pTemp = new Ball(m_iBodyID,vPosition, fRadius/*, ACMI->CreateCircle(vPosition, fRadius, m_iBodyID)*/);

	// push body onto vector 
	m_vRigidBodies.push_back(pTemp);

	// push body into map
	m_mRigidBodies.insert(pair<int, RigidBody*>(m_iBodyID,pTemp) );

	// increment ID system
	return m_iBodyID++;

}
int MovementManager::CreateBoxA(Vector3 vPosition, Vector3 vHalfExtents)
{
	//// create the rigid body and AABB bv
	//Box* pTemp = new Box(m_iBodyID, vPosition, vHalfExtents, CMI->CreateAABB(vPosition, vHalfExtents, m_iBodyID)); 
	//
	//// push body onto vector 
	//m_vRigidBodies.push_back(pTemp);

	//// push body into map
	//m_mRigidBodies.insert(pair<int, RigidBody*>(m_iBodyID,pTemp) );

	// increment ID system
	return m_iBodyID++;
}
int MovementManager::CreateBoxO(Vector3 vPosition, Vector3 vHalfExtents)
{
	// create the rigid body and AABB bv
	Box* pTemp = new Box(m_iBodyID, vPosition, vHalfExtents/*, ACMI->CreateOBB(vPosition, vHalfExtents, m_iBodyID)*/); 
	
	// push body onto vector 
	m_vRigidBodies.push_back(pTemp);

	// push body into map
	m_mRigidBodies.insert(pair<int, RigidBody*>(m_iBodyID,pTemp) );

	// increment ID system
	return m_iBodyID++;
}

int MovementManager::CreateBoxNoGFX(Vector3 vPosition, Vector3 vHalfExtents)
{
	// create the rigid body and AABB bv
	Box* pTemp = new Box(m_iBodyID, vPosition, vHalfExtents,false/*, ACMI->CreateOBB(vPosition, vHalfExtents, m_iBodyID)*/ ); 
	
	// push body onto vector 
	m_vRigidBodies.push_back(pTemp);

	// push body into map
	m_mRigidBodies.insert(pair<int, RigidBody*>(m_iBodyID,pTemp) );

	// increment ID system
	return m_iBodyID++;
}

int MovementManager::CreateHalfSpace(Vector3 vNormal, float fOffset,bool display)
{
	
//	// special case in that a plane is not a rigid body and must be treated as only a piece of immobile 
//	// geometry in which to collide with. a large graphical square is created to represent the plane
//	ACMI->CreatePlane(vNormal,fOffset);
//	
//	if (display)
//	{
//		//create graphics object for it
//		int iGraphID = IGH->CreateSquare();
//
//		if (vNormal == Vector3(1,0,0) || vNormal == Vector3(-1,0,0))
//			IGH->SetSquareRotation(iGraphID,0,1,0,45 * DEG2RAD);
//		/*if (vNormal == Vector3(0,1,0) || vNormal == Vector3(0,-1,0))
//			IGH->SetSquareRotation(iGraphID,0,0,0);*/
//		if (vNormal == Vector3(0,0,1) || vNormal == Vector3(0,0,-1))
//			IGH->SetSquareRotation(iGraphID,0,1,0,1);
//
//		IGH->SetSquarePosition(iGraphID,vNormal.x * fOffset, vNormal.y * fOffset, vNormal.z * fOffset);
//
//		// change obb color to green
////		SMI->SetColor(iGraphID, YELLOW);
//	
//	return 0;//iGraphID;
//	}	

	return 1;
}

// Creates a box, but with a sphere equal to the smallest half-extent - obb detection would be too slow
int MovementManager::CreateBoxS(Vector3 vPosition, Vector3 vHalfExtents)
{
//	//find radius length - want the shortest half-extent to allow some interpenetration without issue
//	float radius = vHalfExtents.x;
//	if (vHalfExtents.y < radius)
//		radius = vHalfExtents.y;
//	if (vHalfExtents.z < radius)
//		radius = vHalfExtents.z;
//
//	radius *= 1.4f;
//	
//	// create the rigid body and circle bv
//	Box* pTemp = new Box(m_iBodyID, vPosition, vHalfExtents, ACMI->CreateCircle(vPosition, radius, m_iBodyID)); 
//	
//	//give it mass
//	pTemp->SetMass(50);
//
//	// change obb color to green
////	SMI->SetColor(pTemp->GetGraphicsID(), RED);
//	
//	// push body onto vector 
//	m_vRigidBodies.push_back(pTemp);
//
//	// push body into map
//	m_mRigidBodies.insert(pair<int, RigidBody*>(m_iBodyID,pTemp) );

	// add to object counter
//	GStats->addSphere(1);
		
	// return and increment ID system
	return m_iBodyID++;
}

///////////////////
// DELETION
///////////////////
void MovementManager::DeleteBody(int iBodyID)
{
	//delete from map
	m_mRigidBodies.erase(iBodyID);

	//delete from update vec
	for (unsigned int i = 0; i < m_vRigidBodies.size(); i++)
	{
		if (m_vRigidBodies[i]->GetID() == iBodyID)
		{

			// Delete it's BV if it had any
			auto bvid = m_vRigidBodies[i]->GetBoundingVolumeID();
			if (bvid >= 0 )
			{
				//ACMI->DeleteBV(bvid);
				m_vRigidBodies[i]->SetBVID(-1);
			}

			delete m_vRigidBodies[i];
			m_vRigidBodies.erase(m_vRigidBodies.begin() + i);
			return;
		}
	}
}

void MovementManager::DeleteAllBodies()
{
	//clear map
	m_mRigidBodies.clear();

	//delete all bodies
	for (unsigned int i = 0; i < m_vRigidBodies.size(); i++)
	{
		delete m_vRigidBodies[i];
	}

	//clear ptrs from vec
	m_vRigidBodies.clear();

	//reset body id counter
	m_iBodyID = 0;
}


	
//////////////////
// ACCESS
//////////////////
// returns rigid body ptr for access
RigidBody* MovementManager::GetBody(int iBodyID)
{

	return m_mRigidBodies.find(iBodyID)->second;
}

////////////////
// ACCESSORS
////////////////
// finds all neighbors within a radius of a point
std::vector<RigidBody*> MovementManager::GetNeighbors(Vector3 vCenter, float radius)
{
	std::vector<RigidBody*> vReturn;

	for (auto it = m_vRigidBodies.begin(); it != m_vRigidBodies.end(); it++)
	{
		if (((*it)->GetPosition() - vCenter).MagnitudeSquared() < radius * radius)
			vReturn.push_back((*it));
	}

	return vReturn;

}
// gets object's position
Vector3 MovementManager::GetPosition(int iBodyID)
{
	return GetBody(iBodyID)->GetPosition();
}
// Gets object's mass
float MovementManager::GetMass(int iBodyID) 
{
	return GetBody(iBodyID)->GetMass();
}
// Returns object's inverse mass (if 0, means object is immobile)
float MovementManager::GetInverseMass(int iBodyID) 
{
	return GetBody(iBodyID)->GetInverseMass();
}
// Returns a bool telling if an object can move or not
bool MovementManager::HasFiniteMass(int iBodyID) 
{
	return GetBody(iBodyID)->HasFiniteMass();
}

// Returns the value of the object's inertial tensor in body space
Matrix3 MovementManager::GetInertiaTensor(int iBodyID) 
{
	return GetBody(iBodyID)->GetInertiaTensor();
}
// Returns the value of the object's inertial tensor in world space 
Matrix3 MovementManager::GetInertiaTensorWorld(int iBodyID) 
{
	return GetBody(iBodyID)->GetInertiaTensorWorld();
}
// Returns value of object's inverse inertial tensor in body space
Matrix3 MovementManager::GetInverseInertiaTensor(int iBodyID) 
{
	return GetBody(iBodyID)->GetInverseInertiaTensor();
}
// Returns value of object's inverse inertial tensor
Matrix3 MovementManager::GetInverseInertiaTensorWorld(int iBodyID) 
{
	return GetBody(iBodyID)->GetInverseInertiaTensorWorld();
}
// Returns Linear Damping Value
float MovementManager::GetLinearDamping(int iBodyID) 
{
	return GetBody(iBodyID)->GetLinearDamping();
}
// REturns Angular Damping Value
float MovementManager::GetAngularDamping(int iBodyID) 
{
	return GetBody(iBodyID)->GetAngularDamping();
}
// Returns the values of the object's orientation
Quaternion MovementManager::GetOrientation(int iBodyID) 
{
	return GetBody(iBodyID)->GetOrientation();
}
// Returns value of transform matrix
Matrix4 MovementManager::GetTransform(int iBodyID) 
{
	return GetBody(iBodyID)->GetTransform();
}
// Returns value of object's velocity
Vector3 MovementManager::GetVelocity(int iBodyID) 
{
	return GetBody(iBodyID)->GetVelocity();
}
// returns rotation value item
Vector3 MovementManager::GetRotation(int iBodyID) 
{
	return GetBody(iBodyID)->GetRotation();
}
// returns last fram accel
Vector3 MovementManager::GetLastFrameAcceleration(int iBodyID) 
{
	return GetBody(iBodyID)->GetLastFrameAcceleration();
}
// returns acceleration value
Vector3 MovementManager::GetAcceleration(int iBodyID)
{
	return GetBody(iBodyID)->GetAcceleration();
}

std::list<void*> MovementManager::GetCollidingObjects(int bodyID)
{
	return GetBody(bodyID)->GetCollidingObjects();
}


////////////////
// MUTATORS
////////////////
// sets position of body
void MovementManager::SetPosition(int iBodyID, Vector3 vPosition)
{
	GetBody(iBodyID)->SetPosition(vPosition);
}
// sets orientation    
void MovementManager::SetOrientation(int iBodyID, const Quaternion &qOrientation)
{
	GetBody(iBodyID)->SetOrientation(qOrientation);
}
// sets orientation by passing individual values
void MovementManager::SetOrientation(int iBodyID, const float w, const float x,
                   const float y, const float z)
{
	GetBody(iBodyID)->SetOrientation(w,x,y,z);
}
// sets velocity 
void MovementManager::SetVelocity(int iBodyID, const Vector3 &vVelocity)
{
	GetBody(iBodyID)->SetVelocity(vVelocity);
}
// sets velocity by passing individual values
void MovementManager::SetVelocity(int iBodyID, const float x, const float y, const float z)
{
   GetBody(iBodyID)->SetVelocity(x,y,z);
}
// affects a change in velocity using the current velocity in the calculation
void MovementManager::AddVelocity(int iBodyID, const Vector3 &deltaVelocity)
{
    GetBody(iBodyID)->AddVelocity(deltaVelocity);
}
// sets the rotation value   
void MovementManager::SetRotation(int iBodyID, const Vector3 &vRotation)
{
	GetBody(iBodyID)->SetRotation(vRotation);
}
// sets rotation values individually
void MovementManager::SetRotation(int iBodyID, const float x, const float y, const float z)
{
	GetBody(iBodyID)->SetRotation(x,y,z);
}
// adds force to center of mass
void MovementManager::AddForce(int iBodyID, const Vector3 &force)
{
    GetBody(iBodyID)->AddForce(force);
}
// add force to point on object in world space
void MovementManager::AddForceAtPoint(int iBodyID, const Vector3 &force,
                                const Vector3 &point)
{
    GetBody(iBodyID)->AddForceAtPoint(force,point);
 
}
// manuall rotates by the given quaternion
void MovementManager::RotateBody(int iBodyID, Quaternion &qRotation)
{
	GetBody(iBodyID)->RotateBody(qRotation);
}
// add force to point on object (in body space)
void AddForceAtBodyPoint(int iBodyID, const Vector3 &force, const Vector3 &point)
{

}
// adds torque (angular accel) to object
void MovementManager::AddTorque(int iBodyID, const Vector3 &torque)
{
    GetBody(iBodyID)->AddTorque(torque);
} 
// adds rotation (angular vel) to object
void MovementManager::AddRotation(int iBodyID, const Vector3 &deltaRotation)
{
	GetBody(iBodyID)->AddRotation(deltaRotation);
}
void MovementManager::AddRotationX(int iBodyID, const float fRotationAmt)
{
	GetBody(iBodyID)->AddRotation(Vector3(fRotationAmt,0,0));
}
// scalar add rotation for a particular axis
void MovementManager::AddRotationY(int iBodyID, const float fRotationAmt)
{
	GetBody(iBodyID)->AddRotation(Vector3(0,fRotationAmt,0));
}
// scalar add rotation for a particular axis
void MovementManager::AddRotationZ(int iBodyID, const float fRotationAmt)
{
	GetBody(iBodyID)->AddRotation(Vector3(0,0,fRotationAmt));
}
// sets acceleration for object
void MovementManager::SetAcceleration(int iBodyID, const Vector3 &vAcceleration)
{
    GetBody(iBodyID)->SetAcceleration(vAcceleration);
}
// sets acceleration by individual axis
void MovementManager::SetAcceleration(int iBodyID, const float x, const float y, const float z)
{
	GetBody(iBodyID)->SetAcceleration(x,y,z);
}
// sets mass of object
void MovementManager::SetMass(int iBodyID, const float fMass)
{
	GetBody(iBodyID)->SetMass(fMass);
}
// sets mass of object
void MovementManager::SetInverseMass(int iBodyID, const float invMass)
{
	GetBody(iBodyID)->SetInverseMass(invMass);
}

// sets the transform of the rigidbody
void MovementManager::SetTransform(int iBodyID, Matrix4 mTransform)
{
	GetBody(iBodyID)->SetTransform(mTransform);
}
void MovementManager::SetBoundingOffset(int iBodyID, float x, float y, float z)
{
	
	Matrix4 temp;

	temp._14 = x;
	temp._24 = y;
	temp._34 = z;

	GetBody(iBodyID)->SetBoundingOffset(temp);
}


void MovementManager::SetObjectRef(int bodyID, void* objRef)
{
	GetBody(bodyID)->SetObjectRef(objRef);
}

//////////////
// FORCES
/////////////
void MovementManager::AddSpringForce(int iObjectID1, Vector3 vLocalAttachPt, 
	int iObjectID2, Vector3 vLocalAttachPt2, 
	float fSpringConstant, float fRestlength)
{
	m_pForces->AddSpringForce(iObjectID1,vLocalAttachPt,
		iObjectID2,vLocalAttachPt2,
		fSpringConstant,fRestlength);
}

void MovementManager::AddGravityForce(int iObjectID, Vector3 vGravVector)
{
	m_pForces->AddGravityForce(iObjectID,vGravVector);
}
void MovementManager::AddGravityForce(int iObjectID, float fGravityY)
{
	m_pForces->AddGravityForce(iObjectID, fGravityY);
}
////////////////
// COLLISIONS
///////////////
bool MovementManager::AABBTest(float minX1, float minY1, float minZ1, float maxX1, float maxY1, float maxZ1,
	float minX2, float minY2, float minZ2, float maxX2, float maxY2, float maxZ2)
{
	//create vectors from data given
	Vector3 vMin1(minX1,minY1,minZ1),
		vMax1(maxX1, maxY1, maxZ1),
		vMin2(minX2,minY2,minZ2),
		vMax2(maxX2, maxY2, maxZ2);

	//create to AABB's
	//AABB * box1 = new AABB(vMin1, vMax1);
	//AABB * box2 = new AABB(vMin2, vMax2);

	//// manual test to see if they are colliding
	//return CMI->BoxBoxAABBDet(box1, box2);

	return 0;
}

void MovementManager::AddOBBToBody(int id, Vector3 vHalfExtents)
{
	// create an obb bv and link to the rigid body

	//MMI->GetBody(id)->SetBVID(ACMI->CreateOBB(MMI->GetBody(id)->GetPosition(),vHalfExtents,id));


}