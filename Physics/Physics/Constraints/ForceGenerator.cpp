#include "ForceGenerator.h"
#include "../MovementManager.h"
#include "../Objects/RigidBody.h"
//#include "../Collisions/Detection/AdvCollisions.h"

//*************************
//FORCE REGISTRY
//*************************
void ForceRegistry::add(RigidBody* add, ForceGenerator* AddForce)
{
	//create an entry for the registry
	ForceRegistration temp;

	//assign values
	temp.RigidBody = add;
	temp.forceGenerator = AddForce;

	//push entry onto forceList
	m_forceList.push_back(temp);
}

//removes a registered pair from registry(forceList). if the pair is not registered, does nothing
void ForceRegistry::remove(RigidBody* deleteBody, ForceGenerator *deleteForce)
{
	Registry::iterator i = m_forceList.begin();
		for (i; i != m_forceList.end(); i++)
		{
			if (i->RigidBody == deleteBody)
			{
				if(i->forceGenerator == deleteForce)
				{
					m_forceList.erase(i);
				}

			}
		}

}

//clears all forces from force list (does not deleteBody the objects themselves, just clears the list)
void ForceRegistry::clear()
{
	Registry::iterator i = m_forceList.begin();
	for (i; i != m_forceList.end(); i++)
		delete i->forceGenerator;

	m_forceList.clear();
}

void ForceRegistry::updateForces(float time_elapsed)
{
	//iterates through list of forces and calls the update force method
	Registry::iterator i = m_forceList.begin();
	for (i; i != m_forceList.end(); i++)
		i->forceGenerator->UpdateForce(i->RigidBody,time_elapsed);
}

void ForceRegistry::AddSpringForce(int bodyID, Vector3 vBodyConnectionPt, int otherID, Vector3 vOtherConnectionPt, float springConstant, float restLength)
{
	//create spring force generator
	Spring * springForce1 = new Spring(vBodyConnectionPt,MMI->GetBody(otherID),vOtherConnectionPt,springConstant,restLength);
	Spring * springForce2 = new Spring(vOtherConnectionPt,MMI->GetBody(bodyID),vBodyConnectionPt,springConstant,restLength);

	//add forces and paired particles to particle registry, connecting the particles to each other.
	add(MMI->GetBody(bodyID),springForce1);
	add(MMI->GetBody(otherID),springForce2);

	//graphics representation with a line
//	springForce1->m_iGraphicsID = SMI->CreateLine(vBodyConnectionPt,vOtherConnectionPt);
	springForce2->m_iGraphicsID =  springForce1->m_iGraphicsID;

}

void ForceRegistry::AddSpringForce(int bodyID, int otherID, float springConstant, float restLength)
{
	//create spring force generator
	Spring * springForce1 = new Spring(MMI->GetBody(otherID),springConstant,restLength);
	Spring * springForce2 = new Spring(MMI->GetBody(bodyID),springConstant,restLength);

	//add forces and paired particles to particle registry, connecting the particles to each other.
	add(MMI->GetBody(bodyID),springForce1);
	add(MMI->GetBody(otherID),springForce2);

	//graphics representation with a line
	

}

void ForceRegistry::AddAnchoredSpringForce(int bodyID, float anchorX, float anchorY, 
	float anchorZ, float springConstant, float restLength)
{
	//create spring force generator
	AnchoredSpring * anchoredSpringForce = new AnchoredSpring(anchorX,anchorY,anchorZ,springConstant, restLength);

	//add forces and paired particles to particle registry, connecting the particles to each other.
	add(MMI->GetBody(bodyID),anchoredSpringForce);

	//ConnectLine(particle1,anchorX,anchorY,anchorZ);
}

void ForceRegistry::AddBungeeForce(int bodyID, int otherID, float springConstant, float restLength)
{
	//create bungee force generator
	Bungee * bungeeForce1 = new Bungee(MMI->GetBody(otherID),springConstant, restLength);
	Bungee * bungeeForce2 = new Bungee(MMI->GetBody(bodyID),springConstant, restLength);


	//add forces and paired particles to particle registry, connecting the particles to each other.
	add(MMI->GetBody(bodyID),bungeeForce1);
	add(MMI->GetBody(otherID),bungeeForce2);

	//ConnectLine(particle1,particle2);
}

void ForceRegistry::AddAnchoredBungeeForce(int bodyID, float anchorX, float anchorY, 
	float anchorZ, float springConstant, float restLength)
{
	//create bungee force generator
	AnchoredBungee * anchoredBungeeForce = new AnchoredBungee(anchorX, anchorY, anchorZ, springConstant,restLength);

	//add forces and paired particles to particle registry, connecting the particles to each other.
	add(MMI->GetBody(bodyID),anchoredBungeeForce);

	//graphics representation with a line
//	anchoredBungeeForce->m_iGraphicsID = SMI->CreateLine(Vector3(anchorX,anchorY,anchorZ),MMI->GetPosition(bodyID));

	
}


void ForceRegistry::AddGravityForce(int bodyID, Vector3 gravity)
{
	//create force generator
	Gravity* gravityForce = new Gravity(gravity);

	//add force to the registry
	add(MMI->GetBody(bodyID),gravityForce);
}

void ForceRegistry::AddGravityForce(int bodyID, float gravity)
{
	Vector3 gravityVec(0,gravity,0);

	//create force generator
	Gravity* gravityForce = new Gravity(gravityVec);

	//add force to the registry
	add(MMI->GetBody(bodyID),gravityForce);
}

void ForceRegistry::AddDragForce(int bodyID, float dragCoefK1, float dragCoefK2)
{
	//create drag force generator
	Drag * dragForce = new Drag(dragCoefK1,dragCoefK2);

	//add force to registry
	add(MMI->GetBody(bodyID),dragForce);
}

void ForceRegistry::AddBuoyancyForce(int bodyID, float maxDepth,float volume, float waterHeight, float liquidDensity = 1000.0f)
{
	//create buoyancy force generator
	Buoyancy * buoyancyForce = new Buoyancy(maxDepth,volume,waterHeight,liquidDensity);

	//add force to registry
	add(MMI->GetBody(bodyID), buoyancyForce);
	
}

void ForceRegistry::AddBlastForce(int bodyID, float blastForce)
{
	//create blast force generator
	Blast * blastForceGen = new Blast(blastForce);

	//add force to registry
	add(MMI->GetBody(bodyID), blastForceGen);
}

ForceRegistry::~ForceRegistry()
{
	clear();
}

//***************************
//END FORCE REGISTRY
//***************************


//***************************
//GRAVITY FORCE GENERATOR
//***************************
//creates the generator with the given acceleration
Gravity::Gravity(Vector3 gravity)
{
	m_gravity = gravity;
}

//applies the gravitational force to the given RigidBody
void Gravity::UpdateForce(RigidBody *Body, float time_elapsed)
{
	//make sure item doesn't have infinite mass
	if (!Body->HasFiniteMass())
		return;

	//apply the mass-scaled force to the RigidBody
	Body->AddForce(m_gravity * MMI->GetBody(Body->GetID())->GetMass());
}

//***************************
//END GRAVITY FORCE GENERATOR
//***************************

//***************************
//SPRING FORCE GENERATOR
//***************************

//creates a new spring with the given parameters
Spring::Spring(RigidBody *other, float springConstant, float restLength)
{
	m_other = other;
	m_springConstant = springConstant;
	m_restLength = restLength;
}

Spring::Spring(const Vector3 &localConnectionPt,
               RigidBody *other,
               const Vector3 &otherConnectionPt,
               float springConstant,
               float restLength)
: m_vConnectionPoint(localConnectionPt),
  m_vOtherConnectionPoint(otherConnectionPt),
  m_other(other),
  m_springConstant(springConstant),
  m_restLength(restLength)
{
}

void Spring::UpdateForce(RigidBody* body, float duration)
{
    // Calculate the two ends in world space
    Vector3 lws = body->GetPointInWorldSpace(m_vConnectionPoint);
    Vector3 ows = m_other->GetPointInWorldSpace(m_vOtherConnectionPoint);

	//update the line
//	SMI->SetLine(m_iGraphicsID,lws,ows);

    // Calculate the vector of the spring
    Vector3 force = lws - ows;

    // Calculate the magnitude of the force
    float magnitude = force.Magnitude();

	if (magnitude > m_restLength)
	{
		magnitude = fabs(magnitude - m_restLength);
		magnitude *= m_springConstant;

		 // Calculate the final force and apply it
		force.Normalize();
		force *= -magnitude;

		force.Scale(body->GetMass());
		force.Scale(.89f);
		body->AddForceAtPoint(force, lws);
	}
	else if (magnitude < m_restLength)
	{
		magnitude = fabs(magnitude - m_restLength);
		magnitude *= m_springConstant;

		 // Calculate the final force and apply it
		force.Normalize();
		force *= magnitude;

		force.Scale(body->GetMass());
		force.Scale(.89f);
		body->AddForceAtPoint(force, lws);
	}

    
}

////applies the spirng force to the given RigidBody
//void Spring::UpdateForce(RigidBody * RigidBody, float duration)
//{
//	//create a direction vector from the other RigidBody (hook) to the affected RigidBody
//	Vector3 x = RigidBody->GetPosition() - m_other->GetPosition();
//	
//	//get length of that vector
//	float Magnitude = x.Magnitude();
//	Magnitude -= m_restLength;
//	
//	//get the direction of that vector
//	x.Normalize();
//	Vector3 direction = x;
//
//	//calculating the force
//	Vector3 force = direction.ScaleReturn(-m_springConstant * Magnitude);
//	
//
//	//adding it to the force list
//	RigidBody->AddForce(force);
//
//}


//***************************
//END SPRING FORCE GENERATOR
//***************************

//***************************
//ANCHORED SPRING FORCE GENERATOR
//***************************

AnchoredSpring::AnchoredSpring()
{
}

AnchoredSpring::AnchoredSpring(Vector3 *anchor, float sc, float rl)
: m_anchor(anchor), m_springConstant(sc), m_restLength(rl)
{
}

AnchoredSpring::AnchoredSpring(float anchorX, float anchorY, float anchorZ, float springConstant, float restLength)
: m_springConstant(springConstant), m_restLength(restLength)
{
	m_anchor = new Vector3(anchorX,anchorY,anchorZ);
}


void AnchoredSpring::init(Vector3 *anchor, float springConstant, float restLength)
{
    m_anchor = anchor;
    m_springConstant = springConstant;
    m_restLength = restLength;
}

void AnchoredSpring::UpdateForce(RigidBody* RigidBody, float duration)
{
    // Calculate the vector of the spring
    Vector3 force;
    force = RigidBody->GetPosition();
    force -= *m_anchor;

    // Calculate the Magnitude of the force
    float Magnitude = force.Magnitude();
    Magnitude = (m_restLength - Magnitude) * m_springConstant;

    // Calculate the final force and apply it
    force.Normalize();
    force.Scale(Magnitude);
    RigidBody->AddForce(force);
}

//***************************
//END ANCHORED SPRING FORCE GENERATOR
//***************************

//***************************
//DRAG FORCE GENERATOR
//***************************

//creates the generator with the coefficients
Drag::Drag(float k1, float k2)
{
	m_k1 = k1;
	m_k2 = k2;
}

//applies the drag force to the given RigidBody
void Drag::UpdateForce(RigidBody* RigidBody, float time_elapsed)
{
	//get the current velocity
	Vector3 force;
    force = RigidBody->GetVelocity();

    // Calculate the total drag coefficient
    float dragCoeff = force.Magnitude();
    dragCoeff = m_k1 * dragCoeff + m_k2 * (dragCoeff * dragCoeff);

    // Calculate the final force and apply it
    //get direction of force
	force.Normalize();
	
	//Scale it to the negative drag coefficient (should go opposite of direction vector)
    force.Scale(-dragCoeff);

	//add the force to the forceList
    RigidBody->AddForce(force);
}

//***************************
//END DRAG FORCE GENERATOR
//***************************



//***************************
//BUNGEE FORCE GENERATOR
//***************************

Bungee::Bungee(RigidBody *other, float sc, float rl)
{
	m_other = other;
	m_springConstant = sc;
	m_restLength = rl;
}

void Bungee::UpdateForce(RigidBody* RigidBody, float duration)
{
	////calculating the force
	//Vector3 force = direction.ScaleReturn(-m_springConstant * Magnitude);

    // Calculate the vector of the bungee
    Vector3 force = RigidBody->GetPosition() - m_other->GetPosition();

    // Check if the bungee is compressed - if it is, do nothing
    float Magnitude = force.Magnitude();
	if (Magnitude <= m_restLength) 
		return;

    // Calculate the Magnitude of the force
	Magnitude = m_springConstant * (Magnitude - m_restLength);

    // Calculate the final force and apply it
    force.Normalize();
    force.Scale(-Magnitude);




    RigidBody->AddForce(force);
}

//***************************
//END BUNGEE FORCE GENERATOR
//***************************

//***************************
//ANCHORED BUNGEE FORCE GENERATOR
//***************************

AnchoredBungee::AnchoredBungee(float anchorX, float anchorY, float anchorZ, float springConstant, float restLength)
	:AnchoredSpring(anchorX, anchorY,anchorZ,springConstant,restLength)
{
	
}

void AnchoredBungee::UpdateForce(RigidBody* RigidBody, float duration)
{
    // Calculate the vector of the spring
    Vector3 force;
    force = RigidBody->GetPosition();
    force -= *m_anchor;

    // Calculate the Magnitude of the force
    float Magnitude = force.Magnitude();
    if (Magnitude < m_restLength) return;

    Magnitude = Magnitude - m_restLength;
    Magnitude *= m_springConstant;

//	SMI->SetLine(m_iGraphicsID,*m_anchor,RigidBody->GetPosition());

    // Calculate the final force and apply it
    force.Normalize();
    force.Scale(-Magnitude);

	force.Scale(RigidBody->GetMass());
    RigidBody->AddForce(force);
}


//***************************
//END ANCHORED BUNGEE FORCE GENERATOR
//***************************

//***************************
//BUOYANCY FORCE GENERATOR
//***************************

Buoyancy::Buoyancy(float maxDepth, float volume, float waterHeight, float liquidDensity)
:m_maxDepth(maxDepth), m_volume(volume), m_waterHeight(waterHeight), m_liquidDensity(liquidDensity)
{
}

void Buoyancy::UpdateForce(RigidBody* RigidBody, float duration)
{
    // Calculate the submersion depth
    float depth = RigidBody->GetPosition().y;

    // Check if we're out of the water
    if (depth >= m_waterHeight + m_maxDepth) 
		return;

    Vector3 force(0,0,0);

    // Check if we're at maximum depth
    if (depth <= m_waterHeight - m_maxDepth)
    {
        force.y = m_liquidDensity * m_volume;
        
		RigidBody->AddForce(force);
        return;
    }

    // Otherwise we are partly submerged
    force.y = m_liquidDensity * m_volume *
        (depth - m_maxDepth - m_waterHeight) / 2 * -m_maxDepth;

	force.Scale(RigidBody->GetMass());

    RigidBody->AddForce(force);
}

//***************************
//END BUOYANCY FORCE GENERATOR
//***************************

//*******************
//BLAST FORCE GENERATOR
//*******************
//360 degree blast
Blast::Blast(float blastForce)
	:m_blastForce(blastForce), m_directional(false), m_currentRadius(5), m_fuseTime(5)
{
	m_blastRadius = blastForce * 3;
}

//directional blast
Blast::Blast(float direction, float blastForce)
	:m_blastForce(blastForce), m_direction(direction), m_directional(true), m_currentRadius(5), m_fuseTime(5)
{
	m_blastRadius = blastForce * 3;
}

void Blast::UpdateForce(RigidBody *Body, float duration)
{
	//grenade creates mechanical energy - technically the blast isn't overly damaging
	//its the shrapnel that kills you. however, this is video games. 
	//theory is a grenade is just an expanding circle of death within the radius
	//in the game world, everything is customizable, so we can control the blast force
	//and the radius. generally, these are connected, so if the person leaves off the
	//blast radius, it is calculated by the blast force
	static bool bDetonated = false;

	if (!bDetonated)
	{
		//step 1: turn object red
//		SMI->SetColor(Body->GetGraphicsID(), RED);

		//cook time a.k.a. fuse time
		m_fuseTime -= duration;

		if(m_fuseTime <= 0.0f)
		{
			
		
			//find everything near the blast
			std::vector<RigidBody*> vNeighbors = MMI->GetNeighbors(Body->GetPosition(), m_blastRadius);
		
			Vector3 vDirection;
			for (auto it = vNeighbors.begin(); it != vNeighbors.end(); it++)
			{
				// get vec from blast to neighbor
				vDirection = (*it)->GetPosition() - Body->GetPosition();
			
				//normalize it
				vDirection.Normalize();

				//scale it times blast force
				vDirection.Scale(500);

				//add it to the velocity of the neighbor
				(*it)->AddVelocity(vDirection);

				// set detonated to tru
				bDetonated = true;
			}
		}// end if 
	}// end outter ifs
	else
	{
		//remove object
		

	}
}

//*******************
//BLAST FORCE GENERATOR
//*******************