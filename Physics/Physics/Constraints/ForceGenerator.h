#pragma once

#include "../Objects/RigidBody.h"


//forward declarations
class D3DLine;

class ForceGenerator
{
public:
	//id of force created (used for deletion/access)
	int m_iForceID;
	// id of graphics object associated with foce (most often a line)
	int m_iGraphicsID;

	virtual void UpdateForce(RigidBody *Body, float time_elapsed) = 0;
};
//*************************
//FORCE REGISTRY
//*************************

class ForceRegistry
{
protected:
	struct ForceRegistration //one force genreator and the RigidBody it applies to
	{
		RigidBody *RigidBody;
		ForceGenerator *forceGenerator;
	};

	typedef std::vector<ForceRegistration> Registry;

	Registry m_forceList; //list of all registered forces

public:

	//register the given force generator to apply the given RigidBody
	void add(RigidBody* add,ForceGenerator *addForce);

	//removes a registered pair from registry(forceList). if the pair is not registered, does nothing
	void remove(RigidBody* deleteBody, ForceGenerator *deleteForce);

	~ForceRegistry(void);

	//clears all forces from force list (does not deleteBody the objects themselves, just clears the list)
	void clear();

	//calls all the force generators to update the forces of their corresponding particles
	void updateForces(float time_elapsed);

	/////////////////////////
	// SIMPLIFIED INTERFACE
	////////////////////////

	void AddSpringForce(int bodyID, int otherID, float springConstant, float restLength);
	void AddSpringForce(int bodyID, Vector3 vBodyConnectionPt, int otherParticleID, Vector3 vOtherConnectionPoint, float springConstant, 
		float restLength);

	void AddAnchoredSpringForce(int bodyID, float anchorX, float anchorY, 
		float anchorZ, float springConstant, float restLength);

	void AddBungeeForce(int bodyID, int otherParticleID, float springConstant, 
		float restLength);

	void AddAnchoredBungeeForce(int bodyID, float anchorX, float anchorY, 
		float anchorZ, float springContant, float restLength);

	void AddGravityForce(int bodyID, float gravity);

	void AddGravityForce(int bodyID, Vector3 gravity);

	void AddWeightForce(int bodyID, float gravity);

	void AddDragForce(int bodyID, float dragCoefK1, float dragCoefK2);

	void AddBuoyancyForce(int bodyID, float maxDepth,float volume, float waterHeight, float liquidDensity);

	void AddBlastForce(int bodyID, float blastForce);
};

//***************************
//END PARTICLE FORCE REGISTRY
//***************************

//***************************
//GRAVITY FORCE GENERATOR
//***************************
class Gravity: public ForceGenerator
{
protected:
	//holds the acceleration due to gravity
	Vector3 m_gravity;

public:
	//creates the generator with the given acceleration
	Gravity(Vector3 gravity);

	//applies the gravitational force to the given RigidBody
	void UpdateForce(RigidBody *RigidBody, float time_elapsed);
};

//***************************
//END GRAVITY FORCE GENERATOR
//***************************



//***************************
//SPRING FORCE GENERATOR
//***************************

class Spring: public ForceGenerator
{
protected:

	//The point of connection of the spring, in local coordinates.
    Vector3 m_vConnectionPoint;

    //The point of connection of the spring to the other object, in that object's local coordinates. 
    Vector3 m_vOtherConnectionPoint;

	//RigidBody at the other end of the spring
	RigidBody *m_other;
	
	//holds the spring constant
	float m_springConstant;

	//rest length of sprint
	float m_restLength;


public:
	//creates a new spring with the given parameters
	Spring(RigidBody *other, float springConstant, float restLength);

	/** Creates a new spring with the given parameters. */
    Spring(const Vector3 &localConnectionPt,
               RigidBody *other,
               const Vector3 &otherConnectionPt,
               float springConstant,
               float restLength);

	//applies the spirng force to the given RigidBody
	void UpdateForce(RigidBody * RigidBody, float duration);
};

//***************************
//END SPRING FORCE GENERATOR
//***************************

//***************************
//ANCHORED SPRING FORCE GENERATOR
//***************************

class AnchoredSpring : public ForceGenerator
{
protected:
    /** The location of the anchored end of the spring. */
    Vector3 *m_anchor;

    /** Holds the sprint constant. */
    float m_springConstant;

    /** Holds the rest length of the spring. */
    float m_restLength;

public:
    AnchoredSpring();

    /** Creates a new spring with the given parameters. */
    AnchoredSpring(Vector3 *anchor, float springConstant, float restLength);
	AnchoredSpring(float anchorX, float anchorY, float anchorZ, float springConstant, float restLength);

    /** Retrieve the anchor point. */
    const Vector3* getAnchor() const { return m_anchor; }

    /** Set the spring's properties. */
    void init(Vector3 *anchor, float springConstant, float restLength);

    /** Applies the spring force to the given RigidBody. */
    virtual void UpdateForce(RigidBody *RigidBody, float duration);
};

//***************************
//END ANCHORED SPRING FORCE GENERATOR
//***************************

//***************************
//DRAG FORCE GENERATOR
//***************************
class Drag : public ForceGenerator
{
protected:
	//holds the velocity drag coefficient
	float m_k1;
	//holds the velocity squared drag coefficient
	float m_k2;

public:
	//creates the generator with the coefficients
	Drag(float k1, float k2);

	//applies the drag force to the given RigidBody
	void UpdateForce(RigidBody* RigidBody, float time_elapsed);
};

//***************************
//END DRAG FORCE GENERATOR
//***************************

//***************************
//BUNGEE FORCE GENERATOR
//***************************

class Bungee : public ForceGenerator
{
protected:
	/** The RigidBody at the other end of the spring. */
	RigidBody *m_other;

	/** Holds the spring constant. */
	float m_springConstant;

	//rest length
	float m_restLength;

public:

/** Creates a new bungee with the given parameters. */
Bungee(RigidBody *other, float springConstant, float restLength);

/** Applies the spring force to the given RigidBody. */
void UpdateForce(RigidBody *RigidBody, float duration);
};

//***************************
//END BUNGEE FORCE GENERATOR
//***************************

//***************************
//ANCHORED BUNGEE FORCE GENERATOR
//***************************
/* A force generator that applies a bungee force, where
* one end is attached to a fixed point in space.
*/
class AnchoredBungee : public AnchoredSpring
{
public:
	AnchoredBungee(float anchorX, float anchorY, float anchorZ, float springConstant, float restLength);

    /** Applies the spring force to the given RigidBody. */
    void UpdateForce(RigidBody *RigidBody, float duration);
};

//***************************
//END ANCHORED BUNGEE FORCE GENERATOR
//***************************

//***************************
//BUOYANCY FORCE GENERATOR
//***************************

class Buoyancy : public ForceGenerator
{

float m_maxDepth;

/**
    * The volume of the object.
    */
float m_volume;

/**
    * The height of the water plane above y=0. The plane will be
    * parrallel to the XZ plane.
    */
float m_waterHeight;

/**
    * The density of the liquid. Pure water has a density of
    * 1000kg per cubic meter.
    */
float m_liquidDensity;

public:

/** Creates a new buoyancy force with the given parameters. */
Buoyancy(float maxDepth, float volume, float waterHeight, float liquidDensity = 1000.0f);

/** Applies the buoyancy force to the given RigidBody. */

void UpdateForce(RigidBody *RigidBody, float duration);
};

//***************************
//END BUOYANCY FORCE GENERATOR
//***************************

//*******************
//BLAST FORCE GENERATOR
//*******************
class Blast : public ForceGenerator
{
protected:
	//blast radius 
	float m_blastRadius,m_currentRadius, m_blastForce, m_direction, m_fuseTime;
	bool m_directional, m_detonated;

public:
	//360 degree blast - set blast Radius
	Blast(float blastForce);

	//directional blast
	Blast(float direction, float blastForce);

	void UpdateForce(RigidBody *RigidBody, float duration);
		
	
};
//*******************
//END BLAST FORCE GENERATOR
//*******************
    
