#pragma once
#pragma warning(disable : 4995)

#include <math.h>
#include <sstream>
#include <string>
#include <vector>
#include <limits>
#include <cassert>
#include <iomanip>
#include <iostream>
#include <cmath>
 

#define float_MAX FLT_MAX    
#define float_sqrt sqrt
#define float_abs fabs
#define float_sin sin
#define float_cos cos
#define float_exp exp
#define float_pow pow
#define float_fmod fmod
#define R_PI 3.14159265358979

	
#define DAMPING .99f
#define DEG2RAD R_PI / 180.0f
#define ELASTICITY .90f



//degree to rad